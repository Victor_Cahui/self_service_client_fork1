export interface IParameterApp {
  codigo: string;
  valor: any;
  descripcion: string;
}

export interface IErrorModal {
  title?: string;
  message?: string;
  suggestion?: string;
  image?: string;
  icon?: string;
}
