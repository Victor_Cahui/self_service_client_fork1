export interface TypeDocument {
  valor: string;
  codigo: string;
  descripcion: string;
  longitudMaxima: number;
  longitudMinima: number;
  tipoDato: string;
}

export interface IdDocument {
  number: string;
  type: string;
}

export interface IdDocumentRequest {
  tipoDocumento: string;
  documento: string;
}
