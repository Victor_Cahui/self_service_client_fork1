import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BlankLayoutComponent } from '@mx-search/layouts';

const routes: Routes = [
  {
    path: '',
    loadChildren: '../main/views/landing/landing.module#LandingModule'
  },
  {
    path: '',
    component: BlankLayoutComponent,
    children: [
      {
        path: 'search',
        loadChildren: '../main/views/records/records.module#RecordsModule'
      },
      {
        path: 'not-found',
        loadChildren: '../main/pages/not-found/not-found.module#NotFoundModule'
      },
      {
        path: '**',
        redirectTo: '/not-found',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, { useHash: true, enableTracing: false, paramsInheritanceStrategy: 'always' })
  ],
  declarations: [],
  exports: [RouterModule]
})
export class RoutesModule {}
