import { environment } from '../../environments/environment';
import { APPLICATION_CODE } from '../../settings/constants/general-values';

export class GeneralEndpoint {
  public static parameters = `${environment.API_URL_COMUN}parametros/${APPLICATION_CODE}`;
  public static typesDocument = `${environment.API_URL_COMUN}tiposdocumento/{seccion}`;
}
