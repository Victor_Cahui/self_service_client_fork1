import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ServicesModule } from './../../../../core/shared/src/common/services/services-module';
import { ApiModule, NotificationModule } from './../../../../core/shared/src/helpers';
import { RecordSearchGuard } from './guards/record-search.guard';
import { CaptchaService } from './services/general/captcha.service';
import { GeneralService } from './services/general/general.service';
import { NavHelpers } from './services/general/nav-helpers.service';
import { RecordService } from './services/record.service';

@NgModule({
  imports: [CommonModule, ApiModule, NotificationModule, ServicesModule],
  declarations: [],
  providers: [GeneralService, RecordService, RecordSearchGuard, NavHelpers, CaptchaService]
})
export class ProvidersModule {}
