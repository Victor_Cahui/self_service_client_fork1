import { NgModule } from '@angular/core';
import { NotFoundComponent } from './not-found.component';
import { NotFoundRoutingModule } from './not-found.routing';

@NgModule({
  imports: [NotFoundRoutingModule],
  exports: [],
  declarations: [NotFoundComponent],
  providers: []
})
export class NotFoundModule {}
