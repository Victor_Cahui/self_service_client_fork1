import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormRecordsModule, InputSearchModule } from '@mx-search/components';
import { BuyPolicyModule } from '@mx-search/components/buy-policy/buy-policy.module';
import { RecordDetailModule } from '@mx-search/components/record-detail/record-detail.module';
import { ItemNotFoundModule } from '@mx-search/components/shared/item-not-found/item-not-found.module';
import { MfPaginatorModule } from '@mx/core/ui';
import { RecaptchaModule } from 'ng-recaptcha';
import { RecordsComponent } from './records.component';
import { RecordsRoutingModule } from './records.routing';

@NgModule({
  imports: [
    CommonModule,
    RecordsRoutingModule,
    FormRecordsModule,
    MfPaginatorModule,
    InputSearchModule,
    RecordDetailModule,
    BuyPolicyModule,
    ItemNotFoundModule,
    RecaptchaModule.forRoot()
  ],
  declarations: [RecordsComponent]
})
export class RecordsModule {}
