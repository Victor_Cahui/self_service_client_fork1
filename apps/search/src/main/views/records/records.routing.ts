import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RecordSearchGuard } from '@mx-search/guards';
import { RecordsComponent } from './records.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [RecordSearchGuard],
    component: RecordsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecordsRoutingModule {}
