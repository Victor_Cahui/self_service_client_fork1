import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { CaptchaService } from '@mx-search/services/general/captcha.service';
import { GeneralService } from '@mx-search/services/general/general.service';
import { RecordService } from '@mx-search/services/record.service';
import { ERROR_TYPES } from '@mx-search/settings/constants/errors-values';
import { GENERAL_MAX_LENGTH } from '@mx-search/settings/constants/general-values';
import { GeneralLang } from '@mx-search/settings/lang/general.lang';
import { RecordLang } from '@mx-search/settings/lang/record.lang';
import {
  IRecordDownloadRequest,
  IRecordFilter,
  IRecordInsuredResponse,
  IRecordRequest,
  IRecordResponse,
  RecordState
} from '@mx-search/statemanagement/models/record.interface';
import { FileUtil } from '@mx/core/shared/helpers/util/file';
import { Subscription, timer } from 'rxjs';

@Component({
  selector: 'search-records',
  templateUrl: './records.component.html'
})
export class RecordsComponent implements OnInit, OnDestroy {
  @ViewChild('captchaRef') captchaRef: any;

  page = 1;
  itemsPerPage = 10;
  currentSearch = '';
  record: IRecordResponse;
  insureds: Array<IRecordInsuredResponse>;
  tmpInsureds: Array<IRecordInsuredResponse>;
  lang = RecordLang;
  generalLang = GeneralLang;
  fileUtil: FileUtil;
  recordDownloadSub: Subscription;
  getRecordsSub: Subscription;
  capchatSub: Subscription;
  recordState = RecordState;
  maxLengthSearch = GENERAL_MAX_LENGTH.maxLengthSearch;
  showSpinnerPdf: boolean;
  filter: IRecordFilter;

  constructor(
    private readonly recordService: RecordService,
    private readonly generalService: GeneralService,
    private readonly capchatService: CaptchaService
  ) {
    this.fileUtil = new FileUtil();
  }

  ngOnInit(): void {
    this.capchatSub = this.capchatService.onReset.subscribe(() => this.captchaRef.reset());
  }

  ngOnDestroy(): void {
    if (this.getRecordsSub) {
      this.getRecordsSub.unsubscribe();
    }
    if (this.capchatSub) {
      this.capchatSub.unsubscribe();
    }
    this.unSubscribePDF();
  }

  searchFn(text?: string): void {
    // tslint:disable-next-line: no-parameter-reassignment
    text = (text || '').trim().toLocaleLowerCase();

    const all = this.insureds || [];
    this.tmpInsureds = !text ? all : all.filter(insured => insured.numDocumento.toLocaleLowerCase().startsWith(text));
  }

  scrollToRowActive(): void {
    const rowActive = document.querySelector(`.table--box--active`);
    if (rowActive !== null) {
      rowActive.scrollIntoView({ behavior: 'smooth', block: 'start' });
    }
  }

  getRecords(): void {
    const filter = this.filter;
    this.currentSearch = this.filter.numberDocument;
    const params: IRecordRequest = {
      nroConstancia: filter.numberRecord.toUpperCase(),
      tipoDocumento: filter.typeDocument,
      documento: filter.numberDocument
    };

    this.capchatService.handler();
    this.unSubscribePDF();
    this.getRecordsSub = this.recordService.getRecords(params).subscribe(
      record => {
        this.record = record || {};
        if (record.estado === this.recordState.NotValid) {
          record.nroConstancia = filter.numberRecord.replace(/_/g, '/');
        }
        this.insureds = (record.asegurados || []).map(insured => {
          insured.fullName = `${insured.nombre} ${insured.apellidoPaterno} ${insured.apellidoMaterno}`;

          return insured;
        });

        this.page = this.getPageDocument(this.insureds || [], this.filter.numberDocument, this.itemsPerPage);

        if (this.insureds.length) {
          const timeOut = timer(100).subscribe(() => {
            this.scrollToRowActive();
            timeOut.unsubscribe();
          });
        }

        this.searchFn();
        this.getRecordsSub.unsubscribe();
      },
      () => {
        this.getRecordsSub.unsubscribe();
      }
    );
  }

  getPageDocument(record: Array<IRecordInsuredResponse>, search: string, itemsPerPage: number): number {
    let page = 1;
    const allPage = Math.ceil(record.length / itemsPerPage);
    for (let index = 1; index <= allPage; index++) {
      const result = record
        .slice((index - 1) * itemsPerPage, index * itemsPerPage)
        .find((item: IRecordInsuredResponse) => item.numDocumento === search);
      if (result !== undefined) {
        page = index;
        break;
      }
    }

    return page;
  }

  downloadRecord(): void {
    const numConstanciaSpli = (this.record.nroConstancia || '').split('/') || [''];
    const numeroConstancia = numConstanciaSpli[numConstanciaSpli.length - 1];
    const params: IRecordDownloadRequest = {
      numPoliza: this.record.numPoliza,
      numeroConstancia
    };

    this.showSpinnerPdf = true;
    this.generalService.setDefaultError(ERROR_TYPES['900']);
    this.recordDownloadSub = this.recordService.downloadRecord(params).subscribe(
      record => {
        this.fileUtil.download(record.base64, 'pdf', this.record.nroConstancia);
        this.generalService.setDefaultError(null);
        this.unSubscribePDF();
      },
      err => {
        this.generalService.setDefaultError(null);
        this.unSubscribePDF();
      }
    );
  }

  clear(): void {
    this.record = null;
    this.insureds = [];
    this.tmpInsureds = [];
    this.unSubscribePDF();
  }

  unSubscribePDF(): void {
    this.showSpinnerPdf = false;
    if (this.recordDownloadSub) {
      this.recordDownloadSub.unsubscribe();
    }
  }

  resolved(response: string): void {
    this.capchatService.setCaptchaResponse(response);
    if (response) {
      this.getRecords();
    }
  }

  executeCaptcha(filter: IRecordFilter): void {
    this.filter = filter;
    setTimeout(() => {
      const storage = this.capchatService.getCaptchaResponse();
      const response: string = storage || (this.captchaRef.grecaptcha && this.captchaRef.grecaptcha.getResponse());
      this.captchaRef.execute();
      if (response) {
        this.capchatService.setCaptchaResponse(response);
        this.getRecords();
      }
    }, 100);
  }
}
