import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FooterModule } from '@mx-search/components';
import { LandingComponentsModule } from '@mx-search/components/landing/landing-components.module';
import { LandingComponent } from './landing.component';
import { LandingRoutingModule } from './landing.routing';

@NgModule({
  imports: [
    RouterModule,
    LandingRoutingModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    LandingComponentsModule,
    FooterModule
  ],
  declarations: [LandingComponent]
})
export class LandingModule {}
