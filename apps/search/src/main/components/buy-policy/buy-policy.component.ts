import { Component, OnInit } from '@angular/core';
import { RecordLang } from '@mx-search/settings/lang/record.lang';
import { SEARCH_ICON } from '@mx/settings/constants/images-values';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'search-buy-policy',
  templateUrl: './buy-policy.component.html'
})
export class BuyPolicyComponent implements OnInit {
  lang = RecordLang;
  quote_url = environment.MAPFRE_QUOTE_SCTR;
  iconSearch = SEARCH_ICON;

  ngOnInit(): void {}
}
