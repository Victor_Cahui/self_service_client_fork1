import { Component } from '@angular/core';
import { BaseContactInfo } from '@mx-search/components/shared/base-contact-info';
import { GeneralService } from '@mx-search/services/general/general.service';

@Component({
  selector: 'search-footer',
  templateUrl: './footer.component.html'
})
export class FooterComponent extends BaseContactInfo {
  constructor(protected generalService: GeneralService) {
    super(generalService);
  }
}
