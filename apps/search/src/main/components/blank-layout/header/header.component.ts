import { Component } from '@angular/core';
import { BaseContactInfo } from '@mx-search/components/shared/base-contact-info';
import { GeneralService } from '@mx-search/services/general/general.service';
import * as generalValues from '@mx-search/settings/constants/general-values';

@Component({
  selector: 'search-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent extends BaseContactInfo {
  title = generalValues.TITLE;

  constructor(protected generalService: GeneralService) {
    super(generalService);
  }
}
