import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MfButtonModule, MfInputModule, MfSelectModule, MfShowErrorsModule } from '@mx/core/ui';
import { FormRecordsComponent } from './form-records.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MfInputModule,
    MfShowErrorsModule,
    MfSelectModule,
    MfButtonModule
  ],
  declarations: [FormRecordsComponent],
  exports: [FormRecordsComponent]
})
export class FormRecordsModule {}
