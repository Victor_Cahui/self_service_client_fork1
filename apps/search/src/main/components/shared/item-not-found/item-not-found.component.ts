import { Component, Input, OnInit } from '@angular/core';
import { GeneralLang } from '@mx-search/settings/lang/general.lang';
import { SEARCH_ICON } from '@mx/settings/constants/images-values';

@Component({
  selector: 'search-item-not-found',
  templateUrl: './item-not-found.component.html'
})
export class ItemNotFoundComponent implements OnInit {
  @Input() text: string;
  @Input() title = GeneralLang.Messages.Fail;
  @Input() show: boolean;
  @Input() icon = SEARCH_ICON;
  @Input() large: boolean;

  ngOnInit(): void {
    // Se crea esta Imagen, para tener precargada y no se muestre
    // el efecto tosco al abrir este componente
    const image = new Image();
    image.src = this.icon;
  }
}
