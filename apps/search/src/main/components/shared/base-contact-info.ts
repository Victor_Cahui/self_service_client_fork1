import { OnDestroy } from '@angular/core';
import { GeneralService } from '@mx-search/services/general/general.service';
import { GeneralLang } from '@mx-search/settings/lang/general.lang';
import { Subscription } from 'rxjs';

export class BaseContactInfo implements OnDestroy {
  phoneNumber: string;
  email: string;
  contact: string;
  lang = GeneralLang.Texts;
  lima = GeneralLang.Texts.Lima;
  paramsSub: Subscription;

  constructor(protected generalService: GeneralService) {
    this.initData();
  }

  initData(): void {
    this.paramsSub = this.generalService.getParameters().subscribe(
      () => {
        this.phoneNumber = this.generalService.getPhoneNumber();
        this.email = this.generalService.getEmail();
        this.contact = this.email
          ? `<a href="mailto:${this.email}">${this.lang.WritteUsYourQuestion}</a>`
          : this.lang.WritteUsYourQuestion;
      },
      () => {
        this.paramsSub.unsubscribe();
      }
    );
  }

  ngOnDestroy(): void {
    if (this.paramsSub) {
      this.paramsSub.unsubscribe();
    }
  }
}
