import { ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FormComponent } from '@mx-search/components/utils/form';
import { GeneralService } from '@mx-search/services/general/general.service';
import { GENERAL_MAX_LENGTH, REGEX } from '@mx-search/settings/constants/general-values';
import { GeneralLang } from '@mx-search/settings/lang/general.lang';
import { RecordLang } from '@mx-search/settings/lang/record.lang';
import { TypeDocument } from '@mx-search/statemanagement/models/typedocument.interface';
import { PLATFORMS, PlatformService } from '@mx/core/shared/helpers/util/platform.service';
import { MfInput, SelectListItem } from '@mx/core/ui';
import { isEmpty } from 'lodash-es';
import { timer } from 'rxjs';

export abstract class BaseFormQuery {
  @ViewChild('inputRecord') inputRecord: MfInput;

  form: FormGroup;
  numberRecord: AbstractControl;
  typeDocument: AbstractControl;
  numberDocument: AbstractControl;

  typesDocuments: Array<TypeDocument>;
  arrTypesDocuments: Array<SelectListItem> = [];
  changedNumberDocument = true;
  changedNumberRecord = true;
  generalLang = GeneralLang;
  recordLang = RecordLang;
  maxLengthRecord = GENERAL_MAX_LENGTH.maxLengthRecord;
  minLengthRecord = GENERAL_MAX_LENGTH.minLengthRecord;
  maxLength: number;
  minLength: number;
  isIE: boolean;
  regex = REGEX;

  constructor(
    protected fb: FormBuilder,
    protected generalService: GeneralService,
    protected router: Router,
    protected platformService: PlatformService
  ) {
    this.isIE = this.platformService.is(PLATFORMS.IE);
  }

  initView(): void {
    this.generalService.getTypesDocument().subscribe(types => {
      this.arrTypesDocuments = this.setTypesDocumentsData(types || []);
      this.createForm();
      this.getDefaultData();
    });
  }

  createForm(): void {
    this.form = this.fb.group({
      numberRecord: [{ value: '', disabled: false }, [Validators.required, Validators.pattern(REGEX.record)]],
      typeDocument: [{ value: '', disabled: false }, Validators.required],
      numberDocument: [{ value: '', disabled: false }, Validators.required]
    });
    this.numberRecord = this.form.controls['numberRecord'];
    this.typeDocument = this.form.controls['typeDocument'];
    this.numberDocument = this.form.controls['numberDocument'];
  }

  getDefaultData(): void {}

  nextClear(): void {}

  setTypesDocumentsData(typesDocuments: Array<TypeDocument>): Array<SelectListItem> {
    this.typesDocuments = typesDocuments;
    const arr = typesDocuments.map(
      (obj: TypeDocument) =>
        new SelectListItem({
          text: obj.codigo,
          value: obj.codigo
        })
    );

    return [...arr];
  }

  eventChangedDocument($event: any): void {
    const typeDocument = this.getTypeDocumentByCode($event.target.value);
    if (typeDocument) {
      this.setControlNumberDocument(typeDocument);
    }
    this.changeNumberDocument();
  }

  setControlNumberDocument(typeDocument: TypeDocument): void {
    this.minLength = typeDocument.longitudMinima;
    this.maxLength = typeDocument.longitudMaxima;

    const validators = FormComponent.getValidatorTypeDocument(typeDocument.codigo, this.typesDocuments);
    this.numberDocument.setValidators(validators);
  }

  setControl(control: AbstractControl, value: any): void {
    if (value) {
      control.setValue(value);
    } else {
      control.setValue('');
      control.markAsPristine();
      control.markAsUntouched();
    }
  }

  showErrors(control: AbstractControl): boolean {
    return !!control && !isEmpty(control.errors) && (control.touched || control.dirty);
  }

  setValues(numberRecord?: string, typeDocument?: string, numberDocument?: string): void {
    if (!numberRecord) {
      this.nextClear();
    }
    const value = typeDocument;
    this.setControl(this.typeDocument, value);
    this.eventChangedDocument({ target: { value } });
    this.changeNumberRecord(numberRecord);
    this.changeNumberDocument(numberDocument);
  }

  changeNumberDocument(value?: string): void {
    this.changedNumberDocument = false;
    this.setControl(this.numberDocument, value || '');
    const timer$ = timer(0).subscribe(() => {
      this.changedNumberDocument = true;
      timer$.unsubscribe();
    });
  }

  changeNumberRecord(value?: string): void {
    this.changedNumberRecord = false;
    this.setControl(this.numberRecord, value || '');
    const timer$ = timer(0).subscribe(() => {
      this.changedNumberRecord = true;
      timer$.unsubscribe();
    });
  }

  getTypeDocumentByCode(code: string): TypeDocument {
    return this.typesDocuments.find((obj: TypeDocument) => obj.codigo === code);
  }

  onFocusNumberRecord(): void {
    if (!this.numberRecord.value) {
      const date = new Date();
      this.numberRecord.markAsUntouched();
      this.numberRecord.markAsPristine();
      this.setControl(this.numberRecord, `MP/${date.getFullYear()}/`);

      if (this.isIE && this.inputRecord) {
        const native = this.inputRecord.input_native.nativeElement;
        native.setSelectionRange(8, 8);
      }
    }
  }
}
