import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DirectivesModule } from '@mx/core/ui';
import { RecordDetailComponent } from './record-detail.component';

@NgModule({
  imports: [CommonModule, DirectivesModule],
  declarations: [RecordDetailComponent],
  exports: [RecordDetailComponent]
})
export class RecordDetailModule {}
