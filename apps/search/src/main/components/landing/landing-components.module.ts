import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MfButtonModule, MfInputModule, MfSelectModule, MfShowErrorsModule } from '@mx/core/ui';
import { TooltipsModule } from '@mx/core/ui/lib/components/tooltips/tooltips.module';
import { LadingStepsComponent } from './lading-steps/lading-steps.component';
import { LandingFormComponent } from './landing-form/landing-form.component';
import { LandingHeaderComponent } from './landing-header/landing-header.component';
import { LandingSectionComponent } from './landing-section/landing-section.component';
import { SwitchNavbarComponent } from './switch-navbar/switch-navbar.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    MfSelectModule,
    MfButtonModule,
    MfInputModule,
    MfShowErrorsModule,
    TooltipsModule
  ],
  exports: [
    LandingHeaderComponent,
    LandingSectionComponent,
    LandingFormComponent,
    SwitchNavbarComponent,
    LadingStepsComponent
  ],
  declarations: [
    LandingHeaderComponent,
    LandingSectionComponent,
    LandingFormComponent,
    SwitchNavbarComponent,
    LadingStepsComponent
  ],
  providers: []
})
export class LandingComponentsModule {}
