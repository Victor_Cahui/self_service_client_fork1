import { Component } from '@angular/core';
import { RecordLang } from '@mx-search/settings/lang/record.lang';

@Component({
  selector: 'search-landing-section',
  templateUrl: './landing-section.component.html'
})
export class LandingSectionComponent {
  recordLang = RecordLang;
}
