import { DOCUMENT } from '@angular/common';
import { Component, HostListener, Inject, OnInit, Renderer2 } from '@angular/core';
import { NavHelpers } from '@mx-search/services/general/nav-helpers.service';
import { NavbarLang } from '@mx-search/settings/lang/navbar.lang';

@Component({
  selector: 'search-switch-navbar',
  templateUrl: './switch-navbar.component.html'
})
export class SwitchNavbarComponent implements OnInit {
  lang = NavbarLang;
  constructor(
    public h: NavHelpers,
    public renderer2: Renderer2,
    @Inject(DOCUMENT) private readonly document: Document
  ) {}

  ngOnInit(): void {}

  @HostListener('window:scroll', ['$event'])
  public onWindowScroll($event): void {
    const e1 = this.document.getElementById('home');
    const e2 = this.document.getElementById('activity');
    const activity: HTMLElement = this.document.querySelector('.g-view-landing-activity');
    const nav: HTMLElement = document.querySelector('.fixed--box');
    nav.style.display = window.pageYOffset > activity.offsetTop - 200 ? 'block' : 'none';
    this.renderer2.removeClass(e1, 'g-font--bold');
    if (window.pageYOffset < activity.offsetTop - 200) {
      this.renderer2.addClass(e2, 'g-font--bold');
    }
  }
}
