import { Component, OnInit, ViewChild } from '@angular/core';
import { GeneralService } from '@mx-search/services/general/general.service';
import { ERROR_TYPES, HTTP_CODE_GATEWAY_TIMEOUT } from '@mx-search/settings/constants/errors-values';
import { GeneralLang } from '@mx-search/settings/lang/general.lang';
import { IErrorModal } from '@mx-search/statemanagement/models/general.models';
import { NotificationService } from '@mx/core/shared/helpers/notification/service/notification.service';
import { MfModalAlertComponent } from '@mx/core/ui/lib/components/modals/modal-alert/modal-alert.component';
import { Subscription } from 'rxjs';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'search-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  @ViewChild(MfModalAlertComponent) modalError: MfModalAlertComponent;

  parametersSub: Subscription;
  typesDocumentSub: Subscription;
  errorHandlerSub: Subscription;
  snipperSubjectSub: Subscription;

  loader: boolean;
  descriptionLoader = GeneralLang.Messages.LoadingWithoutDots;
  btnConfirm = GeneralLang.Buttons.Confirm;
  errorModalContent: IErrorModal = {};

  constructor(
    private readonly generalService: GeneralService,
    private readonly notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.loadingFullScreen();
    this.handlerError();

    this.getParams();
    this.getTypesDocuments();
  }

  loadingFullScreen(): void {
    this.snipperSubjectSub = this.notificationService
      .getSpinner()
      .pipe(delay(0))
      .subscribe((loader: boolean) => {
        this.loader = loader;
      });
  }

  handlerError(): void {
    // Gestión de errores
    this.modalError.sizeClass = 'g-modal--medium';
    this.errorHandlerSub = this.notificationService.getNotifies().subscribe(error => {
      const currentError = this.notificationService.getLastError();
      if (currentError) {
        const errorData = currentError.data as any;
        const typeError = errorData.status || HTTP_CODE_GATEWAY_TIMEOUT;
        this.errorModalContent = ERROR_TYPES[typeError];
        if (this.errorModalContent) {
          this.errorModalContent = JSON.parse(JSON.stringify(this.errorModalContent));
          if (errorData.status !== HTTP_CODE_GATEWAY_TIMEOUT) {
            this.errorModalContent.message = errorData.error.mensaje || this.errorModalContent.message;
            this.errorModalContent = this.generalService.getDefaultError() || this.errorModalContent;
          }
          this.modalError.open();
        }
      }
    });
  }

  getParams(): void {
    this.parametersSub = this.generalService.getParametersApi().subscribe(
      params => {
        this.generalService.setParameters(params);
        this.parametersSub.unsubscribe();
      },
      () => {
        this.parametersSub.unsubscribe();
      }
    );
  }

  getTypesDocuments(): void {
    this.typesDocumentSub = this.generalService.getTypesDocumentApi().subscribe(
      types => {
        this.generalService.setTypesDocument(types);
        this.typesDocumentSub.unsubscribe();
      },
      () => {
        this.typesDocumentSub.unsubscribe();
      }
    );
  }
}
