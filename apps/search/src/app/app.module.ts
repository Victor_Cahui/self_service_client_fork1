import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { BlankLayoutModule } from '@mx-search/layouts';
import { RoutesModule } from '@mx-search/routes/routes.module';
import { MfLoaderModule, MfModalAlertModule } from '@mx/core/ui';
import { ProvidersModule } from '../providers/providers.module';
import { AppComponent } from './app.component';
import { RecaptchaModule } from './recaptcha.module';

@NgModule({
  imports: [
    BrowserModule,
    RoutesModule,
    BlankLayoutModule,
    ProvidersModule,
    MfLoaderModule,
    MfModalAlertModule,
    RecaptchaModule
  ],
  declarations: [AppComponent],
  bootstrap: [AppComponent]
})
export class AppModule {}
