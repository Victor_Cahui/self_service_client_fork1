import { NgModule } from '@angular/core';
import { environment } from '@mx-search/environments/environment';
import { RECAPTCHA_LANGUAGE, RECAPTCHA_SETTINGS, RecaptchaModule as Recaptcha, RecaptchaSettings } from 'ng-recaptcha';

@NgModule({
  imports: [Recaptcha],
  providers: [
    {
      provide: RECAPTCHA_SETTINGS,
      useValue: { siteKey: environment.RECAPTCHA.API_KEY } as RecaptchaSettings
    },
    {
      provide: RECAPTCHA_LANGUAGE,
      useValue: 'es'
    }
  ]
})
export class RecaptchaModule {}
