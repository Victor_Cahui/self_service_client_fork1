// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  API_URL_COMUN: 'https://api.mapfre.com.pe/legacy/ascomun/v2/',
  API_URL_SCTR: '/api/v4/sctr/',
  API_URL_DOWNLOAD_RECORD: 'https://api.mapfre.com.pe/legacy/asweb/downloadConstancia',
  MAPFRE_QUOTE_SCTR: 'https://sctr.mapfre.com.pe',

  RECAPTCHA: {
    API_KEY: '6Lfh94YUAAAAAAeyD3M2DuKhEmYFAYDfaVIVKPQL',
    mode: 'consultas', // tiempo (minutos) || consultas (#)
    value: 3
  }
};

// tslint:disable-next-line: comment-type
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
