export const HTTP_CODE_GATEWAY_TIMEOUT = 504;

export const ERROR_TYPES = {
  500: {
    title: 'Estimado Usuario',
    message: 'Ha ocurrido un error.',
    suggestion: 'Por favor, inténtalo más tarde.',
    icon: 'icon--fail'
  },
  504: {
    title: 'Estimado Usuario',
    message: 'Ha ocurrido un error.',
    suggestion: 'Por favor, inténtalo más tarde.',
    image: 'assets/images/icons/404.svg'
  },
  900: {
    title: '¡Oops!',
    message: 'En estos momentos no es posible descargar el archivo.',
    suggestion: 'Por favor, inténtalo más tarde.',
    icon: 'icon--fail'
  },
  901: {
    title: '¡Oops!',
    message: 'La acción realizada no se ha completado.',
    suggestion: 'Por favor, inténtalo más tarde.',
    icon: 'icon--fail'
  }
};
