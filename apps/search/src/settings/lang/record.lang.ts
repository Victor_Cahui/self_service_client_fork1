import { StepsLang } from './steaps.lang';

export const RecordLang = {
  Titles: {
    CheckYour: 'Consulta tu',
    SctrConstancy: 'Constancia SCTR',
    VerifyYourConstancy: '¡Verifica tu constancia!',
    QueryConstancy: 'BÚSQUEDA DE CONSTANCIAS'
  },
  Buttons: {
    CheckConstancy: 'Consultar constancia'
  },
  Labels: {
    NumberRecord: 'Número de constancia',
    DocumentType: 'Tipo de documento',
    DocumentNumber: 'Número de documento',
    DocumentNumberAbrev: 'Nro. de documento',
    SelectDocumentType: 'Selecciona Tipo de documento',
    FullName: 'Nombre completo',
    Insured: 'Asegurado',
    Insureds: 'Asegurados'
  },
  Detail: {
    title1: 'La constancia',
    title2: 'se encuentra vigente.',
    title3: 'no se encuentra vigente.',
    Validity1: 'Vigencia: Inicio',
    Validity2: '- Final',
    Contractor: 'Contratante',
    PlaceActivity: 'Lugar de actividad',
    DownloadRecord: 'Descargar constancia',
    ActiveYourSCTR: 'Para mayor información, escríbenos un correo a la siguiente dirección:'
  },
  Messages: {
    EnterData: 'Ingresa los siguientes datos para poder realizar la consulta'
  },
  Tooltips: {
    ConstancyNumber: `
      <div class="g-font--medium g-mb--15">¿Qué es el número de constancia?</div>
      <div class="g-mb--15">
        ${StepsLang.Step1.Description}
      </div>
      <img alt="" src="assets/images/tooltips/1.jpg" />
    `,
    DocumentType: `
      <div class="g-font--medium g-mb--15">¿Cuál documento debo utilizar?</div>
      <div class="g-mb--15">
        ${StepsLang.Step2.Description}
      </div>
      <img alt="" src="assets/images/tooltips/2.jpg" />
    `
  },
  Buy: {
    Title: '¡Sin resultados!',
    Parr1: 'No se encontró ninguna coincidencia para la información ingresada.',
    Parr2Start: 'Si necesitas un SCTR puedes adquirirlo en breves minutos haciendo click en',
    Parr2End: 'el siguiente botón.',
    Button: 'NECESITO UN SCTR'
  }
};
