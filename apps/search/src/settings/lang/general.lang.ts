export const GeneralLang = {
  Buttons: {
    Confirm: 'Aceptar',
    Search: 'Buscar',
    Clear: 'Limpiar'
  },
  Links: {
    QuoteNowPolicy: 'Cotiza ahora una póliza'
  },
  Titles: {
    Title404: 'Página no encontrada'
  },
  Messages: {
    Fail: '¡Oops!',
    Message404: 'No hay nada por aquí.',
    DoYouNeedSctr: '¿Necesitas un SCTR?',
    LoadingWithoutDots: 'Cargando',
    ItemNotFound: 'No se encontró ninguna coincidencia.'
  },
  Texts: {
    Lima: 'Lima',
    Need: 'Necesitas',
    Assists: 'asistencia',
    WritteUsYourQuestion: 'Escríbenos tu consulta',
    Copyright: '© Copyright MAPFRE PERÚ. Todos los derechos reservados'
  }
};
