import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { AlertNotify, ErrorNotify, MessageNotify, Notify, TYPES_NOTIFY, WarningNotify } from '..';

@Injectable()
export class NotificationService {
  private notifies: Array<Notify>;
  notifiesBehaviorSubject: BehaviorSubject<Array<Notify>>;

  constructor() {
    this.init();
  }

  /**
   * Inicializador del servicios de Notificaciones
   */
  init(): void {
    this.notifies = new Array<AlertNotify>();
    this.notifiesBehaviorSubject = new BehaviorSubject<Array<AlertNotify>>(this.notifies);
  }

  /**
   * Agregar una notificación de Alerta
   * @param message Descripción de notificación de Alerta
   */
  addAlert(message: string): void {
    this.add(new AlertNotify(message));
  }

  /**
   * Agreagr una notificación tipo Mensaje
   * @param message Descripción de notificación tipo Mensaje
   */
  addMessage(message: string): void {
    this.add(new MessageNotify(message));
  }

  /**
   * Agregar una notificación tipo Advertencia
   * @param message Descripción de notificación tipo Advertencia
   */
  addWarning(message: string): void {
    this.add(new WarningNotify(message));
  }

  /**
   * Agregar una notificación de tipo Error
   * @param message Descripción de notificación tipo Error
   */
  addError(message: string): void {
    this.add(new ErrorNotify(message));
  }

  getNotifies(): BehaviorSubject<Array<Notify>> {
    return this.notifiesBehaviorSubject;
  }

  /**
   * Retorna todas las notificaciones tipo Alerta
   */
  getAlerts(): Array<AlertNotify> {
    return this.notifies.filter((n: Notify) => n.type === TYPES_NOTIFY.ALERT);
  }

  /**
   * Retorna la última notificación de Alerta
   */
  getLastAlert(): ErrorNotify {
    const errors = this.getAlerts();

    return errors[errors.length - 1];
  }

  /**
   * Retorna todas las notificaciones tipo Mensaje
   */
  getMessages(): Array<MessageNotify> {
    return this.notifies.filter((n: Notify) => n.type === TYPES_NOTIFY.MESSAGE);
  }

  /**
   * Retorna todas las notificaciones tipo Advertencia
   */
  getWarnings(): Array<WarningNotify> {
    return this.notifies.filter((n: Notify) => n.type === TYPES_NOTIFY.WARNING);
  }

  /**
   * Retorna todas las notificaciones tipo Error
   */
  getErrors(): Array<ErrorNotify> {
    return this.notifies.filter((n: Notify) => n.type === TYPES_NOTIFY.ERROR);
  }

  /**
   * Retorna la última notificación de Error
   */
  getLastError(): ErrorNotify {
    const errors = this.getErrors();

    return errors[errors.length - 1];
  }

  /**
   * Agrega un mensaje de advertencia por no tener permiso para realizar alguna acción
   */
  notPermission(): void {
    this.addWarning('You do not have permission to perform this action');
  }

  showPreloader(): void {}

  hidePreloader(): void {}

  consoleLog(message: string): void {
    // console.log(message);
  }

  private add(notify: Notify): void {
    this.notifies.push(notify);
    this.getNotifies().next(this.notifies);
  }
}
