import { CommonFiltersModule } from './common-filters.module';

describe('CommonFiltersModule', () => {
  let commonFiltersModule: CommonFiltersModule;

  beforeEach(() => {
    commonFiltersModule = new CommonFiltersModule();
  });

  it('should create an instance', () => {
    expect(commonFiltersModule).toBeTruthy();
  });
});
