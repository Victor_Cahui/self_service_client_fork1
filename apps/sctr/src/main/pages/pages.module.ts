import { NgModule } from '@angular/core';

import { PageForbiddenComponent } from './page-forbidden/page-forbidden.component';
import { PageInternalServerErrorComponent } from './page-internal-server-error/page-internal-server-error.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

@NgModule({
  imports: [],
  exports: [],
  declarations: [PageNotFoundComponent, PageForbiddenComponent, PageInternalServerErrorComponent],
  providers: []
})
export class PagesModule {}
