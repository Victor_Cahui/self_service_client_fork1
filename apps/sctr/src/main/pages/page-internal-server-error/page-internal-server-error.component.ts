import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sctr-page-internal-server-error',
  templateUrl: './page-internal-server-error.component.html',
  styleUrls: ['./page-internal-server-error.component.css']
})
export class PageInternalServerErrorComponent implements OnInit {
  ngOnInit(): void {}
}
