import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageInternalServerErrorComponent } from './page-internal-server-error.component';

describe('PageInternalServerErrorComponent', () => {
  let component: PageInternalServerErrorComponent;
  let fixture: ComponentFixture<PageInternalServerErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PageInternalServerErrorComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageInternalServerErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
