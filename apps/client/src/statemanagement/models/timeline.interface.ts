import { Content, IItemView } from './general.models';

// Item para vista de asistencias o reembolsos
export interface IRequestItemView {
  registerDate: string;
  assistanceNumber: number;
  policyNumber?: string;
  productName?: string;
  itemList: Array<IItemView>;
  currentState: string;
  isCollapse?: boolean;
  collapse?: boolean;
  steps?: Array<ITimelimeStep>;
  year?: string;
  registerDateObj?: any;
  stateColor?: string;
  updateDate?: string;
  showWorkshops?: boolean;
}

// Timeline Horizontal
export interface ITimelimeStep {
  description: string;
  date: string;
  complete?: boolean;
  tooltip?: ITimelineTooltip;
}

export interface ITimelineTooltip {
  title: string;
  description: string;
  linkText?: string;
  files?: Array<{ fileName: string; base64: string }>;
}

// TimeLine Vertical
export interface ItemTimeline {
  // datos que se envian
  date: string;
  title: string;
  rigth?: boolean;
  estimated?: boolean;
  finish?: boolean;
  status: string;
  content?: Content;
  card?: string;
  footer?: string;
  // usados por el componente
  day?: string;
  month?: string;
  year?: string;
}

export interface IOutputEvent {
  card: string;
  data: any;
}
