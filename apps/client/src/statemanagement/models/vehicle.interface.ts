import { IAddress, IClientCorrespondenceAddressRequest } from './client.models';
import { GeneralRequest } from './general.models';
import { ICardPaymentRequest } from './payment.models';

export interface IClientVehicleRequest extends IClientCorrespondenceAddressRequest {
  placa: string;
}

export interface IVehicleRequest {
  codigoApp: string;
  numeroPoliza: string;
  chasis: string;
  nroMotor: string;
}

export interface IClientVehicleUpdateRequest {
  numeroChasis: string;
  numeroMotor: string;
  nuevaPlaca: string;
}

export interface IClientVehicleUpdateResponse {
  nuevaPlaca: string;
}

// Datos de Vehiculo para registro y cotizacion
export interface IClientVehicleDetail {
  placa: string;
  placaEnTramite: boolean;
  marca: string;
  modelo: string;
  anio: number;
  numeroChasis: string;
  numeroMotor: string;
  valorSugerido: number;
  monedaValorSugerido?: string;
  codigoMonedaValorSugerido: number;
  color: string;
  tipoVehiculo: string;
  uso: string;
  title?: string;
  cantidadAccesorios?: number;
  accesorios: Array<IVehicleAccesory>;
  es0Km?: boolean;
  requiereGPS?: boolean;
  ciudad?: string;
  ubigeoId?: number;
  codigoModelo?: number;
  codigoMarca?: number;
  codigoTipoVehiculo?: number;
  codigoUso?: number;
  codigoCiudad?: number;
  codigoColor?: number;
  valorMinimo?: number;
  valorMaximo?: number;
  flagEstadoPoliza?: string;
  fechaVigencia?: string;
  requiereInspeccion?: boolean;
  puedeAutoInspeccionar?: boolean;
}

export interface IVehicleAccesory {
  accesorioId?: number;
  esAutogestionable?: boolean;
  tipoAccesorioId: number;
  tipoAccesorioDescripcion?: string;
  modelo: string;
  monto: string;
}

export interface IVehicleAccesoryDropdown extends IVehicleAccesory {
  descripcion?: string;
}

// Lista de Vehiculos
export interface IVehiclesListRequest {
  codigoApp: string;
  numeroPoliza: string;
}

export interface IVehicleUpdateRequest {
  codigoApp: string;
  numeroPoliza: string;
}

// Accesorios
export interface IVehicleAccesoryType {
  tipoAccesorioId: number;
  tipoAccesorioDescripcion: string;
  minimo: number;
  maximo: number;
}

export interface IVehicleAccesoryRequestPost {
  codigoApp: string;
  numeroPoliza: string;
  chasis: string;
  nroMotor: string;
}

export interface IVehicleAccesoryRequestPut extends IVehicleAccesoryRequestPost {
  accesorioId: string;
}

export interface IVehicleAssistsResponse {
  enProceso: Array<IVehicleAssistsProcessResponse>;
  historial: Array<IVehicleAssistsHistoryResponse>;
}

export interface IVehicleAssistsItemResponse {
  codigoEstado: string;
  datosVehiculo: string;
  estadoAsistencia: string;
  fechaReporteAccidente: string;
  nombreProcurador: string;
  nombreProducto: string;
  numeroAsistencia: number;
  numeroPoliza: string;
  mostrarTalleresPreferentes: boolean;
}

export interface IVehicleAssistsProcessResponse extends IVehicleAssistsItemResponse {
  estados?: Array<IVehicleAssistsItemStateResponse>;
}

export interface IVehicleAssistsHistoryResponse extends IVehicleAssistsItemResponse {
  ultimaFechaActualizacion: string;
}

export interface IVehicleAssistsItemStateResponse {
  codigoEstado: string;
  fechaEstado: string;
  nombreEstado: string;
  tooltip: IVehicleAssistsItemTooltipResponse;
}

export interface IVehicleAssistsItemTooltipResponse {
  textoLinkDescargable: string;
  textoTooltip: string;
  tipoTooltip: number;
  descargables: Array<{ base64: string; nombreDocumento: string }>;
}

// Cotización - Datos de Vehiculo
export interface IVehicleQuoteRequest {
  codigoApp: string;
  marca?: number;
  modelo?: number;
  anio?: number;
  tipoVehiculo?: number;
  es0KM?: boolean;
  usoVehiculo?: number;
}

// Marca
export interface IVehicleBrandsResponse {
  identificadorMarca: number;
  nombreMarca: string;
}

// Modelo
export interface IVehicleModelsResponse {
  identificadorModelo: number;
  nombreModelo: string;
}

// Tipo
export interface IVehicleTypeResponse {
  identificadorTipoVehiculo: number;
  nombreTipoVehiculo: string;
}

// Uso
export interface IVehicleUseResponse {
  identificadorUsoVehiculo: number;
  nombreUsoVehiculo: string;
}

// Valor Sugerido
export interface IVehicleValuesResponse {
  codigoMoneda: number;
  montoValorSugerido: number;
  montoValorMinimo: number;
  montoValorMaximo: number;
}

// Ciudad
export interface IVehicleCityResponse {
  identificadorCiudad: number;
  nombreCiudad: string;
  ubigeoId?: number;
}

// Requiere GPS
export interface IVerifyGPSRequiredResponse {
  requiereGPS: boolean;
}

// Placa existente
export interface IVerifyPlateRequest {
  codigoApp: string;
  tipoDocumento: string;
  documento: string;
  placa: string;
}

export interface IVerifyPlateResponse {
  flagEstadoPoliza?: string;
  fechaVigencia?: string;
  placa: string;
  placaEnTramite: boolean;
  identificadorMarca: number;
  nombreMarca: string;
  identificadorModelo: number;
  nombreModelo: string;
  anioFabricacion: number;
  identificadorTipoVehiculo: number;
  nombreTipoVehiculo: string;
  codigoMoneda: number;
  montoValorSugerido: number;
  montoValorMinimo: number;
  montoValorMaximo: number;
  identificadorUsoVehiculo: number;
  nombreUsoVehiculo: string;
  identificadorCiudad?: number;
  nombreCiudad?: string;
  requiereGPS: boolean;
}

// Cotizacion Vehiculo
export interface ICompareVehicleRequest {
  marcaId: number;
  modeloId: number;
  anio: number;
  tipoVehiculoId: number;
  es0KM: boolean;
  valorSugerido: number;
  codigoMoneda: number;
  usoVehiculoId: number;
  ciudadId: number;
  tienePlaca: boolean;
  placa: string;
}

// Datos de Vehículo en Cotizacion y Emisión de SOAT
export interface IVehicleQuoteSoat {
  placa: string;
  tipoVehiculoId: number;
  tipoVehiculoDescripcion: string;
  marcaId: number;
  marcaDescripcion: string;
  modeloId: number;
  modeloDescripcion: string;
  cantidadAsientos: number;
  tipoUsoId?: number;
  tipoUsoDescripcion?: string;
  anioFabricacion: number;
  serialVin: string;
}

export interface IWorkshopPDFResponse {
  talleresLimaURL: string;
  talleresProvinciaURL: string;
}

export interface IGetColorsRequest {
  codigoApp: string;
}

export interface ISearchChassisMotorRequest extends IGetColorsRequest {
  placa: string;
}

export interface ISearchChassisMotorResponse {
  numChasis: string;
  numMotor: string;
}

export interface IVerifyPolicyChassisMotorRequest extends ISearchChassisMotorRequest {
  chasis: string;
  motor: string;
}

export interface IVerifyPolicyChassisMotorResponse {
  esValidoChasis: boolean;
  esValidoMotor: boolean;
  fechaVigencia: string;
  puedeComprar: boolean;
}

export interface IGetColorsResponse {
  identificadorColorVehiculo: number;
  nombreTipoColor: string;
}

export interface IVerifyInspectionRequest {
  codigoApp: string;
  codigoMarca: number;
  codigoModelo: number;
  anio: number;
  numeroChasis: string;
  numeroMotor: string;
  codigoTipoVehiculo: number;
  codigoMonedaValorSugerido: number;
  sumaAsegurada: number;
  accesorios: number;
  esCeroKm: boolean;
  codigoModalidad: string;
  numeroPlaca: string;
}

export interface IVerifyInsprectionResponse {
  necesitaInspeccion: boolean;
  puedeAutoInspeccionar: boolean;
}

export interface IVehicleBuyRequest {
  numeroCotizacion: string;
  fechaInicioVigencia: string;
  codigoCompania: number;
  codigoRamo: number;
  codigoModalidad: string;
  numeroPlaca: string;
  tipoVehiculo: number;
  codigoMarca: number;
  codigoModelo: number;
  anioFabricacion: number;
  tipoUsoVehiculo: number;
  sumaAsegurada: number;
  esCeroKm: boolean;
  ciudadId: number;
  tienePlaca: boolean;
  codigoMoneda: number;
  numeroCuota: number;
  codigoFraccionamiento: string;
  anios: number;
}

export interface IInspectionConditionsRequest {
  codigoApp: string;
  placa: string;
  numeroChasis: string;
  numeroMotor: string;
}

export interface IInspection {
  selfInspection: boolean;
  conditionsAgreement: boolean;
  previousStep?: number;
  data?: IInspectionData;
}

export interface IInspectionData extends IAddress {
  date: string;
  time: string;
  anotherPerson?: boolean;
  contactName?: string;
  contactPhone?: string;
}

export interface IEmitPaymentBody {
  poliza: {
    codigoCuota: number;
    codigoFraccionamiento: string;
    codigoMoneda?: number;
    fechaFinVigencia: string;
    fechaInicioVigencia: string;
    montoMapfreDolaresRestantes?: number;
    montoMapfreDolaresUsado?: number;
    numeroCotizacion?: string;
    usoMapfreDolares?: boolean;
    aprobacionPolizaElectronica?: boolean;
  };
  producto: {
    codigoCompania: number;
    codigoModalidad: string;
    codigoRamo: number;
  };
  vehiculo: {
    anioFabricacion: number;
    codigoColor: number;
    codigoMarca: number;
    codigoModelo: number;
    esCeroKm: boolean;
    numeroChasis: string;
    numeroMotor: string;
    numeroPlaca: string;
    sumaAsegurada: number;
    tipoUsoVehiculo: number;
    tipoVehiculo: number;
  };
  pago?: {
    monto?: number;
    tipoMoneda?: number;
    tipoPago?: string;
  };
  tarjeta?: ICardPaymentRequest;
}

export interface IEmitPaymentResponse {
  numeroPoliza: string;
  numeroRecibo: string;
}

export interface IInspectionRequestBody {
  datosContratante: {
    apellidoMaterno: string;
    apellidoPaterno: string;
    codigoDepartamento: number;
    codigoDistrito: number;
    codigoProvincia: number;
    correo: string;
    direccion: string;
    nombre: string;
    referencia: string;
    telefonoContratante: string;
  };
  datosVehiculo: {
    anio: number;
    cantidadAccesorios: number;
    codigoMarca: number;
    codigoModelo: number;
    codigoMonedaValorSugerido: number;
    codigoTipoVehiculo: number;
    descripcionMarca: string;
    descripcionModelo: string;
    descripcionTipoVehiculo: string;
    placa: string;
    valorSugerido: number;
  };
  datosInspeccion: {
    esInspeccionPresencial: boolean;
    codigoDepartamento?: number;
    codigoDistrito?: number;
    codigoProvincia?: number;
    direccion?: string;
    esDireccionCorrespondencia?: boolean;
    fechaInspeccionPresencial?: string;
    horaInspeccionPresencial?: string;
    nombreContacto?: string;
    otraPersonaContacto?: boolean;
    referencia?: string;
    telefonoContacto?: string;
  };
}

export interface IInspectionRequestResponse {
  numRiesgo: number;
  numSolicitud: number;
}

// Asistencias por Accidente o Robo
export interface IAssistanceDetailRequest {
  codigoApp: string;
  numeroAsistencia: number;
}

export interface IVehicleAssistsResponse {
  enProceso: Array<IVehicleAssistsProcessResponse>;
  historial: Array<IVehicleAssistsHistoryResponse>;
}

// Calificar taller
export interface IRateWorkshopRequestBody extends GeneralRequest {
  numeroAsistencia: string;
  identificadorTaller: number;
  calificacion: number;
}

export interface IVehicleGetUrlContionsBuy extends GeneralRequest {
  codigoModalidad: string;
  // tslint:disable-next-line: max-file-line-count
}
