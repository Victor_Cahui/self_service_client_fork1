import { FileItem } from '@mx/core/ui/lib/directives/ng-drop/file-item.class';
import { IClientAddressResponse } from './client.models';
import { ICoverageItem, ICoverageResponse } from './coverage.interface';

export interface IListAddress extends IClientAddressResponse {
  id: number;
  numeroPoliza: string;
  numeroSuplemento: number;
  numeroRiesgo: number;
}

export interface IListIncidentTypesRequest {
  codigoApp: string;
}

export interface IListHoursResponse {
  identificadorRango: number;
  descripcionRango: string;
}

export interface IIncidentReason {
  identificadorMotivo: number;
  descripcionMotivo: string;
}

export interface IListIncidentTypesResponse {
  identificadorIncidente: number;
  descripcionIncidente: string;
  motivos: Array<IIncidentReason>;
  llaveIcono?: string;
}

export interface IDeclareSinister {
  address: IListAddress;
  date: Date;
  hour: IListHoursResponse;
  hourId: number;
  idIncident: number;
  idReason: number;
  files: Array<FileItem>;
}

export interface IRegisterIncidentBody {
  numeroPoliza: string;
  numeroSuplemento: number;
  numeroRiesgo: number;
  codRamo: number;
  direccion: string;
  fecha: string;
  identificadorRango: number;
  rangoHorario: string;
  identificadorIncidente: number;
  identificadorMotivo: number;
  descripcionIncidente: string;
  fotosIncidente: File;
}

export interface IRegisterIncidentResponse {
  numeroAsistencia: number;
}

// Coberturas
export interface IHomeCoverageRequest {
  codigoApp: string;
  numeroPoliza: string;
  numeroSuplemento: number;
  numeroRiesgo: number;
  codRamo: number;
}

export interface IHomeCoverageResponse {
  titulo: string;
  descripcion: string;
  codigoGrupo: string;
  coberturas: Array<ICoverageResponse>;
  sublimite: Array<IHomeCoverageSublimitResponse>;
}

export interface IHomeDeductiblesResponse {
  deducible: string;
  nombreCobertura: string;
}

export interface IHomeCoverageSublimitResponse {
  grupoCobertura: string;
  indemnizable: string;
  sublimite: string;
}

export interface IHomeCoveragesListGroup {
  title: string;
  description: string;
  coverages: Array<ICoverageItem>;
  sublimits: Array<IHomeCoverageSublimitResponse>;
}

// Verificar uso
export interface IHomeCoverageDescriptionResponse {
  descripcion: string;
  grupoCobertura: string;
}
