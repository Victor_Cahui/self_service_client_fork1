export interface AuthInterface {
  client_id: string;
  grant_type: string;
  username: string;
  password: string;
  refresh_token?: string;
  groupTypeId?: string;
  osa?: number;
  scope?: string;
}

export interface IAuthResponse {
  access_token: string;
  token_type: string;
  expires_in: number;
  refresh_token: string;
}

export interface IBaseResponseSecurity<T> {
  message?: string;
  data?: T;
  operationCode: number;
  typeMessage: string;
}

export interface IDataUserResponse {
  num_Persona: number;
  cod_Usuario: string;
  txt_Email: string;
  nom_Persona1: string;
  ape_Paterno: string;
  ape_Materno: string;
  email_Crm: string;
  num_TipoGrupo: number;
}

export interface IUserGroupType {
  id?: number;
  peopleId?: number;
  groupType?: number;
  userType?: number;
  userNamePrefix?: string;
  activeDirectory?: string;
  groupTypeName?: string;
  groupTypeIcon?: string;
  codigoAgente?: string;
  documentType?: string;
  documentNumber?: string;
  name?: string;
}

export interface IUsersGroupTypeResponse extends IBaseResponseSecurity<Array<IUserGroupType>> {}

export interface AdminUserResponse extends IBaseResponseSecurity<IDataUserResponse> {}

export interface IUserClaimResponse {
  userType?: string;
  userSubType?: string;
  loginUserName?: string;
  userName?: string;
  clientProfile?: string;
  userProfile?: string;
  agentName?: string;
  agentID?: string;
  documentNumber?: string;
  rucNumber?: string;
  userEmail?: string;
  tokenMapfre?: string;
  roleCode?: string;
  roleName?: string;
  officeCode?: string;
  urlRedirect?: string;
  flagUserByPass?: string;
  iCodeMx?: string;
  documentType?: string;
  perfilId?: string;
  gestorId?: string;
  gestorName?: string;
  personId?: string;
  userId?: string;
  companyId?: string;
  companyName?: string;
  userAdminRegular?: string;
  isAutoService?: string;
  iss?: string;
  aud?: string;
  exp?: string;
  nbf?: string;
}

export interface IChangePasswordRequest {
  token: string;
  password: string;
  applicationCode?: string;
}

export interface IUpdatePasswordRequest {
  documentType: string;
  documentNumber: string;
  oldPassword: string;
  newPassword: string;
}

export interface IChangePasswordResponse {
  message: string;
  data: boolean;
  operationCode: number;
  typeMmessage: string;
}

export interface IChangePasswordToken {
  token: string;
  action: string;
  success?: boolean;
  tryAgain?: boolean;
}

export interface ILogguedUser {
  documentType: string;
  documentNumber: string;
  groupTypeId: string;
  userType: string;
  name: string;
}

export interface ReqBodyTokenDummy {
  connectionIdentifier?: string;
  destiny: string;
  origin?: string;
  urlRedirection: string;
}

export interface IResBodyTokenDummy extends ReqBodyTokenDummy {
  tokenDummy: string;
  token?: string;
  userType?: number;
}

export interface ResBodyTokenDummy extends IBaseResponseSecurity<IResBodyTokenDummy> {}
