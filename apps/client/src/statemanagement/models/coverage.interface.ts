export interface ICoverageItem {
  title: string;
  icon: string;
  description?: string;
  question?: string;
  questionBool?: boolean;
  routeKey?: string;
  iconKey?: string;
  key?: string;
  visible?: boolean;
}

export interface ICoverageIcons {
  key: string;
  name: string;
  icon: string;
}

export interface ICoverageActions {
  key: string;
  route: string;
}

// Respuesta del Servicio
export interface ICoverageResponseItem {
  llave: string;
  tituloInfo: string;
  descripcionInfo: string;
  accion: boolean;
  accionLlave: string;
  accionTexto: string;
  nombreGrupo: string;
}

export interface ICoverageResponse {
  vigencia: number;
  coberturas: Array<ICoverageResponseItem>;
}
