import { IPaymentQuoteCard } from '@mx/statemanagement/models/payment.models';
export interface GeneralSearchRequest {
  criterio?: string;
  orden?: string;
  registros?: number;
  pagina?: number;
}

export interface GeneralRequest {
  codigoApp?: string;
}

// Modulo de Seguridad
export interface GeneralRequestEn {
  applicationCode?: string;
  documentType?: string;
  documentNumber?: string;
}

// Modulo de reportar daños hogar
export interface IlistIncidentHoursRequest {
  codigoApp?: string;
  documento?: string;
  fecha?: string;
  tipoDocumento?: string;
}

// Menu principal
export interface IMenuItem {
  policies?: Array<string>;
  name: string;
  icon?: string;
  route?: string;
  visible?: boolean;
  children?: Array<IMenuItem>;
  collapse?: boolean;
  hideIcon?: boolean;
  active?: boolean;
  badge?: string;
  hideNewIcon?: boolean;
}

// Icon de Action Links
export interface IClientAction {
  code: string;
  circleBgc: string;
  icon: string;
  visible?: boolean;
  route?: string;
  external?: boolean;
}

// What do you to do
export interface IButtonsLink {
  description: string;
  policyTypes: Array<string>;
  route?: string; // Una misma ruta, que puede tener como parametro el tipo de poliza
  routeByType?: Array<string>; // Para manejar rutas diferentes por tipo de poliza
  paramsPolicyType?: boolean; // Para añadir tipo de poliza a la ruta
  openModal?: boolean; // Para abrir modal para seleccionar poliza, luego va a routeByType
  external?: boolean;
  key?: string;
  visible?: boolean;
}

// Contenido de Landings
export interface ItemSectionLanding {
  icon?: string;
  bullet?: string;
  titleItem: string;
  textItem: string;
}

export interface SectionLanding {
  items: Array<ItemSectionLanding>;
  linkedItems: boolean;
  nameSection: string;
  nameSectionBody?: string;
}

export interface ContentLanding {
  title: string;
  sections: Array<SectionLanding>;
}

// Contenido de Planes de Seguros
export interface InsurancePlan {
  title: string;
  subtitle: string;
  content: Array<DescriptionInsurancePlan>;
  price: string;
  conditionPrice?: string;
  textBtnInformation?: string; // Buttons sólo para el plan
  urlInformation?: string;
  textBtnGetQuote?: string;
  urlGetQuote?: string;
}

export interface DescriptionInsurancePlan {
  title?: string; // titulo de la seccion, p.e. Beneficios
  items?: Array<{ text: string }>; // descripción como lista
  text?: string; // descripción como párrafo
  price?: IPaymentInfoInsurancePlam; // cantidad y descripcion del plan anual
}

export interface IPaymentInfoInsurancePlam {
  amount: string;
  desc?: string;
}

export interface ContentInsurancePlans {
  title?: string;
  plans: Array<InsurancePlan>;
  textBtnInformation?: string; // Buttons igual para todos los planes
  urlInformation?: string;
  textBtnGetQuote?: string;
  urlGetQuote?: string;
}

// Contenido ¿Cómo Funciona?
export interface ContentHowtoWork {
  title: string;
  items: Array<string>;
}

// Opciones de Pago
export interface PaymentsOptions {
  title: string;
  url: string;
  images: Array<string>;
}

// FAQs
export interface Faq {
  title: string;
  content: string;
  collapse?: boolean;
  order?: number; // Orden en el que se debe mostrar
  landing?: boolean; // Se muestra o no en landing
}

export interface IParameterApp {
  codigo: string;
  valor: any;
  descripcion: string;
}

// Contenidos
export interface Content {
  title?: string;
  content?: string;
  content2?: string;
  button?: string;
  icon?: string;
  iconBtn?: string;
  warning?: string;
  amountLabel?: string;
  amountText?: string;
  emailLabel?: string;
  emailText?: string;
  dateText?: string;
  dateLabel?: string;
  table?: {
    // encabezado (columns) varias lineas (rows)
    title?: string;
    columns?: Array<string>;
    rows?: Array<any>;
  };
  table2?: Array<{
    // encabezado - detalle
    column: string;
    row: string;
    tooltips?: ITooltips;
  }>;
  tooltips?: ITooltips;
  itemList?: Array<IItemView>;
  action?: {
    // link
    textLink?: string;
    text?: string;
    icon?: string;
    routeLink?: string;
    fileName?: string;
    url?: string;
    base64?: string;
    base64Type?: string;
    fileList?: Array<IFileList>;
    // rating
    itemId?: number;
    rated?: boolean;
    rate?: number;
  };
  // medio de pago
  confirmFormData?: any;
  response?: any;
  quote?: IPaymentQuoteCard;
  isSoat?: any;
}

export interface IFileList {
  identificadorDocumento: number;
  nombreDocumento: string;
  peso: number;
  loading?: boolean;
}

export interface IFileDownloading {
  identificadorDocumento: number;
  loading?: boolean;
}

export interface ITooltips {
  title: string;
  text: string;
  items: Array<string>;
}

export interface ICardTitle {
  title: string;
  icon?: string;
  collapse: boolean;
}

export interface IItemView {
  label?: string;
  text?: string;
  icon?: string;
  iconBullet?: boolean;
  textBold?: boolean;
  other?: string;
}

/**
 * Ruta para imagenes de banner
 * @param sm Obligatorio para dispositivos móviles
 * @param lg Obligatorio para computadoras desktop
 */
export interface IPathImageSize {
  xs?: string;
  sm: string;
  md?: string;
  lg: string;
  xl?: string;
}

export interface IContactedByAgent {
  title: string;
  titleMobile: string;
  phoneNumber: string;
  hoursRange: string;
  hoursRangeList: Array<ISelectList>;
  titleSent?: string;
  messageSent: string;
  titleShortSent?: string;
  email?: string;
  labelHoursRangeWithColons: string;
  labelPhoneNumberWithColons: string;
  labelPhoneNumber: string;
  sendButton: string;
  okButton: string;
}

export interface ISelectList {
  text: string;
  value: string;
  selected?: boolean;
  disabled?: boolean;
}

// Datos para modal en caso de errores
export interface IErrorModal {
  title?: string;
  message?: string;
  suggestion?: string;
  image?: string;
  icon?: string;
  operationCode?: string;
}

export interface ICardFilterOption {
  value: string;
  text: string;
}

export interface IPaginatorBaseResponse {
  totalPaginas: number;
  totalRegistros: number;
}

export interface IPaginatorBaseRequest {
  registros: number;
  pagina: number;
}

export interface IBannerNotification {
  icon?: string;
  title: string;
  items: Array<string>;
  link?: {
    icon?: string;
    label: string;
  };
}

export interface IModalHowWorksSteps {
  icon: string;
  step?: string;
  title?: string;
  text: string;
}

export interface IAlignedText {
  text: string;
  /**
   * @param alignClass Default: 'text-center'
   */
  alignClass?: string;
}

export interface IModalHowWorks {
  link?: string;
  title: string;
  warning?: string;
  body: IAlignedText;
  steps: {
    title?: IAlignedText;
    showStepNumber: boolean;
    items: Array<IModalHowWorksSteps>;
  };
}

export interface IConfigUICardViewItemList {
  hasFilter?: boolean;
  hasShowMore?: boolean;
  routerLinkViewAll?: string;
}

export interface IBannerAccordion {
  title: string;
  body: string;
  endLink?: {
    isExternal: boolean;
    text: string;
    url: string;
  };
}
