import { IItemView } from './general.models';

export interface IServicesListRequest {
  codigoApp: string;
}

export interface IClientServiceDetail {
  llave: string;
  seccion: Array<string>;
  titulo: string;
  nroServiciosSolicitados: number;
  descripcion: string;
  accion: boolean;
  maneraContacto: string;
  onClick?: any;
  icon?: string;
}

export interface IClientSection {
  key: string;
  name: string;
}

// Historial de Servicios
export interface IServicesHistoryRequest {
  tipoPoliza: string;
  numeroPoliza: string;
}

export interface IServicesHistoryResponse {
  enCurso: Array<IServicesItemResponse>;
  historial: Array<IServicesItemResponse>;
}

export interface IServicesItemResponse {
  fechaSolicitud: string;
  codigoTipoSolicitud: string;
  descripcionTipoSolicitud: string;
  identificadorSolicitud: number;
  codigoEstado: string;
  descripcionEstado: string;
  destinoServicio: string;
  codigoMoneda: number;
  costoServicio: number;
  fechaAgendada: string;
  horaAgendada: string;
  empresaProveedora: string;
  chofer: {
    datosVehiculo: string;
    origenServicio: string;
    referenciaOrigen: string;
    latitudOrigen: number;
    longitudOrigen: number;
  };
  medico: {
    esUrgente: boolean;
    paciente: string;
    tipoMedicina: string;
    sintomas: string;
    numeroPoliza: string;
    nombreProducto: string;
  };
  cita: {
    tipoDocumentoUsuario: string;
    documentoUsuario: string;
    clinicaId: string;
    clinicaDescripcion: string;
    numeroPoliza: string;
    tipoDocumentoPaciente: string;
    documentoPaciente: string;
    apellidoPaternoPaciente: string;
    apellidoMaternoPaciente: string;
    nombresPaciente: string;
    especialidadId: number;
    especialidadDescripcion: string;
    horaFin: string;
    doctorId: number;
    nombreCompleto: string;
    estadoCita: string;
    direccion: string;
    distritoDescripcion: string;
    provinciaDescripcion: string;
    departamentoDescripcion: string;
    telefono: string;
    horarioAtencion: string;
    paginaWeb: string;
    diasRestantes: number;
    reagendar: boolean;
    tipoCita: string;
  };
  costoServicioFormat?: string;
  latitudDestino: number;
  longitudDestino: number;
  referenciaDestino: string;
  telefono: string;
}

export interface IServiceHistoryView {
  code: string;
  month: string;
  year: string;
  day: string;
  scheduledTime: string;
  type: string;
  moreInfo: string;
  number: string;
  status: string;
  color: string;
  viewDetail: boolean;
  items: Array<IItemView>;
  payload: IServicesItemResponse;
}

// Chofer de Reemplazo
export interface ISubstituteDriverRequest {
  fechaRecojo: string; // en blanco = servicio urgente
  horaRecojo?: string;
  origen?: {
    puntoPartida: string;
    referenciaPartida: string;
    latitud: number;
    longitud: number;
  };
  destino: {
    puntoLlegada: string;
    referenciaLlegada: string;
    latitud: number;
    longitud: number;
  };
  producto: {
    identificadorProducto: number;
    numeroPoliza: string;
    nombreProducto: string;
    tipoCliente: string;
  };
  pago: {
    codigoMoneda: number;
    precio: number;
    precioReal?: number;
    precioDescuento?: number;
    esGratis: boolean;
  };
  telefono: string;
}

export interface IHomeDoctorRequest extends ISubstituteDriverRequest {
  esUrgente: boolean;
  sintomas: string;
  beneficiario: number;
  especialidad: number;
}

export interface ISubstituteDriverResponse {
  numeroSolicitud: string;
}

export interface IAllowedAreasResponse {
  identificadorZona: number;
  nombreZona: string;
  coordenadas: Array<{
    latitud: number;
    longitud: number;
  }>;
}
export interface IVehicleSubstituteDriverResponse {
  beneficio?: IBeneficiesResponse; // dev swagger
  beneficios?: IBeneficiesResponse; // servicios mapfre
  precio: {
    codigoMoneda: number;
    precio: number;
    monedaSimbolo?: string;
  };
  producto: {
    identificadorProducto: number;
    numeroPoliza: string;
    nombreProducto: string;
    esTarjetaPlatino?: boolean;
  };
}

export interface IBeneficiesResponse {
  esIlimitado: boolean;
  ocultarDisponibles: boolean;
  restantesDisponibles: number;
  tooltip?: Array<string>;
}

export interface IHomeDoctorResponse extends IVehicleSubstituteDriverResponse {
  beneficiarios: Array<{
    identificador: number;
    nombre: string;
  }>;
  especialidades: Array<{
    identificador: number;
    nombre: string;
  }>;
}

export interface ISummaryView {
  doctor?: string;
  driver?: string;
  vehicle?: string;
  policy?: string;
  policyNumber?: string;
  specialty?: string;
  patient?: string;
  symptom?: string;
  date?: string;
  from?: string;
  referenceFrom?: string;
  to?: string;
  referenceTo?: string;
  phone?: string;
  price?: string;
  icon?: string;
  title?: string;
  text?: string;
  warning?: string;
  itemsDetail?: Array<IItemView>;
  itemsSchedule?: Array<IItemView>;
}
