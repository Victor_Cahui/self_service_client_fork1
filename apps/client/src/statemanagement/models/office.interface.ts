export interface IOffice {
  oficinaId: number;
  nombre: string;
  imagenPortraitUrl: string;
  imagenLandscapeUrl: string;
  latitud: number;
  longitud: number;
  direccion: string;
  distritoId: string;
  distritoDescripcion: string;
  provinciaId: string;
  provinciaDescripcion: string;
  departamentoId: string;
  departamentoDescripcion: string;
  distancia: number;
  telefono: string;
  horarioAtencion: string;
  servicios: Array<{
    codigo: string;
    descripcion: string;
  }>;
  puedeAgendarCita: boolean;
}

export interface IOfficeListRequest {
  codigoApp: string;
  latitud?: number;
  longitud?: number;
}
