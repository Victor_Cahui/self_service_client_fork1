import {
  COVERAGE_STATUS,
  POLICY_QUOTA_TYPE,
  POLICY_STATUS,
  SCTR_PERIOD_STATUS,
  SITUATION_TYPE,
  SITUATION_TYPE_SCTR_PERIODS,
  TRAFFIC_LIGHT_COLOR
} from '@mx/components/shared/utils/policy';
import { QUERY_SOURCE } from '@mx/settings/constants/coverage-values';
import { GeneralRequest, GeneralSearchRequest } from './general.models';
import { IAssociatedReceipt, IPaymentQuoteCard } from './payment.models';
import { IClientVehicleDetail } from './vehicle.interface';

export interface IPolicyType {
  code: string;
  icon: string;
  dropDownTitle: string;
  dropDownSubTitle?: string;
  path: string;
}

export interface IPoliciesByClientRequest extends GeneralSearchRequest {
  activarSoatVencidos?: boolean;
  codigoApp: string;
  estadoPoliza?: string;
  identificadorClinica?: string | number;
  numeroPoliza?: string;
  tipoPoliza?: string;
}

export interface IBeneficiary {
  tipoDocumento?: string;
  numeroDocumento?: string;
  nombreBeneficiario: string;
  tipoBeneficiario: string;
}

export interface ICoveragePpfm {
  codigoCobertura?: string;
  descripcion?: string;
  sumaAsegurada?: number;
}

export interface IPoliciesByClientResponse {
  acciones?: any;
  beneficiarios?: Array<IBeneficiary>;
  coberturasPpfm?: Array<ICoveragePpfm>;
  codCia?: number;
  codRamo?: number;
  codigoMoneda?: number;
  cuotas?: number;
  descripcionPoliza?: string;
  datosPago?: any;
  esContratante: boolean;
  estadoPagoRenovacion?: string;
  fechaFin?: string;
  fechaInicio?: string;
  montoCuota?: number;
  montoTotal?: number;
  nombreContratante?: string;
  numeroCuota?: number;
  numeroPoliza?: string;
  numRecibo?: string;
  numSpto: number;
  prima?: number;
  tipoPoliza?: string;
  expiredPolicy?: boolean;
  sumaAsegurada?: number;
  codigoMonedaSumaAsegurada?: number;
  tienePagoPendiente?: boolean;
  tieneHojaAnexa?: boolean;
  estado?: POLICY_STATUS;
  estadoPoliza?: string;
  estadoCobertura?: COVERAGE_STATUS;
  tipoPagoId?: number;
  tipoPagoDescripcion?: string;
  tieneRenovacionAutomatica?: boolean;
  tipoRenovacionId?: number;
  tipoRenovacionDescripcion?: string;
  planEps?: string;
  comparacionPoliza?: {
    cambioModalidadEnProceso?: boolean;
    puedeCambiarModalidad?: boolean;
    fechaSiguienteCambioModalidad?: string;
  };
  semaforo?: string;
  iconoRamo?: string;
  esPolizaVip: boolean;
  esBeneficioAdicional?: boolean;
  descripcionFraccionamiento: string;
  tipoDocumentoPago?: string;
  totalAfiliados?: number;
  codCliente?: string;
}

export interface IAgent {
  nombre: string;
  correo: string;
  telefono: string;
}

export interface ICardPoliciesView {
  amountFee?: string;
  beneficiaries?: Array<IBeneficiary>;
  billNumber?: string;
  bulletColor?: string;
  coverageStatus: string;
  debitRenovation?: string;
  endDate?: string;
  estado?: string;
  expired: boolean;
  expiredDate?: string;
  hiddenViewDetail?: boolean;
  insuranceCost?: string;
  isCurrent?: boolean;
  isNew?: boolean;
  isSoat?: boolean;
  isVitalicia?: boolean;
  isExpiredEps?: boolean;
  labelEndDate?: string;
  planEps?: string;
  policy?: string;
  policyNumber?: string;
  policyNumberHeath?: string;
  policyNumberPension?: string;
  policyStatus?: POLICY_STATUS;
  policyType?: string;
  quotaNumber?: number;
  quota?: any;
  quotas?: number;
  renewButton?: boolean;
  renewDate?: string;
  response: IPoliciesByClientResponse;
  semaforo?: string;
  showIconPdf?: boolean;
  startDate?: string;
  totalAmount?: string;
  txtEndDate?: string;
  warning?: boolean;
  iconoRamo?: string;
  isContractor?: boolean;
  contractorName: string;
  isEPS: boolean;
  isSCTR: boolean;
  isSctrDoubleEmission: boolean;
  isEPSContractor: boolean;
  isVIP: boolean;
  isVisibleCoverage: boolean;
  affiliates?: string;
  fractionationDesc: string;
  constanciesLabel: string;
  isPPFM?: boolean;
  customIcon?: string;
  compare?: {
    modalityInProgress?: boolean;
    changeModality?: boolean;
    nextChangeFrom?: string;
  };
}

export interface IPoliciesAndProductsByClientRequest {
  codigoApp: string;
  estadoPoliza?: string;
}
export interface IPoliciesAndProductsByClientResponse {
  descripcionPoliza: string;
  tipoPoliza: string;
  productos: [
    {
      codProducto: string;
      descripcionProducto: string;
    }
  ];
}
export interface IPolicySearchByNumberRequest {
  tipoDocumento: string;
  documento: string;
  numeroPoliza: string;
}

export interface IPolicyHousesRequest {
  codigoApp: string;
  numeroPoliza: string;
}

export interface IPolicyHousesResponse {
  hogarId: number;
  anioConstruccion: string;
  departamentoId: string;
  departamentoDescripcion: string;
  provinciaId: string;
  provinciaDescripcion: string;
  distritoId: string;
  distritoDescripcion: string;
  direccion: string;
  tipoHogarId: number;
  tipoHogarDescripcion: string;
  codigoMonedaContenido: number;
  valorContenido: number;
  codigoMonedaEdificacion: number;
  valorEdificacion: number;
  numeroSuplemento?: number;
  numeroRiesgo?: number;
  codRamo?: number;
  descripcionProducto?: string;
}

export interface IItemHousePolicyView {
  yearContruction: string;
  department: string;
  province: string;
  district: string;
  address: string;
  homeType: string;
  contentValue: string;
  buildingValue: string;
}

export interface IPolicyInsuredRequest {
  codigoApp: string;
  numeroPoliza: string;
}

export interface IPolicyFeeRequest {
  codigoApp: string;
  numeroPoliza?: string;
  codCia: string;
  numSpto: number;
}

export interface IPolicyFeeResponse {
  codCia: number;
  codRamo: number;
  codModalidad: number;
  descripcionPoliza: string;
  tipoPoliza: string;
  numeroPoliza: string;
  numRecibo: string;
  cuota: string;
  pagoActivo: boolean;
  tipoCuota: POLICY_QUOTA_TYPE;
  codigoMoneda: number;
  montoTotal: number;
  estado: string;
  fechaDebitoRenovacion: string;
  semaforo: TRAFFIC_LIGHT_COLOR;
  estadoCobertura: COVERAGE_STATUS;
  fechaExpiracion: string;
  fechaPago: string;
  tieneCobertura: boolean;
  tiposPagosId: string;
  tiposPagosLlave: string;
  tiposPagosNombre?: string;
  amountFee?: string;
  expirationDate?: string;
  paymentDate?: string;
  showQuota?: boolean;
  bulletColor?: string;
  policyType?: string;
  policyStatusLabel?: string;
  showCoverageStatus?: boolean;
  tipoDocumentoPago: string;
  numeroAviso: string;
  listaRecibosAsociados: Array<IAssociatedReceipt>;
  tipoSituacion: SITUATION_TYPE;
  isNotice?: boolean;
  paymentFee?: IPaymentQuoteCard;
}

export interface IPolicyInsuredEditDataRequest {
  codigoApp: string;
  numeroPoliza: string;
  tipoDocumento: string;
  documento: string;
  tipoDocumentoModificado?: string;
  numeroDocumentoModificado?: string;
  apellidoPaterno?: string;
  apellidoMaterno?: string;
  nombre?: string;
  fechaNacimiento?: string;
  relacionId?: number;
  telefonoMovil?: string;
  correoElectronico?: string;
  fotosDocumento?: File;
}

export interface IPolicyInsuredResponse {
  tipoDocumento: string;
  documento: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  nombre: string;
  sexo: string;
  relacionId: number;
  relacionDescripcion: string;
  tipoAseguradoId: number;
  tipoAseguradoDescripcion: string;
  beneficio: string;
  sueldo: string;
  fechaNacimiento: string;
  telefono: string;
  correoElectronico: string;
  canEdit: boolean;
  isVisibleDependent: boolean;
  isEdit: boolean;
  showLoading: boolean;
  frmNamePhone: string;
  frmNameEmail: string;
}

export interface IPolicyBasicInfo {
  policyType: string;
  policyName: string;
  policyNumber: string;
  insuredNumber?: number;
  sumAssured: number;
  startDateValidity: string;
  endValidityDate: string;
  moneyCode: string;
  planEps?: string;
  frequencyStatement?: string;
  activityRisk?: Array<IActivityRisk>;
  coveragesPpfm?: Array<ICoveragePpfm>;
}

export interface IActivityRisk {
  actRiesgoId: number;
  actRiesgoDescripcion: string;
}

export interface IPaymentPolicy {
  insuranceCost: number;
  quotas?: number;
  quotaNumber?: number;
  amountDue: number;
  paymentType: string;
  paymentTypeId?: number;
  automaticRenewal: boolean;
  renewalTypeId?: number;
  renewalType?: string;
  moneyCode: string;
  hasPendingPayment: boolean;
  policyType: string;
  policyNumber?: string;
  frequencyStatement?: string;
  numberInsured?: number;
  amountMax?: number;
  fractionationDesc?: string;
}
export interface ICoveragesPolicyRequest {
  codigoApp: string;
  numeroPoliza?: string;
  origenConsulta?: QUERY_SOURCE;
}

export interface ICoveragesPolicy {
  llave: string;
  tituloInfo: string;
  descripcionInfo: string;
  accion: boolean;
  accionTexto: string;
  accionLlave: string;
  nombreGrupo: string;
}

export interface ISctrByClientRequest extends GeneralSearchRequest {
  codigoApp: string;
  estadoPoliza?: string;
  numeroPoliza?: string;
}

export interface ISctrByClientResponse {
  asegurados: Array<IInsuredSctrByClientResponse>;
  codCia: number;
  codRamo: number;
  codigoMoneda: number;
  descripcionPoliza: string;
  fechaFin: string;
  fechaInicio: string;
  nombreContratante: string;
  numeroPoliza: string;
  tipoPoliza: string;
  sumaAsegurada: number;
  codigoMonedaSumaAsegurada: number;
  prima: number;
  montoPlanilla: number;
  montoTopado?: number;
  aseguradosDeclaracion: number;
  frecuenciaDeclaracion: string;
  actividadesRiesgo?: Array<IActivityRisk>;
  numeroConstancias?: number;
  tieneHojaAnexa?: boolean;
  tipoPagoId?: number;
  tipoPagoDescripcion?: string;
  estadoCobertura?: COVERAGE_STATUS;
  semaforo?: string;
  numSpto?: number;
  descripcionFraccionamiento?: string;
  colectivoAsegurado?: string;
  esContratante: boolean;
}

export interface IInsuredSctrByClientResponse {
  nombreAsegurado: string;
}

export interface IItemPolicySctr {
  insured: Array<{
    fullName: string;
  }>;
  moneyCode: string;
  insuranceCost: number;
  policy: string;
  expired: boolean;
  warning: boolean;
  endDate: string;
  startDate: string;
  policyNumber: string;
  policyNumberHeath: string;
  policyNumberPension: string;
  policyNumberTitle: string;
  policyNumberLabel: string;
  policyType: string;
  response: ISctrByClientResponse;
  constancyCount: number;
  coverageStatus?: string;
  bulletColor?: string;
  isContractor: boolean;
  contractorName: string;
  isVisibleCoverage?: boolean;
  isSctrDoubleEmission: boolean;
}

export interface IDeductiblePolicy {
  vehiculo: IClientVehicleDetail;
  danioParcial: Array<{
    descripcion: string;
    valor: string;
  }>;
  danioTotal: {
    monto: number;
    codigoMoneda: number;
  };
}

export interface IInsuredSctrByPolcyResponse {
  nombreAsegurado: string;
  tipoDocumento: string;
  numeroDocumento: string;
  fechaNacimiento: string;
  sueldo: number;
  codigoMonedaSueldo: number;
  fechaInicioVigencia: string;
  fechaFinVigencia: string;
  tieneCobertura: boolean;
}

export interface IBodySendPolicyTransfer {
  tipoDocumento: string;
  documento: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  nombre: string;
  telefono: string;
  email: string;
}

export interface IPolicyTransferRequest {
  codigoApp: string;
  numeroPoliza: string;
}

export interface IFoilAttachedRequest {
  codigoApp: string;
  numeroPoliza: string;
}

export interface IFoilAttachedResponse {
  hojaAnexa: string;
}

export interface ICoveragesPolicyVehicleRequest {
  codigoApp: string;
  numeroPoliza: string;
  chasis: string;
  nroMotor: string;
}

export interface IExclusionsByInsuredRequest {
  codigoApp: string;
  numeroPoliza: string;
  tipoDocumentoAsegurado: string;
  documentoAsegurado: string;
}

export interface IExclusionsResponse {
  exclusion: string;
  observacion: string;
}

export interface IObservationsResponse {
  observaciones: string;
}

// Listado de Clinicas en Coberturas de Salud
export interface ICareNetworksRequest {
  codigoApp: string;
  numeroPoliza: string;
  tipoCobertura: string;
}

export interface ICareNetworksResponse {
  nombreBeneficio: string;
  redes: Array<{
    collapse: boolean;
    tipoUbicacion: string;
    nombreRed: string;
    deducible: string;
    codigoMonedaCopago?: number;
    codigoTipoDeducible?: string;
    montoCopago?: number;
    textoDeducibleCorto?: string;
    textoDeducibleLargo?: string;
    cobertura: string;
    total: number;
    clinicas: Array<{
      nombreClinica: string;
    }>;
    textDeducible?: string;
    textMonto?: string;
  }>;
}

// Datos de contratante
export interface IContractingRequest {
  codigoApp: string;
  numeroContrato: string;
}
export interface IContractingResponse {
  razonSocial: string;
  nombreComercial: string;
  ruc: string;
  telefono: string;
  direccion: string;
}

export interface IContractingInfo {
  fullName?: string;
  typeDocument?: string;
  document?: string;
  phone?: string;
  address?: string;
  comercialName: string;
}

// Contratante para SOAT
export interface IContractingSoat {
  tipoDocumento: string;
  documento: string;
  tieneControlTecnico: boolean;
  tieneDatosCompletos: boolean;
  codigoNacionalidad: string;
  descripcionNacionalidad: string;
  apellidoPaterno?: string;
  apellidoMaterno?: string;
  nombre?: string;
  razonSocial?: string;
  nombreComercial?: string;
  telefono: string;
  correoElectronico: string;
  departamentoId: number;
  departamentoDescripcion: string;
  provinciaId: number;
  provinciaDescripcion: string;
  distritoId: number;
  distritoDescripcion: string;
  direccion: string;
}

// Listado de Pólizas
export class IPolicyList {
  policyType: string;
  policySelected?: string;
  typeDocument?: string;
  document?: string;
  agente?: IAgent;
  list: Array<ICardPoliciesView>;
}

// Comparar Pólizas - Request/Response
export interface ICompareHeaderResponse {
  codigoCompania: number;
  codigoRamo: number;
  nombreProducto: string;
  numeroCotizacion: number;
  flagIndicadorPoliza: string;
  observaciones?: IObservationCoverages;
  duracion: Array<{
    codigoModalidad: string;
    descripcion: string;
    tiempo: number;
    cuotas: Array<{
      numero: number;
      descripcion: string;
      monto: number;
      codigoMoneda: number;
      prima: number;
      codigoFraccionamiento: string;
    }>;
  }>;
}

export interface ICompareHeaderHomeResponse {
  codigoCompania: number;
  codigoRamo: number;
  codigoModalidad: string;
  nombreProducto: string;
  flagIndicadorPoliza: string;
  flagPuedeCambiarPoliza: boolean;
  numeroCotizacion?: number;
  observaciones?: IObservationCoverages;
  cuotas: Array<{
    numero: number;
    codigoFraccionamiento: string;
    descripcion: string;
    monto: number;
    codigoMoneda: number;
    prima: number;
  }>;
}

export interface ICompareDetailsResponse {
  coberturas: Array<ICoverageDetailsResponse>;
  otrosDeducibles: Array<ICoverageDetailsResponse>;
}

export interface ICoverageDetailsResponse {
  codigo: string;
  nombre: string;
  observaciones: IObservationCoverages;
  productos: Array<{
    codigoCompania: number;
    codigoRamo: number;
    flagIndicadorPoliza: string; // "NORMAL", "RECOMENDAMOS",
    duracion: Array<{
      codigoModalidad: string;
      tiempo: number;
      deducibles: Array<IDeductiblesResponse>;
    }>;
  }>;
}

export interface ICompareDetailsHomeResponse {
  grupoCoberturas: Array<ICoveragesHomeResponse>;
}

export interface ICoveragesHomeResponse {
  codigo: string;
  nombre: string;
  coberturas: Array<ICoverageGroupHomeResponse>;
}

export interface ICoverageGroupHomeResponse {
  codigo: string;
  nombre: string;
  observaciones: IObservationCoverages;
  productos: Array<{
    codigoCompania: number;
    codigoRamo: number;
    codigoModalidad: string;
    flagIndicadorPoliza: string;
    deducibles: Array<IDeductiblesResponse>;
  }>;
}

export interface IDeductiblesResponse {
  descripcion: {
    titulo: string;
    detalles: Array<string>;
  };
  tipo: number; // 1: titulo 2: tu pagas 3: deducibles
}

export interface IObservationCoverages {
  titulo: string;
  detalles: Array<string>;
}

// Comparar Pólizas - Vista
export interface ICompareHeaderView {
  company?: number;
  branch?: number;
  title: string;
  modality?: string;
  moneySymbol: string;
  monthlyCost: number;
  quote: number;
  requestYears: boolean; // Solicita 1 ó 2 años
  years?: number;
  titleTooltips?: string;
  contentTooltips?: Array<string>;
  amountFee: number;
  current?: boolean;
  recommended?: boolean;
  textValue?: string;
  textSumaryValue?: string;
  changeModality?: boolean;
  textChangeModality?: string;
  textChangeModalityFrom?: string;
  textPaymentFrecuency?: string;
  quoteNumber?: number;
  fractionationCode?: string;
}

export interface ICompareDetailView {
  codCoverages?: string;
  title: string;
  titleTooltips?: string;
  contentTooltips?: Array<string>;
  hasDeductibles?: boolean;
  collapse?: boolean; // muestra/oculta los deducibles
  options?: Array<{
    coverages: Array<ITextCoveragesCompare>;
    deductibles: Array<ITextCoveragesCompare>;
  }>;
}

export interface ICoverageGroupView {
  name: string;
  coverages: Array<ICompareDetailView>;
  collapse: boolean;
}

export interface ITextCoveragesCompare {
  descripcion: string;
  tipo: number; // 1: Texto principal, 2: Título en negrita, 3: Texto normal, 4: check, 5: vacio
  detalles: Array<string>;
}

export interface IChoiseTime {
  selectedTab?: number;
  month: number;
  years: Array<string>;
}

// Cambiar Modalidad
export interface IChangeModalityRequest extends GeneralRequest {
  numeroPoliza: string;
}

export interface IChangeModalityBody {
  datosPolizaVigente: IModalityRequest;
  datosPolizaNueva: IModalityRequest;
}

export interface IModalityRequest {
  codigoCompania: number;
  codigoRamo: number;
  codigoModalidad: string;
}

export interface IPolicyNotification {
  title?: string;
  subtitle?: string;
  action?: {
    actionLabel: string;
    actionKey: string;
  };
  icon?: {
    iconName: string;
    iconSize?: string;
  };
}

export interface ISctrPeriodResponse {
  cantTrabajadores: string;
  colectivoAsegurado: string;
  estadoPeriodo: SCTR_PERIOD_STATUS;
  fechaFinVigencia: string;
  fechaInicioVigencia: string;
  fechaPeriodo: string;
  polizas: Array<ISctrDetailsPeriodResponse>;
  puedeDeclarar: boolean;
  puedeDesglosar: boolean;
  semaforo: TRAFFIC_LIGHT_COLOR;
  tieneDeclaracion: boolean;
  tieneInclusion: boolean;
  tipoSituacion: SITUATION_TYPE_SCTR_PERIODS;
  frecuenciaPeriodo: string;
}

export interface ISctrDetailsPeriodResponse {
  numeroPoliza: string;
  codRamo: number;
  descripcionRamo: string;
  tipoPoliza: string;
  codCia: number;
  numApli: number;
  numSpto: number;
  numApliSpto: number;
  estadoPeriodo: SCTR_PERIOD_STATUS;
  cantTrabajadores: string;
  tieneOpciones: boolean;
}

export interface ISctrPeriodInfoCard {
  title?: string;
  insuredGroup: string;
  perdiod: string;
  start: string;
  end: string;
}

// tslint:disable-next-line: max-file-line-count
