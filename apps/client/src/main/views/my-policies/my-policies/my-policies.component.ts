import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'client-my-policies',
  templateUrl: './my-policies.component.html'
})
export class MyPoliciesComponent implements OnInit {
  disableCollapse = true;
  disableIcon = true;

  ngOnInit(): void {}
}
