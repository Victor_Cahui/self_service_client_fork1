import { Component } from '@angular/core';

@Component({
  selector: 'client-clinics-root-component',
  template: '<router-outlet></router-outlet>'
})
export class ClientClinicsRootComponent {}
