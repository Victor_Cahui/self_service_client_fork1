import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ClinicsSearchComponent } from './clinics-search.component';

const routes: Routes = [
  {
    path: '',
    component: ClinicsSearchComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClinicsSearchRoutingModule {}
