import { CurrencyPipe } from '@angular/common';
import { ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { ModalSelectPolicyComponent } from '@mx/components/shared/modals/modal-select-policy/modal-select-policy.component';

import { GmFilterComponent, GmFilterEvents, GmFilterService } from '@mx/components';
import { getDDMMYYYY, isExpired } from '@mx/core/shared/helpers/util/date';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { AppointmentFacade } from '@mx/core/shared/state/appointment';
import { environment } from '@mx/environments/environment';
import { AuthService } from '@mx/services/auth/auth.service';
import { HealthService } from '@mx/services/health/health.service';
import { PoliciesService, STORAGE_KEYS } from '@mx/services/policies.service';
import { APPLICATION_CODE, ConfigMapView, TYPE_APPOINTMENT, Y_AXIS } from '@mx/settings/constants/general-values';
import { FILE_ICON, MARKER_HEALTH } from '@mx/settings/constants/images-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { CLINICS_SEARCH_HEADER } from '@mx/settings/constants/router-titles';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { MapLang } from '@mx/settings/lang/map.lang';
import { OfficeLang } from '@mx/settings/lang/office.lang';
import { IItemMarker } from '@mx/statemanagement/models/google-map.interface';
import { IClinicFavoriteRequest, IClinicResponse, IClinicsRequest } from '@mx/statemanagement/models/health.interface';
import { ICardPoliciesView, IPoliciesByClientResponse } from '@mx/statemanagement/models/policy.interface';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { isNullOrUndefined } from 'util';

export class BaseClinicsSearch extends GaUnsubscribeBase {
  @ViewChild(ModalSelectPolicyComponent) modalSelectedPolicy: ModalSelectPolicyComponent;
  @ViewChild(GmFilterComponent) gmFilter: GmFilterComponent;

  markers: Array<IItemMarker> = [];
  showLoading: boolean;
  currentPolicySub: Subscription;
  subTitleModal: string;
  generalLang = GeneralLang;
  positionSelected = 0;
  openModalSelectPolicy: boolean;
  hasPermission: boolean;
  hasHealthPolicies: boolean;
  policies: Array<IPoliciesByClientResponse>;
  currentPolicy: IPoliciesByClientResponse;
  centerButtonResponsive: any = { icon: FILE_ICON };
  firstQuery = true;
  params: IClinicsRequest;
  policyListModal: Array<ICardPoliciesView>;
  auth: IdDocument;
  searchByClientSub: Subscription;
  filterClinicsSub: Subscription;
  favoriteSub: Subscription;
  configView = ConfigMapView;
  defaultCoords = MapLang.CLINIC.defaultCoords;
  title = MapLang.CLINIC.Title;
  policyTypes = POLICY_TYPES;
  activateSearchClinicKey = environment.ACTIVATE_SEARCH_CLINIC_CLD;
  geolocationButtonClass: string;
  codRamoClinicDigital = '275';
  isProductClinicDigital: boolean;

  constructor(
    public authService: AuthService,
    public healthService: HealthService,
    public activatedRoute: ActivatedRoute,
    public policiesService: PoliciesService,
    public currencyPipe: CurrencyPipe,
    public router: Router,
    public _AppointmentFacade: AppointmentFacade,
    public gmFilterService: GmFilterService,
    protected gaService: GAService,
    public storageService: StorageService
  ) {
    super(gaService);
    this.subTitleModal = CLINICS_SEARCH_HEADER.title;
    this.auth = this.authService.retrieveEntity();
    this.params = {
      codigoApp: APPLICATION_CODE,
      numeroPoliza: ''
    };
    this._AppointmentFacade.CleanAppointment();
    this.setPositionMapButtons();
  }

  getPolicies(): void {
    this.searchByClientSub = this.policiesService.getSortedHealthyPolicies().subscribe(
      (policies = []) => {
        const policiesNotClinicDigital = policies.filter(
          policy => policy.numeroPoliza.substring(0, 3) !== this.codRamoClinicDigital && !policy.esBeneficioAdicional
        );
        this.policies =
          policies.length > 1 && this.activateSearchClinicKey
            ? policiesNotClinicDigital.length === 0
              ? policies
              : policiesNotClinicDigital
            : policies;
        this.healthService.setPolicies(policies);
        if (policies.length) {
          const policyNumberQueryParam = this.activatedRoute.snapshot.queryParams['policy'];
          this.currentPolicy = this.activateSearchClinicKey
            ? this.policies[0]
            : policies.find(policy => policy.numeroPoliza === policyNumberQueryParam) || policies[0];
          this.isProductClinicDigital =
            this.currentPolicy.numeroPoliza.substring(0, 3) === this.codRamoClinicDigital &&
            this.activateSearchClinicKey;
          this.healthService.setCurrentPolicy(this.currentPolicy);
          this.centerButtonResponsive.label = this.currentPolicy.descripcionPoliza;
          this.formatModalPolicies();
        }
      },
      () => {
        this.showLoading = false;
        this.searchByClientSub.unsubscribe();
      }
    );
  }

  formatMarker(clinics: Array<IClinicResponse>): Array<IItemMarker> {
    const hasHealthPolicies = this.storageService.getStorage(STORAGE_KEYS.HAS_HEALTH_POLICIES);
    const items = clinics.map(item => {
      const moneyS = `${StringUtil.getMoneyDescription(item.codigoMonedaBeneficioCopago)} `;
      let address = `${item.direccion}, ${OfficeLang.Labels.district} ${item.distritoDescripcion}, `;
      address += `${item.provinciaDescripcion}, ${item.departamentoDescripcion}${this.getDistanceForCards(item)}`;
      const marker: IItemMarker = {
        id: item.clinicaId,
        latitude: item.latitud,
        longitude: item.longitud,
        pinUrl: MARKER_HEALTH,
        isFavorite: item.esFavorito,
        showFavoriteButton: hasHealthPolicies,
        imageUrl: item.imagenPortraitUrl,
        title: item.nombre,
        description: item.beneficioCopago
          ? `${OfficeLang.Labels.copayment} ${this.currencyPipe.transform(item.beneficioCopago, moneyS)}`
          : null,
        address,
        district: item.distritoDescripcion,
        province: item.provinciaDescripcion,
        department: item.departamentoDescripcion,
        phone: item.telefono,
        timetable: item.horarioAtencion || this.generalLang.Texts.WithOutInformation,
        distance: item.distancia,
        canScheduleAppointment: item.puedeAgendarCita,
        payload: item
      };

      return marker;
    });

    if (this.params.latitud) {
      return items.sort((a, b) => a.distance - b.distance);
    }

    return items.sort((a, b) => {
      if (a.title < b.title) {
        return -1;
      }
      if (a.title > b.title) {
        return 1;
      }

      return 0;
    });
  }

  favoriteFn(marker: IItemMarker): void {
    const clinic: IClinicResponse = marker.payload;
    clinic.esFavorito = marker.isFavorite;
    const params: IClinicFavoriteRequest = {
      codigoApp: APPLICATION_CODE,
      clinicaId: clinic.clinicaId
    };

    this.favoriteSub = this.healthService.clinicFavorite(params, { esFavorito: marker.isFavorite }).subscribe(
      () => {
        this.favoriteSub.unsubscribe();
      },
      () => {
        this.favoriteSub.unsubscribe();
      }
    );
  }

  formatModalPolicies(): void {
    this.policyListModal = this.policies.map(policy => {
      const expiredPolicy = isExpired(policy.fechaFin);

      return {
        policyType: policy.tipoPoliza,
        policyNumber: policy.numeroPoliza,
        beneficiaries: policy.beneficiarios,
        policy: policy.descripcionPoliza,
        expired: expiredPolicy,
        endDate: getDDMMYYYY(policy.fechaFin),
        iconoRamo: policy.iconoRamo,
        isVIP: policy.esPolizaVip,
        response: policy
      } as ICardPoliciesView;
    });
  }

  selectedCurrentPolicy(policySelected: ICardPoliciesView): void {
    this.currentPolicy = this.policies.filter(policy => policy.numeroPoliza === policySelected.policyNumber)[0];
    this.centerButtonResponsive.label = this.currentPolicy.descripcionPoliza;
    this.healthService.setCurrentPolicy(this.currentPolicy);
  }

  openPolicyModal(): void {
    this.openModalSelectPolicy = true;
    if (this.policies.length > 1) {
      setTimeout(() => {
        this.modalSelectedPolicy.open();
      });
    }
  }

  setDefaultPosition(): void {
    if (this.markers && this.markers.length && !this.hasPermission) {
      this.defaultCoords.latitude = this.markers[0].latitude;
      this.defaultCoords.longitude = this.markers[0].longitude;
    }
  }

  viewDetail(marker: IItemMarker): void {
    this.healthService.setCurrentClinic(marker.payload);
    this.router.navigate(['/clinics/detail']);
  }

  goToScheduleAppointment(marker): void {
    this._AppointmentFacade.AddEstablishment(marker.payload);
    this._AppointmentFacade.AddModality(TYPE_APPOINTMENT.PRESENCIAL);
    this._AppointmentFacade.AddPolicy(this.currentPolicy);
    this.router.navigate(['/digital-clinic/schedule/steps/1']);
  }

  userPosition(latitude: number, longitude: number): void {
    if (latitude && longitude) {
      this.hasPermission = true;
      this.params.latitud = latitude;
      this.params.longitud = longitude;
      this.currentPolicy
        ? this.getClinics(this.currentPolicy.numeroPoliza)
        : !this.hasHealthPolicies && this.getClinics();
    }
  }

  onChangePolicy(): void {
    this.currentPolicySub = this.healthService.getCurrentPolicy().subscribe(newPolicy => {
      this.gmFilterService.eventEmit(GmFilterEvents.CLEAR_SEARCH_INPUT);
      if (newPolicy && this.policies) {
        this.positionSelected = this.policies.findIndex(policy => policy.numeroPoliza === newPolicy.numeroPoliza);
        if (newPolicy !== this.currentPolicy) {
          this.currentPolicy = newPolicy;
          this.centerButtonResponsive.label = this.currentPolicy.descripcionPoliza;
        }
      }
    });
  }

  isDifferentParams(params: any = {}): boolean {
    const isPolicy = this.params.numeroPoliza === params.numeroPoliza;
    const isCoverage = (this.params.tipoCobertura || '') === params.tipoCobertura;
    const isType = (this.params.tiposEstablecimiento || '') === params.tiposEstablecimiento;
    const isDepartment = this.params.departamentoId === params.departamentoId;
    const isProvince = this.params.provinciaId === params.provinciaId;
    const isDistrict = this.params.distritoId === params.distritoId;

    return !isPolicy || !isCoverage || !isType || !isDepartment || !isProvince || !isDistrict;
  }

  getClinics(numeroPoliza?: string, fromFilter?: boolean): void {
    //
  }

  getDistanceForCards(item: any): string {
    if (!this.configView.canViewDistance) {
      return '';
    }

    return !isNullOrUndefined(item.distancia)
      ? item.distancia === 0
        ? ''
        : ` - ${item.distancia} ${OfficeLang.Labels.km}`
      : '';
  }

  setPositionMapButtons(): void {
    this.hasHealthPolicies = this.storageService.getStorage(STORAGE_KEYS.HAS_HEALTH_POLICIES);
    this.geolocationButtonClass = this.hasHealthPolicies ? Y_AXIS.TOP : Y_AXIS.BOTTOM;
  }
}
