import { CurrencyPipe } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GmFilterEvents, GmFilterService, GmFilterViewClinicComponent, GmFilterViewItem } from '@mx/components';
import { GeolocationService } from '@mx/core/shared/helpers/util/geolocation.service';
import { GAService, IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AppointmentFacade } from '@mx/core/shared/state/appointment';
import { AuthService } from '@mx/services/auth/auth.service';
import { HealthService } from '@mx/services/health/health.service';
import { PoliciesService } from '@mx/services/policies.service';
import {
  Agregar_como_favorito_48C,
  Buscar_48B,
  Filtrar_48A,
  Filtrar_por_48E,
  Filtrar_por_48F,
  MFP_Buscador_clininca_48H,
  Tu_Poliza_48G,
  Ver_detalle_48D
} from '@mx/settings/constants/events.analytics';
import { HEALTH_QUOTE_HEADER } from '@mx/settings/constants/router-titles';
import { HealthLang } from '@mx/settings/lang/health.lang';
import { IBannerAccordion } from '@mx/statemanagement/models/general.models';
import { IClinicResponse } from '@mx/statemanagement/models/health.interface';
import { Subscription } from 'rxjs';
import { BaseClinicsSearch } from './base-clinics-search.component';

@Component({
  selector: 'client-clinics-search-component',
  templateUrl: './clinics-search.component.html',
  providers: [CurrencyPipe]
})
export class ClinicsSearchComponent extends BaseClinicsSearch implements OnInit, OnDestroy {
  healthSub: Subscription;
  gmFilterViewItem: GmFilterViewItem;
  bannerAccordion: IBannerAccordion;
  codRamoClinicDigital = '275';

  ga: Array<IGaPropertie>;

  constructor(
    authService: AuthService,
    healthService: HealthService,
    activatedRoute: ActivatedRoute,
    policiesService: PoliciesService,
    currencyPipe: CurrencyPipe,
    router: Router,
    appointmentFacade: AppointmentFacade,
    gmFilterService: GmFilterService,
    private readonly geolocationService: GeolocationService,
    protected gaService: GAService,
    storageService: StorageService,
    public _Autoservicios: Autoservicios
  ) {
    super(
      authService,
      healthService,
      activatedRoute,
      policiesService,
      currencyPipe,
      router,
      appointmentFacade,
      gmFilterService,
      gaService,
      storageService
    );
    this.gmFilterViewItem = new GmFilterViewItem(GmFilterViewClinicComponent);
    this.ga = [
      Filtrar_48A(),
      Buscar_48B(),
      Agregar_como_favorito_48C(),
      Ver_detalle_48D(),
      Filtrar_por_48E(),
      Filtrar_por_48F(),
      Tu_Poliza_48G(),
      MFP_Buscador_clininca_48H()
    ];
    this.gmFilterViewItem = new GmFilterViewItem(GmFilterViewClinicComponent);

    this.configView = {
      ...this.configView,
      canViewDeductibles: this.hasHealthPolicies,
      canViewFooterButtons: this.hasHealthPolicies,
      inDesktop: {
        hasFilter: this.hasHealthPolicies,
        hasSeeker: true
      }
    };
  }

  ngOnInit(): void {
    this.showLoading = true;
    this.getPolicies();
    this.setBannerAccordionInfo();

    this.geolocationService.hasPermission().then(hasPermission => {
      this.hasPermission = hasPermission;
      this.onFilter();
    });

    this.onChangePolicy();
  }

  ngOnDestroy(): void {
    this.currentPolicySub && this.currentPolicySub.unsubscribe();
    this.filterClinicsSub && this.filterClinicsSub.unsubscribe();
    this.searchByClientSub && this.searchByClientSub.unsubscribe();
    this.gmFilterService.setModifiedItems(null);
  }

  onFilter(): void {
    this.filterClinicsSub = this.healthService.getFilterClinics().subscribe(filter => {
      this.gmFilterService.eventEmit(GmFilterEvents.CLEAR_SEARCH_INPUT);
      if (filter && this.isDifferentParams(filter)) {
        this.params.numeroPoliza = filter.numeroPoliza;
        this.params.tipoCobertura = filter.tipoCobertura;
        this.params.tiposEstablecimiento = filter.tiposEstablecimiento;
        this.params.departamentoId = filter.departamentoId;
        this.params.provinciaId = filter.provinciaId;
        this.params.distritoId = filter.distritoId;

        if (!(this.firstQuery && this.hasPermission && !this.params.latitud)) {
          this.getClinics(filter.numeroPoliza, !this.firstQuery);
        }
      }
    });
  }

  getClinics(numeroPoliza: string, fromFilter?: boolean): void {
    this.showLoading = true;
    if (this.firstQuery) {
      this.params.tipoCobertura = this.activatedRoute.snapshot.queryParams['coverage'] || '';
      this.firstQuery = false;
    }
    this.params.numeroPoliza = numeroPoliza || '';
    if (!this.hasHealthPolicies || this.params.numeroPoliza.substring(0, 3) === this.codRamoClinicDigital) {
      // usuario invitado
      this._Autoservicios
        .ObtenerClinicasGeneral(this.params)
        .subscribe(this._mapResponse.bind(this, fromFilter), this._mapError.bind(this));
    } else {
      // usuario contratante (MD_SALUD, MD_EPS)
      this.healthService
        .clinicsList(this.params)
        .subscribe(this._mapResponse.bind(this, fromFilter), this._mapError.bind(this));
    }
  }

  setBannerAccordionInfo(): void {
    this.bannerAccordion = {
      title: HealthLang.Titles.AccessToClinics,
      body: HealthLang.Messages.AccessToClinics,
      endLink: {
        isExternal: false,
        text: HealthLang.Links.Here,
        url: HEALTH_QUOTE_HEADER.url
      }
    };
  }

  private _mapResponse(fromFilter: boolean, clinics: Array<IClinicResponse>): void {
    let markers = this.formatMarker(clinics);
    this.gmFilterService.setModifiedItems(markers);
    markers = this.gmFilter.fnSearch([]);
    setTimeout(() => (this.showLoading = false), 100);
    if (fromFilter) {
      this.gmFilterService.eventEmit(GmFilterEvents.FILTER, markers);
      this.markers = markers;
    } else {
      setTimeout(() => {
        this.markers = markers;
        this.setDefaultPosition();
      }, 0);
    }
  }

  private _mapError(): void {
    this.showLoading = false;
    this.markers = [];
  }
}
