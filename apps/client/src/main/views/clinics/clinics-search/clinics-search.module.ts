import { NgModule } from '@angular/core';
import { ClinicsSearchRoutingModule } from './clinics-search.routing';

import { CommonModule } from '@angular/common';
import {
  GmFilterModule,
  GmFilterViewClinicModule,
  GmFilterViewOfficeModule,
  GoogleMapFilterViewModule
} from '@mx/components';
import { BannerAccordionModule } from '@mx/components/shared/banner-accordion/banner-accordion.module';
import { ModalSelectPolicyModule } from '@mx/components/shared/modals/modal-select-policy/modal-select-policy.module';
import { GeolocationService } from '@mx/core/shared/helpers/util/geolocation.service';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { ClinicsSearchComponent } from './clinics-search.component';

@NgModule({
  imports: [
    CommonModule,
    ClinicsSearchRoutingModule,
    BannerAccordionModule,
    GoogleMapFilterViewModule,
    GmFilterModule,
    ModalSelectPolicyModule,
    GmFilterViewOfficeModule,
    GmFilterViewClinicModule,
    GoogleModule
  ],
  exports: [],
  declarations: [ClinicsSearchComponent],
  providers: [GeolocationService]
})
export class ClinicsSearchModule {}
