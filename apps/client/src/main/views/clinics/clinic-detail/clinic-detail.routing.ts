import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ClinicDetailComponent } from './clinic-detail.component';

const routes: Routes = [
  {
    path: '',
    component: ClinicDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClinicDetailRoutingModule {}
