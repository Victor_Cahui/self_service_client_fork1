import { NgModule } from '@angular/core';
import { ClinicDetailInfoModule } from '@mx/components/clinics/clinic-detail-info/clinic-detail-info.module';
import { ClinicListCoverageModule } from '@mx/components/clinics/clinic-list-coverage/clinic-list-coverage.module';
import { ClinicDetailComponent } from './clinic-detail.component';
import { ClinicDetailRoutingModule } from './clinic-detail.routing';

@NgModule({
  imports: [ClinicDetailRoutingModule, ClinicDetailInfoModule, ClinicListCoverageModule],
  declarations: [ClinicDetailComponent],
  providers: []
})
export class ClinicsSearchModule {}
