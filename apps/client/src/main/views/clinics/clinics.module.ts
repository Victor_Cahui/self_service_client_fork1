import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ClinicsRoutingModule } from './clinics-routing.module';
import { ClientClinicsRootComponent } from './clinics.component';

@NgModule({
  imports: [CommonModule, ClinicsRoutingModule],
  declarations: [ClientClinicsRootComponent]
})
export class ClinicsModule {}
