import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'search'
  },
  {
    path: 'search',
    loadChildren: './clinics-search/clinics-search.module#ClinicsSearchModule'
  },
  {
    path: 'detail',
    loadChildren: './clinic-detail/clinic-detail.module#ClinicsSearchModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClinicsRoutingModule {}
