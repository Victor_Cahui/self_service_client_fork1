import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardMyGuaranteeLettersModule } from '@mx/components/card-my-guarantee-letters/card-my-guarantee-letters.module';
import { HealthGuaranteeLettersComponent } from './health-guarantee-letters.component';
import { HealthGuaranteeLettersRoutingModule } from './health-guarantee-letters.routing';

@NgModule({
  imports: [CommonModule, HealthGuaranteeLettersRoutingModule, CardMyGuaranteeLettersModule],
  exports: [],
  declarations: [HealthGuaranteeLettersComponent],
  providers: []
})
export class HealthGuaranteeLettersModule {}
