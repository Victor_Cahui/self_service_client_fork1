import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { EPS_INSURANCE_FAQS } from '@mx/settings/faqs/eps-insurance.faq';
import { HEALTH_INSURANCE_FAQS } from '@mx/settings/faqs/health-insurance.faq';
import { GeneralLang } from '@mx/settings/lang/general.lang';

@Component({
  selector: 'client-health-insurance-faq-component',
  templateUrl: 'health-insurance-faq.component.html'
})
export class HealthInsuranceFAQComponent implements OnInit {
  faqs: Array<{ title: string; content: string; collapse: boolean }>;
  policyType: string;
  isEps: boolean;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly headerHelperService: HeaderHelperService
  ) {}

  ngOnInit(): void {
    this.policyType = this.activatedRoute.snapshot.params['policyType'];
    this.isEps = this.policyType === POLICY_TYPES.MD_EPS.code;
    this.headerHelperService.set({
      subTitle: this.isEps ? POLICY_TYPES.MD_EPS.code.replace('MD_', '') : POLICY_TYPES.MD_SALUD.title,
      title: GeneralLang.Titles.Faq.toUpperCase(),
      icon: '',
      back: true
    });
    const content = this.isEps ? EPS_INSURANCE_FAQS : HEALTH_INSURANCE_FAQS;
    this.faqs = content.map(faq => ({ title: faq.title, content: faq.content, collapse: true }));
  }
}
