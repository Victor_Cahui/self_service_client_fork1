import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardMoreFaqModule } from '@mx/components/shared/card-more-faq/card-more-faq.module';
import { HealthInsuranceFAQComponent } from './health-insurance-faq.component';
import { HealthInsuranceFAQRoutingModule } from './health-insurance-faq.routing';

@NgModule({
  imports: [CommonModule, CardMoreFaqModule, HealthInsuranceFAQRoutingModule],
  declarations: [HealthInsuranceFAQComponent]
})
export class HealthInsuranceFAQModule {}
