import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardContentModule } from '@mx/components/shared/card-content/card-content.module';
import { DownLoadConditionedModule } from '@mx/components/shared/download-conditioned/download-conditioned.module';
import { HealthConditionedComponent } from './health-conditioned.component';
import { HealthConditionedRoutingModule } from './health-conditioned.routing';

@NgModule({
  imports: [CommonModule, HealthConditionedRoutingModule, DownLoadConditionedModule, CardContentModule],
  declarations: [HealthConditionedComponent]
})
export class HealthConditionedModule {}
