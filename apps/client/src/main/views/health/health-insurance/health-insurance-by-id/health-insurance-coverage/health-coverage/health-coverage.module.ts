import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardIconsCoverageModule } from '@mx/components/shared/card-icons-coverage/card-icons-coverage.module';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { HealthCoverageComponent } from './health-coverage.component';
import { HealthCoverageRouting } from './health-coverage.routing';

@NgModule({
  imports: [CommonModule, HealthCoverageRouting, CardIconsCoverageModule, MfLoaderModule],
  declarations: [HealthCoverageComponent]
})
export class HealthCoverageModule {}
