import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FoilAttachedModule } from '@mx/components/shared/foil-attached/foil-attached.module';
import { HealthFoilAttachedComponent } from './health-foil-attached.component';
import { HealthFoilAttachedRoutingModule } from './health-foil-attached.routing';

@NgModule({
  imports: [CommonModule, RouterModule, HealthFoilAttachedRoutingModule, FoilAttachedModule],
  declarations: [HealthFoilAttachedComponent]
})
export class HealthFoilAttachedModule {}
