import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HospitalCareComponent } from './hospital-care.component';

const routes: Routes = [
  {
    path: '',
    component: HospitalCareComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HospitalCareRoutingModule {}
