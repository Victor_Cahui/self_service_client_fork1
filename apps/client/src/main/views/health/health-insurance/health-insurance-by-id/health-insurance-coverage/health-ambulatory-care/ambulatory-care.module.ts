import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardContentModule } from '@mx/components/shared/card-content/card-content.module';
import { ListCareNetworkModule } from '@mx/components/shared/list-care-network/list-care-network.module';
import { AmbulatoryCareComponent } from './ambulatory-care.component';
import { AmbulatoryCareRouting } from './ambulatory-care.routing';

@NgModule({
  imports: [CommonModule, AmbulatoryCareRouting, CardContentModule, ListCareNetworkModule],
  declarations: [AmbulatoryCareComponent]
})
export class AmbulatoryCareModule {}
