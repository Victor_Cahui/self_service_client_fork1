import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HealthWaitingPeriodComponent } from './health-waiting-period.component';

const routes: Routes = [
  {
    path: '',
    component: HealthWaitingPeriodComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HealthWaitingPeriodRoutingModule {}
