import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardContentModule } from '@mx/components/shared/card-content/card-content.module';
import { HealthWaitingPeriodComponent } from './health-waiting-period.component';
import { HealthWaitingPeriodRoutingModule } from './health-waiting-period.routing';

@NgModule({
  imports: [CommonModule, HealthWaitingPeriodRoutingModule, CardContentModule],
  declarations: [HealthWaitingPeriodComponent]
})
export class HealthWaitingPeriodModule {}
