import { Component, HostBinding } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { COB_SAL_CESAREA, COB_SAL_PARTO_NAT } from '@mx/settings/constants/coverage-values';
import { TYPE_MATERNITY } from '@mx/settings/constants/policy-values';
import { CLINICS_SEARCH_HEADER } from '@mx/settings/constants/router-titles';
import { HealthLang, MATERNITY } from '@mx/settings/lang/health.lang';
import { IViewOnMapOptions } from '@mx/statemanagement/models/google-map.interface';
import { HealthCoverageBase } from '../health-coverage-base';

@Component({
  selector: 'client-maternity',
  templateUrl: './maternity.component.html'
})
export class MaternityComponent extends HealthCoverageBase {
  @HostBinding('class') class = 'w-100';

  content = MATERNITY;
  coverageType = TYPE_MATERNITY;
  viewOnMapOptions: IViewOnMapOptions = {
    labelLink: HealthLang.Links.ViewMap,
    urlLink: CLINICS_SEARCH_HEADER.url,
    coverages: [COB_SAL_PARTO_NAT, COB_SAL_CESAREA],
    hidenLinkMap: this.hidenLinkMap
  };

  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService,
    protected policiesService: PoliciesService,
    protected headerHelperService: HeaderHelperService,
    protected activatedRoute: ActivatedRoute,
    protected localStorageService: LocalStorageService,
    protected router: Router,
    protected _Autoservicios: Autoservicios
  ) {
    super(
      clientService,
      policiesInfoService,
      configurationService,
      authService,
      policiesService,
      headerHelperService,
      activatedRoute,
      localStorageService,
      router,
      _Autoservicios
    );
  }
}
