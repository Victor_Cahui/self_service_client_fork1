import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CardDetailPolicyBase } from '@mx/components/shared/card-detail-policy/card-detail-policy-base';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import {
  MFP_Cuotas_de_Poliza_Pagar_28C,
  MFP_Poliza_de_Seguro_de_Salud_27B,
  MFP_Poliza_de_Seguro_de_Salud_27C,
  MFP_Poliza_de_Seguro_de_Salud_28A,
  MFP_Poliza_de_Seguro_de_Salud_28B
} from '@mx/settings/constants/events.analytics';

@Component({
  selector: 'client-health-insurance-detail',
  templateUrl: './health-insurance-detail.component.html'
})
export class HealthInsuranceDetailComponent extends CardDetailPolicyBase implements OnInit {
  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService,
    protected policiesService: PoliciesService,
    protected headerHelperService: HeaderHelperService,
    protected activatedRoute: ActivatedRoute,
    protected localStorageService: LocalStorageService,
    protected router: Router,
    protected _Autoservicios: Autoservicios
  ) {
    super(
      clientService,
      policiesInfoService,
      configurationService,
      authService,
      policiesService,
      headerHelperService,
      activatedRoute,
      localStorageService,
      router,
      false,
      _Autoservicios
    );
    this.ga = [
      MFP_Poliza_de_Seguro_de_Salud_27C(),
      MFP_Poliza_de_Seguro_de_Salud_27B(),
      MFP_Poliza_de_Seguro_de_Salud_28A('Detalles'),
      MFP_Poliza_de_Seguro_de_Salud_28B('Detalles'),
      MFP_Cuotas_de_Poliza_Pagar_28C('Salud')
    ];
  }

  ngOnInit(): void {
    this.loadInfoProfile();
  }

  doExtraAsyncActions(): void {
    this.loadHeadlineData();
  }
}
