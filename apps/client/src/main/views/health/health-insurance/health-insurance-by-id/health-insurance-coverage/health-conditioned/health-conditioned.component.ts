import { Component } from '@angular/core';
import { CONDITIONED } from '@mx/settings/lang/health.lang';

@Component({
  selector: 'client-health-conditioned-component',
  templateUrl: './health-conditioned.component.html'
})
export class HealthConditionedComponent {
  content = CONDITIONED;
}
