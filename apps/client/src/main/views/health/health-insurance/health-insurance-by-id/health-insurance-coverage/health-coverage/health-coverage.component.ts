import { Component, HostBinding, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { COVERAGES_HEALTH, ICON_COVERAGE_DEFAULT, QUERY_SOURCE } from '@mx/settings/constants/coverage-values';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { MYPOLICY } from '@mx/settings/lang/health.lang';
import { ICoverageIcons, ICoverageItem } from '@mx/statemanagement/models/coverage.interface';
import {
  ICoveragesPolicy,
  ICoveragesPolicyRequest,
  IPoliciesByClientRequest,
  IPoliciesByClientResponse
} from '@mx/statemanagement/models/policy.interface';
import { ITabItem } from '@mx/statemanagement/models/router.interface';
import { Subscription } from 'rxjs';
import { HealthCoverageBase } from '../health-coverage-base';

@Component({
  selector: 'client-health-coverage',
  templateUrl: './health-coverage.component.html'
})
export class HealthCoverageComponent extends HealthCoverageBase implements OnDestroy, OnInit {
  @HostBinding('attr.class') attr_class = 'w-10';

  options: Array<ITabItem>;
  coveragesSub: Subscription;
  itemsList: Array<ICoverageItem>;
  itemsListBase: Array<ICoverageIcons>;
  title: string;
  text: string;
  typeCoverage: string;
  paramsSub: Subscription;
  policyDetailSub: Subscription;

  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService,
    protected policiesService: PoliciesService,
    protected headerHelperService: HeaderHelperService,
    protected activatedRoute: ActivatedRoute,
    protected localStorageService: LocalStorageService,
    protected router: Router,
    protected _Autoservicios: Autoservicios
  ) {
    super(
      clientService,
      policiesInfoService,
      configurationService,
      authService,
      policiesService,
      headerHelperService,
      activatedRoute,
      localStorageService,
      router,
      _Autoservicios
    );
  }

  ngOnInit(): void {
    this.showLoading = true;
  }

  ngOnDestroy(): void {
    if (this.paramsSub) {
      this.paramsSub.unsubscribe();
    }
  }

  // Detalle de la poliza
  loadInfoDetail(): void {
    const clientPolicy = this.policiesInfoService.getClientPolicy(this.policyNumber);
    if (!clientPolicy) {
      this.setClearInfoDetail();
      this.policyDetailSub = this.policiesService
        .searchByClientService({
          codigoApp: APPLICATION_CODE,
          numeroPoliza: this.policyNumber
        } as IPoliciesByClientRequest)
        .subscribe(
          (response: Array<IPoliciesByClientResponse>) => {
            if (response && response.length > 0) {
              const clientPolicyResponse = response[0];
              this.policiesInfoService.setClientPolicyResponse(clientPolicyResponse);
              this.setInfoDetail(clientPolicyResponse);
              this.configView(clientPolicyResponse);
            }
          },
          () => {},
          () => {
            this.policyDetailSub.unsubscribe();
          }
        );
    } else {
      this.setInfoDetail(clientPolicy);
      this.configView(clientPolicy);
      this.showLoading = false;
    }
  }

  configView(clientPolicy): void {
    // Titulos
    this.setSubTitle();
    this.namePolice = (clientPolicy.descripcionPoliza || '').toUpperCase();
    this.headerHelperService.set({
      title: this.namePolice,
      subTitle: this.subTitle,
      icon: '',
      back: true
    });
    // Contenido
    this.paramsSub = this.activatedRoute.params.subscribe(params => {
      this.typeCoverage = params['typeCoverage'];
      this.setContent();
    });
  }

  // Setear contenido
  setContent(): void {
    if (!this.typeCoverage) {
      this.typeCoverage = ' ';
    }
    switch (this.typeCoverage) {
      case 'coverages':
        this.title = MYPOLICY.title;
        this.text = MYPOLICY.content;
        this.text = this.text.replace('{{typeInsured}}', this.namePolice.toUpperCase());
        this.itemsListBase = COVERAGES_HEALTH;
        this.getCoverages();
        break;
      default:
        this.title = '';
        this.text = '';
        this.itemsList = [];
        this.showLoading = false;
    }
  }

  // Obtener coberturas de servicio
  getCoverages(): void {
    this.showLoading = true;
    const params: ICoveragesPolicyRequest = {
      codigoApp: APPLICATION_CODE,
      numeroPoliza: this.policyNumber,
      origenConsulta: QUERY_SOURCE.POLICY_DETAIL
    };

    this.coveragesSub = this.policiesService.getCoveragesByPolicy(params).subscribe(
      (response: any) => {
        if (response && response.coberturas && response.coberturas.length > 0) {
          this.setDataItemCoverage(response.coberturas);
        }
        this.coveragesSub.unsubscribe();
        this.showLoading = false;
      },
      () => {
        this.showLoading = false;
      }
    );
  }

  setDataItemCoverage(list: Array<ICoveragesPolicy>): void {
    this.itemsList = list.map(item => {
      const iconC = this.itemsListBase.find(c => {
        return c.key === item.llave;
      });
      const itemCoverage: ICoverageItem = {
        title: item.tituloInfo,
        description: item.descripcionInfo,
        question: item.accionTexto,
        questionBool: item.accion,
        routeKey: item.accionLlave,
        iconKey: item.llave,
        icon: (iconC && iconC.icon) || ICON_COVERAGE_DEFAULT
      };

      return itemCoverage;
    });
  }
}
