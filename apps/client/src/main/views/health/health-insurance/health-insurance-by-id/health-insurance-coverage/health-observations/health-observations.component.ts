import { Component, HostBinding, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { PoliciesService } from '@mx/services/policies.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { HealthLang, OBSERVATIONS } from '@mx/settings/lang/health.lang';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import { IObservationsResponse, IPolicyInsuredResponse } from '@mx/statemanagement/models/policy.interface';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-health-observations-component',
  templateUrl: './health-observations.component.html'
})
export class HealthObservationsComponent implements OnInit {
  @HostBinding('class') class = 'w-100';

  policyNumber: string;
  observationsSub: Subscription;
  content = { ...OBSERVATIONS };
  labelInsured = PolicyLang.Labels.InsuredSelect;
  insuredList: any;
  observationsText: string;
  oneInsured: boolean;
  auth: IdDocument;
  messageInsured: string;
  showLoading: boolean;
  flowFinished: boolean;

  constructor(
    private readonly _Autoservicios: Autoservicios,
    private readonly policiesService: PoliciesService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly authService: AuthService
  ) {}

  ngOnInit(): void {
    this.auth = this.authService.retrieveEntity();
    this.activatedRoute.paramMap.subscribe((res: any) => {
      this.policyNumber = res.params.policyNumber;
    });
    this.getInsured();
  }

  // Lista de asegurados
  getInsured(): void {
    this.showLoading = true;

    const params = {
      codigoApp: APPLICATION_CODE
    };

    this._Autoservicios.ObtenerAseguradosTipoUsuario({ numeroPoliza: this.policyNumber }, params).subscribe(
      (response: Array<IPolicyInsuredResponse>) => {
        if (response && response.length) {
          this.insuredList = [];
          // Si hay un solo asegurado muestra las exclusiones
          this.oneInsured = response.length === 1;
          if (this.oneInsured) {
            const insured = response[0];
            const name = `${insured.nombre} ${insured.apellidoPaterno}`;
            const titular = this.auth.type === insured.tipoDocumento && this.auth.number === insured.documento;
            this.content.content = titular
              ? HealthLang.Messages.observationsOneInsuredTitular.replace('{{nameInsured}}', name)
              : HealthLang.Messages.observationsOneInsured.replace('{{nameInsured}}', name);
            this.messageInsured = titular
              ? HealthLang.Messages.withOutObservationsTitular.replace('{{nameInsured}}', name)
              : HealthLang.Messages.withOutObservationsOneInsured.replace('{{nameInsured}}', name);
            this.getObservations(`${insured.tipoDocumento}-${insured.documento}`);
          } else {
            this.messageInsured = HealthLang.Messages.withOutObservations;
            // Llena Select
            response.forEach(item => {
              this.insuredList.push({
                text: `${item.nombre} ${item.apellidoPaterno} - ${item.relacionDescripcion}`,
                value: `${item.tipoDocumento}-${item.documento}`,
                selected: false,
                disabled: false
              });
            });
          }
        } else {
          this.showLoading = false;
          this.content.content = '';
          this.observationsText = `${GeneralLang.Alert.titleFail} ${GeneralLang.Messages.ItemNotFound}`;
        }
      },
      () => {
        this.showLoading = false;
      },
      () => {
        this.showLoading = false;
      }
    );
  }

  // Exclusiones por asegurado
  getObservations(insured: any): void {
    // tslint:disable-next-line: no-parameter-reassignment
    insured = insured.split('-');
    delete this.observationsText;
    this.showLoading = true;
    this.observationsSub = this.policiesService
      .getObservationsByInsured({
        codigoApp: APPLICATION_CODE,
        numeroPoliza: this.policyNumber,
        tipoDocumentoAsegurado: insured[0],
        documentoAsegurado: insured[1]
      })
      .subscribe(
        (response: IObservationsResponse) => {
          this.flowFinished = true;
          if (response.observaciones) {
            this.observationsText = response.observaciones;
          } else {
            delete this.content.content;
            this.observationsText = this.messageInsured;
          }
        },
        () => {
          this.flowFinished = true;
          delete this.content.content;
          this.showLoading = false;
          this.observationsText = `${GeneralLang.Alert.titleFail} ${GeneralLang.Alert.sendData.error.message}`;
        },
        () => {
          this.showLoading = false;
          this.observationsSub.unsubscribe();
        }
      );
  }

  changeInsuranced(insured: any): void {
    this.getObservations(insured);
  }
}
