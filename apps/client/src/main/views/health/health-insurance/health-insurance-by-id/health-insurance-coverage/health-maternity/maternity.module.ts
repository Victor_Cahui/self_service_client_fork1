import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardContentModule } from '@mx/components/shared/card-content/card-content.module';
import { ListCareNetworkModule } from '@mx/components/shared/list-care-network/list-care-network.module';
import { MaternityComponent } from './maternity.component';
import { MaternityRoutingModule } from './maternity.routing';

@NgModule({
  imports: [CommonModule, MaternityRoutingModule, CardContentModule, ListCareNetworkModule],
  declarations: [MaternityComponent]
})
export class MaternityModule {}
