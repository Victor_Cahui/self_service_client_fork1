import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AffiliateModule } from '@mx/components/affiliate/affiliate.module';
import { InputSearchModule } from '@mx/components/shared/input-search/input-search.module';
import { ItemNotFoundModule } from '@mx/components/shared/item-not-found/item-not-found.module';
import { MfInputModule, MfPaginatorModule, MfSelectModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { AffiliatesComponent } from './affiliates.component';
import { AffiliatesRoutingModule } from './affiliates.routing';

@NgModule({
  imports: [
    AffiliateModule,
    AffiliatesRoutingModule,
    CommonModule,
    FormsModule,
    InputSearchModule,
    ItemNotFoundModule,
    MfInputModule,
    MfLoaderModule,
    MfPaginatorModule,
    MfSelectModule,
    ReactiveFormsModule
  ],
  declarations: [AffiliatesComponent]
})
export class AffiliatesModule {}
