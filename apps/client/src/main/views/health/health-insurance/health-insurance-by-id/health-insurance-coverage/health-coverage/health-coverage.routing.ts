import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HealthCoverageComponent } from './health-coverage.component';

const routes: Routes = [
  {
    path: '',
    component: HealthCoverageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HealthCoverageRouting {}
