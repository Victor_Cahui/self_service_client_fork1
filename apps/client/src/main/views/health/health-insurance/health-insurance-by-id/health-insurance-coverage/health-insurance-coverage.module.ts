import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardWhatYouWantToDoModule } from '@mx/components/card-what-you-want-to-do/card-what-you-want-to-do.module';
import { CardContentCoverageModule } from '@mx/components/shared/card-content-coverage/card-content-coverage.module';
import { ItemNotFoundModule } from '@mx/components/shared/item-not-found/item-not-found.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { HealthInsuranceCoverageComponent } from './health-insurance-coverage.component';
import { HealthInsuranceCoverageRoutingModule } from './health-insurance-coverage.routing';

@NgModule({
  imports: [
    CommonModule,
    HealthInsuranceCoverageRoutingModule,
    CardContentCoverageModule,
    CardWhatYouWantToDoModule,
    ItemNotFoundModule,
    MfLoaderModule,
    GoogleModule
  ],
  declarations: [HealthInsuranceCoverageComponent]
})
export class HealthInsuranceCoverageModule {}
