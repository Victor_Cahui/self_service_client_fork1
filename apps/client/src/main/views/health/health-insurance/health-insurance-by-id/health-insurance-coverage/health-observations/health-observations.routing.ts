import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HealthObservationsComponent } from './health-observations.component';

const routes: Routes = [
  {
    path: '',
    component: HealthObservationsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HealthObservationsRoutingModule {}
