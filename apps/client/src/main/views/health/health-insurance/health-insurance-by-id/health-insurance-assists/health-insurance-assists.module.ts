import { NgModule } from '@angular/core';
import { HealthInsuranceAssistsComponent } from './health-insurance-assists.component';
import { HealthInsuranceAssistsRoutingModule } from './health-insurance-assists.routing';

import { UnderConstructionModule } from '@mx/pages/under-construction/under-construction.module';

@NgModule({
  imports: [HealthInsuranceAssistsRoutingModule, UnderConstructionModule],
  declarations: [HealthInsuranceAssistsComponent]
})
export class HealthInsuranceAssistsModule {}
