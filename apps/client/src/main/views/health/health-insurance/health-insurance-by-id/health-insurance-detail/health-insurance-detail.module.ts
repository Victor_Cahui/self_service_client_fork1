import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardWhatYouWantToDoModule } from '@mx/components';
import { CardAffiliatesModule } from '@mx/components/card-affiliates/card-affiliates.module';
import { CardMyFeesModule } from '@mx/components/card-my-fees/card-my-fees/card-my-fees.module';
import { CardPendingInvoicesModule } from '@mx/components/card-pending-invoices/card-pending-invoices.module';
import { NotificationsnModule } from '@mx/components/notifications/notifications.module';
import { CardPaymentEpsModule } from '@mx/components/payment/card-payment-eps/card-payment-eps.module';
import { CardPaymentPolicyModule } from '@mx/components/payment/card-payment-policy/card-payment-policy.module';
import { CardContractingInfoModule } from '@mx/components/policy/card-contracting-info/card-contracting-info.module';
import { CardEpsBasicInfoModule } from '@mx/components/policy/card-eps-basic-info/card-eps-basic-info.module';
import { CardPolicyBasicInfoModule } from '@mx/components/policy/card-policy-basic-info/card-policy-basic-info.module';
import { CardPolicyInsuredGroupedModule } from '@mx/components/policy/card-policy-insured-grouped/card-policy-insured-grouped.module';
import { CardBasicInfoModule } from '@mx/components/profile/card-basic-info/card-basic-info.module';
import { BannerCarouselModule } from '@mx/components/shared/banner-carousel/banner-carousel.module';
import { BannerModule } from '@mx/components/shared/banner/banner.module';
import { CardDetailPolicyModule } from '@mx/components/shared/card-detail-policy/card-detail-policy.module';
import { ItemNotFoundModule } from '@mx/components/shared/item-not-found/item-not-found.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { HealthInsuranceDetailComponent } from './health-insurance-detail.component';
import { HealthInsuranceDetailRoutingModule } from './health-insurance-detail.routing';

@NgModule({
  imports: [
    BannerModule,
    CommonModule,
    CardAffiliatesModule,
    GoogleModule,
    HealthInsuranceDetailRoutingModule,
    CardDetailPolicyModule,
    CardMyFeesModule,
    CardPendingInvoicesModule,
    NotificationsnModule,
    CardPaymentEpsModule,
    CardPaymentPolicyModule,
    CardEpsBasicInfoModule,
    CardPolicyBasicInfoModule,
    CardWhatYouWantToDoModule,
    CardPolicyInsuredGroupedModule,
    BannerCarouselModule,
    CardContractingInfoModule,
    ItemNotFoundModule,
    MfLoaderModule,
    CardBasicInfoModule
  ],
  declarations: [HealthInsuranceDetailComponent]
})
export class HealthInsuranceDetailModule {}
