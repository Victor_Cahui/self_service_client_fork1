import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfSelectModule } from '@mx/core/ui/lib/components/forms/select';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { HealthExclusionsComponent } from './health-exclusions.component';
import { HealthExclusionsRoutingModule } from './health-exclusions.routing';

@NgModule({
  imports: [CommonModule, HealthExclusionsRoutingModule, MfSelectModule, MfLoaderModule],
  declarations: [HealthExclusionsComponent]
})
export class HealthExclusionsModule {}
