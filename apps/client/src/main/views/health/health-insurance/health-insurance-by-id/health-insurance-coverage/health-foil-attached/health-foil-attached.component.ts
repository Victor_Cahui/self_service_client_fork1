import { Component, HostBinding } from '@angular/core';
import { FOIL_ATTACHED } from '@mx/settings/lang/health.lang';

@Component({
  selector: 'client-health-foil-attached-component',
  templateUrl: './health-foil-attached.component.html'
})
export class HealthFoilAttachedComponent {
  @HostBinding('class') class = 'w-100';

  content = { title: FOIL_ATTACHED.title, content: '' };
}
