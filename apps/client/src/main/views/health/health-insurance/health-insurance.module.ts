import { NgModule } from '@angular/core';

import { HealthInsuranceRoutingModule } from './health-insurance.routing';

@NgModule({
  imports: [HealthInsuranceRoutingModule],
  declarations: []
})
export class HealthInsuranceModule {}
