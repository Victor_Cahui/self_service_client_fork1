import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfButtonModule } from '@mx/core/ui/lib/components/forms/button/button.module';
import { InsuredDependentEditStep3Component } from './insured-dependent-edit-step-3.component';
import { InsuredDependentEditStep3RoutingModule } from './insured-dependent-edit-step-3.routing';

@NgModule({
  imports: [CommonModule, MfButtonModule, InsuredDependentEditStep3RoutingModule],
  exports: [],
  declarations: [InsuredDependentEditStep3Component],
  providers: []
})
export class InsuredDependentEditStep3Module {}
