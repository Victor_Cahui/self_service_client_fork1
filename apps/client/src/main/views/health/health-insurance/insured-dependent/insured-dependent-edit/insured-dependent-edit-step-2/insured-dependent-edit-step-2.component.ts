import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { dateToStringFormatMMDDYYYY } from '@mx/core/shared/helpers/util/date';
import { MfModalMessageComponent } from '@mx/core/ui/lib/components/modals/modal-message/modal-message.component';
import { FileItem } from '@mx/core/ui/lib/directives/ng-drop/file-item.class';
import { IErrorEvent } from '@mx/core/ui/lib/directives/ng-drop/ng-drop-files.directive';
import { IChangesDataInsured, InsuredDependentEditService } from '@mx/services/health/insured-dependent-edit.service';
import { PoliciesService } from '@mx/services/policies.service';
import * as typeDocConstant from '@mx/settings/constants/types-documents';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { EditProfileLang } from '@mx/settings/lang/insured-dependent';
import ImageCompressor from 'image-compressor.js';
import * as JSZip from 'jszip';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-insured-dependent-edit-step-2-component',
  templateUrl: './insured-dependent-edit-step-2.component.html',
  styleUrls: ['./insured-dependent-edit-step-2.component.scss']
})
export class InsuredDependentEditStep2Component implements OnInit {
  @ViewChild(MfModalMessageComponent) modalMessage: MfModalMessageComponent;

  titleAlert: string;
  messageAlert: string;
  items: Array<IChangesDataInsured>;
  enterFront: boolean;
  enterReverse: boolean;
  enterPex: boolean;
  fileFront: FileItem;
  fileReverse: FileItem;
  filePex: FileItem;
  typeDocument: string;
  isPex: boolean;
  isSubmitting: boolean;
  maximumConfimationDaysPerRequest: number;
  maximumSizeFileMB: number;
  langtitle = EditProfileLang.step2.title;
  langmessagePhoto = EditProfileLang.step2.messagePhoto;
  langmessageDay1 = EditProfileLang.step2.messageDay1;
  langmessageDay2 = EditProfileLang.step2.messageDay2;
  langmessagePex = EditProfileLang.step2.messagePex;
  langmessageNPex = EditProfileLang.step2.messageNPex;
  langLFrontal = EditProfileLang.step2.LFrontal;
  langLReverse = EditProfileLang.step2.LReverse;
  langLAdd = EditProfileLang.step2.LAdd;
  langBCancel = EditProfileLang.step2.BCancel;
  langBSubmit1 = EditProfileLang.step2.BSubmit1;
  langBSubmit2 = EditProfileLang.step2.BSubmit2;
  typeDoc = typeDocConstant;
  formSub: Subscription;
  btnConfirm = GeneralLang.Buttons.Confirm;
  withOutInformation = GeneralLang.Texts.WithOutInformation;

  constructor(
    private readonly insuredDependentEditService: InsuredDependentEditService,
    private readonly policeService: PoliciesService,
    private readonly router: Router
  ) {
    this.items = [];
    this.enterFront = false;
    this.enterReverse = false;
    this.isPex = false;
    this.isSubmitting = false;
    this.maximumSizeFileMB = 20;
    this.titleAlert = '';
    this.messageAlert = '';
  }

  ngOnInit(): void {
    this.items = this.insuredDependentEditService.getChangesInInsured();
    const originDocument = this.insuredDependentEditService.getPolicyInsuredResponse();
    const newDocument = this.items.find((obj: IChangesDataInsured) => obj.key === 'typeDocument');
    this.typeDocument = !!newDocument
      ? newDocument.currentValue
      : !!originDocument
      ? originDocument.tipoDocumento
      : void 0;
    this.maximumConfimationDaysPerRequest = this.insuredDependentEditService.getMaxDay();
    this.maximumSizeFileMB = this.insuredDependentEditService.getMaxSize();
  }

  onErrorEvent($event: IErrorEvent): void {
    this.titleAlert = GeneralLang.Messages.Fail;
    this.messageAlert = $event.message;
    this.modalMessage.open();
  }

  sendData(key: string): void {
    this.isSubmitting = true;
    switch (key) {
      case this.typeDoc.TYPE_DOCUMENT_RUC:
        this.updateData();
        break;
      case this.typeDoc.TYPE_DOCUMENT_PEX:
        this.updateData([this.compressImage(this.filePex)]);
        break;
      default:
        this.updateData([this.compressImage(this.fileFront), this.compressImage(this.fileReverse)]);
        break;
    }
  }

  updateData(arr?: Array<Promise<File>>): void {
    if (!!arr) {
      Promise.all([...arr]).then((files: Array<File>) => {
        this.compressZip(files)
          .then((blob: Blob) => {
            const objZip = new File([blob], 'fotosDocumento.zip');
            const data = this.insuredDependentEditService.getChangesObjectInsured();
            data.fotosDocumento = objZip;
            this.formSub = this.policeService.editInsured(data).subscribe(
              (res: any) => {
                this.isSubmitting = false;
                this.formSub.unsubscribe();
                this.insuredDependentEditService.setSuccess(true);
                this.router.navigate(['/health/health-insurance/insured-dependent/edit/3']);
              },
              (err: any) => {
                this.isSubmitting = false;
                this.titleAlert = GeneralLang.Messages.Fail;
                this.messageAlert = GeneralLang.Messages.ErrorUpdateAndAdd;
                this.modalMessage.open();
                this.formSub.unsubscribe();
              }
            );
          })
          .catch((err: any) => {
            this.isSubmitting = false;
            this.titleAlert = GeneralLang.Messages.Fail;
            this.messageAlert = GeneralLang.Messages.ErrorUpdateAndAdd;
            this.modalMessage.open();
          });
      });
    } else {
      const data = this.insuredDependentEditService.getChangesObjectInsured();
      this.formSub = this.policeService.editInsured(data).subscribe((res: any) => {
        this.isSubmitting = false;
        this.insuredDependentEditService.setSuccess(true);
        this.router.navigate(['/health/health-insurance/insured-dependent/edit/3']);
        this.formSub.unsubscribe();
      });
    }
  }

  compressZip(files: Array<File>): Promise<Blob> {
    const jsZip = new JSZip();
    for (const file of files) {
      jsZip.file(file.name, file);
    }

    return jsZip.generateAsync({
      type: 'blob',
      compression: 'DEFLATE',
      compressionOptions: {
        level: 9 // force a compression and a compression level for this file
      }
    });
  }

  compressImage(file: FileItem, name?: string): Promise<File> {
    const imageCompressor = new ImageCompressor();

    return new Promise((resolve, reject) => {
      imageCompressor
        .compress(file.file, {
          quality: 0.6
        })
        .then((value: Blob) => {
          const newFile = !name
            ? new File([value], file.name)
            : new File([value], `${name}.${file.name.split('.')[file.name.split('.').length - 1]}`);
          resolve(newFile);
        });
    });
  }

  eventEnter($event: boolean, name: string): void {
    switch (name) {
      case 'frontal':
        this.enterFront = $event;
        this.enterReverse = false;
        this.enterPex = false;
        break;
      case 'reverso':
        this.enterFront = false;
        this.enterReverse = $event;
        this.enterPex = false;
        break;
      case this.typeDoc.TYPE_DOCUMENT_PEX:
        this.enterFront = false;
        this.enterReverse = false;
        this.enterPex = $event;
        break;
      default:
        break;
    }
  }

  getImages($event: Array<FileItem>, name: string): void {
    if ($event.length > 0) {
      switch (name) {
        case 'frontal':
          if (this.fileReverse && this.fileReverse.name === $event[0].name) {
            this.showErrorDuplicateImagen();
          } else {
            this.fileFront = $event[0];
          }
          break;
        case 'reverso':
          if (this.fileFront && this.fileFront.name === $event[0].name) {
            this.showErrorDuplicateImagen();
          } else {
            this.fileReverse = $event[0];
          }
          break;
        case this.typeDoc.TYPE_DOCUMENT_PEX:
          this.filePex = $event[0];
          break;
        default:
          break;
      }
    }
  }

  showErrorDuplicateImagen(): void {
    this.titleAlert = GeneralLang.Messages.Fail;
    this.messageAlert = GeneralLang.Alert.errorDuplicateImage.error.message;
    this.modalMessage.open();
  }

  goToStep1(): void {
    this.router.navigate(['/health/health-insurance/insured-dependent/edit/1']);
  }

  trackByFn(index, item): any {
    return index;
  }

  // Valor a mostrar del item
  valueItemOld(item): string {
    return item.key === 'relation'
      ? item.oldValue.name
      : (item.key.startsWith('date') ? dateToStringFormatMMDDYYYY(item.oldValue) : item.oldValue) ||
          this.withOutInformation;
  }

  valueItemNew(item): string {
    return item.key === 'relation'
      ? item.currentValue.name
      : (item.key.startsWith('date') ? dateToStringFormatMMDDYYYY(item.currentValue) : item.currentValue) ||
          this.withOutInformation;
  }
}
