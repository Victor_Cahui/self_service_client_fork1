import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InsureDependentEditDeactivateGuard } from '@mx/guards/insure-dependent-edit-deactivate.guard';
import { InsureDependentEditGuard } from '@mx/guards/insure-dependent-edit.guard';
import { InsureDependentEditGuardStep12 } from '@mx/guards/insure-dependent-edit.guard-step-12.guard';
import { InsureDependentEditGuardStep2 } from '@mx/guards/insure-dependent-edit.guard-step-2.guard';
import { InsureDependentEditGuardStep3 } from '@mx/guards/insure-dependent-edit.guard-step-3.guard';
import { InsuredDependentEditComponent } from './insured-dependent-edit.component';

const routes: Routes = [
  {
    path: '',
    component: InsuredDependentEditComponent,
    canActivate: [InsureDependentEditGuard],
    canDeactivate: [InsureDependentEditDeactivateGuard],
    children: [
      {
        path: '',
        redirectTo: '1',
        pathMatch: 'prefix'
      },
      {
        path: '1',
        loadChildren:
          './insured-dependent-edit-step-1/insured-dependent-edit-step-1.module#InsuredDependentEditStep1Module',
        canActivate: [InsureDependentEditGuardStep12]
      },
      {
        path: '2',
        loadChildren:
          './insured-dependent-edit-step-2/insured-dependent-edit-step-2.module#InsuredDependentEditStep2Module',
        canActivate: [InsureDependentEditGuardStep12, InsureDependentEditGuardStep2]
      },
      {
        path: '3',
        loadChildren:
          './insured-dependent-edit-step-3/insured-dependent-edit-step-3.module#InsuredDependentEditStep3Module',
        canActivate: [InsureDependentEditGuardStep3]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InsuredDependentEditRoutingModule {}
