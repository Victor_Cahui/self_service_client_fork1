import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { MfCustomAlertModule } from '@mx/core/ui/lib/components/alerts/custom-alert';
import { MfButtonModule } from '@mx/core/ui/lib/components/forms/button/button.module';
import { MfInputDatepickerModule } from '@mx/core/ui/lib/components/forms/input-datepicker/input-datepicker.module';
import { MfInputModule } from '@mx/core/ui/lib/components/forms/input/input.module';
import { MfSelectModule } from '@mx/core/ui/lib/components/forms/select/select.module';
import { MfLoaderModule } from '@mx/core/ui/lib/components/global/loader';
import { MfShowErrorsModule } from '@mx/core/ui/lib/components/global/show-errors/show-errors.module';
import { MfModalAlertModule } from '@mx/core/ui/lib/components/modals/modal-alert/modal-alert.module';
import { MfModalMessageModule } from '@mx/core/ui/lib/components/modals/modal-message/modal-message.module';
import { MfModalModule } from '@mx/core/ui/lib/components/modals/modal/modal.module';
import { InsuredDependentEditStep1Component } from './insured-dependent-edit-step-1.component';
import { InsuredDependentEditStep1RoutingModule } from './insured-dependent-edit-step-1.routing';

@NgModule({
  imports: [
    CommonModule,
    GoogleModule,
    ReactiveFormsModule,
    MfInputModule,
    MfInputDatepickerModule,
    MfShowErrorsModule,
    MfSelectModule,
    MfInputModule,
    MfModalModule,
    MfModalAlertModule,
    MfModalMessageModule,
    MfLoaderModule,
    MfButtonModule,
    InsuredDependentEditStep1RoutingModule,
    MfCustomAlertModule
  ],
  exports: [],
  declarations: [InsuredDependentEditStep1Component],
  providers: []
})
export class InsuredDependentEditStep1Module {}
