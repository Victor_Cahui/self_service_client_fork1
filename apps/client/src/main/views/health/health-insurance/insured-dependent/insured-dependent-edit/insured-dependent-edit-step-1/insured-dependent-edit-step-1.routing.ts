import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InsuredDependentEditStep1Component } from './insured-dependent-edit-step-1.component';

const routes: Routes = [
  {
    path: '',
    component: InsuredDependentEditStep1Component
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InsuredDependentEditStep1RoutingModule {}
