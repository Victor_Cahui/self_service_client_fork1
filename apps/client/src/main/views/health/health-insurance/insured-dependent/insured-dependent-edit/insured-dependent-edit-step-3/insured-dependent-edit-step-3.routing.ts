import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InsuredDependentEditStep3Component } from './insured-dependent-edit-step-3.component';

const routes: Routes = [
  {
    path: '',
    component: InsuredDependentEditStep3Component
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InsuredDependentEditStep3RoutingModule {}
