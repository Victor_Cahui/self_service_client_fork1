import { Component, OnInit } from '@angular/core';
import { CanComponentDeactivate } from '@mx/guards/insure-dependent-edit-deactivate.guard';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { InsuredDependentEditService } from '@mx/services/health/insured-dependent-edit.service';
import { HEALTH_INSURANCE_EDIT } from '@mx/settings/constants/router-titles';
import { Observable } from 'rxjs';

@Component({
  selector: 'client-insured-dependent-edit-component',
  template: '<router-outlet></router-outlet>'
})
export class InsuredDependentEditComponent implements OnInit, CanComponentDeactivate {
  constructor(
    private readonly headerHelperService: HeaderHelperService,
    private readonly insuredDependentEditService: InsuredDependentEditService
  ) {}

  ngOnInit(): void {
    this.headerHelperService.set({
      title: HEALTH_INSURANCE_EDIT.title,
      subTitle: this.insuredDependentEditService.namePolice,
      icon: '',
      back: true
    });
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    this.insuredDependentEditService.reset();
    this.insuredDependentEditService.removeLocalStorage();

    return true;
  }
}
