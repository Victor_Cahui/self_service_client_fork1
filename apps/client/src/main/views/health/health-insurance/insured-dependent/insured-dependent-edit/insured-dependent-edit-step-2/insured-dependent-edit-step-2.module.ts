import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfModalAlertModule } from '@mx/core/ui/lib/components/modals/modal-alert/modal-alert.module';
import { MfModalMessageModule } from '@mx/core/ui/lib/components/modals/modal-message/modal-message.module';
import { MfModalModule } from '@mx/core/ui/lib/components/modals/modal/modal.module';
import { NgDropModule } from '@mx/core/ui/lib/directives/ng-drop/ng-drop.module';
import { InsuredDependentEditStep2Component } from './insured-dependent-edit-step-2.component';
import { InsuredDependentEditStep2RoutingModule } from './insured-dependent-edit-step-2.routing';

@NgModule({
  imports: [
    CommonModule,
    InsuredDependentEditStep2RoutingModule,
    NgDropModule,
    MfModalModule,
    MfModalAlertModule,
    MfModalMessageModule
  ],
  exports: [],
  declarations: [InsuredDependentEditStep2Component],
  providers: []
})
export class InsuredDependentEditStep2Module {}
