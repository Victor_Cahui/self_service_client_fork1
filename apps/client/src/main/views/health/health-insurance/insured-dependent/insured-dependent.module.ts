import { NgModule } from '@angular/core';
import { InsuredDependentRoutingModule } from './insured-dependent.routing';

@NgModule({
  imports: [InsuredDependentRoutingModule],
  exports: [],
  declarations: [],
  providers: []
})
export class InsuredDependentModule {}
