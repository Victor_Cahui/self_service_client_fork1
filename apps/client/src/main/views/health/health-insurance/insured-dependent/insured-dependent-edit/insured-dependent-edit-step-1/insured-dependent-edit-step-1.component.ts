import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FormComponent } from '@mx/components/shared/utils/form';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { NotificationStatus } from '@mx/core/ui/lib/components/alerts/custom-alert/custom-alert.component';
import { SelectListItem } from '@mx/core/ui/lib/components/forms/select/select-item';
import { MfModalMessageComponent } from '@mx/core/ui/lib/components/modals/modal-message/modal-message.component';
import { EmailValidator } from '@mx/core/ui/lib/validators/validators';
import { AuthService } from '@mx/services/auth/auth.service';
import { GeneralService } from '@mx/services/general/general.service';
import { IChangesDataInsured, InsuredDependentEditService } from '@mx/services/health/insured-dependent-edit.service';
import { PoliciesService } from '@mx/services/policies.service';
import { MFP_Modificar_Datos_Asegurado_29A } from '@mx/settings/constants/events.analytics';
import { GENERAL_MAX_LENGTH, REG_EX } from '@mx/settings/constants/general-values';
import { MAX_CONFIRMATION_DAY_SENSIBLE_PROFILE, MAX_SIZE_MB_IMAGE_PROFILE } from '@mx/settings/constants/key-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { EditProfileLang } from '@mx/settings/lang/insured-dependent';
import { IParameterApp } from '@mx/statemanagement/models/general.models';
import { IPolicyInsuredEditDataRequest, IPolicyInsuredResponse } from '@mx/statemanagement/models/policy.interface';
import { Relation } from '@mx/statemanagement/models/realtion.interface';
import { IdDocument, TypeDocument } from '@mx/statemanagement/models/typedocument.interface';
import { isEmpty } from 'lodash-es';
import { Observable, Subscription, timer, zip } from 'rxjs';

@Component({
  selector: 'client-insured-dependent-edit-step-1-component',
  templateUrl: './insured-dependent-edit-step-1.component.html'
})
export class InsuredDependentEditStep1Component implements OnInit, OnDestroy {
  @ViewChild(MfModalMessageComponent) modalMessage: MfModalMessageComponent;
  titleAlert: string;
  messageAlert: string;
  // Config init
  load: boolean;
  generalMaxLength = GENERAL_MAX_LENGTH;
  nameRegExp = REG_EX.name;
  lastnameRegExp = REG_EX.lastname;
  phoneRegExp = REG_EX.phone;
  numericRegExp = REG_EX.numeric;
  alphaNumericRegExp = REG_EX.alphaNumeric;
  objNameRegExp: RegExp;
  objLastnameRegExp: RegExp;
  objNumericeRegExp: RegExp;
  objAlphaNumericRegExp: RegExp;
  limitBirthDay: Date;
  // Init Component
  arrRelation: Array<SelectListItem>;
  arrTypesDocuments: Array<SelectListItem>;
  insuredSub: Subscription;
  formSub: Subscription;
  insured: IPolicyInsuredResponse;
  form: FormGroup;
  names: AbstractControl;
  lastNameFather: AbstractControl;
  lastNameMother: AbstractControl;
  dateOfBirth: AbstractControl;
  typeDocument: AbstractControl;
  numberDocument: AbstractControl;
  relation: AbstractControl;
  phone: AbstractControl;
  email: AbstractControl;
  // Services Data
  relations: Array<Relation>;
  typesDocuments: Array<TypeDocument>;
  maxLength: number;
  minLength: number;
  isNumber: boolean;
  isNumberLetter: boolean;
  changedNumberDocument: boolean;
  policeNumber: string;
  // Flags
  isSensible: boolean;
  showLoading: boolean;
  langTitle: string;
  langMessage: string;
  langFormLNames: string;
  langFormLLastNameFather: string;
  langFormLLastNameMother: string;
  langFormLDateOfBirth: string;
  langFormLTypeDocument: string;
  langFormLNumberDocument: string;
  langFormLLRelation: string;
  langFormLPhone: string;
  langFormLEmail: string;
  langFormBCancel: string;
  langFormBSubmit: string;
  btnConfirm = GeneralLang.Buttons.Confirm;
  entity: IdDocument;
  parametersSubject: Observable<Array<IParameterApp>>;

  generalLang = GeneralLang;
  gaCancel: IGaPropertie;
  gaSubmit: IGaPropertie;
  warningStatus = NotificationStatus.INFO;

  public arrayChangeData: any;
  public isSameDocumentNumber: boolean;
  public isChangeDocumentNumber: boolean;

  constructor(
    private readonly insuredDependentEditService: InsuredDependentEditService,
    private readonly generalService: GeneralService,
    private readonly policeService: PoliciesService,
    private readonly router: Router,
    private readonly fb: FormBuilder,
    private readonly authService: AuthService
  ) {
    this.titleAlert = '';
    this.messageAlert = '';
    this.load = false;
    const dateNow = new Date();
    this.limitBirthDay = new Date(dateNow.setFullYear(dateNow.getFullYear() - 18));
    this.arrRelation = [];
    this.arrTypesDocuments = [];
    this.changedNumberDocument = true;
    this.isSensible = false;
    // Regex
    this.objNameRegExp = new RegExp(this.nameRegExp);
    this.objLastnameRegExp = new RegExp(this.lastnameRegExp);
    this.objNumericeRegExp = new RegExp(this.numericRegExp);
    this.objAlphaNumericRegExp = new RegExp(this.alphaNumericRegExp);
    this.langTitle = EditProfileLang.step1.title;
    this.langMessage = EditProfileLang.step1.messageDependent;
    this.langFormLNames = EditProfileLang.step1.form.LNames;
    this.langFormLLastNameFather = EditProfileLang.step1.form.LLastNameFather;
    this.langFormLLastNameMother = EditProfileLang.step1.form.LLastNameMother;
    this.langFormLDateOfBirth = EditProfileLang.step1.form.LDateOfBirth;
    this.langFormLTypeDocument = EditProfileLang.step1.form.LTypeDocument;
    this.langFormLNumberDocument = EditProfileLang.step1.form.LNumberDocument;
    this.langFormLLRelation = EditProfileLang.step1.form.LRelation;
    this.langFormLPhone = EditProfileLang.step1.form.LPhoneOptional;
    this.langFormLEmail = EditProfileLang.step1.form.LEmail;
    this.langFormBCancel = EditProfileLang.step1.form.BCancel;
    this.langFormBSubmit = EditProfileLang.step1.form.BSubmit;

    this.gaCancel = MFP_Modificar_Datos_Asegurado_29A(this.langFormBCancel);
    this.gaSubmit = MFP_Modificar_Datos_Asegurado_29A(this.langFormBSubmit);
  }

  ngOnInit(): void {
    this.arrayChangeData = [];
    this.entity = this.authService.retrieveEntity();
    this.insuredDependentEditService.setSuccessStep1(false);
    this.policeNumber = this.insuredDependentEditService.numberPolice;
    this.insured = this.insuredDependentEditService.getPolicyInsuredResponse();
    const types$: Observable<Array<TypeDocument>> = this.getTypesDocuments();
    const relations$: Observable<Array<Relation>> = this.getRelations();
    if (
      this.insuredDependentEditService.getTypeDocuments().length === 0 ||
      this.insuredDependentEditService.getRelations().length === 0 ||
      this.insuredDependentEditService.getMaxDay() === null ||
      !this.insuredDependentEditService.getMaxSize() === null
    ) {
      this.setParameters();
      this.insuredSub = zip(types$, relations$).subscribe((res: any) => {
        this.arrTypesDocuments = this.setTypesDocumentsData(res[0]);
        this.arrRelation = this.setRelationData(res[1]);
        // Save in service
        this.insuredDependentEditService.setArr(res[0], res[1]);
        // End Save
        this.createForm(this.insured);
        // tslint:disable-next-line:max-line-length
        const typeDocument: TypeDocument = this.typesDocuments.find(
          (obj: TypeDocument) => obj.codigo === this.insured.tipoDocumento
        );
        this.setControlNumberDocument(typeDocument);
        this.load = true;
      });
    } else {
      this.arrTypesDocuments = this.setTypesDocumentsData(this.insuredDependentEditService.getTypeDocuments());
      this.arrRelation = this.setRelationData(this.insuredDependentEditService.getRelations());
      this.createForm(this.insured);
      // tslint:disable-next-line:max-line-length
      const typeDocument: TypeDocument = this.typesDocuments.find(
        (obj: TypeDocument) => obj.codigo === this.insured.tipoDocumento
      );
      this.setControlNumberDocument(typeDocument);
      this.load = true;
    }
  }

  ngOnDestroy(): void {
    if (!!this.insuredSub) {
      this.insuredSub.unsubscribe();
    }
  }

  onSubmit(): void {
    if (this.form.valid) {
      this.insuredDependentEditService.setChangesInInsured(
        this.seeChangedData().filter((obj: IChangesDataInsured) => obj.key !== 'phone' && obj.key !== 'email')
      );
      this.insuredDependentEditService.setSuccessStep1(true);
      if (this.hasSencibleDataModified()) {
        this.router.navigate(['/health/health-insurance/insured-dependent/edit/2']);
      } else {
        const data = this.insuredDependentEditService.getChangesObjectInsured();
        this.showLoading = true;
        this.formSub = this.policeService.editInsured(data).subscribe(
          (res: any) => {
            this.showLoading = false;
            this.insuredDependentEditService.setOpenModal(true);
            this.router.navigate([`/health/health-insurance/${this.policeNumber}/detail`]);
            this.formSub.unsubscribe();
          },
          () => {
            this.showLoading = false;
            this.formSub.unsubscribe();
          }
        );
      }
    } else {
      this.markAsError();
    }
  }

  hasSencibleDataModified(): boolean {
    const arr = this.seeChangedData();

    return arr.some(a => a.isSensible);
  }

  eventChangedDocument($event: any): void {
    this.changedNumberDocument = false;
    const typeDocument: TypeDocument = this.typesDocuments.find(
      (obj: TypeDocument) => obj.codigo === $event.target.value
    );
    this.setControlNumberDocument(typeDocument);
    this.setControl(this.numberDocument, '');
    const timer$ = timer(0).subscribe(() => {
      this.changedNumberDocument = true;
      timer$.unsubscribe();
    });
  }

  eventChangedNumberDocument($event: any): void {
    this.isChangeDocumentNumber = true;
    this.isSameDocumentNumber = this.numberDocument.value === this.insured.documento;
  }

  setControlNumberDocument(typeDocument: TypeDocument): void {
    this.minLength = typeDocument.longitudMinima;
    this.maxLength = typeDocument.longitudMaxima;

    const validators = FormComponent.getValidatorTypeDocument(typeDocument.codigo, this.typesDocuments);
    this.numberDocument.setValidators(validators);
  }

  createForm(insured: IPolicyInsuredResponse): void {
    const fn = insured.fechaNacimiento ? new Date(insured.fechaNacimiento.replace(/-/g, '/')) : void 0;
    this.form = this.fb.group({
      names: [{ value: insured.nombre, disabled: false }, Validators.required],
      lastNameFather: [{ value: insured.apellidoPaterno, disabled: false }, Validators.required],
      lastNameMother: [{ value: insured.apellidoMaterno, disabled: false }, Validators.required],
      dateOfBirth: [{ value: fn, disabled: false }, Validators.required],
      typeDocument: [{ value: insured.tipoDocumento, disabled: false }, Validators.required],
      numberDocument: [{ value: insured.documento, disabled: false }, Validators.required],
      relation: [{ value: insured.relacionId, disabled: false }, Validators.required],
      phone: [
        { value: insured.telefono, disabled: false },
        Validators.compose([Validators.min(9), Validators.pattern(this.phoneRegExp)])
      ],
      email: [{ value: insured.correoElectronico, disabled: false }, EmailValidator.isValid]
    });
    this.names = this.form.controls['names'];
    this.lastNameFather = this.form.controls['lastNameFather'];
    this.lastNameMother = this.form.controls['lastNameMother'];
    this.dateOfBirth = this.form.controls['dateOfBirth'];
    this.typeDocument = this.form.controls['typeDocument'];
    this.numberDocument = this.form.controls['numberDocument'];
    this.relation = this.form.controls['relation'];
    this.phone = this.form.controls['phone'];
    this.email = this.form.controls['email'];

    this.form.valueChanges.subscribe(r => {
      this.arrayChangeData = this.seeChangedData();
    });
  }

  markAsError(): void {
    this.markAsTouchedAndDirty(this.names);
    this.markAsTouchedAndDirty(this.lastNameFather);
    this.markAsTouchedAndDirty(this.lastNameMother);
    this.markAsTouchedAndDirty(this.dateOfBirth);
    this.markAsTouchedAndDirty(this.typeDocument);
    this.markAsTouchedAndDirty(this.numberDocument);
    this.markAsTouchedAndDirty(this.relation);
    this.markAsTouchedAndDirty(this.phone);
    this.markAsTouchedAndDirty(this.email);
  }

  markAsTouchedAndDirty(control: AbstractControl): void {
    control.markAsTouched();
  }

  showErrors(control: AbstractControl): boolean {
    return !!control && !isEmpty(control.errors) && (control.touched || control.dirty);
  }

  getRelations(): Observable<Array<Relation>> {
    return this.generalService.getTypesRelation();
  }

  getTypesDocuments(): Observable<Array<TypeDocument>> {
    return this.generalService.getDocumentTypes();
  }

  setParameters(): void {
    this.parametersSubject = this.generalService.getParametersSubject();
    this.parametersSubject.subscribe(params => {
      const maximumConfimationDaysPerRequest =
        Number(this.generalService.getValueParams(MAX_CONFIRMATION_DAY_SENSIBLE_PROFILE)) || 0;
      const maximumSizeFileMB = Number(this.generalService.getValueParams(MAX_SIZE_MB_IMAGE_PROFILE)) || 0;
      this.insuredDependentEditService.setParameters(maximumConfimationDaysPerRequest, maximumSizeFileMB);
    });
  }

  setRelationData(relations: Array<Relation>): Array<SelectListItem> {
    this.relations = relations;
    const arr = relations.map(
      (obj: Relation) =>
        new SelectListItem({
          text: obj.relacionDescripcion,
          value: obj.relacionId
        })
    );

    return [...arr];
  }

  setTypesDocumentsData(typesDocuments: Array<TypeDocument>): Array<SelectListItem> {
    this.typesDocuments = typesDocuments;
    const arr = typesDocuments.map(
      (obj: TypeDocument) =>
        new SelectListItem({
          text: obj.codigo,
          value: obj.codigo
        })
    );

    return [...arr];
  }

  setControl(control: AbstractControl, value: any): void {
    if (!!value) {
      control.setValue(value);
    } else {
      control.setValue('');
      control.markAsPristine();
      control.markAsUntouched();
    }
  }

  cleanString(text: string): string {
    const newText = text.trim().replace(/ +/g, ' ');

    return newText;
  }

  seeChangedData(): Array<IChangesDataInsured> {
    let arr = [];
    const objSendData: IPolicyInsuredEditDataRequest = {
      codigoApp: this.insuredDependentEditService.appCode,
      numeroPoliza: this.insuredDependentEditService.numberPolice,
      tipoDocumento: this.insured.tipoDocumento,
      documento: this.insured.documento
    };
    const names = this.cleanString(this.names.value);
    if (names !== this.insured.nombre) {
      this.isSensible = true;
      arr = arr.concat([
        {
          key: 'name',
          currentValue: names,
          oldValue: this.insured.nombre,
          label: 'Nombres',
          isSensible: true
        }
      ]);
      objSendData.nombre = names;
    }
    const lastNameFather = this.cleanString(this.lastNameFather.value);
    if (lastNameFather !== this.insured.apellidoPaterno) {
      this.isSensible = true;
      arr = arr.concat([
        {
          key: 'lastNameFather',
          currentValue: lastNameFather,
          oldValue: this.insured.apellidoPaterno,
          label: 'Apellido paterno',
          isSensible: true
        }
      ]);
      objSendData.apellidoPaterno = lastNameFather;
    }
    const lastNameMother = this.cleanString(this.lastNameMother.value);
    if (lastNameMother !== this.insured.apellidoMaterno) {
      this.isSensible = true;
      arr = arr.concat([
        {
          key: 'lastNameMother',
          currentValue: lastNameMother,
          oldValue: this.insured.apellidoMaterno,
          label: 'Apellido materno',
          isSensible: true
        }
      ]);
      objSendData.apellidoMaterno = lastNameMother;
    }
    if (this.insured.fechaNacimiento !== this.getFormattedDate(this.dateOfBirth.value)) {
      this.isSensible = true;
      arr = arr.concat([
        {
          key: 'dateOfBirth',
          currentValue: this.getFormattedDate(this.dateOfBirth.value),
          oldValue: this.insured.fechaNacimiento,
          label: 'Fecha de nacimiento',
          isSensible: true
        }
      ]);
      objSendData.fechaNacimiento = this.getFormattedDate(this.dateOfBirth.value);
    }
    if (this.typeDocument.value !== this.insured.tipoDocumento) {
      this.isSensible = true;
      arr = arr.concat([
        {
          key: 'typeDocument',
          currentValue: this.typeDocument.value,
          oldValue: this.insured.tipoDocumento,
          label: 'Tipo de documento',
          isSensible: true
        }
      ]);
      objSendData.tipoDocumentoModificado = this.typeDocument.value;
    }
    if (this.numberDocument.value !== this.insured.documento) {
      this.isSensible = true;
      arr = arr.concat([
        {
          key: 'numberDocument',
          currentValue: this.numberDocument.value,
          oldValue: this.insured.documento,
          label: 'Número de documento',
          isSensible: true
        }
      ]);
      objSendData.numeroDocumentoModificado = this.numberDocument.value;
    }
    if (parseInt(this.relation.value, null) !== this.insured.relacionId) {
      this.isSensible = true;
      arr = arr.concat([
        {
          key: 'relation',
          currentValue: {
            key: parseInt(this.relation.value, null),
            name: this.relations.find(obj => obj.relacionId === parseInt(this.relation.value, null)).relacionDescripcion
          },
          oldValue: {
            key: this.insured.relacionId,
            name: this.relations.find(obj => obj.relacionId === this.insured.relacionId).relacionDescripcion
          },
          label: 'Relación',
          isSensible: true
        }
      ]);
      objSendData.relacionId = this.relation.value;
    }
    if (this.phone.value !== this.insured.telefono) {
      arr = arr.concat([
        {
          key: 'phone',
          currentValue: this.phone.value,
          oldValue: this.insured.telefono,
          label: 'Teléfono móvil'
        }
      ]);
      objSendData.telefonoMovil = this.phone.value;
    }
    if (this.email.value !== this.insured.correoElectronico) {
      arr = arr.concat([
        {
          key: 'email',
          currentValue: this.email.value,
          oldValue: this.insured.correoElectronico,
          label: 'Correo electrónico'
        }
      ]);
      objSendData.correoElectronico = this.email.value;
    }
    const newBuffInsured: IPolicyInsuredResponse = { ...this.insured };
    newBuffInsured.nombre = names;
    newBuffInsured.apellidoPaterno = lastNameFather;
    newBuffInsured.apellidoMaterno = lastNameMother;
    newBuffInsured.fechaNacimiento = this.getFormattedDate(this.dateOfBirth.value);
    newBuffInsured.tipoDocumento = this.typeDocument.value;
    newBuffInsured.documento = this.numberDocument.value;
    newBuffInsured.relacionId = this.relation.value;
    newBuffInsured.telefono = this.phone.value;
    newBuffInsured.correoElectronico = this.email.value;
    this.insuredDependentEditService.setBuffInsured({ ...newBuffInsured });
    this.insuredDependentEditService.setChangesObjectInsured(objSendData);

    return [...arr];
  }

  cancel(): void {
    this.router.navigate([`/health/health-insurance/${this.policeNumber}/detail`]);
  }

  onKeydown(event, type: string): void {
    const listAllowKeys = [
      'Delete',
      'Backspace',
      'Tab',
      'Shift',
      'Alt',
      'ArrowUp',
      'ArrowDown',
      'ArrowRight',
      'ArrowLeft',
      'CapsLock'
    ];
    if (listAllowKeys.includes(event.key)) {
      return;
    }
    if (/^name$/i.test(type)) {
      if (!this.objNameRegExp.test(event.key)) {
        event.preventDefault();
      }
    } else if (/^lastname$/i.test(type)) {
      if (!this.objLastnameRegExp.test(event.key)) {
        event.preventDefault();
      }
    } else if (/^numeric$/i.test(type)) {
      if (!this.objNumericeRegExp.test(event.key)) {
        event.preventDefault();
      }
    } else if (/^alphanumeric$/i.test(type)) {
      if (!this.objAlphaNumericRegExp.test(event.key)) {
        event.preventDefault();
      }
    }
  }

  getFormattedDate(date: Date): string {
    const year = date.getFullYear();
    let month = (date.getMonth() + 1).toString();
    month = month.length > 1 ? month : `0${month}`;

    let day = date.getDate().toString();
    day = day.length > 1 ? day : `0${day}`;

    return `${year}-${month}-${day}`;
  }
  // tslint:disable-next-line: max-file-line-count
}
