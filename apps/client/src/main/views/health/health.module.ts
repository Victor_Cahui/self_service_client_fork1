import { NgModule } from '@angular/core';

import { CardMyPoliciesByTypeModule } from '@mx/components/card-my-policies/card-my-policies-by-type/card-my-policies-by-type.module';
import { CardWhatYouWantToDoModule } from '@mx/components/card-what-you-want-to-do/card-what-you-want-to-do.module';
import { HealthRoutingModule } from './health-routing.module';

@NgModule({
  imports: [HealthRoutingModule, CardMyPoliciesByTypeModule, CardWhatYouWantToDoModule],
  declarations: []
})
export class HealthModule {}
