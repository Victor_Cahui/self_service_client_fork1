import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HealthRefundDetailComponent } from './health-refund-detail.component';

const routes: Routes = [
  {
    path: '',
    component: HealthRefundDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HealthRefundDetailRouting {}
