import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HealthRefundTrayComponent } from './health-refund-tray.component';

const routes: Routes = [
  {
    path: '',
    component: HealthRefundTrayComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HealthRefundTrayRouting {}
