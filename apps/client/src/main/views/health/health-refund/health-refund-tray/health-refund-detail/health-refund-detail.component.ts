import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModalDownLoadComponent } from '@mx/components/shared/modals/modal-download/modal-download.component';
import { CARD_STATUS_REFUND, KEY_STATUS_REFUND } from '@mx/components/shared/timeline/components/timeline-config';
import { FileUtil } from '@mx/core/shared/helpers/util/file';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { HealthRefundService } from '@mx/services/health/health-refund.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { COVERAGES_ICON, DOWNLOAD_ICON, PROFILE_ICON } from '@mx/settings/constants/images-values';
import { HEALTH_REFUND_HEADER } from '@mx/settings/constants/router-titles';
import { REFUND_HEALT_LANG } from '@mx/settings/lang/health.lang';
import {
  IDownLoadDocumentRequest,
  IDownLoadDocumentResponse,
  IRefundDetailRequest
} from '@mx/statemanagement/models/health.interface';
import { IOutputEvent, ItemTimeline } from '@mx/statemanagement/models/timeline.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-health-refund-detail',
  templateUrl: './health-refund-detail.component.html'
})
export class HealthRefundDetailComponent implements OnInit {
  lang = REFUND_HEALT_LANG.RequestList;
  title: string;
  titleHeader: string;
  subtitleHeader: string;
  requestNumber: string;
  cards = CARD_STATUS_REFUND;
  keys = KEY_STATUS_REFUND;
  showLoading: boolean;
  refundSub: Subscription;
  downLoadSub: Subscription;
  itemList: Array<ItemTimeline> = [];
  fileUtil = new FileUtil();

  constructor(
    private readonly headerHelperService: HeaderHelperService,
    private readonly activePath: ActivatedRoute,
    private readonly healthRefundService: HealthRefundService
  ) {}

  ngOnInit(): void {
    this.title = this.lang.DetailTitle;
    this.titleHeader = HEALTH_REFUND_HEADER.title;
    this.subtitleHeader = this.lang.RequestLabel.toUpperCase();
    this.requestNumber = this.activePath.snapshot.params.number;
    const header = {
      title: `${this.subtitleHeader}: ${this.requestNumber}`,
      subTitle: this.titleHeader,
      back: true
    };
    this.headerHelperService.set(header);
    this.getData();
  }

  getData(): void {
    this.showLoading = true;
    const params: IRefundDetailRequest = {
      codigoApp: APPLICATION_CODE,
      numeroSolicitud: parseInt(this.requestNumber, 10)
    };
    this.refundSub = this.healthRefundService.getRefundRequestDetail(params).subscribe(
      cardList => {
        if (cardList) {
          // Configurar Cards
          cardList.forEach(cardData => {
            let item = null;
            const status = cardData.tipo;
            switch (status) {
              case this.keys.REQUEST:
                item = this.setRequest(status, cardData);
                break;
              case this.keys.REVIEW:
                item = this.setReview(status, cardData);
                break;
              case this.keys.FOR_ANSWER:
                item = this.setForAnswer(status, cardData);
                break;
              case this.keys.REFUSED:
                item = this.setRefused(status, cardData);
                break;
              case this.keys.APPROVED:
                item = this.setApproved(status, cardData);
                break;
              default:
                break;
            }
            if (item) {
              this.itemList.push(item);
            }
          });
        }
        this.showLoading = false;
      },
      () => {
        this.showLoading = false;
      }
    );
  }

  // Solicitado
  setRequest(key: string, cardData: any): ItemTimeline {
    const cardConfig = this.cards[key];
    const data: ItemTimeline = {
      date: cardData.fecha || '',
      title: cardConfig.title,
      status: key
    };

    if (this.hasData(cardData) && cardData.informacion) {
      data.card = cardConfig.card;
      const items = [];

      if (cardData.informacion.nombreBeneficiario) {
        items.push({
          text: `${this.lang.PatientLabel}: ${cardData.informacion.nombreBeneficiario}`,
          icon: PROFILE_ICON
        });
      }
      if (cardData.informacion.nombreBeneficio) {
        items.push({
          text: cardData.informacion.nombreBeneficio,
          icon: COVERAGES_ICON
        });
      }
      if (items.length) {
        data.content = { itemList: items };
      }

      if (cardData && cardData.descargas) {
        data.footer = cardConfig.footer;
        if (!data.content) {
          data.content = {};
        }
        data.content.action = {
          text: this.lang.DownLoadRequest,
          icon: DOWNLOAD_ICON,
          fileList: cardData.descargas
        };
      }
    }

    return data;
  }

  // En revision
  setReview(key: string, cardData: any): ItemTimeline {
    const cardConfig = this.cards[key];
    const data: ItemTimeline = {
      date: cardData.fecha || '',
      title: cardConfig.title,
      status: key,
      rigth: true
    };

    return data;
  }

  // Por responder
  setForAnswer(key: string, cardData: any): ItemTimeline {
    const cardConfig = this.cards[key];
    const data: ItemTimeline = {
      date: cardData.fecha || '',
      title: cardConfig.title,
      status: key,
      estimated: true
    };

    return data;
  }

  // Rechazado
  setRefused(key: string, cardData: any): ItemTimeline {
    const cardConfig = this.cards[key];
    const data: ItemTimeline = {
      date: cardData.fecha || '',
      title: cardConfig.title,
      status: key,
      finish: true,
      card: cardConfig.card,
      content: {
        content: this.lang.ForInformation || ''
      }
    };

    return data;
  }

  // Aprobado
  setApproved(key: string, cardData: any): ItemTimeline {
    const cardConfig = this.cards[key];
    const data: ItemTimeline = {
      date: cardData.fecha || '',
      title: cardConfig.title,
      status: key,
      card: cardConfig.card,
      content: {
        content: this.lang.ContactAgent || ''
      }
    };

    return data;
  }

  // Verifica si tiene data y el objeto viene lleno
  hasData(cardData): boolean {
    return cardData && !!Object.keys(cardData).length;
  }

  // Descargar archivos
  downloadFile(dataCard: IOutputEvent): void {
    if (!dataCard.data) {
      return;
    }
    const id = dataCard.data.identificadorDocumento;
    this.showLoaderFile(id, true);
    const requestNumber = parseInt(this.requestNumber, 10);
    const params: IDownLoadDocumentRequest = {
      codigoApp: APPLICATION_CODE,
      numeroSolicitud: requestNumber,
      identificadorDocumento: id
    };
    this.downLoadSub = this.healthRefundService.downLoadDocument(params).subscribe(
      (response: IDownLoadDocumentResponse) => {
        if (response && response.documento) {
          const nameSplit = dataCard.data.nombreDocumento.split('.');
          const name = nameSplit[0] || `${requestNumber}_${id}`;
          const ext = nameSplit[1] || 'pdf';
          this.fileUtil.download(response.documento, ext, name);
        }
        this.showLoaderFile(id, false);
      },
      () => {
        this.showLoaderFile(id, false);
      }
    );
  }

  // Update loading
  showLoaderFile(id: number, show: boolean): void {
    ModalDownLoadComponent.updateView.next({ identificadorDocumento: id, loading: show });
  }
}
