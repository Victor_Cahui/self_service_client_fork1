import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ViewTimelineModule } from '@mx/components/shared/timeline/view-timeline/view-timeline.module';
import { HealthRefundDetailComponent } from './health-refund-detail.component';
import { HealthRefundDetailRouting } from './health-refund-detail.routing';

@NgModule({
  imports: [CommonModule, ViewTimelineModule, HealthRefundDetailRouting],
  declarations: [HealthRefundDetailComponent]
})
export class HealthRefundDetailModule {}
