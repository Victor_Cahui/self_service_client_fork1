import { CurrencyPipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { KEY_STATUS_REFUND } from '@mx/components/shared/timeline/components/timeline-config';
import { getYear, isEstimatedDate } from '@mx/core/shared/helpers/util/date';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { AuthService } from '@mx/services/auth/auth.service';
import { GeneralService } from '@mx/services/general/general.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { HealthRefundService } from '@mx/services/health/health-refund.service';
import { APPLICATION_CODE, SOLES_SYMBOL_CURRENCY } from '@mx/settings/constants/general-values';
import { COIN_ICON, COVERAGES_ICON, PROFILE_ICON } from '@mx/settings/constants/images-values';
import { YEARS_ANTIQUITY_ASSIST_DEFAULT, YEARS_REFUND_HISTORY } from '@mx/settings/constants/key-values';
import { HEALTH_REFUND_HEADER } from '@mx/settings/constants/router-titles';
import { REFUND_HEALT_LANG } from '@mx/settings/lang/health.lang';
import {
  GeneralRequest,
  IConfigUICardViewItemList,
  IItemView,
  IParameterApp
} from '@mx/statemanagement/models/general.models';
import { IRefundRequestItemResponse } from '@mx/statemanagement/models/health.interface';
import { IRequestItemView, ITimelimeStep } from '@mx/statemanagement/models/timeline.interface';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'client-health-refund-tray',
  templateUrl: 'health-refund-tray.component.html',
  providers: [CurrencyPipe]
})
export class HealthRefundTrayComponent implements OnInit {
  refundSub: Subscription;
  parametersSub: Observable<Array<IParameterApp>>;
  itemsProcess: Array<any>;
  itemsHistory: Array<any>;
  showLoading: boolean;
  showNotification: boolean;
  yearsRange: number;
  lang = REFUND_HEALT_LANG.RequestList;
  noteText: string;
  noteTitle: string;
  params: GeneralRequest;
  configUI: IConfigUICardViewItemList;
  header = HEALTH_REFUND_HEADER;

  constructor(
    private readonly headerHelperService: HeaderHelperService,
    private readonly healthRefundService: HealthRefundService,
    private readonly authService: AuthService,
    protected generalService: GeneralService,
    private readonly router: Router,
    private readonly currencyPipe: CurrencyPipe
  ) {}

  ngOnInit(): void {
    this.configUI = { hasFilter: true };
    this.params = {
      codigoApp: APPLICATION_CODE
    };
    this.headerHelperService.set({
      subTitle: this.header.subTitle,
      title: this.header.title,
      icon: this.header.icon,
      back: false
    });
    this.noteText = this.lang.TracingMessage;
    this.getParameters();
    this.getItems();
    this.getRequestPending();
  }

  // Ver Detalle
  viewDetail(item: IRequestItemView): void {
    this.router.navigate([`health/refund/request/detail/${item.assistanceNumber}`]);
  }

  // Notificacion
  getRequestPending(): void {
    this.refundSub = this.healthRefundService.getRefundPendingRequest(this.params).subscribe(
      res => {
        this.showNotification = res && res.numeroSolicitudesPendientes > 0;
        if (this.showNotification) {
          const request = res.numeroSolicitudesPendientes;
          const note = request > 1 ? this.lang.PendingRequest : this.lang.OnePendingRequest;
          this.noteTitle = note.replace('{{number}}', request.toString());
        }
        this.refundSub.unsubscribe();
      },
      () => {
        this.refundSub.unsubscribe();
      },
      () => {}
    );
  }

  getParameters(): void {
    this.parametersSub = this.generalService.getParametersSubject();
    this.parametersSub.subscribe(() => {
      this.yearsRange =
        Number(this.generalService.getValueParams(YEARS_REFUND_HISTORY)) || YEARS_ANTIQUITY_ASSIST_DEFAULT;
    });
  }

  // Lista de Solicitudes
  getItems(): void {
    this.showLoading = true;
    this.refundSub = this.healthRefundService.getRefundRequestList(this.params).subscribe(
      res => {
        this.itemsHistory = this.formatHistory(res && (res.historial || []));
        this.itemsProcess = this.formatProcess(res && (res.enCurso || []));
        this.showLoading = false;
      },
      () => {
        this.itemsHistory = [];
        this.itemsProcess = [];
        this.showLoading = false;
      }
    );
  }

  // Configurar Vista
  formatHistory(histories: Array<IRefundRequestItemResponse>): Array<IRequestItemView> {
    let itemList = [];
    itemList = (histories || []).map(item => {
      const requestItem: IRequestItemView = {
        registerDate: item.fechaSolicitud,
        updateDate: '',
        assistanceNumber: item.numeroSolicitud,
        currentState: item.descripcion,
        year: getYear(item.fechaSolicitud),
        itemList: this.setItemLits(item),
        stateColor: item.estadoActual === KEY_STATUS_REFUND.REFUSED ? 'red' : ''
      };

      return requestItem;
    });

    return itemList;
  }

  formatProcess(processes: Array<IRefundRequestItemResponse>): Array<IRequestItemView> {
    // tslint:disable-next-line: no-parameter-reassignment
    processes = processes || [];

    return processes.map((item, index) => {
      const requestItem: IRequestItemView = {
        registerDate: item.fechaSolicitud,
        assistanceNumber: item.numeroSolicitud,
        currentState: item.descripcion,
        itemList: this.setItemLits(item),
        isCollapse: true,
        collapse: index > 0
      };

      requestItem.steps = item.estadosReembolso.map(estado => {
        const step: ITimelimeStep = {
          description: estado.descripcion,
          date: estado.fecha,
          complete: !!estado.fecha && !isEstimatedDate(estado.fecha)
        };

        return step;
      });

      return requestItem;
    });
  }

  setItemLits(item: IRefundRequestItemResponse): Array<IItemView> {
    const symbol = `${StringUtil.getMoneyDescription(item.codigoMoneda) || SOLES_SYMBOL_CURRENCY} `;

    return [
      {
        label: `${this.lang.PatientLabel}:`,
        text: item.nombreBeneficiario,
        icon: PROFILE_ICON
      },
      {
        label: item.nombreBeneficio,
        icon: COVERAGES_ICON
      },
      {
        label: `${this.lang.RefundLabel}:`,
        text: this.currencyPipe.transform(item.montoReembolsoAprobado, symbol) || '',
        icon: COIN_ICON
      }
    ];
  }
}
