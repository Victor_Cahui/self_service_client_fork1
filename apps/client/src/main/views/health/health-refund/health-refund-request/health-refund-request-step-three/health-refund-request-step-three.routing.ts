import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HealthRefundRequestStepThreeComponent } from './health-refund-request-step-three.component';

const routes: Routes = [
  {
    path: '',
    component: HealthRefundRequestStepThreeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HealthRefundRequestStepThreeRoutesModule {}
