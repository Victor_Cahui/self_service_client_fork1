import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HealthRefundRequestComponent } from './health-refund-request.component';

const routes: Routes = [
  {
    path: '',
    component: HealthRefundRequestComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HealthRefundRequestRoutingMoudule {}
