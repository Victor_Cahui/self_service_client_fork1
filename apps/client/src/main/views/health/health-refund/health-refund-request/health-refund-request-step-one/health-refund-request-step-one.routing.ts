import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HealthRefundRequestStepOneComponent } from './health-refund-request-step-one.component';

const routes: Routes = [
  {
    path: '',
    component: HealthRefundRequestStepOneComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HealthRefundRequestStepOneRoutesModule {}
