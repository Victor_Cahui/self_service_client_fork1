import { Component, OnInit } from '@angular/core';
import { ConfigurationBase } from '@mx/components/shared/configuration/configuration-base';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { MFP_Reembolsos_Solicitud_32B } from '@mx/settings/constants/events.analytics';
import { POLICY_TYPES, STATIC_BANNERS } from '@mx/settings/constants/policy-values';
import { HEALTH_REFUND_HEADER } from '@mx/settings/constants/router-titles';
import * as source from '@mx/settings/lang/health.lang';
import { IPathImageSize } from '@mx/statemanagement/models/general.models';

@Component({
  selector: 'client-health-refund-request',
  templateUrl: 'health-refund-request.component.html'
})
export class HealthRefundRequestComponent extends ConfigurationBase implements OnInit {
  staticBannersRefund: string;
  header = HEALTH_REFUND_HEADER;
  titleQuote = source.HealthLang.Titles.Cotizar;
  btnQuote = source.HealthLang.Links.Send;
  textQuote = source.HealthLang.Texts.Cotizar;

  pathImgBanner: IPathImageSize;

  howRefundWorks;
  defaultDaysForRequestRefund = '30';

  ga: Array<IGaPropertie>;

  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService,
    private readonly headerHelperService: HeaderHelperService
  ) {
    super(clientService, policiesInfoService, configurationService, authService);
    this.ga = [MFP_Reembolsos_Solicitud_32B()];
  }

  ngOnInit(): void {
    this.howRefundWorks = this._replaceBodyText(this.defaultDaysForRequestRefund);
    this.policyType = POLICY_TYPES.MD_SALUD.code;
    this.staticBannersRefund = POLICY_TYPES[this.policyType].staticBannersRefund;
    this.pathImgBanner = {
      sm: this.staticBannersRefund + STATIC_BANNERS.sm,
      lg: this.staticBannersRefund + STATIC_BANNERS.lg,
      xl: this.staticBannersRefund + STATIC_BANNERS.xl
    };
    this.headerHelperService.set({
      subTitle: this.header.subTitle,
      title: this.header.title,
      icon: this.header.icon,
      back: false
    });
  }

  changedPolicy(policy: any): void {
    this.howRefundWorks = this._replaceBodyText(policy.nroDiasSolReembolso);
  }

  eventQuote(): void {}
  private _replaceBodyText(days = this.defaultDaysForRequestRefund): any {
    return {
      ...source.HOW_REFUND_WORKS,
      body: {
        text: source.HOW_REFUND_WORKS.body.text.replace('{days}', days)
      }
    };
  }
}
