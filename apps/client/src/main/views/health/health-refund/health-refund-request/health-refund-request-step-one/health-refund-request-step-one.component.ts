import { DecimalPipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HealthSearchProviderComponent } from '@mx/components/health/health-search-provider/health-search-provider.component';
import { FormComponent } from '@mx/components/shared/utils/form';
import { dateToStringFormatYYYYMMDD } from '@mx/core/shared/helpers/util/date';
import { AmountValidator, SelectListItem } from '@mx/core/ui';
import { MfInputComponent } from '@mx/core/ui/lib/components/forms/input/input.component';
import { AuthService } from '@mx/services/auth/auth.service';
import { HealthRefundService } from '@mx/services/health/health-refund.service';
import { APPLICATION_CODE, SOLES_SYMBOL_CURRENCY } from '@mx/settings/constants/general-values';
import { CROSS_ICON } from '@mx/settings/constants/images-values';
import { HEALTH_REFUND_STEP } from '@mx/settings/constants/router-step';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { REFUND_HEALT_LANG } from '@mx/settings/lang/health.lang';
import { GeneralRequest } from '@mx/statemanagement/models/general.models';
import {
  IPreRegisterRefund,
  IRefundBeneficTypeRequest,
  IRefundHealth
} from '@mx/statemanagement/models/health.interface';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { Subscription } from 'rxjs';
declare var Cleave;

@Component({
  selector: 'client-health-refund-request-step-one',
  templateUrl: './health-refund-request-step-one.component.html',
  providers: [DecimalPipe]
})
export class HealthRefundRequestStepOneComponent implements OnInit {
  @ViewChild(MfInputComponent) inputAmount: MfInputComponent;
  @ViewChild(HealthSearchProviderComponent) searchProvider: HealthSearchProviderComponent;

  steps = HEALTH_REFUND_STEP;
  crossIcon = CROSS_ICON;
  generalLang = GeneralLang;
  lang = REFUND_HEALT_LANG.StepOne;

  form: FormGroup;
  refund: IRefundHealth;
  auth: IdDocument;

  beneficsOptions: Array<SelectListItem> = [];

  maxDateIncidence: Date = new Date();
  minDateTreatment: Date;

  minValueText: string;
  maxValueText: string;
  maxAmount = 9999999;
  minAmount = 1;
  amountRaw: number;

  benefitTypeSub: Subscription;
  preRegisterSub: Subscription;

  inputAmountMask: any;

  constructor(
    private readonly healthRefundService: HealthRefundService,
    private readonly authService: AuthService,
    private readonly decimalPipe: DecimalPipe,
    private readonly fb: FormBuilder,
    private readonly router: Router
  ) {
    this.createForm();
  }

  ngOnInit(): void {
    this.auth = this.authService.retrieveEntity();
    this.refund = this.healthRefundService.getRefund();

    this.setAmountFormat();
    this.getBenefitType();
    this.onChangeDates();
    setTimeout(() => {
      this.setDefault();
    });
  }

  private createForm(): void {
    this.minValueText = `${SOLES_SYMBOL_CURRENCY} ${this.formatValue(this.minAmount)}`;
    this.maxValueText = `${SOLES_SYMBOL_CURRENCY} ${this.formatValue(this.maxAmount)}`;

    this.form = this.fb.group({
      incidenceDate: [null, [Validators.required]],
      treatmentDate: [{ value: null, disabled: true }, [Validators.required]],
      benefitType: [null, Validators.required],
      amount: [null, [AmountValidator.isRangeValid(this.minAmount, this.maxAmount), Validators.required]]
    });
  }

  private setDefault(): void {
    if (this.refund.attention) {
      this.form.controls['incidenceDate'].setValue(new Date(this.refund.attention.incidenceDate));
      this.form.controls['treatmentDate'].setValue(new Date(this.refund.attention.treatmentDate));
      this.form.controls['benefitType'].setValue(this.refund.attention.benefitTypeCode);
      this.form.controls['amount'].setValue(this.refund.attention.amount);
      this.searchProvider.setDefault(
        this.refund.attention.ruc,
        this.refund.attention.social,
        this.refund.attention.verify
      );
      setTimeout(() => this.inputAmountMask.setRawValue(this.refund.attention.amount));
    }
  }

  private onChangeDates(): void {
    const incidenceDate = this.form.controls['incidenceDate'];
    const treatmentDate = this.form.controls['treatmentDate'];

    incidenceDate.valueChanges.subscribe(res => {
      if (res) {
        this.minDateTreatment = typeof res === 'string' ? new Date(res) : res;
        this.minDateTreatment.setHours(0, 0, 0, 0);
        treatmentDate.enable();
        treatmentDate.setValue(null);
      }
    });
  }

  private getBenefitType(): void {
    const params: IRefundBeneficTypeRequest = {
      codigoApp: APPLICATION_CODE,
      codCia: this.refund.policy.codCia,
      codRamo: this.refund.policy.codRamo,
      codModalidad: this.refund.policy.codModality
    };

    this.benefitTypeSub = this.healthRefundService.getBenefitType(params).subscribe(
      benefics => {
        (benefics || []).forEach(benefic => {
          const type = {
            text: benefic.descripcionBeneficio,
            value: benefic.codigoBeneficio,
            selected: false,
            disabled: false
          };

          this.beneficsOptions.push(type);
        });

        this.benefitTypeSub.unsubscribe();
      },
      () => {
        this.benefitTypeSub.unsubscribe();
      }
    );
  }

  private formatValue(value: number): string {
    return this.decimalPipe.transform(value, '1.0-0');
  }

  private setAmountFormat(): void {
    this.inputAmount.input_native.nativeElement.classList.add('input-amount');
    const amount = this.form.controls['amount'];

    this.inputAmountMask = new Cleave('.input-amount', {
      numeral: true,
      numeralDecimalScale: 0,
      numeralPositiveOnly: true,
      numeralThousandsGroupStyle: 'thousand',
      onValueChanged: (e: any) => {
        this.amountRaw = e.target && e.target.rawValue && Number(e.target.rawValue);
        amount.setValidators([
          AmountValidator.isRangeValid(this.minAmount, this.maxAmount, this.amountRaw),
          Validators.required
        ]);

        amount.updateValueAndValidity();
      }
    });
  }

  showErros(controlName: string): boolean {
    return FormComponent.showBasicErrors(this.form, controlName);
  }

  next(): void {
    if (this.form.valid) {
      const benefitTypeCode = Number(this.form.controls['benefitType'].value);
      const benefitType = this.beneficsOptions.find(benefic => benefic.value === benefitTypeCode).text;

      const attention = {
        ruc: Number(this.form.controls['provider'].get('ruc').value),
        social: this.form.controls['provider'].get('social').value,
        verify: this.form.controls['provider'].get('verify').value,
        incidenceDate: this.form.controls['incidenceDate'].value,
        treatmentDate: this.form.controls['treatmentDate'].value,
        benefitTypeCode,
        benefitType,
        amount: this.amountRaw
      };
      this.refund.attention = attention;

      const params: GeneralRequest = {
        codigoApp: APPLICATION_CODE
      };

      const body: IPreRegisterRefund = {
        numeroPreSolicitud: this.refund.numberPreRequest || 0,
        numeroRuc: attention.ruc,
        fechaIncidente: dateToStringFormatYYYYMMDD(attention.incidenceDate),
        fechaTratamiento: dateToStringFormatYYYYMMDD(attention.treatmentDate),
        codigoBeneficio: attention.benefitTypeCode,
        montoReembolso: attention.amount,
        tipoDocumentoBeneficiario: this.refund.beneficiary.documentType,
        numeroDocumentoBeneficiario: this.refund.beneficiary.document,
        tipoPoliza: this.refund.policy.type,
        numeroPoliza: this.refund.policy.policyNumber
      };

      this.preRegisterSub = this.healthRefundService.preRegister(params, body).subscribe(
        res => {
          if (res && res.numeroPreSolicitud) {
            this.refund.numberPreRequest = res.numeroPreSolicitud;
            this.healthRefundService.setRefund(this.refund);
            this.router.navigate(['/health/refund/request/step2']);
          }

          this.preRegisterSub.unsubscribe();
        },
        () => {
          this.preRegisterSub.unsubscribe();
        }
      );
    }
  }

  back(): void {
    this.router.navigate(['/health/refund/request']);
  }
}
