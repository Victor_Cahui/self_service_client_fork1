import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HealthSearchProviderModule } from '@mx/components/health/health-search-provider/health-search-provider.module';
import { FormBaseModule } from '@mx/components/shared/form-base/form-base.module';
import { MfInputDatepickerModule, MfInputModule, MfSelectModule, MfShowErrorsModule } from '@mx/core/ui';
import { StepperModule } from '@mx/core/ui/lib/components/stepper/stepper.module';
import { HealthRefundRequestStepOneComponent } from './health-refund-request-step-one.component';
import { HealthRefundRequestStepOneRoutesModule } from './health-refund-request-step-one.routing';

@NgModule({
  imports: [
    HealthRefundRequestStepOneRoutesModule,
    CommonModule,
    FormBaseModule,
    StepperModule,
    HealthSearchProviderModule,
    MfInputDatepickerModule,
    MfInputModule,
    MfSelectModule,
    MfShowErrorsModule,
    ReactiveFormsModule
  ],
  declarations: [HealthRefundRequestStepOneComponent]
})
export class HealthRefundRequestStepOneModule {}
