import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { HealthModalUploadFileComponent } from '@mx/components/health/health-modal-upload-file/health-modal-upload-file.component';
import { HealthRefunUploadBase } from '@mx/components/health/health-refund-upload-base';
import { IUploadFileList, UploadFilesComponent } from '@mx/components/shared/upload-files/upload-files.component';
import { AuthService } from '@mx/services/auth/auth.service';
import { HealthRefundService } from '@mx/services/health/health-refund.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { FILE_ICON, PDF_ICON } from '@mx/settings/constants/images-values';
import { HEALTH_REFUND_STEP } from '@mx/settings/constants/router-step';
import { HEALTH_REFUND_REQUEST_2_HEADER } from '@mx/settings/constants/router-titles';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { HealthLang, REFUND_HEALT_LANG } from '@mx/settings/lang/health.lang';
import {
  IFileTypeRefund,
  IGetPdfRefundFormatRequest,
  IRefundImageStorage
} from '@mx/statemanagement/models/health.interface';

@Component({
  selector: 'client-health-refund-request-step-two',
  templateUrl: './health-refund-request-step-two.component.html'
})
export class HealthRefundRequestStepTwoComponent extends HealthRefunUploadBase implements OnInit {
  @ViewChild(HealthModalUploadFileComponent) modalSelect: HealthModalUploadFileComponent;

  @ViewChild('upload1') upload1: UploadFilesComponent;
  @ViewChild('upload2') upload2: UploadFilesComponent;
  @ViewChild('upload3') upload3: UploadFilesComponent;

  lang = REFUND_HEALT_LANG.StepTwo;
  generalLang = GeneralLang;
  titleModal2 = HEALTH_REFUND_REQUEST_2_HEADER.title;
  steps = HEALTH_REFUND_STEP;
  fileIcon = FILE_ICON;
  pdfIcon = PDF_ICON;

  showModal: boolean;
  showSpinner: boolean;

  // TODO: Pasar a constantes
  maxFile = {
    one: 2,
    two: 10,
    three: 10
  };

  maxSize = 20;

  constructor(
    private readonly router: Router,
    protected healthRefundService: HealthRefundService,
    protected authService: AuthService
  ) {
    super(healthRefundService, authService);
  }

  ngOnInit(): void {
    this.loadDefaultFiles();
  }

  private loadDefaultFiles(): void {
    const formatRequest = (this.refund.documents && this.refund.documents.formatRequest) || [];
    const attachments = (this.refund.documents && this.refund.documents.attachments) || [];
    const payments = (this.refund.documents && this.refund.documents.payments) || [];

    this.upload1.listFile = this.formatDefaultFileUpload(formatRequest);
    this.upload2.listFile = this.formatDefaultFileUpload(payments);
    this.upload3.listFile = this.formatDefaultFileUpload(attachments);
  }

  back(): void {
    this.router.navigate(['/health/refund/request/step1']);
  }

  next(): void {
    this.router.navigate(['/health/refund/request/summary']);
  }

  completeLoad(tmpKey: number, type: IFileTypeRefund): void {
    if (type === IFileTypeRefund.REQUEST_FORMAT) {
      this.processCompleteLoad(tmpKey, this.upload1.listFile);
    } else if (type === IFileTypeRefund.ATTACHMENTS) {
      this.processCompleteLoad(tmpKey, this.upload3.listFile);
    }
  }

  afterAddFile(data: IUploadFileList, numberFile: number, type: IFileTypeRefund): void {
    // INFO: Almacena metadata de archivos en storage.
    const file = {
      size: data.file.size,
      name: data.file.name,
      numberFile,
      type: data.file.type,
      tmpKey: data.tmpKey
    };

    this.saveStorage(file, type);
  }

  afterRemoveFile(tmpKey: number, type: IFileTypeRefund): void {
    // INFO: Remueve metadata de archivos en storage.
    // TODO: disminuir lógica
    this.refund.documents = this.refund.documents || {};
    if (type === IFileTypeRefund.REQUEST_FORMAT) {
      this.refund.documents.formatRequest = this.refund.documents.formatRequest || [];
      const index = this.refund.documents.formatRequest.findIndex(item => item.tmpKey === tmpKey);

      this.refund.documents.formatRequest.splice(index, 1);
    } else if (type === IFileTypeRefund.ATTACHMENTS) {
      this.refund.documents.attachments = this.refund.documents.attachments || [];
      const index = this.refund.documents.attachments.findIndex(item => item.tmpKey === tmpKey);

      this.refund.documents.attachments.splice(index, 1);
    } else if (type === IFileTypeRefund.PAYMENT_VOUCHER) {
      this.refund.documents.payments = this.refund.documents.payments || [];
      const index = this.refund.documents.payments.findIndex(item => item.tmpKey === tmpKey);

      this.refund.documents.payments.splice(index, 1);
    }

    this.healthRefundService.setRefund(this.refund);
    this.upload3.completeRemove();
  }

  onConfirm(fileList: Array<IRefundImageStorage>, type: IFileTypeRefund): void {
    fileList.forEach(file => {
      this.saveStorage(file, type);
    });
    this.upload2.listFile = this.formatDefaultFileUpload(this.refund.documents.payments);
    this.upload2.validDisabled();
  }

  saveStorage(file: IRefundImageStorage, type: IFileTypeRefund): void {
    // TODO: disminuir lógica
    this.refund.documents = this.refund.documents || {};
    if (type === IFileTypeRefund.REQUEST_FORMAT) {
      this.refund.documents.formatRequest = this.refund.documents.formatRequest || [];
      this.refund.documents.formatRequest.push(file);
    } else if (type === IFileTypeRefund.ATTACHMENTS) {
      this.refund.documents.attachments = this.refund.documents.attachments || [];
      this.refund.documents.attachments.push(file);
    } else if (type === IFileTypeRefund.PAYMENT_VOUCHER) {
      this.refund.documents.payments = this.refund.documents.payments || [];
      this.refund.documents.payments.push(file);
    }

    this.healthRefundService.setRefund(this.refund);
  }

  openModalSelect(): void {
    this.showModal = true;
    setTimeout(() => {
      this.modalSelect.modal.open();
    }, 0);
  }

  onCloseModal(): void {
    this.showModal = false;
  }

  downloadPDF(): void {
    if (!this.showSpinner) {
      const params: IGetPdfRefundFormatRequest = {
        codigoApp: APPLICATION_CODE,
        codCia: this.refund.policy.codCia,
        codRamo: this.refund.policy.codRamo,
        codModalidad: this.refund.policy.codModality
      };
      this.showSpinner = true;
      const getPDFRefundFormatSub = this.healthRefundService.getPDFRefundFormat(params).subscribe(
        record => {
          this.showSpinner = false;
          const fileName = `${HealthLang.Texts.RefundPdfFile}${this.refund.policy.policyNumber}`;
          this.fileUtil.download(record.base64, 'pdf', fileName);
          getPDFRefundFormatSub.unsubscribe();
        },
        () => {
          getPDFRefundFormatSub.unsubscribe();
          this.showSpinner = false;
        }
      );
    }
  }

  pendingUploadFiles(): boolean {
    return !(
      this.refund.documents &&
      (this.refund.documents.formatRequest || []).length > 0 &&
      (this.refund.documents.payments || []).length > 0
    );
  }
}
