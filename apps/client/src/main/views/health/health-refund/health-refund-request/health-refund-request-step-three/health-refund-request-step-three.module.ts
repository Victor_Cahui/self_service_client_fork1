import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormBaseModule } from '@mx/components/shared/form-base/form-base.module';
import { UploadFilesModule } from '@mx/components/shared/upload-files/upload-files.module';
import { StepperModule } from '@mx/core/ui/lib/components/stepper/stepper.module';
import { HealthRefundRequestStepThreeComponent } from './health-refund-request-step-three.component';
import { HealthRefundRequestStepThreeRoutesModule } from './health-refund-request-step-three.routing';

@NgModule({
  imports: [HealthRefundRequestStepThreeRoutesModule, CommonModule, FormBaseModule, StepperModule, UploadFilesModule],
  declarations: [HealthRefundRequestStepThreeComponent]
})
export class HealthRefundRequestStepThreeModule {}
