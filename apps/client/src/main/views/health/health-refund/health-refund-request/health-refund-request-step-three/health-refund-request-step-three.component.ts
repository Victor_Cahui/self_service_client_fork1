import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HealthRefunUploadBase } from '@mx/components/health/health-refund-upload-base';
import { IUploadFileList } from '@mx/components/shared/upload-files/upload-files.component';
import { AuthService } from '@mx/services/auth/auth.service';
import { HealthRefundService } from '@mx/services/health/health-refund.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { CROSS_ICON, EDIT_ICON } from '@mx/settings/constants/images-values';
import { HEALTH_REFUND_STEP } from '@mx/settings/constants/router-step';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { REFUND_HEALT_LANG } from '@mx/settings/lang/health.lang';
import { GeneralRequest } from '@mx/statemanagement/models/general.models';
import { IRefundHealth } from '@mx/statemanagement/models/health.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-health-refund-request-step-three',
  templateUrl: './health-refund-request-step-three.component.html'
})
export class HealthRefundRequestStepThreeComponent extends HealthRefunUploadBase implements OnInit {
  steps = HEALTH_REFUND_STEP;
  crossIcon = CROSS_ICON;
  generalLang = GeneralLang;
  lang = REFUND_HEALT_LANG;
  editIcon = EDIT_ICON;

  refund: IRefundHealth;

  listFileFormat: Array<IUploadFileList> = [];
  listFilePayments: Array<IUploadFileList> = [];
  listFileOptions: Array<IUploadFileList> = [];

  confirmRefundSub: Subscription;

  constructor(
    protected healthRefundService: HealthRefundService,
    protected authService: AuthService,
    private readonly router: Router
  ) {
    super(healthRefundService, authService);
  }

  ngOnInit(): void {
    this.loadDefaultFiles();
  }

  private loadDefaultFiles(): void {
    const formatRequest = (this.refund.documents && this.refund.documents.formatRequest) || [];
    const attachments = (this.refund.documents && this.refund.documents.attachments) || [];
    const payments = (this.refund.documents && this.refund.documents.payments) || [];

    this.listFileFormat = this.formatDefaultFileUpload(formatRequest);
    this.listFilePayments = this.formatDefaultFileUpload(payments);
    this.listFileOptions = this.formatDefaultFileUpload(attachments);
  }

  finish(): void {
    const params: GeneralRequest = {
      codigoApp: APPLICATION_CODE
    };
    this.confirmRefundSub = this.healthRefundService
      .confirmRefund(params, { numeroPreSolicitud: this.refund.numberPreRequest })
      .subscribe(
        res => {
          this.refund.hoursConfirm = res.horasConfirmacion;
          this.healthRefundService.setRefund(this.refund);
          this.router.navigate(['/health/refund/request/finish']);
          this.confirmRefundSub.unsubscribe();
        },
        () => {
          this.confirmRefundSub.unsubscribe();
        }
      );
  }

  goToStep(step: number): void {
    this.router.navigate([`/health/refund/request/step${step}`]);
  }
}
