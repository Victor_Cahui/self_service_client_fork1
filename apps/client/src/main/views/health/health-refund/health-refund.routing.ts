import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HealthRefundStepDontFinishGuard } from '@mx/guards/health/health-refund-step-dont-finish.guard';
import { HealthRefundStepFinishGuard } from '@mx/guards/health/health-refund-step-finish.guard';
import { HealthRefundStepThreeGuard } from '@mx/guards/health/health-refund-step-three.guard';
import { HealthRefundStepTwoGuard } from '@mx/guards/health/health-refund-step-two.guard';
import { HealthRefundGuard } from '@mx/guards/health/health-refund.guard';
import { HealthRefundComponent } from './health-refund.component';

const routes: Routes = [
  {
    path: '',
    component: HealthRefundComponent,
    children: [
      {
        path: '',
        redirectTo: 'request',
        pathMatch: 'prefix'
      },
      {
        path: 'request',
        loadChildren: './health-refund-request/health-refund-request.module#HealthRefundRequestModule'
      },
      {
        path: 'tray',
        loadChildren: './health-refund-tray/health-refund-tray.module#HealthRefundTrayModule'
      }
    ]
  },
  {
    path: 'request',
    children: [
      {
        path: 'step1',
        canActivate: [HealthRefundGuard, HealthRefundStepDontFinishGuard],
        loadChildren:
          './health-refund-request/health-refund-request-step-one/health-refund-request-step-one.module#HealthRefundRequestStepOneModule'
      },
      {
        path: 'step2',
        canActivate: [HealthRefundGuard, HealthRefundStepTwoGuard, HealthRefundStepDontFinishGuard],
        loadChildren:
          './health-refund-request/health-refund-request-step-two/health-refund-request-step-two.module#HealthRefundRequestStepTwoModule'
      },
      {
        path: 'summary',
        canActivate: [
          HealthRefundGuard,
          HealthRefundStepTwoGuard,
          HealthRefundStepThreeGuard,
          HealthRefundStepDontFinishGuard
        ],
        loadChildren:
          // tslint:disable-next-line:max-line-length
          `./health-refund-request/health-refund-request-step-three/health-refund-request-step-three.module#HealthRefundRequestStepThreeModule`
      },
      {
        path: 'finish',
        canActivate: [
          HealthRefundGuard,
          HealthRefundStepTwoGuard,
          HealthRefundStepThreeGuard,
          HealthRefundStepFinishGuard
        ],
        loadChildren:
          // tslint:disable-next-line:max-line-length
          `./health-refund-request/health-refund-request-step-finish/health-refund-request-step-finish.module#HealthRefundRequestStepFinishModule`
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HealthRefundRoutingModule {}
