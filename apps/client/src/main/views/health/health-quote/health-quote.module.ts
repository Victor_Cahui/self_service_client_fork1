import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BannerCarouselModule } from '@mx/components/shared/banner-carousel/banner-carousel.module';
import { BannerModule } from '@mx/components/shared/banner/banner.module';
import { CardFaqModule } from '@mx/components/shared/card-faq/card-faq.module';
import { CardHowToWorkModule } from '@mx/components/shared/card-how-to-work/card-how-to-work.module';
import { CardInsurancePlansModule } from '@mx/components/shared/card-insurance-plans/card-insurance-plans.module';
import { CardPaymentOptionsModule } from '@mx/components/shared/card-payment-options/card-payment-options.module';
import { ModalContactedByAgentModule } from '@mx/components/shared/modals/modal-contacted-by-agent/modal-contacted-by-agent.module';
import { ModalMoreFaqsModule } from '@mx/components/shared/modals/modal-more-faqs/modal-more-faqs.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule } from '@mx/core/ui';
import { MfButtonModule } from '@mx/core/ui/lib/components/forms/button';
import { HealthQuoteComponent } from './health-quote.component';
import { HealthQuoteRoutingModule } from './health-quote.routing';

@NgModule({
  imports: [
    CommonModule,
    HealthQuoteRoutingModule,
    MfButtonModule,
    BannerCarouselModule,
    BannerModule,
    CardInsurancePlansModule,
    CardHowToWorkModule,
    CardPaymentOptionsModule,
    CardFaqModule,
    ModalMoreFaqsModule,
    DirectivesModule,
    ModalContactedByAgentModule,
    GoogleModule
  ],
  exports: [],
  declarations: [HealthQuoteComponent],
  providers: []
})
export class HealthQuoteModule {}
