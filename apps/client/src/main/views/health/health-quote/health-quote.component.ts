import { Component, OnInit, ViewChild } from '@angular/core';
import { ConfigurationBase } from '@mx/components/shared/configuration/configuration-base';
import { ModalContactedByAgentComponent } from '@mx/components/shared/modals/modal-contacted-by-agent/modal-contacted-by-agent.component';
import { ModalMoreFaqsComponent } from '@mx/components/shared/modals/modal-more-faqs/modal-more-faqs.component';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { MFP_Cotizacion_31A } from '@mx/settings/constants/events.analytics';
import { POLICY_TYPES, STATIC_BANNERS } from '@mx/settings/constants/policy-values';
import { HEALTH_HEADER } from '@mx/settings/constants/router-titles';
import { HEALTH_INSURANCE_FAQS } from '@mx/settings/faqs/health-insurance.faq';
import { CONTACTED_BY_AGENT } from '@mx/settings/lang/contact';
import * as source from '@mx/settings/lang/health.lang';
import { IContactedByAgent, IPathImageSize } from '@mx/statemanagement/models/general.models';

@Component({
  selector: 'client-health-quote-component',
  templateUrl: './health-quote.component.html'
})
export class HealthQuoteComponent extends ConfigurationBase implements OnInit {
  @ViewChild(ModalMoreFaqsComponent) modalFaqs: ModalMoreFaqsComponent;
  @ViewChild(ModalContactedByAgentComponent) modalAgentConnect: ModalContactedByAgentComponent;

  staticBanners: string;
  titleQuote = source.HealthLang.Titles.Cotizar;
  btnQuote = source.HealthLang.Links.Send;
  textQuote = source.HealthLang.Texts.Cotizar;

  howItWorks = source.HOW_IT_WORKS;
  paymentOptions = source.PAYMENTS_OPTIONS;
  insuranceTop = source.INSURANCE_TOP;
  faqs = HEALTH_INSURANCE_FAQS;
  subTitleFaq = HEALTH_HEADER.title;
  showModal: boolean;
  showModalFaqs: boolean;

  auth;
  pathImgBanner: IPathImageSize;
  pathImgBanner2: IPathImageSize;
  form: IContactedByAgent = CONTACTED_BY_AGENT;

  gaQuoteNow: IGaPropertie;

  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService
  ) {
    super(clientService, policiesInfoService, configurationService, authService);
    this.gaQuoteNow = MFP_Cotizacion_31A(this.btnQuote);
  }

  ngOnInit(): void {
    this.policyType = this.getPolicyType();
    this.staticBanners = POLICY_TYPES[this.policyType].staticBanners;
    this.pathImgBanner = {
      sm: this.staticBanners + STATIC_BANNERS.sm,
      lg: this.staticBanners + STATIC_BANNERS.lg,
      xl: this.staticBanners + STATIC_BANNERS.xl
    };
    this.policyType = `${this.getPolicyType()}_COTIZACION`;
    this.loadConfiguration();
  }

  getPolicyType(): string {
    return POLICY_TYPES.MD_SALUD.code;
  }

  openModalMoreFaqs(): void {
    this.showModalFaqs = true;
    setTimeout(() => {
      this.modalFaqs.open();
    }, 10);
  }

  eventQuote(): void {
    window.open(source.INSURANCE_TOP.urlGetQuote, '_blank');
  }

  openModalContactByAgent(): void {
    this.showModal = true;
    this.policyType = this.getPolicyType();
    setTimeout(() => {
      this.modalAgentConnect.open();
    }, 10);
  }
}
