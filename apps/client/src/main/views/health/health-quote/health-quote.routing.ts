import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HealthQuoteComponent } from './health-quote.component';

const routes: Routes = [
  {
    path: '',
    component: HealthQuoteComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HealthQuoteRoutingModule {}
