import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'client-my-cards-list',
  templateUrl: './payments-my-cards.component.html'
})
export class PaymentsMyCardsComponent implements OnInit {
  titleBanner = 'Mis tarjetas';
  cardsLength = '0';

  showTitle = false;
  title = '';
  showDetail = false;
  onlyForm = true;
  myPayment = {
    codigoMoneda: '1'
  };

  ngOnInit(): void {}

  getCardsLength(ev): void {
    this.cardsLength = ev;
  }

  onFormReady(ev): void {
    if (ev) {
      this.showTitle = true;
      this.showDetail = false;
      this.title = 'Agregar una tarjeta de crédito :';
      this.onlyForm = false;
    } else {
      this.showTitle = false;
      this.showDetail = true;
      this.onlyForm = true;
    }
  }

  onCancel(ev): void {
    this.showTitle = false;
    this.showDetail = false;
    this.onlyForm = true;
  }
}
