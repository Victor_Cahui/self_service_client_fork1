import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardPaymentsDetailModule } from '@mx/components/payment/card-payments-detail/card-payments-detail.module';
import { TabsPaymentsModule } from '@mx/components/payment/tabs-payments/tabs-payments.module';
import { PaymentCardListModule } from '@mx/components/shared/payments';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { PaymentsMyCardsComponent } from './payments-my-cards.component';

@NgModule({
  imports: [CommonModule, CardPaymentsDetailModule, GoogleModule, TabsPaymentsModule, PaymentCardListModule],
  declarations: [PaymentsMyCardsComponent]
})
export class PaymentMyCardsModule {}
