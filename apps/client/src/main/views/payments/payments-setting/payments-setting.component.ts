import { Component, OnInit } from '@angular/core';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PAYMENTS_SETTING_POLICY_LIST } from '@mx/settings/constants/router-titles';

@Component({
  selector: 'client-payments-settings',
  templateUrl: './payments-setting.component.html'
})
export class PaymentsSettingComponent implements OnInit {
  constructor(private readonly headerHelperService: HeaderHelperService) {}

  ngOnInit(): void {
    this.headerHelperService.set(PAYMENTS_SETTING_POLICY_LIST);
  }
}
