import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SettingAssociateComponent } from './setting-associate.component';

const routes: Routes = [
  {
    path: '',
    component: SettingAssociateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingAssociateRouting {}
