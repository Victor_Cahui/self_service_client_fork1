import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PaymentCardListModule } from '@mx/components';
import { SettingEditComponent } from './setting-edit.component';
import { SettingEditRouting } from './setting-edit.routing';

@NgModule({
  imports: [CommonModule, SettingEditRouting, PaymentCardListModule],
  declarations: [SettingEditComponent]
})
export class SettingEditModule {}
