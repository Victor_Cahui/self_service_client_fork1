import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BasePaymentSettings } from '@mx/components/payment/card-payments-setting/base-payments-settings';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { AuthService } from '@mx/services/auth/auth.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PaymentService } from '@mx/services/payment.service';
import { MFP_Configuracion_de_Pagos_44A } from '@mx/settings/constants/events.analytics';
import { PAYMENTS_SETTINGS_OPTIONS, PAYMENTS_SETTINGS_OPTIONS_KEYS } from '@mx/settings/constants/router-tabs';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-payments-setting-policy-edit',
  templateUrl: './payments-setting-policy-edit.component.html',
  providers: [DatePipe]
})
export class PaymentsSettingPolicyEditComponent extends BasePaymentSettings implements OnInit {
  selectLabel = GeneralLang.Labels.Select;
  textNotFound = GeneralLang.Messages.ItemNotFound;
  options = PAYMENTS_SETTINGS_OPTIONS;
  paymentTypeSub: Subscription;

  ga: Array<IGaPropertie>;

  constructor(
    protected paymentService: PaymentService,
    protected authService: AuthService,
    protected datePipe: DatePipe,
    protected activatedRoute: ActivatedRoute,
    protected headerHelperService: HeaderHelperService,
    private readonly router: Router
  ) {
    super(paymentService, authService, activatedRoute, headerHelperService, datePipe);
    this.ga = [MFP_Configuracion_de_Pagos_44A()];
  }

  ngOnInit(): void {
    this.policyNumber = this.activatedRoute.snapshot.paramMap.get('policyNumber');
    this.setTitle();
    this.payment ? this.setPaymetSettings() : this.getPaymentList();
    this.paymentService.onPaymentTypeChange().next(null);
    this.paymentTypeSub = this.paymentService
      .onPaymentTypeChange()
      .subscribe(payment => (payment ? (this.payment = payment) : null));
  }

  setPaymetSettings(): void {
    if (this.payment) {
      // Filtrar opciones
      this.options.find(
        e => e.name === PAYMENTS_SETTINGS_OPTIONS_KEYS.EDIT
      ).visible = this.payment.puedeCambiarTipoRenovacion;
      this.options.find(
        e => e.name === PAYMENTS_SETTINGS_OPTIONS_KEYS.ASSOCIATE
      ).visible = this.payment.puedeRefinanciar;
      this.options = this.options.filter(option => option.visible);

      // Cargar primera opción
      if (this.options.length && this.router.url.endsWith(this.policyNumber)) {
        const path = `${this.router.url}${this.options[0].route.replace('.', '')}`;
        this.router.navigate([path], { replaceUrl: true });
      }
    }
  }

  onDestroy(): void {
    if (this.paymentTypeSub) {
      this.paymentTypeSub.unsubscribe();
    }
  }

  onLoadPayments(): void {
    this.getPaymentData();
    this.setPaymetSettings();
  }
}
