import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SettingEditComponent } from './setting-edit.component';

const routes: Routes = [
  {
    path: '',
    component: SettingEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingEditRouting {}
