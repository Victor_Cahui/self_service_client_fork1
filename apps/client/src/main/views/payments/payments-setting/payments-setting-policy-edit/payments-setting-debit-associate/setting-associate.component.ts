import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BasePaymentSettings } from '@mx/components/payment/card-payments-setting/base-payments-settings';
import { AuthService } from '@mx/services/auth/auth.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PaymentService } from '@mx/services/payment.service';
import { ItooltipPM } from '@mx/statemanagement/models/payment.models';

@Component({
  selector: 'client-payment-setting-associate',
  templateUrl: './setting-associate.component.html',
  providers: [DatePipe]
})
export class SettingAssociateComponent extends BasePaymentSettings implements OnInit {
  title = 'Seleccione una tarjeta de crédito';
  showTitle = true;
  showDetail = true;
  tooltip: ItooltipPM = {
    label: 'Seleccione una tarjeta o agrega una nueva'
  };

  constructor(
    protected paymentService: PaymentService,
    protected authService: AuthService,
    protected datePipe: DatePipe,
    protected activatedRoute: ActivatedRoute,
    protected headerHelperService: HeaderHelperService
  ) {
    super(paymentService, authService, activatedRoute, headerHelperService, datePipe);
  }

  ngOnInit(): void {
    this.policyNumber = this.activatedRoute.snapshot.paramMap.get('policyNumber');
    this.getPaymentList();
    this.getPaymentData();
  }

  onFormReady(ev): void {
    this.showTitle = true;
    this.showDetail = true;
    this.title = 'Agregar una tarjeta de crédito';
    this.tooltip = {
      label: 'Ingresa los datos de la tarjeta que deseas afiliar para realizar el débito automático',
      link: '¿Cómo funciona?',
      title: '¿Como funciona el débito automático?',
      text:
        'El débito automático es una modalidad de pago que perminte abonar de manera automática, sin estar pendientes de las fechas de pago.',
      icon: 'assets/images/cards/icon-debito-automatico.svg'
    };
  }
}
