import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PaymentSettingsPolicyModule } from '@mx/components';
import { ItemNotFoundModule } from '@mx/components/shared/item-not-found/item-not-found.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { MfTabVerticalModule } from '@mx/core/ui/lib/components/tabs/tabs-vertical/tab-vertical.module';
import { PaymentsSettingPolicyEditComponent } from './payments-setting-policy-edit.component';
import { PaymentsSettingPolicyEditRoutingModule } from './payments-setting-policy-edit.routing';

@NgModule({
  imports: [
    CommonModule,
    PaymentsSettingPolicyEditRoutingModule,
    PaymentSettingsPolicyModule,
    ItemNotFoundModule,
    MfTabVerticalModule,
    MfLoaderModule,
    GoogleModule
  ],
  declarations: [PaymentsSettingPolicyEditComponent],
  exports: [PaymentsSettingPolicyEditComponent]
})
export class PaymentsSettingPolicyEditModule {}
