import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PaymentsSettingPolicyEditComponent } from './payments-setting-policy-edit.component';

const routes: Routes = [
  {
    path: '',
    component: PaymentsSettingPolicyEditComponent,
    pathMatch: 'prefix',
    children: [
      {
        path: 'edit',
        loadChildren: './payments-setting-debit-edit/setting-edit.module#SettingEditModule'
      },
      {
        path: 'associate',
        loadChildren: './payments-setting-debit-associate/setting-associate.module#SettingAssociateModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentsSettingPolicyEditRoutingModule {}
