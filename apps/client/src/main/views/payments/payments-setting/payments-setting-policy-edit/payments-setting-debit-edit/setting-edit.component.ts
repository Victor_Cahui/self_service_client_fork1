import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BasePaymentSettings } from '@mx/components/payment/card-payments-setting/base-payments-settings';
import { AuthService } from '@mx/services/auth/auth.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PaymentService } from '@mx/services/payment.service';
import { ItooltipPM } from '@mx/statemanagement/models/payment.models';

@Component({
  selector: 'client-payment-setting-edit',
  templateUrl: './setting-edit.component.html',
  providers: [DatePipe]
})
export class SettingEditComponent extends BasePaymentSettings implements OnInit {
  title = 'Editar tarjeta';
  showTitle = true;
  showDetail = true;
  tooltip: ItooltipPM = {
    label: 'Seleccione una tarjeta o agrega una nueva'
  };

  selectedCard: any;
  manualPayment = false;

  constructor(
    protected paymentService: PaymentService,
    protected authService: AuthService,
    protected datePipe: DatePipe,
    protected activatedRoute: ActivatedRoute,
    protected headerHelperService: HeaderHelperService
  ) {
    super(paymentService, authService, activatedRoute, headerHelperService, datePipe);
  }

  ngOnInit(): void {
    this.policyNumber = this.activatedRoute.snapshot.paramMap.get('policyNumber');
    this.getPaymentList();
    this.getPaymentData();
    this.setCardNumber();
    this.manualPayment = this.payment.puedeHacerPagoManual;
  }

  setCardNumber(): void {
    this.selectedCard = this.payment.tarjeta.numero;
  }
}
