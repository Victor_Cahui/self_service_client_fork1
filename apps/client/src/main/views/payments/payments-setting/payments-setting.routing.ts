import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PaymentsSettingComponent } from './payments-setting.component';

const routes: Routes = [
  {
    path: '',
    component: PaymentsSettingComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentsSettingRoutingModule {}
