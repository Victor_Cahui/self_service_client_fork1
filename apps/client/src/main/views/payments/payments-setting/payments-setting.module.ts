import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardPaymentsSettingModule } from '@mx/components/payment/card-payments-setting/card-payments-setting.module';
import { TabsPaymentsModule } from '@mx/components/payment/tabs-payments/tabs-payments.module';
import { PaymentsSettingPolicyEditModule } from './payments-setting-policy-edit/payments-setting-policy-edit.module';
import { PaymentsSettingComponent } from './payments-setting.component';
import { PaymentsSettingRoutingModule } from './payments-setting.routing';

@NgModule({
  imports: [
    PaymentsSettingRoutingModule,
    CommonModule,
    TabsPaymentsModule,
    CardPaymentsSettingModule,
    PaymentsSettingPolicyEditModule
  ],
  declarations: [PaymentsSettingComponent],
  exports: [PaymentsSettingComponent]
})
export class PaymentsSettingModule {}
