import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardReceiptsListModule } from '@mx/components/payment/card-receipts-list/card-receipts-list.module';
import { TabsPaymentsModule } from '@mx/components/payment/tabs-payments/tabs-payments.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { PaymentReceiptsComponent } from './payment-receipts.component';

@NgModule({
  imports: [CommonModule, TabsPaymentsModule, CardReceiptsListModule, GoogleModule],
  declarations: [PaymentReceiptsComponent]
})
export class PaymentReceiptsModule {}
