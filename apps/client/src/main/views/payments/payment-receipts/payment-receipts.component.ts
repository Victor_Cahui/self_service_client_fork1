import { Component } from '@angular/core';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { MFP_Mis_Pagos_42B, MFP_Mis_Pagos_42C } from '@mx/settings/constants/events.analytics';

@Component({
  selector: 'client-payment-receipts',
  templateUrl: './payment-receipts.component.html'
})
export class PaymentReceiptsComponent {
  ga: Array<IGaPropertie>;

  constructor() {
    this.ga = [MFP_Mis_Pagos_42B(), MFP_Mis_Pagos_42C()];
  }
}
