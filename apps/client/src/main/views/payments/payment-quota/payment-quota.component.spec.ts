import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PaymentMode } from '@mx/components/shared/utils/payment';
import { ICardPaymentRequest } from '@mx/statemanagement/models/payment.models';
import { PaymentQuotaComponent } from './payment-quota.component';
import { PaymentQuotaRoutingModule } from './payment-quota.routing';

describe('PaymentQuotaComponent', () => {
  let component: PaymentQuotaComponent;
  let fixture: ComponentFixture<PaymentQuotaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [PaymentQuotaRoutingModule],
      declarations: [PaymentQuotaComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentQuotaComponent);
    component = fixture.debugElement.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', async(() => {
    expect(component).toBeTruthy();
  }));

  it('should payment soat with card', async(() => {
    const dataCard: ICardPaymentRequest = {
      numero: '4111111111111111',
      nombre: 'Juan Perez',
      tipo: 'VISA',
      mesVencimiento: '09',
      anioVencimiento: '2025',
      cvv: '123',
      correoElectronico: 'test@gmail.com'
    };
    component.goPayAndIssueSoat(dataCard, PaymentMode.CARD);
    fixture.detectChanges();
    expect(true).toBe(true);
  }));
});
