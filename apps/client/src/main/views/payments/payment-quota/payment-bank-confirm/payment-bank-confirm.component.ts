import { CurrencyPipe, DecimalPipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PaymentQuotaBase } from '@mx/components/shared/payments/payment-quota-base';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PaymentService } from '@mx/services/payment.service';

@Component({
  selector: 'client-payment-bank-confirm',
  templateUrl: './payment-bank-confirm.component.html',
  providers: [DecimalPipe, CurrencyPipe]
})
export class PaymentBankConfirmComponent extends PaymentQuotaBase implements OnInit {
  constructor(
    protected headerHelperService: HeaderHelperService,
    protected paymentService: PaymentService,
    protected router: Router,
    protected decimalPipe: DecimalPipe,
    protected currencyPipe: CurrencyPipe,
    protected gaService: GAService
  ) {
    super(headerHelperService, paymentService, router, decimalPipe, currencyPipe, gaService);
  }

  ngOnInit(): void {
    this.getCurrentQuote();
    this.setHeader();
    this.setView();
  }
}
