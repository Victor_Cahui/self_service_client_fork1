import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PaymentBankConfirmComponent } from './payment-bank-confirm.component';

const routes: Routes = [
  {
    path: '',
    component: PaymentBankConfirmComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentBankConfirmRoutingModule {}
