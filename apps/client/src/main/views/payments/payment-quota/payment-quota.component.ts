import { CurrencyPipe, DecimalPipe, Location } from '@angular/common';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CardQuoteSoatComponent } from '@mx/components/shared/payments/card-quote-soat/card-quote-soat.component';
import { PaymentQuotaBase } from '@mx/components/shared/payments/payment-quota-base';
import { PaymentMode } from '@mx/components/shared/utils/payment';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { MfModalAlertComponent } from '@mx/core/ui/lib/components/modals/modal-alert/modal-alert.component';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { MenuService } from '@mx/services/menu/menu.service';
import { PaymentService } from '@mx/services/payment.service';
import { MFP_Cuotas_de_Poliza_Pagar_28C } from '@mx/settings/constants/events.analytics';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { WARNING_ICON } from '@mx/settings/constants/images-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { ICardPaymentRequest, IPaymentMethod, IReceiptPayRequest } from '@mx/statemanagement/models/payment.models';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';

@Component({
  selector: 'client-payment-quota',
  templateUrl: './payment-quota.component.html',
  providers: [DecimalPipe, CurrencyPipe]
})
export class PaymentQuotaComponent extends PaymentQuotaBase implements OnInit, AfterViewInit {
  @ViewChild(CardQuoteSoatComponent) cardSoat: CardQuoteSoatComponent;
  @ViewChild(MfModalAlertComponent) mfModalAlert: MfModalAlertComponent;

  auth: IdDocument;
  isFree: boolean;
  contracting: string;
  msgErrorTransaction: string;
  warningIcon = WARNING_ICON;
  isBtnPayment: boolean;

  constructor(
    protected headerHelperService: HeaderHelperService,
    protected paymentService: PaymentService,
    protected router: Router,
    protected decimalPipe: DecimalPipe,
    protected currencyPipe: CurrencyPipe,
    protected gaService: GAService,
    private readonly authService: AuthService,
    private readonly location: Location,
    private readonly clientService: ClientService,
    private readonly menuService: MenuService
  ) {
    super(headerHelperService, paymentService, router, decimalPipe, currencyPipe, gaService);
    this.ga = [MFP_Cuotas_de_Poliza_Pagar_28C(this._getPolicyType())];
  }

  ngAfterViewInit(): void {
    window.scrollTo(0, 0);
  }

  ngOnInit(): void {
    this.auth = this.authService.retrieveEntity();
    this.getCurrentQuote();
    this.setHeader();
    this.isBtnPayment = this.quote.response ? this.quote.response.estadoPagoRenovacion === 'BTN_PAGO' : false;
    if (this.isSoat) {
      this.paymentService.setVigencyDate('', '');
      this.contracting = this.quote.dataQuote ? this.quote.dataQuote.contratante.nombre : '';
      this.vigencyDateSub = this.paymentService.onVigencyDateChange().subscribe(dates => {
        if (dates.start) {
          const quote = this.paymentService.getCurrentQuote();
          quote.startDate = dates.start;
          quote.endDate = dates.end;
          this.paymentService.setCurrentQuote(quote);
        }
      });
      this.useMapfreDollarsSub = this.paymentService.onUseMapfreDolarsChange().subscribe(value => {
        setTimeout(() => (this.isFree = value), 50);
      });
    }
  }

  // Procesar Pago
  payment(data: IPaymentMethod): void {
    if (!data.cvv) {
      this.quote = this.paymentService.getCurrentQuote();
      this.quote.email = data.paymentMode === PaymentMode.CARD ? data.email : null;
      this.quote.paymentMode = data.paymentMode;
      this.paymentService.setCurrentQuote(this.quote);
      this.goToConfirm();
    } else {
      this.quote = this.paymentService.getCurrentQuote();
      const dataCard: ICardPaymentRequest =
        data.paymentMode === PaymentMode.CARD
          ? {
              numero: data.cardNumber,
              nombre: data.ownerName,
              tipo: data.cardType,
              mesVencimiento: data.cardExpiredMonth,
              anioVencimiento: data.cardExpiredYear,
              cvv: data.cvv,
              correoElectronico: data.email,
              token: this.quote.confirmFormData.tokenCard
            }
          : null;
      this.quote.paymentMode = data.paymentMode;
      this.quote.email = data.paymentMode === PaymentMode.CARD ? data.email : null;
      this.paymentService.setCurrentQuote(this.quote);
      if (this.isSoat) {
        this.goPayAndIssueSoat(dataCard, data.paymentMode);
      } else {
        this.goPayReceipts(dataCard, data.paymentMode);
      }
    }
  }

  // Pago SOAT
  goPayAndIssueSoat(dataCard, paymentMode): void {
    const params = {
      codigoApp: APPLICATION_CODE
    };
    const vehicle = this.quote.dataQuote.datosVehiculo.vehiculo;
    const body = {
      vehiculo: {
        placa: vehicle.placa,
        tipoVehiculoId: vehicle.tipoVehiculoId,
        tipoVehiculoDescripcion: vehicle.tipoVehiculoDescripcion,
        marcaId: vehicle.marcaId,
        marcaDescripcion: vehicle.marcaDescripcion,
        modeloId: vehicle.modeloId,
        modeloDescripcion: vehicle.modeloDescripcion,
        cantidadAsientos: vehicle.cantidadAsientos,
        tipoUsoId: vehicle.tipoUsoId,
        tipoUsoDescripcion: vehicle.tipoUsoDescripcion,
        anioFabricacion: vehicle.anioFabricacion,
        serialVin: vehicle.serialVin
      },
      contratante: this.quote.dataQuote.contratante,
      cotizacion: this.quote.withMPD ? this.quote.dataQuote.cotizacionConMPD : this.quote.dataQuote.cotizacionSinMPD,
      vigencia: {
        fechaInicio: this.quote.startDate,
        fechaFin: this.quote.endDate
      },
      pago: {
        tipoPago: paymentMode,
        monto: this.quote.total,
        tipoMoneda: this.quote.currency
      },
      tarjeta: dataCard || {}
    };
    this.paymentServiceSub = this.paymentService.SoatIssuance(body, params).subscribe(
      response => {
        if (response.transaccion.codTransaccion !== '01') {
          this.msgErrorTransaction = response.transaccion.mensajeUsuario;
          this.paymentServiceSub.unsubscribe();
          this.mfModalAlert.open();

          return void 0;
        }
        if (response) {
          this.quote.billNumber = response.numeroRecibo || null;
          this.paymentService.setCurrentQuote(this.quote);
          this.goToConfirm();
        }
      },
      () => {
        this.paymentServiceSub.unsubscribe();
      },
      () => {
        this.paymentServiceSub.unsubscribe();
      }
    );
  }

  // Pago Recibos
  goPayReceipts(data: ICardPaymentRequest, paymentMode: PaymentMode): void {
    if (paymentMode === PaymentMode.CARD) {
      const params = {
        codigoApp: APPLICATION_CODE
      };
      const body: IReceiptPayRequest = {
        modalidadPago: paymentMode,
        recibo: this.quote.response,
        tarjeta: data
      };

      this.paymentServiceSub = this.paymentService.receiptPay(body, params).subscribe(
        res => {
          if (res.transaccion.codTransaccion !== '01') {
            this.msgErrorTransaction = res.transaccion.mensajeUsuario;
            this.paymentServiceSub.unsubscribe();
            this.mfModalAlert.open();

            return void 0;
          }

          this.goToConfirm();
          this.paymentServiceSub.unsubscribe();
        },
        () => {
          this.paymentServiceSub.unsubscribe();
        }
      );
    } else {
      this.location.back();
    }
  }

  goToConfirm(): void {
    const quote = this.paymentService.getCurrentQuote();
    quote.confirm = true;
    this.paymentService.setCurrentQuote(quote);

    // actualiza el badge del menu: numero pagos pendientes
    this.menuService.getNumberPendingPayments();

    this.router.navigate([`${this.router.url}/confirm`]);
  }

  // No usar Mapfre Dollars
  withOutMPD(): void {
    this.paymentService.onUseMapfreDolarsChange().next(false);
    this.cardSoat.switchChange(false);
    this.cardSoat.useMPD = false;
  }

  // Compra Gratis
  confirmPurchase(): void {
    const dataCard = null;
    // Actualiza storage
    this.quote = this.paymentService.getCurrentQuote();
    this.quote.paymentMode = PaymentMode.FREE;
    this.quote.total = 0;
    this.quote.email = this.clientService.getUserEmail();
    this.paymentService.setCurrentQuote(this.quote);

    if (this.isSoat) {
      this.goPayAndIssueSoat(dataCard, PaymentMode.FREE);
    } else {
      this.goPayReceipts(dataCard, PaymentMode.FREE);
    }
  }

  goBack(event): void {
    this.mfModalAlert.close();
  }

  private _getPolicyType(): string {
    const quote = this.paymentService.getCurrentQuote();
    const isNotice = quote.policyType === 'MD_AVISO';
    const policyType = isNotice
      ? 'Aviso'
      : POLICY_TYPES[quote.policyType].description || POLICY_TYPES[quote.policyType].title;

    return policyType;
  }
}
