import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PaymentQuoteConfirmGuard } from '@mx/guards/payments/payment-quote-confrm.guard';
import { PaymentQuoteGuard } from '@mx/guards/payments/payment-quote.guard';
import { PaymentQuotaComponent } from './payment-quota.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [PaymentQuoteGuard],
    component: PaymentQuotaComponent
  },
  {
    path: 'confirm',
    canActivate: [PaymentQuoteConfirmGuard],
    loadChildren: './payment-bank-confirm/payment-bank-confirm.module#PaymentBankConfirmModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentQuotaRoutingModule {}
