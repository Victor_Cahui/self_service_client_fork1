import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  CardQuotaModule,
  CardQuoteSoatModule,
  CardVigencySoatModule,
  PaymentMethodModule
} from '@mx/components/shared/payments';
import { PaymentFreeModule } from '@mx/components/shared/payments/payment-free/payment-free.module';
import { MfModalAlertModule } from '@mx/core/ui/lib/components/modals/modal-alert';
import { PaymentQuotaComponent } from './payment-quota.component';
import { PaymentQuotaRoutingModule } from './payment-quota.routing';

@NgModule({
  imports: [
    CommonModule,
    PaymentMethodModule,
    PaymentFreeModule,
    PaymentQuotaRoutingModule,
    CardQuotaModule,
    CardVigencySoatModule,
    CardQuoteSoatModule,
    MfModalAlertModule
  ],
  declarations: [PaymentQuotaComponent],
  exports: [PaymentQuotaComponent]
})
export class PaymentQuotaModule {}
