import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardPaymentsDetailModule } from '@mx/components/payment/card-payments-detail/card-payments-detail.module';
import { TabsPaymentsModule } from '@mx/components/payment/tabs-payments/tabs-payments.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { PaymentsListComponent } from './payments-list.component';

@NgModule({
  imports: [CommonModule, CardPaymentsDetailModule, GoogleModule, TabsPaymentsModule],
  declarations: [PaymentsListComponent]
})
export class PaymentsListModule {}
