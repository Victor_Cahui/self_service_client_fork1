import { Component } from '@angular/core';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import {
  MFP_Mis_Pagos_41B,
  MFP_Mis_Pagos_41C,
  MFP_Mis_Pagos_Filtrar_41D,
  MFP_Mis_Pagos_Pagar_42A
} from '@mx/settings/constants/events.analytics';

@Component({
  selector: 'client-payments-list',
  templateUrl: './payments-list.component.html'
})
export class PaymentsListComponent {
  ga: Array<IGaPropertie>;

  constructor() {
    this.ga = [MFP_Mis_Pagos_41B(), MFP_Mis_Pagos_41C(), MFP_Mis_Pagos_Filtrar_41D(), MFP_Mis_Pagos_Pagar_42A()];
  }
}
