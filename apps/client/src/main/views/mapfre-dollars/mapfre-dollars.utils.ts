import { FORMAT_DATE_DDMMYYYY, MovementStatus, movementsTypes } from '@mx/settings/constants/general-values';
import { CALENDAR_ICON, CAR_ICON, HOUSE_ICON, PERSON_ICON } from '@mx/settings/constants/images-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';

export function getIcoDescription(item): string {
  if (isAnniversaryOrCampaign(item.tipoMovimiento)) {
    return CALENDAR_ICON;
  }

  if (hasIconCar(item.tipoPoliza)) {
    return CAR_ICON;
  }

  if (hasIconHouse(item.tipoPoliza)) {
    return HOUSE_ICON;
  }

  return PERSON_ICON;
}

export function getNumberIntAndDecimal(completeNumber): any {
  const partInteger = Math.trunc(completeNumber);
  const numberDecimal = `${completeNumber}`.split('.')[1] || '00';
  const partDecimal = numberDecimal.padEnd(2, '0');

  return { partInteger, partDecimal };
}

export function mapMovements(m: any, datePipe: (x: any, y: any) => string): any[] {
  return {
    ...m,
    fechaMovimiento: datePipe(m.fechaMovimiento, FORMAT_DATE_DDMMYYYY),
    icoDescription: getIcoDescription(m),
    icoMovement: movementsTypes[m.tipoMovimiento].ico,
    isPositive: m.montoMovimiento > 0,
    montoMovimiento: addPlusToPositiveNumber(m.montoMovimiento)
  };
}

export function mapReview(r: any, datePipe: (x: any, y: any) => string): any {
  const { partInteger, partDecimal } = getNumberIntAndDecimal(r.montoDisponible);

  return {
    ...r,
    currentPartDecimal: partDecimal,
    currentPartInteger: getNumberWithThousandSymbol(partInteger, r.montoDisponible),
    fechaAniversario: datePipe(r.fechaAniversario, FORMAT_DATE_DDMMYYYY),
    fechaXVencer: datePipe(r.fechaXVencer, FORMAT_DATE_DDMMYYYY),
    hasBeatenMD: !!r.montoXVencer,
    montoGanado: numberWithCommas(r.montoGanado),
    montoUsado: numberWithCommas(r.montoUsado),
    montoXVencer: getIntOrWIthDecimal(r.montoXVencer)
  };
}

export function getMsgNotFound(movementType: string): string {
  const msg = {
    [MovementStatus.GANADO.code]: GeneralLang.Messages.WithOutMDWon,
    [MovementStatus.USADO.code]: GeneralLang.Messages.WithOutMDUsed,
    [MovementStatus.VENCIDO.code]: GeneralLang.Messages.WithOutMDBeaten,
    default: GeneralLang.Messages.WithOutMD
  };

  return msg[movementType] ? msg[movementType] : msg.default;
}

function isAnniversaryOrCampaign(movement: string): boolean {
  return [movementsTypes.Aniversario.code, movementsTypes.Campaña.code].includes(movement);
}

function hasIconCar(policyType: string): boolean {
  return [POLICY_TYPES.MD_AUTOS.code, POLICY_TYPES.MD_SOAT.code, POLICY_TYPES.MD_SOAT_ELECTRO.code].includes(
    policyType
  );
}

function hasIconHouse(policyType: string): boolean {
  return [POLICY_TYPES.MD_HOGAR.code].includes(policyType);
}

Math.trunc =
  // tslint:disable-next-line: no-unbound-method
  Math.trunc ||
  function(x: number): number {
    return x < 0 ? Math.ceil(x) : Math.floor(x);
  };

function getNumberWithThousandSymbol(
  partInteger: number | string,
  completeNumber: number,
  symbolToSeparate: string = ','
): string {
  const isNegativeZero = completeNumber < 0 && partInteger === 0;

  return `${isNegativeZero ? '-' : ''}${partInteger}`.replace(/\B(?=(\d{3})+(?!\d))/g, symbolToSeparate);
}

function joinIntegerAndDecimal(
  n: { partInteger: string; partDecimal: string },
  symbolDecimal: string = '.',
  completeNumber: number
): string {
  return `${getNumberWithThousandSymbol(n.partInteger, completeNumber)}${symbolDecimal}${n.partDecimal}`;
}

function numberWithCommas(completeNumber: number): string {
  return joinIntegerAndDecimal(getNumberIntAndDecimal(completeNumber), '.', completeNumber);
}

function getIntOrWIthDecimal(completeNumber: number): any {
  return Number.isInteger(completeNumber) ? completeNumber : numberWithCommas(completeNumber);
}

function addPlusToPositiveNumber(completeNumber: number): any {
  const isPositive = completeNumber > 0;
  const joinedNumber = joinIntegerAndDecimal(getNumberIntAndDecimal(completeNumber), '.', completeNumber);

  return isPositive ? `+${joinedNumber}` : joinedNumber;
}
