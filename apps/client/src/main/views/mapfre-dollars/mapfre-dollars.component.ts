import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { Autoservicios, FrmProvider } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { APPLICATION_CODE, MovementStatus } from '@mx/settings/constants/general-values';
import { DOCUMENT_ICON } from '@mx/settings/constants/images-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { getMsgNotFound, mapMovements, mapReview } from './mapfre-dollars.utils';

@Component({
  providers: [DatePipe],
  selector: 'client-mapfre-dollars-component',
  templateUrl: './mapfre-dollars.component.html'
})
export class MapfeDollarsComponent implements OnInit {
  frm: FormGroup;
  iconNotFound = DOCUMENT_ICON;
  isVisibleLoaderListMD: boolean;
  isVisibleLoaderReview: boolean;
  itemsPerPage = 10;
  lang = GeneralLang;
  list: any[];
  msgNotFound;
  page = 1;
  review: any;
  showFilter: boolean;
  total: number;
  movementStatus = MovementStatus;

  constructor(
    private readonly _AuthService: AuthService,
    private readonly _Autoservicios: Autoservicios,
    private readonly _DatePipe: DatePipe,
    private readonly _FrmProvider: FrmProvider
  ) {}

  ngOnInit(): void {
    this._initValues();
    this._getReview();
    this.itemsPerPageChange();
  }

  itemsPerPageChange(itemsPerPage?: number): void {
    this.page = 1;
    this.itemsPerPage = itemsPerPage || this.itemsPerPage;
    this._configNotFound();
    this._getListMD();
  }

  pageChange(page: number): void {
    this.page = page;
    this._configNotFound();
    this._getListMD();
  }

  saveInfoMD(): void {
    const pathReq = { codigoApp: APPLICATION_CODE };
    const bodyReq = {
      flagInformacion: true
    };
    this._Autoservicios.GuardarInformacionMD(pathReq, bodyReq).subscribe(() => {
      this.review = { ...this.review, flagInformacion: true };
      // tslint:disable-next-line: no-console no-unbound-method
    }, console.error);
  }

  private _getListMD(): void {
    const pathReq = { codigoApp: APPLICATION_CODE };
    const bodyReq = {
      registros: this.itemsPerPage,
      pagina: this.page,
      estado: this.frm.value.estado
    };

    this.showFilter = false;
    this.isVisibleLoaderListMD = true;
    this.list = [];
    this.total = 0;

    this._Autoservicios.ObtenerDetallePaginadoMD(pathReq, bodyReq).subscribe(this._mapRespGetListMD.bind(this), () => {
      this.isVisibleLoaderListMD = false;
    });
  }

  private _getReview(): void {
    this.isVisibleLoaderReview = true;
    const pathReq = { codigoApp: APPLICATION_CODE };

    this._Autoservicios.ObtenerResumenMD(pathReq).subscribe(
      resp => {
        this.review = mapReview(resp, this._DatePipe.transform.bind(this._DatePipe));
        this.isVisibleLoaderReview = false;
      },
      () => {
        this.isVisibleLoaderReview = false;
      }
    );
  }

  private _mapRespGetListMD(r: any): void {
    if (!r) {
      this.isVisibleLoaderListMD = false;

      return void 0;
    }
    this.list = r.movimientos.map(m => mapMovements(m, this._DatePipe.transform.bind(this._DatePipe)));
    this.total = r.totalRegistros;
    this.isVisibleLoaderListMD = false;
  }

  private _configNotFound(): void {
    this.iconNotFound = 'assets/images/services/mapfre-dolares.svg';
    this.msgNotFound = getMsgNotFound(this.frm.value.estado);
  }

  private _initValues(): void {
    this.frm = this._FrmProvider.MapfeDollarsComponent();
    this.frm.valueChanges.subscribe(() => {
      this.itemsPerPageChange();
    });
  }
}
