import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { MfTabTopModule } from '@mx/core/ui/lib/components/tabs/tabs-top';
import { MapfreServicesComponent } from './mapfre-services.component';
import { MapfreServicesRoutingModule } from './mapfre-services.routing';

@NgModule({
  imports: [CommonModule, MapfreServicesRoutingModule, MfTabTopModule, GoogleModule],
  exports: [],
  declarations: [MapfreServicesComponent],
  providers: []
})
export class MapfreServicesModule {}
