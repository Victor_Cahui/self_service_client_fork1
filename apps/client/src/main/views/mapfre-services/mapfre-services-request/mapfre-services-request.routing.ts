import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MapfreServicesRequestComponent } from './mapfre-services-request.component';

const routes: Routes = [
  {
    path: '',
    component: MapfreServicesRequestComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MapfreServicesRequestRouting {}
