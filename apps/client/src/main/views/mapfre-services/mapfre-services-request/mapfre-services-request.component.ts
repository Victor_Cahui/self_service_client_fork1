import { Component, OnInit, ViewChild } from '@angular/core';
import { InputSearchComponent } from '@mx/components/shared/input-search/input-search.component';
import { GAService, GaUnsubscribeBase, IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { ServicesMFService } from '@mx/services/services-mf.service';
import {
  MFP_Servicios_Mapfre_46B,
  MFP_Servicios_Mapfre_46C,
  MFP_Servicios_Mapfre_46D
} from '@mx/settings/constants/events.analytics';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { MAPFRE_SERVICES_SECTIONS } from '@mx/settings/constants/services-mf';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { ServicesLang } from '@mx/settings/lang/services.lang';
import { IClientInformationResponse } from '@mx/statemanagement/models/client.models';
import { IClientSection, IClientServiceDetail } from '@mx/statemanagement/models/service.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-mapfre-services-request',
  templateUrl: './mapfre-services-request.component.html'
})
export class MapfreServicesRequestComponent extends GaUnsubscribeBase implements OnInit {
  @ViewChild(InputSearchComponent) inputSearch: InputSearchComponent;
  titleServicesRequest = ServicesLang.Titles.ServicesRequest;
  messageServicesRequest = ServicesLang.Messages.ServicesRequest;
  serviceNotFound = ServicesLang.Messages.ServiceNotFound;
  all = GeneralLang.Texts.All;
  select = GeneralLang.Labels.Select;
  serviceList: Array<IClientServiceDetail>;
  serviceListItems: Array<IClientServiceDetail>;
  sectionList: Array<IClientSection>;
  userData: IClientInformationResponse;

  servicesMFSub: Subscription;
  clientInfoSub: Subscription;
  configurationSub: Subscription;
  tipoUsuario: string;
  selectedItemMenu: string;
  hasUserTypeKey: boolean;
  keysToGroup: Array<string>;
  cardsGrouped: boolean;
  showLoading: boolean;

  ga: Array<IGaPropertie>;

  constructor(
    private readonly clientService: ClientService,
    private readonly configurationService: ConfigurationService,
    public authService: AuthService,
    private readonly servicesMFService: ServicesMFService,
    protected gaService: GAService
  ) {
    super(gaService);
    this.sectionList = [];
    this.selectedItemMenu = 'ALL';
    this.keysToGroup = ['MD_AUTOS', 'MD_SOAT', 'MD_SOAT_ELECTRO'];
    this.ga = [MFP_Servicios_Mapfre_46B(), MFP_Servicios_Mapfre_46C(), MFP_Servicios_Mapfre_46D()];
  }

  ngOnInit(): void {
    // Borra storage
    this.servicesMFService.clearServiceHistoryData();

    // Busca datos de usuario en Storage
    const data = this.clientService.getUserData();

    if (data) {
      this.setUserType(data.tipoUsuario);
    } else {
      this.clientInfoSub = this.clientService.getClientInformation({}).subscribe(
        (response: IClientInformationResponse) => {
          this.userData = response;
          this.setUserType(response.tipoUsuario);
        },
        () => {},
        () => {
          // Guarda en storage
          this.clientService.setUserData(this.userData);
          this.clientInfoSub.unsubscribe();
        }
      );
    }
    // Servicios
    this.showLoading = true;
    this.servicesMFSub = this.servicesMFService.getServicesList({ codigoApp: APPLICATION_CODE }).subscribe(
      response => {
        if (response && response.length) {
          this.serviceList = response;
          this.serviceListItems = response;
          this.generateSections(response);
        } else {
          this.serviceNotFound = ServicesLang.Messages.ServicesNotAssociated;
        }
      },
      () => {
        this.showLoading = false;
      },
      () => {
        this.showLoading = false;
        this.servicesMFSub.unsubscribe();
      }
    );
  }

  setUserType(userType): void {
    this.tipoUsuario = userType;
    if (this.hasUserTypeKey) {
      this.sectionList[this.sectionList.length - 1].name = this.tipoUsuario;
    }
  }

  onFilter(searchValue: string): void {
    if (searchValue.length >= 3) {
      this.selectedItemMenu = 'ALL';
      this.serviceListItems = this.serviceList.filter(s => {
        // tslint:disable-next-line: no-parameter-reassignment
        searchValue = StringUtil.withoutDiacritics(searchValue.toLowerCase());
        // PREVIOUS FEATURE: busqueda por titulo sin descomponer en palabras
        // return (this.formatSearch(s.titulo.toLowerCase())).startsWith(searchValue.trim());

        return StringUtil.withoutDiacritics(s.titulo.toLowerCase())
          .split(' ')
          .find(e => e.startsWith(searchValue));
      });
    }
  }

  clearFilter(): void {
    this.serviceListItems = this.serviceList;
  }

  generateSections(response: any): void {
    let sectionList = new Array();
    response.forEach(item => {
      (item.seccion || []).forEach(section => {
        sectionList.push(section);
      });
    });
    sectionList = sectionList.filter((el, i, a) => i === a.indexOf(el)); // filtering duplicates
    this.hasUserTypeKey = !!sectionList.filter(el => el === 'MD_TIPO_USUARIO').length;
    if (this.hasUserTypeKey) {
      sectionList = sectionList.filter(el => el !== 'MD_TIPO_USUARIO');
      sectionList.push('MD_TIPO_USUARIO'); // set MD_TIPO_USUARIO al final
    }
    sectionList.forEach(itemKey => {
      let sectionName: IClientSection;
      sectionName = MAPFRE_SERVICES_SECTIONS.find(c => {
        return c.key === itemKey;
      });
      this.sectionList.push(sectionName);
    });
    this.sectionList = this.sectionList.filter(sec => sec);
    if (this.hasUserTypeKey) {
      this.sectionList[this.sectionList.length - 1].name = this.tipoUsuario;
    }
    this.cardsGrouped = !!this.sectionList.filter(el => this.keysToGroup.find(e => e === el.key)).length; // cards agrupados?
    if (this.cardsGrouped) {
      this.sectionList = this.sectionList.filter(el => this.keysToGroup.indexOf(el.key) === -1);
      this.sectionList.unshift({ key: 'MD_GROUP_AUTOS', name: 'Autos' });
    }
  }

  doSectionFilter(key): void {
    this.inputSearch.clearText(); // limpiar input-search al cambiar de sección
    this.selectedItemMenu = key;

    switch (key) {
      case 'ALL':
        this.serviceListItems = this.serviceList;
        break;
      case 'MD_GROUP_AUTOS':
        this.serviceListItems = this.serviceList.filter(serviceCard => {
          return serviceCard.seccion.find(policyType => this.keysToGroup.indexOf(policyType) !== -1);
        });
        break;
      default:
        this.serviceListItems = this.serviceList.filter(serviceCard => {
          return (serviceCard.seccion || []).find(policyType => policyType === key);
        });
        break;
    }
  }
}
