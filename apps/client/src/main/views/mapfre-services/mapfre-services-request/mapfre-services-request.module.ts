import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { InputSearchModule } from '@mx/components/shared/input-search/input-search.module';
import { ItemNotFoundModule } from '@mx/components/shared/item-not-found/item-not-found.module';
import { LandingModule } from '@mx/components/shared/landing/landing.module';
import { ServiceCardModule } from '@mx/components/shared/mapfre-services/service-card/service-card.module';
import { PageTitleModule } from '@mx/components/shared/page-title/page-title.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { MfCardModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { MapfreServicesRequestComponent } from './mapfre-services-request.component';
import { MapfreServicesRequestRouting } from './mapfre-services-request.routing';

@NgModule({
  imports: [
    CommonModule,
    MapfreServicesRequestRouting,
    MfCardModule,
    LandingModule,
    ServiceCardModule,
    FormsModule,
    ItemNotFoundModule,
    InputSearchModule,
    MfLoaderModule,
    PageTitleModule,
    GoogleModule
  ],
  declarations: [MapfreServicesRequestComponent]
})
export class MapfreServicesRequestModule {}
