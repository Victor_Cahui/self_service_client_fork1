import { Component, OnInit } from '@angular/core';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { EMERGENCY_PHONE } from '@mx/settings/constants/key-values';
import { AMBULANCE_SERVICES_HEADER } from '@mx/settings/constants/router-titles';
import { LANDING_AMBULANCE_SERVICES } from '@mx/settings/lang/landing';
import { ServicesLang } from '@mx/settings/lang/services.lang';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-ambulance-services',
  templateUrl: './ambulance-services.component.html'
})
export class AmbulanceServicesComponent implements OnInit {
  titleAmbulanceServices = ServicesLang.Titles.AmbulanceServices;
  messageAmbulanceServices = ServicesLang.Messages.AmbulanceServices;
  textAmbulanceServicesHelp = ServicesLang.Messages.AmbulanceServicesHelp;
  textAmbulanceServicesHelpMsgWhatsapp = ServicesLang.Messages.AmbulanceServicesHelpMsgWhatsapp;
  landingContent = LANDING_AMBULANCE_SERVICES;
  clientInfoSub: Subscription;
  phoneKey = EMERGENCY_PHONE;

  constructor(private readonly headerHelperService: HeaderHelperService) {}

  ngOnInit(): void {
    this.headerHelperService.set(AMBULANCE_SERVICES_HEADER);
  }
}
