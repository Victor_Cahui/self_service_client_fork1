import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NotificationHelpModule } from '@mx/components/notifications/notification-help/notification-help.module';
import { BannerAppDownloadModule } from '@mx/components/shared/banner-app-download/banner-app-download.module';
import { LandingModule } from '@mx/components/shared/landing/landing.module';
import { PageTitleModule } from '@mx/components/shared/page-title/page-title.module';
import { AmbulanceServicesComponent } from './ambulance-services.component';
import { AmbulanceServicesRouting } from './ambulance-services.routing';

@NgModule({
  imports: [
    CommonModule,
    AmbulanceServicesRouting,
    PageTitleModule,
    NotificationHelpModule,
    BannerAppDownloadModule,
    LandingModule
  ],
  declarations: [AmbulanceServicesComponent]
})
export class AmbulanceServicesModule {}
