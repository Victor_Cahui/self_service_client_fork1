import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormBaseModule } from '@mx/components/shared/form-base/form-base.module';
import { IconPoliciesModule } from '@mx/components/shared/icon-policies/icon-polices.module';
import { ModalHowWorksModule } from '@mx/components/shared/modals/modal-how-works/modal-how-works.module';
import { MfButtonModule } from '@mx/core/ui';
import { StepperModule } from '@mx/core/ui/lib/components/stepper/stepper.module';
import { HomeDoctorRequestStep3Component } from './home-doctor-request-step-3.component';

@NgModule({
  imports: [CommonModule, StepperModule, MfButtonModule, ModalHowWorksModule, FormBaseModule, IconPoliciesModule],
  declarations: [HomeDoctorRequestStep3Component]
})
export class HomeDoctorRequestStep3Module {}
