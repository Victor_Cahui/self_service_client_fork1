import { Location } from '@angular/common';
import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ModalHowWorksComponent } from '@mx/components/shared/modals/modal-how-works/modal-how-works.component';
import { ScrollUtil } from '@mx/core/shared/helpers/util/scroll';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { UnsubscribeOnDestroy } from '@mx/core/shared/helpers/util/unsubscribe-on-destroy';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { GeneralService } from '@mx/services/general/general.service';
import { APPLICATION_CODE, DEFAULT_HOME_DOCTOR_HOURS, STORAGE } from '@mx/settings/constants/general-values';
import { CIRCLE_CHECK_ICON, CROSS_ICON, PERSON_ICON, PHONE_ICON, PIN_ICON } from '@mx/settings/constants/images-values';
import * as KEYS_VALUES from '@mx/settings/constants/key-values';
import { HOME_DOCTOR_STEP } from '@mx/settings/constants/router-step';
import { HOME_DOCTOR_HEADER } from '@mx/settings/constants/router-titles';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { HOME_DOCTOR_LANG } from '@mx/settings/lang/health.lang';
import { HowHomeDoctorWorks, ServiceFormLang } from '@mx/settings/lang/services.lang';
import { IModalHowWorks } from '@mx/statemanagement/models/general.models';
import { IHomeDoctorRequestSummary } from '@mx/statemanagement/models/health.interface';
import { isEmpty } from 'lodash-es';
import { takeUntil } from 'rxjs/internal/operators/takeUntil';

@Component({
  selector: 'client-home-doctor-request-step-3',
  templateUrl: './home-doctor-request-step-3.html'
})
export class HomeDoctorRequestStep3Component extends UnsubscribeOnDestroy implements AfterViewInit {
  @ViewChild(ModalHowWorksComponent) mfModalHowWorks: ModalHowWorksComponent;
  @ViewChild('scrollMe') private readonly myScrollContainer: ElementRef;
  graLng = GeneralLang;
  lang = HOME_DOCTOR_LANG;
  stepList = HOME_DOCTOR_STEP;
  crossIcon = CROSS_ICON;
  personIcon = PERSON_ICON;
  phoneIcon = PHONE_ICON;
  pinIcon = PIN_ICON;
  check = CIRCLE_CHECK_ICON;
  openModalHowWorks: boolean;
  infoFrmOne: any;
  infoFrmTwo: any;
  infoFrmTwoData: any;
  summaryModel: IHomeDoctorRequestSummary;
  howItWorks: IModalHowWorks;
  showLoading: boolean;
  registrationCompleted: boolean;
  currentServiceSelected: any;
  btnDone = GeneralLang.Buttons.Understood;
  header = HOME_DOCTOR_HEADER;
  module = 'CDM';

  constructor(
    protected readonly _StorageService: StorageService,
    protected readonly _GeneralService: GeneralService,
    protected readonly _autoservicios: Autoservicios,
    protected location: Location,
    private readonly router: Router
  ) {
    super();
    this._initValues();
  }

  ngAfterViewInit(): void {
    this.scrollToBottom();
  }

  private _initValues(): void {
    if (!this.validInformation()) {
      this.router.navigate(['/home']);
    } else {
      this.setValuesModal(HowHomeDoctorWorks, KEYS_VALUES.HOME_DOCTOR_HOURS, DEFAULT_HOME_DOCTOR_HOURS);
      this.setInformation();
    }
  }

  private scrollToBottom(): void {
    try {
      const offsetTop = this.myScrollContainer.nativeElement.scrollHeight;
      ScrollUtil.goTo(offsetTop);
    } catch (err) {
      console.error(err);
    }
  }

  private validInformation(): boolean {
    this.infoFrmOne = this._StorageService.getStorage(STORAGE.HOME_DOCTOR_REQUEST_STEP_ONE);
    this.infoFrmTwo = this._StorageService.getStorage(STORAGE.HOME_DOCTOR_REQUEST_STEP_TWO);
    this.infoFrmTwoData = this._StorageService.getStorage(STORAGE.HOME_DOCTOR_REQUEST_STEP_TWO_DATA);

    return !(isEmpty(this.infoFrmOne) || isEmpty(this.infoFrmTwo) || isEmpty(this.infoFrmTwoData));
  }

  private setInformation(): void {
    this.currentServiceSelected = this.infoFrmTwoData;
    const specialty = this.currentServiceSelected.especialidades.find(
      el => el.identificador === +this.infoFrmTwo.specialty
    );
    const patient = this.currentServiceSelected.beneficiarios.find(el => el.identificador === this.infoFrmTwo.patient);
    this.summaryModel = {
      specialty: specialty.nombre,
      patient: patient.nombre,
      telephone: this.infoFrmTwo.telephone,
      address: this.infoFrmOne.endPoint,
      referenceAddress: this.infoFrmOne.referenceEndPoint,
      price: isEmpty(this.currentServiceSelected.precio)
        ? ServiceFormLang.Free
        : this.currentServiceSelected.precio.precio,
      currencySymbol: `${StringUtil.getMoneyDescription(this.currentServiceSelected.precio.codigoMoneda)} `
    };
  }

  private setValuesModal(data: IModalHowWorks, key: string, defaultHours: number): void {
    this.howItWorks = data;
    this._GeneralService
      .getParameters()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(() => {
        let hours = this._GeneralService.getValueParams(key);
        hours = hours ? hours : defaultHours.toString();
        if (this.howItWorks.warning) {
          this.howItWorks.warning = this.howItWorks.warning.replace('{{hours}}', hours);
        }
      });
  }

  private getBodyRequestHomeDoctor(): any {
    return {
      destino: {
        puntoLlegada: this.infoFrmOne.endPoint,
        referenciaLlegada: this.infoFrmOne.referenceEndPoint,
        latitud: this.infoFrmOne.endPointCoords.latitude,
        longitud: this.infoFrmOne.endPointCoords.longitude
      },
      producto: this.infoFrmTwoData.producto,
      pago: {
        codigoMoneda: this.infoFrmTwoData.precio.codigoMoneda,
        precio: this.infoFrmTwoData.precio.precio,
        esGratis: this.infoFrmTwoData.precio.precio === 0
      },
      telefono: this.infoFrmTwo.telephone,
      sintomas: this.infoFrmTwo.reasonForConsultation,
      beneficiario: this.infoFrmTwo.patient,
      especialidad: this.infoFrmTwo.specialty
    };
  }

  public clearDataStorage(): void {
    this._StorageService.removeItem(STORAGE.HOME_DOCTOR_REQUEST_STEP_ONE);
    this._StorageService.removeItem(STORAGE.HOME_DOCTOR_REQUEST_STEP_TWO);
    this._StorageService.removeItem(STORAGE.HOME_DOCTOR_REQUEST_STEP_TWO_DATA);
  }

  viewInfo(): void {
    this.openModalHowWorks = true;
    setTimeout(() => {
      this.mfModalHowWorks.open();
    }, 100);
  }

  public actionCancel(): void {
    this.location.back();
  }

  public actionConfirm(): void {
    this.showLoading = true;
    const body = this.getBodyRequestHomeDoctor();
    this._autoservicios.SolicitarMedicoDomicilio({ codigoApp: APPLICATION_CODE }, body).subscribe(
      response => {
        this.clearDataStorage();
        this.registrationCompleted = true;
        this._GeneralService.saveUserMovements(
          this.module,
          this._GeneralService.action(KEYS_VALUES.CDM_PROCEDURES.PROC_MEDICO_DOMICILIO_CDM),
          this._GeneralService.action(KEYS_VALUES.CDM_DESCRIPTION.DSC_FIN)
        );
      },
      error => {
        this.showLoading = false;
        console.error(error);
      }
    );
  }

  public done(): void {
    this.router.navigate(['/home']);
  }
}
