import { DecimalPipe } from '@angular/common';
import { Component, OnDestroy } from '@angular/core';
import { GeneralService } from '@mx/services/general/general.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { HOME_DOCTOR_HEADER } from '@mx/settings/constants/router-titles';

@Component({
  selector: 'client-home-doctor-request',
  templateUrl: './home-doctor-request.html',
  providers: [DecimalPipe]
})
export class HomeDoctorRequestComponent implements OnDestroy {
  constructor(
    private readonly headerHelperService: HeaderHelperService,
    private readonly generalService: GeneralService
  ) {
    this.headerHelperService.set(HOME_DOCTOR_HEADER);
  }

  ngOnDestroy(): void {
    this.generalService.showChatGenesys.next(true);
  }
}
