import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeDoctorRequestStep1Component } from './home-doctor-request-step-1/home-doctor-request-step-1.component';
import { HomeDoctorRequestStep2Component } from './home-doctor-request-step-2/home-doctor-request-step-2.component';
import { HomeDoctorRequestStep3Component } from './home-doctor-request-step-3/home-doctor-request-step-3.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '1'
  },
  {
    path: '1',
    component: HomeDoctorRequestStep1Component
  },
  {
    path: '2',
    component: HomeDoctorRequestStep2Component
  },
  {
    path: '3',
    component: HomeDoctorRequestStep3Component
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeDoctorRequestRouting {}
