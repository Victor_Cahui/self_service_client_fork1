import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBaseModule } from '@mx/components/shared/form-base/form-base.module';
import { ModalHowWorksModule } from '@mx/components/shared/modals/modal-how-works/modal-how-works.module';
import { MfInputModule, MfSelectModule, MfShowErrorsModule, MfTextAreaModule } from '@mx/core/ui';
import { StepperModule } from '@mx/core/ui/lib/components/stepper/stepper.module';
import { HomeDoctorRequestStep2Component } from './home-doctor-request-step-2.component';

@NgModule({
  imports: [
    CommonModule,
    StepperModule,
    FormBaseModule,
    MfSelectModule,
    MfInputModule,
    MfShowErrorsModule,
    FormsModule,
    ReactiveFormsModule,
    MfTextAreaModule,
    ModalHowWorksModule
  ],
  declarations: [HomeDoctorRequestStep2Component]
})
export class HomeDoctorRequestStep2Module {}
