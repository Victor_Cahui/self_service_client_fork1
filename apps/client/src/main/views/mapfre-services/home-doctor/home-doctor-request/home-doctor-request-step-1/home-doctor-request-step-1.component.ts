import { DOCUMENT } from '@angular/common';
import { Component, Inject, Renderer2 } from '@angular/core';
import { GmMsLayoutBase } from '@mx/components/gm-ms-views/gm-ms-base-layout';
import { GeneralService } from '@mx/services/general/general.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { ServicesMFService } from '@mx/services/services-mf.service';
import { PIN_AMBULANCE_ICON } from '@mx/settings/constants/images-values';
import { HOME_DOCTOR_HEADER } from '@mx/settings/constants/router-titles';
import { IUserMarker } from '@mx/statemanagement/models/google-map.interface';

@Component({
  selector: 'client-home-doctor-request-step-1',
  templateUrl: './home-doctor-request-step-1.html'
})
export class HomeDoctorRequestStep1Component extends GmMsLayoutBase {
  endMarker: IUserMarker;
  constructor(
    protected servicesMfService: ServicesMFService,
    protected render2: Renderer2,
    protected headerHelperService: HeaderHelperService,
    protected readonly generalService: GeneralService,
    @Inject(DOCUMENT) protected document: Document
  ) {
    super(servicesMfService, 'getAllowedAreasDoctor', render2, headerHelperService, document);
    this.header = HOME_DOCTOR_HEADER;
    this.useFocusedSearch = true;
    this.useFilterMapService = false;
    this.endMarker = {
      pinUrl: PIN_AMBULANCE_ICON
    };
    this.generalService.showChatGenesys.next(false);
  }
}
