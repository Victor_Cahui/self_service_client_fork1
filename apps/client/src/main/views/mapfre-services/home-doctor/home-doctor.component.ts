import { DOCUMENT } from '@angular/common';
import { Component, Inject, Renderer2 } from '@angular/core';
import { GmMsLayoutBase } from '@mx/components/gm-ms-views/gm-ms-base-layout';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { ServicesMFService } from '@mx/services/services-mf.service';
import { HOME_DOCTOR_STEP } from '@mx/settings/constants/router-step';
import { HOME_DOCTOR_HEADER } from '@mx/settings/constants/router-titles';

@Component({
  selector: 'client-home-doctor',
  templateUrl: './home-doctor.component.html'
})
export class HomeDoctorComponent extends GmMsLayoutBase {
  stepList = HOME_DOCTOR_STEP;

  constructor(
    protected servicesMfService: ServicesMFService,
    protected render2: Renderer2,
    protected headerHelperService: HeaderHelperService,
    @Inject(DOCUMENT) protected document: Document
  ) {
    super(servicesMfService, 'getAllowedAreasDoctor', render2, headerHelperService, document);
    this.header = HOME_DOCTOR_HEADER;
  }
}
