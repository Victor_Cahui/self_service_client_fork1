import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MapfreServicesComponent } from './mapfre-services.component';

const routes: Routes = [
  {
    path: 'ambulance',
    loadChildren: './ambulance-services/ambulance-services.module#AmbulanceServicesModule'
  },
  {
    path: 'substitute-driver',
    loadChildren: './substitute-driver/substitute-driver.module#SubstituteDriverModule'
  },
  {
    path: 'home-doctor',
    loadChildren: './home-doctor/home-doctor.module#HomeDoctorModule'
  },
  {
    path: '',
    component: MapfreServicesComponent,
    children: [
      {
        path: '',
        redirectTo: 'request',
        pathMatch: 'prefix'
      },
      {
        path: 'request',
        loadChildren: './mapfre-services-request/mapfre-services-request.module#MapfreServicesRequestModule'
      },
      {
        path: 'assists',
        loadChildren: './mapfre-services-assists/mapfre-services-assists.module#MapfreServicesAssistsModule'
      }
    ]
  },
  {
    path: 'schedule-detail',
    loadChildren: './schedule-appointment/detail/schedule-detail.module#ScheduleDetailModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MapfreServicesRoutingModule {}
