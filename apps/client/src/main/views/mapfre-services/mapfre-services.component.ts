import { Component, OnInit } from '@angular/core';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { MFP_Servicios_Mapfre_46A } from '@mx/settings/constants/events.analytics';
import { MAPFRE_SERVICES_TAB } from '@mx/settings/constants/router-tabs';
import { ITabItem } from '@mx/statemanagement/models/router.interface';

@Component({
  selector: 'client-mapfre-services-component',
  templateUrl: './mapfre-services.component.html'
})
export class MapfreServicesComponent implements OnInit {
  tabItemsList: Array<ITabItem>;

  ga: Array<IGaPropertie>;

  constructor() {
    this.tabItemsList = MAPFRE_SERVICES_TAB;
    this.ga = [MFP_Servicios_Mapfre_46A()];
  }

  ngOnInit(): void {}
}
