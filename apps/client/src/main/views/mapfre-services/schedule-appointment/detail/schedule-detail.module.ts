import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FormBaseModule } from '@mx/components/shared/form-base/form-base.module';
import { MfButtonModule, MfModalAlertModule, MfModalMessageModule } from '@mx/core/ui';
import { MfCustomAlertModule } from '@mx/core/ui/lib/components/alerts/custom-alert';
import { TooltipsModule } from '@mx/core/ui/lib/components/tooltips/tooltips.module';
import { ScheduleDetailComponent } from './schedule-detail.component';
import { ScheduleDetailGuard } from './schedule-detail.guard';
import { ScheduleDetailRoutingModule } from './schedule-detail.routing';

@NgModule({
  declarations: [ScheduleDetailComponent],
  imports: [
    CommonModule,
    FormBaseModule,
    FormsModule,
    MfCustomAlertModule,
    MfButtonModule,
    MfModalAlertModule,
    MfModalMessageModule,
    ReactiveFormsModule,
    ScheduleDetailRoutingModule,
    TooltipsModule
  ],
  providers: [ScheduleDetailGuard]
})
export class ScheduleDetailModule {}
