import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { UnsubscribeOnDestroy } from '@mx/core/shared/helpers/util/unsubscribe-on-destroy';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AppointmentFacade, State } from '@mx/core/shared/state/appointment';
import { NotificationStatus } from '@mx/core/ui/lib/components/alerts/custom-alert/custom-alert.component';
import { MfModalAlertComponent } from '@mx/core/ui/lib/components/modals/modal-alert/modal-alert.component';
import { MfModalMessage } from '@mx/core/ui/lib/components/modals/modal-message';
import { GeneralService } from '@mx/services/general/general.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { APPLICATION_CODE, TYPE_APPOINTMENT } from '@mx/settings/constants/general-values';
import { FILE_ICON, TIME_ALERT_ICON, WARNING_ICON } from '@mx/settings/constants/images-values';
import * as KEYS_VALUES from '@mx/settings/constants/key-values';
import { APPOINTMENT_DETAIL, APPOINTMENT_DETAIL_COVID } from '@mx/settings/constants/router-titles';
import { GeneralLang } from '@mx/settings/lang/general.lang';

@Component({
  selector: 'client-schedule-detail',
  templateUrl: './schedule-detail.component.html'
})
export class ScheduleDetailComponent extends UnsubscribeOnDestroy implements OnInit {
  @ViewChild(MfModalAlertComponent) mfModalConfirm: MfModalAlertComponent;
  @ViewChild(MfModalMessage) modalMessage: MfModalMessage;

  typeAppoiment = TYPE_APPOINTMENT;
  appointment: State;
  graLng = GeneralLang;
  iconError: string = WARNING_ICON;
  iconTime = TIME_ALERT_ICON;
  fileIcon = FILE_ICON;
  showLoading = true;
  tooltip = {};
  especialityCovid = [104, 503];
  covidIsActive = false;
  status = NotificationStatus.INFO;
  module = 'CDM';

  constructor(
    private readonly _AppointmentFacade: AppointmentFacade,
    private readonly _Location: Location,
    private readonly _Autoservicios: Autoservicios,
    private readonly _GeneralService: GeneralService,
    private readonly _Router: Router,
    private readonly _HeaderHelperService: HeaderHelperService,
    private readonly _LocalStorageService: LocalStorageService
  ) {
    super();
  }

  ngOnInit(): void {
    this._initValues();
    this._mapState();
  }

  actionCancel(): void {
    this._Location.back();
  }

  onConfirmModal(r: boolean): void {
    if (!r) {
      this._GeneralService.saveUserMovements(
        this.module,
        this._GeneralService.action(KEYS_VALUES.CDM_PROCEDURES.PROC_ANULAR_CITA),
        this._GeneralService.action(KEYS_VALUES.CDM_DESCRIPTION.DSC_CANCELAR)
      );

      return void 0;
    }

    this.delete();
  }

  edit(): void {
    this.showLoading = true;
    this._GeneralService.saveUserMovements(
      this.module,
      this._GeneralService.action(KEYS_VALUES.CDM_PROCEDURES.PROC_MODIFICAR_CITA),
      this._GeneralService.action(KEYS_VALUES.CDM_DESCRIPTION.DSC_INICIO)
    );
    this._Autoservicios
      .ObtenerCita({ citaId: `${this.appointment.citaId}` }, { codigoApp: APPLICATION_CODE })
      .subscribe(
        cita => {
          this.showLoading = false;
          if (!cita.puedeModificar) {
            this.modalMessage.open();

            return void 0;
          }
          // SAVE APPOINMENT ON LOCAL STORAGE ONLY IF TYPE IS PRESENTIAL
          if (cita.tipoCita !== TYPE_APPOINTMENT.VIRTUAL) {
            this._LocalStorageService.setData(KEYS_VALUES.LOCAL_APPOINTMENT_EDIT, {
              appointmentIsOnEdit: true,
              citaId: this.appointment.citaId,
              tipoCita: cita.tipoCita
            });
            this._GeneralService.editAppointmentsBody.next(cita);
          }

          this._Router.navigate(['/digital-clinic/schedule-detail/3']);
        },
        () => {
          this.showLoading = false;
        }
      );
  }

  openModal(): void {
    this.showLoading = true;
    this._GeneralService.saveUserMovements(
      this.module,
      this._GeneralService.action(KEYS_VALUES.CDM_PROCEDURES.PROC_ANULAR_CITA),
      this._GeneralService.action(KEYS_VALUES.CDM_DESCRIPTION.DSC_INICIO)
    );
    this._Autoservicios
      .ObtenerCita({ citaId: `${this.appointment.citaId}` }, { codigoApp: APPLICATION_CODE })
      .subscribe(
        cita => {
          this.showLoading = false;
          if (!cita.puedeEliminar) {
            this.modalMessage.open();

            return void 0;
          }
          this.mfModalConfirm.open();
        },
        () => {
          this.showLoading = false;
        }
      );
  }

  delete(): void {
    this.showLoading = true;
    this._Autoservicios.AnularCita({ codigoApp: APPLICATION_CODE, citaId: this.appointment.citaId }).subscribe(
      () => {
        this.showLoading = false;
        this._AppointmentFacade.CleanAppointment();
        this._Router.navigate(['/digital-clinic/assists']);
      },
      () => {
        this._AppointmentFacade.CleanAppointment();
        this._Router.navigate(['/digital-clinic/assists']);
        this.showLoading = false;
      }
    );

    this._GeneralService.saveUserMovements(
      this.module,
      this._GeneralService.action(KEYS_VALUES.CDM_PROCEDURES.PROC_ANULAR_CITA),
      this._GeneralService.action(KEYS_VALUES.CDM_DESCRIPTION.DSC_FIN)
    );
  }

  private _mapState(): void {
    const getAppointment$ = this._AppointmentFacade.getAppointment$.subscribe(st => {
      if (st.citaId) {
        this.showLoading = false;
      }
      this.appointment = st;
    });
    if (this.especialityCovid.includes(this.appointment.especialidad.especialidadId)) {
      this.covidIsActive = true;
      this._HeaderHelperService.set(APPOINTMENT_DETAIL_COVID);
    } else {
      this._HeaderHelperService.set(APPOINTMENT_DETAIL);
    }
    this.arrToDestroy.push(getAppointment$);
  }

  private _initValues(): void {
    const infoDAA = this._GeneralService.getValueParams(KEYS_VALUES.INFO_DAA);
    this.tooltip = {
      body: infoDAA,
      title: ''
    };
  }
}
