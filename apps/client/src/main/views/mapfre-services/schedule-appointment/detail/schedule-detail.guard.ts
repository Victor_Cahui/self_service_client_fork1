import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

import { AppointmentFacade } from '@mx/core/shared/state/appointment';

@Injectable()
export class ScheduleDetailGuard implements CanActivate {
  constructor(private readonly _AppointmentFacade: AppointmentFacade) {}

  canActivate(): boolean {
    // TODO: comment for develop
    // const st = this._AppointmentFacade.getState();
    // if (st && !st.toEdit) {
    //   return false;
    // }

    return true;
  }
}
