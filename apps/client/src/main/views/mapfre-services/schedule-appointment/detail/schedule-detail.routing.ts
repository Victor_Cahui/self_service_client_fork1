import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ScheduleDetailComponent } from './schedule-detail.component';
import { ScheduleDetailGuard } from './schedule-detail.guard';

const routes: Routes = [
  {
    canActivate: [ScheduleDetailGuard],
    component: ScheduleDetailComponent,
    path: ''
  },
  {
    path: '3',
    loadChildren: '../steps/step-3/schedule-step-3.module#ScheduleStep3Module'
  },
  {
    path: '4',
    loadChildren: '../steps/step-review/schedule-step-review.module#ScheduleStepReviewModule'
  },
  {
    path: '5',
    loadChildren: '../steps/step-success/schedule-step-success.module#ScheduleStepSuccessModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ScheduleDetailRoutingModule {}
