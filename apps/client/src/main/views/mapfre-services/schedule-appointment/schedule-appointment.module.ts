import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ScheduleAppointmentComponent } from './schedule-appointment.component';
import { ScheduleAppointmentRouting } from './schedule-appointment.routing';

@NgModule({
  imports: [CommonModule, ScheduleAppointmentRouting],
  declarations: [ScheduleAppointmentComponent]
})
export class ScheduleAppointmentModule {}
