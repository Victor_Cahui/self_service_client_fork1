import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import {
  GmFilterModule,
  GmFilterViewClinicModule,
  GmFilterViewOfficeModule,
  GoogleMapFilterViewModule
} from '@mx/components';
import { ModalSelectPolicyModule } from '@mx/components/shared/modals/modal-select-policy/modal-select-policy.module';
import { GeolocationService } from '@mx/core/shared/helpers/util/geolocation.service';
import { ClinicMapComponent } from './clinic-map.component';
import { ClinicMapRouting } from './clinic-map.routing';

@NgModule({
  imports: [
    CommonModule,
    ClinicMapRouting,
    GoogleMapFilterViewModule,
    GmFilterModule,
    ModalSelectPolicyModule,
    GmFilterViewOfficeModule,
    GmFilterViewClinicModule
  ],
  declarations: [ClinicMapComponent],
  providers: [GeolocationService]
})
export class ClinicMapModule {}
