import { CurrencyPipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { GmFilterEvents, GmFilterService } from '@mx/components';
import { GeolocationService } from '@mx/core/shared/helpers/util/geolocation.service';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AppointmentFacade } from '@mx/core/shared/state/appointment';
import { AuthService } from '@mx/services/auth/auth.service';
import { HealthService } from '@mx/services/health/health.service';
import { PoliciesService, STORAGE_KEYS } from '@mx/services/policies.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { MapLang } from '@mx/settings/lang/map.lang';
import { ICardPoliciesView } from '@mx/statemanagement/models/policy.interface';
import { BaseClinicsSearch } from '@mx/views/clinics/clinics-search/base-clinics-search.component';

@Component({
  selector: 'client-clinic-map',
  templateUrl: './clinic-map.component.html',
  providers: [CurrencyPipe]
})
export class ClinicMapComponent extends BaseClinicsSearch implements OnInit {
  constructor(
    authService: AuthService,
    healthService: HealthService,
    activatedRoute: ActivatedRoute,
    policiesService: PoliciesService,
    currencyPipe: CurrencyPipe,
    router: Router,
    appointmentFacade: AppointmentFacade,
    gmFilterService: GmFilterService,
    storageService: StorageService,
    private readonly _Autoservicios: Autoservicios,
    private readonly geolocationService: GeolocationService,
    protected gaService: GAService
  ) {
    super(
      authService,
      healthService,
      activatedRoute,
      policiesService,
      currencyPipe,
      router,
      appointmentFacade,
      gmFilterService,
      gaService,
      storageService
    );
    const hasHealthPolicies = this.storageService.getStorage(STORAGE_KEYS.HAS_HEALTH_POLICIES);
    this.configView = {
      ...this.configView,
      canViewDeductibles: hasHealthPolicies,
      inDesktop: {
        hasFilter: false,
        hasSeeker: false
      },
      inResponsive: {
        hasFilter: false,
        hasSeeker: true
      }
    };
  }

  ngOnInit(): void {
    this.showLoading = true;
    this.getPolicies();
    this.onChangePolicy();
    this.geolocationService.hasPermission().then(hasPermission => {
      this.configView = { ...this.configView, canViewDistance: hasPermission };
      this.hasPermission = hasPermission;
      this.params = {
        ...this.params,
        latitud: MapLang.CLINIC.defaultCoords.latitude,
        longitud: MapLang.CLINIC.defaultCoords.longitude
      };
      this.getClinics();
    });
  }

  getClinics(): void {
    this._Autoservicios
      .ObtenerClinicasAgendables(
        { codigoApp: APPLICATION_CODE },
        { latitud: `${this.params.latitud}`, longitud: `${this.params.longitud}` }
      )
      .subscribe(r => {
        this.markers = this.formatMarker(r);
        this.gmFilterService.setModifiedItems(this.markers);
        this.markers = this.gmFilter.fnSearch([]);

        this.gmFilterService.eventEmit(GmFilterEvents.CLEAR_SEARCH_INPUT);
        setTimeout(() => {
          this.showLoading = false;
          this.setDefaultPosition();
        }, 100);
      });
  }

  selectedCurrentPolicy(policySelected: ICardPoliciesView): void {
    this.currentPolicy = this.policies.filter(policy => policy.numeroPoliza === policySelected.policyNumber)[0];
    this.centerButtonResponsive.label = this.currentPolicy.descripcionPoliza;
    this.healthService.setCurrentPolicy(this.currentPolicy);
  }
}
