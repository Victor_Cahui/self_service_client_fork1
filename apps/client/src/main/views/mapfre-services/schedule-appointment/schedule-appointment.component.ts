import { Component, OnDestroy, OnInit } from '@angular/core';
import { AppointmentFacade } from '@mx/core/shared/state/appointment';
@Component({
  selector: 'client-schedule-appointment',
  templateUrl: './schedule-appointment.component.html'
})
export class ScheduleAppointmentComponent implements OnDestroy, OnInit {
  constructor(private readonly _AppointmentFacade: AppointmentFacade) {}

  ngOnDestroy(): void {
    this._AppointmentFacade.CleanAppointment();
  }
  ngOnInit(): void {}
}
