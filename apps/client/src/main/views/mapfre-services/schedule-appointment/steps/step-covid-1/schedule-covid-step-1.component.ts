import { DOCUMENT, Location } from '@angular/common';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { UnsubscribeOnDestroy } from '@mx/core/shared/helpers/util/unsubscribe-on-destroy';
import { FrmProvider } from '@mx/core/shared/providers/services';
import { AppointmentFacade, State } from '@mx/core/shared/state/appointment';
import { ConfigCbo } from '@mx/core/ui/lib/components/forms/select';
import { MfModalMessage } from '@mx/core/ui/lib/components/modals/modal-message';
import { ClientService } from '@mx/services/client.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { HealthService } from '@mx/services/health/health.service';
import { PoliciesService } from '@mx/services/policies.service';
import { GENERAL_MAX_LENGTH, REG_EX, TYPE_APPOINTMENT } from '@mx/settings/constants/general-values';
import { FILE_ICON, WARNING_ICON } from '@mx/settings/constants/images-values';
import { SCHEDULE_COVID_QUOTE_STEP } from '@mx/settings/constants/router-step';
import { SCHEDULE_APPOINTMENT_COVID_STEP_1_HEADER } from '@mx/settings/constants/router-titles';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { VehiclesLang } from '@mx/settings/lang/vehicles.lang';
import { IClientInformationResponse } from '@mx/statemanagement/models/client.models';
import { IPoliciesByClientResponse } from '@mx/statemanagement/models/policy.interface';
import { IProfileBasicInfo } from '@mx/statemanagement/models/profile.interface';
import { Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AppointmentService } from '../../steps/service/appointment.service';
import { mapArrPolicies } from './schedule-covid-step-1.utils';

@Component({
  selector: 'client-schedule-covid-step-1',
  templateUrl: './schedule-covid-step-1.component.html'
})
export class ScheduleCovidStep1Component extends UnsubscribeOnDestroy implements OnInit {
  @ViewChild('cboPolicy') public cboPolicy: any;
  @ViewChild(MfModalMessage) modalMessage: MfModalMessage;

  currentPolicy: IPoliciesByClientResponse;
  policies: Array<IPoliciesByClientResponse>;
  searchByClientSub: Subscription;
  stepList = SCHEDULE_COVID_QUOTE_STEP;
  fileIcon = FILE_ICON;
  lang = VehiclesLang;
  frm: FormGroup;
  graLng = GeneralLang;
  showLoading = true;
  showLoadingSpecialty = true;
  showLoadingPolicy = true;
  appointment: State;
  configPatient: ConfigCbo;
  configPolicy: any;
  arrPolicies: any[];
  iconError: string = WARNING_ICON;
  modality: string;
  centerVirtual: string;
  numericRegExp = REG_EX.numeric;
  maxLengthCellphone = GENERAL_MAX_LENGTH.cellPhone;

  dataClinicSave;
  value;
  btnCancel = GeneralLang.Buttons.Back;
  validCovid = false;
  backCovid = true;

  private _patient: any;
  private _policy: any;
  private readonly _userInfo: IProfileBasicInfo;

  constructor(
    private readonly _AppointmentFacade: AppointmentFacade,
    private readonly _ClientService: ClientService,
    private readonly _FrmProvider: FrmProvider,
    private readonly _HeaderHelperService: HeaderHelperService,
    private readonly _Router: Router,
    private readonly _appointmentService: AppointmentService,
    public readonly policiesService: PoliciesService,
    public readonly healthService: HealthService,
    private readonly _Location: Location,
    @Inject(DOCUMENT) protected document: Document
  ) {
    super();
  }

  ngOnInit(): void {
    this.modality = `${TYPE_APPOINTMENT.PRESENCIAL}`;
    this._initValues();
    this.getPolicies();
    this._mapStateClinica();
  }

  onConfirmModal(r: boolean): void {
    const url = this._appointmentService.getUrlReturn;
    if (r) {
      this._Router.navigateByUrl(url);
    }
  }

  actionConfirm(): void {
    this._AppointmentFacade.AddModality(this.modality);
    if (this.dataClinicSave) {
      this._AppointmentFacade.AddEstablishment(this.dataClinicSave);
    }
    this._AppointmentFacade.AddPatient(this._patient);
    this._AppointmentFacade.AddPolicy(this._policy);

    this._AppointmentFacade.AddFrmStep2(this.frm.value);
    this._AppointmentFacade.AddCellphone(this.frm.value.contactCellphone);

    this._Router.navigate(['/digital-clinic/schedule/steps-covid/2']);
  }

  actionCancel(): void {
    if (!this.validCovid) {
      this.backCovid = false;
    }
    this._Location.back();
  }

  getPolicies(): void {
    this.searchByClientSub = this.policiesService.getSortedHealthyPolicies().subscribe(
      (policies = []) => {
        this.policies = policies;
        this.healthService.setPolicies(policies);
        if (policies.length) {
          this.currentPolicy = policies[0];
          this.healthService.setCurrentPolicy(this.currentPolicy);
          this._AppointmentFacade.AddPolicy(this.currentPolicy);
        }
      },
      () => {
        this.showLoading = false;
        this.searchByClientSub.unsubscribe();
      }
    );
  }

  getSelectedPatient(p): void {
    if (p.isFirstTime) {
      this._patient = p;
    }

    this.frm.controls['documentoPaciente'].setValue(p.documento);

    this._patient = p;

    this.arrPolicies = mapArrPolicies(p.polizas);

    if (`${this.appointment.poliza}`) {
      const selectedPolicyFromMapPage = p.polizas.find(
        (po: any) => `${po.numeroPoliza}` === `${this.appointment.poliza.numeroPoliza}`
      );
      this._policy = selectedPolicyFromMapPage ? selectedPolicyFromMapPage : this.arrPolicies[0];
    } else {
      this._policy = this.arrPolicies[0];
    }

    this.frm.controls['numeroPoliza'].setValue(this._policy.numeroPoliza);
  }

  getLoadingState(st): void {
    this.showLoading = st.isLoadingVisible;
  }

  private _mapStateClinica(): void {
    this._AppointmentFacade.getAppointment$.subscribe(st => {
      this.appointment = st;
      if (this.appointment.clinicaId) {
        this.value = this.appointment.clinicaId;
      }
    });
  }

  private _initValues(): void {
    this._AppointmentFacade.CleanStep2();
    this._HeaderHelperService.set(SCHEDULE_APPOINTMENT_COVID_STEP_1_HEADER);
    this.frm = this._FrmProvider.ScheduleCovidStep1Component();

    this.configPatient = {
      errorMsg: 'No hay pacientes disponibles',
      canAutoSelectFirstOpt: true
    };

    this.configPolicy = {
      errorMsg: 'No hay pólizas disponibles',
      icon: 'icon-mapfre_020_cross',
      lbl: 'Seleccione Póliza',
      mustSetFirstOpt: true
    };
    this._AppointmentFacade.AddModality(this.modality);

    this._ClientService
      .getClientInformation({})
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((res: IClientInformationResponse) => {
        this.frm.controls['contactCellphone'].setValue(res.telefonoMovil);
      });
  }
}
