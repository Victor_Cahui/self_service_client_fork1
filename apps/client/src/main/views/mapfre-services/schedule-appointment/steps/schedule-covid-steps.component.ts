import { Component, HostBinding } from '@angular/core';

@Component({
  selector: 'client-schedule-covid-steps-component',
  templateUrl: './schedule-covid-steps.component.html'
})
export class ScheduleCovidStepsComponent {
  @HostBinding('class') class = 'w-100';
}
