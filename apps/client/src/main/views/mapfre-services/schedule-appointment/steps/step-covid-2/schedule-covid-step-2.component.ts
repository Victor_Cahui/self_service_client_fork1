import { DOCUMENT, Location } from '@angular/common';
import { Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { dateToStringFormatYYYYMMDD } from '@mx/core/shared/helpers/util/date';
import { GeolocationService } from '@mx/core/shared/helpers/util/geolocation.service';
import { UnsubscribeOnDestroy } from '@mx/core/shared/helpers/util/unsubscribe-on-destroy';
import { Autoservicios, FrmProvider } from '@mx/core/shared/providers/services';
import { AppointmentFacade, State } from '@mx/core/shared/state/appointment';
import { ConfigCbo } from '@mx/core/ui/lib/components/forms/select';
import { MfModalMessage } from '@mx/core/ui/lib/components/modals/modal-message';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { CENTER_VIRTUAL, TYPE_APPOINTMENT } from '@mx/settings/constants/general-values';
import { FILE_ICON, WARNING_ICON } from '@mx/settings/constants/images-values';
import { SCHEDULE_COVID_QUOTE_STEP } from '@mx/settings/constants/router-step';
import { APPOINTMENT_STEP_2 } from '@mx/settings/constants/router-tabs';
import { APPOINTMENT_RESCHEDULE_STEP_2_HEADER } from '@mx/settings/constants/router-titles';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { MapLang } from '@mx/settings/lang/map.lang';
import { VehiclesLang } from '@mx/settings/lang/vehicles.lang';
import { take } from 'rxjs/internal/operators/take';
@Component({
  selector: 'client-schedule-covid-step-2',
  templateUrl: './schedule-covid-step-2.component.html'
})
export class ScheduleCovidStep2Component extends UnsubscribeOnDestroy implements OnInit, OnDestroy {
  @ViewChild(MfModalMessage) modalMessage: MfModalMessage;

  appointment: State;
  clinicaId: 0;
  btn = GeneralLang;
  btnCancel = GeneralLang.Buttons.Back;
  configClinics: ConfigCbo;
  configHoursByDate: ConfigCbo;
  configDoctorsByDate: ConfigCbo;
  configDoctors: any;
  frmTab0: FormGroup;
  frmTab1: FormGroup;
  graLng = GeneralLang;
  iconError: string = WARNING_ICON;
  selectedTab = 0;
  showLoadingClinic = true;
  showLoading = true;
  showLoadingHours = true;
  showLoadingDoctors = true;
  showLoadingDateByDoctor = true;
  stepList = SCHEDULE_COVID_QUOTE_STEP;
  tabList = APPOINTMENT_STEP_2;
  fileIcon = FILE_ICON;
  dataPatient;
  centerVirtual = CENTER_VIRTUAL;
  modality;
  citaCovidSpeciality: any;
  validCovid = false;
  lang = VehiclesLang;
  selectedDefault = false;
  configView;
  hasPermission;
  params;
  dataClinics;
  dataClinicSave;
  backCovid = true;

  private _hour: any;
  private _doctor: any;
  protected _value: any;
  protected _placeholder: any;

  get value(): any {
    return this._value;
  }

  set value(value: any) {
    this._value = value;
  }

  get placeholder(): string {
    return this._placeholder;
  }
  set placeholder(placeholder: string) {
    this._placeholder = placeholder;
    this.selectedDefault = true;
  }

  constructor(
    private readonly _AppointmentFacade: AppointmentFacade,
    private readonly _FrmProvider: FrmProvider,
    private readonly _HeaderHelperService: HeaderHelperService,
    private readonly _Location: Location,
    private readonly _Router: Router,
    private readonly _Autoservicios: Autoservicios,
    private readonly geolocationService: GeolocationService,
    @Inject(DOCUMENT) protected document: Document
  ) {
    super();
  }

  ngOnInit(): void {
    this._initValues();
    this._mapState();
    this.getDataPatient();
    this.getModality();
    this.geolocationService.hasPermission().then(hasPermission => {
      this.configView = { ...this.configView, canViewDistance: hasPermission };
      this.hasPermission = hasPermission;
      this.params = {
        ...this.params,
        latitud: MapLang.CLINIC.defaultCoords.latitude,
        longitud: MapLang.CLINIC.defaultCoords.longitude
      };
    });
  }

  getSelectedOptDoctorArr(arr): void {
    if (arr.length > 0) {
      this.frmTab0.controls['doctorId'].setValue(arr[0].doctorId);
      this.frmTab0.get('doctorId').clearValidators();
      this._doctor = arr[0];
    }
  }

  getDataPatient(): void {
    this._AppointmentFacade.getPatient$.subscribe(data => {
      this.dataPatient = data;
    });
  }

  changeClinic(c): any {
    this.dataClinicSave = c;
    this.clinicaId = c.clinicaId;
    this.frmTab0.get('fecha').reset();
    this.frmTab0.get('fecha').clearValidators();
  }

  actionConfirm(): void {
    this._AppointmentFacade.AddEstablishment(this.dataClinicSave);
    this._AppointmentFacade.AddDate(dateToStringFormatYYYYMMDD(this[`frmTab${this.selectedTab}`].value.fecha));
    this._AppointmentFacade.AddDoctor(this._doctor);
    this._AppointmentFacade.AddHour(this._hour);
    this._AppointmentFacade.AddFrmStep2({ tabSelected: this.selectedTab, ...this[`frmTab${this.selectedTab}`].value });
    const goTo = this.appointment.isEditable
      ? '/mapfre-services/schedule-detail/4'
      : '/digital-clinic/schedule/steps-covid/3';
    this._Router.navigate([goTo]);
  }

  actionCancel(): void {
    if (!this.validCovid) {
      this.backCovid = false;
    }
    this._Location.back();
  }

  onSelectTab(t: number): void {
    this.showLoading = true;
    this.selectedTab = t;
  }

  getModality(): void {
    this._AppointmentFacade.getModality$.subscribe(modality => {
      if (modality === `${TYPE_APPOINTMENT.VIRTUAL}`) {
        this.modality = `${TYPE_APPOINTMENT.VIRTUAL}`;
        this.centerVirtual = CENTER_VIRTUAL;
      } else {
        this.modality = `${TYPE_APPOINTMENT.PRESENCIAL}`;
      }
    });
  }

  onConfirmModal(r: boolean): void {
    if (!r) {
      return void 0;
    }

    this._AppointmentFacade.CleanStep2();
    this._Location.back();
  }

  getLoadingStateClinic(st): void {
    this.showLoadingClinic = st.isLoadingVisible;
  }

  getLoadingState(st): void {
    this.showLoading = st.isLoadingVisible;
  }

  getLoadingStateDoctor(st): void {
    this.showLoadingDoctors = st.isLoadingVisible;
  }

  getLoadingStateHours(st): void {
    this.showLoadingHours = st.isLoadingVisible;
  }

  getLoadingStateDateByDoctor(st): void {
    this.showLoadingDateByDoctor = st.isLoadingVisible;
  }

  // by hour

  getSelectedHour(h): void {
    this._hour = h;
    this.frmTab0.patchValue({ horaFin: h.horaFin });
  }

  getSelectedDoctorByDate(d): void {
    this._doctor = d;
  }

  // by doctor

  getSelectedDoctorByDoctor(d): void {
    this._doctor = d;
  }

  getSelectedHourByDoctor(h): void {
    this._hour = h;
    this.frmTab1.patchValue({ horaFin: h.horaFin });
  }

  private _mapState(): void {
    const getAppointment$ = this._AppointmentFacade.getAppointment$.subscribe(st => {
      this.appointment = st;
      this.clinicaId = st.clinicaId;
    });
    const getAppointmentOneTime$ = this._AppointmentFacade.getAppointment$
      .pipe(take(1))
      .subscribe(this._mapAppointmentOneTime.bind(this));
    this.arrToDestroy.push(getAppointment$, getAppointmentOneTime$);
  }

  private _mapAppointmentOneTime(st): void {
    st.isEditable && (this.btnCancel = this.graLng.Buttons.Back);
    if (![0, 1].includes(st.tabSelected)) {
      return void 0;
    }

    this.selectedTab = st.tabSelected;
    this[`frmTab${st.tabSelected}`].patchValue({
      doctorId: st.frmStep2.doctorId,
      fecha: st.frmStep2.fecha && new Date(st.frmStep2.fecha),
      horaFin: st.frmStep2.horaFin,
      horaInicio: st.frmStep2.horaInicio
    });
  }

  private _setTitleHeader(): void {
    const st = this._AppointmentFacade.getState() || {};
    if (st.canReschedule) {
      this._HeaderHelperService.set(APPOINTMENT_RESCHEDULE_STEP_2_HEADER);
    }
  }

  private _initValues(): void {
    this.frmTab0 = this._FrmProvider.ScheduleStep3PComponentByDate();
    this.frmTab1 = this._FrmProvider.ScheduleStep3PComponentByDoctor();
    this._setTitleHeader();

    this.configClinics = {
      errorMsg: 'No hay clinicas disponibles',
      canAutoSelectFirstOpt: false
    };
    this.configHoursByDate = {
      errorMsg: 'No hay horarios disponibles',
      canAutoSelectFirstOpt: false
    };
    this.configDoctorsByDate = {
      errorMsg: 'No hay doctores disponibles',
      canAutoSelectFirstOpt: false
    };
    this.configDoctors = {
      errorMsg: 'No hay doctores disponibles',
      icon: 'icon-mapfre_168_medical_consultation',
      lbl: 'Doctor de turno'
    };
  }
}
