import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ScheduleStep2Component } from './schedule-step-2.component';
import { ScheduleStep2Guard } from './schedule-step-2.guard';

const routes: Routes = [
  {
    canActivate: [ScheduleStep2Guard],
    component: ScheduleStep2Component,
    path: ''
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ScheduleStep2RoutingModule {}
