import { DOCUMENT, Location } from '@angular/common';
import { Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { dateToStringFormatYYYYMMDD } from '@mx/core/shared/helpers/util/date';
import { GeolocationService } from '@mx/core/shared/helpers/util/geolocation.service';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { UnsubscribeOnDestroy } from '@mx/core/shared/helpers/util/unsubscribe-on-destroy';
import { Autoservicios, FrmProvider } from '@mx/core/shared/providers/services';
import { AppointmentFacade, State } from '@mx/core/shared/state/appointment';
import { ConfigCbo } from '@mx/core/ui/lib/components/forms/select';
import { MfModalMessage } from '@mx/core/ui/lib/components/modals/modal-message';
import { GeneralService } from '@mx/services/general/general.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { APPLICATION_CODE, CENTER_VIRTUAL, STORAGE, TYPE_APPOINTMENT } from '@mx/settings/constants/general-values';
import { FILE_ICON, WARNING_ICON } from '@mx/settings/constants/images-values';
import { LOCAL_APPOINTMENT_EDIT } from '@mx/settings/constants/key-values';
import { SCHEDULE_QUOTE_STEP } from '@mx/settings/constants/router-step';
import { APPOINTMENT_STEP_2 } from '@mx/settings/constants/router-tabs';
import { APPOINTMENT_RESCHEDULE_STEP_2_HEADER } from '@mx/settings/constants/router-titles';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { MapLang } from '@mx/settings/lang/map.lang';
import { MediktorLang } from '@mx/settings/lang/mediktor.lang';
import { VehiclesLang } from '@mx/settings/lang/vehicles.lang';
import { take } from 'rxjs/internal/operators/take';

@Component({
  selector: 'client-schedule-step-3',
  templateUrl: './schedule-step-3.component.html'
})
export class ScheduleStep3Component extends UnsubscribeOnDestroy implements OnInit, OnDestroy {
  @ViewChild(MfModalMessage) modalMessage: MfModalMessage;

  appointment: State;
  clinicaId: any;
  btn = GeneralLang;
  btnCancel = GeneralLang.Buttons.Back;
  configClinics: ConfigCbo;
  configHoursByDate: ConfigCbo;
  configDoctorsByDate: ConfigCbo;
  configDoctors: ConfigCbo;
  frmTab0: FormGroup;
  frmTab1: FormGroup;
  graLng = GeneralLang;
  iconError: string = WARNING_ICON;
  selectedTab = 0;
  showLoading = true;
  showLoadingClinic = true;
  showLoadingDate = true;
  showLoadingHours = true;
  showLoadingDoctors = true;
  showLoadingDateByDoctor = true;
  stepList = SCHEDULE_QUOTE_STEP;
  tabList = APPOINTMENT_STEP_2;
  fileIcon = FILE_ICON;
  dataPatient;
  centerVirtual = CENTER_VIRTUAL;
  modality;
  citaCovidSpeciality: any;
  validCovid = true;
  lang = VehiclesLang;
  selectedDefault = false;
  configView;
  hasPermission;
  params;
  backCovid = true;
  appointmentEdited: any;
  appointmentOnEdit = false;

  private _hour: any;
  private _doctor: any;
  private _clinic: any;
  protected _value: any;
  protected _placeholder: any;

  get value(): any {
    return this._value;
  }

  set value(value: any) {
    this._value = value;
  }

  get placeholder(): string {
    return this._placeholder;
  }
  set placeholder(placeholder: string) {
    this._placeholder = placeholder;
    this.selectedDefault = true;
  }

  constructor(
    private readonly _AppointmentFacade: AppointmentFacade,
    private readonly _FrmProvider: FrmProvider,
    private readonly _HeaderHelperService: HeaderHelperService,
    private readonly _Location: Location,
    private readonly _Router: Router,
    private readonly _StorageService: StorageService,
    private readonly _GeneralService: GeneralService,
    private readonly geolocationService: GeolocationService,
    private readonly _Autoservicios: Autoservicios,
    private readonly _LocalStorageService: LocalStorageService,
    @Inject(DOCUMENT) protected document: Document
  ) {
    super();
  }

  ngOnInit(): void {
    this.citaCovidSpeciality = this._StorageService.getStorage(STORAGE.CITA_COVID_SCHEDULE);
    if (this.citaCovidSpeciality) {
      if (this.citaCovidSpeciality.citaCovid) {
        this.validCovid = false;
      }
    }
    this._initValues();
    this._mapState();
    this.getDataPatient();
    this.getModality();
    this._clinic = null;

    this.geolocationService.hasPermission().then(hasPermission => {
      this.configView = { ...this.configView, canViewDistance: hasPermission };
      this.hasPermission = hasPermission;
      this.params = {
        ...this.params,
        latitud: MapLang.CLINIC.defaultCoords.latitude,
        longitud: MapLang.CLINIC.defaultCoords.longitude
      };
    });

    this._GeneralService.rescheduleAppointment.subscribe(s => {
      if (!s) {
        if (this.modality !== TYPE_APPOINTMENT.VIRTUAL) {
          this._GeneralService.editAppointmentsBody.subscribe(res => {
            if (res) {
              this.appointmentEdited = res;
              setTimeout(() => {
                this.appointmentOnEdit = true;
                this.frmTab0.controls.clinicaId.setValue(this.appointmentEdited.clinicaId);
                this.frmTab1.controls.clinicaId.setValue(this.appointmentEdited.clinicaId);
              }, 100);
            } else {
              const LocalStorageAppointmentData = this._LocalStorageService.getData(LOCAL_APPOINTMENT_EDIT);
              if (LocalStorageAppointmentData && LocalStorageAppointmentData.appointmentIsOnEdit === true) {
                this.getAppointment();
              }
            }
          });
        }
      }
    });
  }

  ngOnDestroy(): void {
    let covid: any;

    if (this.citaCovidSpeciality) {
      if (this.citaCovidSpeciality.citaCovid) {
        this.document.getElementById('client-header-title2').style.display = 'none';
        this.document.getElementById('client-header-title1').style.display = 'block';
      }
    }

    if (this.backCovid) {
      covid = {
        citaCovid: false
      };
    } else {
      covid = {
        citaCovid: true
      };
    }

    this._StorageService.saveStorage(STORAGE.CITA_COVID, covid);
    this._StorageService.saveStorage(STORAGE.CITA_COVID_ESPECIALITY, covid);
    this._StorageService.saveStorage(STORAGE.CITA_COVID_SCHEDULE, covid);
    this._LocalStorageService.removeItem(LOCAL_APPOINTMENT_EDIT);
    this.frmTab0.reset();
    this.frmTab1.reset();
    this.appointmentOnEdit = false;
    this._GeneralService.editAppointmentsBody.next(null);
    this._GeneralService.rescheduleAppointment.next(false);
  }

  getSelectedOptDoctorArr(arr): void {
    if (this.citaCovidSpeciality) {
      if (this.citaCovidSpeciality.citaCovid) {
        if (arr.length > 0) {
          this.frmTab0.controls['doctorId'].setValue(arr[0].doctorId);
          this.frmTab0.get('doctorId').clearValidators();
          this._doctor = arr[0];
        }
      }
    }
  }

  getDataPatient(): void {
    this._AppointmentFacade.getPatient$.subscribe(data => {
      this.dataPatient = data;
    });
  }

  changeClinic(c): any {
    this._clinic = c;
    this.clinicaId = c.clinicaId;
    this.frmTab0.get('fecha').reset();
    this.frmTab0.get('fecha').clearValidators();
  }

  actionConfirm(): void {
    if (this.citaCovidSpeciality) {
      if (this.citaCovidSpeciality.citaCovid) {
        const covid: any = {
          citaCovid: true
        };
        this._StorageService.saveStorage(STORAGE.CITA_COVID_REVIEW, covid);
      }
    }

    if (this.appointment.tipoCita === `${TYPE_APPOINTMENT.PRESENCIAL}`) {
      this._AppointmentFacade.AddEstablishment(this._clinic);
    }
    this._AppointmentFacade.AddDate(dateToStringFormatYYYYMMDD(this[`frmTab${this.selectedTab}`].value.fecha));
    this._AppointmentFacade.AddDoctor(this._doctor);
    this._AppointmentFacade.AddHour(this._hour);
    this._AppointmentFacade.AddFrmStep2({ tabSelected: this.selectedTab, ...this[`frmTab${this.selectedTab}`].value });
    const goTo = this.appointment.isEditable ? '/digital-clinic/schedule-detail/4' : '/digital-clinic/schedule/steps/4';
    this._Router.navigate([goTo]);
  }

  actionCancel(): void {
    if (!this.validCovid) {
      this.backCovid = false;
    }
    this._Location.back();
  }

  onSelectTab(t: number): void {
    this.showLoadingClinic = true;
    this.selectedTab = t;
    this.frmTab0.reset();
    this.frmTab1.reset();
  }

  getModality(): void {
    this._AppointmentFacade.getModality$.subscribe(modality => {
      if (modality === `${TYPE_APPOINTMENT.VIRTUAL}`) {
        this.modality = `${TYPE_APPOINTMENT.VIRTUAL}`;
        this.centerVirtual = CENTER_VIRTUAL;
      } else {
        this.modality = `${TYPE_APPOINTMENT.PRESENCIAL}`;
      }
    });
  }

  onConfirmModal(r: boolean): void {
    if (!r) {
      return void 0;
    }

    this._AppointmentFacade.CleanStep2();
    this._Location.back();
  }

  getLoadingState(st): void {
    this.showLoading = st.isLoadingVisible;
  }

  getLoadingStateClinic(st): void {
    this.showLoadingClinic = st.isLoadingVisible;
  }

  getLoadinStateDate(st): void {
    this.showLoadingDate = st.isLoadingVisible;
  }

  getLoadingStateDoctor(st): void {
    this.showLoadingDoctors = st.isLoadingVisible;
  }

  getLoadingStateHours(st): void {
    this.showLoadingHours = st.isLoadingVisible;
  }

  getLoadingStateDateByDoctor(st): void {
    this.showLoadingDateByDoctor = st.isLoadingVisible;
  }

  getSelectedHour(h): void {
    this._hour = h;
    this.frmTab0.patchValue({ horaFin: h.horaFin });
  }

  getSelectedDoctorByDate(d): void {
    this._doctor = d;
  }

  getSelectedDoctorByDoctor(d): void {
    this._doctor = d;
  }

  getSelectedHourByDoctor(h): void {
    this._hour = h;
    this.frmTab1.patchValue({ horaFin: h.horaFin });
  }

  private _mapState(): void {
    const getAppointment$ = this._AppointmentFacade.getAppointment$.subscribe(st => {
      this.appointment = st;
      this.clinicaId = st.tipoCita === 'V' ? this.centerVirtual : st.clinicaId;
    });
    const getAppointmentOneTime$ = this._AppointmentFacade.getAppointment$
      .pipe(take(1))
      .subscribe(this._mapAppointmentOneTime.bind(this));
    this.arrToDestroy.push(getAppointment$, getAppointmentOneTime$);
  }

  private _mapAppointmentOneTime(st): void {
    st.isEditable && (this.btnCancel = this.graLng.Buttons.Back);
    if (![0, 1].includes(st.tabSelected)) {
      return void 0;
    }

    this.selectedTab = st.tabSelected;
    this[`frmTab${st.tabSelected}`].patchValue({
      doctorId: st.frmStep2.doctorId,
      fecha: st.frmStep2.fecha && new Date(st.frmStep2.fecha),
      horaFin: st.frmStep2.horaFin,
      horaInicio: st.frmStep2.horaInicio
    });
  }

  private _setTitleHeader(): void {
    const st = this._AppointmentFacade.getState() || {};
    if (st.canReschedule) {
      this._HeaderHelperService.set(APPOINTMENT_RESCHEDULE_STEP_2_HEADER);
    }
  }

  private getAppointment(): void {
    this._Autoservicios
      .ObtenerCita(
        { citaId: this._LocalStorageService.getData(LOCAL_APPOINTMENT_EDIT).citaId },
        { codigoApp: APPLICATION_CODE }
      )
      .subscribe(
        cita => {
          this.appointmentEdited = cita;
          this.appointmentOnEdit = true;
          this.frmTab0.controls.clinicaId.setValue(this.appointmentEdited.clinicaId);
          this.frmTab1.controls.clinicaId.setValue(this.appointmentEdited.clinicaId);
        },
        err => {
          console.error(err);
        }
      );
  }

  private _initValues(): void {
    this._AppointmentFacade.getModality$.subscribe(modality => {
      if (modality === `${TYPE_APPOINTMENT.VIRTUAL}`) {
        this.frmTab0 = this._FrmProvider.ScheduleStep3VComponentByDate();
        this.frmTab1 = this._FrmProvider.ScheduleStep3VComponentByDoctor();
      } else {
        this.frmTab0 = this._FrmProvider.ScheduleStep3PComponentByDate();
        this.frmTab1 = this._FrmProvider.ScheduleStep3PComponentByDoctor();
      }
    });

    this._setTitleHeader();

    this.configClinics = {
      errorMsg: 'No hay Centros médicos disponibles',
      canAutoSelectFirstOpt: false
    };

    this.configHoursByDate = {
      errorMsg: 'No hay horarios disponibles',
      canAutoSelectFirstOpt: false
    };

    this.configDoctorsByDate = {
      errorMsg: 'No hay doctores disponibles',
      canAutoSelectFirstOpt: false
    };

    this.configDoctors = {
      errorMsg: 'No hay doctores disponibles',
      canAutoSelectFirstOpt: false
    };

    if (this.citaCovidSpeciality) {
      if (this.citaCovidSpeciality.citaCovid) {
        this.document.getElementById('client-header-title1').style.display = 'none';
        this.document.getElementById(
          'client-header-title2'
        ).innerText = MediktorLang.Labels.ScheduleCitaCovid.toUpperCase();
        this.document.getElementById('client-header-title2').style.display = 'block';
      }
    }
  }
  // tslint:disable-next-line: max-file-line-count
}
