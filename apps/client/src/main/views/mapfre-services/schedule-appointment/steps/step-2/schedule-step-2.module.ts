import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FormBaseModule } from '@mx/components/shared/form-base/form-base.module';
import { MfInputModule, MfModalMessageModule, MfSelectModule, MfShowErrorsModule } from '@mx/core/ui';
import { MfSelectBoxModule } from '@mx/core/ui/lib/components/forms/select-box';
import { StepperModule } from '@mx/core/ui/lib/components/stepper/stepper.module';
import { ScheduleStep2Component } from './schedule-step-2.component';
import { ScheduleStep2Guard } from './schedule-step-2.guard';
import { ScheduleStep2RoutingModule } from './schedule-step-2.routing';

@NgModule({
  declarations: [ScheduleStep2Component],
  imports: [
    CommonModule,
    FormBaseModule,
    FormsModule,
    MfInputModule,
    MfModalMessageModule,
    MfSelectBoxModule,
    MfSelectModule,
    ReactiveFormsModule,
    ScheduleStep2RoutingModule,
    StepperModule,
    MfShowErrorsModule
  ],
  providers: [ScheduleStep2Guard]
})
export class ScheduleStep2Module {}
