import { DOCUMENT } from '@angular/common';
import { AfterViewInit, Component, Inject, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { UnsubscribeOnDestroy } from '@mx/core/shared/helpers/util/unsubscribe-on-destroy';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AppointmentFacade, State } from '@mx/core/shared/state/appointment';
import { NotificationStatus } from '@mx/core/ui/lib/components/alerts/custom-alert/custom-alert.component';
import { MfModalMessage } from '@mx/core/ui/lib/components/modals/modal-message';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { GeneralService } from '@mx/services/general/general.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { APPLICATION_CODE, STORAGE, TYPE_APPOINTMENT } from '@mx/settings/constants/general-values';
import { FILE_ICON, WARNING_ICON } from '@mx/settings/constants/images-values';
import * as KEYS_VALUES from '@mx/settings/constants/key-values';
import { SCHEDULE_COVID_QUOTE_STEP, SCHEDULE_QUOTE_STEP } from '@mx/settings/constants/router-step';
import {
  APPOINTMENT_RESCHEDULE_STEP_REVIEW_HEADER,
  SCHEDULE_APPOINTMENT_COVID_STEP_1_HEADER
} from '@mx/settings/constants/router-titles';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { Observable } from 'rxjs/Observable';
import { AppointmentService } from '../../steps/service/appointment.service';
import { getReqBody } from './schedule-step-review.utils';
@Component({
  selector: 'client-schedule-step-review',
  templateUrl: './schedule-step-review.component.html'
})
export class ScheduleStepReviewComponent extends UnsubscribeOnDestroy implements OnInit, AfterViewInit {
  @ViewChild(MfModalMessage) modalMessage: MfModalMessage;
  appointment: State;
  graLng = GeneralLang;
  iconError: string = WARNING_ICON;
  fileIcon: string = FILE_ICON;
  msgCancelModal = GeneralLang.Messages.DeleteData;
  showLoading: boolean;
  stepList = SCHEDULE_QUOTE_STEP;
  tooltip = {};
  modality;
  modalityType = TYPE_APPOINTMENT;
  citaCovidReview: any;
  validCodiv = true;
  especialityCovid = [104, 503];
  covidIsActive = false;
  currentStep = 4;
  especialityId = 0;
  status = NotificationStatus.INFO;
  module = 'CDM';
  resultTime: string;

  constructor(
    private readonly _AppointmentFacade: AppointmentFacade,
    private readonly _AuthService: AuthService,
    private readonly _Autoservicios: Autoservicios,
    private readonly _ClientService: ClientService,
    private readonly _GeneralService: GeneralService,
    private readonly _HeaderHelperService: HeaderHelperService,
    private readonly _Router: Router,
    private readonly _appointmentService: AppointmentService,
    private readonly _StorageService: StorageService,
    @Inject(DOCUMENT) protected document: Document
  ) {
    super();
    this._mapState();
  }

  ngOnInit(): void {
    this.citaCovidReview = this._StorageService.getStorage(STORAGE.CITA_COVID_REVIEW);
    this._initValues();
    this.getModality();
  }

  ngAfterViewInit(): void {
    this.scrollToBottom();
  }

  onConfirmModal(r: boolean): void {
    const url = this._appointmentService.getUrlReturn;
    if (r && this.appointment.canReschedule) {
      this._AppointmentFacade.CleanFrmStep2();
      this.showLoading = true;
      // HACK: efecto q esta haciendo algo, ya que la pagina de destino es identica
      setTimeout(() => {
        this._Router.navigate(['/mapfre-services/assists']);
      }, 1000);

      return void 0;
    }

    if (r && this.appointment.isEditable) {
      this._AppointmentFacade.CleanFrmStep2();
      this.showLoading = true;
      // HACK: efecto q esta haciendo algo, ya que la pagina de destino es identica
      setTimeout(() => {
        this._Router.navigate(['/mapfre-services/schedule-detail']);
      }, 1000);

      return void 0;
    }

    if (!r) {
      return void 0;
    }

    this._Router.navigateByUrl(url);
  }

  actionConfirm(): void {
    this.showLoading = true;
    this._getService().subscribe(
      () => {
        this.showLoading = false;
        if (this.especialityCovid.includes(this.appointment.especialidad.especialidadId)) {
          this._GeneralService.saveUserMovements(
            this.module,
            this._GeneralService.action(KEYS_VALUES.CDM_PROCEDURES.PROC_PRUEBA_COVID_CDM),
            this._GeneralService.action(KEYS_VALUES.CDM_DESCRIPTION.DSC_FIN)
          );
        } else {
          this._GeneralService.saveUserMovements(
            this.module,
            this._GeneralService.action(KEYS_VALUES.CDM_PROCEDURES.PROC_AGENDA_ESPECIALISTA_CDM),
            this._GeneralService.action(KEYS_VALUES.CDM_DESCRIPTION.DSC_FIN)
          );
        }
        if (this.citaCovidReview) {
          if (this.citaCovidReview.citaCovid) {
            const covid: any = {
              citaCovid: true
            };
            this._StorageService.saveStorage(STORAGE.CITA_COVID_SUCCESS, covid);
          }
        }
        const goTo = this.appointment.isEditable
          ? '/mapfre-services/schedule-detail/5'
          : '/digital-clinic/schedule/steps/5';
        this._Router.navigate([goTo]);
      },
      () => {
        this.showLoading = false;
      }
    );
  }

  getModality(): void {
    this._AppointmentFacade.getModality$.subscribe(modality => {
      this.modality =
        modality === `${TYPE_APPOINTMENT.VIRTUAL}` ? `${TYPE_APPOINTMENT.VIRTUAL}` : `${TYPE_APPOINTMENT.PRESENCIAL}`;
    });
  }

  actionCancel(): void {
    this.modalMessage.open();
  }

  private _getService(): any {
    const actions = {
      save: this._save.bind(this),
      edit: this._update.bind(this)
    };
    if (!this.appointment.estadoCita || this.appointment.canReschedule) {
      return actions.save();
    }

    if (this.appointment.estadoCita === 'PENDIENTE') {
      return actions.edit();
    }
  }

  private _mapState(): void {
    const getAppointment$ = this._AppointmentFacade.getAppointment$.subscribe(st => {
      if (st.isEditable) {
        this.msgCancelModal = this.graLng.Messages.RollbackEdit;
      }
      this.appointment = st;
      this.especialityId = this.appointment.especialidad.especialidadId;
      this.resultTime =
        this.especialityId === 104 ? GeneralLang.Texts.AntigenoResults : GeneralLang.Texts.MolecularResults;

      if (this.especialityCovid.includes(this.appointment.especialidad.especialidadId)) {
        this.covidIsActive = true;
        this.stepList = SCHEDULE_COVID_QUOTE_STEP;
        this.currentStep = 4;
        this._HeaderHelperService.set(SCHEDULE_APPOINTMENT_COVID_STEP_1_HEADER);
      } else {
        this.stepList = SCHEDULE_QUOTE_STEP;
        this.currentStep = 4;
      }
    });
    this.arrToDestroy.push(getAppointment$);
  }

  private _getAuthReq(): any {
    const auth = this._AuthService.retrieveEntity();
    const logedUser = this._ClientService.getUserData();

    return { ...auth, nombresUsuario: logedUser.nombreCompletoCliente };
  }

  private _save(): Observable<any> {
    return this._Autoservicios.RegistrarCita(
      { codigoApp: APPLICATION_CODE },
      {
        ...getReqBody(this.appointment, this._getAuthReq()),
        citaId: 0
      }
    );
  }

  private _update(): Observable<any> {
    return this._Autoservicios.ModificarCita(
      { codigoApp: APPLICATION_CODE, citaId: this.appointment.citaId },
      {
        ...getReqBody(this.appointment, this._getAuthReq())
      }
    );
  }

  private _setTitleHeader(): void {
    const st = this._AppointmentFacade.getState() || {};
    if (st.canReschedule) {
      const getAppointment$ = this._AppointmentFacade.getAppointment$.subscribe(st2 => {
        this.appointment = st2;
        if (this.especialityCovid.includes(this.appointment.especialidad.especialidadId)) {
          this.covidIsActive = true;
          this._HeaderHelperService.set(SCHEDULE_APPOINTMENT_COVID_STEP_1_HEADER);
        } else {
          this._HeaderHelperService.set(APPOINTMENT_RESCHEDULE_STEP_REVIEW_HEADER);
        }
      });
      this.arrToDestroy.push(getAppointment$);
    }
  }

  private scrollToBottom(): void {
    window.scrollTo(0, document.body.scrollHeight);
  }

  private _initValues(): void {
    this._setTitleHeader();
    const infoDAA = this._GeneralService.getValueParams(KEYS_VALUES.INFO_DAA);
    this.tooltip = {
      body: infoDAA,
      title: ''
    };
    if (this.especialityCovid.includes(this.especialityId)) {
      this.covidIsActive = true;
      this._HeaderHelperService.set(SCHEDULE_APPOINTMENT_COVID_STEP_1_HEADER);
    }
    this.scrollToBottom();
  }
}
