import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CaptureRouteSteps } from '../guards/schedule-appointment.guard';
import { ScheduleStepsComponent } from './schedule-steps.component';

const routes: Routes = [
  {
    path: '',
    component: ScheduleStepsComponent,
    canActivate: [CaptureRouteSteps],
    children: [
      {
        path: '',
        redirectTo: '1'
      },
      {
        path: '1',
        loadChildren: './step-1/schedule-step-1.module#ScheduleStep1Module'
      },
      {
        path: '2',
        loadChildren: './step-2/schedule-step-2.module#ScheduleStep2Module'
      },
      {
        path: '3',
        loadChildren: './step-3/schedule-step-3.module#ScheduleStep3Module'
      },
      {
        path: '4',
        loadChildren: './step-review/schedule-step-review.module#ScheduleStepReviewModule'
      },
      {
        path: '5',
        loadChildren: './step-success/schedule-step-success.module#ScheduleStepSuccessModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ScheduleStepsRoutingModule {}
