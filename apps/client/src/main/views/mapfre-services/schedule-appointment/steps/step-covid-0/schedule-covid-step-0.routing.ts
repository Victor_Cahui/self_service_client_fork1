import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ScheduleCovidStep0Component } from './schedule-covid-step-0.component';

const routes: Routes = [
  {
    component: ScheduleCovidStep0Component,
    path: ''
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ScheduleCovidStep0RoutingModule {}
