import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBaseModule } from '@mx/components/shared/form-base/form-base.module';
import { GeolocationService } from '@mx/core/shared/helpers/util/geolocation.service';
import { DirectivesModule, MfCardModule, MfInputModule, MfModalMessageModule, MfSelectModule } from '@mx/core/ui';
import { MfSelectBoxModule } from '@mx/core/ui/lib/components/forms/select-box';
import { StepperModule } from '@mx/core/ui/lib/components/stepper/stepper.module';
import { ScheduleStep1Component } from './schedule-step-1.component';
import { ScheduleStep1Guard } from './schedule-step-1.guard';
import { ScheduleStep1RoutingModule } from './schedule-step-1.routing';

@NgModule({
  declarations: [ScheduleStep1Component],
  imports: [
    CommonModule,
    FormBaseModule,
    FormsModule,
    MfModalMessageModule,
    MfSelectBoxModule,
    MfSelectModule,
    ReactiveFormsModule,
    ScheduleStep1RoutingModule,
    StepperModule,
    DirectivesModule,
    MfCardModule,
    MfInputModule
  ],
  providers: [ScheduleStep1Guard, GeolocationService]
})
export class ScheduleStep1Module {}
