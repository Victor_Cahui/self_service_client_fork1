import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ScheduleStepSuccessComponent } from './schedule-step-success.component';
import { ScheduleStepSuccessGuard } from './schedule-step-success.guard';

const routes: Routes = [
  {
    canActivate: [ScheduleStepSuccessGuard],
    component: ScheduleStepSuccessComponent,
    path: ''
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ScheduleStepSuccessRoutingModule {}
