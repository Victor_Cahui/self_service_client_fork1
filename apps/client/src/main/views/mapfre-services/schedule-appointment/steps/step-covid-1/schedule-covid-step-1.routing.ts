import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ScheduleCovidStep1Component } from './schedule-covid-step-1.component';

const routes: Routes = [
  {
    component: ScheduleCovidStep1Component,
    path: ''
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ScheduleCovidStep1RoutingModule {}
