import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBaseModule } from '@mx/components/shared/form-base/form-base.module';
import { GeolocationService } from '@mx/core/shared/helpers/util/geolocation.service';
import { MfCardModule, MfCheckboxModule, MfModalMessageModule, MfShowErrorsModule } from '@mx/core/ui';
import { StepperModule } from '@mx/core/ui/lib/components/stepper/stepper.module';
import { ScheduleCovidStep0Component } from './schedule-covid-step-0.component';
import { ScheduleCovidStep0Guard } from './schedule-covid-step-0.guard';
import { ScheduleCovidStep0RoutingModule } from './schedule-covid-step-0.routing';

@NgModule({
  declarations: [ScheduleCovidStep0Component],
  imports: [
    CommonModule,
    FormBaseModule,
    FormsModule,
    MfModalMessageModule,
    ReactiveFormsModule,
    ScheduleCovidStep0RoutingModule,
    StepperModule,
    MfShowErrorsModule,
    MfCheckboxModule,
    MfCardModule,
    MfModalMessageModule
  ],
  providers: [ScheduleCovidStep0Guard, GeolocationService]
})
export class ScheduleCovidStep0Module {}
