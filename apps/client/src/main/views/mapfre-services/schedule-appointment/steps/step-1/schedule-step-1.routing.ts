import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ScheduleStep1Component } from './schedule-step-1.component';

const routes: Routes = [
  {
    component: ScheduleStep1Component,
    path: ''
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ScheduleStep1RoutingModule {}
