import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBaseModule } from '@mx/components/shared/form-base/form-base.module';
import { GeolocationService } from '@mx/core/shared/helpers/util/geolocation.service';
import { MfInputDatepickerModule, MfModalMessageModule, MfSelectModule } from '@mx/core/ui';
import { MfSelectBoxModule } from '@mx/core/ui/lib/components/forms/select-box';
import { StepperModule } from '@mx/core/ui/lib/components/stepper/stepper.module';
import { MfTabTopModule } from '@mx/core/ui/lib/components/tabs/tabs-top/tab-top.module';
import { ScheduleCovidStep2Component } from './schedule-covid-step-2.component';
import { ScheduleCovidStep2Guard } from './schedule-covid-step-2.guard';
import { ScheduleCovidStep2RoutingModule } from './schedule-covid-step-2.routing';

@NgModule({
  declarations: [ScheduleCovidStep2Component],
  imports: [
    CommonModule,
    FormBaseModule,
    FormsModule,
    MfInputDatepickerModule,
    MfModalMessageModule,
    MfSelectBoxModule,
    MfSelectModule,
    MfTabTopModule,
    ReactiveFormsModule,
    ScheduleCovidStep2RoutingModule,
    StepperModule
  ],
  providers: [ScheduleCovidStep2Guard, GeolocationService]
})
export class ScheduleCovidStep2Module {}
