import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { isObjEmpty } from '@mx/core/shared/helpers';
import { AppointmentFacade } from '@mx/core/shared/state/appointment';

@Injectable()
export class ScheduleStep1Guard implements CanActivate {
  constructor(private readonly _AppointmentFacade: AppointmentFacade, private readonly router: Router) {}

  canActivate(): boolean {
    const st = this._AppointmentFacade.getState();
    if (isObjEmpty(st)) {
      this.router.navigate([this.router.url]);

      return false;
    }

    return true;
  }
}
