import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBaseModule } from '@mx/components/shared/form-base/form-base.module';
import { GeolocationService } from '@mx/core/shared/helpers/util/geolocation.service';
import { MfInputModule, MfModalMessageModule, MfSelectModule, MfShowErrorsModule } from '@mx/core/ui';
import { MfSelectBoxModule } from '@mx/core/ui/lib/components/forms/select-box';
import { StepperModule } from '@mx/core/ui/lib/components/stepper/stepper.module';
import { ScheduleCovidStep1Component } from './schedule-covid-step-1.component';
import { ScheduleCovidStep1Guard } from './schedule-covid-step-1.guard';
import { ScheduleCovidStep1RoutingModule } from './schedule-covid-step-1.routing';

@NgModule({
  declarations: [ScheduleCovidStep1Component],
  imports: [
    CommonModule,
    FormBaseModule,
    FormsModule,
    MfInputModule,
    MfModalMessageModule,
    MfSelectBoxModule,
    MfSelectModule,
    ReactiveFormsModule,
    ScheduleCovidStep1RoutingModule,
    StepperModule,
    MfShowErrorsModule
  ],
  providers: [ScheduleCovidStep1Guard, GeolocationService]
})
export class ScheduleCovidStep1Module {}
