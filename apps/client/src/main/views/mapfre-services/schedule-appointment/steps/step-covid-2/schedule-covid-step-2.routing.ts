import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ScheduleCovidStep2Component } from './schedule-covid-step-2.component';

const routes: Routes = [
  {
    component: ScheduleCovidStep2Component,
    path: ''
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ScheduleCovidStep2RoutingModule {}
