import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CaptureRouteSteps } from '../guards/schedule-appointment.guard';
import { ScheduleCovidStepsComponent } from './schedule-covid-steps.component';
import { ScheduleCovidStepsRoutingModule } from './schedule-covid-steps.routing';
import { AppointmentService } from './service/appointment.service';

@NgModule({
  imports: [CommonModule, ScheduleCovidStepsRoutingModule],
  declarations: [ScheduleCovidStepsComponent],
  providers: [AppointmentService, CaptureRouteSteps]
})
export class ScheduleCovidStepsModule {}
