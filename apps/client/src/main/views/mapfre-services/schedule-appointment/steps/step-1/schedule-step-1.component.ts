import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { getDDMMYYYY, isExpired } from '@mx/core/shared/helpers/util/date';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { UnsubscribeOnDestroy } from '@mx/core/shared/helpers/util/unsubscribe-on-destroy';
import { FrmProvider } from '@mx/core/shared/providers/services';
import { AppointmentFacade, State } from '@mx/core/shared/state/appointment';
import { MfModalMessage } from '@mx/core/ui/lib/components/modals/modal-message';
import { HealthService } from '@mx/services/health/health.service';
import { PoliciesService } from '@mx/services/policies.service';
import { STORAGE, TYPE_APPOINTMENT } from '@mx/settings/constants/general-values';
import { FILE_ICON, WARNING_ICON } from '@mx/settings/constants/images-values';
import { SCHEDULE_QUOTE_STEP } from '@mx/settings/constants/router-step';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { MediktorLang } from '@mx/settings/lang/mediktor.lang';
import { VehiclesLang } from '@mx/settings/lang/vehicles.lang';
import { ICardPoliciesView, IPoliciesByClientResponse } from '@mx/statemanagement/models/policy.interface';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/internal/operators/take';
import { AppointmentService } from '../../steps/service/appointment.service';

@Component({
  selector: 'client-schedule-step-1',
  templateUrl: './schedule-step-1.component.html'
})
export class ScheduleStep1Component extends UnsubscribeOnDestroy implements OnInit, OnDestroy {
  get placeholder(): string {
    return this._placeholder;
  }
  set placeholder(placeholder: string) {
    this._placeholder = placeholder;
    this.selectedDefault = true;
  }
  get options(): any {
    return this._listItem;
  }

  get groups(): any {
    return this._listGroup;
  }
  get value(): any {
    return this._value;
  }

  set value(value: any) {
    this._value = value;
    this.selectedValue(this._value);
    if (!this._disabled) {
      this.propagateChange(this.value);
    }
  }
  constructor(
    private readonly _AppointmentFacade: AppointmentFacade,
    private readonly _FrmProvider: FrmProvider,
    private readonly _Router: Router,
    public readonly policiesService: PoliciesService,
    public readonly healthService: HealthService,
    public readonly activatedRoute: ActivatedRoute,
    private readonly _StorageService: StorageService,
    private readonly _appointmentService: AppointmentService,
    @Inject(DOCUMENT) protected document: Document
  ) {
    super();
  }
  @ViewChild('cboPolicy') public cboPolicy: any;
  @ViewChild('cboPatient') public cboPatient: any;
  @ViewChild(MfModalMessage) modalMessage: MfModalMessage;
  policies: Array<IPoliciesByClientResponse>;
  centerButtonResponsive: any = { icon: FILE_ICON };
  currentPolicy: IPoliciesByClientResponse;
  searchByClientSub: Subscription;
  policyListModal: Array<ICardPoliciesView>;
  checked = false;
  stepList = SCHEDULE_QUOTE_STEP;
  fileIcon = FILE_ICON;
  lang = VehiclesLang;
  frm: FormGroup;
  graLng = GeneralLang;
  showLoading = false;
  appointment: State;
  arrPolicies: any[];
  iconError: string = WARNING_ICON;
  configView;
  hasPermission;
  params;
  isAutomatic;
  selectedPolicy: string;
  typeAppoiment = TYPE_APPOINTMENT;
  disabledButton = false;
  citaCovid: any;
  protected _disabled = false;
  selectedDefault = false;
  public valueCbo = 'value';
  protected _placeholder: string;
  protected _listItem;
  protected _listGroup;
  protected _value: any;
  disableConfirmInput = false;

  propagateChange: any = () => {};

  ngOnInit(): void {
    this.citaCovid = this._StorageService.getStorage(STORAGE.CITA_COVID);
    this._initValues();
    this._mapState();
    this.getPolicies();
    this._AppointmentFacade.AddPolicy(this.currentPolicy);
    this.frm = new FormGroup({
      selectedPolicy: new FormControl('', Validators.required),
      selectedCenter: new FormControl('')
    });
    this.getModality();
    this.disableConfirmInput = true;
  }

  ngOnDestroy(): void {
    if (this.citaCovid) {
      if (this.citaCovid.citaCovid) {
        this.document.getElementById('client-header-title2').style.display = 'none';
        this.document.getElementById('client-header-title1').style.display = 'block';
      }
    }
    const covid: any = {
      citaCovid: false
    };
    this._StorageService.saveStorage(STORAGE.CITA_COVID, covid);
  }

  getModality(): void {
    this._AppointmentFacade.getModality$.pipe(take(1)).subscribe(modality => {
      this.frm.get('selectedPolicy').setValue(modality);
    });
    if (this.citaCovid) {
      if (this.citaCovid.citaCovid) {
        this.frm.get('selectedPolicy').setValue(`${TYPE_APPOINTMENT.PRESENCIAL}`);
        this.disableConfirmInput = true;
      }
    }
  }

  private _mapState(): void {
    this._AppointmentFacade.getAppointment$.subscribe(st => {
      this.appointment = st;
      if (this.appointment.clinicaId) {
        this.value = this.appointment.clinicaId;
      }
    });
  }

  // NEXT ROUTER
  actionConfirm(): void {
    this._AppointmentFacade.AddFrmStep1(this.frm.value);
    this._AppointmentFacade.AddModality(this.frm.get('selectedPolicy').value);
    if (this.citaCovid) {
      if (this.citaCovid.citaCovid) {
        const covidEspeciality = {
          citaCovid: true
        };
        this._StorageService.saveStorage(STORAGE.CITA_COVID_ESPECIALITY, covidEspeciality);
      }
    }
    this._Router.navigate(['/digital-clinic/schedule/steps/2']);
  }

  optionsByGroup(group): void {
    const _newItems = this._listItem.filter(item => {
      return item.group === group;
    });

    return _newItems;
  }

  // SET SELECT FORM
  selectedValue(value: string): void {
    if (this._listItem) {
      this._listItem.forEach(item => {
        item.selected = item.value === value;
      });
    }
  }

  formatModalPolicies(): void {
    this.policyListModal = this.policies.map(policy => {
      const expiredPolicy = isExpired(policy.fechaFin);

      return {
        policyType: policy.tipoPoliza,
        policyNumber: policy.numeroPoliza,
        beneficiaries: policy.beneficiarios,
        policy: policy.descripcionPoliza,
        expired: expiredPolicy,
        endDate: getDDMMYYYY(policy.fechaFin),
        iconoRamo: policy.iconoRamo,
        isVIP: policy.esPolizaVip,
        response: policy
      } as ICardPoliciesView;
    });
  }
  actionCancel(): void {
    this.modalMessage.open();
  }

  // get policies
  getPolicies(): void {
    this.searchByClientSub = this.policiesService.getSortedHealthyPolicies().subscribe(
      (policies = []) => {
        const policiesNotPpfm = policies.filter(policy => !policy.esBeneficioAdicional);

        this.policies = policies.length > 1 ? (policiesNotPpfm.length === 0 ? policies : policiesNotPpfm) : policies;

        this.healthService.setPolicies(policies);
        if (policies.length) {
          const policyNumberQueryParam = this.activatedRoute.snapshot.queryParams['policy'];
          this.currentPolicy =
            policies.find(policy => policy.numeroPoliza === policyNumberQueryParam) || this.policies[0];
          this.healthService.setCurrentPolicy(this.currentPolicy);
          this.centerButtonResponsive.label = this.currentPolicy.descripcionPoliza;
          this.formatModalPolicies();
        }
      },
      () => {
        this.showLoading = false;
        this.searchByClientSub.unsubscribe();
      }
    );
  }

  onConfirmModal(r: boolean): void {
    if (r) {
      this._Router.navigateByUrl(`${this._appointmentService.getUrlReturn}`);
    }
  }

  private _initValues(): void {
    this.frm = this._FrmProvider.ScheduleStep1Component();
    if (this.citaCovid) {
      if (this.citaCovid.citaCovid) {
        this.document.getElementById('client-header-title1').style.display = 'none';
        this.document.getElementById(
          'client-header-title2'
        ).innerText = MediktorLang.Labels.ScheduleCitaCovid.toUpperCase();
        this.document.getElementById('client-header-title2').style.display = 'block';
      }
    }
  }
}
