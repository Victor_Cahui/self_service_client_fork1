import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FormBaseModule } from '@mx/components/shared/form-base/form-base.module';
import { MfShowErrorsModule } from '@mx/core/ui';
import { MfSelectBoxModule } from '@mx/core/ui/lib/components/forms/select-box';
import { StepperModule } from '@mx/core/ui/lib/components/stepper/stepper.module';
import { ScheduleStepSuccessComponent } from './schedule-step-success.component';
import { ScheduleStepSuccessGuard } from './schedule-step-success.guard';
import { ScheduleStepSuccessRoutingModule } from './schedule-step-success.routing';

@NgModule({
  declarations: [ScheduleStepSuccessComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ScheduleStepSuccessRoutingModule,
    FormBaseModule,
    MfShowErrorsModule,
    StepperModule,
    MfSelectBoxModule
  ],
  providers: [ScheduleStepSuccessGuard]
})
export class ScheduleStepSuccessModule {}
