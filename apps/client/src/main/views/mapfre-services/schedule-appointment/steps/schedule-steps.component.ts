import { Component, HostBinding } from '@angular/core';

@Component({
  selector: 'client-schedule-steps-component',
  templateUrl: './schedule-steps.component.html'
})
export class ScheduleStepsComponent {
  @HostBinding('class') class = 'w-100';
}
