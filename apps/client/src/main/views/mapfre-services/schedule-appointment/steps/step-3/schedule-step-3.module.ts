import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBaseModule } from '@mx/components/shared/form-base/form-base.module';
import { GeolocationService } from '@mx/core/shared/helpers/util/geolocation.service';
import { MfInputDatepickerModule, MfModalMessageModule, MfSelectModule } from '@mx/core/ui';
import { MfSelectBoxModule } from '@mx/core/ui/lib/components/forms/select-box';
import { StepperModule } from '@mx/core/ui/lib/components/stepper/stepper.module';
import { MfTabTopModule } from '@mx/core/ui/lib/components/tabs/tabs-top/tab-top.module';
import { ScheduleStep3Component } from './schedule-step-3.component';
import { ScheduleStep3Guard } from './schedule-step-3.guard';
import { ScheduleStep3RoutingModule } from './schedule-step-3.routing';

@NgModule({
  declarations: [ScheduleStep3Component],
  imports: [
    CommonModule,
    FormBaseModule,
    FormsModule,
    MfInputDatepickerModule,
    MfModalMessageModule,
    MfSelectModule,
    MfSelectBoxModule,
    MfTabTopModule,
    ReactiveFormsModule,
    ScheduleStep3RoutingModule,
    StepperModule
  ],
  providers: [ScheduleStep3Guard, GeolocationService]
})
export class ScheduleStep3Module {}
