import { DOCUMENT, Location } from '@angular/common';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { UnsubscribeOnDestroy } from '@mx/core/shared/helpers/util/unsubscribe-on-destroy';
import { Autoservicios, FrmProvider } from '@mx/core/shared/providers/services';
import { AppointmentFacade, State } from '@mx/core/shared/state/appointment';
import { ConfigCbo } from '@mx/core/ui/lib/components/forms/select';
import { ClientService } from '@mx/services/client.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import {
  APPLICATION_CODE,
  CENTER_VIRTUAL,
  GENERAL_MAX_LENGTH,
  NO_CENTER,
  REG_EX,
  STORAGE,
  TYPE_APPOINTMENT
} from '@mx/settings/constants/general-values';
import { FILE_ICON, WARNING_ICON } from '@mx/settings/constants/images-values';
import { SCHEDULE_QUOTE_STEP } from '@mx/settings/constants/router-step';
import { SCHEDULE_APPOINTMENT_STEP_2_HEADER } from '@mx/settings/constants/router-titles';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { MediktorLang } from '@mx/settings/lang/mediktor.lang';
import { VehiclesLang } from '@mx/settings/lang/vehicles.lang';
import { IClientInformationResponse } from '@mx/statemanagement/models/client.models';
import { IProfileBasicInfo } from '@mx/statemanagement/models/profile.interface';
import { take } from 'rxjs/internal/operators/take';
import { takeUntil } from 'rxjs/operators';
import { mapArrPolicies } from './schedule-step-2.utils';

@Component({
  selector: 'client-schedule-step-2',
  templateUrl: './schedule-step-2.component.html'
})
export class ScheduleStep2Component extends UnsubscribeOnDestroy implements OnInit, OnDestroy {
  stepList = SCHEDULE_QUOTE_STEP;
  fileIcon = FILE_ICON;
  lang = VehiclesLang;
  frm: FormGroup;
  graLng = GeneralLang;
  showLoading = true;
  showLoadingSpecialty = true;
  showLoadingPolicy = true;
  appointment: State;
  arrPolicies: any[];
  iconError: string = WARNING_ICON;
  modality: string;
  centerVirtual: string;
  numericRegExp = REG_EX.numeric;
  maxLengthCellphone = GENERAL_MAX_LENGTH.cellPhone;
  citaCovidSpeciality: any;
  visibleSpeciality = true;
  especialityCovid = [104, 503];
  especialityCovidObject: any;
  especialityCovidAmount = 50.0;
  especialityCovidCodMoneda = 1;
  configPatient: ConfigCbo;
  configPolicy: ConfigCbo;
  configSpeciality: ConfigCbo;

  private _patient: any;
  private _policy: any;
  private _specialty: any;
  private readonly _userInfo: IProfileBasicInfo;

  constructor(
    private readonly _AppointmentFacade: AppointmentFacade,
    private readonly _ClientService: ClientService,
    private readonly _FrmProvider: FrmProvider,
    private readonly _HeaderHelperService: HeaderHelperService,
    private readonly _Router: Router,
    private readonly _StorageService: StorageService,
    private readonly _Autoservicios: Autoservicios,
    private readonly _Location: Location,
    @Inject(DOCUMENT) protected document: Document
  ) {
    super();
  }

  ngOnInit(): void {
    this.citaCovidSpeciality = this._StorageService.getStorage(STORAGE.CITA_COVID_ESPECIALITY);
    this._initValues();
    this._mapState();
    this.getModality();

    setTimeout(() => {
      if (this.citaCovidSpeciality) {
        if (this.citaCovidSpeciality.citaCovid) {
          this.frm.get('especialidadId').setValue(this.especialityCovid[0]);
          this.visibleSpeciality = false;
        }
      }
    }, 1000);
  }

  ngOnDestroy(): void {
    if (this.citaCovidSpeciality) {
      if (this.citaCovidSpeciality.citaCovid) {
        this.document.getElementById('client-header-title2').style.display = 'none';
        this.document.getElementById('client-header-title1').style.display = 'block';
      }
    }
    const covid: any = {
      citaCovid: false
    };
    this._StorageService.saveStorage(STORAGE.CITA_COVID_ESPECIALITY, covid);
    this._StorageService.saveStorage(STORAGE.CITA_COVID, covid);
  }

  actionConfirm(): void {
    this._AppointmentFacade.AddPatient(this._patient);
    this._AppointmentFacade.AddPolicy(this._policy);
    this._AppointmentFacade.AddSpeciality(this._specialty);
    this._AppointmentFacade.AddFrmStep2(this.frm.value);
    this._AppointmentFacade.AddCellphone(this.frm.value.contactCellphone);
    if (this.citaCovidSpeciality) {
      if (this.citaCovidSpeciality.citaCovid) {
        this._specialty = this.especialityCovidObject;
        this._specialty.montoCopago = this.especialityCovidAmount;
        this._specialty.codigoMonedaCopago = this.especialityCovidCodMoneda;
        this._AppointmentFacade.AddSpeciality(this._specialty);
        const covid: any = {
          citaCovid: true
        };
        this._StorageService.saveStorage(STORAGE.CITA_COVID_SCHEDULE, covid);
      }
    }
    this._Router.navigate(['/digital-clinic/schedule/steps/3']);
  }

  actionCancel(): void {
    const covid: any = {
      citaCovid: false
    };

    this._StorageService.saveStorage(STORAGE.CITA_COVID_ESPECIALITY, covid);
    this._StorageService.saveStorage(STORAGE.CITA_COVID, covid);
    this._StorageService.saveStorage(STORAGE.CITA_COVID_SCHEDULE, covid);
    this._Location.back();
  }

  getSelectedPatient(p): void {
    this._patient = p;
    this.arrPolicies = mapArrPolicies(p.polizas);

    if (this.citaCovidSpeciality) {
      if (this.citaCovidSpeciality.citaCovid) {
        this.frm.get('especialidadId').setValue(this.especialityCovid[0]);
      }
    }
  }

  getSelectedPolicy(p): void {
    this._policy = p;
    let count = 0;
    setTimeout(() => {
      if (this.citaCovidSpeciality) {
        if (this.citaCovidSpeciality.citaCovid) {
          this.frm.get('especialidadId').setValue(this.especialityCovid[0]);
          this._AppointmentFacade.getAppointment$.subscribe(st => {
            if (count === 0) {
              this.getSpeciality(st.clinicaId, p.numeroPoliza);
            }
            count++;
          });
        }
      }
    }, 1000);
  }

  getSpeciality(clinicId, nroPoliza): void {
    const flagEsp = this.citaCovidSpeciality.citaCovid ? 'N' : 'S';
    if (clinicId !== undefined) {
      const elementStatic = {
        codigoMonedaCopago: this.especialityCovidCodMoneda,
        codigoTipoDeducible: 'DAGR',
        especialidadDescripcion: 'Prueba de Antígenos',
        especialidadId: this.especialityCovid[0],
        montoCopago: this.especialityCovidAmount
      };
      this._Autoservicios
        .ObtenerDetalleCita(
          {
            clinicaId: clinicId,
            codigoApp: APPLICATION_CODE,
            numeroPoliza: nroPoliza
          },
          {
            tipoCita: 'P',
            flagEspecialidad: flagEsp
          }
        )
        .subscribe(data => {
          if (data.especialidades !== null) {
            data.especialidades.forEach(element => {
              if (this.especialityCovid.includes(element.especialidadId)) {
                this.especialityCovidObject = element;
              }
            });
            if (this.especialityCovidObject === undefined) {
              this.especialityCovidObject = elementStatic;
            }
          }
        });
    }
  }

  getSelectedSpecialty(s): void {
    this._specialty = s;
  }

  getLoadingState(st): void {
    this.showLoading = st.isLoadingVisible;
  }

  getLoadingStatePolicy(st): void {
    this.showLoadingPolicy = st.isLoadingVisible;
  }

  getLoadingStateSpecialty(st): void {
    this.showLoadingSpecialty = st.isLoadingVisible;
  }

  private _mapState(): void {
    const getAppointment$ = this._AppointmentFacade.getAppointment$.subscribe(st => {
      this.appointment = st;
    });
    const getAppointmentOneTime$ = this._AppointmentFacade.getAppointment$
      .pipe(take(1))
      .subscribe(this._mapAppointmentOneTime.bind(this));
    this.arrToDestroy.push(getAppointment$, getAppointmentOneTime$);
  }

  getModality(): void {
    this._AppointmentFacade.getModality$.subscribe(modality => {
      if (modality === `${TYPE_APPOINTMENT.VIRTUAL}`) {
        this.modality = `${TYPE_APPOINTMENT.VIRTUAL}`;
        this.centerVirtual = `${CENTER_VIRTUAL}`;
      } else {
        this.modality = `${TYPE_APPOINTMENT.PRESENCIAL}`;
        this.centerVirtual = `${NO_CENTER}`;
      }
    });
  }

  private _mapAppointmentOneTime(st): void {
    if (st.frmStep1) {
      st.poliza && (this.arrPolicies = [{ ...st.poliza }]);

      return void 0;
    }

    this.arrPolicies = [...st.paciente.polizas];
    this.frm.patchValue(st.frmStep1);
  }

  private _initValues(): void {
    this._AppointmentFacade.CleanStep2();
    this._HeaderHelperService.set(SCHEDULE_APPOINTMENT_STEP_2_HEADER);
    this.frm = this._FrmProvider.ScheduleStep1Component();
    this.configPatient = {
      errorMsg: 'No hay pacientes disponibles',
      canAutoSelectFirstOpt: true
    };
    this.configPolicy = {
      errorMsg: 'No hay pólizas disponibles',
      canAutoSelectFirstOpt: true
    };
    this.configSpeciality = {
      errorMsg: 'No hay especialidades disponibles',
      canAutoSelectFirstOpt: false
    };

    this._ClientService
      .getClientInformation({})
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((res: IClientInformationResponse) => {
        this.frm.controls['contactCellphone'].setValue(res.telefonoMovil);
      });

    if (this.citaCovidSpeciality) {
      if (this.citaCovidSpeciality.citaCovid) {
        this.document.getElementById('client-header-title1').style.display = 'none';
        this.document.getElementById(
          'client-header-title2'
        ).innerText = MediktorLang.Labels.ScheduleCitaCovid.toUpperCase();
        this.document.getElementById('client-header-title2').style.display = 'block';
      }
    }
  }
}
