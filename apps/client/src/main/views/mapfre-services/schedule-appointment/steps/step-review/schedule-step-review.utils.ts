export function getReqPatient(state): any {
  return {
    apellidoMaternoPaciente: state.paciente.apellidoMaterno,
    apellidoPaternoPaciente: state.paciente.apellidoPaterno,
    documentoPaciente: state.paciente.documento,
    nombresPaciente: state.paciente.nombreCompleto,
    tipoDocumentoPaciente: state.paciente.tipoDocumento
  };
}

export function getReqCurrentUser(state): any {
  const { nombresUsuario } = state;

  return { nombresUsuario };
}

export function getReqPolicy(state): any {
  return {
    descripcionPoliza: state.poliza.descripcionPoliza,
    numeroPoliza: state.poliza.numeroPoliza
  };
}

export function getReqEstablishment(state): any {
  const { clinicaId, nombre } = state;

  return {
    clinicaDescripcion: nombre,
    clinicaId: clinicaId ? clinicaId : '451-0'
  };
}

export function getReqBody(state, authState): any {
  return {
    ...getReqCurrentUser(authState),
    ...getReqPolicy(state),
    ...getReqPatient(state),
    ...getReqEstablishment(state),
    ...state.especialidad,
    ...state.time,
    tipoCita: state.tipoCita,
    doctorId: +`${state.doctor.doctorId}`.replace('default-', ''),
    estadoCita: 'PENDIENTE',
    fecha: state.fecha,
    nombreCompleto: state.doctor.nombreCompleto,
    telefono: state.cellphone || state.telefono
  };
}
