export function mapArrPolicies(policies): any[] {
  return policies
    .map(po => ({
      ...po,
      numeroPoliza: po.descripcionPoliza.startsWith('EPS') && po.numeroContrato ? po.numeroContrato : po.numeroPoliza
    }))
    .filter(po => !po.esBeneficioAdicional);
}
