import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CaptureRouteSteps } from '../guards/schedule-appointment.guard';
import { ScheduleCovidStepsComponent } from './schedule-covid-steps.component';

const routes: Routes = [
  {
    path: '',
    component: ScheduleCovidStepsComponent,
    canActivate: [CaptureRouteSteps],
    children: [
      {
        path: '',
        redirectTo: 'test-type'
      },
      {
        path: 'test-type',
        loadChildren: './step-covid-0/schedule-covid-step-0.module#ScheduleCovidStep0Module'
      },
      {
        path: '1',
        loadChildren: './step-covid-1/schedule-covid-step-1.module#ScheduleCovidStep1Module'
      },
      {
        path: '2',
        loadChildren: './step-covid-2/schedule-covid-step-2.module#ScheduleCovidStep2Module'
      },
      {
        path: '3',
        loadChildren: './step-review/schedule-step-review.module#ScheduleStepReviewModule'
      },
      {
        path: '4',
        loadChildren: './step-success/schedule-step-success.module#ScheduleStepSuccessModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ScheduleCovidStepsRoutingModule {}
