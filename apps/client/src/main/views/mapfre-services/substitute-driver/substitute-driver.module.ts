import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  GmMsSearchModule,
  GmMsSubstituteDriverFormModule,
  GmMsSubstituteDriverSummaryModule,
  GoogleMapMyServiceModule
} from '@mx/components';
import { SubstituteDriverComponent } from './substitute-driver.component';
import { SubstituteDriverRouting } from './substitute-driver.routing';

@NgModule({
  imports: [
    CommonModule,
    SubstituteDriverRouting,
    GoogleMapMyServiceModule,
    GmMsSubstituteDriverFormModule,
    GmMsSubstituteDriverSummaryModule,
    GmMsSearchModule
  ],
  declarations: [SubstituteDriverComponent]
})
export class SubstituteDriverModule {}
