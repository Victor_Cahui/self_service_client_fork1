import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SubstituteDriverDetailComponent } from './substitute-driver-detail.component';

const routes: Routes = [
  {
    path: '',
    component: SubstituteDriverDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubstituteDriverDetailRouting {}
