import { DecimalPipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServicesUtils } from '@mx/components/shared/mapfre-services/services.utils';
import { HeaderHelperService, IHeader } from '@mx/services/general/header-helper.service';
import { ServicesMFService } from '@mx/services/services-mf.service';
import {
  DRIVER_ICON,
  DRIVER_REJECTED_ICON,
  PIN_END_MARK_ICON,
  PIN_START_MARK_ICON
} from '@mx/settings/constants/images-values';
import { SUBSTITUTE_DRIVER_HEADER } from '@mx/settings/constants/router-titles';
import { ICON_ITEMS, SERVICES_TYPE } from '@mx/settings/constants/services-mf';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { ServiceFormLang, SubstituteDriverLang } from '@mx/settings/lang/services.lang';
import { IUserMarker } from '@mx/statemanagement/models/google-map.interface';
import { IServicesItemResponse, ISummaryView } from '@mx/statemanagement/models/service.interface';

@Component({
  selector: 'client-substitute-driver-detail',
  templateUrl: './substitute-driver-detail.component.html',
  providers: [DecimalPipe]
})
export class SubstituteDriverDetailComponent implements OnInit {
  data: ISummaryView;
  fullMap: boolean;
  driverLang = SubstituteDriverLang;
  generalLang = GeneralLang;
  requestLang = ServiceFormLang;
  header: IHeader;
  startMarker: IUserMarker;
  endMarker: IUserMarker;

  constructor(
    private readonly headerHelperService: HeaderHelperService,
    private readonly activePath: ActivatedRoute,
    private readonly servicesMfService: ServicesMFService,
    private readonly decimalPipe: DecimalPipe
  ) {}

  ngOnInit(): void {
    const serviceNumber = this.activePath.snapshot.params.number;
    if (serviceNumber) {
      const data: IServicesItemResponse = this.servicesMfService.getServiceData(
        SERVICES_TYPE.SUBSTITUTE_DRIVER,
        serviceNumber
      );
      if (data) {
        this.setViewData(data);
      }
    }
  }

  setViewData(values: IServicesItemResponse): void {
    // Titulo
    this.header = ServicesUtils.getHeader(SUBSTITUTE_DRIVER_HEADER, values);
    this.headerHelperService.set(this.header);
    // Costo
    values.costoServicioFormat =
      values.costoServicio > 0 ? this.decimalPipe.transform(values.costoServicio, '1.2-2') : '';

    this.startMarker = {
      pinUrl: PIN_START_MARK_ICON,
      latitude: values.chofer.latitudOrigen,
      longitude: values.chofer.longitudOrigen
    };
    this.endMarker = {
      pinUrl: PIN_END_MARK_ICON,
      latitude: values.latitudDestino,
      longitude: values.longitudDestino
    };

    this.data = {
      icon: ServicesUtils.isRejected(values.codigoEstado) ? DRIVER_REJECTED_ICON : DRIVER_ICON,
      title: ServicesUtils.getStatusTitle(values),
      text: ServicesUtils.getStatusText(values),
      warning: ServicesUtils.getWarning(values),
      itemsDetail: [
        {
          label: `${GeneralLang.Labels.Driver.toUpperCase()}:`,
          text: values.empresaProveedora || this.requestLang.ToBeAssigned,
          icon: ICON_ITEMS.PERSON
        },
        {
          label: values.chofer.datosVehiculo,
          icon: ICON_ITEMS.VEHICLE
        }
      ],
      itemsSchedule: [
        {
          label: this.generalLang.Labels.DateTime,
          text: ServicesUtils.getScheduledText(values.fechaAgendada, values.codigoEstado)
        },
        {
          label: this.requestLang.StartPoint,
          text: values.chofer.origenServicio,
          other: `${this.requestLang.Reference}: ${values.chofer.referenciaOrigen}` || ''
        },
        {
          label: this.requestLang.EndPoint,
          text: values.destinoServicio,
          other: `${this.requestLang.Reference}: ${values.referenciaDestino}` || ''
        },
        {
          label: this.requestLang.ContactPhone,
          text: values.telefono
        }
      ]
    };
  }
}
