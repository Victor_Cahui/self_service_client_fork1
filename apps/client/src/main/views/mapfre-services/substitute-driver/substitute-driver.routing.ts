import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SubstituteDriverComponent } from './substitute-driver.component';

const routes: Routes = [
  {
    path: '',
    component: SubstituteDriverComponent
  },
  {
    path: 'detail/:number',
    loadChildren: './substitute-driver-detail/substitute-driver-detail.module#SubstituteDriverDetailModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubstituteDriverRouting {}
