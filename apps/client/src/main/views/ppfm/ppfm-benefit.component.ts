import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ConfigurationBase } from '@mx/components/shared/configuration/configuration-base';
import { PlatformService } from '@mx/core/shared/helpers/util/platform.service';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { MfModal } from '@mx/core/ui';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { GeneralService } from '@mx/services/general/general.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { INFO_ICON } from '@mx/settings/constants/images-values';
import { PPFM_SECT_DEFAULT_EMAIL, PPFM_SECT_DEFAULT_MESSAGE } from '@mx/settings/constants/key-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { BENEFICIOS_PPFM_HEADER } from '@mx/settings/constants/router-titles';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'client-ppfm-benefit',
  templateUrl: 'ppfm-benefit.component.html'
})
export class PpfmBenefitComponent extends ConfigurationBase implements OnInit {
  @ViewChild(MfModal) mfModal: MfModal;

  lang = GeneralLang;
  showLoading: boolean;
  showContactModal: boolean;
  phone: any;
  ppfmTitle: string;
  contactModalMessage: string;
  defaultMessage: string;
  defaultEmail: string;
  aditionalBenefitList: Array<any>;
  infoIcon = INFO_ICON;

  constructor(
    private readonly headerHelperService: HeaderHelperService,
    private readonly generalService: GeneralService,
    private readonly autoservicios: Autoservicios,
    private readonly router: Router,
    protected clientService: ClientService,
    protected platformService: PlatformService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService
  ) {
    super(clientService, policiesInfoService, configurationService, authService);
  }

  ngOnInit(): void {
    this.setHeader();
    this.getParameters();
    this.getAditionalBenetis();
    this.configurationService.setHideServicesIcons(true);
  }

  setHeader(): void {
    this.headerHelperService.set(BENEFICIOS_PPFM_HEADER);
  }

  getParameters(): void {
    this.generalService
      .getParametersSubject()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(() => {
        this.defaultMessage = this.generalService.getValueParams(PPFM_SECT_DEFAULT_MESSAGE);
        this.defaultEmail = this.generalService.getValueParams(PPFM_SECT_DEFAULT_EMAIL);
      });
  }

  getAditionalBenetis(): void {
    const queryReq = { codigoApp: APPLICATION_CODE };
    this.showLoading = true;
    this.autoservicios.ObtenerBeneficiosAdicionales(queryReq).subscribe(
      res => {
        this.showLoading = false;
        if (!res) {
          return void 0;
        }
        this.ppfmTitle = res.titulo;
        this.aditionalBenefitList = res.beneficios;

        this.aditionalBenefitList = res.beneficios.map((benefit: any) => {
          const isActive = benefit.estado === 'ACTIVO';
          const policyType = isActive && benefit.datosPoliza.tipoPoliza;
          const policyNumber = isActive && benefit.datosPoliza.numeroPoliza;
          const policyNumberLabel = isActive && `${this.lang.Texts.PolicyNumber}: ${benefit.datosPoliza.numeroPoliza}`;
          const isHogarOrAutos = [POLICY_TYPES.MD_HOGAR.code, POLICY_TYPES.MD_AUTOS.code].includes(policyType);
          const isSaludOrVida = [POLICY_TYPES.MD_SALUD.code, POLICY_TYPES.MD_VIDA.code].includes(policyType);
          const attrList = isHogarOrAutos ? 'listaRiesgos' : isSaludOrVida ? 'listaAsegurados' : null;
          let beneficiaries = isActive && benefit.datosPoliza[attrList];
          beneficiaries = isSaludOrVida ? beneficiaries.map(this._mapBeneficiaries.bind(this)) : beneficiaries;
          const attrDisplay = [POLICY_TYPES.MD_HOGAR.code].includes(policyType)
            ? 'direccionHogar'
            : [POLICY_TYPES.MD_AUTOS.code].includes(policyType)
            ? 'descripcion'
            : isSaludOrVida
            ? 'displayName'
            : null;

          !isActive && this.setModalMessage(benefit);

          return {
            ...benefit,
            isActive,
            ...(policyType && { policyType }),
            ...(policyNumber && { policyNumber }),
            ...(policyNumberLabel && { policyNumberLabel }),
            ...(beneficiaries && { beneficiaries }),
            ...(attrDisplay && { attrDisplay })
          };
        });
      },
      e => {
        this.showLoading = false;
        console.error(e);
      }
    );
  }

  openContactModal(): void {
    this.showContactModal = true;
    setTimeout(() => {
      this.mfModal.open();
    });
  }

  goToDetail(benefit: any): void {
    const type = benefit.datosPoliza.tipoPoliza;
    const path = `${POLICY_TYPES[type].path}/${benefit.datosPoliza.numeroPoliza}`;
    this.policiesInfoService.setClientPolicyResponse(benefit.datosPoliza);
    this.router.navigate([path]);
  }

  openWhatsapp(event: boolean): void {
    if (event) {
      const phone = this.phone.startsWith('51') ? this.phone.replace('51', '') : this.phone;
      const whatsappURL = `https://api.whatsapp.com/send?phone=51${phone.replace(/\s/g, '')}`;
      window.open(whatsappURL, '_blank');
    }
  }

  private setModalMessage(benefit: any): void {
    const mapAgent = benefit.datosContacto
      ? {
          agentName: benefit.datosContacto.nombreCompleto,
          agentEmail: benefit.datosContacto.correo,
          agentPhone: benefit.datosContacto.telefono
        }
      : null;
    this.contactModalMessage = benefit.datosContacto
      ? benefit.datosContacto.telefono
        ? this.lang.Messages.PpfmContactByPhone.replace(/agentName|agentEmail|agentPhone/gi, match => mapAgent[match])
        : this.lang.Messages.PpfmContactByEmail.replace(/agentName|agentEmail/gi, match => mapAgent[match])
      : `${this.defaultMessage}: <strong>${this.defaultEmail}</strong>`;
    this.phone = benefit.datosContacto && benefit.datosContacto.telefono;
  }

  private _mapBeneficiaries(beneficiary: any): any {
    return {
      ...beneficiary,
      displayName: StringUtil.getCapitalizedText(
        `${beneficiary.nombre} ${beneficiary.apellidoPaterno} ${beneficiary.apellidoMaterno}`
      )
    };
  }
}
