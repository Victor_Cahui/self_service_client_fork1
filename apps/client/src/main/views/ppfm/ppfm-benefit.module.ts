import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CardWhatYouWantToDoModule } from '@mx/components';
import { CardPolicyInsuredPPFMModule } from '@mx/components/policy/card-policy-insured-ppfm/card-policy-insured-ppfm.module';
import { BannerCarouselModule } from '@mx/components/shared/banner-carousel/banner-carousel.module';
import { BannerModule } from '@mx/components/shared/banner/banner.module';
import { DropdownItemsPolicyModule } from '@mx/components/shared/dropdown/dropdown-items-policy/dropdown-items-policy.module';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { MfModalModule } from '@mx/core/ui/lib/components/modals/modal';
import { PpfmBenefitComponent } from './ppfm-benefit.component';
import { PpfmBenefitRoutingModule } from './ppfm-benefit.routing';

@NgModule({
  imports: [
    CommonModule,
    PpfmBenefitRoutingModule,
    DropdownItemsPolicyModule,
    MfLoaderModule,
    CardWhatYouWantToDoModule,
    BannerCarouselModule,
    BannerModule,
    MfModalModule,
    CardPolicyInsuredPPFMModule
  ],
  declarations: [PpfmBenefitComponent]
})
export class PpfmBenefitModule {}
