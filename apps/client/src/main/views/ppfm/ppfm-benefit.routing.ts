import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PpfmBenefitComponent } from './ppfm-benefit.component';

const routes: Routes = [
  {
    path: '',
    component: PpfmBenefitComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PpfmBenefitRoutingModule {}
