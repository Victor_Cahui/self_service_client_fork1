import { Component, OnInit, ViewChild } from '@angular/core';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { MfModalAlertComponent } from '@mx/core/ui/lib/components/modals/modal-alert/modal-alert.component';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { GROUP_TYPE_ID_COMPANY, KEY_GROUP_TYPE_ID } from '@mx/settings/auth/auth-values';
import {
  MFP_Add_To_Cart_5D,
  MFP_Mis_Polizas_6A_Detail,
  MFP_Mis_Polizas_6A_SoatRenew,
  MFP_Mis_Polizas_6B,
  MFP_Siniestros_5E,
  MFP_ViewAll_5F
} from '@mx/settings/constants/events.analytics';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { EMAIL_ICON } from '@mx/settings/constants/images-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { IObtenerConfiguracionResponse } from '@mx/statemanagement/models/client.models';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
  @ViewChild(MfModalAlertComponent) mfModalConfirm: MfModalAlertComponent;

  ga: Array<IGaPropertie>;
  getConfigurationSub: Subscription;
  showLoading: boolean;
  isEnterprise: boolean;
  lang = GeneralLang;
  emailIcon = EMAIL_ICON;
  withoutPendingPayments: boolean;
  withoutSinisters: boolean;

  public imagesUrl: any;

  constructor(
    private readonly _ClientService: ClientService,
    private readonly _Autoservicios: Autoservicios,
    private readonly configurationService: ConfigurationService,
    private readonly _AuthService: AuthService,
    private readonly storageService: StorageService
  ) {
    this.ga = [
      MFP_Add_To_Cart_5D(),
      MFP_Siniestros_5E(),
      MFP_ViewAll_5F(),
      MFP_Mis_Polizas_6A_Detail(),
      MFP_Mis_Polizas_6A_SoatRenew(),
      MFP_Mis_Polizas_6B()
    ];
  }

  ngOnInit(): void {
    // slider
    this.imagesUrl = [
      'assets/images/banners/life/banner-lg.png',
      'assets/images/banners/household/banner-lg.png',
      'assets/images/banners/travel/banner-lg.png'
    ];

    // Verifica si es cliente empresa
    const groupTypeId = this.storageService.getItem(KEY_GROUP_TYPE_ID);
    this.isEnterprise = +groupTypeId === GROUP_TYPE_ID_COMPANY;

    const configuration = this.configurationService.getConfigurationByType();
    const hasDigitalClinic = this.configurationService.getConfigurationDigitalClinic() || '';

    if (!configuration) {
      this.getConfigurationSub = this._ClientService
        .getConfiguration({
          codigoApp: APPLICATION_CODE,
          esClinicaDigital: hasDigitalClinic
        })
        .subscribe((response: IObtenerConfiguracionResponse) => {
          this.configurationService.setConfiguration(response);
          this.getConfigurationSub.unsubscribe();
        });
    } else {
      this.configurationService.setConfiguration(configuration);
    }
    this._ClientService.canShowModalElectronicPolicy && this._showModalElectronicPolicy();
  }

  confirmElectronicPolicy(e: any): void {
    this.showLoading = true;
    const pathReq = { codigoApp: APPLICATION_CODE };
    const bodyReq = {
      appUsuario: APPLICATION_CODE,
      consentimiento: 'S'
    };
    this._Autoservicios.ConsentimientoPoliza(pathReq, bodyReq).subscribe(
      () => {
        this.showLoading = false;
        this.mfModalConfirm.close();
      },
      err => {
        this.showLoading = false;
        this.mfModalConfirm.close();
        console.error(err);
      }
    );
  }

  closeModal(): void {
    this._ClientService.canShowModalElectronicPolicy = false;
  }

  private _showModalElectronicPolicy(): void {
    if (!this._ClientService.isGroupTypePerson()) {
      return void 0;
    }

    const a = this._ClientService.getUserData();
    a.aceptoTerminosPoliza || this.mfModalConfirm.open();
  }
}
