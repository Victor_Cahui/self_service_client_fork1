import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { VehiclesShopComponent } from './vehicles-shop.component';

const routes: Routes = [
  {
    path: '',
    component: VehiclesShopComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiclesShopRoutingModule {}
