// tslint:disable: max-line-length
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'vehicularInsurance',
    loadChildren: './vehicles-insurance/vehicles-insurance.module#VehiclesInsuranceModule'
  },
  {
    path: 'soat',
    loadChildren: './vehicles-soat/vehicles-soat.module#VehiclesSoatModule'
  },
  {
    path: 'accidents',
    loadChildren: './vehicles-accidents/vehicles-accidents.module#VehiclesAccidentsModule'
  },
  {
    path: 'quote',
    loadChildren: './vehicles-quote/vehicles-quote.module#VehiclesQuoteModule'
  },
  {
    path: 'stolen',
    loadChildren: './vehicles-stolen/vehicles-stolen.module#VehiclesStolenModule'
  },
  {
    path: 'shop',
    loadChildren: './vehicles-shop/vehicles-shop.module#VehiclesShopModule'
  },
  {
    path: 'accidents/assists/detail/:number',
    // tslint:disable-next-line:max-line-length
    loadChildren:
      './vehicles-accidents/vehicle-accidents-assists/vehicle-accidents-assists-detail/vehicle-accidents-assists-detail.module#VehicleAccidentsAssistsDetailModule'
  },
  {
    path: 'stolen/assists/detail/:number',
    // tslint:disable-next-line:max-line-length
    loadChildren:
      './vehicles-stolen/vehicle-stolen-assists/vehicle-stolen-assists-detail/vehicle-stolen-assists-detail.module#VehicleStolenAssistsDetailModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiclesRoutingModule {}
