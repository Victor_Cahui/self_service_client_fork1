import { NgModule } from '@angular/core';
import { VehiclesSoatRoutingModule } from './vehicles-soat.routing';

@NgModule({
  imports: [VehiclesSoatRoutingModule],
  exports: [],
  declarations: [],
  providers: []
})
export class VehiclesSoatModule {}
