import { Component, OnInit } from '@angular/core';
import { ConfigurationBase } from '@mx/components/shared/configuration/configuration-base';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { MFP_SECTION_24A, MFP_SOAT_23A, MFP_SOAT_23B, MFP_SOAT_23C } from '@mx/settings/constants/events.analytics';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { LANDING_SOAT } from '@mx/settings/lang/landing';

@Component({
  selector: 'client-vehicles-soat-list-component',
  templateUrl: './vehicles-soat-list.component.html'
})
export class VehiclesSoatListComponent extends ConfigurationBase implements OnInit {
  ga: Array<IGaPropertie>;
  landingContent = LANDING_SOAT;

  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService
  ) {
    super(clientService, policiesInfoService, configurationService, authService);
    this.ga = [MFP_SOAT_23A(), MFP_SOAT_23B(), MFP_SOAT_23C(), MFP_SECTION_24A()];
  }

  ngOnInit(): void {
    this.policyType = POLICY_TYPES.MD_SOAT.code;
    this.loadConfiguration();
  }
}
