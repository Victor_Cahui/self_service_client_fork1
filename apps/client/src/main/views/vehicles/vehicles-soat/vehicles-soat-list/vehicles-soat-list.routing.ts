import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VehiclesSoatListComponent } from './vehicles-soat-list.component';

const routes: Routes = [
  {
    path: '',
    component: VehiclesSoatListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiclesSoatListRoutingModule {}
