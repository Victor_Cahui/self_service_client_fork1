import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardMyPoliciesByTypeModule } from '@mx/components/card-my-policies/card-my-policies-by-type/card-my-policies-by-type.module';
import { CardWhatYouWantToDoModule } from '@mx/components/card-what-you-want-to-do/card-what-you-want-to-do.module';
import { VehiclesRoutingModule } from './vehicles-routing.module';
import { ClientVehiclesRootComponent } from './vehicles.component';

@NgModule({
  imports: [CommonModule, VehiclesRoutingModule, CardMyPoliciesByTypeModule, CardWhatYouWantToDoModule],
  declarations: [ClientVehiclesRootComponent]
})
export class VehiclesModule {}
