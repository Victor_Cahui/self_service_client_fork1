import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'client-vehicles-root-component',
  template: '<router-outlet></router-outlet>'
})
export class ClientVehiclesRootComponent implements OnInit {
  ngOnInit(): void {}
}
