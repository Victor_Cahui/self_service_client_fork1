import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { VehiclesRecordsComponent } from './vehicles-records.component';

const routes: Routes = [
  {
    path: '',
    component: VehiclesRecordsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiclesRecordsRoutingModule {}
