import { ArrayUtil } from '@mx/core/shared/helpers/util/array';
import { RECORDS } from '@mx/settings/lang/vehicles.lang';

export function isAccidentsPath(url: string): boolean {
  return /accidents/gi.test(url);
}

export function isStolenPath(url: string): boolean {
  return /stolen/gi.test(url);
}

export function getYears(arr): Array<string> {
  const years: string[] = arr.map(v => v.year);

  return Array.from(new Set(years));
}

export function getYearsToCboOptions(arrRecords: any[]): Array<any> {
  const years = getYears(arrRecords);

  return ArrayUtil.arraySortByKey(mapToCboOptions(years), 'text', 'DESC');
}

export function mapToCboOptions(arrYears: string[]): Array<any> {
  const opts = arrYears.map(y => ({ text: y, value: y }));

  return [{ text: RECORDS.Label.CboYear, value: 0 }, ...opts];
}

export function getItemsByYear(arrItems: Array<any>, year: string): Array<any> {
  return arrItems.filter(i => i.year === year);
}
