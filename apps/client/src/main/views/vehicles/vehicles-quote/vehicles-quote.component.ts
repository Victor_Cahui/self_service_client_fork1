import { Component, OnInit, ViewChild } from '@angular/core';
import { ConfigurationBase } from '@mx/components/shared/configuration/configuration-base';
import { FormEditPlateQuoteComponent } from '@mx/components/shared/form-edit-plate-quote/form-edit-plate-quote.component';
// tslint:disable-next-line: max-line-length
import { ModalCurrentPolicyQuoteComponent } from '@mx/components/shared/modals/modal-current-policy-quote/modal-current-policy-quote.component';
import { ModalMoreFaqsComponent } from '@mx/components/shared/modals/modal-more-faqs/modal-more-faqs.component';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { ScrollUtil } from '@mx/core/shared/helpers/util/scroll';
import { environment } from '@mx/environments/environment';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { PoliciesQuoteService } from '@mx/services/policy/policy-quote.service';
import { MFP_Cotizacion_Plan_10A, MFP_Faqs_10B } from '@mx/settings/constants/events.analytics';
import { MAPFRE_VEHICLES_QUOTE_DIR } from '@mx/settings/constants/general-values';
import { POLICY_TYPES, STATIC_BANNERS } from '@mx/settings/constants/policy-values';
import { VEHICLES_FAQ_HEADER } from '@mx/settings/constants/router-titles';
import { VEHICLES_INSURANCE_FAQS } from '@mx/settings/faqs/vehicles-insurance.faq';
import * as LANG from '@mx/settings/lang/vehicles.lang';
import { IPathImageSize } from '@mx/statemanagement/models/general.models';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-quote-component',
  templateUrl: './vehicles-quote.component.html'
})
export class VehiclesQuoteComponent extends ConfigurationBase implements OnInit {
  @ViewChild(ModalMoreFaqsComponent) modalFaqs: ModalMoreFaqsComponent;
  @ViewChild(ModalCurrentPolicyQuoteComponent) modalMessage: ModalCurrentPolicyQuoteComponent;
  @ViewChild(FormEditPlateQuoteComponent) formEditPlateQuoteComponent: FormEditPlateQuoteComponent;
  ga: Array<IGaPropertie>;
  gaFaqs: Array<IGaPropertie>;

  howToWork = LANG.HOW_TO_WORK;
  paymentOptions = LANG.PAYMENTS_OPTIONS;
  insuranceTop = LANG.INSURANCE_TOP;
  faqs = LANG.VEHICLE_INSURANCE_FAQ;
  modalSubTitle = VEHICLES_FAQ_HEADER.subTitle;
  moreFaqs = VEHICLES_INSURANCE_FAQS;
  vehiclesLang = LANG.VehiclesLang;
  environment = environment;
  vehicles_quote_visible = environment.ACTIVATE_VEHICLES_QUOTE;

  auth: IdDocument;
  staticBanners: string;
  pathImgBanner: IPathImageSize;
  scrollSub: Subscription;
  showModalMessage: boolean;
  showModalFaqs: boolean;
  vigencyDate: string;
  flagPolicyStatus: string;

  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService,
    private readonly quoteService: PoliciesQuoteService
  ) {
    super(clientService, policiesInfoService, configurationService, authService);
    this.ga = [MFP_Cotizacion_Plan_10A()];
    this.gaFaqs = [MFP_Faqs_10B()];
  }

  ngOnInit(): void {
    // Banner
    this.policyType = POLICY_TYPES.MD_AUTOS.code;
    this.staticBanners = POLICY_TYPES[this.policyType].staticBanners;
    this.pathImgBanner = {
      sm: this.staticBanners + STATIC_BANNERS.sm,
      lg: this.staticBanners + STATIC_BANNERS.lg,
      xl: this.staticBanners + STATIC_BANNERS.xl
    };
    this.loadConfiguration();
    this.quoteService.clearAllQuoteData();
  }

  openModalMoreFaqs(): void {
    this.showModalFaqs = true;
    setTimeout(() => {
      this.modalFaqs.open();
    }, 100);
  }

  // Scroll a form
  eventQuote(): void {
    this.scrollSub = ScrollUtil.goTo(0).subscribe(
      () => {
        this.formEditPlateQuoteComponent.setFocusPlateInput();
      },
      () => {},
      () => {
        this.scrollSub.unsubscribe();
      }
    );
  }

  // Poliza vigente
  showMessage(data): void {
    if (data && data.show) {
      this.showModalMessage = true;
      this.flagPolicyStatus = data.flag;
      this.vigencyDate = data.date;
      setTimeout(() => {
        this.modalMessage.open();
      }, 100);
    }
  }

  // Redirección temporal al cotizador de mapfre (aut-1094)
  gotoMapfreQuote(): void {
    window.open(MAPFRE_VEHICLES_QUOTE_DIR, '_blank');
  }
}
