import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBaseModule } from '@mx/components/shared/form-base/form-base.module';
import { ModalCurrentPolicyQuoteModule } from '@mx/components/shared/modals/modal-current-policy-quote/modal-current-policy-quote.module';
import { ModalEditPlateModule } from '@mx/components/shared/modals/modal-edit-plate/modal-edit-plate.module';
import { MfCheckboxModule, MfInputModule, MfSelectModule, MfShowErrorsModule } from '@mx/core/ui';
import { MfCustomAlertModule } from '@mx/core/ui/lib/components/alerts/custom-alert';
import { StepperModule } from '@mx/core/ui/lib/components/stepper/stepper.module';
import { VehiclesQuoteStepOneComponent } from './vehicles-quote-step-one.component';
import { VehiclesQuoteStepOneRoutingModule } from './vehicles-quote-step-one.routing';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    VehiclesQuoteStepOneRoutingModule,
    FormBaseModule,
    MfSelectModule,
    MfInputModule,
    MfShowErrorsModule,
    MfCheckboxModule,
    StepperModule,
    MfCustomAlertModule,
    ModalEditPlateModule,
    ModalCurrentPolicyQuoteModule
  ],
  declarations: [VehiclesQuoteStepOneComponent]
})
export class VehiclesQuoteStepOneModule {}
