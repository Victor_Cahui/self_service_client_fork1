import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VehiclesQuoteStepOneComponent } from './vehicles-quote-step-one.component';

const routes: Routes = [
  {
    path: '',
    component: VehiclesQuoteStepOneComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiclesQuoteStepOneRoutingModule {}
