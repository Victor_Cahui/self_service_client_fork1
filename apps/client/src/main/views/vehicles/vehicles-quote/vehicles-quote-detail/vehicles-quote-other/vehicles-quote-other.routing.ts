import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VehiclesQuoteOtherComponent } from './vehicles-quote-other.component';

const routes: Routes = [
  {
    path: '',
    component: VehiclesQuoteOtherComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiclesQuoteOtherRoutingModule {}
