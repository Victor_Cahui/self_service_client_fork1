import { Component, OnInit } from '@angular/core';
import { VEHICLES_QUOTE_STEP } from '@mx/settings/constants/router-step';

@Component({
  selector: 'client-vehicles-quote-step-three',
  templateUrl: './vehicles-quote-step-three.component.html'
})
export class VehiclesQuoteStepThreeComponent implements OnInit {
  stepList = VEHICLES_QUOTE_STEP;

  ngOnInit(): void {}
}
