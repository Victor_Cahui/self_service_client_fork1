import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormBaseModule } from '@mx/components/shared/form-base/form-base.module';
import { FormContactedByAgentModule } from '@mx/components/shared/form-contacted-by-agent/form-contacted-by-agent.module';
import { MfButtonModule } from '@mx/core/ui/lib/components/forms/button';
import { VehiclesQuoteOtherComponent } from './vehicles-quote-other.component';
import { VehiclesQuoteOtherRoutingModule } from './vehicles-quote-other.routing';

@NgModule({
  imports: [CommonModule, VehiclesQuoteOtherRoutingModule, FormBaseModule, FormContactedByAgentModule, MfButtonModule],
  exports: [],
  declarations: [VehiclesQuoteOtherComponent],
  providers: []
})
export class VehiclesQuoteOtherModule {}
