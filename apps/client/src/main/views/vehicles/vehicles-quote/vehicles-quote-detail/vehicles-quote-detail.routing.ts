import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VehicleQuoteDetail2Guard } from '@mx/guards/vehicle/vehicle-quote-detail-2.guard';
import { VehicleQuoteDetailGuard } from '@mx/guards/vehicle/vehicle-quote-detail.guard';
import { VehiclesQuoteDetailComponent } from './vehicles-quote-detail.component';

const routes: Routes = [
  {
    path: '',
    component: VehiclesQuoteDetailComponent,
    canActivate: [VehicleQuoteDetailGuard],
    children: [
      {
        path: '',
        redirectTo: 'step1'
      },
      {
        path: 'step1',
        loadChildren: './vehicles-quote-step-one/vehicles-quote-step-one.module#VehiclesQuoteStepOneModule'
      },
      {
        path: 'step2',
        canActivate: [VehicleQuoteDetail2Guard],
        loadChildren: './vehicles-quote-step-two/vehicles-quote-step-two.module#VehiclesQuoteStepTwoModule'
      },
      {
        path: 'other',
        loadChildren: './vehicles-quote-other/vehicles-quote-other.module#VehiclesQuoteOtherModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiclesQuoteDetailRoutingModule {}
