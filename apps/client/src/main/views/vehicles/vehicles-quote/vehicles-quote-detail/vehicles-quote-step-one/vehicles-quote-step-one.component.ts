import { DecimalPipe } from '@angular/common';
import { Component, HostBinding, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BaseCardMyPolicies } from '@mx/components/card-my-policies/base-card-my-policies';
// tslint:disable-next-line: max-line-length
import { ModalCurrentPolicyQuoteComponent } from '@mx/components/shared/modals/modal-current-policy-quote/modal-current-policy-quote.component';
import { ModalEditPlateComponent } from '@mx/components/shared/modals/modal-edit-plate/modal-edit-plate.component';
import { FormComponent } from '@mx/components/shared/utils/form';
import { PolicyUtil } from '@mx/components/shared/utils/policy.util';
import { validURL } from '@mx/core/shared/helpers/functions/general-functions';
import { ArrayUtil } from '@mx/core/shared/helpers/util/array';
import { CurrentYear } from '@mx/core/shared/helpers/util/date';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { AmountValidator } from '@mx/core/ui';
import { NotificationStatus } from '@mx/core/ui/lib/components/alerts/custom-alert/custom-alert.component';
import { GeneralService } from '@mx/services/general/general.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PolicyListService } from '@mx/services/policy/policy-list.service';
import { PoliciesQuoteService } from '@mx/services/policy/policy-quote.service';
import { VehicleService } from '@mx/services/vehicle.service';
import {
  MFP_Cotiza_Tu_Seguro_Paso_1_12A,
  MFP_Cotiza_Tu_Seguro_Paso_1_12B
} from '@mx/settings/constants/events.analytics';
import {
  APPLICATION_CODE,
  DEFAULT_MPD_CURRENCY,
  GENERAL_MAX_LENGTH,
  OTHER_CITY,
  REG_EX
} from '@mx/settings/constants/general-values';
import { GPS_VEHICLE_QUOTE, YEARS_VEHICLE_QUOTE } from '@mx/settings/constants/key-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { VEHICLES_QUOTE_STEP } from '@mx/settings/constants/router-step';
import { VEHICLES_QUOTE_DETAIL_HEADER, VEHICLES_QUOTE_HEADER } from '@mx/settings/constants/router-titles';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { VehiclesLang } from '@mx/settings/lang/vehicles.lang';
import { IParameterApp, ISelectList } from '@mx/statemanagement/models/general.models';
import { ICoveragesPolicyRequest } from '@mx/statemanagement/models/policy.interface';
import {
  IClientVehicleDetail,
  ICompareVehicleRequest,
  IVehicleBrandsResponse,
  IVehicleCityResponse,
  IVehicleModelsResponse,
  IVehicleQuoteRequest,
  IVehicleTypeResponse,
  IVehicleUseResponse,
  IVehicleValuesResponse,
  IVerifyGPSRequiredResponse
} from '@mx/statemanagement/models/vehicle.interface';
import { Observable, Subscription, zip } from 'rxjs';

@Component({
  selector: 'client-vehicles-quote-step-one',
  templateUrl: './vehicles-quote-step-one.component.html',
  providers: [DecimalPipe]
})
export class VehiclesQuoteStepOneComponent extends BaseCardMyPolicies implements OnInit, OnDestroy {
  @HostBinding('class') class = 'w-100';
  @ViewChild(ModalEditPlateComponent) modalEditPlate: ModalEditPlateComponent;
  @ViewChild(ModalCurrentPolicyQuoteComponent) modalMessage: ModalCurrentPolicyQuoteComponent;

  lang = VehiclesLang;
  placeHolder = GeneralLang.Buttons.Select;
  plate: string;
  policyType: string;
  maxYears = 10;
  maxLength = GENERAL_MAX_LENGTH.amount;
  showIsNew: boolean;
  currency: number;
  currencySymbol: string;
  minValue: number;
  maxValue: number;
  minValueText: string;
  maxValueText: string;
  gpsInstallation: string;
  params: ICoveragesPolicyRequest;
  body: ICompareVehicleRequest;

  vehicleData: IClientVehicleDetail;
  btnNext = GeneralLang.Buttons.Next;
  btnCancel = GeneralLang.Buttons.Cancel;
  brandList: Array<ISelectList>;
  modelList: Array<ISelectList>;
  yearList: Array<ISelectList>;
  typeList: Array<ISelectList>;
  useList: Array<ISelectList>;
  cityList: Array<ISelectList>;
  stepList = VEHICLES_QUOTE_STEP;
  showAlert: boolean;
  checkedIsNew: boolean;
  showLoading: boolean;
  showModalEdit: boolean;
  showModalMessage: boolean;
  changedData: boolean;
  flagPolicyStatus: string;
  vigencyDate: string;

  formData: FormGroup;
  brand: AbstractControl;
  model: AbstractControl;
  year: AbstractControl;
  type: AbstractControl;
  suggestedValue: AbstractControl;
  use: AbstractControl;
  isNew: AbstractControl;
  city: AbstractControl;

  brandsSub: Subscription;
  modelsSub: Subscription;
  parameterSub: Subscription;
  typeSub: Subscription;
  valuesSub: Subscription;
  citySub: Subscription;
  useSub: Subscription;
  gpsSub: Subscription;
  zipSub: Subscription;
  compareHeaderSub: Subscription;
  compareDetailsSub: Subscription;
  pattern = REG_EX.amount;
  parametersSubject: Observable<Array<IParameterApp>>;

  cityListOrigin: Array<IVehicleCityResponse>;

  ga: Array<IGaPropertie>;
  status = NotificationStatus.INFO;

  constructor(
    protected generalService: GeneralService,
    protected policiesService: PoliciesService,
    private readonly headerHelperService: HeaderHelperService,
    private readonly quoteService: PoliciesQuoteService,
    protected policyListService: PolicyListService,
    private readonly router: Router,
    private readonly fb: FormBuilder,
    private readonly vehicleService: VehicleService,
    private readonly decimalPipe: DecimalPipe
  ) {
    super(generalService, policiesService);
    this.headerHelperService.set(VEHICLES_QUOTE_DETAIL_HEADER);
    this.ga = [MFP_Cotiza_Tu_Seguro_Paso_1_12A(), MFP_Cotiza_Tu_Seguro_Paso_1_12B()];
  }

  ngOnInit(): void {
    this.policyType = POLICY_TYPES.MD_AUTOS.code;
    this.plate = this.quoteService.getVehiclePlate();
    this.vehicleData = this.quoteService.getVehicleData();
    this.gpsInstallation = this.lang.Messages.VehicleNewQuote.slice(
      0,
      this.lang.Messages.VehicleNewQuote.indexOf('{{gpsCompanies}}')
    );
    this.getListInit();
    this.createForm();
  }

  ngOnDestroy(): void {
    if (this.brandsSub) {
      this.brandsSub.unsubscribe();
    }
    if (this.modelsSub) {
      this.modelsSub.unsubscribe();
    }
    if (this.parameterSub) {
      this.parameterSub.unsubscribe();
    }
    if (this.typeSub) {
      this.typeSub.unsubscribe();
    }
    if (this.valuesSub) {
      this.valuesSub.unsubscribe();
    }
    if (this.citySub) {
      this.citySub.unsubscribe();
    }
    if (this.useSub) {
      this.useSub.unsubscribe();
    }
    if (this.gpsSub) {
      this.gpsSub.unsubscribe();
    }
  }

  // Volver a Landing
  actionCancel(): void {
    this.router.navigate([VEHICLES_QUOTE_HEADER.url]);
  }

  // Botón Continuar
  actionConfirm(): void {
    if (this.formData.valid && parseInt(this.suggestedValue.value, 10) > 0) {
      this.changedData = this.isChangedData();
      const data: IClientVehicleDetail = this.quoteService.getVehicleData();
      // Guardar datos en local
      data.placa = this.plate;
      data.placaEnTramite = !this.plate;
      data.marca = ArrayUtil.getTextfromList(this.brandList, this.brand.value);
      data.modelo = ArrayUtil.getTextfromList(this.modelList, this.model.value);
      data.tipoVehiculo = ArrayUtil.getTextfromList(this.typeList, this.type.value);
      data.uso = ArrayUtil.getTextfromList(this.useList, this.use.value);
      data.ciudad = ArrayUtil.getTextfromList(this.cityList, this.city.value);
      data.anio = parseInt(this.year.value, 10);
      data.codigoMarca = parseInt(this.brand.value, 10);
      data.codigoModelo = parseInt(this.model.value, 10);
      data.codigoTipoVehiculo = parseInt(this.type.value, 10);
      data.codigoUso = parseInt(this.use.value, 10);
      data.codigoCiudad = parseInt(this.city.value, 10);
      data.ubigeoId = this.cityListOrigin.find(city => city.identificadorCiudad === data.codigoCiudad).ubigeoId;
      data.codigoMonedaValorSugerido = this.currency;
      data.valorSugerido = parseInt(this.suggestedValue.value, 10);
      data.es0Km = this.isNew.value || false;
      data.valorMaximo = this.maxValue;
      data.valorMinimo = this.minValue;
      data.requiereGPS = this.showAlert;
      this.quoteService.setVehicleData(data);
      this.policyListService.setPolicySelected(null);
      //  Si cuidad es Otros
      if (data.ciudad === OTHER_CITY) {
        this.router.navigate(['/vehicles/quote/detail/other']);
      } else {
        // Buscar datos para cuadro, si todo bien va a paso 2
        this.getDataQuote();
      }
    }
  }

  // Editar Placa
  editPlate(): void {
    this.showModalEdit = true;
    setTimeout(() => {
      this.modalEditPlate.open();
    }, 100);
  }

  // Mensaje Poliza vigente
  showMessage(data): void {
    this.showModalEdit = false;
    if (data && data.show) {
      this.showModalMessage = true;
      this.flagPolicyStatus = data.flag;
      this.vigencyDate = data.date;
      setTimeout(() => {
        this.modalMessage.open();
      }, 100);
    }
    this.modalEditPlate.close();
  }

  // Actualizar Vista
  showDataVehicle(update: boolean): void {
    this.showModalEdit = false;
    if (update) {
      this.vehicleData = this.quoteService.getVehicleData();
      this.plate = this.vehicleData.placa;
      FormComponent.setControl(this.brand, this.vehicleData.codigoMarca || '');
      FormComponent.setControl(this.model, this.vehicleData.codigoModelo || '');
      FormComponent.setControl(this.year, this.vehicleData.anio || '');
      FormComponent.setControl(this.type, this.vehicleData.codigoTipoVehiculo || '');
      FormComponent.setControl(this.use, this.vehicleData.codigoUso || '');
      FormComponent.setControl(this.isNew, this.vehicleData.es0Km || false);
      FormComponent.setControl(this.city, this.vehicleData.codigoCiudad || '');
      this.showIsNew = this.compareYear(this.vehicleData.anio);
      this.showAlert = this.vehicleData.requiereGPS;
      this.currency = this.vehicleData.codigoMonedaValorSugerido || DEFAULT_MPD_CURRENCY;
      this.setSuggestedValue(
        this.vehicleData.valorSugerido,
        this.vehicleData.valorMinimo,
        this.vehicleData.valorMaximo,
        this.vehicleData.codigoMonedaValorSugerido
      );
      this.modelList = [];
      this.typeList = [];
      this.useList = [];
      if (this.vehicleData.codigoMarca) {
        this.getModelsList();
        this.getTypeList();
        this.getUseList();
      }
      this.verifyYearList(this.vehicleData.anio);
    }
  }

  // Seleccionar Marca
  changeBrand(event): void {
    this.modelList = [];
    this.typeList = [];
    this.useList = [];
    FormComponent.setControl(this.model);
    FormComponent.setControl(this.type);
    FormComponent.setControl(this.use);
    FormComponent.setControl(this.suggestedValue);
    if (!event.target.value) {
      return;
    }
    this.getModelsList();
    this.showAlert = false;
  }

  // Seleccionar Modelo
  changeModel(event): void {
    this.typeList = [];
    this.useList = [];
    FormComponent.setControl(this.type);
    FormComponent.setControl(this.use);
    FormComponent.setControl(this.suggestedValue);
    if (!event.target.value) {
      return;
    }
    this.getTypeList();
    this.showAlert = false;
  }

  // Seleccionar año
  changeYear(event): void {
    if (!event.target.value) {
      return;
    }
    this.showIsNew = this.compareYear(event.target.value);
    if (!this.showIsNew) {
      this.isNew.setValue(false);
    }
    this.getValues();
    this.requiredGPS();
  }

  // Seleccionar Tipo
  changeType(event): void {
    this.useList = [];
    FormComponent.setControl(this.use);
    if (!event.target.value) {
      return;
    }
    this.getValues();
    this.getUseList();
    this.requiredGPS();
  }

  // Seleccionar uso
  changeUse(event): void {
    if (!event.target.value) {
      return;
    }
    this.getValues();
    this.requiredGPS();
  }

  // Vehiculo Nuevo
  vehicleIsNew(event): void {
    this.isNew.setValue(event.target.checked);
    this.getValues();
    this.requiredGPS();
  }

  // Crear Form
  createForm(): void {
    this.formData = this.fb.group({
      brand: [
        { value: this.vehicleData.codigoMarca ? this.vehicleData.codigoMarca : '', disabled: false },
        Validators.required
      ],
      model: [
        { value: this.vehicleData.codigoModelo ? this.vehicleData.codigoModelo : '', disabled: false },
        Validators.required
      ],
      year: [{ value: this.vehicleData.anio ? this.vehicleData.anio : '', disabled: false }, Validators.required],
      type: [
        { value: this.vehicleData.codigoTipoVehiculo ? this.vehicleData.codigoTipoVehiculo : '', disabled: false },
        Validators.required
      ],
      suggestedValue: [{ value: '', disabled: false }, Validators.compose(this.getValidatorsSuggestedValue())],
      use: [
        { value: this.vehicleData.codigoUso ? this.vehicleData.codigoUso : '', disabled: false },
        Validators.required
      ],
      isNew: [{ value: this.vehicleData.es0Km ? this.vehicleData.es0Km : false, disabled: false }],
      city: [
        { value: this.vehicleData.codigoCiudad ? this.vehicleData.codigoCiudad : '', disabled: false },
        Validators.required
      ]
    });
    this.brand = this.formData.controls['brand'];
    this.model = this.formData.controls['model'];
    this.year = this.formData.controls['year'];
    this.type = this.formData.controls['type'];
    this.suggestedValue = this.formData.controls['suggestedValue'];
    this.use = this.formData.controls['use'];
    this.isNew = this.formData.controls['isNew'];
    this.city = this.formData.controls['city'];

    // Inicializa Monto si está modificando
    if (this.vehicleData.valorSugerido) {
      this.currency = this.vehicleData.codigoMonedaValorSugerido;
      this.setSuggestedValue(
        this.vehicleData.valorSugerido,
        this.vehicleData.valorMinimo,
        this.vehicleData.valorMaximo,
        this.vehicleData.codigoMonedaValorSugerido
      );
    }
    // Inicializa Combos si está modificando
    if (this.vehicleData.codigoModelo) {
      this.showAlert = this.vehicleData.requiereGPS;
      this.getModelsList();
      this.getTypeList();
      this.getUseList();
    }

    this.showIsNew = this.vehicleData.anio ? this.compareYear(this.vehicleData.anio) : false;
    this.checkedIsNew = this.vehicleData.es0Km ? this.vehicleData.es0Km : false;

    // Valida año dentro del select
    this.verifyYearList(this.vehicleData.anio);
  }

  showErrors(control: string): boolean {
    return FormComponent.showBasicErrors(this.formData, control);
  }

  // Obtener datos iniciales para los select
  getListInit(): void {
    // Limpiar
    this.brandList = [];
    this.modelList = [];
    this.yearList = [];
    this.typeList = [];
    this.useList = [];
    this.cityList = [];
    // Marcas
    this.getBrandsList();
    // Año
    this.getYearList();
    // Ciudad
    this.getCityList();
  }

  // Marca
  getBrandsList(): void {
    this.brandsSub = this.vehicleService.getBrands({ codigoApp: APPLICATION_CODE } as IVehicleQuoteRequest).subscribe(
      (res: Array<IVehicleBrandsResponse>) => {
        if (res && res.length) {
          this.brandList = res.map(brand => {
            return { text: brand.nombreMarca, value: brand.identificadorMarca.toString() } as ISelectList;
          });
        }
      },
      () => {},
      () => {
        this.brandsSub.unsubscribe();
      }
    );
  }

  // Modelo
  getModelsList(): void {
    this.modelsSub = this.vehicleService
      .getModels({ codigoApp: APPLICATION_CODE, marca: this.brand.value } as IVehicleQuoteRequest)
      .subscribe(
        (res: Array<IVehicleModelsResponse>) => {
          if (res && res.length) {
            this.modelList = res.map(model => {
              return { text: model.nombreModelo, value: model.identificadorModelo.toString() } as ISelectList;
            });
          }
        },
        () => {},
        () => {
          this.modelsSub.unsubscribe();
        }
      );
  }

  // Años de Fabricación
  getYearList(): void {
    this.parametersSubject = this.generalService.getParametersSubject();
    this.parametersSubject.subscribe(params => {
      if (!this.yearList.length) {
        const years = parseInt(this.generalService.getValueParams(YEARS_VEHICLE_QUOTE), 10) || 0;
        this.maxYears = years > 0 && years < 21 ? years : this.maxYears;
        this.yearList = PolicyUtil.setYears(this.maxYears);
      }
      const url = this.generalService.getValueParams(GPS_VEHICLE_QUOTE);
      if (validURL(url)) {
        this.gpsInstallation = this.lang.Messages.VehicleNewQuote.replace(
          '{{gpsCompanies}}',
          `<a class="g-font--medium" href="${url}" target="_blank">${GeneralLang.Labels.Companies}</a>`
        );
      }
    });
  }

  // Tipo de Vehículo
  getTypeList(): void {
    this.typeSub = this.vehicleService
      .getType({
        codigoApp: APPLICATION_CODE,
        marca: this.brand.value,
        modelo: this.model.value
      } as IVehicleQuoteRequest)
      .subscribe(
        (res: Array<IVehicleTypeResponse>) => {
          if (res && res.length) {
            this.typeList = res.map(type => {
              return { text: type.nombreTipoVehiculo, value: type.identificadorTipoVehiculo.toString() } as ISelectList;
            });
          }
        },
        () => {},
        () => {
          this.typeSub.unsubscribe();
        }
      );
  }

  // Uso de Vehículo
  getUseList(): void {
    this.useSub = this.vehicleService
      .getUse({
        codigoApp: APPLICATION_CODE,
        marca: this.brand.value,
        modelo: this.model.value,
        tipoVehiculo: this.type.value
      } as IVehicleQuoteRequest)
      .subscribe(
        (res: Array<IVehicleUseResponse>) => {
          if (res && res.length) {
            this.useList = res.map(use => {
              return { text: use.nombreUsoVehiculo, value: use.identificadorUsoVehiculo.toString() } as ISelectList;
            });
          }
        },
        () => {},
        () => {
          this.useSub.unsubscribe();
        }
      );
  }

  // Valor sugerido
  getValues(): void {
    if (!(this.brand.value && this.model.value && this.year.value && this.type.value)) {
      return;
    }
    const params: IVehicleQuoteRequest = {
      codigoApp: APPLICATION_CODE,
      marca: this.brand.value,
      modelo: this.model.value,
      anio: this.year.value,
      tipoVehiculo: this.type.value,
      es0KM: this.isNew.value || false
    };
    if (this.use.value) {
      params.usoVehiculo = this.use.value;
    }
    this.currency = DEFAULT_MPD_CURRENCY;
    this.valuesSub = this.vehicleService.getValues(params).subscribe(
      (res: IVehicleValuesResponse) => {
        if (res) {
          this.setSuggestedValue(res.montoValorSugerido, res.montoValorMinimo, res.montoValorMaximo, res.codigoMoneda);
          this.currency = res.codigoMoneda || DEFAULT_MPD_CURRENCY;
        }
      },
      () => {},
      () => {
        this.valuesSub.unsubscribe();
      }
    );
  }

  // Cuidades
  getCityList(): void {
    this.citySub = this.vehicleService.getCity({ codigoApp: APPLICATION_CODE } as IVehicleQuoteRequest).subscribe(
      (res: Array<IVehicleCityResponse>) => {
        if (res && res.length) {
          this.cityListOrigin = res;
          this.cityList = res.map(city => {
            return { text: city.nombreCiudad, value: city.identificadorCiudad.toString() } as ISelectList;
          });
        }
      },
      () => {},
      () => {
        this.citySub.unsubscribe();
      }
    );
  }

  // Requiere GPS
  requiredGPS(): void {
    if (!(this.brand.value && this.model.value && this.year.value)) {
      return;
    }
    const params: IVehicleQuoteRequest = {
      codigoApp: APPLICATION_CODE,
      marca: this.brand.value,
      modelo: this.model.value,
      anio: this.year.value
    };
    if (this.use.value) {
      params.usoVehiculo = this.use.value;
    }
    if (this.type.value) {
      params.tipoVehiculo = this.type.value;
    }
    if (this.isNew.value) {
      params.es0KM = this.isNew.value;
    }

    this.gpsSub = this.vehicleService.verifyGPSRequired(params).subscribe(
      (res: IVerifyGPSRequiredResponse) => {
        if (res) {
          this.showAlert = res.requiereGPS;
        }
      },
      () => {},
      () => {
        this.gpsSub.unsubscribe();
      }
    );
  }

  // Comparar año
  compareYear(year: number): boolean {
    const currentYear = CurrentYear();

    return year >= currentYear - 1 && year <= currentYear;
  }

  // asignar año
  verifyYearList(year: number): void {
    if (!year) {
      return;
    }
    const yearFind = ArrayUtil.getTextfromList(this.yearList, year);
    if (!yearFind) {
      const newYear = year.toString();
      this.yearList.push({ text: newYear, value: newYear });
    }
  }

  // Formato y Validaciones para Valor Sugerido
  setSuggestedValue(value: number, min: number, max: number, currency: number): void {
    this.currencySymbol = `${StringUtil.getMoneyDescription(currency)} `;
    this.minValue = min || 0;
    this.maxValue = max || 0;
    this.minValueText = `${this.currencySymbol} ${this.formatValue(min)}`;
    this.maxValueText = `${this.currencySymbol} ${this.formatValue(max)}`;
    FormComponent.setControl(this.suggestedValue, value.toString());
    this.suggestedValue.setValidators(this.getValidatorsSuggestedValue());
    this.suggestedValue.updateValueAndValidity();
  }

  getValidatorsSuggestedValue(): Array<ValidatorFn> {
    const validators: Array<ValidatorFn> = [];
    validators.push(Validators.maxLength(Number(this.maxLength)));
    if (this.maxValue > 0) {
      validators.push(AmountValidator.isRangeValid(this.minValue, this.maxValue));
    }
    validators.push(Validators.required);

    return validators;
  }

  formatValue(value: number): string {
    return this.decimalPipe.transform(value, '1.0-0');
  }

  // Buscar datos para cuadro si hay cambios en la data
  getDataQuote(): void {
    // Verifica si ha cambiado el vehiculo y si hay datos por mostrar
    const header = this.quoteService.getHeaderData();
    const details = this.quoteService.getDetailsData();
    if (!this.changedData && header && header.length && details && details.coberturas.length) {
      this.router.navigate([VEHICLES_QUOTE_STEP[1].route]);
    } else {
      // Va a servicio
      this.showLoading = true;
      this.params = {
        codigoApp: APPLICATION_CODE
      };
      this.vehicleData = this.quoteService.getVehicleData();
      this.body = {
        marcaId: this.vehicleData.codigoMarca,
        modeloId: this.vehicleData.codigoModelo,
        anio: this.vehicleData.anio,
        tipoVehiculoId: this.vehicleData.codigoTipoVehiculo,
        valorSugerido: this.vehicleData.valorSugerido,
        usoVehiculoId: this.vehicleData.codigoUso,
        ciudadId: this.vehicleData.codigoCiudad,
        tienePlaca: !this.vehicleData.placaEnTramite,
        placa: this.vehicleData.placa,
        es0KM: this.vehicleData.es0Km || false,
        codigoMoneda: this.vehicleData.codigoMonedaValorSugerido
      };
      this.quoteService.setHeaderData(null);
      this.quoteService.setDetailsData(null);
      const compareHeader$ = this.vehicleService.getCompareHeader(this.params, this.body);
      const compareDetails$ = this.vehicleService.getCompareDetails(this.params, this.body);
      this.zipSub = zip(compareHeader$, compareDetails$).subscribe(
        res => {
          // Header
          if (res[0] && res[0].length) {
            // Select
            this.quoteService.setHeaderData(res[0]);
          }
          // Detalle
          if (res[1]) {
            this.quoteService.setDetailsData(res[1]);
          }
          // Va al paso 2 si hay datos por mostrar
          if (res[1] && res[1].coberturas && res[0] && res[0].length) {
            this.router.navigate([VEHICLES_QUOTE_STEP[1].route]);
          }
        },
        () => {
          this.showLoading = false;
        },
        () => {
          this.zipSub.unsubscribe();
          this.showLoading = false;
        }
      );
    }
  }

  // Verifica si hay cambios en la data
  isChangedData(): boolean {
    const data = this.quoteService.getVehicleData();

    return (
      data.placa !== this.plate ||
      data.es0Km !== this.isNew.value ||
      data.codigoCiudad !== this.city.value ||
      data.codigoMarca !== this.brand.value ||
      data.codigoModelo !== this.model.value ||
      data.anio !== this.year.value ||
      data.codigoTipoVehiculo !== this.type.value ||
      data.valorSugerido !== this.suggestedValuesToNumber() ||
      data.codigoUso !== this.use.value
    );
  }

  validValue(): boolean {
    return this.suggestedValuesToNumber() > 0;
  }

  private suggestedValuesToNumber(): number {
    return parseInt(this.suggestedValue.value, 10);
  }
  // tslint:disable-next-line: max-file-line-count
}
