import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BannerCarouselModule } from '@mx/components/shared/banner-carousel/banner-carousel.module';
import { BannerModule } from '@mx/components/shared/banner/banner.module';
import { CardFaqModule } from '@mx/components/shared/card-faq/card-faq.module';
import { CardHowToWorkModule } from '@mx/components/shared/card-how-to-work/card-how-to-work.module';
import { CardInsurancePlansModule } from '@mx/components/shared/card-insurance-plans/card-insurance-plans.module';
import { CardPaymentOptionsModule } from '@mx/components/shared/card-payment-options/card-payment-options.module';
import { FormEditPlateQuoteModule } from '@mx/components/shared/form-edit-plate-quote/form-edit-plate-quote.module';
import { ModalCurrentPolicyQuoteModule } from '@mx/components/shared/modals/modal-current-policy-quote/modal-current-policy-quote.module';
import { ModalMoreFaqsModule } from '@mx/components/shared/modals/modal-more-faqs/modal-more-faqs.module';
import { DirectivesModule, MfButtonModule } from '@mx/core/ui';
import { VehiclesQuoteComponent } from './vehicles-quote.component';
import { VehiclesQuoteRoutingModule } from './vehicles-quote.routing';

@NgModule({
  imports: [
    VehiclesQuoteRoutingModule,
    CommonModule,
    BannerCarouselModule,
    BannerModule,
    CardInsurancePlansModule,
    CardHowToWorkModule,
    CardPaymentOptionsModule,
    CardFaqModule,
    ModalMoreFaqsModule,
    DirectivesModule,
    FormEditPlateQuoteModule,
    ModalCurrentPolicyQuoteModule,
    MfButtonModule
  ],
  exports: [],
  declarations: [VehiclesQuoteComponent],
  providers: []
})
export class VehiclesQuoteModule {}
