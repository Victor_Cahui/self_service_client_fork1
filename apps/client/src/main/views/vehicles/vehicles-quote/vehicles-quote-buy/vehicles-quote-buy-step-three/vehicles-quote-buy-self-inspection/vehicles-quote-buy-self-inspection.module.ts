import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { MfButtonModule } from '@mx/core/ui';
import { VehiclesQuoteBuySelfInspectionComponent } from './vehicles-quote-buy-self-inspection.component';

@NgModule({
  imports: [CommonModule, MfButtonModule, GoogleModule],
  exports: [VehiclesQuoteBuySelfInspectionComponent],
  declarations: [VehiclesQuoteBuySelfInspectionComponent]
})
export class VehiclesQuoteBuySelfInspectionModule {}
