import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CardSummaryQuoteComponent } from '@mx/components/shared/payments/card-summary-quote/card-summary-quote.component';
import { FormComponent } from '@mx/components/shared/utils/form';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { MfModal } from '@mx/core/ui';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { PaymentService } from '@mx/services/payment.service';
import { PoliciesQuoteService } from '@mx/services/policy/policy-quote.service';
import { VehicleService } from '@mx/services/vehicle.service';
import { MFP_Compra_Tu_Seguro_Paso_2_15A } from '@mx/settings/constants/events.analytics';
import { APPLICATION_CODE, GENERAL_MAX_LENGTH, GENERAL_MIN_LENGTH } from '@mx/settings/constants/general-values';
import { VEHICLES_QUOTE_BUY_HEADER } from '@mx/settings/constants/router-titles';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { ProfileLang } from '@mx/settings/lang/profile.lang';
import { VehicleQuoteBuyLang, VehiclesLang } from '@mx/settings/lang/vehicles.lang';
import { IClientInformationResponse, IContractor } from '@mx/statemanagement/models/client.models';
import { GeneralRequest } from '@mx/statemanagement/models/general.models';
import { IPaymentQuoteCard } from '@mx/statemanagement/models/payment.models';
import { IVehicleBuyRequest } from '@mx/statemanagement/models/vehicle.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-vehicles-quote-buy-step-two',
  templateUrl: './vehicles-quote-buy-step-two.component.html'
})
export class VehiclesQuoteBuyStepTwoComponent extends GaUnsubscribeBase implements OnInit, OnDestroy {
  @ViewChild(MfModal) modal: MfModal;

  header = VEHICLES_QUOTE_BUY_HEADER;
  generalLang = GeneralLang;
  vehicleLang = VehiclesLang;
  lang = VehicleQuoteBuyLang;
  profileLang = ProfileLang;
  maxLength = GENERAL_MAX_LENGTH;
  minLength = GENERAL_MIN_LENGTH;

  clientInformation: IClientInformationResponse;
  contractor: IContractor;
  quote: IPaymentQuoteCard;

  form: FormGroup;
  clauseSub: Subscription;
  buyDataSub: Subscription;

  clause: string;
  extraLabel: string;

  constructor(
    private readonly quoteService: PoliciesQuoteService,
    private readonly clientService: ClientService,
    private readonly vehicleService: VehicleService,
    private readonly paymentService: PaymentService,
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly fb: FormBuilder,
    protected gaService: GAService
  ) {
    super(gaService);
    this.quoteService.setCurrentStep(2);
    this.ga = [MFP_Compra_Tu_Seguro_Paso_2_15A()];
  }

  ngOnInit(): void {
    this.clientInformation = this.clientService.getUserData();
    const quote = this.paymentService.getCurrentQuote();
    this.contractor = quote && quote.contractor;
    this.extraLabel = `<span class="g-c--green1 g-font--medium"> ${this.lang.Step2.Clause.end}</span>`;
    this.createForm();
    this.getBuyData();
    this.getClause();
  }

  ngOnDestroy(): void {
    if (this.clauseSub) {
      this.clauseSub.unsubscribe();
    }
  }

  getClause(): void {
    const clause = this.quoteService.getPersonalDataClause();
    if (clause) {
      this.clause = clause;
    } else {
      this.clauseSub = this.vehicleService.getPersonalDataClause({ codigoApp: APPLICATION_CODE }).subscribe(res => {
        this.clause = res.descripcionClausula || '';
        this.quoteService.setPersonalDataClause(this.clause);
        this.clauseSub.unsubscribe();
      });
    }
  }

  createForm(): void {
    this.form = this.fb.group({
      email: [
        (this.contractor && this.contractor.email) || this.clientInformation.emailCliente || '',
        FormComponent.emailValidators(this.maxLength.card_email, this.minLength.card_email)
      ],
      phone: [
        (this.contractor && this.contractor.phone) || this.clientInformation.telefonoMovil || '',
        FormComponent.phoneValidators(this.maxLength.sd_phone_contact, this.minLength.sd_phone_contact)
      ],
      clause: [false, Validators.requiredTrue]
    });

    setTimeout(() => {
      this.form.controls['clause'].setValue(!!this.contractor);
    });
  }

  clauseChange(event): void {
    this.form.controls['clause'].setValue(event.target.checked);
  }

  back(): void {
    this.router.navigate(['/vehicles/quote/buy/step1']);
  }

  removeSpaces(controlName: string): void {
    FormComponent.removeSpaces(this.form.controls[controlName]);
  }

  showErrors(controlName: string): boolean {
    return FormComponent.showBasicErrors(this.form, controlName);
  }

  clauseOpen(): void {
    this.modal.open();
  }

  verifyInfo(): void {
    const address = this.form.getRawValue().address;
    const uca = address.useCorrespondenceAddress;
    // Guardar en storage
    const quote = this.paymentService.getCurrentQuote();
    quote.contractor = {
      documentType: this.clientInformation.tipoDocumento,
      document: this.clientInformation.documento,
      email: this.form.controls['email'].value,
      phone: this.form.controls['phone'].value,
      departmentId: uca ? address.correspondenceAddress.identificadorDepartamento : parseInt(address.departments, 10),
      department: uca ? address.correspondenceAddress.departamento : address.departmentName || '',
      provinceId: uca ? address.correspondenceAddress.identificadorProvincia : parseInt(address.provinces, 10),
      province: uca ? address.correspondenceAddress.provincia : address.provinceName || '',
      districtId: uca ? address.correspondenceAddress.identificadorDistrito : parseInt(address.districts, 10),
      district: uca ? address.correspondenceAddress.distrito : address.districtName || '',
      address: uca ? address.correspondenceAddress.direccion : address.address,
      useDefault: uca,
      reference: address.reference || ''
    };
    this.paymentService.setCurrentQuote(quote);
    // Condicionar flujo hacia inspeccion
    if (this.quoteService.requiresInspection()) {
      this.router.navigate(['/vehicles/quote/buy/step3']);
    } else {
      this.router.navigate(['/vehicles/quote/buy/step4']);
    }
  }

  // Obtener datos de la compra
  getBuyData(): void {
    this.quote = this.paymentService.getCurrentQuote();
    if (this.quote && !this.quote.dataQuoteVehicle) {
      const vehicle = this.quoteService.getVehicleData();
      const params: GeneralRequest = {
        codigoApp: APPLICATION_CODE
      };
      const body: IVehicleBuyRequest = {
        numeroCotizacion: this.quote.modalitySelected.quoteNumber.toString(),
        fechaInicioVigencia: vehicle.fechaVigencia,
        codigoCompania: this.quote.modalitySelected.company,
        codigoRamo: this.quote.modalitySelected.branch,
        codigoModalidad: this.quote.modalitySelected.modality,
        numeroPlaca: vehicle.placa,
        tipoVehiculo: vehicle.codigoTipoVehiculo,
        codigoMarca: vehicle.codigoMarca,
        codigoModelo: vehicle.codigoModelo,
        anioFabricacion: vehicle.anio,
        tipoUsoVehiculo: vehicle.codigoUso,
        sumaAsegurada: vehicle.valorSugerido,
        esCeroKm: vehicle.es0Km,
        ciudadId: vehicle.codigoCiudad,
        tienePlaca: !vehicle.placaEnTramite,
        codigoMoneda: vehicle.codigoMonedaValorSugerido,
        numeroCuota: this.quote.modalitySelected.quotas,
        codigoFraccionamiento: this.quote.modalitySelected.fractionationCode,
        anios: this.quote.modalitySelected.years
      };
      this.buyDataSub = this.vehicleService.getQuoteData(params, body).subscribe(
        data => {
          this.quote = this.paymentService.getCurrentQuote();
          this.quote.dataQuoteVehicle = data;
          this.paymentService.setCurrentQuote(this.quote);
          // Actualizar Resumen de Compra
          if (CardSummaryQuoteComponent.updateMPD) {
            CardSummaryQuoteComponent.updateMPD.next();
          }
        },
        () => {},
        () => {
          this.buyDataSub.unsubscribe();
        }
      );
    }
  }
}
