import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VehiclesQuoteBuyStepTwoComponent } from './vehicles-quote-buy-step-two.component';

const routes: Routes = [
  {
    path: '',
    component: VehiclesQuoteBuyStepTwoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiclesQuoteBuyStepTwoRoutingModule {}
