import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VehiclesQuoteBuyStepThreeComponent } from './vehicles-quote-buy-step-three.component';

const routes: Routes = [
  {
    path: '',
    component: VehiclesQuoteBuyStepThreeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiclesQuoteBuyStepThreeRoutingModule {}
