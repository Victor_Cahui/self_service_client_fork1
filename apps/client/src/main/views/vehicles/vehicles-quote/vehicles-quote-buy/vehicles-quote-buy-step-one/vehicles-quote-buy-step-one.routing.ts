import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VehiclesQuoteBuyStepOneComponent } from './vehicles-quote-buy-step-one.component';

const routes: Routes = [
  {
    path: '',
    component: VehiclesQuoteBuyStepOneComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiclesQuoteBuyStepOneRoutingModule {}
