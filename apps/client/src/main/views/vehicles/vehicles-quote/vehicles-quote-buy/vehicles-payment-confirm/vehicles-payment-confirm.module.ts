import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BankPaymentConfirmModule } from '@mx/components/shared/payments/bank-payment-confirm/bank-payment-confirm.module';
import { VehiclesPaymentConfirmComponent } from './vehicles-payment-confirm.component';
import { VehiclesPaymentConfirmRoutingModule } from './vehicles-payment-confirm.routing';

@NgModule({
  imports: [CommonModule, VehiclesPaymentConfirmRoutingModule, BankPaymentConfirmModule],
  declarations: [VehiclesPaymentConfirmComponent],
  exports: [VehiclesPaymentConfirmComponent]
})
export class VehiclesPaymentConfirmModule {}
