import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { QuoteBuyAddressModule } from '@mx/components/vehicles/quote-buy-address/quote-buy-address.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { MfButtonModule, MfCheckboxModule, MfInputModule, MfModalModule, MfShowErrorsModule } from '@mx/core/ui';
import { VehiclesQuoteBuyStepTwoComponent } from './vehicles-quote-buy-step-two.component';
import { VehiclesQuoteBuyStepTwoRoutingModule } from './vehicles-quote-buy-step-two.routing';

@NgModule({
  imports: [
    CommonModule,
    GoogleModule,
    MfButtonModule,
    MfCheckboxModule,
    MfInputModule,
    MfModalModule,
    MfShowErrorsModule,
    QuoteBuyAddressModule,
    ReactiveFormsModule,
    VehiclesQuoteBuyStepTwoRoutingModule
  ],
  declarations: [VehiclesQuoteBuyStepTwoComponent]
})
export class VehiclesQuoteBuyStepTwoModule {}
