import { CurrencyPipe, DecimalPipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PaymentQuotaBase } from '@mx/components/shared/payments/payment-quota-base';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { PLATFORMS, PlatformService } from '@mx/core/shared/helpers/util/platform.service';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { GeneralService } from '@mx/services/general/general.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PaymentService } from '@mx/services/payment.service';
import { PoliciesQuoteService } from '@mx/services/policy/policy-quote.service';
import { CAR_ICON, FILE_ICON } from '@mx/settings/constants/images-values';
import * as KEYS from '@mx/settings/constants/key-values';
import { VEHICLES_QUOTE_BUY_HEADER } from '@mx/settings/constants/router-titles';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { MethodPaymentLang } from '@mx/settings/lang/payment.lang';
import { VehiclesLang } from '@mx/settings/lang/vehicles.lang';
import { IParameterApp } from '@mx/statemanagement/models/general.models';
import { IClientVehicleDetail } from '@mx/statemanagement/models/vehicle.interface';
import { Observable } from 'rxjs';

@Component({
  selector: 'client-vehicles-payment-confirm',
  templateUrl: './vehicles-payment-confirm.component.html',
  providers: [CurrencyPipe, DecimalPipe]
})
export class VehiclesPaymentConfirmComponent extends PaymentQuotaBase implements OnInit {
  lang = VehiclesLang;
  carIcon = CAR_ICON;
  buyIcon = FILE_ICON;
  buyLang = MethodPaymentLang;
  buyTitle = MethodPaymentLang.BuyData;
  appTitle = GeneralLang.Titles.AppDownload;
  appText = GeneralLang.Messages.AppDownload;
  btnAppStore: string;
  btnAppStoreHover: string;
  btnPlayStore: string;
  btnPlayStoreHover: string;
  lnkAppStore: string;
  lnkPlayStore: string;
  vehicleData: IClientVehicleDetail;
  vehicleValue: string;
  vehicleDescription: string;
  parametersSubject: Observable<Array<IParameterApp>>;
  showAppStore: boolean;
  showPlayStore: boolean;

  constructor(
    protected headerHelperService: HeaderHelperService,
    protected paymentService: PaymentService,
    protected router: Router,
    protected decimalPipe: DecimalPipe,
    protected currencyPipe: CurrencyPipe,
    protected gaService: GAService,
    private readonly quoteService: PoliciesQuoteService,
    private readonly platformService: PlatformService,
    private readonly generalService: GeneralService
  ) {
    super(headerHelperService, paymentService, router, decimalPipe, currencyPipe, gaService);
  }

  ngOnInit(): void {
    // Valida plataforma para mostrar botones de descarga
    const navigator = this.platformService.getPlarform();
    if (navigator !== PLATFORMS.IOSMOBILE && navigator !== PLATFORMS.ANDROID) {
      this.showAppStore = true;
      this.showPlayStore = true;
    } else {
      this.showAppStore = navigator === PLATFORMS.IOSMOBILE;
      this.showPlayStore = navigator === PLATFORMS.ANDROID;
    }
    // Configura Vista
    this.getCurrentQuote();
    const header = VEHICLES_QUOTE_BUY_HEADER;
    header.back = false;
    this.headerHelperService.set(header);
    this.setView(true);
    this.getVehicleData();
    this.setDownLoadView();
  }

  // Datos del Vehículo
  getVehicleData(): void {
    this.vehicleData = this.quoteService.getVehicleData();
    if (this.vehicleData) {
      const currency = `${StringUtil.getMoneyDescription(this.vehicleData.codigoMonedaValorSugerido)} `;
      this.vehicleValue = this.currencyPipe.transform(this.vehicleData.valorSugerido, currency);
      this.vehicleDescription = `${this.vehicleData.marca} ${this.vehicleData.modelo} ${this.vehicleData.anio}`.toLowerCase();
      this.vehicleData.uso = this.vehicleData.uso.toLowerCase();
      this.vehicleData.ciudad = this.vehicleData.ciudad.toLowerCase();
      this.vehicleData.color = (this.vehicleData.color && this.vehicleData.color.toLowerCase()) || '';
    }
  }

  // Descarga de app
  setDownLoadView(): void {
    this.parametersSubject = this.generalService.getParametersSubject();
    this.parametersSubject.subscribe(() => {
      this.lnkAppStore = this.generalService.getValueParams(KEYS.APPSTORE_APP_LINK);
      this.lnkPlayStore = this.generalService.getValueParams(KEYS.PLAYSTORE_APP_LINK);
      this.btnPlayStore = this.generalService.getValueParams(KEYS.PLAYSTORE_BOTON_HOVER);
      this.btnPlayStoreHover = this.generalService.getValueParams(KEYS.PLAYSTORE_BOTON);
      this.btnAppStore = this.generalService.getValueParams(KEYS.APPSTORE_BOTON_HOVER);
      this.btnAppStoreHover = this.generalService.getValueParams(KEYS.APPSTORE_BOTON);
    });
  }
}
