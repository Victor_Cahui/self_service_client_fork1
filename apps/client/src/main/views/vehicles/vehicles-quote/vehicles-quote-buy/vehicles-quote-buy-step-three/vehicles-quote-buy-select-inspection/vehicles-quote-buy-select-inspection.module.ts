import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule, MfButtonModule, MfCheckboxModule, MfModalModule } from '@mx/core/ui';
import { VehiclesQuoteBuySelectInspectionComponent } from './vehicles-quote-buy-select-inspection.component';

@NgModule({
  imports: [
    CommonModule,
    DirectivesModule,
    FormsModule,
    GoogleModule,
    MfButtonModule,
    MfCheckboxModule,
    MfModalModule,
    ReactiveFormsModule
  ],
  exports: [VehiclesQuoteBuySelectInspectionComponent],
  declarations: [VehiclesQuoteBuySelectInspectionComponent]
})
export class VehiclesQuoteBuySelectInspectionModule {}
