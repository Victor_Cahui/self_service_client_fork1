import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalErrorBuyStepOneModule } from '@mx/components/vehicles/modal-error-buy-step-one/modal-error-buy-step-one.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { MfButtonModule, MfInputModule, MfModalAlertModule, MfSelectModule, MfShowErrorsModule } from '@mx/core/ui';
import { TooltipsModule } from '@mx/core/ui/lib/components/tooltips/tooltips.module';
import { VehiclesQuoteBuyStepOneComponent } from './vehicles-quote-buy-step-one.component';
import { VehiclesQuoteBuyStepOneRoutingModule } from './vehicles-quote-buy-step-one.routing';

@NgModule({
  imports: [
    CommonModule,
    GoogleModule,
    MfButtonModule,
    MfInputModule,
    MfModalAlertModule,
    MfSelectModule,
    MfShowErrorsModule,
    ModalErrorBuyStepOneModule,
    ReactiveFormsModule,
    TooltipsModule,
    VehiclesQuoteBuyStepOneRoutingModule
  ],
  declarations: [VehiclesQuoteBuyStepOneComponent]
})
export class VehiclesQuoteBuyStepOneModule {}
