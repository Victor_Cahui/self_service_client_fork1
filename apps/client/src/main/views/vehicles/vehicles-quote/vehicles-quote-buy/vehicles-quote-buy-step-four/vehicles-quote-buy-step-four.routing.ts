import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VehiclesQuoteBuyStepFourComponent } from './vehicles-quote-buy-step-four.component';

const routes: Routes = [
  {
    path: '',
    component: VehiclesQuoteBuyStepFourComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiclesQuoteBuyStepFourRoutingModule {}
