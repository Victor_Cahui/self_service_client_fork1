import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { PaymentMethodComponent } from '@mx/components';
import { gaEventDecorator, GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { MfModal } from '@mx/core/ui';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { PaymentService } from '@mx/services/payment.service';
import { PoliciesQuoteService } from '@mx/services/policy/policy-quote.service';
import { VehicleService } from '@mx/services/vehicle.service';
import { MFP_Compra_Tu_Seguro_Paso_4_19B, MFP_Forma_De_Pago_19A } from '@mx/settings/constants/events.analytics';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { VEHICLES_QUOTE_BUY_HEADER } from '@mx/settings/constants/router-titles';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { MethodPaymentLang } from '@mx/settings/lang/payment.lang';
import { VehicleQuoteBuyLang } from '@mx/settings/lang/vehicles.lang';
import { IClientInformationResponse } from '@mx/statemanagement/models/client.models';
import { GeneralRequest } from '@mx/statemanagement/models/general.models';
import { IPaymentMethod, IPaymentQuoteCard } from '@mx/statemanagement/models/payment.models';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import {
  IClientVehicleDetail,
  IEmitPaymentBody,
  IInspectionRequestBody
} from '@mx/statemanagement/models/vehicle.interface';
import { Subscription, zip } from 'rxjs';

@Component({
  selector: 'client-vehicles-quote-buy-step-four',
  templateUrl: './vehicles-quote-buy-step-four.component.html'
})
export class VehiclesQuoteBuyStepFourComponent extends GaUnsubscribeBase implements OnInit, OnDestroy {
  @ViewChild(PaymentMethodComponent) paymentMethod: PaymentMethodComponent;
  @ViewChild(MfModal) modal: MfModal;

  header = VEHICLES_QUOTE_BUY_HEADER;
  generalLang = GeneralLang;
  paymentLang = MethodPaymentLang;
  lang = VehicleQuoteBuyLang;

  isAcceptContidion: boolean;
  doingPayment: boolean;
  urlConditions: string;
  indication: string;
  conditionsLabel: string;
  conditionsExtraLabel: string;
  quote: IPaymentQuoteCard;
  auth: IdDocument;
  client: IClientInformationResponse;

  linkDataSub: Subscription;
  emitSub: Subscription;
  inspectionSub: Subscription;
  updateQuotaSub: Subscription;

  constructor(
    private readonly quoteService: PoliciesQuoteService,
    private readonly vehicleService: VehicleService,
    private readonly paymentService: PaymentService,
    private readonly authService: AuthService,
    private readonly clientService: ClientService,
    private readonly router: Router,
    protected gaService: GAService
  ) {
    super(gaService);
    this.ga = [MFP_Forma_De_Pago_19A(), MFP_Compra_Tu_Seguro_Paso_4_19B()];
    this.quoteService.setCurrentStep(4);
    // Actualiza Costo en Boton
    this.updateQuotaSub = this.paymentService.updateQuota().subscribe(value => {
      setTimeout(() => (this.quote.quota = value), 50);
    });
  }

  ngOnInit(): void {
    this.auth = this.authService.retrieveEntity();
    this.client = this.clientService.getUserData();
    this.getModalsData();
  }

  showLoading(): boolean {
    this.quote = this.paymentService.getCurrentQuote();

    return (this.quote && !this.quote.dataQuoteVehicle) || this.doingPayment;
  }

  @gaEventDecorator()
  gotoBack(): void {
    if (this.quoteService.requiresInspection()) {
      this.router.navigate(['/vehicles/quote/buy/step3']);
    } else {
      this.router.navigate(['/vehicles/quote/buy/step2']);
    }
  }

  contionChange(event: any): void {
    this.isAcceptContidion = event.target.checked;
  }

  openModal(): void {
    this.modal.open();
  }

  getModalsData(): void {
    const quote = this.paymentService.getCurrentQuote();

    const contidion$ = this.vehicleService.getUrlConditions({
      codigoModalidad: quote.modalitySelected.modality,
      codigoApp: APPLICATION_CODE
    });
    const indication$ = this.vehicleService.getIndication({ codigoApp: APPLICATION_CODE });
    this.linkDataSub = zip(contidion$, indication$).subscribe(
      res => {
        this.urlConditions = res[0].detalleCondicionesProducto || '';
        this.indication = res[1].descripcionPolizaElectronica || '';
        this.linkDataSub.unsubscribe();

        const copys = this.lang.Step4;
        this.conditionsLabel = `
          ${copys.Condition.start}
          <a href="${this.urlConditions}" target="_blank" class="g-c--green1--hover g-font--medium">${copys.Condition.end}</a>
          ${copys.SendEmail.start}`;
        this.conditionsExtraLabel = `
          <span class="g-c--green1 g-font--medium">${copys.SendEmail.end}</span>`;
      },
      () => {
        this.linkDataSub.unsubscribe();
      }
    );
  }

  verifyInfo(): void {
    // Todo: manejar excepciones/escenarios cuando falle el pago
    this.doingPayment = true;
    this.quoteService.doingPaymentSubject.next(this.doingPayment);

    const quote = this.paymentService.getCurrentQuote();
    const vehicle = this.quoteService.getVehicleData();

    let paymentData: IPaymentMethod = { paymentMode: this.paymentMethod.paymentMode.BANK };

    if (this.paymentMethod.currentPaymentMode === this.paymentMethod.paymentMode.CARD) {
      paymentData = this.paymentMethod.emitCard();
    }

    const body = this.formatBodyEmit(quote, vehicle, paymentData);
    this.emitPayment(body, quote);
  }

  emitPayment(body: IEmitPaymentBody, quote: IPaymentQuoteCard): void {
    const params: GeneralRequest = {
      codigoApp: APPLICATION_CODE
    };

    this.emitSub = this.vehicleService.emitPayment(params, body).subscribe(
      res => {
        quote.emitResponse = res;
        quote.billNumber = res.numeroRecibo;
        quote.paymentMode = body.pago.tipoPago;
        quote.document = this.auth.number;
        quote.documentType = this.auth.type;
        quote.email = body.tarjeta ? body.tarjeta.correoElectronico : '';

        if (!quote.inspection) {
          quote.confirm = true;
          this.paymentService.setCurrentQuote(quote);
          this.goToFinish();
        } else {
          this.emitInspection(quote);
        }

        this.emitSub.unsubscribe();
      },
      err => {
        this.emitSub.unsubscribe();
        this.doingPayment = false;
        this.quoteService.doingPaymentSubject.next(this.doingPayment);
        console.error(err);
      },
      () => {
        this.emitSub.unsubscribe();
      }
    );
  }

  emitInspection(quote: IPaymentQuoteCard): void {
    const vehicle = this.quoteService.getVehicleData();

    const body = this.formatBodyInspection(quote, vehicle);
    this.inspectionSub = this.vehicleService.inspectionRequest({ codigoApp: APPLICATION_CODE }, body).subscribe(
      res => {
        quote.inspectionResponse = res;
        quote.confirm = true;
        this.paymentService.setCurrentQuote(quote);

        this.goToFinish();
        this.inspectionSub.unsubscribe();
      },
      () => {
        this.inspectionSub.unsubscribe();
      }
    );
  }

  goToFinish(): void {
    this.router.navigate(['/vehicles/quote/payment-confirm']);
  }

  formatBodyEmit(
    quote: IPaymentQuoteCard,
    vehicle: IClientVehicleDetail,
    paymentData: IPaymentMethod
  ): IEmitPaymentBody {
    const body: IEmitPaymentBody = {
      poliza: {
        fechaInicioVigencia: quote.startDate,
        fechaFinVigencia: quote.endDate,
        aprobacionPolizaElectronica: true,
        codigoCuota: quote.modalitySelected.quotas,
        codigoFraccionamiento: quote.modalitySelected.fractionationCode
      },
      producto: {
        codigoCompania: quote.dataQuoteVehicle.codigoCompania,
        codigoRamo: quote.dataQuoteVehicle.codigoRamo,
        codigoModalidad: quote.dataQuoteVehicle.codigoModalidad
      },
      vehiculo: {
        numeroPlaca: vehicle.placa,
        numeroMotor: vehicle.numeroMotor,
        numeroChasis: vehicle.numeroChasis,
        esCeroKm: vehicle.es0Km,
        anioFabricacion: vehicle.anio,
        tipoVehiculo: vehicle.codigoTipoVehiculo,
        codigoMarca: vehicle.codigoMarca,
        codigoModelo: vehicle.codigoModelo,
        tipoUsoVehiculo: vehicle.codigoUso,
        sumaAsegurada: vehicle.valorSugerido,
        codigoColor: vehicle.codigoColor
      },
      pago: {
        tipoPago: paymentData.paymentMode
      }
    };

    const cot = quote.withMPD ? quote.dataQuoteVehicle.cotizacionConMPD : quote.dataQuoteVehicle.cotizacionSinMPD;

    body.poliza.usoMapfreDolares = quote.withMPD;
    body.poliza.montoMapfreDolaresUsado = cot.montoMapfreDolaresUsado;
    body.poliza.montoMapfreDolaresRestantes = cot.montoMapfreDolaresRestantes;
    body.poliza.codigoMoneda = cot.codigoMoneda;
    body.poliza.numeroCotizacion = cot.numeroCotizacion;

    body.pago.monto = cot.montoCuota;
    body.pago.tipoMoneda = cot.codigoMoneda;

    if (this.paymentMethod.currentPaymentMode === this.paymentMethod.paymentMode.CARD) {
      body.tarjeta = {
        numero: paymentData.cardNumber,
        nombre: paymentData.ownerName,
        cvv: paymentData.cvv,
        correoElectronico: paymentData.email,
        mesVencimiento: paymentData.cardExpiredMonth,
        anioVencimiento: paymentData.cardExpiredYear,
        tipo: paymentData.cardType
      };
    }

    return body;
  }

  formatBodyInspection(quote: IPaymentQuoteCard, vehicle: IClientVehicleDetail): IInspectionRequestBody {
    const body: IInspectionRequestBody = {
      datosContratante: {
        apellidoMaterno: this.client.apellidoMaternoCliente,
        apellidoPaterno: this.client.apellidoPaternoCliente,
        codigoDepartamento: quote.contractor.departmentId,
        codigoDistrito: quote.contractor.districtId,
        codigoProvincia: quote.contractor.provinceId,
        correo: quote.contractor.email,
        direccion: quote.contractor.address,
        nombre: this.client.nombreCompletoCliente,
        referencia: quote.contractor.reference || '',
        telefonoContratante: quote.contractor.phone
      },
      datosVehiculo: {
        anio: vehicle.anio,
        cantidadAccesorios: vehicle.cantidadAccesorios,
        codigoMarca: vehicle.codigoMarca,
        codigoModelo: vehicle.codigoModelo,
        codigoMonedaValorSugerido: vehicle.codigoMonedaValorSugerido,
        codigoTipoVehiculo: vehicle.codigoTipoVehiculo,
        descripcionMarca: vehicle.marca,
        descripcionModelo: vehicle.modelo,
        descripcionTipoVehiculo: vehicle.tipoVehiculo,
        placa: vehicle.placa,
        valorSugerido: vehicle.valorSugerido
      },
      datosInspeccion: {
        esInspeccionPresencial: !quote.inspection.selfInspection
      }
    };

    if (!quote.inspection.selfInspection) {
      body.datosInspeccion = {
        codigoDepartamento: quote.inspection.data.departmentId,
        codigoDistrito: quote.inspection.data.districtId,
        codigoProvincia: quote.inspection.data.provinceId,
        direccion: quote.inspection.data.address,
        esDireccionCorrespondencia: quote.inspection.data.useDefault,
        esInspeccionPresencial: !quote.inspection.selfInspection,
        fechaInspeccionPresencial: quote.inspection.data.date,
        horaInspeccionPresencial: quote.inspection.data.time,
        nombreContacto: quote.inspection.data.contactName,
        otraPersonaContacto: !!quote.inspection.data.contactName && !!quote.inspection.data.contactPhone,
        referencia: quote.inspection.data.reference,
        telefonoContacto: quote.inspection.data.contactPhone
      };
    }

    return body;
  }

  ngOnDestroy(): void {
    if (this.linkDataSub) {
      this.linkDataSub.unsubscribe();
    }
    if (this.linkDataSub) {
      this.linkDataSub.unsubscribe();
    }
    if (this.emitSub) {
      this.emitSub.unsubscribe();
    }
    if (this.inspectionSub) {
      this.inspectionSub.unsubscribe();
    }
    if (this.updateQuotaSub) {
      this.updateQuotaSub.unsubscribe();
    }
  }

  getBtnTxt(): string {
    if (this.paymentMethod.currentPaymentMode === this.paymentMethod.paymentMode.CARD) {
      return `${this.generalLang.Buttons.Payment} ${this.quote.quota}`;
    }

    if (this.paymentMethod.currentPaymentMode === this.paymentMethod.paymentMode.BANK) {
      return this.paymentLang.PaymentOrder;
    }
  }

  getTabTxt(): string {
    return this.paymentMethod.currentPaymentMode.replace('PAGO_', '');
  }
}
