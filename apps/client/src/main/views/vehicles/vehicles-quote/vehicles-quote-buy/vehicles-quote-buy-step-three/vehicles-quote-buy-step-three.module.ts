import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { VehiclesQuoteBuyOnsiteInspectionModule } from './vehicles-quote-buy-onsite-inspection/vehicles-quote-buy-onsite-inspection.module';
import { VehiclesQuoteBuySelectInspectionModule } from './vehicles-quote-buy-select-inspection/vehicles-quote-buy-select-inspection.module';
import { VehiclesQuoteBuySelfInspectionModule } from './vehicles-quote-buy-self-inspection/vehicles-quote-buy-self-inspection.module';
import { VehiclesQuoteBuyStepThreeComponent } from './vehicles-quote-buy-step-three.component';
import { VehiclesQuoteBuyStepThreeRoutingModule } from './vehicles-quote-buy-step-three.routing';

@NgModule({
  imports: [
    CommonModule,
    VehiclesQuoteBuyStepThreeRoutingModule,
    VehiclesQuoteBuySelectInspectionModule,
    VehiclesQuoteBuySelfInspectionModule,
    VehiclesQuoteBuyOnsiteInspectionModule
  ],
  declarations: [VehiclesQuoteBuyStepThreeComponent]
})
export class VehiclesQuoteBuyStepThreeModule {}
