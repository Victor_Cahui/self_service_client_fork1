import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FormComponent } from '@mx/components/shared/utils/form';
import { IsHoliday, IsWeekend } from '@mx/core/shared/helpers/util/date';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { SelectListItem } from '@mx/core/ui';
import { PaymentService } from '@mx/services/payment.service';
import { VehicleService } from '@mx/services/vehicle.service';
import { MFP_Compra_Tu_Seguro_Paso_3_17A } from '@mx/settings/constants/events.analytics';
import {
  APPLICATION_CODE,
  GENERAL_MAX_LENGTH,
  GENERAL_MIN_LENGTH,
  MAX_NUMBER_DAYS_TO_INSPECTION
} from '@mx/settings/constants/general-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { VehicleQuoteBuyLang } from '@mx/settings/lang/vehicles.lang';
import { IListHoursResponse } from '@mx/statemanagement/models/home.interface';
import { IPaymentQuoteCard } from '@mx/statemanagement/models/payment.models';
import { IInspectionData } from '@mx/statemanagement/models/vehicle.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-vehicles-quote-buy-onsite-inspection',
  templateUrl: './vehicles-quote-buy-onsite-inspection.component.html'
})
export class VehiclesQuoteBuyOnsiteInspectionComponent implements OnInit, OnDestroy {
  @Output() goBack: EventEmitter<number> = new EventEmitter<number>();

  generalLang = GeneralLang;
  lang = VehicleQuoteBuyLang;
  generalMaxLength = GENERAL_MAX_LENGTH;
  generalMinLength = GENERAL_MIN_LENGTH;

  form: FormGroup;

  rangeHours: Array<SelectListItem>;

  showLoading: boolean;
  isAnotherPerson: boolean;

  minDays = new Date();
  maxDays = new Date();

  rangeHoursSub: Subscription;

  quote: IPaymentQuoteCard;
  inspectionData: IInspectionData;

  gaBack: IGaPropertie;
  gaNext: IGaPropertie;

  constructor(
    private readonly fb: FormBuilder,
    private readonly vehicleService: VehicleService,
    private readonly paymentService: PaymentService,
    private readonly router: Router
  ) {
    this.gaBack = MFP_Compra_Tu_Seguro_Paso_3_17A(this.generalLang.Buttons.Back);
    this.gaNext = MFP_Compra_Tu_Seguro_Paso_3_17A(this.generalLang.Buttons.Next);
  }

  ngOnInit(): void {
    this.quote = this.paymentService.getCurrentQuote();
    this.inspectionData = this.quote && this.quote.inspection ? this.quote.inspection.data : null;
    this.createForm();
    this.setMinMaxDates();
    this.getRangeHoursInspection();
  }

  customFilter(date: Date): boolean {
    return !IsWeekend(date) && !IsHoliday(date);
  }

  setMinMaxDates(): void {
    this.minDays.setDate(this.minDays.getDate());
    this.minDays.setHours(0, 0, 0, 0);

    let i = 0;
    while (i < 1) {
      this.minDays.setDate(this.minDays.getDate() + 1);
      if (this.customFilter(this.minDays)) {
        i++;
      }
    }

    let j = 0;
    while (j < MAX_NUMBER_DAYS_TO_INSPECTION) {
      this.maxDays.setDate(this.maxDays.getDate() + 1);
      this.maxDays.setHours(0, 0, 0, 0);
      if (this.customFilter(this.maxDays)) {
        j++;
      }
    }

    this.maxDays.setHours(23, 59, 59);
  }

  createForm(): void {
    this.form = this.fb.group({
      date: ['', Validators.required],
      hour: ['', Validators.required],
      anotherPerson: [''],
      anotherName: [''],
      anotherPhone: ['']
    });
    setTimeout(() => {
      this.getStorageData();
    });
  }

  getStorageData(): void {
    if (this.inspectionData) {
      this.form.controls['date'].setValue(this.inspectionData.date);
      this.form.controls['hour'].setValue(this.inspectionData.time);
      this.isAnotherPerson = this.inspectionData.anotherPerson;
      this.form.controls['anotherName'].setValue(this.inspectionData.contactName);
      this.form.controls['anotherPhone'].setValue(this.inspectionData.contactPhone);
    }
  }

  changeAnotherPerson(value: any): void {
    this.isAnotherPerson = value.target.checked;
    if (this.isAnotherPerson) {
      this.form.controls['anotherName'].setValidators(Validators.required);
      this.form.controls['anotherPhone'].setValidators(
        FormComponent.phoneValidators(
          this.generalMaxLength.correspondence_another_phone,
          this.generalMinLength.correspondence_another_phone
        )
      );
    } else {
      this.form.controls['anotherName'].clearValidators();
      this.form.controls['anotherPhone'].clearValidators();
    }
    this.form.controls['anotherName'].updateValueAndValidity();
    this.form.controls['anotherPhone'].updateValueAndValidity();
  }

  onHourInspectionChange(value: any): void {}

  getRangeHoursInspection(): void {
    const params = { codigoApp: APPLICATION_CODE };
    this.rangeHoursSub = this.vehicleService.getRangeHoursInspection(params).subscribe(
      (rangeHours: Array<IListHoursResponse>) => {
        if (rangeHours) {
          this.rangeHours = rangeHours.map(
            item => new SelectListItem({ text: item.descripcionRango, value: item.identificadorRango })
          );
          this.rangeHoursSub.unsubscribe();
        }
      },
      () => {
        this.rangeHoursSub.unsubscribe();
      }
    );
  }

  ngOnDestroy(): void {
    if (this.rangeHoursSub) {
      this.rangeHoursSub.unsubscribe();
    }
  }

  back(): void {
    this.quote.inspection && delete this.quote.inspection.previousStep;
    this.paymentService.setCurrentQuote(this.quote);
    this.goBack.emit();
  }

  removeSpaces(controlName: string): void {
    FormComponent.removeSpaces(this.form.controls[controlName]);
  }

  showErrors(controlName: string): boolean {
    return FormComponent.showBasicErrors(this.form, controlName);
  }

  next(): void {
    // Guardar Datos en Storage
    const address = this.form.getRawValue().address;
    const uca = address.useCorrespondenceAddress;
    this.quote.inspection = {
      selfInspection: false,
      conditionsAgreement: true,
      previousStep: this.lang.StepSelectInspection.Options.onsiteInspection.Id,
      data: {
        date: this.form.controls['date'].value,
        time: this.form.controls['hour'].value,
        anotherPerson: this.isAnotherPerson,
        contactName: this.isAnotherPerson ? this.form.controls['anotherName'].value : '',
        contactPhone: this.isAnotherPerson ? this.form.controls['anotherPhone'].value : '',
        departmentId: uca ? address.correspondenceAddress.identificadorDepartamento : parseInt(address.departments, 10),
        department: uca ? address.correspondenceAddress.departamento : address.departmentName || '',
        provinceId: uca ? address.correspondenceAddress.identificadorProvincia : parseInt(address.provinces, 10),
        province: uca ? address.correspondenceAddress.provincia : address.provinceName || '',
        districtId: uca ? address.correspondenceAddress.identificadorDistrito : parseInt(address.districts, 10),
        district: uca ? address.correspondenceAddress.distrito : address.districtName || '',
        address: uca ? address.correspondenceAddress.direccion : address.address,
        useDefault: uca,
        reference: address.reference || ''
      }
    };
    this.paymentService.setCurrentQuote(this.quote);
    // Pagar
    this.router.navigate(['/vehicles/quote/buy/step4']);
  }
}
