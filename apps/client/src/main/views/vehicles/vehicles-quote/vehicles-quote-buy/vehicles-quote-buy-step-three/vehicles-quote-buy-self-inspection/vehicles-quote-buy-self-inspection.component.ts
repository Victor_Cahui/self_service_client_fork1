import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { PaymentService } from '@mx/services/payment.service';
import { MFP_Compra_Tu_Seguro_Paso_3_18A } from '@mx/settings/constants/events.analytics';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { VehicleQuoteBuyLang } from '@mx/settings/lang/vehicles.lang';
import { IPaymentQuoteCard } from '@mx/statemanagement/models/payment.models';
import { IInspection } from '@mx/statemanagement/models/vehicle.interface';

@Component({
  selector: 'client-vehicles-quote-buy-self-inspection',
  templateUrl: './vehicles-quote-buy-self-inspection.component.html'
})
export class VehiclesQuoteBuySelfInspectionComponent implements OnInit {
  @Output() goBack: EventEmitter<number> = new EventEmitter<number>();

  lang = VehicleQuoteBuyLang;
  generalLang = GeneralLang;
  quote: IPaymentQuoteCard;
  inspection: IInspection;

  gaBack: IGaPropertie;
  gaNext: IGaPropertie;

  constructor(private readonly paymentService: PaymentService, private readonly router: Router) {
    this.gaBack = MFP_Compra_Tu_Seguro_Paso_3_18A(this.generalLang.Buttons.Back);
    this.gaNext = MFP_Compra_Tu_Seguro_Paso_3_18A(this.generalLang.Buttons.Next);
  }

  ngOnInit(): void {
    this.quote = this.paymentService.getCurrentQuote();
  }

  // Volver a seleccion o a paso 2
  back(): void {
    delete this.quote.inspection.previousStep;
    this.paymentService.setCurrentQuote(this.quote);
    this.goBack.emit();
  }

  // Pagar
  next(): void {
    // Guardar Datos en Storage
    this.quote.inspection = {
      selfInspection: true,
      conditionsAgreement: this.quote.inspection.conditionsAgreement,
      previousStep: this.lang.StepSelectInspection.Options.selfInspection.Id
    };
    this.paymentService.setCurrentQuote(this.quote);
    // Pagar
    this.router.navigate(['/vehicles/quote/buy/step4']);
  }
}
