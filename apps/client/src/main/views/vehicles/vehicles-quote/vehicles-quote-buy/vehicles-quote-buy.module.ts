import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormBaseModule } from '@mx/components/shared/form-base/form-base.module';
import { QuoteBuyDetailModule } from '@mx/components/vehicles/quote-buy-detail/quote-buy-detail.module';
import { StepperModule } from '@mx/core/ui/lib/components/stepper/stepper.module';
import { VehiclesQuoteBuyComponent } from './vehicles-quote-buy.component';
import { VehiclesQuoteBuyRoutingModule } from './vehicles-quote-buy.routing';

@NgModule({
  imports: [CommonModule, FormBaseModule, QuoteBuyDetailModule, StepperModule, VehiclesQuoteBuyRoutingModule],
  declarations: [VehiclesQuoteBuyComponent]
})
export class VehiclesQuoteBuyModule {}
