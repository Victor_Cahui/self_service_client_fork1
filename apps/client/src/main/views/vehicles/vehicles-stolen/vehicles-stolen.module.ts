import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfTabTopModule } from '@mx/core/ui/lib/components/tabs/tabs-top';
import { VehiclesStolenComponent } from './vehicles-stolen.component';
import { VehiclesStolenRoutingModule } from './vehicles-stolen.routing';

@NgModule({
  imports: [CommonModule, VehiclesStolenRoutingModule, MfTabTopModule],
  exports: [],
  declarations: [VehiclesStolenComponent],
  providers: []
})
export class VehiclesStolenModule {}
