import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { VehiclesStolenComponent } from '@mx/views/vehicles/vehicles-stolen/vehicles-stolen.component';

const routes: Routes = [
  {
    path: '',
    component: VehiclesStolenComponent,
    children: [
      {
        path: '',
        redirectTo: 'assists',
        pathMatch: 'prefix'
      },
      {
        path: 'assists',
        loadChildren: './vehicle-stolen-assists/vehicle-stolen-assists.module#VehicleStolenAssistsModule'
      },
      {
        path: 'reports',
        loadChildren: './vehicle-stolen-report/vehicle-stolen-report.module#VehicleStolenReportModule'
      },
      {
        path: 'records',
        loadChildren: '../vehicles-records/vehicles-records.module#VehiclesRecordsModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiclesStolenRoutingModule {}
