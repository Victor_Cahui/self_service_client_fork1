import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VehicleStolenAssistsDetailComponent } from './vehicle-stolen-assists-detail.component';

const routes: Routes = [
  {
    path: '',
    component: VehicleStolenAssistsDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehicleStolenAssistsDetailRouting {}
