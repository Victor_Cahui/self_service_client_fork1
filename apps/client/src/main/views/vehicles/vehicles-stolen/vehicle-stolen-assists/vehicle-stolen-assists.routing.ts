import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VehicleStolenAssistsComponent } from './vehicle-stolen-assists.component';

const routes: Routes = [
  {
    path: '',
    component: VehicleStolenAssistsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehicleStolenAssistsRouting {}
