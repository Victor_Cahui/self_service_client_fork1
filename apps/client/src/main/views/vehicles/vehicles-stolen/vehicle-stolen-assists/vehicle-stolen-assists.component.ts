import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BaseCardAssistanceVehicle } from '@mx/components/vehicles/card-assistance-base/card-assistance-base.component';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { GeneralService } from '@mx/services/general/general.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { VehicleService } from '@mx/services/vehicle.service';
import { VEHICLES_STOLEN_HEADER } from '@mx/settings/constants/router-titles';

@Component({
  selector: 'client-vehicle-stolen-assists',
  templateUrl: './vehicle-stolen-assists.component.html'
})
export class VehicleStolenAssistsComponent extends BaseCardAssistanceVehicle implements OnInit {
  constructor(
    private readonly headerHelperService: HeaderHelperService,
    private readonly vehicleService: VehicleService,
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService,
    protected gaService: GAService,
    protected _Router: Router,
    protected generalService: GeneralService,
    public router: Router
  ) {
    super(clientService, policiesInfoService, configurationService, authService, gaService, generalService, _Router);
    this.headerHelperService.set(VEHICLES_STOLEN_HEADER);
    this.configUI = this.getRouteRecords();
  }

  getItems(params): void {
    this.showLoading = true;
    this.vehicleSub = this.vehicleService.getStolens(params).subscribe(
      response => {
        this.itemsHistory = this.sortHistory(response && (response.historial || []), 'registerDateObj', 'DESC');
        this.itemsProcess = this.formatProcess(response && (response.enProceso || []));
        this.showLoading = false;
      },
      () => {
        this.itemsProcess = [];
        this.showLoading = false;
      }
    );
  }

  viewDetail(item): void {
    this.router.navigate([`/vehicles/stolen/assists/detail/${item.assistanceNumber}`]);
  }
}
