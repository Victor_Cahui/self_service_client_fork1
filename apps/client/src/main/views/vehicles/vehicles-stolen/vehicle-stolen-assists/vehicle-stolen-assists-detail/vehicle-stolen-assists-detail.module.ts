import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ViewTimelineModule } from '@mx/components/shared/timeline/view-timeline/view-timeline.module';
import { ModalAttachLetterModule } from '@mx/components/vehicles/modal-attach-letter/modal-attach-letter.module';
import { VehicleStolenAssistsDetailComponent } from './vehicle-stolen-assists-detail.component';
import { VehicleStolenAssistsDetailRouting } from './vehicle-stolen-assists-detail.routing';

@NgModule({
  imports: [CommonModule, ViewTimelineModule, VehicleStolenAssistsDetailRouting, ModalAttachLetterModule],
  declarations: [VehicleStolenAssistsDetailComponent]
})
export class VehicleStolenAssistsDetailModule {}
