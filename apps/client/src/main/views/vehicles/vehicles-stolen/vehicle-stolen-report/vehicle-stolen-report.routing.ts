import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VehicleStolenReportComponent } from './vehicle-stolen-report.component';

const routes: Routes = [
  {
    path: '',
    component: VehicleStolenReportComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehicleStolenReportRouting {}
