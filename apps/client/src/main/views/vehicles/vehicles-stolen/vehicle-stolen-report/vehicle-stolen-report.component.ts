import { Component, OnInit } from '@angular/core';
import { ConfigurationBase } from '@mx/components/shared/configuration/configuration-base';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { MFP_SECTION_24A } from '@mx/settings/constants/events.analytics';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { VEHICLES_STOLEN_HEADER } from '@mx/settings/constants/router-titles';
import { LANDING_VEHICLE_STOLEN } from '@mx/settings/lang/landing';
import { VehiclesLang } from '@mx/settings/lang/vehicles.lang';

@Component({
  selector: 'client-vehicle-stolen-report',
  templateUrl: './vehicle-stolen-report.component.html'
})
export class VehicleStolenReportComponent extends ConfigurationBase implements OnInit {
  titleVehiclesStolen = VehiclesLang.Titles.VehiclesStolen;
  messageVehiclesStolen = VehiclesLang.Messages.VehiclesStolen;
  textVehiclesStolenHelp = VehiclesLang.Messages.VehiclesStolenHelp;
  landingContent = LANDING_VEHICLE_STOLEN;

  ga: Array<IGaPropertie>;

  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    private readonly headerHelperService: HeaderHelperService,
    protected authService: AuthService
  ) {
    super(clientService, policiesInfoService, configurationService, authService);
    this.ga = [MFP_SECTION_24A()];
  }

  ngOnInit(): void {
    this.policyType = POLICY_TYPES.MD_AUTOS.code;
    this.loadConfiguration();
    this.headerHelperService.set(VEHICLES_STOLEN_HEADER);
  }
}
