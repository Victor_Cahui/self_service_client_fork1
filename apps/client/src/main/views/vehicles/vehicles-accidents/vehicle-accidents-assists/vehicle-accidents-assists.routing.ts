import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VehicleAccidentsAssistsComponent } from './vehicle-accidents-assists.component';

const routes: Routes = [
  {
    path: '',
    component: VehicleAccidentsAssistsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehicleAccidentsAssistsRouting {}
