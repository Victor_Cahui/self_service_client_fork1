import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VehicleAccidentsAssistsDetailComponent } from './vehicle-accidents-assists-detail.component';

const routes: Routes = [
  {
    path: '',
    component: VehicleAccidentsAssistsDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehicleAccidentsAssistsDetailRouting {}
