import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TimelineVehiclesBase } from '@mx/components/vehicles/utils/timeline-vehicles-base';
import { GeolocationService } from '@mx/core/shared/helpers/util/geolocation.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { VehicleService } from '@mx/services/vehicle.service';
import { VEHICLES_ACCIDENTS_HEADER } from '@mx/settings/constants/router-titles';

@Component({
  selector: 'client-vehicle-accidents-assists-detail',
  templateUrl: './vehicle-accidents-assists-detail.component.html',
  providers: [GeolocationService]
})
export class VehicleAccidentsAssistsDetailComponent extends TimelineVehiclesBase implements OnInit {
  constructor(
    protected headerHelperService: HeaderHelperService,
    protected activePath: ActivatedRoute,
    protected vehicleService: VehicleService,
    protected geolocationService: GeolocationService
  ) {
    super(headerHelperService, activePath, vehicleService, geolocationService);
  }

  ngOnInit(): void {
    this.titleHeader = VEHICLES_ACCIDENTS_HEADER.title;
    this.subtitleHeader = this.lang.AssistanceNumber.toUpperCase();
    this.setView();
    this.assistanceType$ = this.vehicleService.getAccidentDetail(this.getParams());
    this.titleCardDeclared = this.cards.AUTOS_ASIST_ESTADO_DECLARADO.title;
    this.getData();
  }
}
