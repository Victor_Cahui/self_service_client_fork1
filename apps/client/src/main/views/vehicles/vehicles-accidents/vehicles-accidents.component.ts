import { Component, OnDestroy, OnInit } from '@angular/core';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { MFP_Accidentes_25A } from '@mx/settings/constants/events.analytics';
import { VEHICLE_ACCIDENTS_TAB } from '@mx/settings/constants/router-tabs';
import { ITabItem } from '@mx/statemanagement/models/router.interface';

@Component({
  selector: 'client-accidents-component',
  templateUrl: './vehicles-accidents.component.html'
})
export class VehiclesAccidentsComponent implements OnDestroy, OnInit {
  ga: Array<IGaPropertie>;
  tabItemsList: Array<ITabItem>;

  constructor(private readonly headerHelperService: HeaderHelperService) {
    this.tabItemsList = VEHICLE_ACCIDENTS_TAB;
    this.ga = [MFP_Accidentes_25A()];
  }

  ngOnInit(): void {
    this.headerHelperService.setTab(true);
  }

  ngOnDestroy(): void {
    this.headerHelperService.setTab(false);
  }
}
