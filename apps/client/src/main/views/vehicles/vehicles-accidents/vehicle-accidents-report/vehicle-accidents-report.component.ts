import { Component, OnInit } from '@angular/core';
import { ConfigurationBase } from '@mx/components/shared/configuration/configuration-base';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { MFP_Accidentes_26A, MFP_Accidentes_26B, MFP_SECTION_24A } from '@mx/settings/constants/events.analytics';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { VEHICLES_ACCIDENTS_HEADER } from '@mx/settings/constants/router-titles';
import { LANDING_ACCIDENTS } from '@mx/settings/lang/landing';
import { VehiclesLang } from '@mx/settings/lang/vehicles.lang';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-vehicle-accidents-report',
  templateUrl: './vehicle-accidents-report.component.html'
})
export class VehicleAccidentsReportComponent extends ConfigurationBase implements OnInit {
  ga: Array<IGaPropertie>;
  titleVehiclesAccidents = VehiclesLang.Titles.VehiclesAccidents;
  messageVehiclesAccidents = VehiclesLang.Messages.VehiclesAccidents;
  textVehiclesAccidentsHelp = VehiclesLang.Messages.VehiclesAccidentsHelp;
  landingContent = LANDING_ACCIDENTS;
  clientInfoSub: Subscription;

  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService,
    private readonly headerHelperService: HeaderHelperService
  ) {
    super(clientService, policiesInfoService, configurationService, authService);
    this.ga = [MFP_SECTION_24A(), MFP_Accidentes_26A(), MFP_Accidentes_26B()];
  }

  ngOnInit(): void {
    this.policyType = POLICY_TYPES.MD_AUTOS.code;
    this.loadConfiguration();
    this.headerHelperService.set(VEHICLES_ACCIDENTS_HEADER);
  }
}
