import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VehicleAccidentsReportComponent } from './vehicle-accidents-report.component';

const routes: Routes = [
  {
    path: '',
    component: VehicleAccidentsReportComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehicleAccidentsReportRouting {}
