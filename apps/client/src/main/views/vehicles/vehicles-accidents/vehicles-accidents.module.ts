import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfTabTopModule } from '@mx/core/ui/lib/components/tabs/tabs-top';
import { VehiclesAccidentsComponent } from './vehicles-accidents.component';
import { VehiclesAccidentsRoutingModule } from './vehicles-accidents.routing';

@NgModule({
  imports: [CommonModule, VehiclesAccidentsRoutingModule, MfTabTopModule],
  exports: [],
  declarations: [VehiclesAccidentsComponent],
  providers: []
})
export class VehiclesAccidentsModule {}
