import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardMoreFaqModule } from '@mx/components/shared/card-more-faq/card-more-faq.module';
import { VehiclesInsuranceFAQComponent } from './vehicles-insurance-faq.component';
import { VehiclesInsuranceFAQRoutingModule } from './vehicles-insurance-faq.routing';

@NgModule({
  imports: [CommonModule, CardMoreFaqModule, VehiclesInsuranceFAQRoutingModule],
  declarations: [VehiclesInsuranceFAQComponent]
})
export class VehiclesInsuranceFAQModule {}
