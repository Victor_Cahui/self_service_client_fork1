import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSliderModule } from '@angular/material';
import { MfModalAlertModule, MfSelectModule } from '@mx/core/ui';
import { MfCustomAlertModule } from '@mx/core/ui/lib/components/alerts/custom-alert';
import { MfInputModule } from '@mx/core/ui/lib/components/forms/input';
import { MfLoaderModule } from '@mx/core/ui/lib/components/global/loader';
import { MfShowErrorsModule } from '@mx/core/ui/lib/components/global/show-errors';
import { MfModalModule } from '@mx/core/ui/lib/components/modals/modal';
import { MfModalMessageModule } from '@mx/core/ui/lib/components/modals/modal-message';
import { LinksModule } from '@mx/core/ui/lib/links/links.module';
import { AddNumberPlateComponent } from './add-number-plate/add-number-plate.component';
import { DetailVehicleComponent } from './detail-vehicle.component';
import { DetailVehicleRouting } from './detail-vehicle.routing';
import { ManagementVehicleAccesoryComponent } from './vehicle-accesories/management-vehicle-accesory/management-vehicle-accesory.component';
import { VehicleAccesoriesComponent } from './vehicle-accesories/vehicle-accesories.component';

@NgModule({
  imports: [
    CommonModule,
    DetailVehicleRouting,
    MfModalModule,
    MfInputModule,
    MfSelectModule,
    MfShowErrorsModule,
    MfSelectModule,
    MfShowErrorsModule,
    LinksModule,
    MfModalAlertModule,
    MfModalMessageModule,
    FormsModule,
    ReactiveFormsModule,
    MatSliderModule,
    MfLoaderModule,
    MfCustomAlertModule
  ],
  declarations: [
    DetailVehicleComponent,
    VehicleAccesoriesComponent,
    AddNumberPlateComponent,
    ManagementVehicleAccesoryComponent
  ],
  exports: [
    DetailVehicleComponent,
    VehicleAccesoriesComponent,
    AddNumberPlateComponent,
    ManagementVehicleAccesoryComponent
  ]
})
export class DetailVehicleModule {}
