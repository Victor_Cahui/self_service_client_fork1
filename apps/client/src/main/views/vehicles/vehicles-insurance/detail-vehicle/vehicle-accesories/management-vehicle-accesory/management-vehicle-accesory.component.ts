import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSlider, MatSliderChange } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { FormComponent } from '@mx/components/shared/utils/form';
import { SelectListItem } from '@mx/core/ui';
import { MfModal } from '@mx/core/ui/lib/components/modals/modal';
import { MfModalMessage } from '@mx/core/ui/lib/components/modals/modal-message';
import { WhiteSpaceValidator } from '@mx/core/ui/lib/validators/validators';
import { VehicleService } from '@mx/services/vehicle.service';
import { APPLICATION_CODE, GENERAL_MAX_LENGTH } from '@mx/settings/constants/general-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { VehiclesLang } from '@mx/settings/lang/vehicles.lang';
import {
  IVehicleAccesory,
  IVehicleAccesoryRequestPut,
  IVehicleAccesoryType
} from '@mx/statemanagement/models/vehicle.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-management-vehicle-accesory',
  templateUrl: './management-vehicle-accesory.component.html',
  styleUrls: ['./management-vehicle-accesory.component.scss'],
  encapsulation: ViewEncapsulation.None // tslint:disable-line use-view-encapsulation
})
export class ManagementVehicleAccesoryComponent implements OnInit, OnDestroy {
  @ViewChild(MfModal) mfModal: MfModal;
  @ViewChild(MfModalMessage) mfModalMessage: MfModalMessage;
  @ViewChild(MatSlider) matSlider: MatSlider;
  @Output() update: EventEmitter<IVehicleAccesory> = new EventEmitter<IVehicleAccesory>();
  @Output() add: EventEmitter<IVehicleAccesory> = new EventEmitter<IVehicleAccesory>();
  @Input() accesories: Array<IVehicleAccesory>;
  @Input() index: number;

  accesory?: IVehicleAccesory;
  accesoriesType: Array<IVehicleAccesoryType>;
  accesoriesTypeListItem: Array<SelectListItem>;
  accesorySelected?: IVehicleAccesoryType;

  formManageAccesory: FormGroup;

  accesory_type: AbstractControl;
  price_range: AbstractControl;
  model: AbstractControl;
  oldValue: string;
  newValue: string;

  routeSub: Subscription;
  accesoryTypeSub: Subscription;

  motorNumber: string;
  chassis: string;
  policyNumber: string;
  showLoading: boolean;

  doneUpdate = false;
  doneMessage = GeneralLang.Messages.Done;
  messageSuccessOrError = GeneralLang.Messages.SuccessUpdate;
  dataVehicleTitle = VehiclesLang.Titles.DataVehicle;
  generalMaxLength = GENERAL_MAX_LENGTH;
  labelMinValue = GeneralLang.Labels.MinValue;
  labelMaxValue = GeneralLang.Labels.MaxValue;
  labelPriceRange = GeneralLang.Labels.PriceRange;
  labelTypeAccesory = VehiclesLang.Labels.TypeAccesory;
  labelModelAccesory = VehiclesLang.Labels.ModelAccesory;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly vehicleAccesoryService: VehicleService,
    private readonly route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.createForm();
    this.routeSub = this.route.params.subscribe(params => {
      this.motorNumber = params['motorNumber'];
      this.chassis = params['chassis'];
      this.policyNumber = params['policyNumber'];
    });
  }

  initialiceForm(accesory?: IVehicleAccesory): void {
    this.accesory = accesory;
    this.createForm();
    if (!this.accesoriesType) {
      this.getAccesoriesType().then(() => {
        this.accesorySelected = this.setAccesoryTypeSelected();
      });
    } else {
      this.accesorySelected = this.setAccesoryTypeSelected();
    }
  }

  async getAccesoriesType(): Promise<void> {
    this.accesoriesType = await this.vehicleAccesoryService.getAccesoriesType().toPromise();
    this.accesoriesTypeListItem = this.accesoriesType.map(
      v =>
        new SelectListItem({
          text: v.tipoAccesorioDescripcion,
          value: v.tipoAccesorioId
        })
    );
  }

  private createForm(): void {
    this.formManageAccesory = this.formBuilder.group({
      accesory_type: [(this.accesory && this.accesory.tipoAccesorioId) || void 0, Validators.required],
      price_range: [(this.accesory && this.accesory.monto) || '', Validators.required],
      model: [
        this.accesory ? this.accesory.modelo : '',
        Validators.compose([
          Validators.required,
          WhiteSpaceValidator.isValid,
          Validators.maxLength(this.generalMaxLength.accesoryModel)
        ])
      ]
    });

    this.accesory_type = this.formManageAccesory.get('accesory_type');
    this.price_range = this.formManageAccesory.get('price_range');
    this.model = this.formManageAccesory.get('model');

    if (this.matSlider) {
      this.matSlider.value = +((this.accesory && this.accesory.monto) || '0');
      this.price_range.setValue(this.matSlider.value);
    }
  }

  selectAccesoryType(): void {
    this.accesorySelected = this.setAccesoryTypeSelected();
    if (this.accesorySelected) {
      this.matSlider.value = this.accesorySelected.minimo;
      this.price_range.setValue(this.matSlider.value);
    }
  }

  setAccesoryTypeSelected(): IVehicleAccesoryType | null {
    const accesoryId = +this.accesory_type.value;
    if (accesoryId && this.accesoriesType) {
      return this.accesoriesType.find(v => v.tipoAccesorioId === accesoryId);
    }

    return null;
  }

  open(): void {
    this.createForm();
    this.mfModal.open();
  }

  prepareRequest(): IVehicleAccesoryRequestPut {
    return {
      accesorioId: this.accesory ? `${this.accesory.accesorioId}` : undefined,
      chasis: this.chassis,
      nroMotor: this.motorNumber,
      numeroPoliza: this.policyNumber,
      codigoApp: APPLICATION_CODE
    };
  }

  prepareData(): IVehicleAccesory {
    return {
      tipoAccesorioId: +this.accesory_type.value,
      tipoAccesorioDescripcion: this.accesoriesType.find(v => v.tipoAccesorioId === +this.accesory_type.value)
        .tipoAccesorioDescripcion,
      modelo: this.model.value,
      monto: this.price_range.value
    };
  }

  async saveForm(confirm: boolean): Promise<void> {
    if (confirm && this.formManageAccesory.valid) {
      const requestData = this.prepareRequest();
      const formData = this.prepareData();
      const buffIndex = this.accesories.findIndex(
        (obj: IVehicleAccesory) => obj.tipoAccesorioId === formData.tipoAccesorioId
      );
      this.mfModalMessage.mfModalAlert.sizeClass = 'g-modal--medium';
      try {
        this.showLoading = true;
        if (this.accesory) {
          if (buffIndex === -1 || buffIndex === this.index) {
            const response = await this.vehicleAccesoryService.editAccesory(requestData, formData).toPromise();
            this.update.emit(response);
            this.messageSuccessOrError = GeneralLang.Messages.SuccessUpdate;
            this.doneUpdate = true;
            this.doneMessage = GeneralLang.Messages.Done;
            this.mfModal.close();
            this.mfModalMessage.open();
          } else {
            this.doneUpdate = false;
            this.doneMessage = GeneralLang.Messages.Fail;
            this.messageSuccessOrError = VehiclesLang.Messages.VehicleErrorEditOrCreate;
            this.mfModalMessage.open();
          }
        } else {
          if (buffIndex === -1) {
            delete requestData.accesorioId;
            const response = await this.vehicleAccesoryService.addAccesory(requestData, formData).toPromise();
            this.add.emit(response);
            this.messageSuccessOrError = GeneralLang.Messages.SuccessAdd;
            this.doneUpdate = true;
            this.doneMessage = GeneralLang.Messages.Done;
            this.mfModal.close();
            this.mfModalMessage.open();
          } else {
            this.doneUpdate = false;
            this.doneMessage = GeneralLang.Messages.Fail;
            this.messageSuccessOrError = VehiclesLang.Messages.VehicleErrorEditOrCreate;
            this.mfModalMessage.open();
          }
        }
        this.showLoading = false;
      } catch (error) {
        this.showLoading = false;
        this.doneUpdate = false;
      }
    }
  }

  destroySubscriptions(): void {
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
    if (this.accesoryTypeSub) {
      this.accesoryTypeSub.unsubscribe();
    }
  }

  setRangePriceValue(slider: MatSliderChange): void {
    this.price_range.setValue(slider.value);
  }

  ngOnDestroy(): void {
    this.destroySubscriptions();
  }

  getModalTitle(): string {
    return this.accesory ? VehiclesLang.Titles.EditAccesory : VehiclesLang.Titles.AddAccesory;
  }

  formatLabel(value: number | null): string {
    const amount = value;
    let decimalCount = 2;
    const decimal = '.';
    const thousands = ',';

    decimalCount = Math.abs(decimalCount);
    decimalCount = isNaN(decimalCount) ? 2 : decimalCount;
    const negativeSign = amount < 0 ? '-' : '';
    const i: string = parseInt(Math.abs(amount).toFixed(decimalCount), null).toString();
    const j: number = i.length > 3 ? i.length % 3 : 0;

    return `$${negativeSign}${j ? i.substr(0, j) + thousands : ''}${i
      .substr(j)
      .replace(/(\d{3})(?=\d)/g, `$1${thousands}`)}${
      decimalCount
        ? decimal +
          Math.abs(amount - parseInt(i, null))
            .toFixed(decimalCount)
            .slice(2)
        : ''
    }

      `;
  }

  showErrors(control: string): boolean {
    return FormComponent.showBasicErrors(this.formManageAccesory, control);
  }
}
