import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MfModalMessage } from '@mx/core/ui';
import { MfModalAlertComponent } from '@mx/core/ui/lib/components/modals/modal-alert/modal-alert.component';
import { VehicleService } from '@mx/services/vehicle.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { ADD_ICON } from '@mx/settings/constants/images-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { VehiclesLang } from '@mx/settings/lang/vehicles.lang';
import { IVehicleAccesory, IVehicleAccesoryRequestPut } from '@mx/statemanagement/models/vehicle.interface';
import { Observable, Subscription } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ManagementVehicleAccesoryComponent } from './management-vehicle-accesory/management-vehicle-accesory.component';

@Component({
  selector: 'client-vehicle-accesories',
  templateUrl: './vehicle-accesories.component.html'
})
export class VehicleAccesoriesComponent implements OnInit {
  @Input() accesories: Array<IVehicleAccesory>;
  @Input() numberPhoneErrorEditAccesory: string;
  @ViewChild(ManagementVehicleAccesoryComponent) manageVehicleAccesory: ManagementVehicleAccesoryComponent;
  @ViewChild(MfModalMessage) mfModalMessage: MfModalMessage;
  @ViewChild(MfModalAlertComponent) mfModalDelete: MfModalAlertComponent;

  accesorySelected: IVehicleAccesory;
  index: number;
  subRoute: Subscription;

  motorNumber: string;
  chassis: string;
  policyNumber: string;

  vehiclesLang = VehiclesLang;
  doneMessage = GeneralLang.Messages.Fail;
  messageSuccessOrError = '';
  doneDelete = false;
  icon: string;
  showSpinner: boolean;
  addIcon = ADD_ICON;

  constructor(private readonly vehicleAccesoryService: VehicleService, private readonly route: ActivatedRoute) {}

  ngOnInit(): void {
    this.mfModalDelete.sizeClass = 'g-modal--medium';
    this.subRoute = this.route.params.subscribe(params => {
      this.motorNumber = params['motorNumber'];
      this.chassis = params['chassis'];
      this.policyNumber = params['policyNumber'];
    });
  }

  trackByFn(index, item): number {
    return item;
  }

  openManageVehicleAccesory(): void {
    this.accesorySelected = null;
    this.index = null;
    this.manageVehicleAccesory.initialiceForm();
    setTimeout(() => {
      this.manageVehicleAccesory.open();
    }, 10);
  }

  editAccesory(accesory: IVehicleAccesory, index: number): void {
    if (accesory.esAutogestionable) {
      this.accesorySelected = accesory;
      this.index = index;
      this.manageVehicleAccesory.initialiceForm(accesory);
      setTimeout(() => {
        this.manageVehicleAccesory.open();
      }, 10);
    } else {
      this.doneDelete = false;
      this.icon = VehiclesLang.Icon.error;
      this.doneMessage = VehiclesLang.Messages.VehicleErrorEditAccesoryTitle;
      this.messageSuccessOrError = `${VehiclesLang.Messages.VehicleErrorEditAccesory}${this.numberPhoneErrorEditAccesory}.`;
      this.mfModalMessage.mfModalAlert.sizeClass = 'g-modal--medium';
      this.mfModalMessage.open();
    }
  }

  deleteAccesoryModal(accesory: IVehicleAccesory): void {
    this.accesorySelected = accesory;
    this.manageVehicleAccesory.initialiceForm();
    setTimeout(() => {
      this.mfModalDelete.open();
    }, 10);
  }

  prepareRequest(): IVehicleAccesoryRequestPut {
    return {
      accesorioId: `${this.accesorySelected.accesorioId}`,
      chasis: this.chassis,
      nroMotor: this.motorNumber,
      numeroPoliza: this.policyNumber,
      codigoApp: APPLICATION_CODE
    };
  }

  async deleteAccesory(confirm: boolean): Promise<void> {
    if (confirm && !this.showSpinner) {
      const requestData = this.prepareRequest();
      const index = this.accesories.findIndex(v => v.accesorioId === parseInt(requestData.accesorioId, 10));
      this.showSpinner = true;
      await this.vehicleAccesoryService
        .deleteAccesory(requestData)
        .pipe(
          catchError((error: any, caught: Observable<any>) => {
            this.doneDelete = false;
            this.showSpinner = false;
            this.mfModalDelete.close();

            return new Observable<any>();
          })
        )
        .toPromise();
      this.showSpinner = false;
      this.doneDelete = true;
      this.accesories = [...this.accesories.slice(0, index), ...this.accesories.slice(index + 1)];
      this.mfModalDelete.close();
    }
  }

  updateAccesory(accesory: IVehicleAccesory): void {
    if (accesory && this.index > -1) {
      this.accesories = [
        ...this.accesories.slice(0, this.index),
        {
          ...accesory,
          esAutogestionable: true
        },
        ...this.accesories.slice(this.index + 1)
      ];
    }
  }

  addAccesory(accesory: IVehicleAccesory): void {
    if (accesory) {
      accesory.esAutogestionable = true;
      this.accesories = this.accesories.concat(accesory);
    }
  }
}
