import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: './vehicles-insurance-list/vehicles-insurance-list.module#VehiclesInsuranceListModule'
  },
  {
    path: 'faqs',
    loadChildren: './vehicles-insurance-faq/vehicles-insurance-faq.module#VehiclesInsuranceFAQModule'
  },
  {
    path: 'compare/:policyNumber',
    loadChildren: './vehicles-compare/vehicles-compare.module#VehiclesCompareModule'
  },
  {
    path: ':policyNumber',
    loadChildren: './vehicles-insurance-by-id/vehicles-insurance-by-id.module#VehiclesInsuranceByIdModule'
  },
  {
    path: 'detail-vehicle/:policyNumber/:chassis/:motorNumber',
    loadChildren: './detail-vehicle/detail-vehicle.module#DetailVehicleModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiclesInsuranceRoutingModule {}
