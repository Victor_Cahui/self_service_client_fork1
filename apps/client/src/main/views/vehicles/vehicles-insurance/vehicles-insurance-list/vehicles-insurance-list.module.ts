import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardMyPoliciesByTypeModule } from '@mx/components/card-my-policies/card-my-policies-by-type/card-my-policies-by-type.module';
import { CardWhatYouWantToDoModule } from '@mx/components/card-what-you-want-to-do/card-what-you-want-to-do.module';
import { BannerCarouselModule } from '@mx/components/shared/banner-carousel/banner-carousel.module';
import { VehiclesInsuranceListComponent } from './vehicles-insurance-list.component';
import { VehiclesInsuranceListRoutingModule } from './vehicles-insurance-list.routing';

@NgModule({
  imports: [
    CommonModule,
    CardMyPoliciesByTypeModule,
    VehiclesInsuranceListRoutingModule,
    CardWhatYouWantToDoModule,
    BannerCarouselModule
  ],
  declarations: [VehiclesInsuranceListComponent]
})
export class VehiclesInsuranceListModule {}
