import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VehiclesInsuranceListComponent } from './vehicles-insurance-list.component';

const routes: Routes = [
  {
    path: '',
    component: VehiclesInsuranceListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiclesInsuranceListRoutingModule {}
