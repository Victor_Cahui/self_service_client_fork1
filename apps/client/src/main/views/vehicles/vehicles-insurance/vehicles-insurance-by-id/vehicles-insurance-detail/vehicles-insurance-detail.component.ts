import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CardDetailPolicyBase } from '@mx/components/shared/card-detail-policy/card-detail-policy-base';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { KEY_PERMISSIONS } from '@mx/settings/auth/auth-values';
import { MFP_Cuotas_de_Poliza_Pagar_28C } from '@mx/settings/constants/events.analytics';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';

@Component({
  selector: 'client-vehicles-insurance-detail',
  templateUrl: './vehicles-insurance-detail.component.html'
})
export class VehiclesInsuranceDetailComponent extends CardDetailPolicyBase implements OnInit {
  protected viewTrace: boolean;
  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService,
    protected policiesService: PoliciesService,
    protected headerHelperService: HeaderHelperService,
    protected activatedRoute: ActivatedRoute,
    protected localStorageService: LocalStorageService,
    protected router: Router,
    protected _Autoservicios: Autoservicios
  ) {
    super(
      clientService,
      policiesInfoService,
      configurationService,
      authService,
      policiesService,
      headerHelperService,
      activatedRoute,
      localStorageService,
      router,
      false,
      _Autoservicios
    );
    this.policyType = POLICY_TYPES.MD_AUTOS.code;
    this.ga = [MFP_Cuotas_de_Poliza_Pagar_28C('Vehiculos')];
  }

  ngOnInit(): void {
    this.loadInfoProfile();
    this.canTransferPolicy();
  }

  canTransferPolicy(): void {
    const TRANSFER_POLICY = 'ACTIVATE_TRANSFER_POLICY';
    const canTransferPolicy = JSON.parse(this.localStorageService.getItem(KEY_PERMISSIONS)).find(
      permission => permission.codigo === TRANSFER_POLICY
    );
    this.viewTrace = canTransferPolicy && canTransferPolicy.valor;
  }
}
