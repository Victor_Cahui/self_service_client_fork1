import { Component, HostBinding } from '@angular/core';
import { COVERAGES_FOIL_ATTACHED } from '@mx/settings/lang/vehicles.lang';

@Component({
  selector: 'client-coverages-foil-attached-component',
  templateUrl: './coverages-foil-attached.component.html'
})
export class CoveragesFoilAttachedComponent {
  @HostBinding('class') class = 'w-100';
  content = { title: COVERAGES_FOIL_ATTACHED.title };
}
