import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { InputSearchModule } from '@mx/components/shared/input-search/input-search.module';
import { ItemNotFoundModule } from '@mx/components/shared/item-not-found/item-not-found.module';
import { CardPolicyDeductibleModule } from '@mx/components/vehicles/card-policy-deductible/card-policy-deductible.module';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { CoveragesDeductiblesComponent } from './coverages-deductibles.component';
import { CoveragesDeductiblesRoutingModule } from './coverages-deductibles.routing';

@NgModule({
  imports: [
    CommonModule,
    CoveragesDeductiblesRoutingModule,
    CardPolicyDeductibleModule,
    FormsModule,
    ItemNotFoundModule,
    InputSearchModule,
    MfLoaderModule
  ],
  declarations: [CoveragesDeductiblesComponent]
})
export class CoveragesDeductiblesModule {}
