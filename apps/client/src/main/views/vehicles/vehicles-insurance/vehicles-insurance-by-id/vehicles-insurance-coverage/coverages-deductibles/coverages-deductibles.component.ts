import { Component, HostBinding, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PoliciesService } from '@mx/services/policies.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { VEHICLES_COVERAGE_TABS } from '@mx/settings/constants/router-tabs';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { VehiclesLang } from '@mx/settings/lang/vehicles.lang';
import { ICoveragesPolicyRequest, IDeductiblePolicy } from '@mx/statemanagement/models/policy.interface';

@Component({
  selector: 'client-coverages-deductibles-component',
  templateUrl: './coverages-deductibles.component.html'
})
export class CoveragesDeductiblesComponent implements OnInit {
  @HostBinding('class') class = 'w-100';

  itemsList: Array<IDeductiblePolicy>;
  policyNumber: string;
  hideShowAll: boolean;
  limitItemsInit = 2;
  showLoading: boolean;
  labelViewAll = GeneralLang.Buttons.ViewAll;
  messageNotFound = VehiclesLang.Messages.NotFoundVehicles;
  title = VEHICLES_COVERAGE_TABS[2].name.toUpperCase();
  labelFindYourVehicle = VehiclesLang.Labels.FindYourVehicle;

  private deductiblesList: Array<IDeductiblePolicy>;
  private filterItemsList: Array<IDeductiblePolicy>;
  private searchText: string;

  constructor(private readonly policyService: PoliciesService, private readonly activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((res: any) => {
      this.policyNumber = res.params.policyNumber;
    });
    this.getDeductibles();
  }

  private getDeductibles(): void {
    this.showLoading = true;
    this.policyService
      .getDeductiblesByPolicy({
        codigoApp: APPLICATION_CODE,
        numeroPoliza: this.policyNumber
      } as ICoveragesPolicyRequest)
      .subscribe(
        res => {
          // tslint:disable-next-line: no-parameter-reassignment
          res = res || [];
          this.deductiblesList = res;
          this.limit();
        },
        () => {
          this.showLoading = false;
        },
        () => {
          this.showLoading = false;
        }
      );
  }

  filter(searchText?: string): void {
    this.searchText = searchText;
    let items;
    if (!this.searchText) {
      items = this.deductiblesList;
    } else {
      items = this.deductiblesList.filter(deductible => {
        // Filtro por placa
        const isPlaca =
          (deductible.vehiculo.placa || '').toLowerCase().indexOf(this.searchText.toLowerCase().trim()) > -1;
        // Filtro por modelo
        const isModelo =
          (deductible.vehiculo.modelo || '').toLowerCase().indexOf(this.searchText.toLowerCase().trim()) > -1;
        // Filtro por marca
        const isMarca =
          (deductible.vehiculo.marca || '').toLowerCase().indexOf(this.searchText.toLowerCase().trim()) > -1;

        return isPlaca || isModelo || isMarca;
      });
    }
    this.limit(items);
  }

  private limit(items?: Array<IDeductiblePolicy>): void {
    this.filterItemsList = items || this.deductiblesList;
    this.itemsList =
      !this.hideShowAll && !this.searchText ? this.filterItemsList.slice(0, this.limitItemsInit) : this.filterItemsList;
  }

  showAll(): void {
    this.hideShowAll = true;
    this.filter();
  }

  isHideButtonShowAll(): boolean {
    if (this.hideShowAll) {
      return this.hideShowAll;
    } else if (this.searchText) {
      return true;
    }

    return (this.filterItemsList || []).length <= this.limitItemsInit;
  }
}
