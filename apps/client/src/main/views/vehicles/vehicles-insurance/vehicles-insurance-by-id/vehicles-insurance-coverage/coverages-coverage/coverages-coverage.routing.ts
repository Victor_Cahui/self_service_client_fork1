import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CoveragesCoverageComponent } from './coverages-coverage.component';

const routes: Routes = [
  {
    path: '',
    component: CoveragesCoverageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoveragesCoverageRoutingModule {}
