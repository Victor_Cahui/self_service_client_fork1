import { Component, HostBinding, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { environment } from '@mx/environments/environment';
import { AuthService } from '@mx/services/auth/auth.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesService } from '@mx/services/policies.service';
import { VehicleService } from '@mx/services/vehicle.service';
import { COVERAGES_VEHICLES, ICON_COVERAGE_DEFAULT } from '@mx/settings/constants/coverage-values';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { VEHICLES_INSURANCE_TAB } from '@mx/settings/constants/router-tabs';
import { COVERAGES_BY_POLICY_VEHICLE } from '@mx/settings/lang/vehicles.lang';
import { ICoverageItem } from '@mx/statemanagement/models/coverage.interface';
import { ICoveragesPolicy, ICoveragesPolicyVehicleRequest } from '@mx/statemanagement/models/policy.interface';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { IClientVehicleDetail, IVehiclesListRequest } from '@mx/statemanagement/models/vehicle.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-coverages-coverage-component',
  templateUrl: './coverages-coverage.component.html'
})
export class CoveragesCoverageComponent implements OnDestroy, OnInit {
  @HostBinding('class') class = 'w-100';
  descriptionStart: string;
  descriptionEnd: string;
  nameSelectLabel: string;
  headerSub: Subscription;
  typePolicy: string;
  changesSub: Subscription;
  coveragesVehicleSub: Subscription;
  vehiclesSub: Subscription;
  formVehicle: FormGroup;
  selectVehicle: AbstractControl;
  vehiclesList: Array<IClientVehicleDetail>;
  userClient: IdDocument;
  policyNumber: string;
  coveragesList: Array<ICoveragesPolicy>;
  title: string;
  coveragesListGroup: any;
  titlesGroup: any;
  showLoading: boolean;

  constructor(
    private readonly policyService: PoliciesService,
    private readonly authService: AuthService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly vehicleService: VehicleService,
    private readonly formBuilder: FormBuilder,
    private readonly headerHelperService: HeaderHelperService
  ) {
    this.title = VEHICLES_INSURANCE_TAB[1].name.toUpperCase();
    this.descriptionStart = COVERAGES_BY_POLICY_VEHICLE.startCopy;
    this.descriptionEnd = COVERAGES_BY_POLICY_VEHICLE.endCopy;
    this.nameSelectLabel = COVERAGES_BY_POLICY_VEHICLE.selectLabel;
  }

  ngOnInit(): void {
    this.createFormVehicle();
    this.onChanges();
    this.userClient = this.authService.retrieveEntity();
    this.activatedRoute.paramMap.subscribe((res: any) => {
      this.policyNumber = res.params.policyNumber;
    });
    this.getListVehicles();
    this.headerSub = this.headerHelperService.get().subscribe(res => {
      this.typePolicy = res.title;
    });
  }

  ngOnDestroy(): void {
    if (!!this.changesSub) {
      this.changesSub.unsubscribe();
    }
    if (!!this.headerSub) {
      this.headerSub.unsubscribe();
    }
  }

  createFormVehicle(): void {
    this.formVehicle = this.formBuilder.group({
      selectVehicle: ['']
    });
    this.selectVehicle = this.formVehicle.controls['selectVehicle'];
  }

  onChanges(): void {
    this.changesSub = this.selectVehicle.valueChanges.subscribe(val => {
      this.getCoveragesByVehicle(val);
    });
  }

  getCoveragesByVehicle(vehicle): void {
    this.showLoading = true;
    this.coveragesVehicleSub = this.policyService
      .getCoveragesByPolicyAndVehicle({
        codigoApp: APPLICATION_CODE,
        numeroPoliza: this.policyNumber,
        chasis: vehicle.numeroChasis,
        nroMotor: vehicle.numeroMotor
      } as ICoveragesPolicyVehicleRequest)
      .subscribe(
        (res: any) => {
          this.coveragesList = res.coberturas === null ? [] : res.coberturas || [];
          if (this.coveragesList.length > 0) {
            this.setGroupList(this.coveragesList);
          }
        },
        () => {
          this.showLoading = false;
        },
        () => {
          this.showLoading = false;
          this.coveragesVehicleSub.unsubscribe();
        }
      );
  }

  getListVehicles(): void {
    this.vehiclesSub = this.vehicleService
      .getVehiclesList({
        codigoApp: APPLICATION_CODE,
        numeroPoliza: this.policyNumber
      } as IVehiclesListRequest)
      .subscribe((res: Array<IClientVehicleDetail>) => {
        this.vehiclesList = res === null ? [] : res;
        if (this.vehiclesList.length > 0) {
          this.selectVehicle.setValue(this.vehiclesList[0]);
        }
      });
  }

  setDataItemCoverage(list: Array<ICoveragesPolicy>): Array<ICoverageItem> {
    let itemList = [];
    itemList = list.map(item => {
      const iconC = COVERAGES_VEHICLES.find(c => {
        return c.key === item.llave;
      });
      const itemCoverage: ICoverageItem = {
        title: item.tituloInfo,
        description: item.descripcionInfo,
        question: item.accionTexto,
        questionBool: item.accion,
        routeKey: item.accionLlave,
        iconKey: item.llave,
        icon: (iconC && iconC.icon) || ICON_COVERAGE_DEFAULT
      };

      return itemCoverage;
    });

    return itemList;
  }

  setGroupList(list: Array<ICoveragesPolicy>): void {
    this.titlesGroup = list.map(item => {
      return item.nombreGrupo;
    });
    const titles = [];
    const itemsGroupList = [];
    this.titlesGroup.forEach(element => {
      if (titles.indexOf(element) === -1) {
        titles.push(element);
      }
    });
    this.titlesGroup = titles;
    this.titlesGroup.forEach(element => {
      this.coveragesListGroup = list.filter(item => {
        return item.nombreGrupo === element;
      });
      itemsGroupList.push({
        title: element,
        items: this.setDataItemCoverage(this.coveragesListGroup)
      });
    });
    this.coveragesListGroup = itemsGroupList;
    if (!environment.ACTIVATE_OTHERS_COVERAGES) {
      this.coveragesListGroup = itemsGroupList.filter(
        e => !['OTRAS COBERTURAS', 'OTROS'].includes(e.title.toUpperCase())
      );
    }
  }
}
