import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CoveragesDeductiblesComponent } from './coverages-deductibles.component';

const routes: Routes = [
  {
    path: '',
    component: CoveragesDeductiblesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoveragesDeductiblesRoutingModule {}
