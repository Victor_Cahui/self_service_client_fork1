import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfTabTopModule } from '@mx/core/ui/lib/components/tabs/tabs-top/tab-top.module';
import { VehiclesInsuranceByIdComponent } from './vehicles-insurance-by-id.component';
import { VehiclesInsuranceByIdRoutingModule } from './vehicles-insurance-by-id.routing';

@NgModule({
  imports: [CommonModule, VehiclesInsuranceByIdRoutingModule, MfTabTopModule],
  exports: [],
  declarations: [VehiclesInsuranceByIdComponent]
})
export class VehiclesInsuranceByIdModule {}
