import { Component, HostBinding } from '@angular/core';
import { EXCLUSIONS } from '@mx/settings/lang/vehicles.lang';

@Component({
  selector: 'client-coverages-exclusions-component',
  templateUrl: './coverages-exclusions.component.html'
})
export class CoveragesExclusionsComponent {
  @HostBinding('class') class = 'w-100';

  content = EXCLUSIONS;
}
