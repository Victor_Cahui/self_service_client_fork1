import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VehiclesInsuranceDetailComponent } from './vehicles-insurance-detail.component';

const routes: Routes = [
  {
    path: '',
    component: VehiclesInsuranceDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiclesInsuranceDetailRoutingModule {}
