import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardContentModule } from '@mx/components/shared/card-content/card-content.module';
import { DownLoadConditionedModule } from '@mx/components/shared/download-conditioned/download-conditioned.module';
import { CoveragesExclusionsComponent } from './coverages-exclusions.component';
import { CoveragesExclusionsRoutingModule } from './coverages-exclusions.routing';

@NgModule({
  imports: [CommonModule, CoveragesExclusionsRoutingModule, CardContentModule, DownLoadConditionedModule],
  declarations: [CoveragesExclusionsComponent]
})
export class CoveragesExclusionsModule {}
