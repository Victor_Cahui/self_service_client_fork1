import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VehiclesCompareComponent } from './vehicles-compare.component';

const routes: Routes = [
  {
    path: '',
    component: VehiclesCompareComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiclesCompareRoutingModule {}
