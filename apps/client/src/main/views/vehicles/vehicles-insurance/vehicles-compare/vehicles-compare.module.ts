import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PoliciesCompareModule } from '@mx/components/shared/compare-policy/policies-compare/policies-compare.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { MfAlertWarningModule } from '@mx/core/ui/lib/components/alerts/alert-warning';
import { MfCustomAlertModule } from '@mx/core/ui/lib/components/alerts/custom-alert';
import { MfLoaderModule } from '@mx/core/ui/lib/components/global/loader';
import { MfModalModule } from '@mx/core/ui/lib/components/modals/modal';
import { MfModalMessageModule } from '@mx/core/ui/lib/components/modals/modal-message';
import { VehiclesCompareComponent } from './vehicles-compare.component';
import { VehiclesCompareRoutingModule } from './vehicles-compare.routing';

@NgModule({
  imports: [
    CommonModule,
    GoogleModule,
    MfAlertWarningModule,
    MfLoaderModule,
    MfModalMessageModule,
    MfModalModule,
    PoliciesCompareModule,
    VehiclesCompareRoutingModule,
    MfCustomAlertModule
  ],
  declarations: [VehiclesCompareComponent]
})
export class VehiclesCompareModule {}
