import { DecimalPipe } from '@angular/common';
import { Component, HostBinding, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { BaseVehicleComparePolicies } from '@mx/components/shared/compare-policy/base-vehicle-compare-policies';
import { GAService, IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { MfModal, MfModalMessage } from '@mx/core/ui';
import { NotificationStatus } from '@mx/core/ui/lib/components/alerts/custom-alert/custom-alert.component';
import { AuthService } from '@mx/services/auth/auth.service';
import { GeneralService } from '@mx/services/general/general.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PolicyListService } from '@mx/services/policy/policy-list.service';
import { PoliciesQuoteService } from '@mx/services/policy/policy-quote.service';
import { VehicleService } from '@mx/services/vehicle.service';
import { MFP_Comparar_Poliza_21A, MFP_Comparar_Poliza_22A } from '@mx/settings/constants/events.analytics';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { MODALITY_CHANGE_TIME } from '@mx/settings/constants/key-values';
import { VEHICLES_COMPARE_HEADER } from '@mx/settings/constants/router-titles';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import { IParameterApp } from '@mx/statemanagement/models/general.models';
import {
  IChangeModalityBody,
  IChangeModalityRequest,
  ICompareHeaderView
} from '@mx/statemanagement/models/policy.interface';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'client-vehicles-compare-component',
  templateUrl: './vehicles-compare.component.html',
  providers: [DecimalPipe]
})
export class VehiclesCompareComponent extends BaseVehicleComparePolicies implements OnInit {
  @HostBinding('class') class = 'w-100';
  @ViewChild(MfModal) mfModalConfirm: MfModal;
  @ViewChild(MfModalMessage) mfModalDone: MfModalMessage;
  ga: Array<IGaPropertie>;

  parametersSubject: Observable<Array<IParameterApp>>;
  lang = PolicyLang;
  old = GeneralLang.Labels.Old;
  new = GeneralLang.Labels.New;
  messsageDaysForChange: string;
  messsageDaysForChangeSuccess: string;
  titleMobile: string;
  subTitleMobile: string;
  quoteOld: string;
  feeOld: string;
  quoteNew: string;
  feeNew: string;
  diff: string;
  days: string;
  bodyParams: IChangeModalityBody;
  modalitySub: Subscription;
  status = NotificationStatus.INFO;

  constructor(
    protected policyListService: PolicyListService,
    protected headerHelperService: HeaderHelperService,
    protected quoteService: PoliciesQuoteService,
    protected vehicleService: VehicleService,
    private readonly generalService: GeneralService,
    private readonly policyServices: PoliciesService,
    private readonly authService: AuthService,
    public router: Router,
    protected decimalPipe: DecimalPipe,
    protected gaService?: GAService
  ) {
    super(policyListService, headerHelperService, quoteService, vehicleService, decimalPipe, gaService);
    this.headerHelperService.set(VEHICLES_COMPARE_HEADER);
    this.titleMobile = VEHICLES_COMPARE_HEADER.subTitle;
    this.subTitleMobile = VEHICLES_COMPARE_HEADER.title;
    this.ga = [MFP_Comparar_Poliza_21A()];
  }

  ngOnInit(): void {
    this.compare = true;
    this.getParameters();
    this.viewInitial();
  }

  // Cambiar de Modalidad
  onChange(item: ICompareHeaderView): void {
    this.policyNew = item.title;
    this.quoteNew = `${this.lang.Texts.Quote}:  ${item.moneySymbol} ${this.decimalPipe.transform(
      item.quote,
      '1.2-2'
    )} `;
    this.feeNew = `${this.lang.Texts.Fee}: ${item.moneySymbol} ${this.decimalPipe.transform(
      item.monthlyCost,
      '1.2-2'
    )}`;
    const current = this.coveragesHeader.filter(policy => policy.current)[0];
    if (!current) {
      return;
    }
    this.policyCurrent = current.title;
    this.quoteOld = `${this.lang.Texts.Quote}: ${item.moneySymbol} ${this.decimalPipe.transform(
      current.quote,
      '1.2-2'
    )}`;
    this.feeOld = `${this.lang.Texts.Fee}: ${item.moneySymbol} ${this.decimalPipe.transform(
      current.monthlyCost,
      '1.2-2'
    )}`;
    const diff = item.quote - current.quote;
    const less = item.quote < current.quote;
    this.diff = `${less || diff === 0 ? '' : '+'}${this.decimalPipe.transform(diff, '1.2-2')}`;
    this.bodyParams = {
      datosPolizaVigente: {
        codigoCompania: current.company,
        codigoRamo: current.branch,
        codigoModalidad: current.modality
      },
      datosPolizaNueva: {
        codigoCompania: item.company,
        codigoRamo: item.branch,
        codigoModalidad: item.modality
      }
    };
    this.openModalConfirm = true;
    setTimeout(() => {
      this.mfModalConfirm.open();
    }, 10);
  }

  // Confirmar cambio de modalidad
  changeModality(confirm: boolean): void {
    this.addGAEvent(MFP_Comparar_Poliza_22A(), { lbl: this.yes });

    if (!confirm) {
      return;
    }
    this.openModalConfirm = false;
    const params: IChangeModalityRequest = {
      codigoApp: APPLICATION_CODE,
      numeroPoliza: this.policySelected
    };
    this.showLoading = true;
    this.modalitySub = this.policyServices.changeModality(params, this.bodyParams).subscribe(
      response => {
        this.titleDone = GeneralLang.Messages.Done;
        this.messageDone = PolicyLang.Messages.SuccessChangeModality;
        this.openModalDone = true;
        setTimeout(() => {
          this.mfModalDone.open();
        }, 10);
      },
      () => {
        this.showLoading = false;
      },
      () => {
        this.showLoading = false;
        this.modalitySub.unsubscribe();
      }
    );
  }

  closeEvent(b: boolean): void {
    if (!b) {
      this.addGAEvent(MFP_Comparar_Poliza_22A(), { lbl: this.no });
    }
  }

  backToList(): void {
    this.router.navigate(['/vehicles/vehicularInsurance']);
  }

  // Días para el cambio
  getParameters(): void {
    this.parametersSubject = this.generalService.getParametersSubject();
    this.parametersSubject.subscribe(params => {
      this.days = this.generalService.getValueParams(MODALITY_CHANGE_TIME);
      this.messsageDaysForChange = this.lang.Messages.TimeChangeModality.replace('{{days}}', this.days);
      this.messsageDaysForChangeSuccess = this.lang.Messages.SuccessChangeModality.replace('{{days}}', this.days);
    });
  }
}
