import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'sctr-insurance',
    loadChildren: './sctr-insurance/sctr-insurance.module#SctrInsuranceModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SctrRoutingModule {}
