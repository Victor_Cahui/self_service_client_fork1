import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardMyPoliciesByTypeModule } from '@mx/components/card-my-policies/card-my-policies-by-type/card-my-policies-by-type.module';
import { CardMyPoliciesSctrModule } from '@mx/components/card-my-policies/card-my-policies-sctr/card-my-policies-sctr.module';
import { CardWhatYouWantToDoModule } from '@mx/components/card-what-you-want-to-do/card-what-you-want-to-do.module';
import { BannerCarouselModule } from '@mx/components/shared/banner-carousel/banner-carousel.module';
import { SctrInsuranceListComponent } from './sctr-insurance-list.component';
import { SctrInsuranceListRoutingModule } from './sctr-insurance-list.routing';

@NgModule({
  imports: [
    CommonModule,
    BannerCarouselModule,
    SctrInsuranceListRoutingModule,
    CardMyPoliciesSctrModule,
    CardMyPoliciesByTypeModule,
    CardWhatYouWantToDoModule
  ],
  declarations: [SctrInsuranceListComponent],
  exports: [SctrInsuranceListComponent]
})
export class SctrInsuranceListModule {}
