import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SctrCoverageSurvivalComponent } from '../sctr-coverage-survival/sctr-coverage-survival.component';

const routes: Routes = [
  {
    path: '',
    component: SctrCoverageSurvivalComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SctrCoverageSurvivalRouting {}
