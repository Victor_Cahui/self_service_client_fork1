import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CardSctrPeriodConfirModule } from '@mx/components/shared/card-sctr-period-confirm/card-sctr-period-confirm.module';
import { SctrPeriodOperationConfirmComponent } from './sctr-period-operation-confirm.component';
import { SctrPeriodOperationConfirmRoutingModule } from './sctr-period-operation-confirm.routing';

@NgModule({
  imports: [CommonModule, SctrPeriodOperationConfirmRoutingModule, CardSctrPeriodConfirModule],
  declarations: [SctrPeriodOperationConfirmComponent],
  exports: [SctrPeriodOperationConfirmComponent]
})
export class SctrPeriodOperationConfirmModule {}
