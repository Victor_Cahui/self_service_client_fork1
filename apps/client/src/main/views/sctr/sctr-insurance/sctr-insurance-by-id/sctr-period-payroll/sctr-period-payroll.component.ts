import { CurrencyPipe, DatePipe, DOCUMENT } from '@angular/common';
import { AfterViewInit, Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { FileUtil } from '@mx/core/shared/helpers/util/file';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { Autoservicios, FrmProvider } from '@mx/core/shared/providers/services';
import { MfModal } from '@mx/core/ui';
import { ConfigCbo } from '@mx/core/ui/lib/components/forms/select';
import { AuthService } from '@mx/services/auth/auth.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { SctrService } from '@mx/services/sctr.service';
import {
  APPLICATION_CODE,
  GENERAL_MAX_LENGTH,
  GENERAL_MIN_LENGTH,
  REG_EX
} from '@mx/settings/constants/general-values';
import { SCTR_PERIOD_PAYROLL_HEADER } from '@mx/settings/constants/router-titles';
import { Subscription } from 'rxjs';
import { SctrPeriodReceiptsBase } from '../sctr-period-receipts/sctr-period-receipts-base';

@Component({
  selector: 'client-sctr-period-payroll',
  templateUrl: './sctr-period-payroll.component.html',
  providers: [CurrencyPipe, DatePipe]
})
export class SctrPeriodPayrollComponent extends SctrPeriodReceiptsBase implements AfterViewInit, OnDestroy, OnInit {
  @ViewChild('inputSearch') public inputSearch: any;
  @ViewChild(MfModal) mfModal: MfModal;

  insuredList: Array<any> = [];
  searchText: string;
  totalPluralized: string;
  configCbo: ConfigCbo;

  isDownloading: boolean;
  isSctrDoubleEmission: boolean;
  isSelectMode: boolean;
  isEditing: boolean;
  showModal: boolean;
  isSelectAll = true;
  selected: number;
  downloadIcon = 'assets/images/services/icon-file-download.svg';
  maxLengthSearch = GENERAL_MAX_LENGTH.searchInsured;
  minLengthSearch = GENERAL_MIN_LENGTH.searchInsured;

  fileUtil: FileUtil;
  pdfSub: Subscription;

  formAditional: FormGroup;

  constructor(
    @Inject(DOCUMENT) protected document: Document,
    public sctrService: SctrService,
    protected datePipe: DatePipe,
    protected currencyPipe: CurrencyPipe,
    protected activatedRoute: ActivatedRoute,
    protected authService: AuthService,
    protected _Autoservicios: Autoservicios,
    protected policiesInfoService: PoliciesInfoService,
    protected headerHelperService: HeaderHelperService,
    protected policiesService: PoliciesService,
    protected storageService: StorageService,
    protected gaService: GAService,
    private readonly _FrmProvider: FrmProvider,
    private readonly formBuilder: FormBuilder
  ) {
    super(
      document,
      datePipe,
      activatedRoute,
      authService,
      _Autoservicios,
      policiesInfoService,
      headerHelperService,
      policiesService,
      storageService,
      gaService
    );
    this.policyNumber = this.activatedRoute.snapshot.params['policyNumber'];
    this.auth = this.authService.retrieveEntity();
    this.fileUtil = new FileUtil();
  }

  ngOnInit(): void {
    this.header = SCTR_PERIOD_PAYROLL_HEADER;
    this.loadInfoDetailSctr();

    this.setForms();
    this.frmPeriod.valueChanges.subscribe(this._mapChangedFrmPeriod.bind(this));
    this.frmFilter.valueChanges.subscribe(this._mapChangedFrmFilter.bind(this));
  }

  ngAfterViewInit(): void {
    this.hideTabTop();
  }

  ngOnDestroy(): void {
    this.showTabTop();
    this.pdfSub && this.pdfSub.unsubscribe();
  }

  getPeriodPayroll(): void {
    this.insuredList = [];
    this.showLoading = true;
    this._isEventFromFilter = false;
    this.totalPluralized = '';
    this.isEditing = false;

    const pathReq = {
      numeroPoliza: this.policyNumber
    };

    const fechaFinVigencia = this.fechaFinVigencia;
    const fechaInicioVigencia = this.fechaInicioVigencia;
    const codRamo = this.policyType;
    const textoBuscar = this.searchText;

    const queryReq: any = {
      codigoApp: APPLICATION_CODE,
      tipoDocumento: this.auth.type,
      documento: this.auth.number,
      registros: this.itemsPerPage,
      pagina: this.page,
      ...(fechaFinVigencia && { fechaFinVigencia }),
      ...(fechaInicioVigencia && { fechaInicioVigencia }),
      ...(codRamo && { codRamo }),
      ...(textoBuscar && { textoBuscar })
    };

    this._Autoservicios.ObtenerPlanillaAsegurados(pathReq, queryReq).subscribe(
      (response: any) => {
        if (response) {
          this.insuredList = response.listaResultados;
          this.total = response.totalRegistros;
          this.totalPluralized = `- ${this.total} ${this.total === 1 ? this.lang.Insured : this.lang.Insureds}`;
          this.showLoading = false;
        }
      },
      () => (this.showLoading = false),
      () => (this.showLoading = false)
    );
  }

  pageChange(page: number): void {
    this.page = page;
    this.getPeriodPayroll();
  }

  itemsPerPageChange(itemsPerPage?: number): void {
    this.page = 1;
    this.itemsPerPage = itemsPerPage || this.itemsPerPage;
    this.getPeriodPayroll();
  }

  setForms(): void {
    this.configCbo = { canAutoSelectFirstOpt: false };
    this.frmPeriod = this._FrmProvider.SctrPeriodReceiptsComponent();
    this.frmFilter = this._FrmProvider.SctrPeriodPayrollFilter();

    this.formAditional = this.formBuilder.group({
      jobCenter: ['', Validators.pattern(REG_EX.alphaNumericText)]
    });
  }

  search(): void {
    if (this.frmPeriod.valid) {
      this.itemsPerPageChange();
    }
  }

  onSearch(text: string): void {
    this.searchText = text;
  }

  clear(): void {
    this.searchText = '';
    this.inputSearch.clearText();
    this._isEventFromFilter || this.itemsPerPageChange();
  }

  onCancelSelectMode(): void {
    this.isSelectMode = false;
    this.isSelectAll = true;
    this.sctrService.clearInsuredConstancyList();
  }

  onAction(): void {
    if (this.isEditing || !this.isSelectAll) {
      return void 0;
    }

    if (this.isSelectMode) {
      this._openModal();
    } else {
      this.isSelectMode = true;
      this.selected = this.total;
    }
  }

  doSelectAll(isSelectAll: boolean): void {
    this.isSelectAll = isSelectAll;
    this.selected = isSelectAll ? this.total : 0;
  }

  doEdit(isEditing: boolean): void {
    this.isEditing = isEditing;
  }

  doSelect(insured: any): void {
    this.isSelectAll
      ? insured.checked
        ? this.sctrService.removeInsuredConstancyList(insured.codAsegurado)
        : this.sctrService.addInsuredConstancyList(insured.codAsegurado)
      : insured.checked
      ? this.sctrService.addInsuredConstancyList(insured.codAsegurado)
      : this.sctrService.removeInsuredConstancyList(insured.codAsegurado);

    this.selected = this.isSelectAll
      ? this.total - this.sctrService.getInsuredConstancyList().length
      : this.sctrService.getInsuredConstancyList().length;
  }

  setFilters(f: any): void {
    this.showFilters = true;
    const periodArr = f.periodMonth.split('|') || [];
    this.fechaInicioVigencia = periodArr[0] || '';
    this.fechaFinVigencia = periodArr[1] || '';
    this.periodType = periodArr[2] || '';
    this.isAdditional = this.periodType === 'MA';
  }

  doActionsAfterPeriodsAsyncEnds(): void {
    this.period = this.periodList.length ? this.periodList[0] : null;
    this.isSctrDoubleEmission = this.period.policies.length === 2;
    this.showLoadingPeriod = false;
    this.getPeriodPayroll();
  }

  getCboLoadingState(e: any): void {
    this.isLoadingFilterVisible = e.isLoadingVisible;
    this.verifySelected();
  }

  confirmResponse(event: boolean): void {
    event && this._downloadConstancy();
    !event && this.formAditional.controls['jobCenter'].setValue('');
  }

  onPaste(event: any, control: string): void {
    let clipboard = event.clipboardData.getData('text/plain');
    clipboard = clipboard.replace(/<[^>]*>/g, '');
    setTimeout(() => {
      this.formAditional.controls[control].setValue(clipboard);
    });
  }

  private _openModal(): void {
    this.showModal = true;
    setTimeout(() => {
      this.mfModal.open();
    });
  }

  private _mapChangedFrmPeriod(f: any): void {
    this._isEventFromFilter = true;
    this.showLoadingPeriod = true;
    this.frmFilter.controls['policyType'].setValue('Todos');
    this.clear();
    this.page = 1;
    this.setFilters(f);
    this.getPeriods(null, null, this.fechaInicioVigencia, this.fechaFinVigencia, this.isAdditional);
    this.onCancelSelectMode();
  }

  private _mapChangedFrmFilter(f: any): void {
    this.policyType = f.policyType === 'Todos' ? '' : f.policyType;
    this.clear();
    this.onCancelSelectMode();
  }

  private _downloadConstancy(): void {
    const pathReq = { numeroPoliza: this.policyNumber };
    const queryReq = {
      codigoApp: APPLICATION_CODE,
      fechaFinVigencia: this.fechaFinVigencia,
      fechaInicioVigencia: this.fechaInicioVigencia
    };
    const centroTrabajo = this.formAditional.controls['jobCenter'].value;
    const bodyReq = {
      seleccionadoTodos: this.isSelectAll,
      listaSeleccionada: this.sctrService.getInsuredConstancyList(),
      polizas: this.period.payload.polizas,
      informacionOperacion: {
        ...(centroTrabajo && { centroTrabajo })
      }
    };
    this.isDownloading = true;
    this.pdfSub = this._Autoservicios.DescargarConstanciaManualSCTR(pathReq, queryReq, bodyReq).subscribe(
      response => {
        try {
          const fileName = `Constancia ${this.sctr.numeroPoliza}`;
          this.fileUtil.download(response.base64, 'pdf', fileName);
          this._downloadComplete();
        } catch (error) {}
      },
      () => (this.isDownloading = false),
      () => {
        this.isDownloading = false;
        this.pdfSub.unsubscribe();
      }
    );
  }

  private _downloadComplete(): void {
    this.mfModal && this.mfModal.close();
    this.formAditional.controls['jobCenter'].setValue('');
    this.onCancelSelectMode();
  }
}
