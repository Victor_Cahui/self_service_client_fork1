import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { BaseSctrPeriods } from '@mx/components/sctr/card-sctr-periods/base-sctr-periods';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { HeaderHelperService, IHeader } from '@mx/services/general/header-helper.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { APPLICATION_CODE, FORMAT_DATE_DDMMYYYY } from '@mx/settings/constants/general-values';
import { CURRENT_SCTR_PERIOD } from '@mx/settings/constants/key-values';
import {
  ISctrByClientResponse,
  ISctrPeriodInfoCard,
  ISctrPeriodResponse
} from '@mx/statemanagement/models/policy.interface';

export abstract class SctrPeriodOperationBase extends BaseSctrPeriods {
  sctr: ISctrByClientResponse;
  period: ISctrPeriodResponse;
  periodInfoCard: ISctrPeriodInfoCard;
  header: IHeader;

  showSummary: boolean;
  canContinue: boolean;
  movement: number;
  mpKeys: any;

  isDeclaration: boolean;

  constructor(
    protected datePipe: DatePipe,
    protected activatedRoute: ActivatedRoute,
    protected authService: AuthService,
    protected _Autoservicios: Autoservicios,
    protected policiesInfoService: PoliciesInfoService,
    protected headerHelperService: HeaderHelperService,
    protected policiesService: PoliciesService,
    protected storageService: StorageService,
    protected gaService?: GAService
  ) {
    super(datePipe, activatedRoute, authService, _Autoservicios, gaService);
    this.periodInfoCard = { title: '', insuredGroup: '', perdiod: '', start: '', end: '' };
  }

  loadInfoPeriodSctr(): void {
    const period = this.policiesInfoService.getClientSctrPeriod();
    period ? this.configPeriod(period) : this.getPeriods();
  }

  doActionsAfterPeriodsAsyncEnds(): void {
    const currentPeriod = this.storageService.getItem(CURRENT_SCTR_PERIOD);
    if (currentPeriod) {
      const period = this.periodList.find(p => p.payload.fechaPeriodo === currentPeriod).payload;
      this.configPeriod(period);
    }
  }

  configPeriod(period: ISctrPeriodResponse): void {
    const startDate = this.datePipe.transform(period.fechaInicioVigencia, FORMAT_DATE_DDMMYYYY);
    const endDate = this.datePipe.transform(period.fechaFinVigencia, FORMAT_DATE_DDMMYYYY);
    const insuredGroup = period.colectivoAsegurado;
    const perdiod = `${this.isDeclaration ? this.lang.DeclarationPeriod : this.lang.InclusionPeriod}: ${
      period.fechaPeriodo
    }`;
    const start = `${this.lang.StartPeriod}: ${startDate}`;
    const end = `${this.lang.EndPeriod}: ${endDate}`;
    this.periodInfoCard = { insuredGroup, perdiod, start, end };
    this.period = period;
  }

  loadInfoDetailSctr(): void {
    const clientPolicy = this.policiesInfoService.getClientSctrPolicy(this.policyNumber);
    if (!clientPolicy) {
      this.policiesService
        .sctrSearchByClient({
          codigoApp: APPLICATION_CODE,
          numeroPoliza: this.policyNumber
        })
        .subscribe(response => {
          if (response && response.length > 0) {
            const clientPolicyResponse = response[0];
            this.policiesInfoService.setClientSctrPolicyResponse(clientPolicyResponse);
            this.mpKeys = this.policiesInfoService.getMPKeys();
            this.configSctr(clientPolicyResponse);
          }
        });
    } else {
      this.configSctr(clientPolicy);
    }
  }

  configSctr(sctr: ISctrByClientResponse): void {
    this.periodInfoCard.title = `${sctr.descripcionPoliza}: ${sctr.numeroPoliza}`;
    this.headerHelperService.set({
      subTitle: sctr.descripcionPoliza.toUpperCase(),
      title: this.isDeclaration ? this.lang.Declaration.toUpperCase() : this.lang.Inclusion.toUpperCase(),
      icon: '',
      back: true
    });
    this.header.subTitle = sctr.descripcionPoliza.toUpperCase();
    this.sctr = sctr;
  }

  doSummary(): void {
    this.showSummary = true;
  }
}
