import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SctrPeriodOperationConfirmComponent } from './sctr-period-operation-confirm.component';

const routes: Routes = [
  {
    path: '',
    component: SctrPeriodOperationConfirmComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SctrPeriodOperationConfirmRoutingModule {}
