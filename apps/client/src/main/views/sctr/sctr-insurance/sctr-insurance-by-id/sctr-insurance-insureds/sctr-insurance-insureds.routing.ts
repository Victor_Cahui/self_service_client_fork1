import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SctrInsuranceInsuredsComponent } from './sctr-insurance-insureds.component';

const routes: Routes = [
  {
    path: '',
    component: SctrInsuranceInsuredsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SctrInsuranceInsuredsRoutingModule {}
