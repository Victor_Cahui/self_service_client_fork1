import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { SctrService } from '@mx/services/sctr.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { ICardPeriodPreview } from '@mx/statemanagement/models/sctr.interface';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';

@Component({
  selector: 'client-sctr-period-payroll-insured',
  templateUrl: './sctr-period-payroll-insured.component.html'
})
export class SctrPeriodPayrollInsuredComponent extends GaUnsubscribeBase implements OnInit, OnChanges {
  @Input() insured: any;
  @Input() isSelectMode: boolean;
  @Input() isSelectAll: boolean;
  @Input() policyNumber: string;
  @Input() period: ICardPeriodPreview;

  @Output() doEdit: EventEmitter<boolean> = new EventEmitter();
  @Output() doSelect: EventEmitter<{ codAsegurado: string; checked: boolean }> = new EventEmitter();

  auth: IdDocument;
  isUpdating: boolean;
  isEditMode: boolean;
  isFormExtended: boolean;
  checked = true;
  name: string;
  symbol: string;

  frmUpdate: FormGroup;

  constructor(
    private readonly sctrService: SctrService,
    private readonly authService: AuthService,
    private readonly _Autoservicios: Autoservicios,
    private readonly formBuilder: FormBuilder,
    protected gaService: GAService
  ) {
    super(gaService);
    this.auth = this.authService.retrieveEntity();
  }

  ngOnInit(): void {
    this.setForm();
    this.symbol = this.insured ? `${StringUtil.getMoneyDescription(this.insured.codigoMoneda)} ` : '';
    this.name =
      this.insured.nombreCompleto ||
      `${this.insured.apellidoPaterno} ${this.insured.apellidoMaterno} ${this.insured.nombres}`;

    const listed = this.sctrService.getInsuredConstancyList().includes(this.insured.codAsegurado);
    listed && (this.checked = !this.isSelectAll);
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { isSelectAll } = changes;
    isSelectAll && (this.checked = this.isSelectAll);
  }

  setForm(): void {
    const i = this.insured;
    this.isFormExtended = !!(i.apellidoPaterno && i.apellidoMaterno && i.nombres);

    this.frmUpdate = this.formBuilder.group({
      firstName: [this.isFormExtended ? i.apellidoPaterno : i.nombreCompleto, Validators.required]
    });

    if (this.isFormExtended) {
      this.frmUpdate.addControl('lastName', new FormControl(i.apellidoMaterno, Validators.required));
      this.frmUpdate.addControl('names', new FormControl(i.nombres, Validators.required));
    }
  }

  onEdit(): void {
    if (this.isSelectMode) {
      return void 0;
    }

    this.setForm();

    this.isEditMode = true;
    this.doEdit.emit(true);
  }

  onCancel(): void {
    if (this.isUpdating) {
      return void 0;
    }

    this.isEditMode = false;
    this.doEdit.emit(false);
  }

  onCheck(): void {
    this.checked = !this.checked;
    this.doSelect.emit({ codAsegurado: this.insured.codAsegurado, checked: this.checked });
  }

  onUpdate(): void {
    if (this.frmUpdate.invalid) {
      return void 0;
    }

    this.isUpdating = true;

    const pathReq = {
      numeroPoliza: this.policyNumber,
      codAsegurado: this.insured.codAsegurado
    };

    const queryReq = {
      codigoApp: APPLICATION_CODE,
      tipoDocumento: this.auth.type,
      documento: this.auth.number,
      fechaInicioVigencia: this.period.payload.fechaInicioVigencia,
      fechaFinVigencia: this.period.payload.fechaFinVigencia
    };

    const insuredUpdate = {
      ...this.insured,
      ...(this.isFormExtended && { apellidoPaterno: this.frmUpdate.controls['firstName'].value }),
      ...(this.isFormExtended && { apellidoMaterno: this.frmUpdate.controls['lastName'].value }),
      ...(this.isFormExtended && { nombres: this.frmUpdate.controls['names'].value }),
      ...(!this.isFormExtended && { nombreCompleto: this.frmUpdate.controls['firstName'].value })
    };

    const bodyReq = {
      asegurado: insuredUpdate,
      polizas: this.period.payload.polizas
    };

    this._Autoservicios.ActualizarAseguradoSctr(pathReq, queryReq, bodyReq).subscribe(
      response => {
        if (!response) {
          return void 0;
        }

        this.isUpdating = this.isEditMode = false;
        this.doEdit.emit(false);

        const firstName = this.frmUpdate.controls['firstName'].value;
        const lastName = this.isFormExtended && this.frmUpdate.controls['lastName'].value;
        const names = this.isFormExtended && this.frmUpdate.controls['names'].value;

        if (this.isFormExtended) {
          this.insured.apellidoPaterno = firstName;
          this.insured.apellidoMaterno = lastName;
          this.insured.nombres = names;
          this.name = `${firstName} ${lastName} ${names}`;
        } else {
          this.insured.nombreCompleto = this.name = firstName;
        }
      },
      () => (this.isUpdating = false),
      () => (this.isUpdating = false)
    );
  }
}
