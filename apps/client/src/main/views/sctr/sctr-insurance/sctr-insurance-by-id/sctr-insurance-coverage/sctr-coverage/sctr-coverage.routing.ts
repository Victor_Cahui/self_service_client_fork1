import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SctrCoverageComponent } from './sctr-coverage.component';

const routes: Routes = [
  {
    path: '',
    component: SctrCoverageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SctrCoverageRouting {}
