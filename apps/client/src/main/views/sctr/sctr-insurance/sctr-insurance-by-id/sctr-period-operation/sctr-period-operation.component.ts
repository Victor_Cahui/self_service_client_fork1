import { DatePipe, DOCUMENT } from '@angular/common';
import { AfterViewInit, Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { CURRENT_SCTR_PERIOD } from '@mx/settings/constants/key-values';
import { SCTR_PERIOD_OPERATION_HEADER } from '@mx/settings/constants/router-titles';
import { SctrPeriodOperationBase } from './sctr-period-operation-base';

@Component({
  selector: 'client-sctr-period-operation',
  templateUrl: './sctr-period-operation.component.html',
  providers: [DatePipe]
})
export class SctrPeriodOperationComponent extends SctrPeriodOperationBase implements AfterViewInit, OnDestroy, OnInit {
  constructor(
    @Inject(DOCUMENT) private readonly document: Document,
    private readonly router: Router,
    protected datePipe: DatePipe,
    protected activatedRoute: ActivatedRoute,
    protected authService: AuthService,
    protected _Autoservicios: Autoservicios,
    protected policiesInfoService: PoliciesInfoService,
    protected headerHelperService: HeaderHelperService,
    protected policiesService: PoliciesService,
    protected storageService: StorageService,
    protected gaService: GAService
  ) {
    super(
      datePipe,
      activatedRoute,
      authService,
      _Autoservicios,
      policiesInfoService,
      headerHelperService,
      policiesService,
      storageService,
      gaService
    );
    this.auth = this.authService.retrieveEntity();
  }

  ngOnInit(): void {
    this.isDeclaration = this.router.url.split('/').slice(-1)[0] === this.lang.DeclarationPath;
    this.header = SCTR_PERIOD_OPERATION_HEADER;
    this.loadInfoPeriodSctr();
    this.loadInfoDetailSctr();
  }

  ngAfterViewInit(): void {
    const tapTop = this.document.getElementsByTagName('mf-tab-top')[0];
    tapTop.classList.add('g-none');
  }

  ngOnDestroy(): void {
    const tapTop = this.document.getElementsByTagName('mf-tab-top')[0];
    tapTop.classList.remove('g-none');

    this.storageService.removeItem(CURRENT_SCTR_PERIOD);
  }
}
