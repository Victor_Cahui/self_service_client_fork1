import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardContentModule } from '@mx/components/shared/card-content/card-content.module';
import { SctrCoverageExclusionsComponent } from '../sctr-coverage-exclusions/sctr-coverage-exclusions.component';
import { SctrCoverageExclusionsRouting } from '../sctr-coverage-exclusions/sctr-coverage-exclusions.routing';

@NgModule({
  imports: [CommonModule, SctrCoverageExclusionsRouting, CardContentModule],
  declarations: [SctrCoverageExclusionsComponent]
})
export class SctrCoverageExclusionsModule {}
