import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SctrInsuranceRecordsComponent } from './sctr-insurance-records.component';

const routes: Routes = [
  {
    path: '',
    component: SctrInsuranceRecordsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SctrInsuranceRecordsRoutingModule {}
