import { Component, HostBinding } from '@angular/core';
import { FOIL_ATTACHED } from '@mx/settings/lang/sctr.lang';

@Component({
  selector: 'client-sctr-foil-attached-component',
  templateUrl: './sctr-foil-attached.component.html'
})
export class SctrFoilAttachedComponent {
  @HostBinding('class') class = 'w-100';

  content = { title: FOIL_ATTACHED.title };
}
