import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SctrInsuranceCoverageRedirectionGuard } from '@mx/guards/sctr-insurance-coverage.guard';
import { SctrInsuranceCoverageComponent } from './sctr-insurance-coverage.component';

const routes: Routes = [
  {
    path: '',
    component: SctrInsuranceCoverageComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        canActivate: [SctrInsuranceCoverageRedirectionGuard]
      },
      {
        path: 'survival',
        loadChildren: './sctr-coverage-survival/sctr-coverage-survival.module#SctrCoverageSurvivalModule'
      },
      {
        path: 'funeral',
        loadChildren: './sctr-coverage-funeral/sctr-coverage-funeral.module#SctrCoverageFuneralModule'
      },
      {
        path: 'exclusions',
        loadChildren: './sctr-coverage-exclusions/sctr-coverage-exclusions.module#SctrCoverageExclusionsModule'
      },
      {
        path: 'exclusions-double-emission',
        loadChildren: './sctr-coverage-exclusions/sctr-coverage-exclusions.module#SctrCoverageExclusionsModule'
      },
      {
        path: 'foil-attached',
        loadChildren: './sctr-foil-attached/sctr-foil-attached.module#SctrFoilAttachedModule'
      },
      {
        path: ':typeCoverage',
        loadChildren: './sctr-coverage/sctr-coverage.module#SctrCoverageModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SctrInsuranceCoverageRoutingModule {}
