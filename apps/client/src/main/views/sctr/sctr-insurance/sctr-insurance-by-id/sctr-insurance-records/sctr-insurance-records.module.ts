import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ItemSctrRecordModule } from '@mx/components/sctr/item-sctr-record/item-sctr-record.module';
import { InputSearchModule } from '@mx/components/shared/input-search/input-search.module';
import { ItemNotFoundModule } from '@mx/components/shared/item-not-found/item-not-found.module';
import { MfButtonModule, MfCardModule, MfPaginatorModule, MfSelectModule, MfShowErrorsModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { SctrInsuranceRecordsComponent } from './sctr-insurance-records.component';
import { SctrInsuranceRecordsRoutingModule } from './sctr-insurance-records.routing';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SctrInsuranceRecordsRoutingModule,
    MfPaginatorModule,
    MfSelectModule,
    MfLoaderModule,
    MfCardModule,
    ItemSctrRecordModule,
    InputSearchModule,
    MfButtonModule,
    MfShowErrorsModule,
    ItemNotFoundModule
  ],
  declarations: [SctrInsuranceRecordsComponent]
})
export class SctrInsuranceRecordsModule {}
