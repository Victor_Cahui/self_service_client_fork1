import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SctrPeriodPayrollComponent } from './sctr-period-payroll.component';

const routes: Routes = [
  {
    path: '',
    component: SctrPeriodPayrollComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SctrPeriodPayrollRoutingModule {}
