import { DOCUMENT } from '@angular/common';
import { AfterViewInit, Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { HeaderHelperService, IHeader } from '@mx/services/general/header-helper.service';
import { COMPLETE_ICON } from '@mx/settings/constants/images-values';
import { PERIODS_LANAG } from '@mx/settings/lang/sctr.lang';
import { Content } from '@mx/statemanagement/models/general.models';

@Component({
  selector: 'client-sctr-period-operation-confirm',
  templateUrl: './sctr-period-operation-confirm.component.html'
})
export class SctrPeriodOperationConfirmComponent implements AfterViewInit, OnDestroy, OnInit {
  lang = PERIODS_LANAG;
  header: IHeader;
  content: Content;
  policyNumber: string;
  operation: any;
  isDeclaration: boolean;

  constructor(
    @Inject(DOCUMENT) private readonly document: Document,
    protected router: Router,
    protected activatedRoute: ActivatedRoute,
    protected storageService: StorageService,
    protected headerHelperService: HeaderHelperService
  ) {
    this.header = { title: '', subTitle: '', back: false };
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => (this.policyNumber = params.policyNumber));
    this.isDeclaration = this.router.url.split('/').slice(-2, -1)[0] === this.lang.DeclarationPath;
    this.setHeader();
    this.setContent();
  }

  ngAfterViewInit(): void {
    const tapTop = this.document.getElementsByTagName('mf-tab-top')[0];
    tapTop.classList.add('g-none');
  }

  ngOnDestroy(): void {
    const tapTop = this.document.getElementsByTagName('mf-tab-top')[0];
    tapTop.classList.remove('g-none');

    this.storageService.removeItem('SCTR_OPERATION_SUCCESS');
  }

  setHeader(): void {
    this.header.subTitle = 'SCTR';
    this.header.title = this.isDeclaration ? this.lang.Declaration.toUpperCase() : this.lang.Inclusion.toUpperCase();
    this.headerHelperService.set(this.header);
  }

  setContent(): void {
    this.operation = JSON.parse(this.storageService.getItem('SCTR_OPERATION_SUCCESS')) || {};
    const title = this.isDeclaration ? this.lang.DeclarationIssued : this.lang.InclusionIssued;
    const content = this.isDeclaration ? this.lang.DeclarationSuccess : this.lang.InclusionSuccess;
    const emailLabel = this.isDeclaration ? this.lang.WorkersDeclared : this.lang.WorkersIncluded;
    const action = { routeLink: `/sctr/sctr-insurance/${this.policyNumber}/detail` };

    this.content = {
      title,
      content,
      button: this.lang.GotoPolicyDetail,
      icon: COMPLETE_ICON,
      amountLabel: this.lang.ProofNumber,
      amountText: this.operation.numConstancia,
      emailLabel,
      emailText: this.operation.totalTrabajadores,
      dateLabel: this.lang.OperationDate,
      dateText: this.operation.fechaOperacion,
      action
    };
  }
}
