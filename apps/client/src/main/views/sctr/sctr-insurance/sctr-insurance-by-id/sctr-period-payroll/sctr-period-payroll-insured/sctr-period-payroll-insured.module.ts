import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaymentButtonModule } from '@mx/components/payment/payment-button/payment-button.module';
import { IconPaymentTypeModule } from '@mx/components/shared/icon-payment-type/icon-payment-type.module';
import { DirectivesModule, MfCardModule, MfCheckboxModule, MfInputModule } from '@mx/core/ui';

import { SctrPeriodPayrollInsuredComponent } from './sctr-period-payroll-insured.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DirectivesModule,
    MfCardModule,
    MfInputModule,
    MfCheckboxModule,
    IconPaymentTypeModule,
    PaymentButtonModule
  ],
  declarations: [SctrPeriodPayrollInsuredComponent],
  exports: [SctrPeriodPayrollInsuredComponent]
})
export class SctrPeriodPayrollInsuredModule {}
