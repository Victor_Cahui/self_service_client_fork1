import { DOCUMENT } from '@angular/common';
import { FormGroup } from '@angular/forms';
import { filter } from 'rxjs/internal/operators/filter';
import { skip } from 'rxjs/internal/operators/skip';

import { AfterViewInit, Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { everyPropsAreTruthy } from '@mx/core/shared/helpers';
import { FrmProvider } from '@mx/core/shared/providers/services';
import { ConfigCbo } from '@mx/core/ui/lib/components/forms/select';
import { AuthService } from '@mx/services/auth/auth.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { SctrService } from '@mx/services/sctr.service';
import { APPLICATION_CODE, GENERAL_MAX_LENGTH, GENERAL_MIN_LENGTH } from '@mx/settings/constants/general-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import { RECORD_LANG } from '@mx/settings/lang/sctr.lang';
import { ISctrByClientResponse } from '@mx/statemanagement/models/policy.interface';
import { IRecordResponse, IRecordsRequest } from '@mx/statemanagement/models/sctr.interface';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-sctr-insurance-records',
  templateUrl: './sctr-insurance-records.component.html'
})
export class SctrInsuranceRecordsComponent implements AfterViewInit, OnDestroy, OnInit {
  @ViewChild('inputSearch') public inputSearch: any;

  page = 1;
  itemsPerPage = 10;
  maxLengthSearch = GENERAL_MAX_LENGTH.searchRecord;
  minLengthSearch = GENERAL_MIN_LENGTH.searchRecord;
  searchText: string;
  policyNumber: string;
  auth: IdDocument;
  title: string;
  generalLang = GeneralLang;
  lang = RECORD_LANG;
  policyLang = PolicyLang;
  showLoading = true;
  isLoadingFiltersVisible = true;
  getRecordsSub: Subscription;
  records: Array<IRecordResponse> = [];
  total: number;
  totalPluralized: string;
  mpKeys: any;
  configCbo: ConfigCbo;
  fechaInicioVigencia: string;
  fechaFinVigencia: string;
  tipoMovimiento: string;
  private _isEventFromFilter: boolean;

  frm: FormGroup;

  constructor(
    @Inject(DOCUMENT) private readonly document: Document,
    private readonly activatedRoute: ActivatedRoute,
    private readonly policiesInfoService: PoliciesInfoService,
    private readonly headerHelperService: HeaderHelperService,
    private readonly policiesService: PoliciesService,
    private readonly authService: AuthService,
    private readonly sctrService: SctrService,
    private readonly _FrmProvider: FrmProvider
  ) {
    this.auth = this.authService.retrieveEntity();
  }

  ngOnInit(): void {
    this.setForm();
    this.activatedRoute.params.subscribe(params => {
      this.policyNumber = params.policyNumber;
      this.loadInfoDetailSctr();
    });

    this.frm.valueChanges
      .pipe(
        filter(everyPropsAreTruthy),
        // HACK: el select al inicio dispara 2 veces el seteo del frm
        skip(1)
      )
      .subscribe(this._mapChangedFrm.bind(this));
  }

  private _mapChangedFrm(f: any): void {
    this._isEventFromFilter = true;
    this.clear();
    this.page = 1;
    this.setFilters(f);
    this.getRecords();
  }

  setFilters(f: any): void {
    const isDefaultVigency = f.vigencyMonth === 'default';
    const isDefaultRecord = f.recordType === 'default';
    this.fechaInicioVigencia = !isDefaultVigency ? f.vigencyMonth.split('|')[0] : null;
    this.fechaFinVigencia = !isDefaultVigency ? f.vigencyMonth.split('|')[1] : null;
    this.tipoMovimiento = !isDefaultRecord ? f.recordType : null;
  }

  getCboLoadingState(e: any): void {
    this.isLoadingFiltersVisible = e.isLoadingVisible;
  }

  ngAfterViewInit(): void {
    const tapTop = this.document.getElementsByTagName('mf-tab-top')[0];
    tapTop.classList.add('g-none');
  }

  ngOnDestroy(): void {
    const tapTop = this.document.getElementsByTagName('mf-tab-top')[0];
    tapTop.classList.remove('g-none');
    this.getRecordsSub && this.getRecordsSub.unsubscribe();
  }

  loadInfoDetailSctr(): void {
    const clientPolicy = this.policiesInfoService.getClientSctrPolicy(this.policyNumber);
    if (!clientPolicy) {
      this.policiesService
        .sctrSearchByClient({
          codigoApp: APPLICATION_CODE,
          numeroPoliza: this.policyNumber
        })
        .subscribe(response => {
          if (response && response.length > 0) {
            const clientPolicyResponse = response[0];
            this.policiesInfoService.setClientSctrPolicyResponse(clientPolicyResponse);
            this.configView(clientPolicyResponse);
          }
        });
    } else {
      this.configView(clientPolicy);
    }
  }

  configView(sctr: ISctrByClientResponse): void {
    this.title = `${this.generalLang.Texts.PolicyNumber} ${sctr.numeroPoliza}`;
    this.mpKeys = this.policiesInfoService.getMPKeys();
    this.headerHelperService.set({
      subTitle: sctr.descripcionPoliza.toUpperCase(),
      title: RECORD_LANG.MyRecords.toUpperCase(),
      icon: '',
      back: true
    });
  }

  getRecords(): void {
    this.showLoading = true;
    this._isEventFromFilter = false;
    const params = this.getParams();
    this.getRecordsSub = this.sctrService.getRecords(params).subscribe(
      response => {
        this.records = response.constancias || [];
        this.total = response.totalRegistros;
        this.totalPluralized = `${this.total} ${
          this.total === 1 ? PolicyLang.Labels.Constancy : PolicyLang.Labels.Constancies
        }`;
        this.showLoading = false;
      },
      err => {
        this.getRecordsSub.unsubscribe();
        this.showLoading = false;
      }
    );
  }

  getParams(): IRecordsRequest {
    const params: IRecordsRequest = {
      tipoDocumento: this.auth.type,
      documento: this.auth.number,
      codigoApp: APPLICATION_CODE,
      numeroPoliza: this.policyNumber,
      registros: this.itemsPerPage,
      pagina: this.page
    };
    const numeroConstancia = (this.searchText || '').replace(/\//g, '_');
    const filters: any = {
      ...(numeroConstancia && { numeroConstancia }),
      ...(this.fechaInicioVigencia && { fechaInicioVigencia: this.fechaInicioVigencia }),
      ...(this.fechaFinVigencia && { fechaFinVigencia: this.fechaFinVigencia }),
      ...(this.tipoMovimiento && { tipoMovimiento: this.tipoMovimiento })
    };

    return { ...params, ...filters };
  }

  pageChange(page: number): void {
    this.page = page;
    this.getRecords();
  }

  itemsPerPageChange(itemsPerPage?: number): void {
    this.page = 1;
    this.itemsPerPage = itemsPerPage || this.itemsPerPage;
    this.getRecords();
  }

  setForm(): void {
    this.configCbo = { canAutoSelectFirstOpt: true };
    this.frm = this._FrmProvider.SctrRecordComponent();
  }

  search(): void {
    if (this.frm.valid) {
      this.itemsPerPageChange();
    }
  }

  onSearch(text: string): void {
    this.searchText = text;
  }

  clear(): void {
    this.searchText = '';
    this.inputSearch.clearText();
    this._isEventFromFilter || this.itemsPerPageChange();
  }
}
