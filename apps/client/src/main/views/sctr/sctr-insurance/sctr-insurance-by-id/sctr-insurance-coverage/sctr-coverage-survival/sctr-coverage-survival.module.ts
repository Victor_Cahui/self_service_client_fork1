import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardContentModule } from '@mx/components/shared/card-content/card-content.module';
import { SctrCoverageSurvivalComponent } from '../sctr-coverage-survival/sctr-coverage-survival.component';
import { SctrCoverageSurvivalRouting } from '../sctr-coverage-survival/sctr-coverage-survival.routing';

@NgModule({
  imports: [CommonModule, SctrCoverageSurvivalRouting, CardContentModule],
  declarations: [SctrCoverageSurvivalComponent]
})
export class SctrCoverageSurvivalModule {}
