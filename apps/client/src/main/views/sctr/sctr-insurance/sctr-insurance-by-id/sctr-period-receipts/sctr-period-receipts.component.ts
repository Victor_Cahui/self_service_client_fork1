import { CurrencyPipe, DatePipe, DOCUMENT } from '@angular/common';
import { AfterViewInit, Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TRAFFIC_LIGHT_COLOR } from '@mx/components/shared/utils/policy';
import { isExpired } from '@mx/core/shared/helpers/util/date';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { Autoservicios, FrmProvider } from '@mx/core/shared/providers/services';
import { ConfigCbo } from '@mx/core/ui/lib/components/forms/select';
import { AuthService } from '@mx/services/auth/auth.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { MFP_Cuotas_de_Poliza_Pagar_28C } from '@mx/settings/constants/events.analytics';
import { APPLICATION_CODE, COLOR_STATUS, FORMAT_DATE_DDMMYYYY } from '@mx/settings/constants/general-values';
import { SCTR_PERIOD_RECEIPTS_HEADER } from '@mx/settings/constants/router-titles';
import { IPaymentQuoteCard } from '@mx/statemanagement/models/payment.models';
import { SctrPeriodReceiptsBase } from './sctr-period-receipts-base';

@Component({
  selector: 'client-sctr-period-receipts',
  templateUrl: './sctr-period-receipts.component.html',
  providers: [CurrencyPipe, DatePipe]
})
export class SctrPeriodReceiptsComponent extends SctrPeriodReceiptsBase implements AfterViewInit, OnDestroy, OnInit {
  @ViewChild('inputSearch') public inputSearch: any;

  situationType: string;
  withoutReceipt: string;
  showReceipts: boolean;
  receipts: Array<any> = [];
  configCbo: ConfigCbo;

  constructor(
    @Inject(DOCUMENT) protected document: Document,
    protected datePipe: DatePipe,
    protected currencyPipe: CurrencyPipe,
    protected activatedRoute: ActivatedRoute,
    protected authService: AuthService,
    protected _Autoservicios: Autoservicios,
    protected storageService: StorageService,
    protected gaService: GAService,
    protected policiesInfoService: PoliciesInfoService,
    protected policiesService: PoliciesService,
    protected headerHelperService: HeaderHelperService,
    private readonly _FrmProvider: FrmProvider
  ) {
    super(
      document,
      datePipe,
      activatedRoute,
      authService,
      _Autoservicios,
      policiesInfoService,
      headerHelperService,
      policiesService,
      storageService,
      gaService
    );
    this.policyNumber = this.activatedRoute.snapshot.params['policyNumber'];
    this.auth = this.authService.retrieveEntity();
    this.ga = [MFP_Cuotas_de_Poliza_Pagar_28C('SCTR')];
  }

  ngOnInit(): void {
    this.header = SCTR_PERIOD_RECEIPTS_HEADER;
    this.loadInfoDetailSctr();

    this.setForms();
    this.frmPeriod.valueChanges.subscribe(this._mapChangedFrmPeriod.bind(this));
    this.frmFilter.valueChanges.subscribe(this._mapChangedFrmFilter.bind(this));
  }

  ngAfterViewInit(): void {
    this.hideTabTop();
  }

  ngOnDestroy(): void {
    this.showTabTop();
  }

  getPeriodReceipts(): void {
    this.receipts = [];
    this.showLoading = true;
    this._isEventFromFilter = false;
    const pathReq = {
      numeroPoliza: this.policyNumber
    };

    const fechaFinVigencia = this.fechaFinVigencia;
    const fechaInicioVigencia = this.fechaInicioVigencia;
    const tipoSituacion = this.situationType;

    const queryReq: any = {
      codigoApp: APPLICATION_CODE,
      tipoDocumento: this.auth.type,
      documento: this.auth.number,
      registros: this.itemsPerPage,
      pagina: this.page,
      ...(fechaFinVigencia && { fechaFinVigencia }),
      ...(fechaInicioVigencia && { fechaInicioVigencia }),
      ...(tipoSituacion && { tipoSituacion })
    };

    const bodyReq = {
      ...(this.period && { polizas: this.period.payload.polizas || [] })
    };

    this._Autoservicios.ListarRecibosPorPeriodo(pathReq, queryReq, bodyReq).subscribe(
      (response: any) => {
        if (response) {
          this.receipts = response.listaResultados.map(this._mapReceipt.bind(this));
          this.total = response.totalRegistros;
          this.showLoading = false;
        }
      },
      () => (this.showLoading = false),
      () => (this.showLoading = false)
    );
  }

  pageChange(page: number): void {
    this.page = page;
    this.getPeriodReceipts();
  }

  itemsPerPageChange(itemsPerPage?: number): void {
    this.page = 1;
    this.itemsPerPage = itemsPerPage || this.itemsPerPage;
    this.getPeriodReceipts();
  }

  setForms(): void {
    this.configCbo = { canAutoSelectFirstOpt: false };
    this.frmPeriod = this._FrmProvider.SctrPeriodReceiptsComponent();
    this.frmFilter = this._FrmProvider.SctrPeriodReceiptsFilter();
  }

  setFilters(f: any): void {
    this.showFilters = true;
    this.showReceipts = true;
    const periodArr = f.periodMonth.split('|') || [];
    this.fechaInicioVigencia = periodArr[0] || '';
    this.fechaFinVigencia = periodArr[1] || '';
    this.periodType = periodArr[2] || '';
    this.isAdditional = this.periodType === 'MA';
  }

  doActionsAfterPeriodsAsyncEnds(): void {
    this.period = this.periodList.length ? this.periodList[0] : null;
    this.period &&
      (this.withoutReceipt = this.period.hasDeclaration
        ? 'Este periodo no tiene recibos generados.'
        : 'No se encontró ningun recibo.');
    this.showLoadingPeriod = false;
    this.getPeriodReceipts();
  }

  getCboLoadingState(e: any): void {
    this.isLoadingFilterVisible = e.isLoadingVisible;
    this.verifySelected();
  }

  private _mapChangedFrmPeriod(f: any): void {
    this._isEventFromFilter = true;
    this.showLoadingPeriod = true;
    this.frmFilter.controls['situationType'].setValue('Todos');
    this.page = 1;
    this.setFilters(f);
    this.getPeriods(null, null, this.fechaInicioVigencia, this.fechaFinVigencia, this.isAdditional);
  }

  private _mapChangedFrmFilter(f: any): void {
    this.situationType = f.situationType === 'Todos' ? '' : f.situationType;
    this._isEventFromFilter || this.itemsPerPageChange();
  }

  private _mapReceipt(receipt: any): Array<any> {
    const paymentReceipt = this._mapPaymentReceipt(receipt);

    return {
      ...receipt,
      paymentReceipt
    };
  }

  private _mapPaymentReceipt(payment: any): IPaymentQuoteCard {
    const semaforo = TRAFFIC_LIGHT_COLOR[payment.semaforo];
    const symbol = `${StringUtil.getMoneyDescription(payment.codigoMoneda)} `;
    const expired = isExpired(payment.fechaFinVigencia);
    const expiredDate = this.datePipe.transform(payment.fechaExpiracion, FORMAT_DATE_DDMMYYYY);
    const totalAmount = this.currencyPipe.transform(payment.montoTotal, symbol);

    return {
      billNumber: payment.numRecibo,
      bulletColor: COLOR_STATUS[semaforo],
      currencySymbol: symbol,
      expired,
      expiredDate,
      month: payment.fechaFinVigencia.substr(0, 7),
      policy: payment.descripcion,
      policyNumber: payment.numeroPoliza,
      policyStatus: payment.estado,
      policyType: payment.tipoPoliza,
      semaforo,
      total: payment.montoTotal,
      totalAmount,
      response: { ...payment }
    } as IPaymentQuoteCard;
  }
}
