import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CardSctrPeriodItemModule } from '@mx/components/shared/card-sctr-period-item/card-sctr-period-item.module';
import { InputSearchModule } from '@mx/components/shared/input-search/input-search.module';
import { ItemNotFoundModule } from '@mx/components/shared/item-not-found/item-not-found.module';
import {
  MfButtonModule,
  MfCardModule,
  MfInputModule,
  MfPaginatorModule,
  MfSelectModule,
  MfShowErrorsModule
} from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { MfModalModule } from '@mx/core/ui/lib/components/modals/modal';
import { SctrPeriodPayrollInsuredModule } from './sctr-period-payroll-insured/sctr-period-payroll-insured.module';
import { SctrPeriodPayrollSelectAllModule } from './sctr-period-payroll-select-all/sctr-period-payroll-select-all.module';

import { SctrPeriodPayrollComponent } from './sctr-period-payroll.component';
import { SctrPeriodPayrollRoutingModule } from './sctr-period-payroll.routing';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    InputSearchModule,
    MfButtonModule,
    MfCardModule,
    MfInputModule,
    MfPaginatorModule,
    MfSelectModule,
    MfShowErrorsModule,
    CardSctrPeriodItemModule,
    SctrPeriodPayrollRoutingModule,
    SctrPeriodPayrollInsuredModule,
    SctrPeriodPayrollSelectAllModule,
    ItemNotFoundModule,
    MfLoaderModule,
    MfModalModule
  ],
  exports: [],
  declarations: [SctrPeriodPayrollComponent]
})
export class SctrPeriodPayrollModule {}
