import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfTabTopModule } from '@mx/core/ui/lib/components/tabs/tabs-top/tab-top.module';
import { SctrInsuranceByIdComponent } from './sctr-insurance-by-id.component';
import { SctrInsuranceByIdRoutingModule } from './sctr-insurance-by-id.routing';

@NgModule({
  imports: [CommonModule, SctrInsuranceByIdRoutingModule, MfTabTopModule],
  declarations: [SctrInsuranceByIdComponent],
  exports: [SctrInsuranceByIdComponent]
})
export class SctrInsuranceByIdModule {}
