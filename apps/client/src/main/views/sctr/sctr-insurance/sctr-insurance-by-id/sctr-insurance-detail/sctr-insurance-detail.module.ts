import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardWhatYouWantToDoModule } from '@mx/components';
import { CardPaymentPolicyModule } from '@mx/components/payment/card-payment-policy/card-payment-policy.module';
import { CardContractingInfoModule } from '@mx/components/policy/card-contracting-info/card-contracting-info.module';
import { CardPolicyBasicInfoModule } from '@mx/components/policy/card-policy-basic-info/card-policy-basic-info.module';
import { CardPolicyHousesModule } from '@mx/components/policy/card-policy-houses/card-policy-houses.module';
import { CardSctrPeriodsModule } from '@mx/components/sctr/card-sctr-periods/card-sctr-periods.module';
import { CardSctrRecordListModule } from '@mx/components/sctr/card-sctr-record-list/card-sctr-record-list.module';
import { CardDetailPolicyModule } from '@mx/components/shared/card-detail-policy/card-detail-policy.module';
import { ItemNotFoundModule } from '@mx/components/shared/item-not-found/item-not-found.module';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { SctrInsuranceDetailComponent } from './sctr-insurance-detail.component';
import { SctrInsuranceDetailRoutingModule } from './sctr-insurance-detail.routing';

@NgModule({
  imports: [
    CommonModule,
    SctrInsuranceDetailRoutingModule,
    CardDetailPolicyModule,
    CardContractingInfoModule,
    CardPaymentPolicyModule,
    CardPolicyBasicInfoModule,
    CardWhatYouWantToDoModule,
    CardSctrPeriodsModule,
    CardPolicyHousesModule,
    CardSctrRecordListModule,
    ItemNotFoundModule,
    MfLoaderModule
  ],
  declarations: [SctrInsuranceDetailComponent]
})
export class SctrInsuranceDetailModule {}
