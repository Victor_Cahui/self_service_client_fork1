import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FoilAttachedModule } from '@mx/components/shared/foil-attached/foil-attached.module';
import { SctrFoilAttachedComponent } from './sctr-foil-attached.component';
import { SctrFoilAttachedRoutingModule } from './sctr-foil-attached.routing';

@NgModule({
  imports: [CommonModule, RouterModule, SctrFoilAttachedRoutingModule, FoilAttachedModule],
  declarations: [SctrFoilAttachedComponent]
})
export class SctrFoilAttachedModule {}
