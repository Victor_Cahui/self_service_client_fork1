import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardIconsCoverageModule } from '@mx/components/shared/card-icons-coverage/card-icons-coverage.module';
import { SctrCoverageComponent } from './sctr-coverage.component';
import { SctrCoverageRouting } from './sctr-coverage.routing';

@NgModule({
  imports: [CommonModule, SctrCoverageRouting, CardIconsCoverageModule],
  declarations: [SctrCoverageComponent]
})
export class SctrCoverageModule {}
