import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SctrPeriodReceiptsComponent } from './sctr-period-receipts.component';

const routes: Routes = [
  {
    path: '',
    component: SctrPeriodReceiptsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SctrPeriodReceiptsRoutingModule {}
