import { DatePipe, DOCUMENT } from '@angular/common';
import { AfterViewInit, Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AlphanumericValidator } from '@mx/core/ui';
import { AuthService } from '@mx/services/auth/auth.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { SctrService } from '@mx/services/sctr.service';
import {
  APPLICATION_CODE,
  FORMAT_DATE_DDMMYYYY,
  GENERAL_MAX_LENGTH,
  GENERAL_MIN_LENGTH
} from '@mx/settings/constants/general-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { RECORD_LANG } from '@mx/settings/lang/sctr.lang';
import { ISctrByClientResponse } from '@mx/statemanagement/models/policy.interface';
import { IInsuredByRecordRequest, IInsuredRecordResponse } from '@mx/statemanagement/models/sctr.interface';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-sctr-insurance-insureds',
  templateUrl: './sctr-insurance-insureds.component.html',
  providers: [DatePipe]
})
export class SctrInsuranceInsuredsComponent implements AfterViewInit, OnDestroy, OnInit {
  itemsPerPage = 10;
  page = 1;
  maxLengthSearch = GENERAL_MAX_LENGTH.searchInsureds;
  minLengthSearch = GENERAL_MIN_LENGTH.searchInsureds;
  auth: IdDocument;
  form: FormGroup;
  insureds: Array<IInsuredRecordResponse> = [];
  insuredSearch: string;
  numberPolicy: string;
  numberRecord: string;
  showLoading: boolean;
  title: string;
  total: number;
  lang = RECORD_LANG;
  generalLang = GeneralLang;
  getInsuredsSub: Subscription;
  insured: AbstractControl;
  searchText: string;

  constructor(
    @Inject(DOCUMENT) private readonly document: Document,
    private readonly headerHelperService: HeaderHelperService,
    private readonly policiesService: PoliciesService,
    private readonly policiesInfoService: PoliciesInfoService,
    private readonly authService: AuthService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly sctrService: SctrService,
    private readonly fb: FormBuilder,
    private readonly datePipe: DatePipe
  ) {
    this.auth = this.authService.retrieveEntity();
  }

  ngOnInit(): void {
    this.createForm();
    this.activatedRoute.paramMap.subscribe((res: any) => {
      this.numberPolicy = res.params.policyNumber;
      this.numberRecord = decodeURIComponent(decodeURIComponent(res.params.numberRecord));
      this.title = `${this.lang.Record} ${this.numberRecord}`;
      this.loadInfoDetailSctr();
      this.getInsureds();
    });
  }

  ngAfterViewInit(): void {
    const tapTop = this.document.getElementsByTagName('mf-tab-top')[0];
    tapTop.classList.add('g-none');
  }

  ngOnDestroy(): void {
    const tapTop = this.document.getElementsByTagName('mf-tab-top')[0];
    tapTop.classList.remove('g-none');
    if (this.getInsuredsSub) {
      this.getInsuredsSub.unsubscribe();
    }
  }

  getInsureds(): void {
    const params: IInsuredByRecordRequest = {
      codigoApp: APPLICATION_CODE,
      numeroConstancia: this.numberRecord.replace(/\//g, '_'),
      registros: this.itemsPerPage,
      pagina: this.page,
      nombreODocumento: this.insuredSearch || ''
    };
    this.showLoading = true;
    this.getInsuredsSub = this.sctrService.getInsuredsByRecord(params).subscribe(
      response => {
        this.insureds = response.asegurados || [];
        this.insureds = this.insureds.map(this.__mapInsured.bind(this));
        this.total = response.totalRegistros;
        this.showLoading = false;
      },
      () => {
        this.getInsuredsSub.unsubscribe();
      }
    );
  }

  private __mapInsured(insured: IInsuredRecordResponse): IInsuredRecordResponse {
    return {
      ...insured,
      fechaNacimiento:
        insured.fechaNacimiento === '-'
          ? insured.fechaNacimiento
          : this.datePipe.transform(insured.fechaNacimiento, FORMAT_DATE_DDMMYYYY)
    };
  }

  pageChange(page: number): void {
    this.page = page;
    this.getInsureds();
  }

  itemsPerPageChange(itemsPerPage?: number): void {
    this.page = 1;
    this.itemsPerPage = itemsPerPage || this.itemsPerPage;
    this.getInsureds();
  }

  loadInfoDetailSctr(): void {
    const clientPolicy = this.policiesInfoService.getClientSctrPolicy(this.numberPolicy);
    if (!clientPolicy) {
      this.policiesService
        .sctrSearchByClient({
          codigoApp: APPLICATION_CODE,
          numeroPoliza: this.numberPolicy
        })
        .subscribe(response => {
          if (response && response.length > 0) {
            const clientPolicyResponse = response[0];
            this.policiesInfoService.setClientSctrPolicyResponse(clientPolicyResponse);
            this.configView(clientPolicyResponse);
          }
        });
    } else {
      this.configView(clientPolicy);
    }
  }

  configView(sctr: ISctrByClientResponse): void {
    this.headerHelperService.set({
      title: GeneralLang.Titles.InsuredTypes.MyInsureds.toUpperCase(),
      subTitle: sctr.descripcionPoliza.toUpperCase(),
      icon: '',
      back: true
    });
  }

  createForm(): void {
    this.form = this.fb.group({
      insured: [
        '',
        [
          Validators.required,
          Validators.minLength(this.minLengthSearch),
          Validators.maxLength(this.maxLengthSearch),
          AlphanumericValidator.isValid
        ]
      ]
    });
    this.insured = this.form.controls['insured'];
  }

  searchInput(text?: string): void {
    this.insured.setValue(text || '');
    if (this.form.valid) {
      this.insuredSearch = text;
    }
    if (text) {
      this.insured.markAsTouched();
    } else {
      this.insured.markAsUntouched();
    }
  }

  search(): void {
    if (this.form.valid) {
      this.itemsPerPageChange();
    }
  }

  clear(): void {
    this.insuredSearch = '';
    this.searchInput();
    this.itemsPerPageChange();
  }
}
