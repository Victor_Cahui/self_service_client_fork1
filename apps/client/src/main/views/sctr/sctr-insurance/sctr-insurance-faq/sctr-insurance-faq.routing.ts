import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SctrInsuranceFaqComponent } from '../sctr-insurance-faq/sctr-insurance-faq.component';

const routes: Routes = [
  {
    path: '',
    component: SctrInsuranceFaqComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SctrInsuranceFaqRouting {}
