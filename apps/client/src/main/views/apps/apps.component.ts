import { Component, OnInit } from '@angular/core';

import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { UnsubscribeOnDestroy } from '@mx/core/shared/helpers/util/unsubscribe-on-destroy';
import { Seguridad } from '@mx/core/shared/providers/services';
import { environment } from '@mx/environments/environment';
import { SYSTEM_CODE_OIM, SYSTEM_CODE_SELF_SERVICE } from '@mx/settings/constants/general-values';
import { ServicesLang } from '@mx/settings/lang/services.lang';
import { takeUntil } from 'rxjs/operators';
import { reduceForCard } from './apps.utils';

@Component({
  selector: 'client-apps-component',
  templateUrl: './apps.component.html'
})
export class AppsComponent extends UnsubscribeOnDestroy implements OnInit {
  lang = ServicesLang;
  serviceList: any[];
  serviceListItems: any[];
  legacyApps: any[];
  showAppsLoader: boolean;

  constructor(private readonly _Seguridad: Seguridad) {
    super();
    this.showAppsLoader = true;
  }

  ngOnInit(): void {
    this._getApps();
  }

  onFilter(searchValue: string): void {
    if (searchValue.length >= 3) {
      this.serviceListItems = this.serviceList.filter(this._searchWord.bind(this, searchValue));
    }
  }

  clearFilter(): void {
    this.serviceListItems = this.serviceList;
  }

  private _getApps(): void {
    this._Seguridad
      .ObtenerAplicacionesClienteEmpresa()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(this._mapResGetApps.bind(this), () => {
        this.showAppsLoader = false;
      });
  }

  private _mapResGetApps(r): void {
    this.serviceList = reduceForCard(r.data, this.goToApp.bind(this));
    this.serviceListItems = [...this.serviceList];
    this.showAppsLoader = false;
  }

  private _searchWord(searchValue, s): any {
    const wordToSearch = StringUtil.withoutDiacritics(searchValue.toLowerCase());

    return StringUtil.withoutDiacritics(s.titulo.toLowerCase())
      .split(' ')
      .find(e => e.startsWith(wordToSearch));
  }

  goToApp(e): void {
    if (e.isLegacy) {
      this._redirectToOldOIM(e);
    } else {
      this._redirectToOIM(e);
    }
  }

  private _redirectToOIM(app): void {
    this._Seguridad
      .GenerarTokenDummy(
        {
          connectionIdentifier: '',
          destiny: SYSTEM_CODE_OIM,
          origin: SYSTEM_CODE_SELF_SERVICE,
          urlRedirection: `/${app.redirect}`
        },
        { preloader: true }
      )
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        window.open(`${environment.WEB_OIM_URL}/by-pass/#/?token=${response.data.tokenDummy}`, '_self');
      });
  }

  private _redirectToOldOIM(app): void {
    this._Seguridad
      .GenerarTokenMapfre({ preloader: true })
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        const url: string = app.rutaLegacy.replace('{tokenMapfre}', response);
        window.open(url, '_self');
      });
  }
}
