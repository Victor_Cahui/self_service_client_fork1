import { APPS_PATH } from '@mx/settings/constants/images-values';
import { getAppIcon, getObjIcon } from '@mx/settings/constants/services-mf';

export function reduceForCard(arr: any[], cbClick: VoidFunction, parent?: any): any {
  return arr.reduce((pre, cur) => {
    // INFO: casuistica especial por EMISA
    const value = parent && parent.codigoAplicacion !== 'EMISA' ? parent : cur;

    if (cur.items && cur.items.length) {
      return [...pre, ...reduceForCard(cur.items, cbClick, cur)];
    }

    pre.push({
      ...cur,
      accion: 'redirect',
      descripcion: value.nombreCorto,
      icon: `${APPS_PATH}${
        value.codigoAplicacion ? getAppIcon(value.codigoAplicacion) : getObjIcon(value.nombreCorto)
      }`,
      maneraContacto: 'INGRESAR',
      onClick: cbClick,
      titulo: value.nombreCorto,
      redirect: parent ? `${parent.urlRedireccion}/#/${cur.urlRedireccion}` : cur.urlRedireccion
    });

    return pre;
  }, []);
}
