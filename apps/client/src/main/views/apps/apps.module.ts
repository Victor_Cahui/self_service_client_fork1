import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { InputSearchModule } from '@mx/components/shared/input-search/input-search.module';
import { ItemNotFoundModule } from '@mx/components/shared/item-not-found/item-not-found.module';
import { ServiceCardModule } from '@mx/components/shared/mapfre-services/service-card/service-card.module';
import { PageTitleModule } from '@mx/components/shared/page-title/page-title.module';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { AppsComponent } from './apps.component';
import { AppsRoutingModule } from './apps.routing';

@NgModule({
  imports: [
    AppsRoutingModule,
    CommonModule,
    FormsModule,
    InputSearchModule,
    ItemNotFoundModule,
    PageTitleModule,
    ServiceCardModule,
    MfLoaderModule
  ],
  exports: [],
  declarations: [AppsComponent],
  providers: []
})
export class AppsModule {}
