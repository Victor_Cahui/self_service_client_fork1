import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DigitalClinicLandingComponent } from './digital-clinic-landing.component';

const routes: Routes = [
  {
    path: '',
    component: DigitalClinicLandingComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DigitalClinicLandingRoutingModule {}
