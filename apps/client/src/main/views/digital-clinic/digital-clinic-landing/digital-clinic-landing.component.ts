import { Component, OnInit, ViewChild } from '@angular/core';
import { GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { MfModalAlertComponent } from '@mx/core/ui/lib/components/modals/modal-alert/modal-alert.component';
import { GeneralService } from '@mx/services/general/general.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { InitModalService } from '@mx/services/general/init-modal.service';
import { APPLICATION_CODE, BenefitsList, CodigoMoneda, ServicesList } from '@mx/settings/constants/general-values';
import * as KEYS_VALUES from '@mx/settings/constants/key-values';
import { DIGITAL_CLINIC_LANDING_HEADER } from '@mx/settings/constants/router-titles';
import { GeneralLang } from '@mx/settings/lang/general.lang';

@Component({
  selector: 'client-digital-clinic-landing-component',
  templateUrl: './digital-clinic-landing.component.html',
  styleUrls: ['./digital-clinic-landing.component.scss']
})
export class DigitalClinicLandingComponent extends GaUnsubscribeBase implements OnInit {
  @ViewChild(MfModalAlertComponent) modal: MfModalAlertComponent;
  btnConfirm = GeneralLang.Buttons.Confirm;

  carouselHeader = 'Haz tus consultas y usa gratis los múltiples servicios online';
  showLoading = true;
  saludPlanList = [];

  codigoMoneda: string;
  precioPlan: any;

  lang = GeneralLang;
  servicesList = ServicesList;
  benefitsList = BenefitsList;
  txtReceived = GeneralLang.Texts.CdmPlanContact;

  cotizarURL = 'https://salud.mapfre.com.pe/clinica-digital/';
  saludURL = 'https://salud.mapfre.com.pe/cotiza-salud/';
  module = 'CDM';

  constructor(
    private readonly headerHelperService: HeaderHelperService,
    private readonly _Autoservicios: Autoservicios,
    private readonly initModalService: InitModalService,
    private readonly _GeneralService: GeneralService
  ) {
    super();
  }

  ngOnInit(): void {
    this.obtenerPlanes();
    this._GeneralService.saveUserMovements(
      this.module,
      this._GeneralService.action(KEYS_VALUES.CDM_PROCEDURES.PROC_SERVICIOS_CDM),
      this._GeneralService.action(KEYS_VALUES.CDM_DESCRIPTION.DSC_INICIO)
    );
    this.txtReceived = GeneralLang.Texts.CdmPlanContact.replace('{{nroCelular}}', '954950900');
  }

  obtenerPlanes(): void {
    this.showLoading = true;

    const bodyReq: any = {
      codigoApp: APPLICATION_CODE
    };

    this._Autoservicios.ObtenerPlanesCdm(bodyReq).subscribe(
      res => {
        if (res) {
          this.showLoading = false;
          this.saludPlanList = res;
          this.onSelectedPlan({ target: { value: this.getFirstElementFromList(this.saludPlanList) } });
        }
      },
      err => {
        console.error(err);
        this.showLoading = false;
      }
    );
  }

  getFirstElementFromList(list: any): number {
    let obj: any = {};
    if (list && list.length > 0) {
      obj = list[0];

      return obj.codigo;
    }
  }

  onSelectedPlan(ev): void {
    const codigoPlan = ev.target.value;
    const plan = this.saludPlanList.find(e => e.codigo === parseInt(codigoPlan, 10));
    this.codigoMoneda = plan.codigoMoneda === CodigoMoneda.SOLES ? 'S/' : '$';
    this.precioPlan = parseFloat(plan.precio).toFixed(2);
  }

  takeOutPlan(): void {
    this.modal.open();
  }

  closeModal(): void {
    this.initModalService.emitCloseModalEvent();
  }

  setHeader(): void {
    this.headerHelperService.set(DIGITAL_CLINIC_LANDING_HEADER);
  }
}
