import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesUtils } from '@mx/components/shared/mapfre-services/services.utils';
import { convertMonth } from '@mx/core/shared/helpers/util/date';
import { UnsubscribeOnDestroy } from '@mx/core/shared/helpers/util/unsubscribe-on-destroy';
import { AppointmentFacade } from '@mx/core/shared/state/appointment';
import { AuthService } from '@mx/services/auth/auth.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { ServicesMFService } from '@mx/services/services-mf.service';
import { APPLICATION_CODE, TYPE_APPOINTMENT } from '@mx/settings/constants/general-values';
import { DIGITAL_CLINIC_ASSISTS } from '@mx/settings/constants/router-titles';
import { ICON_ITEMS, SERVICES_TYPE } from '@mx/settings/constants/services-mf';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { ServiceFormLang, ServicesLang } from '@mx/settings/lang/services.lang';
import { GeneralRequest } from '@mx/statemanagement/models/general.models';
import { IServiceHistoryView, IServicesItemResponse } from '@mx/statemanagement/models/service.interface';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-mapfre-services-assists',
  templateUrl: './mapfre-services-assists.component.html'
})
export class MapfreServicesAssistsComponent extends UnsubscribeOnDestroy implements OnInit {
  showLoading: boolean;
  serviceInProgressList: Array<IServiceHistoryView>;
  serviceHistoryList: Array<IServiceHistoryView>;
  lang = ServicesLang;
  requestLang = ServiceFormLang;
  auth: IdDocument;

  serviceListSub: Subscription;

  constructor(
    private readonly _AppointmentFacade: AppointmentFacade,
    private readonly _Router: Router,
    private readonly authService: AuthService,
    private readonly servicesMfService: ServicesMFService,
    private readonly headerHelperService: HeaderHelperService
  ) {
    super();
  }

  ngOnInit(): void {
    this.auth = this.authService.retrieveEntity();
    this.getDataList();
    this._mapState();
    this.setHeader();
  }

  // Buscar datos
  getDataList(): void {
    // Ya no se consulta al storage para no llamar al servicio
    // const data = this.servicesMfService.getServicesHistoryData();
    this.auth = this.authService.retrieveEntity();
    const params: GeneralRequest = {
      codigoApp: APPLICATION_CODE
    };
    this.showLoading = true;
    this.serviceListSub = this.servicesMfService.getServicesHistory(params).subscribe(
      response => {
        if (response) {
          this.servicesMfService.setServicesHistoryData(response);
          this.serviceInProgressList = response.enCurso.map((item: IServicesItemResponse) => this.setData(item));
          this.serviceHistoryList = response.historial.map((item: IServicesItemResponse) => this.setData(item));
        }
      },
      () => {
        this.showLoading = false;
      },
      () => {
        this.showLoading = false;
        this.serviceListSub.unsubscribe();
      }
    );
  }

  // Configurar Vista
  // tslint:disable-next-line: cyclomatic-complexity
  setData(item: IServicesItemResponse): IServiceHistoryView {
    const date = item.fechaAgendada.split('-');
    const monthName = convertMonth(parseInt(date[1], 10));
    const statusColor = ServicesUtils.getStatusColor(item.codigoEstado);
    const moreInfo =
      item.cita && item.cita.diasRestantes ? `${item.cita.diasRestantes} ${this.lang.Labels.DaysLeft}` : null;
    const na = GeneralLang.Texts.WithOutInformation;

    const itemView: IServiceHistoryView = {
      code: item.codigoTipoSolicitud,
      day: date[2],
      month: monthName,
      year: date[0],
      scheduledTime: item.horaAgendada,
      type: item.descripcionTipoSolicitud,
      moreInfo,
      number: item.identificadorSolicitud.toString(),
      color: item.codigoEstado ? statusColor : '',
      status: item.codigoEstado ? item.descripcionEstado : '',
      viewDetail: ServicesUtils.viewDetail(item.codigoEstado),
      items: [],
      payload: item
    };

    switch (item.codigoTipoSolicitud) {
      // Chofer de Reemplazo
      case SERVICES_TYPE.SUBSTITUTE_DRIVER:
        itemView.items = [
          {
            label: `${this.requestLang.From}:`,
            text: item.chofer.origenServicio || na
          },
          {
            label: `${this.requestLang.To}:`,
            text: item.destinoServicio || na
          },
          {
            label: `${this.requestLang.Driver}:`,
            text: item.empresaProveedora || na,
            icon: ICON_ITEMS.PERSON
          },
          {
            label: item.chofer.datosVehiculo || na,
            icon: ICON_ITEMS.VEHICLE
          }
        ];
        break;
      // Médico a Domicilio
      case SERVICES_TYPE.HOME_DOCTOR:
        itemView.items = [
          {
            icon: ICON_ITEMS.PIN,
            text: item.destinoServicio || na
          },
          {
            text: item.medico.tipoMedicina || na,
            icon: ICON_ITEMS.COVERAGE
          },
          {
            label: `${this.requestLang.Doctor}:`,
            text: item.empresaProveedora || na,
            icon: ICON_ITEMS.DOCTOR
          },
          {
            label: `${this.requestLang.Patient}:`,
            text: item.medico.paciente || na,
            icon: ICON_ITEMS.PERSON
          }
        ];
        break;
      // Citas Médicas
      case SERVICES_TYPE.MEDICAL_APPOINTMENT:
        const patient = `${item.cita.nombresPaciente} ${item.cita.apellidoPaternoPaciente} ${item.cita.apellidoMaternoPaciente}`;
        itemView.items = [
          {
            icon: ICON_ITEMS.PIN,
            text:
              item.cita.tipoCita === TYPE_APPOINTMENT.VIRTUAL
                ? GeneralLang.Labels.AppointmentVirtual
                : item.cita.clinicaDescripcion
          },
          {
            text: item.cita.especialidadDescripcion.toLowerCase() || na,
            icon: ICON_ITEMS.POLICY
          },
          {
            label: `${this.requestLang.Doctor}:`,
            text: item.cita.nombreCompleto.toLowerCase() || na,
            icon: ICON_ITEMS.DOCTOR
          },
          {
            label: `${this.requestLang.Patient}:`,
            text: patient.toLowerCase(),
            icon: ICON_ITEMS.PERSON
          }
        ];
        break;
      default:
        break;
    }

    return itemView;
  }

  private _mapState(): void {
    const GetAppointmentForDetailSuccess = this._AppointmentFacade.GetAppointmentForDetailSuccess$.subscribe(() => {
      if (this._AppointmentFacade.getState().canReschedule) {
        this._Router.navigate([`/digital-clinic/schedule-detail/3`]);

        return void 0;
      }
      this._Router.navigate([`/digital-clinic/schedule-detail/3`]);
    });

    this.arrToDestroy.push(GetAppointmentForDetailSuccess);
  }

  private setHeader(): void {
    this.headerHelperService.set(DIGITAL_CLINIC_ASSISTS);
  }
}
