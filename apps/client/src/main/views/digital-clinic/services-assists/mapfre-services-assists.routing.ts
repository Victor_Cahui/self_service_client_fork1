import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MapfreServicesAssistsComponent } from './mapfre-services-assists.component';

const routes: Routes = [
  {
    path: '',
    component: MapfreServicesAssistsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MapfreServicesAssistsRouting {}
