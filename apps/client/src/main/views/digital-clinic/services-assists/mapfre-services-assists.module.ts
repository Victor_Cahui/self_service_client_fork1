import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ServiceListModule } from '@mx/components/shared/mapfre-services/service-list/service-list.module';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { MapfreServicesAssistsComponent } from './mapfre-services-assists.component';
import { MapfreServicesAssistsRouting } from './mapfre-services-assists.routing';

@NgModule({
  imports: [CommonModule, MapfreServicesAssistsRouting, MfLoaderModule, ServiceListModule],
  declarations: [MapfreServicesAssistsComponent]
})
export class MapfreServicesAssistsModule {}
