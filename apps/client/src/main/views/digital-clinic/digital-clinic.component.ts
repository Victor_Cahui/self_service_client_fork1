import { DatePipe } from '@angular/common';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ConfigurationBase } from '@mx/components/shared/configuration/configuration-base';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { PlatformService } from '@mx/core/shared/helpers/util/platform.service';
import { ReqQueryGetObtenerSolicitudesFiltro } from '@mx/core/shared/providers/models';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { MfModalAlertComponent } from '@mx/core/ui/lib/components/modals/modal-alert/modal-alert.component';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { GeneralService } from '@mx/services/general/general.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { GA_DIGITAL_CLINIC_ITEM } from '@mx/settings/constants/events.analytics';
import {
  ACTION_SERVICE_DIGITAL_CLINIC,
  APPLICATION_CODE,
  KEY_SERVICE_DIGITAL_CLINIC,
  TYPE_OF_MODULE,
  WHATSAPPPHONE,
  WHATSAPPWEB
} from '@mx/settings/constants/general-values';
import { EVALUATION_COVID_ICON, EVALUATION_ICON, PHONE_ALERT_ICON } from '@mx/settings/constants/images-values';
import * as KEYS_VALUES from '@mx/settings/constants/key-values';
import { MEDIKTOR_URLS_REDIRECT } from '@mx/settings/constants/mediktor-values';
import { DIGITAL_CLINIC_HEADER } from '@mx/settings/constants/router-titles';
import { MediktorLang } from '@mx/settings/lang/mediktor.lang';
import { IServiceDigitalClinic } from '@mx/statemanagement/models/health.interface';
import { Subscription } from 'rxjs';

export enum KEYS {
  CITACOVID = 'cita_covid'
}

@Component({
  selector: 'client-digital-clinic',
  templateUrl: 'digital-clinic.component.html'
})
export class DigitalClinicComponent extends ConfigurationBase implements OnInit, OnDestroy {
  @ViewChild(MfModalAlertComponent) mfModalAlert: MfModalAlertComponent;
  lang = MediktorLang;
  // tslint:disable-next-line:max-line-length
  urlWhatsAppWebVirtualConsultWithDoctor = `${WHATSAPPWEB}?phone=${WHATSAPPPHONE}&text=${MEDIKTOR_URLS_REDIRECT.WhatsAppTextVirtualWithDoctor}`;
  urlPhaymacyDelivery = `${MEDIKTOR_URLS_REDIRECT.PharmacyDelivery}`;
  urlMapfreDoc = `${MEDIKTOR_URLS_REDIRECT.MapfreDoc}`;
  titleModal: string;
  subTitleModal: string;
  iconModal: string;
  textButtonConfirmModal: string;
  textButtonCancelModal: string;
  altTextIconModal: string;
  workflowProcess: string;
  flagConfirmModal: boolean;
  outlineModal: boolean;
  showTwoSubTitle = false;
  subTitleSpanModal: string;
  subTitleTwoModal: string;
  subTitleTwoSpanModal: string;
  htmlModalText: string;

  showLoading = false;
  appointments: any = [];
  services: IServiceDigitalClinic[];

  iniServTime: string;
  endServTime: string;
  messageOutOfTime: string;
  onTimeToChat = false;
  showTermsAndConditions = true;
  moduleTermsConditions = TYPE_OF_MODULE.CDM;
  requestsSubs$: Subscription;
  servicesSubs$: Subscription;
  enumActionServiceDigitalClinic = ACTION_SERVICE_DIGITAL_CLINIC;
  cardLoading = false;

  module = 'CDM';
  mediktorPhone = `tel:${MEDIKTOR_URLS_REDIRECT.MapfreTel}`;

  actualRoute: string;
  routesList: Array<string> = [];

  constructor(
    private readonly headerHelperService: HeaderHelperService,
    private readonly _Router: Router,
    protected clientService: ClientService,
    protected platformService: PlatformService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService,
    protected gaService: GAService,
    private readonly _GeneralService: GeneralService,
    private readonly _Autoservicios: Autoservicios,
    private readonly datePipe: DatePipe,
    protected localStorage: LocalStorageService
  ) {
    super(clientService, policiesInfoService, configurationService, authService, gaService);
    this.ga = [GA_DIGITAL_CLINIC_ITEM];
  }

  ngOnInit(): void {
    this.checkIfFirstTimeInRoute();
    this.setHeader();
    this.obtenerServicios();
  }

  ngOnDestroy(): void {
    if (this.requestsSubs$) {
      this.requestsSubs$.unsubscribe();
    }

    if (this.servicesSubs$) {
      this.servicesSubs$.unsubscribe();
    }
  }
  setHeader(): void {
    this.headerHelperService.set(DIGITAL_CLINIC_HEADER);
  }

  goEvent(service: IServiceDigitalClinic): void {
    if (!service) {
      return void 0;
    }

    if (service.action === ACTION_SERVICE_DIGITAL_CLINIC.REDIRECTION) {
      if (service.key === KEY_SERVICE_DIGITAL_CLINIC.AUT_MED) {
        this._GeneralService.saveUserMovements(
          this.module,
          this._GeneralService.action(KEYS_VALUES.CDM_PROCEDURES.PROC_AUTOEVALUADOR_CDM),
          this._GeneralService.action(KEYS_VALUES.CDM_DESCRIPTION.DSC_INICIO)
        );
      } else if (service.key === KEY_SERVICE_DIGITAL_CLINIC.CIT_ESP) {
        this._GeneralService.saveUserMovements(
          this.module,
          this._GeneralService.action(KEYS_VALUES.CDM_PROCEDURES.PROC_AGENDA_ESPECIALISTA_CDM),
          this._GeneralService.action(KEYS_VALUES.CDM_DESCRIPTION.DSC_INICIO)
        );
      } else if (service.key === KEY_SERVICE_DIGITAL_CLINIC.MED_DOM) {
        this._GeneralService.saveUserMovements(
          this.module,
          this._GeneralService.action(KEYS_VALUES.CDM_PROCEDURES.PROC_MEDICO_DOMICILIO_CDM),
          this._GeneralService.action(KEYS_VALUES.CDM_DESCRIPTION.DSC_INICIO)
        );
      } else if (service.key === KEY_SERVICE_DIGITAL_CLINIC.RED_CLI) {
        this._GeneralService.saveUserMovements(
          this.module,
          this._GeneralService.action(KEYS_VALUES.CDM_PROCEDURES.PROC_CONSULTA_CLINICA_CDM),
          this._GeneralService.action(KEYS_VALUES.CDM_DESCRIPTION.DSC_INICIO)
        );
      } else {
        console.warn('Invalid Key');
      }
      this._Router.navigate([service.route]);
    } else if (service.action === ACTION_SERVICE_DIGITAL_CLINIC.MODAL) {
      if (service.key === KEY_SERVICE_DIGITAL_CLINIC.CIT_COV) {
        this.goToCitaCovid();
      } else if (service.key === KEY_SERVICE_DIGITAL_CLINIC.VID_CON) {
        this.goVideoConsultWithDoctor();
      } else if (service.key === KEY_SERVICE_DIGITAL_CLINIC.CHA_DOC) {
        this.goToChatWithDoctor();
      } else if (service.key === KEY_SERVICE_DIGITAL_CLINIC.DEL_MED) {
        this.goDeliveryMedicines();
      } else {
        console.warn('Invalid Key');
      }
    } else if (service.action === ACTION_SERVICE_DIGITAL_CLINIC.REDIRECTION_OUT) {
      if (service.key === KEY_SERVICE_DIGITAL_CLINIC.SOL_AMB) {
        this.goToAmbulance(service);
      } else {
        console.warn('Invalid Key');
      }
    } else {
      console.warn('Invalid Action');
    }
  }

  goToAmbulance(service: IServiceDigitalClinic): void {
    if (this.platformService.isMobile) {
      this._GeneralService.saveUserMovements(
        this.module,
        this._GeneralService.action(KEYS_VALUES.CDM_PROCEDURES.PROC_AMBULANCIA_CDM),
        this._GeneralService.action(KEYS_VALUES.CDM_DESCRIPTION.DSC_LLAMADA_TELEFONO)
      );
      window.location.href = this.mediktorPhone;
    } else {
      this._GeneralService.saveUserMovements(
        this.module,
        this._GeneralService.action(KEYS_VALUES.CDM_PROCEDURES.PROC_AMBULANCIA_CDM),
        this._GeneralService.action(KEYS_VALUES.CDM_DESCRIPTION.DSC_INICIO)
      );
      this._Router.navigate([service.route]);
    }
  }

  goVideoConsultWithDoctor(): void {
    this._GeneralService.saveUserMovements(
      this.module,
      this._GeneralService.action(KEYS_VALUES.CDM_PROCEDURES.PROC_VIDEO_LLAMADA_MAPFREDOC),
      this._GeneralService.action(KEYS_VALUES.CDM_DESCRIPTION.DSC_INICIO)
    );
    this.titleModal = this.lang.Modal.TitleMapfreDoc;
    this.subTitleModal = this.lang.Modal.SubTitleMapfreDoc;
    this.textButtonConfirmModal = this.lang.Modal.TextButtonYesMapfreDoc;
    this.textButtonCancelModal = this.lang.Modal.TextButtonCreateAccountMapfreDoc;
    this.flagConfirmModal = true;
    this.outlineModal = false;
    this.iconModal = PHONE_ALERT_ICON;
    this.altTextIconModal = 'Phone Alert';
    this.workflowProcess = 'VIDEO_CONSULT_WITH_DOCTOR';
    this.showTwoSubTitle = false;
    this.htmlModalText = '';
    this.mfModalAlert.open();
  }

  goDeliveryMedicines(): void {
    this._GeneralService.saveUserMovements(
      this.module,
      this._GeneralService.action(KEYS_VALUES.CDM_PROCEDURES.PROC_DELIVERY_MEDICAMENTO_CDM),
      this._GeneralService.action(KEYS_VALUES.CDM_DESCRIPTION.DSC_INICIO)
    );
    this.titleModal = this.lang.Modal.TitleDeliveryMedicines;
    this.subTitleModal = this.lang.Modal.SubTitleDeliveryMedicines;
    this.subTitleSpanModal = '';
    this.subTitleTwoModal = this.lang.Modal.SubTitleTwoDeliveryMedicines;
    this.textButtonConfirmModal = this.lang.Modal.TextButtonYesDeliveryMedicines;
    this.textButtonCancelModal = this.lang.Modal.TextButtonNoDeliveryMedicines;
    this.flagConfirmModal = true;
    this.outlineModal = true;
    this.iconModal = EVALUATION_ICON;
    this.altTextIconModal = 'Phone Alert';
    this.workflowProcess = 'DELIVERY_MEDICINES';
    this.showTwoSubTitle = true;
    this.htmlModalText = '';
    this.mfModalAlert.open();
  }

  goToCitaCovid(): void {
    this._GeneralService.saveUserMovements(
      this.module,
      this._GeneralService.action(KEYS_VALUES.CDM_PROCEDURES.PROC_PRUEBA_COVID_CDM),
      this._GeneralService.action(KEYS_VALUES.CDM_DESCRIPTION.DSC_INICIO)
    );
    this.titleModal = this.lang.ModalCovid.TitleCovid;
    this.subTitleSpanModal = this.lang.ModalCovid.SubTitleSpanCovid;
    this.subTitleModal = this.lang.ModalCovid.SubTitleCovid;
    this.subTitleTwoModal = this.lang.ModalCovid.SubTitleTwoCovid;
    this.subTitleTwoSpanModal = this.lang.ModalCovid.SubTitleSpanTwoModal;
    this.textButtonConfirmModal = this.lang.ModalCovid.TextButtonYesCovid;
    this.textButtonCancelModal = this.lang.ModalCovid.TextButtonNoCovid;
    this.flagConfirmModal = true;
    this.outlineModal = true;
    this.iconModal = EVALUATION_COVID_ICON;
    this.altTextIconModal = 'Phone Alert';
    this.workflowProcess = 'COVID_CONSULT';
    this.showTwoSubTitle = true;
    this.htmlModalText = '';
    this.mfModalAlert.open();
  }

  goToChatWithDoctor(): void {
    this._GeneralService.saveUserMovements(
      this.module,
      this._GeneralService.action(KEYS_VALUES.CDM_PROCEDURES.PROC_CHAT_DOCTOR_CDM),
      this._GeneralService.action(KEYS_VALUES.CDM_DESCRIPTION.DSC_INICIO)
    );
    this.getDataToChatWithMedic();
    if (this.onTimeToChat) {
      const msgWhatsapp = this._GeneralService.getValueParams(KEYS_VALUES.CD_CHAT_MSJ_WHATSAPP_DOCTOR);
      const urlWhatsAppWeb = `${WHATSAPPWEB}?phone=${WHATSAPPPHONE}&text=${msgWhatsapp}`;
      window.open(urlWhatsAppWeb, '_blank');
    } else {
      this.titleModal = 'HORARIO DE ATENCIÓN:';
      this.subTitleSpanModal = '';
      this.subTitleModal = '';
      this.htmlModalText = this.messageOutOfTime;
      this.subTitleTwoModal = '';
      this.subTitleTwoSpanModal = '';
      this.textButtonConfirmModal = 'ACEPTAR';
      this.flagConfirmModal = false;
      this.outlineModal = true;
      this.iconModal = 'assets/images/services/icon-chatea-doctor.svg';
      this.altTextIconModal = 'CHAT ALERT';
      this.workflowProcess = '';
      this.showTwoSubTitle = false;
      this.mfModalAlert.open();
    }
  }

  confirmModal(e: any): void {
    switch (this.workflowProcess) {
      case 'VIDEO_CONSULT_WITH_DOCTOR': {
        if (e) {
          this._GeneralService.saveUserMovements(
            this.module,
            this._GeneralService.action(KEYS_VALUES.CDM_PROCEDURES.PROC_VIDEO_LLAMADA_MAPFREDOC),
            this._GeneralService.action(KEYS_VALUES.CDM_DESCRIPTION.DSC_TENGO_CUENTA)
          );
          window.open(this.urlMapfreDoc, '_blank');
        } else {
          this._GeneralService.saveUserMovements(
            this.module,
            this._GeneralService.action(KEYS_VALUES.CDM_PROCEDURES.PROC_VIDEO_LLAMADA_MAPFREDOC),
            this._GeneralService.action(KEYS_VALUES.CDM_DESCRIPTION.DSC_CREAR_CUENTA)
          );
          window.open(this.urlWhatsAppWebVirtualConsultWithDoctor, '_blank');
        }
        this.mfModalAlert.close();
        break;
      }
      case 'DELIVERY_MEDICINES': {
        if (e) {
          this._GeneralService.saveUserMovements(
            this.module,
            this._GeneralService.action(KEYS_VALUES.CDM_PROCEDURES.PROC_DELIVERY_MEDICAMENTO_CDM),
            this._GeneralService.action(KEYS_VALUES.CDM_DESCRIPTION.DSC_CONTINUAR)
          );
          window.open(this.urlPhaymacyDelivery, '_blank');
        } else {
          this._GeneralService.saveUserMovements(
            this.module,
            this._GeneralService.action(KEYS_VALUES.CDM_PROCEDURES.PROC_DELIVERY_MEDICAMENTO_CDM),
            this._GeneralService.action(KEYS_VALUES.CDM_DESCRIPTION.DSC_CANCELAR)
          );
        }
        this.mfModalAlert.close();
        break;
      }
      case 'COVID_CONSULT': {
        if (e) {
          this._Router.navigate(['digital-clinic/schedule/steps-covid/test-type']);
        } else {
          this._GeneralService.saveUserMovements(
            this.module,
            this._GeneralService.action(KEYS_VALUES.CDM_PROCEDURES.PROC_PRUEBA_COVID_CDM),
            this._GeneralService.action(KEYS_VALUES.CDM_DESCRIPTION.DSC_CANCELAR)
          );
        }
        this.mfModalAlert.close();
        break;
      }
      default: {
        this.mfModalAlert.close();
        break;
      }
    }
  }

  obtenerServicios(): void {
    this.showLoading = true;
    const queryReq: ReqQueryGetObtenerSolicitudesFiltro = {
      codigoApp: APPLICATION_CODE,
      codigoTipoEstado: 'C'
    };
    this._Autoservicios.ObtenerSolicitudesFiltro(queryReq).subscribe(
      response => {
        this.appointments = response.length > 0 ? response : [];
        this.showLoading = false;
      },
      err => {
        this.showLoading = false;
        console.error(err);
      }
    );
    this.servicesSubs$ = this._Autoservicios.ObtenerServiciosCdm({ codigoApp: APPLICATION_CODE }, {}).subscribe(
      response => {
        if (response) {
          this.services = response.map(this._mapService.bind(this));
        }
        this.showLoading = false;
      },
      err => {
        this.services = [];
        this.showLoading = false;
        console.error(err);
      }
    );
  }

  private _mapService(service: any): IServiceDigitalClinic {
    return {
      id: service.id,
      key: service.llave,
      description: service.descripcion,
      icon: service.icono,
      route: service.ruta,
      isNew: service.nuevo,
      action: service.accion
    };
  }

  getDataToChatWithMedic(): void {
    this.iniServTime = this._GeneralService.getValueParams(KEYS_VALUES.HORARIO_INICIO_CHAT_MEDIC_CDM);
    this.endServTime = this._GeneralService.getValueParams(KEYS_VALUES.HORARIO_FIN_CHAT_MEDIC_CDM);
    this.messageOutOfTime = this._GeneralService.getValueParams(KEYS_VALUES.MSG_MODAL_CHAT_MEDIC_CDM);

    this.validateServiceOnTime(this.iniServTime, this.endServTime);
  }

  validateServiceOnTime(init: string, end: string): void {
    const actualTime = parseInt(this.datePipe.transform(new Date(), 'H'), 10);
    const iniDate = parseInt(
      this.datePipe.transform(
        new Date(
          new Date().getFullYear(),
          new Date().getMonth(),
          new Date().getDate(),
          this.getTimeOnSplit(init)[0],
          this.getTimeOnSplit(init)[1]
        ),
        'H'
      ),
      10
    );
    const endDate = parseInt(
      this.datePipe.transform(
        new Date(
          new Date().getFullYear(),
          new Date().getMonth(),
          new Date().getDate(),
          this.getTimeOnSplit(end)[0],
          this.getTimeOnSplit(end)[1]
        ),
        'H'
      ),
      10
    );

    this.onTimeToChat = iniDate < actualTime && actualTime < endDate ? true : false;
  }

  getTimeOnSplit(time: string): any {
    const dateArr: any[] = time.split(':');
    dateArr.forEach(parseInt);

    return dateArr;
  }

  changeTermsAndConditions(): void {
    this.showTermsAndConditions = !this.showTermsAndConditions;
  }

  checkIfFirstTimeInRoute(): void {
    this.actualRoute = this._Router.url;
    const pathRoute = `/${this.actualRoute.split('/')[1]}`;
    const urlList: any[] = this.localStorage.getData('url_list');
    if (!urlList.includes(pathRoute)) {
      this._GeneralService.saveUserMovements(
        this.module,
        this._GeneralService.action(KEYS_VALUES.CDM_PROCEDURES.PROC_SERVICIOS_CDM),
        this._GeneralService.action(KEYS_VALUES.CDM_DESCRIPTION.DSC_INICIO)
      );
      urlList.push(pathRoute);
      this.localStorage.setData('url_list', urlList);
    }
  }
  // tslint:disable: max-file-line-count
}
