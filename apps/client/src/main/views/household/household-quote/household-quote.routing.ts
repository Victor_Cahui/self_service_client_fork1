import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HouseholdQuoteComponent } from './household-quote.component';

const routes: Routes = [
  {
    path: '',
    component: HouseholdQuoteComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HouseholdQuoteRoutingModule {}
