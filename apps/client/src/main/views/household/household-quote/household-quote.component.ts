import { Component, OnInit, ViewChild } from '@angular/core';
import { ConfigurationBase } from '@mx/components/shared/configuration/configuration-base';
import { FormContactedByAgentComponent } from '@mx/components/shared/form-contacted-by-agent/form-contacted-by-agent.component';
import { ModalMoreFaqsComponent } from '@mx/components/shared/modals/modal-more-faqs/modal-more-faqs.component';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { ScrollUtil } from '@mx/core/shared/helpers/util/scroll';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { MFP_Cotizacion_Contacto_de_Agente_38A } from '@mx/settings/constants/events.analytics';
import { POLICY_TYPES, STATIC_BANNERS } from '@mx/settings/constants/policy-values';
import { HOUSE_INSURANCE_HEADER } from '@mx/settings/constants/router-titles';
import { HOUSEHOLD_INSURANCE_FAQS } from '@mx/settings/faqs/household-insurance.faq';
import * as SOURCE from '@mx/settings/lang/household.lang';
import { IContactData } from '@mx/statemanagement/models/client.models';
import { IPathImageSize } from '@mx/statemanagement/models/general.models';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-quote-component',
  templateUrl: './household-quote.component.html'
})
export class HouseholdQuoteComponent extends ConfigurationBase implements OnInit {
  @ViewChild(ModalMoreFaqsComponent) modalFaqs: ModalMoreFaqsComponent;
  @ViewChild(FormContactedByAgentComponent) formContactedByAgentComponent: FormContactedByAgentComponent;

  staticBanners: string;
  contactData: IContactData;
  howToWork = SOURCE.HOW_TO_WORK;
  paymentOptions = SOURCE.PAYMENTS_OPTIONS;
  insuranceTop = SOURCE.INSURANCE_TOP;
  faqs = HOUSEHOLD_INSURANCE_FAQS;
  subTitleFaq = HOUSE_INSURANCE_HEADER.title;

  isSent: boolean;
  scrollSub: Subscription;
  pathImgBanner: IPathImageSize;

  ga: Array<IGaPropertie>;

  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService
  ) {
    super(clientService, policiesInfoService, configurationService, authService);
    this.ga = [MFP_Cotizacion_Contacto_de_Agente_38A()];
  }

  ngOnInit(): void {
    this.isSent = false;
    this.policyType = POLICY_TYPES.MD_HOGAR.code;
    this.staticBanners = POLICY_TYPES[this.policyType].staticBanners;
    this.contactData = { subject: POLICY_TYPES.MD_HOGAR.subject };
    this.pathImgBanner = {
      sm: this.staticBanners + STATIC_BANNERS.sm,
      lg: this.staticBanners + STATIC_BANNERS.lg,
      xl: this.staticBanners + STATIC_BANNERS.xl
    };
    this.loadConfiguration();
  }

  openModalMoreFaqs(): void {
    this.modalFaqs.open();
  }

  // Scroll a form
  eventInfo(): void {
    if (this.isSent) {
      this.isSent = false;
    }
    this.scrollSub = ScrollUtil.goTo(0).subscribe(
      () => {
        if (!this.formContactedByAgentComponent.isSent) {
          this.formContactedByAgentComponent.setFocusPhoneInput();
        }
      },
      () => {},
      () => {
        this.scrollSub.unsubscribe();
      }
    );
  }
}
