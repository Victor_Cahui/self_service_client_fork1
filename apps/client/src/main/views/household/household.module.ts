import { NgModule } from '@angular/core';
import { HouseholdRoutingModule } from './household-routing.module';

@NgModule({
  imports: [HouseholdRoutingModule],
  declarations: []
})
export class HouseholdModule {}
