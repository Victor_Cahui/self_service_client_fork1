import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PoliciesCompareModule } from '@mx/components/shared/compare-policy/policies-compare/policies-compare.module';
import { ModalContactedByAgentModule } from '@mx/components/shared/modals/modal-contacted-by-agent/modal-contacted-by-agent.module';
import { MfAlertWarningModule } from '@mx/core/ui/lib/components/alerts/alert-warning';
import { MfLoaderModule } from '@mx/core/ui/lib/components/global/loader';
import { MfModalModule } from '@mx/core/ui/lib/components/modals/modal';
import { MfModalMessageModule } from '@mx/core/ui/lib/components/modals/modal-message';
import { HouseholdCompareComponent } from './household-compare.component';
import { HouseholdCompareRoutingModule } from './household-compare.routing';

@NgModule({
  imports: [
    CommonModule,
    HouseholdCompareRoutingModule,
    PoliciesCompareModule,
    MfLoaderModule,
    MfModalModule,
    MfModalMessageModule,
    MfAlertWarningModule,
    ModalContactedByAgentModule
  ],
  declarations: [HouseholdCompareComponent]
})
export class HouseholdCompareModule {}
