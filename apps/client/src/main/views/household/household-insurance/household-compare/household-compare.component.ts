import { DecimalPipe } from '@angular/common';
import { Component, HostBinding, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { BaseHouseComparePolicies } from '@mx/components/shared/compare-policy/base-house-compare-policies';
import { ModalContactedByAgentComponent } from '@mx/components/shared/modals/modal-contacted-by-agent/modal-contacted-by-agent.component';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { HomeService } from '@mx/services/home/home.service';
import { PolicyListService } from '@mx/services/policy/policy-list.service';
import { PoliciesQuoteService } from '@mx/services/policy/policy-quote.service';
import { LANDING_ID, POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { HOUSE_COMPARE_HEADER } from '@mx/settings/constants/router-titles';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import { IContactData } from '@mx/statemanagement/models/client.models';
import { ICompareHeaderView } from '@mx/statemanagement/models/policy.interface';

@Component({
  selector: 'client-household-compare-component',
  templateUrl: './household-compare.component.html',
  providers: [DecimalPipe]
})
export class HouseholdCompareComponent extends BaseHouseComparePolicies implements OnInit {
  @HostBinding('class') class = 'w-100';
  @ViewChild(ModalContactedByAgentComponent) modalAgentConnect: ModalContactedByAgentComponent;

  lang = PolicyLang;
  old = GeneralLang.Labels.Old;
  new = GeneralLang.Labels.New;
  showPaymentPlan = true;
  titleMobile: string;
  subTitleMobile: string;
  policyType = POLICY_TYPES.MD_HOGAR.code;
  contactData: IContactData;

  constructor(
    protected policyListService: PolicyListService,
    protected headerHelperService: HeaderHelperService,
    protected quoteService: PoliciesQuoteService,
    protected homeService: HomeService,
    protected decimalPipe: DecimalPipe,
    public router: Router
  ) {
    super(policyListService, headerHelperService, quoteService, homeService, decimalPipe);
    this.headerHelperService.set(HOUSE_COMPARE_HEADER);
    this.titleMobile = HOUSE_COMPARE_HEADER.subTitle;
    this.subTitleMobile = HOUSE_COMPARE_HEADER.title;
  }

  ngOnInit(): void {
    this.compare = true;
    this.viewInitial();
  }

  // Cambiar de Modalidad
  onChange(item: ICompareHeaderView): void {
    this.openModalConfirm = true;
    this.contactData = {
      company: item.company,
      branch: item.branch,
      modality: item.modality,
      policyNumber: this.policyListService.getPolicySelected(),
      landingId: LANDING_ID.HOME_CHANGE
    };
    setTimeout(() => {
      this.modalAgentConnect.open();
    }, 10);
  }

  backToList(): void {
    this.router.navigate(['/household/household-insurance']);
  }
}
