import { NgModule } from '@angular/core';
import { HouseholdInsuranceRoutingModule } from './household-insurance.routing';

@NgModule({
  imports: [HouseholdInsuranceRoutingModule],
  exports: [],
  declarations: [],
  providers: []
})
export class HouseholdInsuranceModule {}
