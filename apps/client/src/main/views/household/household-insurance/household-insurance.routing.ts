import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: './household-insurance-list/household-insurance-list.module#HouseholdInsuranceListModule'
  },
  {
    path: 'faqs',
    loadChildren: './household-insurance-faq/household-insurance-faq.module#HouseholdInsuranceFAQModule'
  },
  {
    path: 'compare/:policyNumber',
    loadChildren: './household-compare/household-compare.module#HouseholdCompareModule'
  },
  {
    path: ':policyNumber',
    loadChildren: './household-insurance-by-id/household-insurance-by-id.module#HouseholdInsuranceByIdModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HouseholdInsuranceRoutingModule {}
