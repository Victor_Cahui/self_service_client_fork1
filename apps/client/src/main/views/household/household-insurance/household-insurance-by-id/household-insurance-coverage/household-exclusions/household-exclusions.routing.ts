import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HouseholdExclusionsComponent } from './household-exclusions.component';

const routes: Routes = [
  {
    path: '',
    component: HouseholdExclusionsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HouseholdExclusionsRoutingModule {}
