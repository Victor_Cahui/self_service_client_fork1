import { OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CardDetailPolicyBase } from '@mx/components/shared/card-detail-policy/card-detail-policy-base';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { HomeService } from '@mx/services/home/home.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { COVERAGES_HOUSEHOLD, ICON_COVERAGE_DEFAULT } from '@mx/settings/constants/coverage-values';
import { HOUSEHOLD_COVERAGE_TABS } from '@mx/settings/constants/router-tabs';
import { ICoverageIcons, ICoverageItem } from '@mx/statemanagement/models/coverage.interface';
import { IHomeCoverageSublimitResponse } from '@mx/statemanagement/models/home.interface';
import { ICoveragesPolicy, IPolicyHousesResponse } from '@mx/statemanagement/models/policy.interface';
import { ITabItem } from '@mx/statemanagement/models/router.interface';
import { Subscription } from 'rxjs';

export class HouseHoldCoverageBase extends CardDetailPolicyBase implements OnDestroy {
  options: Array<ITabItem>;
  showLoading: boolean;
  sublimitList: Array<IHomeCoverageSublimitResponse>;
  itemsList: Array<ICoverageItem>;
  itemsListBase: Array<ICoverageIcons>;
  itemListBase: Array<ICoverageIcons>;
  openModal: boolean;
  messageError: string;

  coveragesSub: Subscription;
  deductiblesSub: Subscription;
  policiesServiceSub: Subscription;
  homeServiceSub: Subscription;

  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService,
    protected policiesService: PoliciesService,
    protected homeService: HomeService,
    protected headerHelperService: HeaderHelperService,
    protected activatedRoute: ActivatedRoute,
    protected localStorageService: LocalStorageService,
    protected router: Router,
    protected _Autoservicios: Autoservicios
  ) {
    super(
      clientService,
      policiesInfoService,
      configurationService,
      authService,
      policiesService,
      headerHelperService,
      activatedRoute,
      localStorageService,
      router,
      false,
      _Autoservicios
    );
    this.options = HOUSEHOLD_COVERAGE_TABS;
    this.loadInfoDetail();
  }

  // Configurar response con icons de coberturas
  setDataItemCoverage(list: Array<ICoveragesPolicy>): Array<ICoverageItem> {
    this.itemListBase = COVERAGES_HOUSEHOLD;

    return list.map(item => {
      const iconC = this.itemListBase.find(c => {
        return c.key === item.llave;
      });
      const itemCoverage: ICoverageItem = {
        title: item.tituloInfo,
        description: item.descripcionInfo,
        question: item.accionTexto,
        questionBool: item.accion,
        routeKey: item.accionLlave,
        iconKey: item.llave,
        icon: (iconC && iconC.icon) || ICON_COVERAGE_DEFAULT
      };

      return itemCoverage;
    });
  }

  trackByFn(index): any {
    return index;
  }

  getAddress(house: IPolicyHousesResponse): string {
    return `${house.direccion}, ${house.distritoDescripcion}, ${house.provinciaDescripcion}, ${house.departamentoDescripcion}`;
  }
}
