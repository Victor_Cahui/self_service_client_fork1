import { Component } from '@angular/core';
import { EXCLUSIONS } from '@mx/settings/lang/household.lang';

@Component({
  selector: 'client-household-exclusions-component',
  templateUrl: './household-exclusions.component.html'
})
export class HouseholdExclusionsComponent {
  content = EXCLUSIONS;
}
