import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HouseholdCoveragesComponent } from './household-coverages.component';

const routes: Routes = [
  {
    path: '',
    component: HouseholdCoveragesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HouseholdCoveragesRoutingModule {}
