import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardIconsCoverageModule } from '@mx/components/shared/card-icons-coverage/card-icons-coverage.module';
import { SublimitsTableModule } from '@mx/components/shared/sublimits-table/sublimits-table.module';
import { MfSelectModule } from '@mx/core/ui/lib/components/forms/select';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { MfModalModule } from '@mx/core/ui/lib/components/modals/modal/modal.module';
import { HouseholdCoveragesComponent } from './household-coverages.component';
import { HouseholdCoveragesRoutingModule } from './household-coverages.routing';

@NgModule({
  imports: [
    CommonModule,
    HouseholdCoveragesRoutingModule,
    MfSelectModule,
    MfLoaderModule,
    MfModalModule,
    CardIconsCoverageModule,
    SublimitsTableModule
  ],
  declarations: [HouseholdCoveragesComponent]
})
export class HouseholdCoveragesModule {}
