import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfSelectModule } from '@mx/core/ui/lib/components/forms/select';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { HouseholdDeductiblesComponent } from './household-deductibles.component';
import { HouseholdDeductiblesRoutingModule } from './household-deductibles.routing';

@NgModule({
  imports: [CommonModule, HouseholdDeductiblesRoutingModule, MfSelectModule, MfLoaderModule],
  declarations: [HouseholdDeductiblesComponent]
})
export class HouseholdDeductiblesModule {}
