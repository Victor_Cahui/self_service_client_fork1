import { Component, HostBinding, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { ObjectUtil } from '@mx/core/shared/helpers/util/object';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { HomeService } from '@mx/services/home/home.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { DEDUCIBLES, HouseholdLang } from '@mx/settings/lang/household.lang';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import { IHomeDeductiblesResponse } from '@mx/statemanagement/models/home.interface';
import { IPolicyHousesResponse } from '@mx/statemanagement/models/policy.interface';
import { HouseHoldCoverageBase } from '../household-coverage-base';

@Component({
  selector: 'client-household-deductibles-component',
  templateUrl: './household-deductibles.component.html'
})
export class HouseholdDeductiblesComponent extends HouseHoldCoverageBase implements OnInit {
  @HostBinding('class') class = 'w-100';

  houseList: any;
  content = ObjectUtil.clone(DEDUCIBLES);
  houseSelected: string;
  labelHouse = PolicyLang.Labels.HouseSelect;
  deductiblesList: Array<IHomeDeductiblesResponse>;

  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService,
    protected policiesService: PoliciesService,
    protected headerHelperService: HeaderHelperService,
    protected activatedRoute: ActivatedRoute,
    protected localStorageService: LocalStorageService,
    protected router: Router,
    protected homeService: HomeService,
    protected _Autoservicios: Autoservicios
  ) {
    super(
      clientService,
      policiesInfoService,
      configurationService,
      authService,
      policiesService,
      homeService,
      headerHelperService,
      activatedRoute,
      localStorageService,
      router,
      _Autoservicios
    );
  }

  ngOnInit(): void {
    this.auth = this.authService.retrieveEntity();
    this.getHouses();
  }

  // Lista de casas
  getHouses(): void {
    this.showLoading = true;
    this.messageError = '';
    this.homeServiceSub = this.policiesService
      .houses({
        codigoApp: APPLICATION_CODE,
        numeroPoliza: this.policyNumber
      })
      .subscribe(
        (response: Array<IPolicyHousesResponse>) => {
          if (response && response.length) {
            this.houseList = [];
            // Si hay una casa muestra los deducibles
            const oneHouse = response.length === 1;
            const house = response[0];
            this.houseSelected = `${house.numeroSuplemento}-${house.numeroRiesgo}-${house.codRamo}`;
            if (oneHouse) {
              this.content.content = HouseholdLang.Messages.deductiblesOneHouse.replace(
                '{{policyName}}',
                house.descripcionProducto ? house.descripcionProducto.toUpperCase() : ''
              );
            } else {
              this.content.content = DEDUCIBLES.content.replace(
                '{{policyName}}',
                house.descripcionProducto ? house.descripcionProducto.toUpperCase() : ''
              );
              // Llena Select
              response.forEach(item => {
                this.houseList.push({
                  text: this.getAddress(item),
                  value: `${item.numeroSuplemento}-${item.numeroRiesgo}-${item.codRamo}`,
                  selected: false,
                  disabled: false
                });
              });
            }
            this.content.table.title = DEDUCIBLES.table.title.replace(
              '{{policyName}}',
              house.descripcionProducto ? house.descripcionProducto.toUpperCase() : ''
            );
            this.getDeductibles(this.houseSelected);
          } else {
            this.content.content = '';
            this.messageError = `${GeneralLang.Alert.titleFail} ${GeneralLang.Alert.sendData.error.message}`;
          }
        },
        () => {
          this.showLoading = false;
        },
        () => {
          this.showLoading = false;
          this.homeServiceSub.unsubscribe();
        }
      );
  }

  // Deductibles por Casa
  getDeductibles(house): void {
    // tslint:disable-next-line: no-parameter-reassignment
    house = house.split('-');
    this.showLoading = true;
    this.messageError = '';
    this.deductiblesList = [];
    this.deductiblesSub = this.homeService
      .getDeductibles({
        codigoApp: APPLICATION_CODE,
        numeroPoliza: this.policyNumber,
        numeroSuplemento: house[0],
        numeroRiesgo: house[1],
        codRamo: house[2]
      })
      .subscribe(
        (response: Array<IHomeDeductiblesResponse>) => {
          if (response && response.length) {
            this.deductiblesList = response;
            this.messageError = '';
          } else {
            this.content.content = '';
            this.messageError = `${GeneralLang.Alert.titleFail} ${GeneralLang.Alert.sendData.error.message}`;
          }
        },
        () => {
          this.content.content = '';
          this.showLoading = false;
          this.messageError = `${GeneralLang.Alert.titleFail} ${GeneralLang.Alert.sendData.error.message}`;
        },
        () => {
          this.showLoading = false;
          this.deductiblesSub.unsubscribe();
        }
      );
  }

  changeHouse(house): void {
    this.houseSelected = house;
    this.getDeductibles(house);
  }
}
