import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardContentModule } from '@mx/components/shared/card-content/card-content.module';
import { DownLoadConditionedModule } from '@mx/components/shared/download-conditioned/download-conditioned.module';
import { HouseholdExclusionsComponent } from './household-exclusions.component';
import { HouseholdExclusionsRoutingModule } from './household-exclusions.routing';

@NgModule({
  imports: [CommonModule, HouseholdExclusionsRoutingModule, CardContentModule, DownLoadConditionedModule],
  declarations: [HouseholdExclusionsComponent]
})
export class HouseholdExclusionsModule {}
