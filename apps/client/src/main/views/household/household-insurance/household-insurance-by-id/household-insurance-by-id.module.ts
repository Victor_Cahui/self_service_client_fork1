import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfTabTopModule } from '@mx/core/ui/lib/components/tabs/tabs-top/tab-top.module';
import { HouseholdInsuranceByIdComponent } from './household-insurance-by-id.component';
import { HouseholdInsuranceByIdRoutingModule } from './household-insurance-by-id.routing';

@NgModule({
  imports: [CommonModule, HouseholdInsuranceByIdRoutingModule, MfTabTopModule],
  declarations: [HouseholdInsuranceByIdComponent]
})
export class HouseholdInsuranceByIdModule {}
