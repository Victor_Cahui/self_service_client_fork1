import { Component, HostBinding, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { MfModal } from '@mx/core/ui/lib/components/modals/modal/public-api';
import { environment } from '@mx/environments/environment';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { HomeService } from '@mx/services/home/home.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { HOUSEHOLD_COVERAGE_TABS } from '@mx/settings/constants/router-tabs';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { COVERAGES, COVERAGES_SUBLIMITS, HouseholdLang } from '@mx/settings/lang/household.lang';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import { Content } from '@mx/statemanagement/models/general.models';
import { IHomeCoverageResponse, IHomeCoveragesListGroup } from '@mx/statemanagement/models/home.interface';
import { IPolicyHousesResponse } from '@mx/statemanagement/models/policy.interface';
import { Subscription } from 'rxjs';
import { HouseHoldCoverageBase } from '../household-coverage-base';

@Component({
  selector: 'client-household-coverages-component',
  templateUrl: './household-coverages.component.html'
})
export class HouseholdCoveragesComponent extends HouseHoldCoverageBase implements OnInit {
  @HostBinding('class') class = 'w-100';
  @ViewChild(MfModal) mfModal: MfModal;

  houseList: any;
  content: Content;
  houseSelected: string;
  labelHouse = PolicyLang.Labels.HouseSelect;
  linkSublimits = PolicyLang.Buttons.ShowSublimits;
  title = COVERAGES_SUBLIMITS.title;
  titleMobile = HOUSEHOLD_COVERAGE_TABS[0].name;
  subTitleMobile = POLICY_TYPES.MD_HOGAR.title;
  coveragesSub: Subscription;
  messageError: string;
  coveragesListGroup: Array<IHomeCoveragesListGroup>;

  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService,
    protected policiesService: PoliciesService,
    protected headerHelperService: HeaderHelperService,
    protected activatedRoute: ActivatedRoute,
    protected localStorageService: LocalStorageService,
    protected router: Router,
    protected homeService: HomeService,
    protected _Autoservicios: Autoservicios
  ) {
    super(
      clientService,
      policiesInfoService,
      configurationService,
      authService,
      policiesService,
      homeService,
      headerHelperService,
      activatedRoute,
      localStorageService,
      router,
      _Autoservicios
    );
  }

  ngOnInit(): void {
    this.content = { title: COVERAGES.title, content: '' };
    this.auth = this.authService.retrieveEntity();
    this.getHouses();
  }

  // Lista de casas
  getHouses(): void {
    this.showLoading = true;
    this.messageError = '';
    this.homeServiceSub = this.policiesService
      .houses({
        codigoApp: APPLICATION_CODE,
        numeroPoliza: this.policyNumber
      })
      .subscribe(
        (response: Array<IPolicyHousesResponse>) => {
          if (response && response.length) {
            this.houseList = [];
            // Si hay una casa muestra las coberturas
            const oneHouse = response.length === 1;
            const house = response[0];
            this.houseSelected = `${house.numeroSuplemento}-${house.numeroRiesgo}-${house.codRamo}`;
            if (oneHouse) {
              this.content.content = HouseholdLang.Messages.coverageOneHouse.replace(
                '{{policyName}}',
                house.descripcionProducto ? house.descripcionProducto.toUpperCase() : ''
              );
            } else {
              this.content.content = COVERAGES.content.replace(
                '{{policyName}}',
                house.descripcionProducto ? house.descripcionProducto.toUpperCase() : ''
              );
              // Llena Select
              response.forEach(item => {
                this.houseList.push({
                  text: this.getAddress(item),
                  value: `${item.numeroSuplemento}-${item.numeroRiesgo}-${item.codRamo}`,
                  selected: false,
                  disabled: false
                });
              });
            }
            this.getCoverages(this.houseSelected);
          } else {
            this.content.content = '';
            this.messageError = `${GeneralLang.Alert.titleFail} ${GeneralLang.Alert.sendData.error.message}`;
          }
        },
        () => {
          this.showLoading = false;
        },
        () => {
          this.showLoading = false;
          this.homeServiceSub.unsubscribe();
        }
      );
  }

  // Coberturas por Casa
  getCoverages(house): void {
    // tslint:disable-next-line: no-parameter-reassignment
    house = house.split('-');
    this.showLoading = true;
    this.messageError = '';
    this.coveragesSub = this.homeService
      .getCoverages({
        codigoApp: APPLICATION_CODE,
        numeroPoliza: this.policyNumber,
        numeroSuplemento: house[0],
        numeroRiesgo: house[1],
        codRamo: house[2]
      })
      .subscribe(
        (response: Array<IHomeCoverageResponse>) => {
          if (response && response.length) {
            this.coveragesListGroup = [];
            response.forEach((item: any) => {
              this.coveragesListGroup.push({
                title: item.titulo,
                description: item.descripcion,
                coverages: this.setDataItemCoverage(item.coberturas),
                sublimits: item.sublimite
              });
            });
            this.messageError = '';
            if (!environment.ACTIVATE_OTHERS_COVERAGES) {
              this.coveragesListGroup = this.coveragesListGroup.filter(
                e => !['OTRAS COBERTURAS', 'OTROS'].includes(e.title.toUpperCase())
              );
            }
          } else {
            this.content.content = '';
            this.messageError = `${GeneralLang.Alert.titleFail} ${GeneralLang.Alert.sendData.error.message}`;
          }
        },
        () => {
          this.content.content = '';
          this.showLoading = false;
          this.messageError = `${GeneralLang.Alert.titleFail} ${GeneralLang.Alert.sendData.error.message}`;
        },
        () => {
          this.showLoading = false;
          this.coveragesSub.unsubscribe();
        }
      );
  }

  changeHouse(house): void {
    this.houseSelected = house;
    this.getCoverages(house);
  }

  showSubLimits(sublimits): void {
    this.sublimitList = sublimits;
    this.openModal = true;
    setTimeout(() => {
      this.mfModal.open();
    }, 100);
  }
}
