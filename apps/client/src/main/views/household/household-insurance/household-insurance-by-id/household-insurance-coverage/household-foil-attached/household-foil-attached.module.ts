import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FoilAttachedModule } from '@mx/components/shared/foil-attached/foil-attached.module';
import { HouseholdFoilAttachedComponent } from './household-foil-attached.component';
import { HouseholdFoilAttachedRoutingModule } from './household-foil-attached.routing';

@NgModule({
  imports: [CommonModule, HouseholdFoilAttachedRoutingModule, FoilAttachedModule],
  declarations: [HouseholdFoilAttachedComponent]
})
export class HouseholdFoilAttachedModule {}
