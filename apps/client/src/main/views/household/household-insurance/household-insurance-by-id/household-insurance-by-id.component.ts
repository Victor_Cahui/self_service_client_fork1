import { Component, OnDestroy, OnInit } from '@angular/core';
import { UnsubscribeOnDestroy } from '@mx/core/shared/helpers/util/unsubscribe-on-destroy';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { HOUSEHOLD_INSURANCE_TAB } from '@mx/settings/constants/router-tabs';
import { ITabItem } from '@mx/statemanagement/models/router.interface';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'client-household-insurance-by-id-component',
  templateUrl: './household-insurance-by-id.component.html'
})
export class HouseholdInsuranceByIdComponent extends UnsubscribeOnDestroy implements OnDestroy, OnInit {
  tabItemsList: Array<ITabItem>;

  constructor(
    private readonly headerHelperService: HeaderHelperService,
    private readonly policiesInfoService: PoliciesInfoService
  ) {
    super();
    this.tabItemsList = HOUSEHOLD_INSURANCE_TAB();
  }

  ngOnInit(): void {
    this.headerHelperService.setTab(true);
    this.verifyCoveragesTab();
  }

  ngOnDestroy(): void {
    this.headerHelperService.setTab(false);
  }

  verifyCoveragesTab(): void {
    this.policiesInfoService
      .getPolicyBeh()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(policy => {
        if (!policy) {
          return void 0;
        }

        policy.esBeneficioAdicional &&
          (this.tabItemsList = this.policiesInfoService.hideCoveragesTab(this.tabItemsList));
      });
  }
}
