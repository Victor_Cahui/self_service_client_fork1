import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HouseholdFoilAttachedComponent } from './household-foil-attached.component';

const routes: Routes = [
  {
    path: '',
    component: HouseholdFoilAttachedComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HouseholdFoilAttachedRoutingModule {}
