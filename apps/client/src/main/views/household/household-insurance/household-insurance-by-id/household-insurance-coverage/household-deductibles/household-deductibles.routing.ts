import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HouseholdDeductiblesComponent } from './household-deductibles.component';

const routes: Routes = [
  {
    path: '',
    component: HouseholdDeductiblesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HouseholdDeductiblesRoutingModule {}
