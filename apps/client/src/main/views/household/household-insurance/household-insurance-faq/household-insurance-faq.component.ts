import { Component } from '@angular/core';
import { HOUSEHOLD_INSURANCE_FAQS } from '@mx/settings/faqs/household-insurance.faq';

@Component({
  selector: 'client-household-insurance-faq-component',
  templateUrl: 'household-insurance-faq.component.html'
})
export class HouseholdInsuranceFAQComponent {
  faqs: Array<{ title: string; content: string; collapse: boolean }>;

  constructor() {
    this.faqs = HOUSEHOLD_INSURANCE_FAQS.map(faq => ({ title: faq.title, content: faq.content, collapse: true }));
  }
}
