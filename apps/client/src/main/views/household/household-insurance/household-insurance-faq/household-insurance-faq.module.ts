import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardMoreFaqModule } from '@mx/components/shared/card-more-faq/card-more-faq.module';
import { HouseholdInsuranceFAQComponent } from './household-insurance-faq.component';
import { HouseholdInsuranceFAQRoutingModule } from './household-insurance-faq.routing';

@NgModule({
  imports: [CommonModule, CardMoreFaqModule, HouseholdInsuranceFAQRoutingModule],
  declarations: [HouseholdInsuranceFAQComponent]
})
export class HouseholdInsuranceFAQModule {}
