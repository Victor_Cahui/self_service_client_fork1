import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HouseholdInsuranceFAQComponent } from './household-insurance-faq.component';

const routes: Routes = [
  {
    path: '',
    component: HouseholdInsuranceFAQComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HouseholdInsuranceFAQRoutingModule {}
