import { Component, OnDestroy, OnInit } from '@angular/core';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { MFP_Danhos_y_Robos_33A } from '@mx/settings/constants/events.analytics';
import { HURT_AND_STEAL_TAB } from '@mx/settings/constants/router-tabs';
import { ITabItem } from '@mx/statemanagement/models/router.interface';

@Component({
  selector: 'client-hurt-and-steal-component',
  templateUrl: './hurt-and-steal.component.html'
})
export class HurtAndStealComponent implements OnDestroy, OnInit {
  tabItemsList: Array<ITabItem> = HURT_AND_STEAL_TAB;

  ga: Array<IGaPropertie>;

  constructor(private readonly headerHelperService: HeaderHelperService) {}

  ngOnInit(): void {
    this.headerHelperService.setTab(true);
    this.ga = [MFP_Danhos_y_Robos_33A()];
  }

  ngOnDestroy(): void {
    this.headerHelperService.setTab(false);
  }
}
