import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HurtAndStealComponent } from './hurt-and-steal.component';

const routes: Routes = [
  {
    path: '',
    component: HurtAndStealComponent,
    children: [
      {
        path: '',
        redirectTo: 'reports',
        pathMatch: 'prefix'
      },
      {
        path: 'reports',
        loadChildren: './hurt-and-steal-report/hurt-and-steal-report.module#HurtAndStealReportModule'
      },
      {
        path: 'assists',
        loadChildren: './hurt-and-steal-assists/hurt-and-steal-assists.module#HurtAndStealAssistsModule'
      }
    ]
  },
  {
    path: 'declare-sinister-home',
    loadChildren: './declare-sinister-home/declare-sinister-home.module#DeclareSinisterHomeModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HurtAndStealRoutingModule {}
