import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NotificationHelpModule } from '@mx/components/notifications/notification-help/notification-help.module';
import { BannerModule } from '@mx/components/shared/banner/banner.module';
import { LandingModule } from '@mx/components/shared/landing/landing.module';
import { PageTitleModule } from '@mx/components/shared/page-title/page-title.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { MfCardModule } from '@mx/core/ui';
import { MfButtonModule } from '@mx/core/ui/lib/components/forms/button';
import { HurtAndStealReportComponent } from './hurt-and-steal-report.component';
import { HurtAndStealReportRouting } from './hurt-and-steal-report.routing';

@NgModule({
  imports: [
    CommonModule,
    HurtAndStealReportRouting,
    PageTitleModule,
    MfCardModule,
    NotificationHelpModule,
    BannerModule,
    LandingModule,
    MfButtonModule,
    GoogleModule
  ],
  declarations: [HurtAndStealReportComponent]
})
export class HurtAndStealReportModule {}
