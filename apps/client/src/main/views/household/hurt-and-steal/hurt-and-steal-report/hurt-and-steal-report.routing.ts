import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HurtAndStealReportComponent } from './hurt-and-steal-report.component';

const routes: Routes = [
  {
    path: '',
    component: HurtAndStealReportComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HurtAndStealReportRouting {}
