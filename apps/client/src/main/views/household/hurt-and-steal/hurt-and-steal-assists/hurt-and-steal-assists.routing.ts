import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HurtAndStealAssistsComponent } from './hurt-and-steal-assists.component';

const routes: Routes = [
  {
    path: '',
    component: HurtAndStealAssistsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HurtAndStealAssistsRouting {}
