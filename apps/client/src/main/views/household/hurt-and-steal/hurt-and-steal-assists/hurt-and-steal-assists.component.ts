import { Component, OnInit } from '@angular/core';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { HOUSE_HURTANDSTEAL_HEADER } from '@mx/settings/constants/router-titles';

@Component({
  selector: 'client-hurt-and-steal-assists',
  templateUrl: './hurt-and-steal-assists.component.html'
})
export class HurtAndStealAssistsComponent implements OnInit {
  constructor(private readonly headerHelperService: HeaderHelperService) {}

  ngOnInit(): void {
    this.headerHelperService.set(HOUSE_HURTANDSTEAL_HEADER);
  }
}
