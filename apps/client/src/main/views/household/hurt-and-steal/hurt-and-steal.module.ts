import { NgModule } from '@angular/core';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { MfTabTopModule } from '@mx/core/ui/lib/components/tabs/tabs-top';
import { HurtAndStealComponent } from './hurt-and-steal.component';
import { HurtAndStealRoutingModule } from './hurt-and-steal.routing';

@NgModule({
  imports: [GoogleModule, HurtAndStealRoutingModule, MfTabTopModule],
  exports: [],
  declarations: [HurtAndStealComponent],
  providers: []
})
export class HurtAndStealModule {}
