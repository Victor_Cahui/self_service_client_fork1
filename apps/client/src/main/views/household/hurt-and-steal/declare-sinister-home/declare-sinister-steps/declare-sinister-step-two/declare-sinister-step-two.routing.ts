import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DeclareSinisterStepTwoComponent } from './declare-sinister-step-two.component';

const routes: Routes = [
  {
    path: '',
    component: DeclareSinisterStepTwoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeclareSinisterStepTwoRoutingModule {}
