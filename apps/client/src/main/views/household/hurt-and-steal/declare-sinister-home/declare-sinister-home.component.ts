import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ArrayUtil } from '@mx/core/shared/helpers/util/array';
import { dateToStringFormatYYYYMMDD } from '@mx/core/shared/helpers/util/date';
import { CanComponentDeactivate } from '@mx/guards/declare-sinester/declare-sinester-deactivate.guard';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { GeneralService } from '@mx/services/general/general.service';
import { InitModalService } from '@mx/services/general/init-modal.service';
import { DeclareSinisterHomeService } from '@mx/services/home/declare-sinister-home.service';
import { HomeService } from '@mx/services/home/home.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { DIAS_ANTERI_REGIST_INCIDE_HOGAR, MAX_SIZE_MB_IMAGE_PROFILE } from '@mx/settings/constants/key-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { IClientAddressResponse } from '@mx/statemanagement/models/client.models';
import { IParameterApp } from '@mx/statemanagement/models/general.models';
import { IListHoursResponse, IListIncidentTypesResponse } from '@mx/statemanagement/models/home.interface';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { Observable, Subscription, zip } from 'rxjs';

@Component({
  selector: 'client-declare-sinister-home-component',
  templateUrl: './declare-sinister-home.component.html'
})
export class DeclareSinisterHomeComponent implements OnInit, OnDestroy, CanComponentDeactivate {
  entity: IdDocument;
  sinisterSub: Subscription;
  closeModalEventSub: Subscription;
  load: boolean;
  parametersSubject: Observable<Array<IParameterApp>>;
  daysAfterIncident: string;
  maxSizeImage: number;

  constructor(
    private readonly generalService: GeneralService,
    private readonly clientService: ClientService,
    private readonly homeService: HomeService,
    private readonly authService: AuthService,
    private readonly declareSinisterHomeService: DeclareSinisterHomeService,
    private readonly initModalService: InitModalService,
    private readonly router: Router
  ) {
    this.load = false;
  }

  ngOnInit(): void {
    this.entity = this.getEntity();
    const incidentHours$: Observable<Array<IListHoursResponse>> = this.getListIncidentHours();
    const address$: Observable<Array<IClientAddressResponse>> = this.getAddress();
    const incidentTypes$: Observable<Array<IListIncidentTypesResponse>> = this.getListIncidentTypes();
    this.getParameters();
    this.sinisterSub = zip(incidentHours$, address$, incidentTypes$).subscribe(
      (res: Array<any>) => {
        this.load = true;
        // Ordenar lista de incidentes
        res[2] = ArrayUtil.arraySortByKey(res[2], 'descripcionIncidente');

        this.declareSinisterHomeService.setConstantsRequired(
          this.daysAfterIncident,
          this.maxSizeImage,
          res[0],
          res[1],
          res[2]
        );
      },
      () => {}
    );

    this.closeModalEventSub = this.initModalService.getCloseModalEvent().subscribe(() => {
      if (this.router.url === '/household/hurt-and-steal/declare-sinister-home/step/1') {
        this.router.navigate(['/household/hurt-and-steal/reports']);
      }
    });
  }

  getListIncidentHours(): Observable<Array<IListHoursResponse>> {
    const curentDay = dateToStringFormatYYYYMMDD(new Date());

    return this.homeService.ListIncidentHours({ codigoApp: APPLICATION_CODE, fecha: curentDay });
  }

  getAddress(): Observable<Array<IClientAddressResponse>> {
    return this.clientService.getAddress({
      tipoPoliza: POLICY_TYPES.MD_HOGAR.code
    });
  }

  getParameters(): void {
    this.parametersSubject = this.generalService.getParametersSubject();
    this.parametersSubject.subscribe(params => {
      this.daysAfterIncident = this.generalService.getValueParams(DIAS_ANTERI_REGIST_INCIDE_HOGAR) || '';
      this.maxSizeImage = parseInt(this.generalService.getValueParams(MAX_SIZE_MB_IMAGE_PROFILE), 10) || null;
    });
  }

  getEntity(): IdDocument {
    return this.authService.retrieveEntity();
  }

  getListIncidentTypes(): Observable<Array<IListIncidentTypesResponse>> {
    return this.homeService.ListIncidentTypes({ codigoApp: APPLICATION_CODE });
  }

  ngOnDestroy(): void {
    if (this.sinisterSub) {
      this.sinisterSub.unsubscribe();
    }
    if (this.closeModalEventSub) {
      this.closeModalEventSub.unsubscribe();
    }
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    this.declareSinisterHomeService.reset();
    this.declareSinisterHomeService.removeLocalStorage();

    return true;
  }
}
