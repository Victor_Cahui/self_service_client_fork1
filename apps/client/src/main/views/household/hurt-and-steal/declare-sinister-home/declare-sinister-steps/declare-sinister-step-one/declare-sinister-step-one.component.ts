import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { dateToStringFormatYYYYMMDD } from '@mx/core/shared/helpers/util/date';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { UnsubscribeOnDestroy } from '@mx/core/shared/helpers/util/unsubscribe-on-destroy';
import { SelectListItem } from '@mx/core/ui/lib/components/forms/select/select-item';
import { DeclareSinisterHomeService } from '@mx/services/home/declare-sinister-home.service';
import { HomeService } from '@mx/services/home/home.service';
import { MFP_Danhos_y_Robos_Reportar_Asistencia_Paso_1_34A } from '@mx/settings/constants/events.analytics';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { DECLARE_SINISTER_HOME_STEP_1 } from '@mx/settings/lang/household.lang';
import { IDeclareSinister, IListAddress, IListHoursResponse } from '@mx/statemanagement/models/home.interface';
import isEmpty from 'lodash-es/isEmpty';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'client-declare-sinister-step-one-component',
  templateUrl: './declare-sinister-step-one.component.html'
})
export class DeclareSinisterStepOneComponent extends UnsubscribeOnDestroy implements OnInit {
  dayAfterIncident: string;
  listIncidentHours: Array<IListHoursResponse>;
  listAddress: Array<IListAddress>;
  sinester: IDeclareSinister;

  formCreate: FormGroup;
  address: AbstractControl;
  dateIncident: AbstractControl;
  hourIncident: AbstractControl;
  limitDateMin: Date;
  limitDateMax: Date;
  arrHours: Array<SelectListItem>;

  titleLang: string;
  addressLang: string;
  safeLang: string;
  dateAndTimeOfTheIncidentLang: string;
  labelDateLang: string;
  labelHourLang: string;
  btnCancelLang: string;
  btnSuccessLang: string;

  gaCancel: IGaPropertie;
  gaNext: IGaPropertie;

  constructor(
    private readonly fb: FormBuilder,
    private readonly declareSinisterHomeService: DeclareSinisterHomeService,
    private readonly homeService: HomeService,
    private readonly router: Router
  ) {
    super();
    this.listIncidentHours = [];
    this.listAddress = [];
    this.arrHours = [];
    this.titleLang = DECLARE_SINISTER_HOME_STEP_1.title;
    this.addressLang = DECLARE_SINISTER_HOME_STEP_1.address;
    this.safeLang = DECLARE_SINISTER_HOME_STEP_1.safe;
    this.dateAndTimeOfTheIncidentLang = DECLARE_SINISTER_HOME_STEP_1.dateAndTimeOfTheIncident;
    this.labelDateLang = DECLARE_SINISTER_HOME_STEP_1.form.labelDate;
    this.labelHourLang = DECLARE_SINISTER_HOME_STEP_1.form.labelHour;
    this.btnCancelLang = DECLARE_SINISTER_HOME_STEP_1.form.btnCancel;
    this.btnSuccessLang = DECLARE_SINISTER_HOME_STEP_1.form.btnSuccess;
    this.gaCancel = MFP_Danhos_y_Robos_Reportar_Asistencia_Paso_1_34A(this.btnCancelLang);
    this.gaNext = MFP_Danhos_y_Robos_Reportar_Asistencia_Paso_1_34A(this.btnSuccessLang);
  }

  ngOnInit(): void {
    this.beforeInit();
    this.createForm(this.sinester);
    this.formCreate
      .get('dateIncident')
      .valueChanges.pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(selectedDate => {
        this.getListIncidentHours(selectedDate);
      });
  }

  beforeInit(): void {
    this.dayAfterIncident = this.declareSinisterHomeService.getDayAfterIncident();
    this.listIncidentHours = this.declareSinisterHomeService.getListIncidentHours();
    this.arrHours = this.setHoursData(this.listIncidentHours);
    this.listAddress = this.declareSinisterHomeService.getListAddress();
    const dateNow = new Date();
    dateNow.setHours(0, 0, 0, 0);
    this.limitDateMin = new Date(dateNow.setDate(dateNow.getDate() - parseInt(this.dayAfterIncident, 10)));
    this.limitDateMax = new Date();
    this.sinester = this.declareSinisterHomeService.getSinester();
  }

  onSubmit(): void {
    const hourId = parseInt(this.hourIncident.value, 10);
    const address = this.listAddress.find((obj: IListAddress) => obj.id === this.address.value);
    const hour = this.listIncidentHours.find((obj: IListHoursResponse) => obj.identificadorRango === hourId);
    this.declareSinisterHomeService.successSteep1(address, this.dateIncident.value, hour, hourId);
    this.router.navigate(['/household/hurt-and-steal/declare-sinister-home/step/2']);
  }

  createForm(sinester: IDeclareSinister): void {
    this.formCreate = this.fb.group({
      address: [{ value: !!sinester.address ? sinester.address.id : '', disabled: false }, Validators.required],
      dateIncident: [{ value: !!sinester.date ? sinester.date : '', disabled: false }, Validators.required],
      hourIncident: [{ value: !!sinester.hourId ? sinester.hourId : '', disabled: false }, Validators.required]
    });
    this.address = this.formCreate.get('address');
    this.dateIncident = this.formCreate.get('dateIncident');
    this.hourIncident = this.formCreate.get('hourIncident');
  }

  setHoursData(hourIncident: Array<IListHoursResponse>): Array<SelectListItem> {
    // tslint:disable-next-line: no-parameter-reassignment
    hourIncident = [...hourIncident];
    const arr = hourIncident.map(
      (obj: IListHoursResponse) =>
        new SelectListItem({
          text: obj.descripcionRango,
          value: obj.identificadorRango
        })
    );

    return [...arr];
  }

  showErrors(control: AbstractControl | FormControl): boolean {
    return !!control && !isEmpty(control.errors) && (control.touched || control.dirty);
  }

  trackByFn(index, item): any {
    return index;
  }

  goToHome(): void {
    this.router.navigate(['/household/hurt-and-steal/reports']);
  }

  onSelectAddress(val: number): void {
    this.address.setValue(val);
  }

  getListIncidentHours(val: any): void {
    const selectedDay = dateToStringFormatYYYYMMDD(val);
    this.homeService
      .ListIncidentHours({ codigoApp: APPLICATION_CODE, fecha: selectedDay })
      .subscribe((res: Array<any>) => {
        this.listIncidentHours = res;
        this.arrHours = this.setHoursData(this.listIncidentHours);
        this.formCreate.controls['hourIncident'].setValue('');
      });
  }
}
