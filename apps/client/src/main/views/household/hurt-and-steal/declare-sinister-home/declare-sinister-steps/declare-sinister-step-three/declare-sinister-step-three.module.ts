import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { MfModalAlertModule } from '@mx/core/ui/lib/components/modals/modal-alert/modal-alert.module';
import { MfModalMessageModule } from '@mx/core/ui/lib/components/modals/modal-message/modal-message.module';
import { MfModalModule } from '@mx/core/ui/lib/components/modals/modal/modal.module';
import { NgDropModule } from '@mx/core/ui/lib/directives/ng-drop/ng-drop.module';
import { DeclareSinisterStepThreeComponent } from './declare-sinister-step-three.component';
import { DeclareSinisterStepThreeRoutingModule } from './declare-sinister-step-three.routing';

@NgModule({
  imports: [
    CommonModule,
    DeclareSinisterStepThreeRoutingModule,
    GoogleModule,
    MfModalAlertModule,
    MfModalMessageModule,
    MfModalModule,
    NgDropModule
  ],
  declarations: [DeclareSinisterStepThreeComponent]
})
export class DeclareSinisterStepThreeModule {}
