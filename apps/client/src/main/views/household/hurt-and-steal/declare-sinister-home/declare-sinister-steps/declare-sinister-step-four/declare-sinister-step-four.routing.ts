import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DeclareSinisterStepFourComponent } from './declare-sinister-step-four.component';

const routes: Routes = [
  {
    path: '',
    component: DeclareSinisterStepFourComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeclareSinisterStepFourRoutingModule {}
