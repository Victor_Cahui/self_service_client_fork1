import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DeclareSinisterStepThreeComponent } from './declare-sinister-step-three.component';

const routes: Routes = [
  {
    path: '',
    component: DeclareSinisterStepThreeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeclareSinisterStepThreeRoutingModule {}
