import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { MfInputDatepickerModule } from '@mx/core/ui/lib/components/forms/input-datepicker/input-datepicker.module';
import { MfInputModule } from '@mx/core/ui/lib/components/forms/input/input.module';
import { MfSelectModule } from '@mx/core/ui/lib/components/forms/select/select.module';
import { MfShowErrorsModule } from '@mx/core/ui/lib/components/global/show-errors/show-errors.module';
import { DeclareSinisterStepOneComponent } from './declare-sinister-step-one.component';
import { DeclareSinisterStepOneRoutingModule } from './declare-sinister-step-one.routing';

@NgModule({
  imports: [
    CommonModule,
    DeclareSinisterStepOneRoutingModule,
    MfInputModule,
    MfInputDatepickerModule,
    MfSelectModule,
    MfShowErrorsModule,
    ReactiveFormsModule,
    GoogleModule
  ],
  declarations: [DeclareSinisterStepOneComponent]
})
export class DeclareSinisterStepOneModule {}
