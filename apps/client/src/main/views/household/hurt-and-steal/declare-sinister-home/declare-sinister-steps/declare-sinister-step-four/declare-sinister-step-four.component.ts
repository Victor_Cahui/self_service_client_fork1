import { DatePipe } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FileUtil } from '@mx/core/shared/helpers/util/file';
import { GAService, IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google/google.gabase';
import { AuthService } from '@mx/services/auth/auth.service';
import { DeclareSinisterHomeService } from '@mx/services/home/declare-sinister-home.service';
import { HomeService } from '@mx/services/home/home.service';
import {
  MFP_Danhos_y_Robos_Reportar_Asistencia_Resumen_37A,
  MFP_Danhos_y_Robos_Reportar_Asistencia_Resumen_37B
} from '@mx/settings/constants/events.analytics';
import { DECLARE_SINISTER_HOME_STEP_4 } from '@mx/settings/lang/household.lang';
import {
  IDeclareSinister,
  IRegisterIncidentBody,
  IRegisterIncidentResponse
} from '@mx/statemanagement/models/home.interface';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-declare-sinister-step-four-component',
  templateUrl: './declare-sinister-step-four.component.html',
  providers: [DatePipe]
})
export class DeclareSinisterStepFourComponent extends GaUnsubscribeBase implements OnInit, OnDestroy {
  sinister: IDeclareSinister;
  address: string;
  incident: string;
  reason: string;
  credentials: IdDocument;
  isSubmitting: boolean;
  formSub: Subscription;

  titleLang: string;
  descriptionLang: string;
  step1TitleLang: string;
  step1LabelAddressLang: string;
  step1LabelDateLang: string;
  step1LabelHourLang: string;
  step2TitleLang: string;
  step2LabelType1Lang: string;
  step2LabelType2Lang: string;
  step3TitleLang: string;
  btnReturnLang: string;
  btnSuccessLang: string;
  btnLoadingLang: string;
  fileUtil: FileUtil;

  ga: Array<IGaPropertie>;

  constructor(
    private readonly declareSinisterHomeService: DeclareSinisterHomeService,
    private readonly homeService: HomeService,
    private readonly authService: AuthService,
    private readonly datePipe: DatePipe,
    private readonly router: Router,
    protected gaService?: GAService
  ) {
    super(gaService);
    this.titleLang = DECLARE_SINISTER_HOME_STEP_4.title;
    this.descriptionLang = DECLARE_SINISTER_HOME_STEP_4.description;
    this.step1TitleLang = DECLARE_SINISTER_HOME_STEP_4.step1.title;
    this.step1LabelAddressLang = DECLARE_SINISTER_HOME_STEP_4.step1.labelAddress;
    this.step1LabelDateLang = DECLARE_SINISTER_HOME_STEP_4.step1.labelDate;
    this.step1LabelHourLang = DECLARE_SINISTER_HOME_STEP_4.step1.labelHour;
    this.step2TitleLang = DECLARE_SINISTER_HOME_STEP_4.step2.title;
    this.step2LabelType1Lang = DECLARE_SINISTER_HOME_STEP_4.step2.labelType1;
    this.step2LabelType2Lang = DECLARE_SINISTER_HOME_STEP_4.step2.labelType2;
    this.step3TitleLang = DECLARE_SINISTER_HOME_STEP_4.step3.title;
    this.btnReturnLang = DECLARE_SINISTER_HOME_STEP_4.form.btnReturn;
    this.btnSuccessLang = DECLARE_SINISTER_HOME_STEP_4.form.btnSuccess;
    this.btnLoadingLang = DECLARE_SINISTER_HOME_STEP_4.form.btnLoading;
    this.isSubmitting = false;
    this.fileUtil = new FileUtil();
    this.ga = [
      MFP_Danhos_y_Robos_Reportar_Asistencia_Resumen_37A(),
      MFP_Danhos_y_Robos_Reportar_Asistencia_Resumen_37B()
    ];
  }

  ngOnInit(): void {
    this.sinister = this.declareSinisterHomeService.getSinester();
    this.address = this.declareSinisterHomeService.getAddressReview();
    this.incident = this.declareSinisterHomeService.getIncidentReview();
    this.reason = this.declareSinisterHomeService.getReasonReview();
    this.credentials = this.authService.retrieveEntity();
  }

  goToStep(route: number): void {
    const steps = [1, 2, 3];
    if (steps.includes(route)) {
      this.router.navigate([`/household/hurt-and-steal/declare-sinister-home/step/${route}`]);
    }
  }

  onSubmit(): void {
    if (!this.isSubmitting) {
      this.isSubmitting = true;
      this.fileUtil
        .compressImage(this.sinister.files)
        .then((files: Array<File>) => this.fileUtil.compressZip(files, 'fotosIncidente.zip'))
        .then((objZip: File) => {
          const body: IRegisterIncidentBody = {
            numeroPoliza: this.sinister.address.numeroPoliza,
            numeroSuplemento: this.sinister.address.numeroSuplemento,
            numeroRiesgo: this.sinister.address.numeroRiesgo,
            codRamo: this.sinister.address.codRamo,
            direccion: this.address,
            fecha: this.datePipe.transform(this.sinister.date, 'yyyy-MM-dd'),
            identificadorRango: this.sinister.hour.identificadorRango,
            rangoHorario: this.sinister.hour.descripcionRango,
            identificadorIncidente: this.sinister.idIncident,
            identificadorMotivo: this.sinister.idReason,
            descripcionIncidente: `${this.incident} - ${this.reason}`,
            fotosIncidente: objZip
          };
          this.formSub = this.homeService.RegisterIncident({}, body).subscribe(
            (res: IRegisterIncidentResponse) => {
              this.declareSinisterHomeService.successSteep4(res.numeroAsistencia);
              this.router.navigate([`/household/hurt-and-steal/declare-sinister-home/finalize`]);
            },
            () => {
              this.isSubmitting = false;
            }
          );
        });
    }
  }

  trackByFn(index, item): any {
    return index;
  }

  ngOnDestroy(): void {
    if (this.formSub) {
      this.formSub.unsubscribe();
    }
  }
}
