import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DeclareSinisterStepsComponent } from './declare-sinister-steps.component';
import { DeclareSinisterStepsRoutingModule } from './declare-sinister-steps.routing';

@NgModule({
  imports: [CommonModule, DeclareSinisterStepsRoutingModule],
  exports: [],
  declarations: [DeclareSinisterStepsComponent],
  providers: []
})
export class DeclareSinisterStepsModule {}
