import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { DeclareSinisterHomeService } from '@mx/services/home/declare-sinister-home.service';
import { MFP_Danhos_y_Robos_Reportar_Asistencia_Reportada_37C } from '@mx/settings/constants/events.analytics';
import { HOUSE_DECLARE_SINISTER_FINALIZE } from '@mx/settings/constants/router-titles';
import { DECLARE_SINISTER_HOME_FINALIZE } from '@mx/settings/lang/household.lang';

@Component({
  selector: 'client-declare-sinister-finalize-component',
  templateUrl: './declare-sinister-finalize.component.html'
})
export class DeclareSinisterFinalizeComponent implements OnInit {
  address: string;
  assistanceNumber: number;

  titleLang: string;
  messageLang: string;
  assistanceNumberLang: string;
  addressLang: string;
  contactLang: string;
  numberContactLang: string;
  btnHomeLang: string;
  gaAssistance: IGaPropertie;

  constructor(
    private readonly router: Router,
    private readonly declareSinisterHomeService: DeclareSinisterHomeService,
    private readonly headerHelperService: HeaderHelperService
  ) {
    this.titleLang = DECLARE_SINISTER_HOME_FINALIZE.title;
    this.messageLang = DECLARE_SINISTER_HOME_FINALIZE.message;
    this.assistanceNumberLang = DECLARE_SINISTER_HOME_FINALIZE.assistanceNumber;
    this.addressLang = DECLARE_SINISTER_HOME_FINALIZE.address;
    this.contactLang = DECLARE_SINISTER_HOME_FINALIZE.contact;
    this.numberContactLang = DECLARE_SINISTER_HOME_FINALIZE.numberContact;
    this.btnHomeLang = DECLARE_SINISTER_HOME_FINALIZE.btnHome;
    this.gaAssistance = MFP_Danhos_y_Robos_Reportar_Asistencia_Reportada_37C(this.btnHomeLang);
  }

  ngOnInit(): void {
    if (this.declareSinisterHomeService.getFinalize()) {
      this.router.navigate(['/household/hurt-and-steal/reports']);

      return;
    }
    this.headerHelperService.set(HOUSE_DECLARE_SINISTER_FINALIZE);
    this.address = this.declareSinisterHomeService.getAddressReview();
    this.assistanceNumber = this.declareSinisterHomeService.getAssistanceNumber();
    this.declareSinisterHomeService.stepSeen();
  }

  goToHome(): void {
    this.router.navigate(['/household/hurt-and-steal/reports']);
  }
}
