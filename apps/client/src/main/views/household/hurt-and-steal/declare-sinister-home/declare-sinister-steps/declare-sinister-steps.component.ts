import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { DeclareSinisterHomeService, IStepsSinester } from '@mx/services/home/declare-sinister-home.service';
import { HOUSE_DECLARE_SINISTER_STEP } from '@mx/settings/constants/router-titles';
import { DECLARE_SINISTER_HOME } from '@mx/settings/lang/household.lang';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-declare-sinister-steps-component',
  templateUrl: './declare-sinister-steps.component.html'
})
export class DeclareSinisterStepsComponent implements OnInit, OnDestroy {
  steps: IStepsSinester;
  subSteps: Subscription;
  routerSub: Subscription;
  step1NumberLang: string;
  step1TextLang: string;
  step2NumberLang: string;
  step2TextLang: string;
  step3NumberLang: string;
  step3TextLang: string;
  index: number;
  constructor(
    private readonly declareSinisterHomeService: DeclareSinisterHomeService,
    private readonly router: Router,
    private readonly headerHelperService: HeaderHelperService
  ) {
    this.steps = { step1: false, step2: false, step3: false, step4: false };
    this.step1NumberLang = DECLARE_SINISTER_HOME.step1.number;
    this.step1TextLang = DECLARE_SINISTER_HOME.step1.text;
    this.step2NumberLang = DECLARE_SINISTER_HOME.step2.number;
    this.step2TextLang = DECLARE_SINISTER_HOME.step2.text;
    this.step3NumberLang = DECLARE_SINISTER_HOME.step3.number;
    this.step3TextLang = DECLARE_SINISTER_HOME.step3.text;
  }

  ngOnInit(): void {
    this.headerHelperService.set(HOUSE_DECLARE_SINISTER_STEP);
    this.subSteps = this.declareSinisterHomeService.subSteps.subscribe((steps: IStepsSinester) => {
      this.steps = { ...steps };
    });

    let url = this.router.url.split('/');
    this.index = parseInt(url[url.length - 1], 10);
    this.routerSub = this.router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        url = e.url.split('/');
        this.index = parseInt(url[url.length - 1], 10);
      }
    });
  }

  ngOnDestroy(): void {
    if (this.subSteps) {
      this.subSteps.unsubscribe();
    }
    if (this.routerSub) {
      this.routerSub.unsubscribe();
    }
  }
}
