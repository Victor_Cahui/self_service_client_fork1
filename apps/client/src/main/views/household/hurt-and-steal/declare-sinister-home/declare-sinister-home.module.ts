import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfLoaderModule } from '@mx/core/ui/lib/components/global/loader';
import { DeclareSinisterHomeComponent } from './declare-sinister-home.component';
import { DeclareSinisterHomeRoutingModule } from './declare-sinister-home.routing';

@NgModule({
  imports: [CommonModule, DeclareSinisterHomeRoutingModule, MfLoaderModule],
  declarations: [DeclareSinisterHomeComponent]
})
export class DeclareSinisterHomeModule {}
