import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { CarouselModule } from '@mx/core/ui/lib/components/carousel/carousel.module';
import { DeclareSinisterStepTwoComponent } from './declare-sinister-step-two.component';
import { DeclareSinisterStepTwoRoutingModule } from './declare-sinister-step-two.routing';

@NgModule({
  imports: [CommonModule, DeclareSinisterStepTwoRoutingModule, CarouselModule, ReactiveFormsModule, GoogleModule],
  declarations: [DeclareSinisterStepTwoComponent]
})
export class DeclareSinisterStepTwoModule {}
