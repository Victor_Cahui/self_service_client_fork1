import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { GAPushEvent } from '@mx/core/shared/common/services/google.analytics.utils';
import { GAService, GaUnsubscribeBase, IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { CarouselComponent, NumberItemsForScreen } from '@mx/core/ui/lib/components/carousel/carousel.component';
import { DeclareSinisterHomeService } from '@mx/services/home/declare-sinister-home.service';
import { MFP_Danhos_y_Robos_Reportar_Asistencia_Paso_2_35A } from '@mx/settings/constants/events.analytics';
import { LIST_TYPE_INCIDENTE } from '@mx/settings/constants/images-values';
import { DECLARE_SINISTER_HOME_STEP_2 } from '@mx/settings/lang/household.lang';
import {
  IDeclareSinister,
  IIncidentReason,
  IListIncidentTypesResponse
} from '@mx/statemanagement/models/home.interface';

@Component({
  selector: 'client-declare-sinister-step-two-component',
  templateUrl: './declare-sinister-step-two.component.html'
})
export class DeclareSinisterStepTwoComponent extends GaUnsubscribeBase implements OnInit {
  @ViewChild(CarouselComponent) carousel: CarouselComponent;
  listIncidentTypes: Array<IListIncidentTypesResponse>;
  listIncidentConst: any;
  listReason: Array<IIncidentReason>;
  sinister: IDeclareSinister;
  numberItemsCarousel: NumberItemsForScreen;

  idIncident: number;
  formCreate: FormGroup;
  buffIdReason: number;
  buffIdIncident: number;
  reason: AbstractControl;

  titleLang: string;
  titleReasonLang: string;
  btnReturnLang: string;
  btnSuccessLang: string;
  sinisterDescription: string;

  ga: Array<IGaPropertie>;
  constructor(
    private readonly fb: FormBuilder,
    private readonly declareSinisterHomeService: DeclareSinisterHomeService,
    private readonly router: Router,
    protected gaService: GAService
  ) {
    super(gaService);
    this.listIncidentTypes = [];
    this.listReason = [];
    this.titleLang = DECLARE_SINISTER_HOME_STEP_2.title;
    this.titleReasonLang = DECLARE_SINISTER_HOME_STEP_2.titleReason;
    this.btnReturnLang = DECLARE_SINISTER_HOME_STEP_2.form.btnReturn;
    this.btnSuccessLang = DECLARE_SINISTER_HOME_STEP_2.form.btnSuccess;
    this.listIncidentConst = LIST_TYPE_INCIDENTE;
    this.numberItemsCarousel = {
      XS: 2,
      SM: 4,
      MD: 4,
      LG: 4,
      XL: 4
    };
    this.ga = [MFP_Danhos_y_Robos_Reportar_Asistencia_Paso_2_35A()];
  }

  ngOnInit(): void {
    this.listIncidentTypes = this.declareSinisterHomeService.getListIncidentTypes();
    this.sinister = this.declareSinisterHomeService.getSinester();
    this.buffIdReason = this.sinister.idReason;
    this.buffIdIncident = this.sinister.idIncident;
    this.createForm(this.sinister);
    if (!!this.sinister.idIncident) {
      const sinister = this.listIncidentTypes.find(i => i.identificadorIncidente === this.sinister.idIncident);
      this.selectIncident(sinister, true);
    }
  }

  selectIncident(sinister: any = {}, init?: boolean): void {
    const id = sinister.identificadorIncidente;
    this.sinisterDescription = sinister.descripcionIncidente;
    if (this.idIncident !== id) {
      this.idIncident = id;
      this.listReason = this.declareSinisterHomeService.getListIncidentReason(id);
      if (this.idIncident === this.buffIdIncident) {
        this.reason.setValue(this.buffIdReason);
      } else {
        this.reason.reset();
      }
      if (init) {
        setTimeout(() => {
          this.carousel.goTo(this.declareSinisterHomeService.getIndexListIncident(id));
        }, 100);
      }
    }
  }

  onSubmit(): void {
    this.declareSinisterHomeService.successSteep2(this.idIncident, this.reason.value);
    GAPushEvent(this._getDataForGA(this.btnSuccessLang));
    this.router.navigate(['/household/hurt-and-steal/declare-sinister-home/step/3']);
  }

  createForm(sinister: IDeclareSinister): void {
    this.formCreate = this.fb.group({
      reason: [{ value: !!sinister.idReason ? sinister.idReason : '', disabled: false }, Validators.required]
    });
    this.reason = this.formCreate.get('reason');
  }

  goToStep1(): void {
    GAPushEvent(this._getDataForGA(this.btnReturnLang));
    this.router.navigate(['/household/hurt-and-steal/declare-sinister-home/step/1']);
  }

  prevCarousel(): void {
    this.carousel.prev();
  }

  nextCarousel(): void {
    this.carousel.next();
  }

  trackByFn(index, item): any {
    return index;
  }

  private _getDataForGA(lbl): any {
    return {
      ...MFP_Danhos_y_Robos_Reportar_Asistencia_Paso_2_35A(),
      label: MFP_Danhos_y_Robos_Reportar_Asistencia_Paso_2_35A().label.replace('{{label}}', lbl),
      incidenteTipo: this.sinisterDescription,
      motivoDaño: this._getReason().descripcionMotivo
    };
  }

  private _getReason(): any {
    return this.listReason.find(r => r.identificadorMotivo === this.reason.value);
  }
}
