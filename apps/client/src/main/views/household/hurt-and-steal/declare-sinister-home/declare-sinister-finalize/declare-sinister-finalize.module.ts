import { NgModule } from '@angular/core';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DeclareSinisterFinalizeComponent } from './declare-sinister-finalize.component';
import { DeclareSinisterFinalizeRoutingModule } from './declare-sinister-finalize.routing';

@NgModule({
  imports: [DeclareSinisterFinalizeRoutingModule, GoogleModule],
  exports: [],
  declarations: [DeclareSinisterFinalizeComponent],
  providers: []
})
export class DeclareSinisterFinalizeModule {}
