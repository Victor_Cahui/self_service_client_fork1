import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'household-insurance',
    loadChildren: './household-insurance/household-insurance.module#HouseholdInsuranceModule'
  },
  {
    path: 'hurt-and-steal',
    loadChildren: './hurt-and-steal/hurt-and-steal.module#HurtAndStealModule'
  },
  {
    path: 'quote',
    loadChildren: './household-quote/household-quote.module#HouseholdQuoteModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HouseholdRoutingModule {}
