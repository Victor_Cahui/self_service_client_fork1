import { Component, OnDestroy, OnInit } from '@angular/core';
import { GmFilterViewItem, GmFilterViewOfficeComponent } from '@mx/components';
import { GeolocationService } from '@mx/core/shared/helpers/util/geolocation.service';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { OfficeService } from '@mx/services/office.service';
import {
  Buscar_47B,
  Filtrar_47A,
  Filtrar_por_47D,
  Filtrar_por_47E,
  Ver_detalle_47C
} from '@mx/settings/constants/events.analytics';
import { APPLICATION_CODE, ConfigMapView } from '@mx/settings/constants/general-values';
import { MARKER_OFFICE } from '@mx/settings/constants/images-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { MapLang } from '@mx/settings/lang/map.lang';
import { IItemMarker } from '@mx/statemanagement/models/google-map.interface';
import { IOffice, IOfficeListRequest } from '@mx/statemanagement/models/office.interface';
import { Subscription } from 'rxjs';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'client-offices-component',
  templateUrl: './offices.component.html'
})
export class OfficesComponent implements OnInit, OnDestroy {
  title = MapLang.OFFICE.Title;
  defaultCoords = MapLang.OFFICE.defaultCoords;
  officeDetail: IItemMarker;
  configView = ConfigMapView;

  markers: Array<IItemMarker> = [];
  officeListSub: Subscription;

  gmFilterViewItem: GmFilterViewItem;

  showOfficeDetail: boolean;
  hasPermission: boolean;
  showLoading: boolean;

  ga: Array<IGaPropertie>;

  constructor(private readonly officeService: OfficeService, private readonly geolocationService: GeolocationService) {
    this.gmFilterViewItem = new GmFilterViewItem(GmFilterViewOfficeComponent);
    this.ga = [Filtrar_47A(), Buscar_47B(), Ver_detalle_47C(), Filtrar_por_47D(), Filtrar_por_47E()];
  }

  ngOnInit(): void {
    this.geolocationService.hasPermission().then(hasPermission => {
      this.hasPermission = hasPermission;
      if (!hasPermission) {
        this.getOfficeList();
      }
    });
    this.showLoading = true;
  }

  ngOnDestroy(): void {
    this.unSubscribeOfficeList();
  }

  userPosition(latitude: number, longitude: number): void {
    if (latitude && longitude) {
      this.hasPermission = true;
      this.unSubscribeOfficeList();
      this.getOfficeList(latitude, longitude);
    }
  }

  getOfficeList(latitude?: number, longitude?: number): void {
    const params: IOfficeListRequest = {
      codigoApp: APPLICATION_CODE
    };

    if (latitude && longitude) {
      params.latitud = latitude;
      params.longitud = longitude;
    }

    this.officeListSub = this.officeService.getOfficeList(params).subscribe(
      (offices: Array<IOffice>) => {
        // tslint:disable-next-line: no-parameter-reassignment
        offices = offices || [];
        setTimeout(() => (this.showLoading = false), 50);
        const markerList = offices.map(item => {
          const noAddress = GeneralLang.Texts.WithOutAddress;
          const distance = !isNullOrUndefined(item.distancia) ? ` - ${item.distancia} km` : '';
          const district = item.distritoDescripcion ? `, Distrito de ${item.distritoDescripcion}` : '';
          const province = item.provinciaDescripcion ? `, ${item.provinciaDescripcion}` : '';
          const department = item.departamentoDescripcion ? `, ${item.departamentoDescripcion}` : '';
          const address = `${item.direccion || noAddress}${district}${province}${department}${distance}`;

          const marker: IItemMarker = {
            id: item.oficinaId,
            latitude: item.latitud,
            longitude: item.longitud,
            pinUrl: MARKER_OFFICE,
            imageUrl: item.imagenPortraitUrl,
            title: item.nombre,
            address,
            district: item.distritoDescripcion,
            province: item.provinciaDescripcion,
            department: item.departamentoDescripcion,
            distance: !isNullOrUndefined(item.distancia) ? item.distancia : 0,
            phone: item.telefono,
            timetable: item.horarioAtencion,
            showFavoriteButton: false,
            canScheduleAppointment: item.puedeAgendarCita,
            payload: item
          };

          return marker;
        });
        setTimeout(() => {
          this.markers = markerList;
        }, 0);
      },
      () => {
        this.markers = [];
      },
      () => {
        this.officeListSub.unsubscribe();
      }
    );
  }

  viewDetail(marker: IItemMarker): void {
    this.showOfficeDetail = true;
    this.officeDetail = marker.payload;
  }

  exitOfficeDeta(): void {
    this.showOfficeDetail = false;
  }

  unSubscribeOfficeList(): void {
    if (this.officeListSub) {
      this.officeListSub.unsubscribe();
    }
  }
}
