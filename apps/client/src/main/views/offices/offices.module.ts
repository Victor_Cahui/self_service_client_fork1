import { NgModule } from '@angular/core';
import { GmFilterModule, GmFilterViewOfficeModule, GoogleMapFilterViewModule } from '@mx/components';
import { OfficeDetailModule } from '@mx/components/office-detail/office-detail.module';
import { OfficesComponent } from './offices.component';
import { OfficesRoutingModule } from './offices.routing';

@NgModule({
  imports: [
    OfficesRoutingModule,
    GoogleMapFilterViewModule,
    GmFilterModule,
    GmFilterViewOfficeModule,
    OfficeDetailModule
  ],
  exports: [],
  declarations: [OfficesComponent],
  providers: []
})
export class OfficesModule {}
