import { NgModule } from '@angular/core';

import { DeathRoutingModule } from './death-routing.module';

@NgModule({
  imports: [DeathRoutingModule]
})
export class DeathModule {}
