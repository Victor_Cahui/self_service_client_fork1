import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'death-insurance',
    loadChildren: './death-insurance/death-insurance.module#DeathInsuranceModule'
  },
  {
    path: 'quote',
    loadChildren: './death-quote/death-quote.module#DeathQuoteModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeathRoutingModule {}
