import { Component, OnInit, ViewChild } from '@angular/core';
import { ConfigurationBase } from '@mx/components/shared/configuration/configuration-base';
import { FormContactedByAgentComponent } from '@mx/components/shared/form-contacted-by-agent/form-contacted-by-agent.component';
import { ModalMoreFaqsComponent } from '@mx/components/shared/modals/modal-more-faqs/modal-more-faqs.component';
import { ScrollUtil } from '@mx/core/shared/helpers/util/scroll';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { POLICY_TYPES, STATIC_BANNERS } from '@mx/settings/constants/policy-values';
import { DEATH_HEADER } from '@mx/settings/constants/router-titles';
import { DEATH_INSURANCE_FAQS_FOR_LANDING } from '@mx/settings/faqs/death-insurance.faq';
import * as source from '@mx/settings/lang/death.lang';
import { IContactData } from '@mx/statemanagement/models/client.models';
import { IPathImageSize } from '@mx/statemanagement/models/general.models';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-death-quote-component',
  templateUrl: './death-quote.component.html'
})
export class DeathQuoteComponent extends ConfigurationBase implements OnInit {
  @ViewChild(ModalMoreFaqsComponent) modalFaqs: ModalMoreFaqsComponent;
  @ViewChild(FormContactedByAgentComponent) formContactedByAgentComponent: FormContactedByAgentComponent;

  staticBanners: string;
  insuranceTop = source.INSURANCE_TOP;
  howToWork = source.HOW_TO_WORK;
  paymentOptions = source.PAYMENTS_OPTIONS;
  subTitleFaq = DEATH_HEADER.title;
  faqs = DEATH_INSURANCE_FAQS_FOR_LANDING;
  masonry = true;
  subSendData: Subscription;
  scrollSub: Subscription;
  isSent: boolean;
  showModalFaq: boolean;
  pathImgBanner: IPathImageSize;
  contactData: IContactData;

  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService
  ) {
    super(clientService, policiesInfoService, configurationService, authService);
  }

  ngOnInit(): void {
    this.isSent = false;
    this.policyType = POLICY_TYPES.MD_DECESOS.code;
    this.contactData = { subject: POLICY_TYPES.MD_DECESOS.subject };
    this.staticBanners = POLICY_TYPES[this.policyType].staticBanners;
    this.pathImgBanner = {
      sm: this.staticBanners + STATIC_BANNERS.sm,
      lg: this.staticBanners + STATIC_BANNERS.lg,
      xl: this.staticBanners + STATIC_BANNERS.xl
    };
    this.loadConfiguration();
  }

  openModalMoreFaqs(): void {
    this.showModalFaq = true;
    setTimeout(() => {
      this.modalFaqs.open();
    });
  }

  // Scroll a form
  eventInfo(): void {
    if (this.isSent) {
      this.isSent = false;
    }
    this.scrollSub = ScrollUtil.goTo(0).subscribe(
      () => {
        if (!this.formContactedByAgentComponent.isSent) {
          this.formContactedByAgentComponent.setFocusPhoneInput();
        }
      },
      () => {},
      () => {
        this.scrollSub.unsubscribe();
      }
    );
  }
}
