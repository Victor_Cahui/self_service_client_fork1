import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DeathQuoteComponent } from './death-quote.component';

const routes: Routes = [
  {
    path: '',
    component: DeathQuoteComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeathQuoteRoutingModule {}
