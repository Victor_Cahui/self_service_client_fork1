import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DeathInsuranceListComponent } from './death-insurance-list.component';

const routes: Routes = [
  {
    path: '',
    component: DeathInsuranceListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeathInsuranceListRoutingModule {}
