import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: './death-insurance-list/death-insurance-list.module#DeathInsuranceListModule'
  },
  {
    path: 'faqs',
    loadChildren: './death-insurance-faq/death-insurance-faq.module#DeathInsuranceFAQModule'
  },
  {
    path: ':policyNumber',
    loadChildren: './death-insurance-by-id/death-insurance-by-id.module#DeathInsuranceByIdModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeathInsuranceRoutingModule {}
