import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DeathInsuranceDetailComponent } from './death-insurance-detail.component';

const routes: Routes = [
  {
    path: '',
    component: DeathInsuranceDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeathInsuranceDetailRoutingModule {}
