import { Component, HostBinding, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';

import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { COVERAGES_DEATH, ICON_COVERAGE_DEFAULT, QUERY_SOURCE } from '@mx/settings/constants/coverage-values';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { COVERAGE } from '@mx/settings/lang/death.lang';
import { ICoverageIcons, ICoverageItem } from '@mx/statemanagement/models/coverage.interface';
import {
  ICoveragesPolicy,
  ICoveragesPolicyRequest,
  IPoliciesByClientResponse
} from '@mx/statemanagement/models/policy.interface';

@Component({
  selector: 'client-death-coverage-component',
  templateUrl: './death-coverage.component.html'
})
export class DeathCoverageComponent implements OnInit {
  @HostBinding('attr.class') attr_class = 'w-10';

  coverageTitle: string;
  policyType: string;
  policyNumber: string;
  description: string;
  showLoading: boolean;
  policiesServiceSub: Subscription;
  coverageList: Array<ICoveragesPolicy>;
  itemList: Array<ICoverageItem>;
  itemListBase: Array<ICoverageIcons>;
  clientPolicy: IPoliciesByClientResponse;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    protected policiesService: PoliciesService,
    protected policiesInfoService: PoliciesInfoService
  ) {}

  ngOnInit(): void {
    this.coverageTitle = COVERAGE.title.toUpperCase();
    this.description = COVERAGE.content;
    this.policyType = POLICY_TYPES.MD_DECESOS.code;
    this.activatedRoute.paramMap.subscribe((res: any) => {
      this.policyNumber = res.params.policyNumber;
    });
    timer(0)
      .toPromise()
      .then(() => {
        this.clientPolicy = this.policiesInfoService.getClientPolicy(this.policyNumber);
        this.description = this.description.replace(
          '{{typeInsured}}',
          (this.clientPolicy.descripcionPoliza || '').toUpperCase()
        );
      });
    this.getCoverages();
  }

  getCoverages(): void {
    this.showLoading = true;
    this.policiesServiceSub = this.policiesService
      .getCoveragesByPolicy({
        codigoApp: APPLICATION_CODE,
        numeroPoliza: this.policyNumber,
        tipoRamoPoliza: this.policyType,
        origenConsulta: QUERY_SOURCE.POLICY_DETAIL
      } as ICoveragesPolicyRequest)
      .subscribe(
        (res: any) => {
          // tslint:disable-next-line: no-parameter-reassignment
          res = res || [];
          this.coverageList = res.coberturas || [];
          this.setDataItemCoverage(this.coverageList);
        },
        () => {
          this.showLoading = false;
        },
        () => {
          this.showLoading = false;
          this.policiesServiceSub.unsubscribe();
        }
      );
  }

  setDataItemCoverage(list: Array<ICoveragesPolicy>): void {
    this.itemListBase = COVERAGES_DEATH;
    this.itemList = list.map(item => {
      const iconCoverage = this.itemListBase.find(c => {
        return c.key === item.llave;
      });
      const itemCoverage: ICoverageItem = {
        title: item.tituloInfo,
        description: item.descripcionInfo,
        question: item.accionTexto,
        questionBool: item.accion,
        routeKey: item.accionLlave,
        iconKey: item.llave,
        icon: (iconCoverage && iconCoverage.icon) || ICON_COVERAGE_DEFAULT
      };

      return itemCoverage;
    });
  }
}
