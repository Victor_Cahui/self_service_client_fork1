import { Component, OnDestroy, OnInit } from '@angular/core';

import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { DEATH_INSURANCE_TAB } from '@mx/settings/constants/router-tabs';
import { ITabItem } from '@mx/statemanagement/models/router.interface';

@Component({
  selector: 'client-death-insurance-by-id',
  templateUrl: './death-insurance-by-id.component.html'
})
export class DeathInsuranceByIdComponent implements OnDestroy, OnInit {
  tabItemsList: Array<ITabItem>;
  constructor(private readonly headerHelperService: HeaderHelperService) {
    this.tabItemsList = DEATH_INSURANCE_TAB;
  }

  ngOnInit(): void {
    this.headerHelperService.setTab(true);
  }
  ngOnDestroy(): void {
    this.headerHelperService.setTab(false);
  }
}
