import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DeathExclusionsComponent } from './death-exclusions.component';

const routes: Routes = [
  {
    path: '',
    component: DeathExclusionsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeathExclusionsRoutingModule {}
