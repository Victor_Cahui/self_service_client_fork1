import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DeathInsuranceByIdComponent } from './death-insurance-by-id.component';

const routes: Routes = [
  {
    path: '',
    component: DeathInsuranceByIdComponent,
    children: [
      {
        path: '',
        redirectTo: 'detail',
        pathMatch: 'prefix'
      },
      {
        path: 'detail',
        loadChildren: './death-insurance-detail/death-insurance-detail.module#DeathInsuranceDetailModule'
      },
      {
        path: 'coverage',
        loadChildren: './death-insurance-coverage/death-insurance-coverage.module#DeathInsuranceCoverageModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeathInsuranceByIdRoutingModule {}
