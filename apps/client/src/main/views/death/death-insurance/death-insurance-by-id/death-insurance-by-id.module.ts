import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MfTabTopModule } from '@mx/core/ui/lib/components/tabs/tabs-top/tab-top.module';
import { DeathInsuranceByIdComponent } from './death-insurance-by-id.component';
import { DeathInsuranceByIdRoutingModule } from './death-insurance-by-id.routing';

@NgModule({
  imports: [CommonModule, DeathInsuranceByIdRoutingModule, MfTabTopModule],
  declarations: [DeathInsuranceByIdComponent]
})
export class DeathInsuranceByIdModule {}
