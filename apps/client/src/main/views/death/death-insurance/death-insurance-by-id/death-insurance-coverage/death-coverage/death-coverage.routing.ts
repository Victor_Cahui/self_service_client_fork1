import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DeathCoverageComponent } from './death-coverage.component';

const routes: Routes = [
  {
    path: '',
    component: DeathCoverageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeathCoverageRoutingModule {}
