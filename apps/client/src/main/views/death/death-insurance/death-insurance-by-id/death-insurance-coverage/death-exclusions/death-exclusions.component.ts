import { Component } from '@angular/core';

import { EXCLUSIONS } from '@mx/settings/lang/life.lang';

@Component({
  selector: 'client-death-exclusions-component',
  templateUrl: './death-exclusions.component.html'
})
export class DeathExclusionsComponent {
  content = EXCLUSIONS;
}
