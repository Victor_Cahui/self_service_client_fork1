import { Component, OnInit } from '@angular/core';

import { DEATH_INSURANCE_FAQS_INSIDE } from '@mx/settings/faqs/death-insurance.faq';

@Component({
  selector: 'client-death-insurance-faq-component',
  templateUrl: './death-insurance-faq.component.html'
})
export class DeathInsuranceFAQComponent implements OnInit {
  faqs: Array<{ title: string; content: string; collapse: boolean }>;

  constructor() {
    this.faqs = DEATH_INSURANCE_FAQS_INSIDE.map(faq => ({ title: faq.title, content: faq.content, collapse: true }));
  }

  ngOnInit(): void {}
}
