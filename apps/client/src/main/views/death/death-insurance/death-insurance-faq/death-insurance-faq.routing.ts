import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DeathInsuranceFAQComponent } from './death-insurance-faq.component';

const routes: Routes = [
  {
    path: '',
    component: DeathInsuranceFAQComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeathInsuranceFAQRoutingModule {}
