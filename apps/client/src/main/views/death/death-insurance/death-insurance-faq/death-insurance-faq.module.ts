import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CardMoreFaqModule } from '@mx/components/shared/card-more-faq/card-more-faq.module';
import { DeathInsuranceFAQComponent } from './death-insurance-faq.component';
import { DeathInsuranceFAQRoutingModule } from './death-insurance-faq.routing';

@NgModule({
  imports: [CommonModule, CardMoreFaqModule, DeathInsuranceFAQRoutingModule],
  declarations: [DeathInsuranceFAQComponent]
})
export class DeathInsuranceFAQModule {}
