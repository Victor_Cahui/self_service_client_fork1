import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'client-sinisters-in-progress',
  templateUrl: './sinisters-in-progress.component.html'
})
export class SinistersInProgressComponent implements OnInit {
  disableCollapse = true;
  disableIcon = true;
  isHistorical = true;

  ngOnInit(): void {}
}
