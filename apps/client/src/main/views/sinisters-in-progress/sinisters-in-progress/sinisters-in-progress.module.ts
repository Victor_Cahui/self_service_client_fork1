import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardSinistersInProgressModule } from '@mx/components';
import { SinistersInProgressComponent } from './sinisters-in-progress.component';
import { SinistersInProgressRoutingModule } from './sinisters-in-progress.routing';

@NgModule({
  imports: [CommonModule, SinistersInProgressRoutingModule, CardSinistersInProgressModule],
  declarations: [SinistersInProgressComponent]
})
export class SinistersInProgressModule {}
