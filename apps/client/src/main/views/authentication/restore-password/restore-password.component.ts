import { Component } from '@angular/core';

@Component({
  selector: 'client-restore-password',
  template: '<client-form-restore-password></client-form-restore-password>'
})
export class RestorePasswordComponent {}
