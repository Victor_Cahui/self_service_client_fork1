import { NgModule } from '@angular/core';
import { MfLoaderModule } from '@mx/core/ui/lib/components/global/loader';
import { ValidateTokenRoutingModule } from './validate-token.routing';

import { ValidateTokenComponent } from './validate-token.component';

@NgModule({
  imports: [ValidateTokenRoutingModule, MfLoaderModule],
  exports: [],
  declarations: [ValidateTokenComponent],
  providers: []
})
export class ValidateTokenModule {}
