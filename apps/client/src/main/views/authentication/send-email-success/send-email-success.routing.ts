import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SendEmailSuccessComponent } from './send-email-success.component';

const routes: Routes = [
  {
    path: '',
    component: SendEmailSuccessComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SendEmailSuccessRoutingModule {}
