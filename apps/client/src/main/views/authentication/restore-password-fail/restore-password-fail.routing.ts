import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RestorePasswordFailComponent } from './restore-password-fail.component';

const routes: Routes = [
  {
    path: '',
    component: RestorePasswordFailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RestorePasswordFailRoutingModule {}
