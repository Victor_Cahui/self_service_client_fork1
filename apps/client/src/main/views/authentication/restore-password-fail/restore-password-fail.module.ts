import { NgModule } from '@angular/core';
import { LoginModule as ComponentesLogin } from '@mx/components/login/login.module';
import { RestorePasswordFailComponent } from './restore-password-fail.component';
import { RestorePasswordFailRoutingModule } from './restore-password-fail.routing';

@NgModule({
  imports: [RestorePasswordFailRoutingModule, ComponentesLogin],
  exports: [],
  declarations: [RestorePasswordFailComponent],
  providers: []
})
export class RestorePasswordFailModule {}
