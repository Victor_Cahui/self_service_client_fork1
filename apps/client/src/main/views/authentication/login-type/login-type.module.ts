import { NgModule } from '@angular/core';
import { LoginModule as ComponentesLogin } from '@mx/components/login/login.module';
import { LoginTypeComponent } from './login-type.component';
import { LoginTypeRoutingModule } from './login-type.routing';

@NgModule({
  imports: [LoginTypeRoutingModule, ComponentesLogin],
  declarations: [LoginTypeComponent]
})
export class LoginTypeModule {}
