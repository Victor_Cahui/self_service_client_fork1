import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RestorePasswordSuccessComponent } from './restore-password-success.component';

const routes: Routes = [
  {
    path: '',
    component: RestorePasswordSuccessComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RestorePasswordSuccessRoutingModule {}
