import { NgModule } from '@angular/core';
import { LoginModule as ComponentesLogin } from '@mx/components/login/login.module';
import { RestorePasswordSuccessComponent } from './restore-password-success.component';
import { RestorePasswordSuccessRoutingModule } from './restore-password-success.routing';

@NgModule({
  imports: [RestorePasswordSuccessRoutingModule, ComponentesLogin],
  exports: [],
  declarations: [RestorePasswordSuccessComponent],
  providers: []
})
export class RestorePasswordSuccessModule {}
