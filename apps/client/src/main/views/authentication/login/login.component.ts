import { Component } from '@angular/core';

@Component({
  selector: 'client-app-login',
  template: '<client-form-login></client-form-login>'
})
export class LoginComponent {}
