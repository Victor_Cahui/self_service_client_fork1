import { NgModule } from '@angular/core';
import { LoginRoutingModule } from './login.routing';

import { LoginModule as ComponentesLogin } from '@mx/components/login/login.module';
import { LoginComponent } from './login.component';

@NgModule({
  imports: [LoginRoutingModule, ComponentesLogin],
  exports: [],
  declarations: [LoginComponent],
  providers: []
})
export class LoginModule {}
