import { NgModule } from '@angular/core';
import { LoginModule as ComponentesLogin } from '@mx/components/login/login.module';
import { RecoverUserComponent } from './recover-user.component';
import { RecoverUserRoutingModule } from './recover-user.routing';

@NgModule({
  imports: [RecoverUserRoutingModule, ComponentesLogin],
  exports: [],
  declarations: [RecoverUserComponent],
  providers: []
})
export class RecoverUserModule {}
