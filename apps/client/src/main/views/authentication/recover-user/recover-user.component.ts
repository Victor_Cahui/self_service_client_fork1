import { Component } from '@angular/core';

@Component({
  selector: 'client-app-recover-user',
  template: '<client-form-recover-user></client-form-recover-user>'
})
export class RecoverUserComponent {}
