import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RecoverUserComponent } from './recover-user.component';

const routes: Routes = [
  {
    path: '',
    component: RecoverUserComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecoverUserRoutingModule {}
