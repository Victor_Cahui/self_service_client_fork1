import { Component } from '@angular/core';

@Component({
  selector: 'client-app-login-recurrent',
  template: '<client-form-login-recurrent></client-form-login-recurrent>'
})
export class LoginRecurrentComponent {}
