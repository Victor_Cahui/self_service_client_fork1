import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EnableUserComponent } from './enable-user.component';

const routes: Routes = [
  {
    path: '',
    component: EnableUserComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EnableUserRoutingModule {}
