import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfTabTopModule } from '@mx/core/ui/lib/components/tabs/tabs-top/tab-top.module';
import { LifeInsuranceByIdComponent } from './life-insurance-by-id.component';
import { LifeInsuranceByIdRoutingModule } from './life-insurance-by-id.routing';

@NgModule({
  imports: [CommonModule, LifeInsuranceByIdRoutingModule, MfTabTopModule],
  declarations: [LifeInsuranceByIdComponent]
})
export class LifeInsuranceByIdModule {}
