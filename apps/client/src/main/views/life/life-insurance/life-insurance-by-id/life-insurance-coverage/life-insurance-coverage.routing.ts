import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LifeInsuranceCoverageComponent } from './life-insurance-coverage.component';

const routes: Routes = [
  {
    path: '',
    component: LifeInsuranceCoverageComponent,
    children: [
      {
        path: '',
        loadChildren: './life-coverage/life-coverage.module#LifeCoverageModule'
      },
      {
        path: 'exclusions',
        loadChildren: './life-exclusions/life-exclusions.module#LifeExclusionsModule'
      },
      {
        path: 'foil-attached',
        loadChildren: './life-foil-attached/life-foil-attached.module#LifeFoilAttachedModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LifeInsuranceCoverageRoutingModule {}
