import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LifeCoverageComponent } from './life-coverage.component';

const routes: Routes = [
  {
    path: '',
    component: LifeCoverageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LifeCoverageRoutingModule {}
