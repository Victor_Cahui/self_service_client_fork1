import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LifeInsuranceDetailComponent } from './life-insurance-detail.component';

const routes: Routes = [
  {
    path: '',
    component: LifeInsuranceDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LifeInsuranceDetailRoutingModule {}
