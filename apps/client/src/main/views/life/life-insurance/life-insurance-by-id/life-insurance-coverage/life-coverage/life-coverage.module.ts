import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardIconsCoverageModule } from '@mx/components/shared/card-icons-coverage/card-icons-coverage.module';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { LifeCoverageComponent } from './life-coverage.component';
import { LifeCoverageRoutingModule } from './life-coverage.routing';

@NgModule({
  imports: [CommonModule, LifeCoverageRoutingModule, CardIconsCoverageModule, MfLoaderModule],
  declarations: [LifeCoverageComponent]
})
export class LifeCoverageModule {}
