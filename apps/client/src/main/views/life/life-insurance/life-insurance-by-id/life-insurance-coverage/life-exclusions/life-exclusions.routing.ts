import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LifeExclusionsComponent } from './life-exclusions.component';

const routes: Routes = [
  {
    path: '',
    component: LifeExclusionsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LifeExclusionsRoutingModule {}
