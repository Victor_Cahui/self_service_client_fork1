import { Component } from '@angular/core';
import { EXCLUSIONS } from '@mx/settings/lang/life.lang';

@Component({
  selector: 'client-life-exclusions-component',
  templateUrl: './life-exclusions.component.html'
})
export class LifeExclusionsComponent {
  content = EXCLUSIONS;
}
