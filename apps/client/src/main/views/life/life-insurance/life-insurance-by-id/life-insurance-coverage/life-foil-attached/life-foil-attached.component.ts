import { Component, HostBinding, OnInit } from '@angular/core';
import { FOIL_ATTACHED } from '@mx/settings/lang/life.lang';

@Component({
  selector: 'client-life-foil-attached-component',
  templateUrl: './life-foil-attached.component.html'
})
export class LifeFoilAttachedComponent implements OnInit {
  @HostBinding('class') class = 'w-100';

  content = { title: FOIL_ATTACHED.title, content: '' };

  ngOnInit(): void {}
}
