import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LifeInsuranceByIdComponent } from './life-insurance-by-id.component';

const routes: Routes = [
  {
    path: '',
    component: LifeInsuranceByIdComponent,
    children: [
      {
        path: '',
        redirectTo: 'detail',
        pathMatch: 'prefix'
      },
      {
        path: 'detail',
        loadChildren: './life-insurance-detail/life-insurance-detail.module#LifeInsuranceDetailModule'
      },
      {
        path: 'coverage',
        loadChildren: './life-insurance-coverage/life-insurance-coverage.module#LifeInsuranceCoverageModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LifeInsuranceByIdRoutingModule {}
