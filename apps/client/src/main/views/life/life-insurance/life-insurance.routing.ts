import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: './life-insurance-list/life-insurance-list.module#LifeInsuranceListModule'
  },
  {
    path: 'faqs',
    loadChildren: './life-insurance-faq/life-insurance-faq.module#LifeInsuranceFAQModule'
  },
  {
    path: ':policyNumber',
    loadChildren: './life-insurance-by-id/life-insurance-by-id.module#LifeInsuranceByIdModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LifeInsuranceRoutingModule {}
