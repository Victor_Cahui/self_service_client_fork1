import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardWhatYouWantToDoModule } from '@mx/components';
import { CardMyPoliciesByTypeModule } from '@mx/components/card-my-policies/card-my-policies-by-type/card-my-policies-by-type.module';
import { BannerCarouselModule } from '@mx/components/shared/banner-carousel/banner-carousel.module';
import { LifeInsuranceListComponent } from './life-insurance-list.component';
import { LifeInsuranceListRoutingModule } from './life-insurance-list.routing';

@NgModule({
  imports: [
    BannerCarouselModule,
    CommonModule,
    LifeInsuranceListRoutingModule,
    CardMyPoliciesByTypeModule,
    CardWhatYouWantToDoModule
  ],
  declarations: [LifeInsuranceListComponent]
})
export class LifeInsuranceListModule {}
