import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LifeInsuranceListComponent } from './life-insurance-list.component';

const routes: Routes = [
  {
    path: '',
    component: LifeInsuranceListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LifeInsuranceListRoutingModule {}
