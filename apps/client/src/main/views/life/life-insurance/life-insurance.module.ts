import { NgModule } from '@angular/core';
import { LifeInsuranceRoutingModule } from './life-insurance.routing';

@NgModule({
  imports: [LifeInsuranceRoutingModule],
  declarations: []
})
export class LifeInsuranceModule {}
