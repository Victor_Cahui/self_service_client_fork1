import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LifeInsuranceFAQComponent } from './life-insurance-faq.component';

const routes: Routes = [
  {
    path: '',
    component: LifeInsuranceFAQComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LifeInsuranceFAQRoutingModule {}
