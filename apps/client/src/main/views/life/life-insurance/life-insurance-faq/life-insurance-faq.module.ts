import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardMoreFaqModule } from '@mx/components/shared/card-more-faq/card-more-faq.module';
import { LifeInsuranceFAQComponent } from './life-insurance-faq.component';
import { LifeInsuranceFAQRoutingModule } from './life-insurance-faq.routing';

@NgModule({
  imports: [CommonModule, CardMoreFaqModule, LifeInsuranceFAQRoutingModule],
  declarations: [LifeInsuranceFAQComponent]
})
export class LifeInsuranceFAQModule {}
