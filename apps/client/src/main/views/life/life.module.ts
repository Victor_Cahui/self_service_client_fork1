import { NgModule } from '@angular/core';

import { LifeRoutingModule } from './life-routing.module';

@NgModule({
  imports: [LifeRoutingModule]
})
export class LifeModule {}
