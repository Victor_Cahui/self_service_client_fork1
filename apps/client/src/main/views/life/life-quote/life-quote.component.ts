import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConfigurationBase } from '@mx/components/shared/configuration/configuration-base';
import { ModalMoreFaqsComponent } from '@mx/components/shared/modals/modal-more-faqs/modal-more-faqs.component';
import { GAService, IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { ScrollUtil } from '@mx/core/shared/helpers/util/scroll';
import { MfSelectComponent } from '@mx/core/ui/lib/components/forms/select/select.component';
import { MfModalErrorComponent } from '@mx/core/ui/lib/components/modals/modal-error/modal-error.component';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { MFP_Cotizacion_Vida_39A } from '@mx/settings/constants/events.analytics';
import { LIFE_INSURANCE_QUOTE_DIR } from '@mx/settings/constants/general-values';
import { POLICY_TYPES, STATIC_BANNERS } from '@mx/settings/constants/policy-values';
import { LIFE_INSURANCE_HEADER } from '@mx/settings/constants/router-titles';
import { LIFE_INSURANCE_FAQS } from '@mx/settings/faqs/life-insurance.faq';
import * as source from '@mx/settings/lang/life.lang';
import { IPathImageSize } from '@mx/statemanagement/models/general.models';
import { isEmpty } from 'lodash-es';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-quote-component',
  templateUrl: './life-quote.component.html'
})
export class LifeQuoteComponent extends ConfigurationBase implements OnInit {
  @ViewChild(MfModalErrorComponent) modalError: MfModalErrorComponent;
  @ViewChild(ModalMoreFaqsComponent) modalFaqs: ModalMoreFaqsComponent;
  @ViewChild('selectTypeInsurance') selectTypeInsurance: MfSelectComponent;

  staticBanners: string;
  titleForm = source.LifeLang.Titles.Form;
  subtitleForm = source.LifeLang.Titles.Subtitle;
  labelTypeInsurance = source.LifeLang.Labels.TypeInsurance;
  btnActions = source.LifeLang.Links.Send;
  typeInsuranceList = source.TYPE_INSURANCE_LIST;

  faqs = LIFE_INSURANCE_FAQS;
  insuranceTop = source.INSURANCE_TOP;
  howToWork = source.HOW_TO_WORK;
  paymentOptions = source.PAYMENTS_OPTIONS;
  subTitleFaq = LIFE_INSURANCE_HEADER.title;
  masonry = true;

  formContact: FormGroup;
  typeInsurance: AbstractControl;

  configurationSub: Subscription;
  sendDataSub: Subscription;
  scrollSub: Subscription;

  pathImgBanner: IPathImageSize;
  ga: Array<IGaPropertie>;

  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService,
    private readonly formBuilder: FormBuilder,
    protected gaService: GAService
  ) {
    super(clientService, policiesInfoService, configurationService, authService, gaService);
    this.ga = [MFP_Cotizacion_Vida_39A()];
  }

  ngOnInit(): void {
    this.policyType = POLICY_TYPES.MD_VIDA.code;
    this.staticBanners = POLICY_TYPES[this.policyType].staticBanners;
    this.pathImgBanner = {
      sm: this.staticBanners + STATIC_BANNERS.sm,
      lg: this.staticBanners + STATIC_BANNERS.lg,
      xl: this.staticBanners + STATIC_BANNERS.xl
    };
    this.loadConfiguration();
    // Form
    this.formContact = this.formBuilder.group({
      typeInsurance: ['', Validators.required]
    });
    this.typeInsurance = this.formContact.get('typeInsurance');
  }

  quote(): void {
    if (this.formContact.valid) {
      window.open(LIFE_INSURANCE_QUOTE_DIR, '_blank');
    }
  }

  showErrors(control: AbstractControl): boolean {
    return !!control && !isEmpty(control.errors) && (control.touched || control.dirty);
  }

  openModalMoreFaqs(): void {
    this.modalFaqs.open();
  }

  // Scroll a form
  eventQuote(): void {
    this.scrollSub = ScrollUtil.goTo(0).subscribe(
      () => {},
      () => {},
      () => {
        this.scrollSub.unsubscribe();
      }
    );
  }
}
