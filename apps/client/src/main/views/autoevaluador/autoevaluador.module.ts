import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormBaseModule } from '@mx/components/shared/form-base/form-base.module';
import { MfCardModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { AutoevaluadorComponent } from './autoevaluador.component';
import { AutoevaluadorRoutingModule } from './autoevaluador.routing';

@NgModule({
  imports: [AutoevaluadorRoutingModule, MfCardModule, CommonModule, MfLoaderModule, FormBaseModule],
  exports: [],
  declarations: [AutoevaluadorComponent],
  providers: []
})
export class AutoevaluadorModule {}
