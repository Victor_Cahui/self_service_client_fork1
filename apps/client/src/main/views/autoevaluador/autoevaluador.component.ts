import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google/google.gabase';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { GeneralService } from '@mx/services/general/general.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { MEDIKTOR_URLS_REDIRECT } from '@mx/settings/constants/mediktor-values';
import { MEDIKTOR_HEADER } from '@mx/settings/constants/router-titles';
import { takeUntil } from 'rxjs/operators';
@Component({
  selector: 'client-mediktor-component',
  templateUrl: './autoevaluador.component.html'
})
export class AutoevaluadorComponent extends GaUnsubscribeBase implements OnInit {
  urlMediktor: SafeResourceUrl;
  urlHostMediktor = MEDIKTOR_URLS_REDIRECT.MediktorDiagnostic;
  showLoading = true;
  module = 'CDM';
  constructor(
    private readonly headerHelperService: HeaderHelperService,
    private readonly sanitizer: DomSanitizer,
    private readonly _Autoservicios: Autoservicios,
    private readonly _AuthService: AuthService,
    private readonly _GeneralService: GeneralService,
    protected gaService: GAService,
    @Inject(DOCUMENT) protected document: Document
  ) {
    super(gaService);
    this.urlMediktor = this.sanitizer.bypassSecurityTrustResourceUrl(this.urlHostMediktor);
  }

  ngOnInit(): void {
    this.headerHelperService.set(MEDIKTOR_HEADER);
    setTimeout(() => {
      this.showLoading = false;
      this.document.getElementById('divMediktor').style.top = '0px';
      this.document.getElementById('divMediktor').style.height = '800px';
      this.document.getElementById('iframeMediktor').style.display = 'block';
    }, 10000);
    this.loadToken();
  }

  loadToken(): void {
    const auth = this._AuthService.retrieveEntity();
    this._Autoservicios
      .ObtenerToken({
        codigoApp: APPLICATION_CODE,
        documento: auth.number,
        tipoDocumento: auth.type
      })
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(res => {
        try {
        } catch (error) {}
      });
  }
}
