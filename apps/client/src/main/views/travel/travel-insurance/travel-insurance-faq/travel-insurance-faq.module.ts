import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardMoreFaqModule } from '@mx/components/shared/card-more-faq/card-more-faq.module';
import { TravelInsuranceFAQComponent } from './travel-insurance-faq.component';
import { TravelInsuranceFAQRoutingModule } from './travel-insurance-faq.routing';

@NgModule({
  imports: [CommonModule, CardMoreFaqModule, TravelInsuranceFAQRoutingModule],
  declarations: [TravelInsuranceFAQComponent]
})
export class TravelInsuranceFAQModule {}
