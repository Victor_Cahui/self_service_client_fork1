import { Component, OnInit } from '@angular/core';
import { TRAVEL_INSURANCE_FAQS } from '@mx/settings/faqs/travel-insurance.faq';

@Component({
  selector: 'client-travel-insurance-faq-component',
  templateUrl: './travel-insurance-faq.component.html'
})
export class TravelInsuranceFAQComponent implements OnInit {
  faqs: Array<{ title: string; content: string; collapse: boolean }>;

  constructor() {
    this.faqs = TRAVEL_INSURANCE_FAQS.map(faq => ({ title: faq.title, content: faq.content, collapse: true }));
  }

  ngOnInit(): void {}
}
