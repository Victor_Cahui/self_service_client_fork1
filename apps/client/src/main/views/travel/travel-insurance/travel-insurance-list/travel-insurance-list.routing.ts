import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TravelInsuranceListComponent } from './travel-insurance-list.component';

const routes: Routes = [
  {
    path: '',
    component: TravelInsuranceListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TravelInsuranceListRoutingModule {}
