import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TravelInsuranceDetailComponent } from './travel-insurance-detail.component';

const routes: Routes = [
  {
    path: '',
    component: TravelInsuranceDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TravelInsuranceDetailRoutingModule {}
