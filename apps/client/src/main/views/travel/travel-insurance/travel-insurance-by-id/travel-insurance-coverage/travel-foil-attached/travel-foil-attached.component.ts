import { Component, HostBinding, OnInit } from '@angular/core';
import { FOIL_ATTACHED } from '@mx/settings/lang/life.lang';

@Component({
  selector: 'client-travel-foil-attached-component',
  templateUrl: './travel-foil-attached.component.html'
})
export class TravelFoilAttachedComponent implements OnInit {
  @HostBinding('class') class = 'w-100';

  content = { title: FOIL_ATTACHED.title, content: '' };

  ngOnInit(): void {}
}
