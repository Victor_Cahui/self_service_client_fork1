import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfTabTopModule } from '@mx/core/ui/lib/components/tabs/tabs-top/tab-top.module';
import { TravelInsuranceByIdComponent } from './travel-insurance-by-id.component';
import { TravelInsuranceByIdRoutingModule } from './travel-insurance-by-id.routing';

@NgModule({
  imports: [CommonModule, TravelInsuranceByIdRoutingModule, MfTabTopModule],
  declarations: [TravelInsuranceByIdComponent]
})
export class TravelInsuranceByIdModule {}
