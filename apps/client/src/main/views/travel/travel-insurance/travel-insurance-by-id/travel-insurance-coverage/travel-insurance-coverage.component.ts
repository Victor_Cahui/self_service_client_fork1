import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { LIFE_COVERAGE_TABS } from '@mx/settings/constants/router-tabs';
import { ITabItem } from '@mx/statemanagement/models/router.interface';
import { TravelInsuranceCoverageBase } from './travel-insurance-coverage-base';

@Component({
  selector: 'client-travel-insurance-coverage',
  templateUrl: './travel-insurance-coverage.component.html'
})
export class TravelInsuranceCoverageComponent extends TravelInsuranceCoverageBase implements OnInit {
  faqPath: string;
  options: Array<ITabItem>;

  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService,
    protected policiesService: PoliciesService,
    protected headerHelperService: HeaderHelperService,
    protected activatedRoute: ActivatedRoute,
    protected localStorageService: LocalStorageService,
    protected router: Router,
    protected _Autoservicios: Autoservicios
  ) {
    super(
      clientService,
      policiesInfoService,
      configurationService,
      authService,
      policiesService,
      headerHelperService,
      activatedRoute,
      localStorageService,
      router,
      _Autoservicios
    );
    this.options = LIFE_COVERAGE_TABS;
  }

  ngOnInit(): void {
    this.faqPath = '/travel/travel-insurance/faqs';
  }
}
