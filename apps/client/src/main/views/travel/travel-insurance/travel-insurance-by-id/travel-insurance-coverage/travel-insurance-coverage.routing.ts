import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TravelInsuranceCoverageComponent } from './travel-insurance-coverage.component';

const routes: Routes = [
  {
    path: '',
    component: TravelInsuranceCoverageComponent,
    children: [
      {
        path: '',
        loadChildren: './travel-coverage/travel-coverage.module#TravelCoverageModule'
      },
      {
        path: 'exclusions',
        loadChildren: './travel-exclusions/travel-exclusions.module#TravelExclusionsModule'
      },
      {
        path: 'foil-attached',
        loadChildren: './travel-foil-attached/travel-foil-attached.module#TravelFoilAttachedModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TravelInsuranceCoverageRoutingModule {}
