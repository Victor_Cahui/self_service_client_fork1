import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TravelFoilAttachedComponent } from './travel-foil-attached.component';

const routes: Routes = [
  {
    path: '',
    component: TravelFoilAttachedComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TravelFoilAttachedRoutingModule {}
