import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TravelInsuranceByIdComponent } from './travel-insurance-by-id.component';

const routes: Routes = [
  {
    path: '',
    component: TravelInsuranceByIdComponent,
    children: [
      {
        path: '',
        redirectTo: 'detail',
        pathMatch: 'prefix'
      },
      {
        path: 'detail',
        loadChildren: './travel-insurance-detail/travel-insurance-detail.module#TravelInsuranceDetailModule'
      },
      {
        path: 'coverage',
        loadChildren: './travel-insurance-coverage/travel-insurance-coverage.module#TravelInsuranceCoverageModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TravelInsuranceByIdRoutingModule {}
