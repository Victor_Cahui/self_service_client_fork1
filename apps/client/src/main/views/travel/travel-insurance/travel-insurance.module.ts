import { NgModule } from '@angular/core';
import { TravelInsuranceRoutingModule } from './travel-insurance.routing';

@NgModule({
  imports: [TravelInsuranceRoutingModule],
  declarations: []
})
export class TravelInsuranceModule {}
