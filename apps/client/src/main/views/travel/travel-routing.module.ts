import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'travel-insurance',
    loadChildren: './travel-insurance/travel-insurance.module#TravelInsuranceModule'
  },
  {
    path: 'quote',
    loadChildren: './travel-quote/travel-quote.module#TravelQuoteModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TravelRoutingModule {}
