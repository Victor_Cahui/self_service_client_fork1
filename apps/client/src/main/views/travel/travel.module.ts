import { NgModule } from '@angular/core';

import { TravelRoutingModule } from './travel-routing.module';

@NgModule({
  imports: [TravelRoutingModule]
})
export class TravelModule {}
