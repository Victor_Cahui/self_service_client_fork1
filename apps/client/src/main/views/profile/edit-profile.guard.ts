import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { EditProfileService } from './edit.service';

@Injectable({
  providedIn: 'root'
})
export class EditProfileGuard implements CanActivate {
  constructor(private readonly router: Router, private readonly editProfileService: EditProfileService) {
    this.router = router;
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.isPageRefresh()) {
      if (state.url.includes('/profile/edit/step/3')) {
        this.router.navigate(['/profile']);
      } else {
        this.router.navigate(['/profile/edit/step/1']);
      }

      return false;
    }
    let newProfileSensibleData;
    [, newProfileSensibleData] = this.editProfileService.retrieveSensibleData();
    if (state.url.includes('/profile/edit/step/2')) {
      if (!newProfileSensibleData) {
        this.router.navigate(['/profile']);
      }

      return !!newProfileSensibleData;
    } else if (state.url.includes('/profile/edit/step/3')) {
      if (!newProfileSensibleData) {
        this.router.navigate(['/profile']);
      }

      return !!newProfileSensibleData;
    }
  }

  private isPageRefresh(): boolean {
    return !this.router.navigated;
  }
}
