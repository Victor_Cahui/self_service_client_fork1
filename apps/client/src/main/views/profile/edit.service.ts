import { Injectable } from '@angular/core';
import { IParameterApp } from '@mx/statemanagement/models/general.models';
import { IProfileNonSensibleDataRequest, IProfileSensibleDataRequest } from '@mx/statemanagement/models/profile.model';
import { isEqual } from 'lodash-es';

@Injectable({
  providedIn: 'root'
})
export class EditProfileService {
  oldProfileSensibleData: IProfileSensibleDataRequest;
  newProfileSensibleData: IProfileSensibleDataRequest;

  oldNonSensibleData: IProfileNonSensibleDataRequest;
  newNonSensibleData: IProfileNonSensibleDataRequest;

  hasOpenSuccessModalForNSD = false;

  parameterData: IParameterApp;

  resetData(): void {
    this.oldNonSensibleData = void 0;
    this.oldProfileSensibleData = void 0;
    this.newNonSensibleData = void 0;
    this.newProfileSensibleData = void 0;
    this.hasOpenSuccessModalForNSD = false;
  }

  saveSensibleData(oldData: IProfileSensibleDataRequest, newData: IProfileSensibleDataRequest): void {
    this.oldProfileSensibleData = oldData;
    this.newProfileSensibleData = newData;
  }

  retrieveSensibleData(): Array<IProfileSensibleDataRequest> {
    return [this.oldProfileSensibleData, this.newProfileSensibleData];
  }

  saveNonSensibleData(oldData: IProfileNonSensibleDataRequest, newData: IProfileNonSensibleDataRequest): void {
    this.oldNonSensibleData = oldData;
    this.newNonSensibleData = newData;
    if (!isEqual(this.oldNonSensibleData, this.newNonSensibleData)) {
      this.hasOpenSuccessModalForNSD = true;
    }
  }

  retrieveNonSensibleData(): Array<IProfileNonSensibleDataRequest> {
    return [this.oldNonSensibleData, this.newNonSensibleData];
  }

  doOpenSuccessModalForNonSensibleData(): boolean {
    return this.hasOpenSuccessModalForNSD;
  }

  saveParameterData(parameterData: IParameterApp): void {
    this.parameterData = parameterData;
  }

  retrieveParemeterData(): IParameterApp {
    return this.parameterData;
  }
}
