import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { EditDataWorkComponent } from '@mx/components/profile/edit-data-work/edit-data-work.component';
import { getDDMMYYYY } from '@mx/core/shared/helpers/util/date';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { Autoservicios, FrmProvider } from '@mx/core/shared/providers/services';
import { MfModalAlertComponent } from '@mx/core/ui/lib/components/modals/modal-alert/modal-alert.component';
import { MfModalMessageComponent } from '@mx/core/ui/lib/components/modals/modal-message/modal-message.component';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { GeneralService } from '@mx/services/general/general.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { GROUP_TYPE_ID_COMPANY } from '@mx/settings/auth/auth-values';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { LOCAL_STORAGE_KEYS } from '@mx/settings/constants/key-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { ProfileLang } from '@mx/settings/lang/profile.lang';
import { ILogguedUser } from '@mx/statemanagement/models/auth.interface';
import { IClientInformationResponse, IClientJobInformationResponse } from '@mx/statemanagement/models/client.models';
import { IJobBasicInfo, IProfileBasicInfo } from '@mx/statemanagement/models/profile.interface';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { isEqual } from 'lodash-es';
import { Subscription } from 'rxjs';
import { EditProfileService } from '../edit.service';

@Component({
  selector: 'client-home-profile-component',
  templateUrl: './home-profile.component.html'
})
export class HomeProfileComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild(MfModalMessageComponent) modalMessage: MfModalMessageComponent;
  @ViewChild(EditDataWorkComponent) editWork: EditDataWorkComponent;
  @ViewChild(MfModalAlertComponent) mfModalConfirm: MfModalAlertComponent;

  salaryRange: Array<any>;
  subClient: Subscription;
  entity: IdDocument;
  dataClientJob: IClientJobInformationResponse;
  data: IClientInformationResponse;
  titleAlert: string;
  messageAlert: string;
  modalTrabajador: boolean;
  isEnterprise: boolean;
  isEnterpriseWithMPD: boolean;
  subRange: Subscription;
  profileBasicInfo: IProfileBasicInfo;
  jobBasicInfo: IJobBasicInfo;
  btnConfirm = GeneralLang.Buttons.Confirm;
  lang = GeneralLang;
  witoutAddress = ProfileLang.Texts.WithoutCompanyAddress;
  frm: FormGroup;
  showLoading: boolean;
  isPerson: boolean;
  updatedFrm: boolean;

  private _initFrm: any;
  private _frmRx: Subscription;

  openSuccessModalFromEditProfile = false;

  constructor(
    private readonly _ClientService: ClientService,
    private readonly _AuthService: AuthService,
    private readonly _LocalStorageService: LocalStorageService,
    private readonly _Autoservicios: Autoservicios,
    private readonly generalService: GeneralService,
    private readonly router: Router,
    private readonly policiesInfoService: PoliciesInfoService,
    private readonly editProfileService: EditProfileService,
    private readonly _FrmProvider: FrmProvider
  ) {
    this.modalTrabajador = false;
    this.titleAlert = GeneralLang.Alert.updateWork.success.title;
    this.messageAlert = GeneralLang.Alert.updateWork.success.message;
  }

  ngOnInit(): void {
    this.frm = this._FrmProvider.HomeProfileComponent();
    this.entity = this._AuthService.retrieveEntity();
    this.getClientInformation();
    this._getTerms();
    this.subRange = this.generalService.getSalaryRange().subscribe((res: any) => {
      this.salaryRange = res;
    });
    this._frmRx = this.frm.valueChanges.subscribe(f => {
      const frmBoolean = this._getObjWithBoolean(f);
      this.updatedFrm = this._isDifferentObj(this._initFrm, frmBoolean);
    });
    this._initValues();
  }

  ngOnDestroy(): void {
    if (!!this.subClient) {
      this.subClient.unsubscribe();
    }

    if (!!this._frmRx) {
      this._frmRx.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    if (this.editProfileService.doOpenSuccessModalForNonSensibleData()) {
      this.editProfileService.resetData();
      this.modalMessage.open();
    }
  }

  save(): void {
    this._initFrm = this._getObjWithBoolean(this.frm.value);
    this.updatedFrm = false;
    this._saveTerms(this.frm.value);
  }

  openModalTerms(): void {
    this.mfModalConfirm.open();
  }

  getClientInformation(): void {
    if (this.subClient) {
      this.subClient.unsubscribe();
    }
    this.subClient = this._ClientService.getClientInformation({}).subscribe(
      (res: IClientInformationResponse) => {
        if (res) {
          this.data = res;
          this.setProfileData(res, this.entity);
          this.setProfileWork(res.trabajo);
        }
      },
      () => {},
      () => {
        if (this.data) {
          this._ClientService.setUserData(this.data);
        }
      }
    );
  }

  setProfileData(data: IClientInformationResponse, entity: IdDocument): void {
    // verifica si es cliente empresa
    const user: ILogguedUser = this._LocalStorageService.getData(LOCAL_STORAGE_KEYS.USER);
    if (user && user.groupTypeId) {
      this.isEnterprise = +user.groupTypeId === GROUP_TYPE_ID_COMPANY;
    }

    const profileBasicInfo = {
      fullName: data.nombreCompletoCliente,
      typeDocument: entity.type,
      document: entity.number,
      phone: data.telefonoMovil,
      email: data.emailCliente,
      birthDay: getDDMMYYYY(data.fechaNacimiento),
      registrationDate: data.fechaAsegurado && getDDMMYYYY(data.fechaAsegurado),
      isEnterprise: this.isEnterprise,
      payload: data
    } as IProfileBasicInfo;
    // Estructura para detalle de polizas
    this.policiesInfoService.setProfileBasicInfo(profileBasicInfo);
  }

  setProfileWork(jobInformation: IClientJobInformationResponse): void {
    this.dataClientJob = jobInformation;
    this.data.trabajo = jobInformation;
    this._ClientService.setUserData(this.data);
    this.jobBasicInfo = {
      companyName: jobInformation.nombreEmpresa,
      workstation: jobInformation.profesionDescripcion,
      oldWorking: jobInformation.antiguedadTrabajo,
      phone: jobInformation.telefonoTrabajo,
      companyAddress: this.parseDataCompanyAddress(
        jobInformation.direccionTrabajo,
        jobInformation.distritoDescripcion,
        jobInformation.provinciaDescripcion,
        jobInformation.departamentoDescripcion
      ),
      salaryRangeDescription: jobInformation.ingresosClienteDescripcion
    } as IJobBasicInfo;
  }

  /** @ToDo Mejorar esta funcionalidad */
  openModalTrabajador($event: boolean): void {
    if (!!this.salaryRange && !!this.dataClientJob) {
      this.modalTrabajador = $event;
      this.editWork.initialiceForm();
    }
  }

  // tslint:disable-next-line:cyclomatic-complexity
  parseDataCompanyAddress(address: string, district: string, province: string, department: string): string {
    let result = '';
    if (!!address && !!district && !!province && !!department) {
      result = `${ProfileLang.Labels.Address}: ${address}, ${district}, ${province}, ${department}`;
    } else {
      if (!address && !district && !province && !department) {
        result = this.witoutAddress;
      } else {
        result = `${ProfileLang.Labels.Address}:`;
        result += address ? ` ${address}` : '';
        result += district ? `${address ? ', ' : ''} ${district}` : '';
        result += province ? `${address || district ? ', ' : ''} ${province}` : '';
        result += department ? `${address || district || province ? ', ' : ''} ${department}` : '';
      }
    }

    return result;
  }

  dataUpdate(jobInformation: IClientJobInformationResponse): void {
    this.modalMessage.open();
    this.setProfileWork(jobInformation);
  }

  goToEditProfilePage($event: any): void {
    this.router.navigate(['/profile/edit/step/1']);
  }

  trackByFn(index, item): number {
    return index;
  }

  private _getTerms(): void {
    const pathReq = {
      codigoApp: APPLICATION_CODE
    };
    this.showLoading = true;
    this._Autoservicios.ObtenerTerminosCondiciones(pathReq).subscribe(this._mapInitTerms.bind(this), () => {
      this.showLoading = false;
    });
  }

  private _mapInitTerms(r): void {
    const defaultFrm = { ...r, seguros: true };
    if (this._isNewUser(r)) {
      this._saveTerms({ actualizacion: true, ...defaultFrm });
    }
    this._initFrm = this._getObjWithBoolean(defaultFrm);
    this.showLoading = false;
    this.frm.patchValue(this._initFrm);
  }

  private _getObjWithBoolean(obj: any): any {
    const ok = Object.keys(obj);

    return ok.reduce((acc, k) => ({ ...acc, [k]: !!obj[k] }), {});
  }

  private _isDifferentObj(obj1, obj2): boolean {
    return !isEqual(obj1, obj2);
  }

  private _initValues(): void {
    this.isPerson = this._ClientService.isGroupTypePerson();
  }

  private _isNewUser(frm: any): boolean {
    const ok = Object.keys(frm);

    return ok.every(k => frm[k] === null);
  }

  private _saveTerms(frm): void {
    const frmBoolean = this._getObjWithBoolean(frm);
    const pathReq = {
      codigoApp: APPLICATION_CODE
    };
    const bodyReq = {
      ...frmBoolean,
      actualizacion: true
    };
    this.showLoading = true;
    this._Autoservicios.RegistrarTerminosCondiciones(pathReq, bodyReq).subscribe(
      () => {
        this.showLoading = false;
      },
      () => {
        this.showLoading = false;
      }
    );
  }
}
