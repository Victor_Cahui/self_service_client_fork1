import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'client-perfil-edit-component',
  template: '<router-outlet></router-outlet>'
})
export class EditComponent implements OnInit {
  ngOnInit(): void {}
}
