import { Component, OnInit } from '@angular/core';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PROFILE_EDIT_HEADER } from '@mx/settings/constants/router-titles';
import { EditProfileLang } from '@mx/settings/lang/insured-dependent';
import { EditProfileService } from '../../edit.service';

@Component({
  selector: 'client-perfil-edit-three',
  templateUrl: './step-three.component.html'
})
export class StepThreeComponent implements OnInit {
  lang = EditProfileLang.step3;

  maximumConfimationDaysPerRequest = 5;

  constructor(
    private readonly editProfileService: EditProfileService,
    private readonly headerHelperService: HeaderHelperService
  ) {
    this.headerHelperService.set({
      title: PROFILE_EDIT_HEADER.title,
      subTitle: PROFILE_EDIT_HEADER.subTitle,
      icon: PROFILE_EDIT_HEADER.icon,
      back: false
    });
  }

  ngOnInit(): void {
    const pd = this.editProfileService.retrieveParemeterData();
    this.maximumConfimationDaysPerRequest = pd ? pd.valor : 5;
    this.editProfileService.resetData();
  }
}
