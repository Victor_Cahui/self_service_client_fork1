import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { StepThreeRoutingModule } from './step-three.routing';

import { StepThreeComponent } from './step-three.component';

@NgModule({
  imports: [CommonModule, StepThreeRoutingModule],
  exports: [],
  declarations: [StepThreeComponent],
  providers: []
})
export class StepThreeModule {}
