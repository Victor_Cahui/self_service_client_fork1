import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EditProfileGuard } from '../edit-profile.guard';
import { EditComponent } from './edit.component';

const routes: Routes = [
  {
    path: 'step',
    component: EditComponent,
    children: [
      {
        path: '',
        redirectTo: '1',
        pathMatch: 'prefix'
      },
      {
        path: '1',
        loadChildren: './step-one/step-one.module#StepOneModule'
      },
      {
        path: '2',
        canActivate: [EditProfileGuard],
        loadChildren: './step-two/step-two.module#StepTwoModule'
      },
      {
        path: '3',
        canActivate: [EditProfileGuard],
        loadChildren: './step-three/step-three.module#StepThreeModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditRoutingModule {}
