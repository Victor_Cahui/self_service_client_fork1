import { Location } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FormComponent } from '@mx/components/shared/utils/form';
import { SelectListItem } from '@mx/core/ui';
import { NotificationStatus } from '@mx/core/ui/lib/components/alerts/custom-alert/custom-alert.component';
import { MfModalMessageComponent } from '@mx/core/ui/lib/components/modals/modal-message/modal-message.component';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { GeneralService } from '@mx/services/general/general.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { GENERAL_MAX_LENGTH, GENERAL_MIN_LENGTH, REG_EX } from '@mx/settings/constants/general-values';
import { PROFILE_DATA_HEADER } from '@mx/settings/constants/router-titles';
import { EditProfileLang } from '@mx/settings/lang/insured-dependent';
import { IClientInformationResponse } from '@mx/statemanagement/models/client.models';
import { IProfileSensibleDataRequest } from '@mx/statemanagement/models/profile.model';
import { IdDocument, TypeDocument } from '@mx/statemanagement/models/typedocument.interface';
import { isEmpty, isEqual } from 'lodash-es';
import { Subscription } from 'rxjs';
import { EditProfileService } from '../../edit.service';
import { AllValidationErrors, BasicComponent } from '../basic.component';

@Component({
  selector: 'client-perfil-edit-one',
  templateUrl: './step-one.component.html'
})
export class StepOneComponent implements OnInit {
  @ViewChild(MfModalMessageComponent) modalMessage: MfModalMessageComponent;

  lang = EditProfileLang.step1;
  profile: IClientInformationResponse;
  oldProfileSensibleData: IProfileSensibleDataRequest;
  newProfileSensibleData: IProfileSensibleDataRequest;
  oldTelefonoMovil: string;

  entity: IdDocument;
  profileForm: FormGroup;
  tipoDocumento: AbstractControl;
  numeroDocumento: AbstractControl;
  nombres: AbstractControl;
  apellidoPaterno: AbstractControl;
  apellidoMaterno: AbstractControl;
  fechaNacimiento: AbstractControl;
  telefonoFijo: AbstractControl;
  telefonoMovil: AbstractControl;

  lstDocumentType: Array<SelectListItem>;
  lstDocumentTypeValidators: Array<TypeDocument>;

  nameRegExp = REG_EX.name;
  lastnameRegExp = REG_EX.lastname;
  phoneRegExp = REG_EX.phone;
  numericRegExp = REG_EX.numeric;
  alphaNumericRegExp = REG_EX.alphaNumeric;

  maxLength = 0;
  minLength = 0;

  limitBirthDay: Date;

  generalMaxLength = GENERAL_MAX_LENGTH;
  generalMinLength = GENERAL_MIN_LENGTH;
  objNameRegExp: RegExp;
  objLastnameRegExp: RegExp;
  objNumericeRegExp: RegExp;
  objAlphaNumericRegExp: RegExp;
  showLoading: boolean;
  titleAlert = '';
  messageAlert = '';

  parametersSub: Subscription;
  documentTypesSub: Subscription;
  warningStatus = NotificationStatus.INFO;

  constructor(
    private readonly authService: AuthService,
    private readonly generalService: GeneralService,
    private readonly clientService: ClientService,
    protected formBuilder: FormBuilder,
    private readonly router: Router,
    private readonly editProfileService: EditProfileService,
    private readonly headerHelperService: HeaderHelperService,
    private readonly policiesInfoService: PoliciesInfoService,
    private readonly location: Location
  ) {
    this.objNameRegExp = new RegExp(this.nameRegExp);
    this.objLastnameRegExp = new RegExp(this.lastnameRegExp);
    this.headerHelperService.set(PROFILE_DATA_HEADER);
    this.objNumericeRegExp = new RegExp(this.numericRegExp);
    this.objAlphaNumericRegExp = new RegExp(this.alphaNumericRegExp);
  }

  ngOnInit(): void {
    const dateNow = new Date();
    this.editProfileService.resetData();
    this.limitBirthDay = new Date(dateNow.setFullYear(dateNow.getFullYear() - 18));

    this.entity = this.authService.retrieveEntity();
    this.clientService
      .getClientInformation({
        registros: 1
      })
      .subscribe((response: IClientInformationResponse) => {
        this.profile = response;
        this.oldProfileSensibleData = {
          apellidoPaterno: response.apellidoPaternoCliente,
          apellidoMaterno: response.apellidoMaternoCliente,
          nombre: response.nombreCliente,
          fechaNacimiento: response.fechaNacimiento,
          tipoDocumentoModificado: this.entity.type,
          numeroDocumentoModificado: this.entity.number
        };
        this.oldTelefonoMovil = response.telefonoMovil;
        this.clientService.setUserData(this.profile);
        this.createForm();
        this.getDocumentTypes();
      });
  }

  createForm(): void {
    // workaround for date
    // https://stackoverflow.com/questions/7556591/javascript-date-object-always-one-day-off
    const fn = this.profile.fechaNacimiento ? new Date(this.profile.fechaNacimiento.replace(/-/g, '/')) : void 0;

    this.profileForm = this.formBuilder.group({
      tipoDocumento: [this.entity.type, Validators.required],
      numeroDocumento: [this.entity.number, Validators.required],
      nombres: [this.profile.nombreCliente, Validators.compose([Validators.required, Validators.pattern(REG_EX.name)])],
      apellidoPaterno: [
        this.profile.apellidoPaternoCliente,
        Validators.compose([Validators.required, Validators.pattern(REG_EX.lastname)])
      ],
      apellidoMaterno: [
        this.profile.apellidoMaternoCliente,
        Validators.compose([Validators.required, Validators.pattern(REG_EX.lastname)])
      ],
      fechaNacimiento: [fn, Validators.required],
      telefonoMovil: [this.profile.telefonoMovil, Validators.required]
    });

    this.tipoDocumento = this.profileForm.controls['tipoDocumento'];
    this.numeroDocumento = this.profileForm.controls['numeroDocumento'];
    this.nombres = this.profileForm.controls['nombres'];
    this.apellidoPaterno = this.profileForm.controls['apellidoPaterno'];
    this.apellidoMaterno = this.profileForm.controls['apellidoMaterno'];
    this.fechaNacimiento = this.profileForm.controls['fechaNacimiento'];
    this.telefonoFijo = this.profileForm.controls['telefonoFijo'];
    this.telefonoMovil = this.profileForm.controls['telefonoMovil'];

    this.tipoDocumento.valueChanges.subscribe(this.updateMinMaxDocumentNumber.bind(this));
  }

  showErrors(control: string): boolean {
    return BasicComponent.showBasicErrors(this.profileForm, control);
  }

  getDocumentTypes(): void {
    this.documentTypesSub = this.generalService.getDocumentTypes().subscribe(
      (types: Array<TypeDocument>) => {
        if (types) {
          this.lstDocumentTypeValidators = types;
          this.lstDocumentType = types.map(type => new SelectListItem({ text: type.codigo, value: type.codigo }));
          this.updateMinMaxDocumentNumber(this.entity.type);
        }
      },
      () => {},
      () => {
        this.generalService.setDocumentTypes(this.lstDocumentTypeValidators);
        this.documentTypesSub.unsubscribe();
      }
    );
  }

  updateMinMaxDocumentNumber(value: string): void {
    // this.numeroDocumento.clearValidators();
    const maxLengthNumberDoc = FormComponent.getMaxMinLength(
      'maxlength',
      this.lstDocumentTypeValidators,
      value
    ).toString();
    const minLengthNumberDoc = FormComponent.getMaxMinLength(
      'minlength',
      this.lstDocumentTypeValidators,
      value
    ).toString();
    this.maxLength = parseInt(maxLengthNumberDoc, 10);
    this.minLength = parseInt(minLengthNumberDoc, 10);

    const validators = FormComponent.getValidatorTypeDocument(value, this.lstDocumentTypeValidators);
    this.numeroDocumento.setValidators(validators);
    this.numeroDocumento.updateValueAndValidity();
  }

  clearInput(): void {
    this.numeroDocumento.setValue('');
  }

  isReadyToGo(): boolean {
    this.newProfileSensibleData = {
      apellidoPaterno: this.apellidoPaterno.value,
      apellidoMaterno: this.apellidoMaterno.value,
      nombre: this.nombres.value,
      fechaNacimiento: this.getFormattedDate(this.fechaNacimiento.value),
      tipoDocumentoModificado: this.tipoDocumento.value,
      numeroDocumentoModificado: this.numeroDocumento.value
    };

    return (
      this.profileForm.valid &&
      !(
        isEqual(this.oldProfileSensibleData, this.newProfileSensibleData) &&
        this.oldTelefonoMovil === this.telefonoMovil.value
      )
    );
  }

  cancel(): void {
    this.location.back();
  }

  evaluateData(): void {
    let isSD = false;
    let isNSD = false;
    // has the sensible data changed?
    this.newProfileSensibleData = {
      apellidoPaterno: this.apellidoPaterno.value,
      apellidoMaterno: this.apellidoMaterno.value,
      nombre: this.nombres.value,
      fechaNacimiento: this.getFormattedDate(this.fechaNacimiento.value),
      tipoDocumentoModificado: this.tipoDocumento.value,
      numeroDocumentoModificado: this.numeroDocumento.value
    };

    if (!isEqual(this.oldProfileSensibleData, this.newProfileSensibleData)) {
      isSD = true;
      this.editProfileService.saveSensibleData(this.oldProfileSensibleData, this.newProfileSensibleData);
    }

    if (this.oldTelefonoMovil !== this.telefonoMovil.value) {
      isNSD = true;
      this.editProfileService.saveNonSensibleData(
        { telefonoMovil: this.oldTelefonoMovil },
        { telefonoMovil: this.telefonoMovil.value }
      );
    }

    if (this.validForm()) {
      if ((isSD && isNSD) || isSD) {
        this.router.navigate(['/profile/edit/step/2']);

        return;
      }

      if (isNSD) {
        this.showLoading = true;
        this.clientService
          .sendRequestToUpdateNoSensibleData({
            telefonoMovil: this.telefonoMovil.value
          })
          .toPromise()
          .then(
            response => {
              this.showLoading = false;
              this.profile.telefonoMovil = this.telefonoMovil.value;
              this.clientService.setUserData(this.profile);
              const profileBasicInfo = this.policiesInfoService.profileBasicInfo;
              if (profileBasicInfo) {
                profileBasicInfo.phone = this.telefonoMovil.value;
                this.policiesInfoService.setProfileBasicInfo(profileBasicInfo);
              }
              this.router.navigate(['/profile']);
            },
            (error: HttpErrorResponse) => {
              this.showErrorResponse(error);
            }
          );
      }
    }
  }

  showErrorResponse(error: any): void {
    this.showLoading = false;
    this.editProfileService.resetData();
    setTimeout(() => {
      this.router.navigate(['/profile']);
    }, 3000);
  }

  validForm(): boolean {
    const error: AllValidationErrors = BasicComponent.getFormValidationErrors(this.profileForm.controls).shift();

    return isEmpty(error);
  }

  getFormattedDate(date: Date): string {
    if (!date) {
      return '';
    }
    const year = date.getFullYear();
    let month = (date.getMonth() + 1).toString();
    month = month.length > 1 ? month : `0${month}`;

    let day = date.getDate().toString();
    day = day.length > 1 ? day : `0${day}`;

    return `${year}-${month}-${day}`;
  }

  onKeydown(event, type: string): void {
    const listAllowKeys = [
      'Delete',
      'Backspace',
      'Tab',
      'Shift',
      'Alt',
      'ArrowUp',
      'ArrowDown',
      'ArrowRight',
      'ArrowLeft',
      'CapsLock'
    ];
    if (listAllowKeys.includes(event.key)) {
      return;
    }
    if (/^name$/i.test(type)) {
      if (!this.objNameRegExp.test(event.key)) {
        event.preventDefault();
      }
    } else if (/^lastname$/i.test(type)) {
      if (!this.objLastnameRegExp.test(event.key)) {
        event.preventDefault();
      }
    } else if (/^numeric$/i.test(type)) {
      if (!this.objNumericeRegExp.test(event.key)) {
        event.preventDefault();
      }
    } else if (/^alphanumeric$/i.test(type)) {
      if (!this.objAlphaNumericRegExp.test(event.key)) {
        event.preventDefault();
      }
    }
  }
}
