import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MfButtonModule,
  MfInputDatepickerModule,
  MfInputModule,
  MfModalAlertModule,
  MfModalMessageModule,
  MfModalModule,
  MfSelectModule,
  MfShowErrorsModule
} from '@mx/core/ui';
import { MfCustomAlertModule } from '@mx/core/ui/lib/components/alerts/custom-alert';
import { MfLoaderModule } from '@mx/core/ui/lib/components/global/loader';
import { StepOneComponent } from './step-one.component';
import { StepOneRoutingModule } from './step-one.routing';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    StepOneRoutingModule,
    MfInputModule,
    MfInputDatepickerModule,
    MfShowErrorsModule,
    MfSelectModule,
    MfInputModule,
    MfModalModule,
    MfModalAlertModule,
    MfModalMessageModule,
    MfLoaderModule,
    MfButtonModule,
    MfCustomAlertModule
  ],
  exports: [],
  declarations: [StepOneComponent],
  providers: []
})
export class StepOneModule {}
