import { NgModule } from '@angular/core';
import { EditComponent } from './edit.component';
import { EditRoutingModule } from './edit.routing';

@NgModule({
  imports: [EditRoutingModule],
  exports: [],
  declarations: [EditComponent],
  providers: []
})
export class EditModule {}
