import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { dateToStringFormatMMDDYYYY } from '@mx/core/shared/helpers/util/date';
import { MfModalMessageComponent } from '@mx/core/ui/lib/components/modals/modal-message/modal-message.component';
import { ClientService } from '@mx/services/client.service';
import { GeneralService } from '@mx/services/general/general.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { MAX_CONFIRMATION_DAY_SENSIBLE_PROFILE, MAX_SIZE_MB_IMAGE_PROFILE } from '@mx/settings/constants/key-values';
import { PROFILE_EDIT_HEADER } from '@mx/settings/constants/router-titles';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { EditProfileLang } from '@mx/settings/lang/insured-dependent';
import { IParameterApp } from '@mx/statemanagement/models/general.models';
import { IProfileNonSensibleDataRequest, IProfileSensibleDataRequest } from '@mx/statemanagement/models/profile.model';
import ImageCompressor from 'image-compressor.js';
import * as JSZip from 'jszip';
import { isEmpty, isEqual } from 'lodash-es';
import { humanizeBytes, UploaderOptions, UploadFile, UploadInput, UploadOutput } from 'ngx-uploader';
import { BehaviorSubject, Observable } from 'rxjs';
import { EditProfileService } from '../../edit.service';

@Component({
  selector: 'client-perfil-edit-two',
  templateUrl: './step-two.component.html',
  styleUrls: ['./step-two.component.scss']
})
export class StepTwoComponent implements OnInit, OnDestroy {
  langStep2 = EditProfileLang.step2;
  langStep1 = EditProfileLang.step1;

  oldProfileSensibleData: IProfileSensibleDataRequest;
  newProfileSensibleData: IProfileSensibleDataRequest;

  oldNonSensibleData: IProfileNonSensibleDataRequest;
  newNonSensibleData: IProfileNonSensibleDataRequest;

  hasToUploadOnePicture = false;
  hasToUploadTwoPictures = false;

  // options for upload component
  options: UploaderOptions;
  formData: FormData;

  files: Array<UploadFile>;
  fileUrl: any;

  filesReverso: Array<UploadFile>;
  fileReversoUrl: any;

  uploadInput: EventEmitter<UploadInput>;
  uploadInputReverso: EventEmitter<UploadInput>;
  dragOver: boolean;
  dragOverReverso: boolean;

  zipFile: JSZip;
  uploadFilesEvent = new BehaviorSubject<number>(0);
  filesProcessed = 0;
  filesToProcess = 0;

  uploadEvent: UploadInput = {
    fieldName: 'fotosDocumento',
    type: 'uploadAll',
    method: 'PUT',
    data: {}
  };

  maximumSizeFileMB = 20;
  maximumConfirmationDaysPerRequest: string;
  isSubmitting = false;

  @ViewChild(MfModalMessageComponent) modalMessage: MfModalMessageComponent;
  titleAlert = '';
  messageAlert = '';
  messageDocumentNew = GeneralLang.Messages.messageDocumentNew;
  withOutInformation = GeneralLang.Texts.WithOutInformation;

  sensibleData: IProfileSensibleDataRequest = {};
  timeoutError = 104500;
  eventFrontal: any;
  eventReverse: any;
  parametersSubject: Observable<Array<IParameterApp>>;

  constructor(
    private readonly editProfileService: EditProfileService,
    private readonly profileService: ClientService,
    private readonly router: Router,
    private readonly generalService: GeneralService,
    private readonly headerHelperService: HeaderHelperService
  ) {
    this.sensibleData = {};
    this.options = { concurrency: 1, allowedContentTypes: ['image/jpeg', 'image/png'], maxUploads: 1 };
    this.files = []; // local uploading files array
    this.filesReverso = []; // local uploading files array
    this.uploadInput = new EventEmitter<UploadInput>(); // input events, we use this to emit data to ngx-uploader
    this.uploadInputReverso = new EventEmitter<UploadInput>(); // input events, we use this to emit data to ngx-uploader
    this.zipFile = new JSZip();
    this.headerHelperService.set(PROFILE_EDIT_HEADER);
  }

  ngOnInit(): void {
    [this.oldProfileSensibleData, this.newProfileSensibleData] = this.editProfileService.retrieveSensibleData();
    [this.oldNonSensibleData, this.newNonSensibleData] = this.editProfileService.retrieveNonSensibleData();

    switch (this.newProfileSensibleData.tipoDocumentoModificado) {
      case 'RUC':
        this.hasToUploadOnePicture = this.hasToUploadTwoPictures = false;
        this.filesToProcess = 0;
        break;
      case 'PEX':
        this.hasToUploadTwoPictures = false;
        this.hasToUploadOnePicture = true;
        this.filesToProcess = 1;
        this.messageDocumentNew = this.langStep2.uploadImageDocument;
        break;
      case 'DNI':
      default:
        this.hasToUploadTwoPictures = true;
        this.hasToUploadOnePicture = false;
        this.filesToProcess = 2;
        break;
    }

    if (this.hasToUploadOnePicture || this.hasToUploadTwoPictures) {
      this.uploadFilesEvent.subscribe({
        next: val => {
          if (this.filesProcessed === this.filesToProcess) {
            this.zipFile
              .generateAsync(
                {
                  type: 'blob',
                  compression: 'DEFLATE',
                  compressionOptions: {
                    level: 9 // force a compression and a compression level for this file
                  }
                },
                metadata => {}
              )
              .then(content => {
                const zipFile = JSON.parse(JSON.stringify(this.files[0]));

                const blob: any = new Blob([content]);
                blob.lastModifiedDate = new Date();
                blob.name = 'fotosDocumento.zip';

                zipFile.nativeFile = blob;
                zipFile.name = 'fotosDocumento';
                // emiting library event to start the upload process to the server
                // this event is part of the thrid party library
                // this.uploadInput.emit(this.uploadEvent);

                // instead of uploading with library, we will upload with our services
                this.profileService
                  .sendRequestToUpdateSensibleData(this.sensibleData, zipFile)
                  .toPromise()
                  .then(
                    () => {
                      if (
                        !isEmpty(this.newNonSensibleData) &&
                        !isEqual(this.newNonSensibleData, this.oldNonSensibleData)
                      ) {
                        this.profileService
                          .sendRequestToUpdateNoSensibleData(this.newNonSensibleData)
                          .toPromise()
                          .then(
                            () => {
                              this.router.navigate(['/profile/edit/step/3']);
                            },
                            (error: HttpErrorResponse) => {
                              this.showErrorResponse(error);
                              setTimeout(() => {
                                this.router.navigate(['/profile/edit/step/1']);
                              }, this.timeoutError);
                            }
                          );
                      } else {
                        this.router.navigate(['/profile/edit/step/3']);
                      }
                    },
                    (error: HttpErrorResponse) => {
                      this.showErrorResponse(error);
                      setTimeout(() => {
                        this.router.navigate(['/profile/edit/step/1']);
                      }, this.timeoutError);
                    }
                  );
              });
          }
          // This subscriber is executed at declaration time
          this.filesProcessed += 1;
        }
      });
    }

    // getting parameters
    this.getParameters();
  }

  ngOnDestroy(): void {
    this.uploadFilesEvent.unsubscribe();
  }

  cancel(): void {
    this.router.navigate(['/profile/edit/step/1']);
  }

  showErrorResponse(error: any): void {
    this.titleAlert = GeneralLang.Messages.Fail;
    this.messageAlert = (error && error.message) || GeneralLang.Messages.ErrorUpdateAndAdd;
    this.modalMessage.open();
    this.isSubmitting = false;
    this.files.length = 0;
    this.filesReverso.length = 0;
  }

  getParameters(): void {
    this.parametersSubject = this.generalService.getParametersSubject();
    this.parametersSubject.subscribe(params => {
      this.maximumConfirmationDaysPerRequest = this.generalService.getValueParams(
        MAX_CONFIRMATION_DAY_SENSIBLE_PROFILE
      );
      this.maximumSizeFileMB = Number(this.generalService.getValueParams(MAX_SIZE_MB_IMAGE_PROFILE));
    });
  }

  // tslint:disable-next-line:cyclomatic-complexity
  onUploadOutput(output: UploadOutput): void {
    if (output.type === 'allAddedToQueue') {
      // when all files added in queue
      // uncomment this if you want to auto upload files when added
    } else if (output.type === 'addedToQueue' && output.file) {
      // add file to array when added
      // checking if we are adding the same file
      if (
        this.filesReverso.length &&
        output.file.nativeFile.name === this.filesReverso[0].nativeFile.name &&
        output.file.nativeFile.size === this.filesReverso[0].nativeFile.size
      ) {
        this.files.length = 0;
        this.titleAlert = GeneralLang.Messages.Fail;
        this.messageAlert = this.langStep2.dontRepeatImage;
        this.modalMessage.open();

        if (this.eventFrontal) {
          this.eventFrontal.srcElement.value = '';
        }

        return;
      }
      // checking size
      if (output.file.size > this.maximumSizeFileMB * 1024 * 1024) {
        this.files.length = 0;
        this.titleAlert = GeneralLang.Messages.Fail;
        if (this.eventFrontal) {
          this.eventFrontal.srcElement.value = '';
        }
        this.messageAlert = `${GeneralLang.Messages.ErrorMaxMB.start}${this.maximumSizeFileMB}${GeneralLang.Messages.ErrorMaxMB.end}`;
        this.modalMessage.open();

        return;
      }

      // overwritten an existent image
      if (!isEmpty(this.files)) {
        this.files.length = 0;
      }
      this.files.push(output.file);
    } else if (output.type === 'uploading' && output.file) {
      // update current data in files array for uploading file
      const index = this.files.findIndex(file => output.file && file.id === output.file.id);
      this.files[index] = output.file;
    } else if (output.type === 'removed') {
      // remove file from array when removed
      this.files = this.files.filter((file: UploadFile) => file !== output.file);
    } else if (output.type === 'done') {
      this.files.slice(0, -1);
    } else if (output.type === 'dragOver') {
      this.dragOver = true;
    } else if (output.type === 'dragOut') {
      this.dragOver = false;
    } else if (output.type === 'drop') {
      this.dragOver = false;
    } else if (output.type === 'rejected' && output.file) {
      this.removeAllFiles(null);
      this.files.length = 0;

      this.titleAlert = GeneralLang.Messages.Fail;
      this.messageAlert = GeneralLang.Messages.ErrorImageFormat;
      this.modalMessage.open();
    }

    this.fileUrl = this.files.length ? this.getUrl(this.files[0]) : null;
  }

  openFileModal(id: string): void {
    document.getElementById(id).click();
  }

  onUploadOutputReverso(output: UploadOutput): void {
    if (output.type === 'allAddedToQueue') {
      // when all files added in queue
    } else if (output.type === 'addedToQueue' && typeof output.file !== 'undefined') {
      // add file to array when added
      // checking if we are adding the same file
      if (
        this.files.length &&
        output.file.nativeFile.name === this.files[0].nativeFile.name &&
        output.file.nativeFile.size === this.files[0].nativeFile.size
      ) {
        this.filesReverso.length = 0;
        this.titleAlert = GeneralLang.Messages.Fail;
        this.messageAlert = this.langStep2.dontRepeatImage;
        this.modalMessage.open();
        if (this.eventReverse) {
          this.eventReverse.srcElement.value = '';
        }

        return;
      }
      // checking size
      if (output.file.size > this.maximumSizeFileMB * 1024 * 1024) {
        this.filesReverso.length = 0;
        this.titleAlert = GeneralLang.Messages.Fail;
        if (this.eventReverse) {
          this.eventReverse.srcElement.value = '';
        }
        this.messageAlert = `${GeneralLang.Messages.ErrorMaxMB.start}${this.maximumSizeFileMB}${GeneralLang.Messages.ErrorMaxMB.end}`;
        this.modalMessage.open();

        return;
      }

      // overwritten an existent image
      if (!isEmpty(this.filesReverso)) {
        this.filesReverso.length = 0;
      }
      this.filesReverso.push(output.file);
    } else if (output.type === 'uploading' && typeof output.file !== 'undefined') {
      // update current data in files array for uploading file
      const index = this.filesReverso.findIndex(
        file => typeof output.file !== 'undefined' && file.id === output.file.id
      );
      this.filesReverso[index] = output.file;
    } else if (output.type === 'removed') {
      // remove file from array when removed
      this.filesReverso = this.filesReverso.filter((file: UploadFile) => file !== output.file);
    } else if (output.type === 'dragOver') {
      this.dragOverReverso = true;
    } else if (output.type === 'dragOut') {
      this.dragOverReverso = false;
    } else if (output.type === 'drop') {
      this.dragOverReverso = false;
    } else if (output.type === 'rejected' && typeof output.file !== 'undefined') {
      this.removeAllFiles(null, false);
      this.filesReverso.length = 0;
      this.titleAlert = GeneralLang.Messages.Fail;
      this.messageAlert = GeneralLang.Messages.ErrorImageFormat;
      this.modalMessage.open();
    }

    if (this.filesReverso.length) {
      this.fileReversoUrl = this.getUrl(this.filesReverso[0]);
    }
  }

  sendData(): void {
    this.sensibleData = {};
    if (this.newProfileSensibleData.apellidoPaterno !== this.oldProfileSensibleData.apellidoPaterno) {
      this.sensibleData.apellidoPaterno = this.newProfileSensibleData.apellidoPaterno || '';
      this.uploadEvent.data['apellidoPaterno'] = this.sensibleData.apellidoPaterno;
    }
    if (this.newProfileSensibleData.apellidoMaterno !== this.oldProfileSensibleData.apellidoMaterno) {
      this.sensibleData.apellidoMaterno = this.newProfileSensibleData.apellidoMaterno || '';
      this.uploadEvent.data['apellidoMaterno'] = this.sensibleData.apellidoMaterno;
    }
    if (this.newProfileSensibleData.nombre !== this.oldProfileSensibleData.nombre) {
      this.sensibleData.nombre = this.newProfileSensibleData.nombre || '';
      this.uploadEvent.data['nombre'] = this.sensibleData.nombre;
    }
    if (this.newProfileSensibleData.fechaNacimiento !== this.oldProfileSensibleData.fechaNacimiento) {
      this.sensibleData.fechaNacimiento = this.newProfileSensibleData.fechaNacimiento || '';
      this.uploadEvent.data['fechaNacimiento'] = this.sensibleData.fechaNacimiento;
    }
    if (this.newProfileSensibleData.tipoDocumentoModificado !== this.oldProfileSensibleData.tipoDocumentoModificado) {
      this.sensibleData.tipoDocumentoModificado = this.newProfileSensibleData.tipoDocumentoModificado || '';
      this.uploadEvent.data['tipoDocumento'] = this.sensibleData.tipoDocumentoModificado;
    }
    if (
      this.newProfileSensibleData.numeroDocumentoModificado !== this.oldProfileSensibleData.numeroDocumentoModificado
    ) {
      this.sensibleData.numeroDocumentoModificado = this.newProfileSensibleData.numeroDocumentoModificado || '';
      this.uploadEvent.data['numeroDocumento'] = this.sensibleData.numeroDocumentoModificado;
    }

    if (/^RUC$/i.test(this.newProfileSensibleData.tipoDocumentoModificado)) {
      this.isSubmitting = true;
      this.profileService
        .sendRequestToUpdateSensibleData(this.sensibleData)
        .toPromise()
        .then(
          (response: any) => {
            if (!isEmpty(this.newNonSensibleData) && !isEqual(this.newNonSensibleData, this.oldNonSensibleData)) {
              this.profileService
                .sendRequestToUpdateNoSensibleData(this.newNonSensibleData)
                .toPromise()
                .then(
                  () => {
                    this.isSubmitting = false;
                    this.router.navigate(['/profile/edit/step/3']);
                  },
                  (error: HttpErrorResponse) => {
                    this.showErrorResponse(error);
                    setTimeout(() => {
                      this.router.navigate(['/profile/edit/step/1']);
                    }, this.timeoutError);
                  }
                );
            } else {
              this.isSubmitting = false;
              this.router.navigate(['/profile/edit/step/3']);
            }
          },
          (error: HttpErrorResponse) => {
            this.showErrorResponse(error);
            setTimeout(() => {
              this.router.navigate(['/profile/edit/step/1']);
            }, this.timeoutError);
          }
        );
    } else {
      this.startUpload();
    }
  }

  startUpload(): void {
    this.isSubmitting = true;
    // tslint:disable-next-line: no-this-assignment
    const that = this;
    // workaround to use only one array
    if (this.hasToUploadTwoPictures) {
      this.files.push(this.filesReverso[0]);
    }

    let index = 0;
    for (const item of this.files) {
      index++;
      // tslint:disable-next-line:no-unused
      const imageCompressor = new ImageCompressor(item.nativeFile, {
        quality: 0.6,
        success(result: Blob): void {
          that.performTransformations(result, item, index);
        },
        error(e: Error): void {
          that.titleAlert = GeneralLang.Messages.Fail;
          // tslint:disable-next-line: no-invalid-this
          that.messageAlert = this.langStep2.errorImageProcess;
          that.modalMessage.open();
          that.isSubmitting = false;
        }
      });
    }
  }

  isReadyToGo(): boolean {
    if (this.hasToUploadOnePicture && !isEmpty(this.files)) {
      return true;
    }
    if (this.hasToUploadTwoPictures && !isEmpty(this.files) && !isEmpty(this.filesReverso)) {
      return true;
    }
    if (!(this.hasToUploadOnePicture || this.hasToUploadTwoPictures)) {
      return true;
    }

    return false;
  }

  /**
   * Performs image and zip compression, and it sends the final event to upload the data to the
   * backend
   */
  performTransformations(result: Blob, item: UploadFile, index: number): void {
    // tslint:disable-next-line: no-this-assignment
    const that = this;
    const blob: any = new Blob([result]);
    blob.lastModifiedDate = new Date();
    item.nativeFile = blob;
    const fn = item.name;
    const myfile: Blob = item.nativeFile as Blob;
    this.zipFile.file(fn, myfile);
    this.zipFile
      .generateAsync(
        {
          type: 'blob',
          compression: 'DEFLATE',
          compressionOptions: {
            level: 9 // force a compression and a compression level for this file
          }
        },
        metadata => {
          if (metadata.currentFile) {
          }
        }
      )
      .then(content => {
        const myblob: any = new Blob([content]);
        myblob.lastModifiedDate = new Date();
        item.nativeFile = myblob;
        that.uploadFilesEvent.next(1);
      });
  }

  cancelUpload(id: string): void {
    this.uploadInput.emit({ type: 'cancel', id });
  }

  removeFile(id: string, isFrontal: boolean = true): void {
    if (isFrontal) {
      this.files = [];
      this.uploadInput.emit({ type: 'remove', id });
    } else {
      this.filesReverso = [];
      this.uploadInputReverso.emit({ type: 'remove', id });
    }
  }

  removeAllFiles(event, isFrontal: boolean = true): void {
    if (isFrontal) {
      this.eventFrontal = event;
      this.uploadInput.emit({ type: 'removeAll' });
    } else {
      this.eventReverse = event;
      this.uploadInputReverso.emit({ type: 'removeAll' });
    }
  }

  trackByFn(index, item): any {
    return index;
  }

  flipTopMessage(): boolean {
    return (
      !/^RUC$/i.test(this.newProfileSensibleData.tipoDocumentoModificado) &&
      (isEmpty(this.files) && isEmpty(this.filesReverso))
    );
  }

  // Formato de fecha
  formatDate(date): string {
    return dateToStringFormatMMDDYYYY(date);
  }

  getUrl(file: UploadFile): any {
    const _URL = window.URL || (window as any).webkitURL;

    return _URL.createObjectURL(file.nativeFile);
  }

  getSize(size: number): string {
    return humanizeBytes(size);
  }
  // tslint:disable-next-line: max-file-line-count
}
