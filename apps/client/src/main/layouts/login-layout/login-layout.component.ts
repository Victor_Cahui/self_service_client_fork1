import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit, Renderer2 } from '@angular/core';
import { NotificationService } from '@mx/core/shared/helpers/notification/service/notification.service';
import { PLATFORMS, PlatformService } from '@mx/core/shared/helpers/util/platform.service';
import { Autoservicios } from '@mx/core/shared/providers/services';

@Component({
  selector: 'client-app-login-layout',
  templateUrl: './login-layout.component.html'
})
export class LoginLayoutComponent implements OnInit {
  constructor(
    private readonly platformService: PlatformService,
    private readonly _Autoservicios: Autoservicios,
    private readonly notifyService: NotificationService,
    private readonly renderer2: Renderer2,
    @Inject(DOCUMENT) private readonly document: Document
  ) {}

  ngOnInit(): void {
    const isIE = this.platformService.is(PLATFORMS.IE);
    const body = this.document.getElementsByTagName('body')[0];
    this.renderer2[isIE ? 'addClass' : 'removeClass'](body, 'g-ie-body--login');
    this.checkMaintenanceStatus();
  }

  checkMaintenanceStatus(): void {
    const params = {
      codigoApp: 'APC',
      documento: ''
    };
    this._Autoservicios.ObtenerEstadoMantenimiento(params).subscribe(
      response => {
        if (response && response.mantenimiento) {
          const error = {
            status: '503'
          };
          this.notifyService.addErrorWithData('', error);
        }
      },
      error => {
        console.error(error);
      }
    );
  }
}
