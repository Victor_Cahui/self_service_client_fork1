import { Component, OnDestroy, OnInit } from '@angular/core';
import { environment } from '@mx/environments/environment';

@Component({
  selector: 'client-app-login-footer',
  templateUrl: './login-footer.component.html',
  styleUrls: ['./login-footer.component.scss']
})
export class LoginFooterComponent implements OnInit, OnDestroy {
  version: string;

  ngOnInit(): void {
    const v = environment.VERSION;
    this.version = v.split('+')[0];
  }

  ngOnDestroy(): void {}
}
