import { NgModule } from '@angular/core';
import { PlatformService } from '@mx/core/shared/helpers/util/platform.service';
import { LoginFooterModule } from './login-footer/login-footer.module';
import { LoginLayoutComponent } from './login-layout.component';
import { LoginTopModule } from './login-top/login-top.module';

@NgModule({
  imports: [LoginFooterModule, LoginTopModule],
  exports: [LoginLayoutComponent],
  declarations: [LoginLayoutComponent],
  providers: [PlatformService]
})
export class LoginLayoutModule {}
