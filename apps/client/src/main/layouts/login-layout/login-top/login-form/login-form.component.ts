import { Component, HostBinding, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { PROFILE_ADD_ICON, PROFILE_ICON } from '@mx/settings/constants/images-values';
import { LOCAL_STORAGE_KEYS } from '@mx/settings/constants/key-values';
import { AuthLang } from '@mx/settings/lang/auth.lang';
import { ILogguedUser } from '@mx/statemanagement/models/auth.interface';
import { Subject, Subscription } from 'rxjs';

@Component({
  selector: 'client-app-login-form',
  templateUrl: './login-form.component.html'
})
export class LoginFormComponent implements OnInit, OnDestroy {
  static updateView: Subject<boolean> = new Subject();

  @HostBinding('class') container = 'col col-12 col-lg-6';
  text: string;
  icon = PROFILE_ICON;
  success: boolean;
  showIcon: boolean;
  subRouter: Subscription;
  lang = AuthLang.Titles;

  constructor(private readonly localStorage: LocalStorageService, private readonly router: Router) {}

  ngOnInit(): void {
    const ruta = this.router.url.split('?')[0];
    this.text = this.getTexto(ruta);
    this.subRouter = this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        this.text = this.getTexto(event.urlAfterRedirects.split('?')[0]);
      }
    });
    LoginFormComponent.updateView.subscribe((res: boolean) => {
      setTimeout(() => {
        this.icon = res ? PROFILE_ICON : PROFILE_ADD_ICON;
      }, 10);
    });
  }

  ngOnDestroy(): void {
    if (this.subRouter) {
      this.subRouter.unsubscribe();
    }
  }

  getTexto(ruta: string): string {
    let texto: string;
    this.success = false;
    this.showIcon = false;
    switch (ruta) {
      case '/login':
        texto = this.lang.Login;
        break;
      case '/login-type':
        texto = this.lang.UserType;
        break;
      case '/registro':
        texto = this.lang.NewAccount;
        break;
      case '/remember-password':
        texto = this.lang.RememberMe;
        break;
      case '/welcome':
        const data: ILogguedUser = this.localStorage.getData(LOCAL_STORAGE_KEYS.USER);
        let userWelcome = data ? data.name : '';
        userWelcome = StringUtil.replaceInitials(userWelcome);
        texto = this.lang.Welcome.replace('{{user}}', userWelcome);
        break;
      case '/enable-user':
        texto = this.lang.EnableUser;
        this.icon = PROFILE_ADD_ICON;
        break;
      case '/recover-user':
        texto = this.lang.ForgotPassword;
        break;
      case '/send-success':
        this.success = true;
        break;
      case '/restore-password':
        texto = this.lang.RestorePassword;
        this.icon = PROFILE_ADD_ICON;
        break;
      case '/confirm-restore-password':
        this.success = true;
        break;
      case '/expired-token':
        this.success = true;
        this.showIcon = false;
        break;
      default:
        break;
    }

    return texto;
  }
}
