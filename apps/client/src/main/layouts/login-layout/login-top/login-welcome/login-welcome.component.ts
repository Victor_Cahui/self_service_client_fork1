import { Component, HostBinding, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { WellcomeMessage } from '@mx/settings/lang/auth.lang';
import { Subject } from 'rxjs';

@Component({
  selector: 'client-app-login-welcome',
  templateUrl: './login-welcome.component.html'
})
export class LoginWelcomeComponent implements OnInit {
  static updateView: Subject<boolean> = new Subject();

  @HostBinding('class') container = 'col col-12 col-lg-5 g-c--white1';
  showMessage: boolean;
  lang = WellcomeMessage;

  constructor(private readonly router: Router) {
    LoginWelcomeComponent.updateView.subscribe((res: boolean) => {
      setTimeout(() => {
        this.showMessage = res;
      }, 10);
    });
  }

  ngOnInit(): void {
    const ruta = this.router.url.split('?')[0];
    this.showMessage = !(ruta === '/expired-token');
  }
}
