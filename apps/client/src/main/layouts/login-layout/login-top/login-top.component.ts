import { Component, HostBinding, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'client-app-login-top',
  templateUrl: './login-top.component.html'
})
export class LoginTopComponent implements OnInit, OnDestroy {
  @HostBinding('class') container = 'g-ui--sckity d-flex align-items-center';

  ngOnInit(): void {}

  ngOnDestroy(): void {}
}
