import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { MenuComponent } from '@mx/components/shared/utils/menu';
import { VerifySessionService } from '@mx/core/shared/common/services/verify-session.service';
import { NotificationService } from '@mx/core/shared/helpers/notification/service/notification.service';
import { UnsubscribeOnDestroy } from '@mx/core/shared/helpers/util/unsubscribe-on-destroy';
import { AppFacade } from '@mx/core/shared/state/app';
import { AuthService } from '@mx/services/auth/auth.service';
import { GeneralService } from '@mx/services/general/general.service';
import { WebSocketAPI } from '@mx/services/websockets/websocket-api';
import { ERROR_CODE, ErrorCodes, VERIFY_TOKEN_TIME_M } from '@mx/settings/constants/general-values';
import { BUTTON_TO_TOP } from '@mx/settings/constants/images-values';
import { WEB_SOCKET_HAB_MANTENIMIENTO } from '@mx/settings/constants/key-values';
import { IParameterApp } from '@mx/statemanagement/models/general.models';
import { Observable, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'client-main-layout',
  templateUrl: './main-layout.component.html'
})
export class MainLayoutComponent extends UnsubscribeOnDestroy implements OnInit, OnDestroy {
  buttonTop: number;
  buttonTopIcon = BUTTON_TO_TOP;
  verifySessionSub: Subscription;
  parametersSubject: Observable<Array<IParameterApp>>;
  showModalSession: boolean;
  enableSocketMaintenance: boolean;

  constructor(
    private readonly _AuthService: AuthService,
    private readonly renderer2: Renderer2,
    private readonly _AppFacade: AppFacade,
    private readonly verifySessionService: VerifySessionService,
    private readonly notifyService: NotificationService,
    @Inject(DOCUMENT) private readonly document: Document,
    private readonly generalService: GeneralService,
    private readonly _WebSocketAPI: WebSocketAPI
  ) {
    super();
    this.buttonTop = window.innerHeight * 2;
  }

  ngOnInit(): void {
    this._AuthService.getUserToken() && this._AppFacade.LoadLookups();
    this.getParameters();
    const body = this.document.getElementsByTagName('body')[0];
    this.renderer2.removeClass(body, 'g-ie-body--login');
    this.hanlderSession();
  }

  ngOnDestroy(): void {
    if (this.verifySessionSub) {
      this.verifySessionSub.unsubscribe();
    }
  }

  closeNavBox(): void {
    MenuComponent.closeNav();
  }

  getParameters(): void {
    this.enableSocketMaintenance = this.generalService.getValueParams(WEB_SOCKET_HAB_MANTENIMIENTO) === '1' || false;
    this.handlerWebSocket();
  }

  private hanlderSession(): void {
    // INFO: Para router y intersector
    this.verifySessionService.onSessionExpired.subscribe(() => {
      this.handlerModal(true);
    });
    // INFO: Para validacion cada 60s
    this.verifySessionSub = this.verifySessionService.watchVerifySession(VERIFY_TOKEN_TIME_M).subscribe(res => {
      this.handlerModal(!res);
    });
  }

  private handlerModal(open: boolean): void {
    if (open && !this.showModalSession) {
      this.showModalSession = true;
    }
  }

  private handlerWebSocket(): void {
    if (this.enableSocketMaintenance) {
      this._WebSocketAPI._connect();
      this._WebSocketAPI.onMaintenance$
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(this.changeMaintenance.bind(this), this.errorCallBack.bind(this));
    }
  }

  errorCallBack(error: any): void {
    if (!error) {
      return;
    }
    console.warn(`errorCallBack -> `, error);
    setTimeout(() => {
      this._WebSocketAPI._connect();
    }, 5000);
  }

  changeMaintenance(message: any): void {
    if (!message) {
      return;
    }
    const result = JSON.parse(message.body);
    if (result) {
      const error = {
        status: ERROR_CODE,
        error: {
          mensaje: ErrorCodes['AUT-150'].message,
          codigoOperacion: ErrorCodes['AUT-150'].code
        }
      };
      this.notifyService.addErrorWithData('', error);
      setTimeout(() => {
        localStorage.setItem('canReload', '1');
        document.getElementsByTagName('body')[0].style.display = 'none';
        this._AuthService.logout();
      }, 10000);
    }
  }
}
