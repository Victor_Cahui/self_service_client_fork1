import { CurrencyPipe, DatePipe } from '@angular/common';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SendReceiptComponent } from '@mx/components/payment/send-receipt/send-receipt.component';
import {
  COVERAGE_STATUS,
  POLICY_QUOTA_TYPE,
  SITUATION_TYPE,
  TRAFFIC_LIGHT_COLOR
} from '@mx/components/shared/utils/policy';
import { isExpired } from '@mx/core/shared/helpers/util/date';
import { FileUtil } from '@mx/core/shared/helpers/util/file';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { GeneralService } from '@mx/services/general/general.service';
import { PaymentService } from '@mx/services/payment.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import {
  APPLICATION_CODE,
  COLOR_STATUS,
  FORMAT_DATE_DDMMYYYY,
  URL_FRAGMENTS
} from '@mx/settings/constants/general-values';
import { CALENDAR_ICON, NO_PENDING_PROCEDURES_ICON, PORCENTAGE_ICON } from '@mx/settings/constants/images-values';
import { EARLY_POLICY_RENEWAL_DAYS } from '@mx/settings/constants/key-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { PaymentLang } from '@mx/settings/lang/payment.lang';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import { VehiclesLang } from '@mx/settings/lang/vehicles.lang';
import { IAssociatedReceipt, IPaymentQuoteCard, IPaymentTypeResponse } from '@mx/statemanagement/models/payment.models';
import { IPolicyFeeRequest, IPolicyFeeResponse } from '@mx/statemanagement/models/policy.interface';
import { Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'client-card-my-fees',
  templateUrl: './card-my-fees.component.html',
  providers: [CurrencyPipe, DatePipe]
})
export class CardMyFeesComponent extends GaUnsubscribeBase implements OnInit {
  @ViewChild(SendReceiptComponent) modalEmail: SendReceiptComponent;

  @Input() policyType: string;

  policyNumber: string;
  feeList: Array<IPolicyFeeResponse>;
  paymentsTypes: Array<IPaymentTypeResponse>;
  titleMyFees = GeneralLang.Titles.MyFees;
  labelQuota = PolicyLang.Texts.Fee;
  labelPolicies = PolicyLang.Labels.Policies;
  billNumber = PolicyLang.Labels.BillNumber;
  noticeNumber = PolicyLang.Labels.NoticeNumber;
  calendarIcon = CALENDAR_ICON;
  myFeesIcon = PORCENTAGE_ICON;
  tipoDocumentoPago = POLICY_QUOTA_TYPE;
  tipoSituacion = SITUATION_TYPE;
  coverageStatus = COVERAGE_STATUS;
  textInProcess = VehiclesLang.Texts.InProcess;
  labelChassis = VehiclesLang.Labels.ChassisNumber;
  labelMotor = VehiclesLang.Labels.MotorNumber;
  labelColor = VehiclesLang.Labels.Color;
  insuredFragment = URL_FRAGMENTS.INSURED;
  noFeesIcon = NO_PENDING_PROCEDURES_ICON;
  colorStatus = COLOR_STATUS;
  messageWithoutFees = PaymentLang.Messages.WithoutFees;
  collapse = false;
  showLoading: boolean;
  showSendBillToEmail: boolean;
  showSpinnerReceipts: boolean;
  openEmail: boolean;
  receiptSelected: string;
  fileUtil: FileUtil;
  pdfSub: Subscription;
  renewDays: number;

  constructor(
    private readonly generalService: GeneralService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly currencyPipe: CurrencyPipe,
    private readonly datePipe: DatePipe,
    private readonly policiesService: PoliciesService,
    private readonly paymentService: PaymentService,
    private readonly policiesInfoService: PoliciesInfoService,
    private readonly authService: AuthService,
    private readonly _Autoservicios: Autoservicios,
    protected gaService: GAService
  ) {
    super(gaService);
    this.fileUtil = new FileUtil();
  }

  ngOnInit(): void {
    this.getParameters();
    this.paymentsTypes = [];
    this.policyNumber = this.activatedRoute.snapshot.params['policyNumber'];
    const policyInfo = this.policiesInfoService.getClientPolicy(this.policyNumber);

    const params: IPolicyFeeRequest = {
      codigoApp: APPLICATION_CODE,
      numeroPoliza: this.policyNumber,
      codCia: `${policyInfo.codCia}`,
      numSpto: policyInfo.numSpto
    };
    this.showLoading = true;
    this.policiesService
      .feePlan(params)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(
        (response: Array<IPolicyFeeResponse>) => {
          response.forEach(fee => {
            const expiredPayment = isExpired(fee.fechaExpiracion);
            const symbol = `${StringUtil.getMoneyDescription(fee.codigoMoneda)} `;
            const semaforo = TRAFFIC_LIGHT_COLOR[fee.semaforo];
            const coverageStatus = `${fee.estadoCobertura || ''}`;
            const showQuota = !(
              coverageStatus === COVERAGE_STATUS.RECIBO_VENCIDO || coverageStatus === COVERAGE_STATUS.SIN_COBERTURA
            );
            const situationType = fee.tipoSituacion;
            const isUnique = fee.cuota === '1/1';

            fee.amountFee = this.currencyPipe.transform(fee.montoTotal, symbol);
            fee.expirationDate = this.datePipe.transform(fee.fechaExpiracion, FORMAT_DATE_DDMMYYYY);
            fee.paymentDate = this.datePipe.transform(fee.fechaPago, FORMAT_DATE_DDMMYYYY);
            fee.tiposPagosNombre = this.getPaymentTypeName(fee.tiposPagosId);
            fee.showQuota = showQuota;
            fee.bulletColor = COLOR_STATUS[semaforo];
            fee.policyType = fee.tipoPoliza;
            fee.showCoverageStatus = coverageStatus !== COVERAGE_STATUS.CON_COBERTURA;
            fee.policyStatusLabel = expiredPayment
              ? PaymentLang.Texts.ExpiredPayment
              : PaymentLang.Texts.CurrentPayment;
            fee.tipoSituacion = situationType;
            fee.cuota = isUnique ? PolicyLang.Texts.Unique.toLowerCase() : fee.cuota;
            fee.isNotice = fee.tipoDocumentoPago === POLICY_QUOTA_TYPE.AVISO;
            fee.paymentFee = this._mapPayment(fee);

            return fee;
          });

          this.feeList = response;
          this.showLoading = false;
        },
        error => {
          this.feeList = [];
          this.showLoading = false;
          console.error(error);
        }
      );
  }

  getPaymentTypeName(id: any): string {
    this.paymentsTypes = this.paymentService.getPaymentTypesData() || [];
    const findPaymentsType = this.paymentsTypes.filter(data => data.tiposPagosId.toString() === id);

    return findPaymentsType.length ? findPaymentsType[0].tiposPagosDescripcion : '';
  }

  // Parametros Generales
  getParameters(): void {
    this.generalService
      .getParametersSubject()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(() => (this.renewDays = +this.generalService.getValueParams(EARLY_POLICY_RENEWAL_DAYS)));
  }

  // Enviar recibo por correo
  openEmailModal(receipt: string): void {
    this.openEmail = true;
    setTimeout(() => {
      this.modalEmail.receiptNumber = receipt;
      this.modalEmail.open();
    });
  }

  // Descargar PDF de Recibo
  downloadPDF(receipt: IPolicyFeeResponse): void {
    if (this.showSpinnerReceipts) {
      return;
    }
    this.receiptSelected = receipt.numRecibo;
    const isNotice = receipt.tipoDocumentoPago === this.tipoDocumentoPago.AVISO;
    const numberReciboOrAviso = isNotice ? receipt.numeroAviso : receipt.numRecibo;
    const pathReq = {
      codigoApp: APPLICATION_CODE,
      codCia: receipt.codCia,
      numeroRecibo: numberReciboOrAviso,
      tipoDocumentoPago: receipt.tipoDocumentoPago
    };
    this.showSpinnerReceipts = true;
    this.pdfSub = this._Autoservicios.ObtenerComprobantePagoHpexstream(pathReq).subscribe(
      (res: any) => {
        try {
          const fileName = isNotice ? `Aviso ${receipt.numeroAviso}` : `Recibo ${receipt.numRecibo}`;
          this.fileUtil.download(res.base64, 'pdf', fileName);
        } catch (error) {}
      },
      () => {
        this.showSpinnerReceipts = false;
      },
      () => {
        this.showSpinnerReceipts = false;
        this.pdfSub.unsubscribe();
      }
    );
  }

  private _mapPayment(payment: IPolicyFeeResponse): IPaymentQuoteCard {
    const semaforo = TRAFFIC_LIGHT_COLOR[payment.semaforo];
    const symbol = `${StringUtil.getMoneyDescription(payment.codigoMoneda)} `;
    const expiredPayment = isExpired(payment.fechaExpiracion);
    const coverageStatus = `${payment.estadoCobertura}`;
    const showQuota = coverageStatus === COVERAGE_STATUS.CON_COBERTURA;
    const isNotice = payment.tipoDocumentoPago === POLICY_QUOTA_TYPE.AVISO;
    const noticeNumber = payment.numeroAviso;

    return {
      billNumber: payment.numRecibo,
      bulletColor: COLOR_STATUS[semaforo],
      coverage: payment.tieneCobertura,
      coverageStatus,
      currencySymbol: symbol,
      deadline: payment.expirationDate,
      debitRenovation: this.datePipe.transform(payment.fechaDebitoRenovacion, FORMAT_DATE_DDMMYYYY),
      expired: expiredPayment,
      expiredDate: payment.expirationDate,
      month: payment.fechaExpiracion.substr(0, 7),
      policy: payment.descripcionPoliza,
      policyNumber: payment.numeroPoliza,
      policyStatus: payment.estado,
      policyType: payment.tipoPoliza,
      quota: payment.cuota,
      semaforo,
      showQuota,
      total: payment.montoTotal,
      totalAmount: this.currencyPipe.transform(payment.montoTotal, symbol),
      isNotice,
      noticeNumber,
      associatedReceipts: payment.listaRecibosAsociados.map(this._mapAssociatedReceipts.bind(this)),
      response: { ...payment }
    } as IPaymentQuoteCard;
  }

  private _mapAssociatedReceipts(associatedReceipts: IAssociatedReceipt): IAssociatedReceipt {
    const symbol = `${StringUtil.getMoneyDescription(associatedReceipts.codigoMoneda)} `;
    const totalAmount = this.currencyPipe.transform(associatedReceipts.montoTotal, symbol);

    return { ...associatedReceipts, totalAmount };
  }
}
