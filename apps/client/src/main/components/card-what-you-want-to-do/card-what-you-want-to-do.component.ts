import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GAService, GaUnsubscribeBase, IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { ObjectUtil } from '@mx/core/shared/helpers/util/object';
import { MfModal } from '@mx/core/ui';
import { environment } from '@mx/environments/environment';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { PolicyListService } from '@mx/services/policy/policy-list.service';
import { MFP_Que_quieres_hacer_Mas_Servicios_6F } from '@mx/settings/constants/events.analytics';
import {
  APPLICATION_CODE,
  BUTTONS_LINK,
  CLIENT_ACTIONS,
  COMPARE_KEY,
  EDIT_DATA_KEY,
  HOME,
  MY_PAYMENTS,
  QUOTE
} from '@mx/settings/constants/general-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { LANDING_TRAVEL } from '@mx/settings/lang/landing';
import { ModalEmergencyPhones } from '@mx/settings/lang/travel.lang';
import {
  IObtenerConfiguracionActionResponse,
  IObtenerConfiguracionResponse
} from '@mx/statemanagement/models/client.models';
import { IButtonsLink } from '@mx/statemanagement/models/general.models';
import {
  IPoliciesAndProductsByClientRequest,
  IPoliciesAndProductsByClientResponse
} from '@mx/statemanagement/models/policy.interface';
import { takeUntil } from 'rxjs/operators';
import { ComparePolicyComponent } from '../shared/compare-policy/compare-policy.component';

@Component({
  selector: 'client-card-what-you-want-to-do',
  templateUrl: './card-what-you-want-to-do.component.html'
})
export class CardWhatYouWantToDoComponent extends GaUnsubscribeBase implements OnInit, OnDestroy {
  @ViewChild(ComparePolicyComponent) mfModalSelect: ComparePolicyComponent;
  @ViewChild(MfModal) mfModal: MfModal;

  @Input() policyType = HOME;
  @Input() isEpsContractor: boolean;
  @Input() isPPFM: boolean;

  landingTravel = LANDING_TRAVEL;

  titleWhatDoYouWantToDo = GeneralLang.Titles.WhatDoYouWantToDo;
  labelAllMyServices = GeneralLang.Buttons.AllMyServices;
  requestAssistanceTitle = GeneralLang.Titles.RequestAssistance;
  policyTypes = POLICY_TYPES;
  disableCollapse = true;

  buttonsLink: Array<IButtonsLink>;
  policiesByClient: Array<IPoliciesAndProductsByClientResponse>;
  actionItems: Array<IObtenerConfiguracionActionResponse>;
  showAllService = false;
  hideIcons = false;
  openModalSelect: boolean;
  openModalEmergencyPhones: boolean;
  hasPoliciesValid: boolean;
  mpKeys: any;

  ga: Array<IGaPropertie>;

  constructor(
    private readonly configurationService: ConfigurationService,
    private readonly policiesService: PoliciesService,
    private readonly policiesInfoService: PoliciesInfoService,
    private readonly policyListService: PolicyListService,
    private readonly activatedRoute: ActivatedRoute,
    protected gaService: GAService
  ) {
    super(gaService);
    this.policiesByClient = [];
    this.ga = [MFP_Que_quieres_hacer_Mas_Servicios_6F()];
  }

  ngOnInit(): void {
    this.policyListService
      .getValidPolicies()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(res => (this.hasPoliciesValid = res));

    this.configurationService
      .getHideServicesIcons()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(hide => (this.hideIcons = hide));

    if (this.policyType) {
      this.setConfiguration();
    }

    // Filtrar según el Tipo de Polizas que tiene el Cliente
    const params = this.fnParamPoliciesAndProductsByClient();
    this.policiesService
      .policiesAndProductsByClient(params)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((response: Array<IPoliciesAndProductsByClientResponse>) => {
        if (!ObjectUtil.isEquals(this.policiesByClient, response)) {
          this.policiesByClient = response;
          this.setConfiguration();
          this.policiesService.setPolicyTypes(this.policiesByClient);
        }
      });

    // Iconos a mostrar - Pension no tiene Servicios
    if (!this.hideIcons) {
      this.configurationService
        .getConfiguration()
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(
          (response: IObtenerConfiguracionResponse) => {
            if (response && response.acciones) {
              // filtrar visibles
              this.actionItems = response.acciones.filter(action =>
                CLIENT_ACTIONS.find(item => item.code === action.llave && item.visible)
              );
            }
          },
          () => {},
          () => {
            this.setConfiguration();
          }
        );
    }
  }

  ngOnDestroy(): void {
    this.destroySubscriptions();
    this.policiesInfoService.resetClientPolicyResponse();
  }

  fnParamPoliciesAndProductsByClient(): IPoliciesAndProductsByClientRequest {
    const params: IPoliciesAndProductsByClientRequest = {
      codigoApp: APPLICATION_CODE
    };

    return params;
  }

  // tslint:disable-next-line: cyclomatic-complexity
  setConfiguration(): void {
    this.mpKeys = this.policiesInfoService.getMPKeys() || {};
    const buttonsLink = BUTTONS_LINK.filter(link => link.visible);
    const type = (this.policyType && this.policyType.split('|')[0]) || ''; // separar MD_SALUD de MD_EPS
    this.policyType = type;
    this.showAllService = this.policyType === HOME;
    if (this.policiesByClient && this.policiesByClient.length > 0) {
      let hideViewPayments = false;

      const isHealth = this.policyType === POLICY_TYPES.MD_SALUD.code || this.policyType === POLICY_TYPES.MD_EPS.code;

      if (isHealth) {
        hideViewPayments = this.hasOnlyEPS() || !this.mpKeys.MP_LINK_VER_MIS_PAGOS;
      }
      if (this.showAllService) {
        hideViewPayments = this.hasOnlyEPSorSOAP();
      }

      // Para SOAT y EPS
      if (hideViewPayments) {
        this.buttonsLink = buttonsLink.filter(
          item1 => item1.policyTypes.find(item2 => item2 === this.policyType) && item1.description !== 'Ver mis pagos'
        );
      } else {
        this.buttonsLink = buttonsLink.filter(item1 => item1.policyTypes.find(item2 => item2 === this.policyType));
      }
    } else {
      this.buttonsLink = buttonsLink.filter(item1 => item1.policyTypes.find(item2 => item2 === this.policyType));
    }

    if (this.policyType === POLICY_TYPES.MD_HOGAR.code && !this.hasPoliciesValid) {
      this.buttonsLink = this.buttonsLink.filter(button => button.key !== COMPARE_KEY);
    }

    // AUT-3820: cambios para EPS - contratantes y afiliados
    if (this.policyType === POLICY_TYPES.MD_EPS.code) {
      if (this.isEpsContractor) {
        this.buttonsLink = this.buttonsLink
          .filter(button => button.key !== EDIT_DATA_KEY)
          .filter(button => button.key !== QUOTE);
        if (!environment.ACTIVATE_EPS_VIEW_PAYMENTS) {
          this.buttonsLink = this.buttonsLink.filter(button => button.key !== MY_PAYMENTS);
          if (this.mpKeys && !this.mpKeys.MP_LINK_VER_MIS_PAGOS) {
            this.buttonsLink = this.buttonsLink.filter(button => button.key !== MY_PAYMENTS);
          }
        }
      } else {
        this.buttonsLink = this.buttonsLink.filter(button => button.key !== MY_PAYMENTS);
      }
    }

    if (Object.keys(this.mpKeys).length && !this.mpKeys.MP_LINK_VER_MIS_PAGOS) {
      this.buttonsLink = this.buttonsLink.filter(button => button.key !== MY_PAYMENTS);
    }
  }

  /**
   * VALIDACION PARA SALUD
   * 1. Si solo tiene EPS no mostras (Ver mis Pagos)
   */
  hasOnlyEPS(): boolean {
    const policiesByClient = this.policiesByClient.filter(
      item => item.tipoPoliza === POLICY_TYPES.MD_SALUD.code || item.tipoPoliza === POLICY_TYPES.MD_EPS.code
    );

    const hideViewPayments = policiesByClient.find(item => item.tipoPoliza === POLICY_TYPES.MD_EPS.code);

    return hideViewPayments && !policiesByClient.filter(item => item.tipoPoliza !== POLICY_TYPES.MD_EPS.code).length;
  }

  /**
   * VALIDACION PARA EL HOME
   * 1. Si solo tiene EPS no mostras (Ver mis Pagos)
   * 2. SI solo tiene SOAP no mostras (Ver mis Pagos)
   * 3. Si solo tiene SOAP y EPS no mostras (Ver mis Pagos)
   * NOTA: SOAP referencia al FISICO y ELECTRONICO
   */
  hasOnlyEPSorSOAP(): boolean {
    const hasSE = this.policiesByClient.filter(
      item =>
        item.tipoPoliza === POLICY_TYPES.MD_SOAT.code ||
        item.tipoPoliza === POLICY_TYPES.MD_SOAT_ELECTRO.code ||
        item.tipoPoliza === POLICY_TYPES.MD_EPS.code
    );

    return hasSE.length && this.policiesByClient.length === hasSE.length;
  }

  // Pasar tipo de póliza según el link
  setLink(button: IButtonsLink): string {
    const policyNumber = this.activatedRoute.snapshot.params['policyNumber'];

    if (!button.routeByType) {
      return button.paramsPolicyType && this.policyType !== HOME
        ? policyNumber
          ? `${button.route}/${this.policyType}/${policyNumber}`
          : `${button.route}/${this.policyType}`
        : button.route;
    }

    return button.routeByType[button.policyTypes.indexOf(this.policyType)];
  }

  // Abre Modal
  openModal(): void {
    if (BUTTONS_LINK.find(link => link.key === 'compare' && link.visible)) {
      this.openModalSelect = true;
      setTimeout(() => {
        this.mfModalSelect.open();
      }, 100);
    }
  }

  doExtraAction(action: string): void {
    if (action === ModalEmergencyPhones) {
      this.openModalEmergencyPhones = true;
      setTimeout(() => {
        this.mfModal.open();
      }, 100);
    }
  }
}
