import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { MfButtonLinkModule, MfCardModule } from '@mx/core/ui';
import { MfModalModule } from '@mx/core/ui/lib/components/modals/modal';
import { ComparePolicyModule } from '../shared/compare-policy/compare-policy.module';
import { TravelEmergencyPhonesModule } from '../shared/travel-emergency-phones/travel-emergency-phones.module';
import { ActionItemComponent } from './action-item/action-item.component';
import { CardWhatYouWantToDoComponent } from './card-what-you-want-to-do.component';

@NgModule({
  imports: [
    CommonModule,
    MfCardModule,
    ComparePolicyModule,
    MfButtonLinkModule,
    TravelEmergencyPhonesModule,
    MfModalModule,
    GoogleModule
  ],
  declarations: [CardWhatYouWantToDoComponent, ActionItemComponent],
  exports: [CardWhatYouWantToDoComponent]
})
export class CardWhatYouWantToDoModule {}
