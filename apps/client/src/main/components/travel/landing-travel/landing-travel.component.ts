import { Component } from '@angular/core';
import { LANDING_TRAVEL } from '@mx/settings/lang/landing';

@Component({
  selector: 'client-landing-travel',
  templateUrl: './landing-travel.component.html'
})
export class LandingTravelComponent {
  landingContent = LANDING_TRAVEL;
}
