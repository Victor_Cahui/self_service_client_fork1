import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LandingModule } from '@mx/components/shared/landing/landing.module';
import { LandingTravelComponent } from './landing-travel.component';

@NgModule({
  imports: [CommonModule, LandingModule],
  exports: [LandingTravelComponent],
  declarations: [LandingTravelComponent],
  providers: []
})
export class LandingTravelModule {}
