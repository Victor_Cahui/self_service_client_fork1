import { CurrencyPipe, DatePipe } from '@angular/common';
import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import {
  COVERAGE_STATUS,
  POLICY_STATUS,
  SITUATION_TYPE,
  TRAFFIC_LIGHT_COLOR
} from '@mx/components/shared/utils/policy';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { MfModal } from '@mx/core/ui';
import { AuthService } from '@mx/services/auth/auth.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { APPLICATION_CODE, COLOR_STATUS, FORMAT_DATE_DDMMYYYY } from '@mx/settings/constants/general-values';
import { NO_PENDING_PAYMENTS_ICON, WARNING_2_ICON } from '@mx/settings/constants/images-values';
import { HealthLang } from '@mx/settings/lang/health.lang';
import { PaymentLang } from '@mx/settings/lang/payment.lang';
import { IEpsPendingInvoicesResponse, IEpsPendingInvoicesView } from '@mx/statemanagement/models/health.interface';
import { IPoliciesByClientResponse, IPolicyNotification } from '@mx/statemanagement/models/policy.interface';

@Component({
  selector: 'client-card-pending-invoices',
  templateUrl: './card-pending-invoices.component.html',
  providers: [CurrencyPipe, DatePipe]
})
export class CardPendingInvoicesComponent implements OnInit {
  @ViewChild(MfModal) mfModal: MfModal;
  @Output() setPolicyNotification: EventEmitter<IPolicyNotification> = new EventEmitter<IPolicyNotification>();

  collapse = false;
  disableCollapse: boolean;
  healthLang = HealthLang;
  paymentLang = PaymentLang;
  showLoading: boolean;
  showModalPaymentPlaces: boolean;
  policy: IPoliciesByClientResponse;
  noPendingPaymentsIcon = NO_PENDING_PAYMENTS_ICON;
  situationType = SITUATION_TYPE;
  trafficLight = TRAFFIC_LIGHT_COLOR;
  pendingInvoicesList: Array<IEpsPendingInvoicesView>;

  constructor(
    private readonly policiesInfoService: PoliciesInfoService,
    private readonly _AuthService: AuthService,
    private readonly _Autoservicios: Autoservicios,
    private readonly _CurrencyPipe: CurrencyPipe,
    private readonly _DatePipe: DatePipe
  ) {}

  ngOnInit(): void {
    this.policy = this.policiesInfoService.getPolicy();
    this.getPendingInvoices();
  }

  getPendingInvoices(): void {
    this.showLoading = true;
    const pathParams = { nroContrato: this.policy.numeroPoliza };
    const queryParams = {
      codigoApp: APPLICATION_CODE
    };
    this._Autoservicios.ObtenerEpsContratanteDatosDocsPago(pathParams, queryParams).subscribe(
      response => {
        this.pendingInvoicesList = (response || []).map(this._mapPendingInvoicesList.bind(this));
        this.searchExpiredInvoices();
      },
      error => {
        console.error(error);
      },
      () => (this.showLoading = false)
    );
  }

  openModalPaymentPlaces(): void {
    this.showModalPaymentPlaces = true;
    setTimeout(() => {
      this.mfModal.open();
    });
  }

  searchExpiredInvoices(): void {
    const expiredInvoicesList = this.pendingInvoicesList.filter(
      invoice => invoice.payload.estado === POLICY_STATUS.EXPIRED
    );
    if (expiredInvoicesList.length) {
      const nro = expiredInvoicesList.length;
      const isSinglular = nro === 1;
      const replace = isSinglular ? '' : 's';
      const subtitle = PaymentLang.Messages.AlertExpiredInvoices.replace('{{n}}', `${nro}`).replace(/\(s\)/gi, replace);
      const policyNotification: IPolicyNotification = {
        subtitle,
        icon: { iconName: WARNING_2_ICON }
      };
      this.setPolicyNotification.next(policyNotification);
    }
  }

  private _mapPendingInvoicesList(invoice: IEpsPendingInvoicesResponse): IEpsPendingInvoicesView {
    const symbol = `${StringUtil.getMoneyDescription(invoice.codigoMoneda)} `;
    const totalAmountFormatted = this._CurrencyPipe.transform(invoice.montoTotal, symbol);
    const semaforo = TRAFFIC_LIGHT_COLOR[invoice.semaforo];
    const dateEmission = this._DatePipe.transform(invoice.fechaEmision, FORMAT_DATE_DDMMYYYY);
    const dateExpiration = this._DatePipe.transform(invoice.fechaExpiracion, FORMAT_DATE_DDMMYYYY);
    const situationLabel = `${
      semaforo === TRAFFIC_LIGHT_COLOR.AMARILLO
        ? PaymentLang.Texts.CurrentPayment
        : semaforo === TRAFFIC_LIGHT_COLOR.ROJO
        ? PaymentLang.Texts.ExpiredPayment
        : ''
    }`;
    const situationDescription = `${situationLabel}: ${dateExpiration}`;
    const status = invoice.estado === POLICY_STATUS.EXPIRED ? COVERAGE_STATUS.FACTURA_VENCIDA : null;

    return {
      invoiceNumber: invoice.nroFactura,
      preInvoiceNumber: invoice.nroPreFactura,
      planCode: invoice.codPlan,
      planDescription: invoice.descPlan,
      dateEmission,
      dateExpiration,
      currencyCode: invoice.codigoMoneda,
      totalAmount: invoice.montoTotal,
      totalAmountFormatted,
      trafficLightColor: semaforo,
      bulletColor: COLOR_STATUS[semaforo],
      status,
      situationType: invoice.tipoSituacion,
      situationDescription,
      iconUrl: invoice.urlIcono,
      payload: invoice
    };
  }
}
