import { Component, Input, OnInit } from '@angular/core';
import { SEARCH_ICON } from '@mx/settings/constants/images-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';

@Component({
  selector: 'client-item-not-found',
  templateUrl: './item-not-found.component.html'
})
export class ItemNotFoundComponent implements OnInit {
  @Input() imageTop = true;
  @Input() text: string;
  @Input() title = GeneralLang.Messages.Fail;
  @Input() show: boolean;
  @Input() iconMedium: boolean;
  @Input() icon = SEARCH_ICON;

  ngOnInit(): void {
    // Se crea esta Imagen, para tener precargada y no se muestre
    // el efecto tosco al abrir este componente
    const image = new Image();
    image.src = this.icon;
  }
}
