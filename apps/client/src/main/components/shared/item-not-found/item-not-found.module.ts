import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ItemNotFoundComponent } from './item-not-found.component';

@NgModule({
  imports: [CommonModule, RouterModule],
  exports: [ItemNotFoundComponent],
  declarations: [ItemNotFoundComponent]
})
export class ItemNotFoundModule {}
