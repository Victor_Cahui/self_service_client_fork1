import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { MfModalModule } from '@mx/core/ui/lib/components/modals/modal/modal.module';
import { CardMoreFaqModule } from '../../card-more-faq/card-more-faq.module';
import { ModalMoreFaqsComponent } from './modal-more-faqs.component';

@NgModule({
  imports: [CommonModule, MfModalModule, CardMoreFaqModule, GoogleModule],
  exports: [ModalMoreFaqsComponent],
  declarations: [ModalMoreFaqsComponent]
})
export class ModalMoreFaqsModule {}
