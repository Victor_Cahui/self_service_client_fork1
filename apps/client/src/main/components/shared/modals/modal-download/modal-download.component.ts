import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { MfModal } from '@mx/core/ui/lib/components/modals/modal/public-api';
import { DEFAULT_FILE_SIZE_UNIT } from '@mx/settings/constants/general-values';
import { DOWNLOAD_ICON } from '@mx/settings/constants/images-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { IFileDownloading, IFileList } from '@mx/statemanagement/models/general.models';
import { Subject } from 'rxjs';

@Component({
  selector: 'client-modal-download',
  templateUrl: './modal-download.component.html',
  styles: [
    `
      .g-full-modal--content > .wrap {
        overflow-x: hidden;
      }
    `
  ]
})
export class ModalDownLoadComponent {
  static updateView: Subject<IFileDownloading> = new Subject();

  @ViewChild(MfModal) mfModal: MfModal;
  @Input() fileList: Array<IFileList>;
  @Output() downLoadFile: EventEmitter<any> = new EventEmitter<any>();

  downloadIcon: string;
  unit = DEFAULT_FILE_SIZE_UNIT;
  lang = GeneralLang;

  constructor() {
    ModalDownLoadComponent.updateView.subscribe((res: IFileDownloading) => {
      const index = this.fileList.findIndex(file => file.identificadorDocumento === res.identificadorDocumento);
      if (index > -1) {
        this.fileList[index].loading = res.loading;
      }
    });
  }

  open(): void {
    this.downloadIcon = `g-font--19 g-pl-10 g-c--green1 ${DOWNLOAD_ICON} g-c--pointer`;
    this.mfModal.open();
  }

  downLoad(file: IFileList): void {
    if (!file.identificadorDocumento) {
      return;
    }
    this.downLoadFile.emit(file);
  }
}
