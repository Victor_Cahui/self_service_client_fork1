import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DirectivesModule } from '@mx/core/ui';
import { MfModalModule } from '@mx/core/ui/lib/components/modals/modal/modal.module';
import { ModalDownLoadComponent } from './modal-download.component';

@NgModule({
  imports: [CommonModule, MfModalModule, DirectivesModule],
  exports: [ModalDownLoadComponent],
  declarations: [ModalDownLoadComponent]
})
export class ModalDownLoadModule {}
