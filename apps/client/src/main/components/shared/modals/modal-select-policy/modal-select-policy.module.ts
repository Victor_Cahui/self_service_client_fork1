import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IconPoliciesModule } from '@mx/components/shared/icon-policies/icon-polices.module';
import { MfModalModule } from '@mx/core/ui/lib/components/modals/modal/modal.module';
import { ModalSelectPolicyComponent } from './modal-select-policy.component';

@NgModule({
  imports: [CommonModule, FormsModule, MfModalModule, IconPoliciesModule],
  exports: [ModalSelectPolicyComponent],
  declarations: [ModalSelectPolicyComponent]
})
export class ModalSelectPolicyModule {}
