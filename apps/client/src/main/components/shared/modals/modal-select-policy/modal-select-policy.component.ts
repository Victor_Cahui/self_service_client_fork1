import { Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild } from '@angular/core';
import { BaseCardMyPolicies } from '@mx/components/card-my-policies/base-card-my-policies';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { MfModal } from '@mx/core/ui/lib/components/modals/modal/public-api';
import { environment } from '@mx/environments/environment';
import { GeneralService } from '@mx/services/general/general.service';
import { PoliciesService } from '@mx/services/policies.service';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import { ICardPoliciesView } from '@mx/statemanagement/models/policy.interface';
import { PolicyUtil } from '../../utils/policy.util';

@Component({
  selector: 'client-modal-select-policy',
  templateUrl: './modal-select-policy.component.html'
})
export class ModalSelectPolicyComponent extends BaseCardMyPolicies implements OnInit, OnChanges {
  @ViewChild(MfModal) mfModal: MfModal;
  @Output() selected: EventEmitter<ICardPoliciesView>;
  @Input() policyList: Array<ICardPoliciesView>;
  @Input() btnConfirm: string = GeneralLang.Buttons.Confirm;
  @Input() title: string = PolicyLang.Titles.SelectPolicy;
  @Input() subTitle: string;
  @Input() positionSelected = 0;
  @Input() showInsureds = true;

  policyTypes = POLICY_TYPES;
  showLoading: boolean;
  selectedPolicy: string;
  temporalPolicy: string;
  isEps = PolicyUtil.isEps;
  policyUtil = PolicyUtil;
  activateSearchClinicKey = environment.ACTIVATE_SEARCH_CLINIC_CLD;
  codRamoClincDigital = 275;

  constructor(
    protected generalService: GeneralService,
    protected policiesService: PoliciesService,
    protected gaService: GAService
  ) {
    super(generalService, policiesService, gaService);
    this.selected = new EventEmitter<ICardPoliciesView>();
  }

  ngOnInit(): void {}

  ngOnChanges(): void {
    this.positionSelected = this.positionSelected > -1 ? this.positionSelected : 0;
    if (this.policyList) {
      this.selectedPolicy = this.policyList[this.positionSelected].policyNumber;
      this.temporalPolicy = this.selectedPolicy;
    }
  }

  open(): void {
    this.mfModal.open();
  }

  // Al seleccionar una póliza
  onSubmit(confirm): void {
    this.mfModal.close(true);
    if (confirm && this.selectedPolicy) {
      const policy = this.policyList.filter(item => item.policyNumber === this.selectedPolicy)[0];
      // ga
      if (this.ga) {
        const btn = this.ga.find(fv => fv.control === 'btnModalSelectPolicy') || {};
        this.addGAEvent(btn, {
          lbl: this.btnConfirm,
          action: this.title,
          radioSelected: policy ? policy.policy : ''
        });
      }
      // Guarda poliza seleccionada
      this.policySelected = policy.policyNumber;
      this.selected.next(policy);
      this.temporalPolicy = this.policySelected;
    } else {
      this.selectedPolicy = this.temporalPolicy;
    }
  }

  closeEvent(b: boolean): void {
    if (!b) {
      if (this.ga && this.ga.find(c => c.control === 'btnModalSelectPolicy')) {
        const ga = this.ga.find(c => c.control === 'btnModalSelectPolicy');
        const p = this.policyList.find(item => item.policyNumber === this.selectedPolicy);
        this.addGAEvent(ga, {
          lbl: 'Cancelar',
          action: this.title,
          radioSelected: p ? p.policy : ''
        });
      }
    }
  }
}
