import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormContactedByAgentComponent } from '@mx/components/shared/form-contacted-by-agent/form-contacted-by-agent.component';
import { MfModalErrorComponent } from '@mx/core/ui/lib/components/modals/modal-error/modal-error.component';
import { MfModal } from '@mx/core/ui/lib/components/modals/modal/public-api';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { CONTACTED_BY_AGENT } from '@mx/settings/lang/contact';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { IContactData } from '@mx/statemanagement/models/client.models';
import { IContactedByAgent } from '@mx/statemanagement/models/general.models';

@Component({
  selector: 'client-modal-contacted-by-agent',
  templateUrl: 'modal-contacted-by-agent.component.html'
})
export class ModalContactedByAgentComponent implements OnInit {
  @ViewChild(MfModal) mfModal: MfModal;
  @ViewChild(MfModalErrorComponent) modalError: MfModalErrorComponent;
  @ViewChild(FormContactedByAgentComponent) formContactedByAgentComponent: FormContactedByAgentComponent;

  @Input() subTitle: string;
  @Input() policyType: string;
  @Input() isModal: boolean;
  @Input() showTitle = true;
  @Input() referred = false;
  @Input() contactData: IContactData;

  modalTitle: string;
  messageAlertError: string;
  disableButtonSend: boolean;
  editFooter: boolean;
  showModal: boolean;
  showModalError: boolean;
  btnConfirm = GeneralLang.Buttons.Confirm;
  titleAlertError = GeneralLang.Alert.titleFail;
  form: IContactedByAgent = CONTACTED_BY_AGENT;
  sendSuccess: boolean;

  ngOnInit(): void {
    this.form.titleMobile = POLICY_TYPES[this.policyType].title;
    this.disableButtonSend = true;
  }

  open(): void {
    this.showModal = true;
    this.sendSuccess = false;
    this.modalTitle = this.showTitle ? this.form.title.toUpperCase() : '';
    setTimeout(() => {
      this.formContactedByAgentComponent.initForm();
      this.mfModal.open();
    });
  }

  doSuccess(): void {
    this.disableButtonSend = false;
    this.sendSuccess = true;
    this.editFooter = true;
    this.modalTitle = '';
  }

  showError(message: string): void {
    this.messageAlertError = message;
    this.mfModal.close();
    this.showModalError = true;
    setTimeout(() => {
      this.modalError.open();
    });
  }

  closeEvent(): void {
    this.formContactedByAgentComponent.formContact.reset();
    this.formContactedByAgentComponent.isSent = false;
    this.modalTitle = this.form.title.toUpperCase();
    this.editFooter = false;
  }

  onSend(event: boolean): void {
    this.disableButtonSend = true;
    this.editFooter = true;
    if (event) {
      this.formContactedByAgentComponent.sendMessage();
    }
  }

  onConfirm(): void {
    this.mfModal.close();
  }

  enableButton(valid: boolean): void {
    this.disableButtonSend = !valid;
  }
}
