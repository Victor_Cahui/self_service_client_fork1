import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MfButtonModule } from '@mx/core/ui/lib/components/forms/button';
import { MfModalErrorModule } from '@mx/core/ui/lib/components/modals/modal-error';
import { MfModalModule } from '@mx/core/ui/lib/components/modals/modal/modal.module';
import { FormContactedByAgentModule } from '../../form-contacted-by-agent/form-contacted-by-agent.module';
import { ModalContactedByAgentComponent } from './modal-contacted-by-agent.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MfModalModule,
    MfButtonModule,
    FormContactedByAgentModule,
    MfModalErrorModule,
    MfButtonModule
  ],
  exports: [ModalContactedByAgentComponent],
  declarations: [ModalContactedByAgentComponent]
})
export class ModalContactedByAgentModule {}
