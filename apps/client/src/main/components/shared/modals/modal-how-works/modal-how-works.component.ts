import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MfModal } from '@mx/core/ui/lib/components/modals/modal/public-api';
import { IHeader } from '@mx/services/general/header-helper.service';
import { IModalHowWorks } from '@mx/statemanagement/models/general.models';

@Component({
  selector: 'client-modal-how-works',
  templateUrl: './modal-how-works.component.html',
  styles: [
    `
      .g-full-modal--content > .wrap {
        overflow-x: hidden;
      }
    `
  ]
})
export class ModalHowWorksComponent implements OnInit {
  @Input() modalHeader: IHeader;
  @Input() modalData: IModalHowWorks;

  @ViewChild(MfModal) mfModal: MfModal;

  ngOnInit(): void {}

  open(): void {
    this.mfModal.open();
  }
}
