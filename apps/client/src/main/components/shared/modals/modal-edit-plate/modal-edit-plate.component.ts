import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MfModal } from '@mx/core/ui/lib/components/modals/modal/public-api';
import { PoliciesQuoteService } from '@mx/services/policy/policy-quote.service';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import { VehiclesLang } from '@mx/settings/lang/vehicles.lang';
import { FormEditPlateQuoteComponent } from '../../form-edit-plate-quote/form-edit-plate-quote.component';

@Component({
  selector: 'client-modal-edit-plate',
  templateUrl: 'modal-edit-plate.component.html'
})
export class ModalEditPlateComponent implements OnInit {
  @ViewChild(MfModal) mfModal: MfModal;
  @ViewChild(FormEditPlateQuoteComponent) formEditPlateQuoteComponent: FormEditPlateQuoteComponent;

  @Input() isModal: boolean;
  @Output() updateView: EventEmitter<any> = new EventEmitter<any>();
  @Output() showMessage: EventEmitter<any> = new EventEmitter<any>();

  modalTitle = VehiclesLang.Titles.EditPlate;
  modalTitleMobile = PolicyLang.Titles.QuotePolicy;
  disableButtonSend: boolean;
  btnSave = GeneralLang.Buttons.Save;
  showLoading: boolean;

  constructor(private readonly quoteService: PoliciesQuoteService) {}

  ngOnInit(): void {}

  open(): void {
    this.formEditPlateQuoteComponent.initForm();
    this.mfModal.open();
  }

  close(): void {
    this.mfModal.close();
  }

  // Cierra Modal y actualiza la vista
  doSuccess(): void {
    this.updateView.emit(true);
    this.mfModal.close();
  }

  // Cerrar modal
  closeEvent(): void {
    this.updateView.emit(false);
  }

  // Actualizar Placa
  onSave(event: boolean): void {
    if (event) {
      this.formEditPlateQuoteComponent.searchPlate();
    }
  }

  // Habilitar botón Guardar
  enableButton(valid: boolean): void {
    setTimeout(() => (this.disableButtonSend = !valid));
  }

  // Mostrar mensaje poliza vigente
  showMessagePolicy(data): void {
    if (data && data.show) {
      this.showMessage.emit(data);
    } else {
      this.close();
    }
  }
}
