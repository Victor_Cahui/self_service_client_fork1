import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BaseLogin } from '@mx/components/login/base-login';
import { NotificationService } from '@mx/core/shared/helpers/notification/service/notification.service';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { Autoservicios, Comun } from '@mx/core/shared/providers/services';
import { AppFacade } from '@mx/core/shared/state/app';
import { MfModal } from '@mx/core/ui';
import { NotificationStatus } from '@mx/core/ui/lib/components/alerts/custom-alert/custom-alert.component';
import { AuthService } from '@mx/services/auth/auth.service';
import { UserAdminService } from '@mx/services/auth/user-admin.service';
import { ClientService } from '@mx/services/client.service';
import { GeneralService } from '@mx/services/general/general.service';
import { PaymentService } from '@mx/services/payment.service';
import { PoliciesService } from '@mx/services/policies.service';
import { ATTEMPTS_VERIFY_TOKEN } from '@mx/settings/constants/general-values';
import { TIME_ALERT_ICON } from '@mx/settings/constants/images-values';
import { LOCAL_STORAGE_KEYS } from '@mx/settings/constants/key-values';
import { AuthLang } from '@mx/settings/lang/auth.lang';
import { AuthInterface, ILogguedUser } from '@mx/statemanagement/models/auth.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-modal-expired-session',
  templateUrl: './modal-expired-session.component.html'
})
export class ModalExpiredSessionComponent extends BaseLogin implements OnInit, OnDestroy {
  @ViewChild(MfModal) modal: MfModal;

  @Output() valid: EventEmitter<any> = new EventEmitter<any>();

  lang = AuthLang;
  icon = TIME_ALERT_ICON;

  password: AbstractControl;
  loginSub: Subscription;
  attempts = 0;
  status = NotificationStatus.WARNING;

  constructor(
    protected router: Router,
    protected policiesService: PoliciesService,
    protected clientService: ClientService,
    protected generalService: GeneralService,
    protected authService: AuthService,
    protected paymentService: PaymentService,
    protected fb: FormBuilder,
    protected localStorage: LocalStorageService,
    protected adminService: UserAdminService,
    protected _Comun: Comun,
    protected _Autoservicios: Autoservicios,
    protected notifyService: NotificationService,
    private readonly _AppFacade: AppFacade
  ) {
    super(
      router,
      policiesService,
      clientService,
      generalService,
      authService,
      paymentService,
      fb,
      localStorage,
      adminService,
      _Comun,
      _Autoservicios,
      notifyService
    );
  }

  ngOnInit(): void {
    this.modal.open();
    this.messageUserInvalid = AuthLang.Messages.PasswordInvalid;
    this.createForm();
  }

  ngOnDestroy(): void {
    if (this.loginSub) {
      this.loginSub.unsubscribe();
    }
  }

  createForm(): void {
    this.formLogin = this.fb.group({
      password: ['', Validators.required]
    });
    this.password = this.formLogin.controls['password'];
  }

  action(confirm: boolean): void {
    if (!confirm) {
      this.anotherProfile();
    } else {
      this.login();
    }
  }

  login(): void {
    const user: ILogguedUser = this.localStorage.getData(LOCAL_STORAGE_KEYS.USER);
    this.clientID = {
      tipoDocumento: user ? user.documentType : null,
      documento: user ? user.documentNumber : null,
      groupTypeId: user ? user.groupTypeId : null
    };
    const data: AuthInterface = {
      client_id: '',
      grant_type: '',
      username: this.clientID.documento,
      password: this.password.value,
      refresh_token: this.authService.getUserRefreshToken()
    };

    this.userValidate(data, this._validLogin.bind(this), this._invalidLogin.bind(this));
  }

  anotherProfile(): void {
    this.localStorage.clear();
    this.router.navigate(['/login']);
  }

  recurrentProfile(): void {
    document.getElementsByTagName('body')[0].style.display = 'none';
    setTimeout(() => {
      location.reload(true);
    }, 1000);
  }

  private _validLogin(): void {
    this._AppFacade.LoadLookups();
    setTimeout(() => {
      this.valid.next();
      document.getElementsByTagName('body')[0].style.display = 'none';
      // tslint:disable-next-line: deprecation
      location.reload(true);
    }, 0);
  }

  private _invalidLogin(): void {
    this.attempts++;
    if (this.attempts === ATTEMPTS_VERIFY_TOKEN) {
      this.recurrentProfile();
    }
  }
}
