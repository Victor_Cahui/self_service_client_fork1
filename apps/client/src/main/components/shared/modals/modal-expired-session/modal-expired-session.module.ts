import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MfButtonModule, MfInputModule, MfModalModule, MfShowErrorsModule } from '@mx/core/ui';
import { MfCustomAlertModule } from '@mx/core/ui/lib/components/alerts/custom-alert';
import { ModalExpiredSessionComponent } from './modal-expired-session.component';

@NgModule({
  imports: [
    CommonModule,
    MfModalModule,
    MfInputModule,
    MfButtonModule,
    MfShowErrorsModule,
    ReactiveFormsModule,
    MfCustomAlertModule
  ],
  declarations: [ModalExpiredSessionComponent],
  exports: [ModalExpiredSessionComponent]
})
export class ModalExpiredSessionModule {}
