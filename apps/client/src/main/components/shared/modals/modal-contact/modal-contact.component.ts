import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MenuComponent } from '@mx/components/shared/utils/menu';
import { MfModal } from '@mx/core/ui/lib/components/modals/modal/public-api';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { MenuService } from '@mx/services/menu/menu.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { MODAL_CONTACT } from '@mx/settings/lang/contact';
import { IContactsDataRequest, IContactsDataResponse } from '@mx/statemanagement/models/profile.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-modal-contact',
  templateUrl: 'modal-contact.component.html'
})
export class ModalContactComponent implements OnInit {
  @ViewChild(MfModal) mfModal: MfModal;

  contactList: Array<IContactsDataResponse>;
  contactTitle = MODAL_CONTACT.title;
  contactSubTitle = MODAL_CONTACT.subTitle;
  contactOfficesUrl = MODAL_CONTACT.urlOffices;
  contactDescription = MODAL_CONTACT.description;
  contactOfficiesUs = MODAL_CONTACT.officesUs;
  contactModalBool: boolean;
  showModal: boolean;
  contactSub: Subscription;

  constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly profileService: ClientService,
    private readonly menuService: MenuService
  ) {}

  ngOnInit(): void {
    this.menuService.getModalContact().subscribe(res => {
      this.contactModalBool = res;
      if (this.contactModalBool) {
        MenuComponent.closeNav();
        this.openModalContact();
      }
    });
  }

  openModalContact(): void {
    this.showModal = true;
    this.getContactData();
    setTimeout(() => {
      this.mfModal.open();
    });
  }

  gOfficesUs(): void {
    this.mfModal.close();
    this.router.navigate([this.contactOfficesUrl]);
  }

  getContactData(): void {
    this.contactSub = this.profileService
      .getContactsData({
        codigoApp: APPLICATION_CODE
      } as IContactsDataRequest)
      .subscribe((res: Array<IContactsDataResponse>) => {
        this.contactList = res || [];
        this.contactSub.unsubscribe();
      });
  }
}
