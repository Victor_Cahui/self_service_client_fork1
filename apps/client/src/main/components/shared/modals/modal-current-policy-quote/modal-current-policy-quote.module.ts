import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfModalMessageModule } from '@mx/core/ui';
import { ModalCurrentPolicyQuoteComponent } from './modal-current-policy-quote.component';

@NgModule({
  imports: [CommonModule, MfModalMessageModule],
  exports: [ModalCurrentPolicyQuoteComponent],
  declarations: [ModalCurrentPolicyQuoteComponent]
})
export class ModalCurrentPolicyQuoteModule {}
