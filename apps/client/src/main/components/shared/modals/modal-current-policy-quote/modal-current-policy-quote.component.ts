import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { dateToStringFormatMMDDYYYY } from '@mx/core/shared/helpers/util/date';
import { MfModalMessage } from '@mx/core/ui/lib/components/modals/modal-message';
import { VIGENCY_ICON } from '@mx/settings/constants/images-values';
import { CURRENT_OWN_POLICY, CURRENT_POLICY } from '@mx/settings/constants/policy-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { PolicyLang } from '@mx/settings/lang/policy.lang';

@Component({
  selector: 'client-modal-current-policy-quote',
  templateUrl: 'modal-current-policy-quote.component.html'
})
export class ModalCurrentPolicyQuoteComponent implements OnInit {
  @ViewChild(MfModalMessage) modalMessage: MfModalMessage;

  @Input() vigencyDate: string;
  @Input() flagPolicyStatus: string;
  @Input() isModal: boolean;

  vigencyIcon = VIGENCY_ICON;
  messageTitle: string;
  messageText: string;
  formatedDate: string;

  constructor(private readonly router: Router) {}

  ngOnInit(): void {}

  open(): void {
    this.buildMessage();
    this.messageText = this.messageText.replace('{{fecha}}', this.formatedDate);
    setTimeout(() => {
      this.modalMessage.open();
    });
  }

  buildMessage(): void {
    this.formatedDate = this.vigencyDate ? dateToStringFormatMMDDYYYY(new Date(this.vigencyDate)) : '';
    this.messageTitle = GeneralLang.Messages.EstimatedUser;
    switch (this.flagPolicyStatus) {
      case CURRENT_OWN_POLICY:
        this.messageText = PolicyLang.Messages.CurrentOwnPolicy;
        break;
      case CURRENT_POLICY:
        this.messageText = PolicyLang.Messages.CurrentPolicy;
        break;
      default:
        this.messageText = GeneralLang.Messages.FailUpdate;
        break;
    }
  }

  onConfirm(event): void {
    if (this.isModal) {
      this.router.navigate(['/vehicles/quote']);
    }
  }
}
