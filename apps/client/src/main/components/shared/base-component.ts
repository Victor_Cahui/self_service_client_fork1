import { OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
export abstract class BaseComponent implements OnDestroy, OnInit {
  private _destroy$: Subject<any>;
  arrToDestroy: any[] = [];

  get destroy$(): Subject<any> {
    if (!this._destroy$) {
      // Perf optimization:
      // since this is likely used as base component everywhere
      // only construct a Subject instance if actually used
      this._destroy$ = new Subject();
    }

    return this._destroy$;
  }

  public ngOnInit(): void {
    //
  }

  public ngOnDestroy(): void {
    if (this._destroy$) {
      this._destroy$.next(true);
      this._destroy$.complete();
    }
    Array.isArray(this.arrToDestroy) &&
      this.arrToDestroy.forEach(o => {
        o.unsubscribe && o.unsubscribe();
      });
  }
}
