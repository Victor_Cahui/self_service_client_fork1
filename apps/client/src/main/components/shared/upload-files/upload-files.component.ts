import { DOCUMENT } from '@angular/common';
import { Component, EventEmitter, Inject, Input, OnChanges, OnInit, Output, ViewChild } from '@angular/core';
import { PLATFORMS, PlatformService } from '@mx/core/shared/helpers/util/platform.service';
import { NotificationStatus } from '@mx/core/ui/lib/components/alerts/custom-alert/custom-alert.component';
import { MfModalMessageComponent } from '@mx/core/ui/lib/components/modals/modal-message/modal-message.component';
import { ERROR_UPLOAD_FILE } from '@mx/settings/constants/errors-values';
import { Y_AXIS } from '@mx/settings/constants/general-values';
import { CIRCLE_CLOSE_ICON, PAPER_ICON, UPLOAD_ICON } from '@mx/settings/constants/images-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { UPLOAD_FILE_CONFIG_DEFAULT } from './upload-file.config';

@Component({
  selector: 'client-upload-files',
  templateUrl: './upload-files.component.html'
})
export class UploadFilesComponent implements OnInit, OnChanges {
  @ViewChild(MfModalMessageComponent) modalMessage: MfModalMessageComponent;

  @Output() addFile: EventEmitter<IUploadFileList> = new EventEmitter<IUploadFileList>();
  @Output() removeFile: EventEmitter<number> = new EventEmitter<number>();
  @Output() clickSelector: EventEmitter<null> = new EventEmitter<null>();

  @Input() validFiles: Array<string>;
  @Input() inValidFileNames: Array<string> = [];
  /** Valor en megabytes, default 20 MB */
  @Input() maxSize: number;
  /** True para activar data en 3 líneas */
  @Input() withDescription: false;
  /** Máximo de archivos */
  @Input() maxFiles: number;
  @Input() uploadText: string;
  @Input() listFile: Array<IUploadFileList> = [];
  @Input() buttonPosition: Y_AXIS = Y_AXIS.BOTTOM;
  @Input() externalSelectFile: boolean;
  @Input() useWarmingError: boolean;
  @Input() defaultShowLoader = true;
  @Input() readonly: boolean;
  @Input() disabledOutside: boolean;

  generalLang = GeneralLang.Buttons;
  closeIcon = CIRCLE_CLOSE_ICON;
  paperIcon = PAPER_ICON;
  uploadIcon = UPLOAD_ICON;
  Y_AXIS = Y_AXIS;

  showError: boolean;
  disabled: boolean;
  titleAlert: string;
  messageAlert: string;
  indexToSplit: number;
  status = NotificationStatus.INFO;

  constructor(
    private readonly platformService: PlatformService,
    @Inject(DOCUMENT) private readonly document: Document
  ) {}

  ngOnInit(): void {
    this.validFiles = this.validFiles || UPLOAD_FILE_CONFIG_DEFAULT.validFiles;
    this.maxSize = this.maxSize || UPLOAD_FILE_CONFIG_DEFAULT.maxSize;
    this.validDisabled();
  }

  ngOnChanges(): void {
    this.validDisabled();
  }

  open(): void {
    if (!this.disabled && !this.disabledOutside) {
      this.clickSelector.next();
      if (!this.externalSelectFile) {
        const input = this.document.getElementById('file') as HTMLInputElement;
        // INFO: Reset
        input.type = '';
        input.value = null;
        // end reset
        input.type = 'file';
        input.multiple = false;
        if (!this.platformService.is(PLATFORMS.IOSMOBILE)) {
          input.accept = this.validFiles.join(',');
        }

        const onchange = () => {
          const files = Array.from(input.files);
          const valid = this.validateTypeFiles(files);
          if (valid) {
            this.showError = false;
            const tmpKey = new Date().getTime();
            this.listFile.push({ file: files[0], load: this.defaultShowLoader, tmpKey });
            this.addFile.next({ file: files[0], tmpKey });
          } else {
            this.showError = true;
            if (!this.useWarmingError) {
              this.modalMessage.open();
            }
          }

          this.validDisabled();
        };

        input.onchange = onchange;
        input.click();
      }
    }
  }

  remove(tmpKey: number, index: number): void {
    this.indexToSplit = index;
    this.listFile[this.indexToSplit].load = this.defaultShowLoader;
    this.removeFile.next(tmpKey);
    this.validDisabled();
  }

  completeRemove(): void {
    this.listFile.splice(this.indexToSplit, 1);
    delete this.indexToSplit;
  }

  validDisabled(): void {
    this.disabled = this.maxFiles !== undefined && this.listFile.length >= this.maxFiles;
  }

  private validateTypeFiles(files: Array<File> = []): boolean {
    const differentFormat = !!files.find(file => !this.validFiles.find(type => type === file.type));
    const errorSize = !!files.find(file => file.size / (1024 * 1024) > this.maxSize);

    const invalidNames = this.listFile.map(item => item.file.name).concat(this.inValidFileNames);

    const existFile = !!files.find(file => !!invalidNames.find(name => name === file.name));

    if (differentFormat) {
      this.titleAlert = ERROR_UPLOAD_FILE.Format.title;
      this.messageAlert = ERROR_UPLOAD_FILE.Format.message;
    } else if (errorSize) {
      this.titleAlert = ERROR_UPLOAD_FILE.Size.title;
      this.messageAlert = `${ERROR_UPLOAD_FILE.Size.message} (${this.maxSize}${UPLOAD_FILE_CONFIG_DEFAULT.textSize}).`;
    } else if (existFile) {
      this.titleAlert = ERROR_UPLOAD_FILE.Repeat.title;
      this.messageAlert = ERROR_UPLOAD_FILE.Repeat.message;
    }

    return !(differentFormat || errorSize || existFile);
  }
}

export interface IUploadFileList {
  file: File;
  load?: boolean;
  tmpKey: number;
  text1?: string;
  text2?: string;
}
