import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FileSizePipe } from '@mx/core/shared/common/pipes/file-size.pipe';
import { MfModalMessageModule } from '@mx/core/ui';
import { MfCustomAlertModule } from '@mx/core/ui/lib/components/alerts/custom-alert';
import { UploadFilesComponent } from './upload-files.component';

@NgModule({
  imports: [CommonModule, MfModalMessageModule, MfCustomAlertModule],
  declarations: [UploadFilesComponent, FileSizePipe],
  exports: [UploadFilesComponent]
})
export class UploadFilesModule {}
