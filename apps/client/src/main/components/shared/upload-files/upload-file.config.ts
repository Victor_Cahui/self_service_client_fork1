export const UPLOAD_FILE_CONFIG_DEFAULT = {
  validFiles: ['image/jpeg', 'image/png', 'application/pdf'],
  maxSize: 20,
  textSize: 'MB'
};
