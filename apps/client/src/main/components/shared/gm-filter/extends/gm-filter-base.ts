import { EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { SelectListItem } from '@mx/core/ui';
import { UbigeoService } from '@mx/services/ubigeo.service';
import { IItemMarker } from '@mx/statemanagement/models/google-map.interface';
import { Subscription } from 'rxjs';
import { GmFilterService } from '../providers/gm-filter.service';

export abstract class GmFilterBase extends GaUnsubscribeBase {
  public formFilter: FormGroup;
  public formFilterTmp: FormGroup;
  public applyFilter: EventEmitter<any>;
  public applyReset: EventEmitter<any>;
  public filterButtonState: EventEmitter<boolean>;
  public items: Array<IItemMarker>;
  public _appliedFrm: any = {};
  public _appliedOptionsUbigeo: any = {};
  public departamentos: Array<SelectListItem> = [];
  public provincias: Array<SelectListItem> = [];
  public distritos: Array<SelectListItem> = [];
  public _getOpenedFilterRx$: Subscription;

  constructor(
    public formBuilder: FormBuilder,
    public gmFilterService: GmFilterService,
    public readonly ubigeoService: UbigeoService,
    protected gaService: GAService
  ) {
    super(gaService);
    this.applyFilter = new EventEmitter<any>();
    this.applyReset = new EventEmitter<any>();
    this.filterButtonState = new EventEmitter<boolean>();
  }

  reset(): void {
    this.applyReset.next();
  }

  abstract resetState(): void;

  restoreDepartment(prop: string = 'department'): void {
    this.formFilter.get(prop).setValue(this._appliedFrm[prop]);
  }

  restoreProvince(propDepartment: string = 'department', propProvince: string = 'province'): void {
    this.formFilter.get(propProvince).setValue(this._appliedFrm[propProvince]);
    this._appliedOptionsUbigeo.province && (this.provincias = [...this._appliedOptionsUbigeo.province]);
    if (this._appliedFrm[propDepartment] && !this.provincias.length) {
      this.getArrayProvinces(this._appliedFrm[propDepartment]);
    }
  }

  restoreDistrict(propProvince: string = 'province', propDistrict: string = 'district'): void {
    this.formFilter.get(propDistrict).setValue(this._appliedFrm[propDistrict]);
    this._appliedOptionsUbigeo.district && (this.distritos = [...this._appliedOptionsUbigeo.district]);
    if (this._appliedFrm[propProvince] && !this.distritos.length) {
      this.getArrayDistrict(this._appliedFrm[propProvince]);
    }
  }

  setUbigeoData(ubigeos: Array<any>): Array<SelectListItem> {
    const arr = ubigeos.map(
      (obj: any) =>
        new SelectListItem({
          text: obj.descripcion,
          value: obj.ubigeoId
        })
    );

    return [...arr];
  }

  getArrayProvinces(departamentoId): void {
    this.ubigeoService.getProvinces(departamentoId).subscribe(r => {
      this.provincias = this.setUbigeoData(r);
    });
  }

  getArrayDistrict(provinciaId): void {
    this.ubigeoService.getDistricts(provinciaId).subscribe(r => {
      this.distritos = this.setUbigeoData(r);
    });
  }
}
