import { DOCUMENT } from '@angular/common';
import { ElementRef, EventEmitter, Inject, Input, OnDestroy, Output, Renderer2 } from '@angular/core';
import { ResizeService } from '@mx/core/shared/helpers/events/resize.service';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { PlatformService } from '@mx/core/shared/helpers/util/platform.service';

export class GmHandlerShow extends GaUnsubscribeBase implements OnDestroy {
  private _show: boolean;
  @Input() lightbox: boolean;

  @Input()
  set show(show: boolean) {
    this._show = show;
    this.handlerShow(show);
  }

  get show(): boolean {
    return this._show;
  }

  @Output() exit: EventEmitter<any>;

  constructor(
    protected render2: Renderer2,
    protected elementRef: ElementRef,
    protected platformService: PlatformService,
    protected resizeService: ResizeService,
    protected gaService: GAService,
    @Inject(DOCUMENT) protected document: Document
  ) {
    super(gaService);
    this.exit = new EventEmitter<any>();
    setTimeout(() => {
      this.handlerSize();
    }, 10);
    setTimeout(() => {
      this.handlerShow(this.show);
    }, 10);
    this.resizeService.resizeEvent.subscribe(() => {
      this.handlerSize();
    });
  }

  ngOnDestroy(): void {
    this.handlerBodyClass('g-modal-map--open', false);
    this.mfOnDestroy();
  }

  handlerSize(): void {
    const isDesktop = this.platformService.isDesktop;
    this.render2[isDesktop ? 'addClass' : 'removeClass'](this.elementRef.nativeElement, 'g-map-filter--column');
    this.render2[!isDesktop ? 'addClass' : 'removeClass'](this.elementRef.nativeElement, 'g-map-filter--full');
  }

  handlerShow(show: boolean): void {
    this.render2[show ? 'removeClass' : 'addClass'](this.elementRef.nativeElement, 'd-none');
    this.render2[show ? 'addClass' : 'removeClass'](this.elementRef.nativeElement, 'd-block');
    this.handlerBodyClass('g-modal-map--open', !!show);
    if (this.lightbox) {
      this.handlerLightBoxMap(show);
    }
  }

  handlerLightBoxMap(show: boolean): void {
    try {
      const gmRef = this.document.getElementsByClassName('sebm-google-map-container-inner')[0];
      this.render2[show ? 'addClass' : 'removeClass'](gmRef, 'gm-lightbox');
    } catch (error) {}
  }

  close(): void {
    this.exit.next();
  }

  handlerBodyClass(className: string, add = true): void {
    const headerElements = this.document.getElementsByTagName('html');
    if (headerElements && headerElements.length) {
      this.render2[add ? 'addClass' : 'removeClass'](headerElements[0], className);
    }
  }

  mfOnDestroy(): void {}
}
