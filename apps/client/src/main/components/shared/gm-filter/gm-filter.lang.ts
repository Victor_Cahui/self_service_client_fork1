export const GmFilterLang = {
  Buttons: {
    Filter: 'filtrar',
    ClearFilter: 'restablecer',
    Apply: 'aplicar',
    Search: 'buscar',
    ViewOnMap: 'Ver en el mapa',
    ViewDetail: 'Ver detalle',
    goToAppointment: 'Agendar cita'
  },
  Text: {
    SearchPlaceholder: 'Nombre o dirección',
    ItemNotFound: 'No se encontraron resultados.'
  },
  Values: {
    maxLenghtSearch: 50,
    minInputSearch: 3
  }
};
