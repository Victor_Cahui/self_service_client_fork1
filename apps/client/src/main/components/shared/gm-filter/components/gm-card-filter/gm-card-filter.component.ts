import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { environment } from '@mx/environments/environment';
import { Y_AXIS } from '@mx/settings/constants/general-values';
import { IPoliciesByClientResponse } from '@mx/statemanagement/models/policy.interface';
import { Subscription } from 'rxjs';
import { GmFilterLang } from '../../gm-filter.lang';
import { GmFilterEvents, GmFilterService } from '../../providers/gm-filter.service';

@Component({
  selector: 'client-gm-card-filter',
  templateUrl: './gm-card-filter.component.html'
})
export class GmCardFilterComponent extends GaUnsubscribeBase implements OnInit, OnDestroy {
  buttonTexts = GmFilterLang.Buttons;
  SearchPlaceholder = GmFilterLang.Text.SearchPlaceholder;
  maxLengthSearch = GmFilterLang.Values.maxLenghtSearch;
  minInputSearch = GmFilterLang.Values.minInputSearch;

  searchText = '';
  onEventSub: Subscription;
  filterActived: boolean;
  _filterService$: any;
  _arrToDestroy = [];

  Y_AXIS = Y_AXIS;

  activateSearchClinicKey = environment.ACTIVATE_SEARCH_CLINIC_CLD;
  codRamoClinicDigital = 275;
  isProductRamoClinicDigital: boolean;

  @Input() centerButton: any;
  @Input() configView: any;
  @Input() type: Y_AXIS = Y_AXIS.TOP;
  @Input() policy: IPoliciesByClientResponse;
  @Output() centerButtonClick: EventEmitter<any>;
  @Output() openFilter: EventEmitter<any>;
  @Output() openSearch: EventEmitter<any>;
  @Output() search: EventEmitter<string>;

  constructor(private readonly filterService: GmFilterService, protected gaService: GAService) {
    super(gaService);
    this.openFilter = new EventEmitter<any>();
    this.openSearch = new EventEmitter<any>();
    this.search = new EventEmitter<string>();
    this.centerButtonClick = new EventEmitter<any>();
    this.filterActived = false;
  }

  ngOnInit(): void {
    this.onEventSub = this.filterService.onEvent().subscribe(res => {
      if (res) {
        switch (res.event) {
          case GmFilterEvents.CLEAR_SEARCH_INPUT:
            this.searchText = '';
            break;
          case GmFilterEvents.FILTERS_ACTIVED:
            this.filterActived = res.data;
            break;
          default:
            break;
        }
      }
    });
  }

  ngOnDestroy(): void {
    if (this.onEventSub) {
      this.onEventSub.unsubscribe();
    }
  }

  action(type?: string): void {
    if (type === 'filter') {
      this.openFilter.next();
    } else if (type === 'center') {
      this.centerButtonClick.next();
    } else {
      this.openSearch.next();
    }
  }

  fnSearch(value?: string): void {
    this.searchText = value || '';
    if (((value && value.length) || 0) >= this.minInputSearch) {
      this.search.next(value);
    } else if (!value) {
      this.search.next(value);
    }
  }
}
