import { DOCUMENT } from '@angular/common';
import { Component, ElementRef, EventEmitter, HostBinding, Inject, Input, Output, Renderer2 } from '@angular/core';
import { ResizeService } from '@mx/core/shared/helpers/events/resize.service';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { PlatformService } from '@mx/core/shared/helpers/util/platform.service';
import { IItemMarker } from '@mx/statemanagement/models/google-map.interface';
import { IPoliciesByClientResponse } from '@mx/statemanagement/models/policy.interface';
import { GmHandlerShow } from '../../extends/gm-handler-show';
import { GmFilterLang } from '../../gm-filter.lang';

@Component({
  selector: 'client-gm-list',
  templateUrl: './gm-list.component.html'
})
export class GmListComponent extends GmHandlerShow {
  @HostBinding('attr.class') attr_class = 'g-map-filter--full h-100 d-none g-bg-c--white1';

  @Input() configView: any;
  @Input() items: Array<IItemMarker>;
  @Input() showLoading: boolean;
  @Input() title: string;
  @Input() policy: IPoliciesByClientResponse;
  @Output() criteriaSearch: EventEmitter<Array<string>>;
  @Output() openFilter: EventEmitter<any>;
  @Output() goToAppointment: EventEmitter<IItemMarker> = new EventEmitter();
  @Output() viewDetail: EventEmitter<IItemMarker>;
  @Output() viewOnMap: EventEmitter<IItemMarker>;

  lang = GmFilterLang;
  textSearch: string;

  constructor(
    protected render2: Renderer2,
    protected elementRef: ElementRef,
    protected platformService: PlatformService,
    protected resizeService: ResizeService,
    protected gaService: GAService,
    @Inject(DOCUMENT) protected document: Document
  ) {
    super(render2, elementRef, platformService, resizeService, gaService, document);
    this.openFilter = new EventEmitter<any>();
    this.viewOnMap = new EventEmitter<IItemMarker>();
    this.viewDetail = new EventEmitter<IItemMarker>();
    this.criteriaSearch = new EventEmitter<Array<string>>();
  }

  openfilter(): void {
    this.openFilter.next();
  }

  viewonmap(item: IItemMarker): void {
    this.viewOnMap.next(item);
    if (this.platformService.isMobile) {
      this.close();
    }
  }

  detail(item: IItemMarker): void {
    this.viewDetail.next(item);
  }

  appointment(marker: IItemMarker): void {
    this.goToAppointment.next(marker);
  }

  search(text: string): void {
    this.textSearch = text;
    const words = (text && text.split(' ')) || [];
    this.criteriaSearch.next(words);
  }
}
