import { Type } from '@angular/core';

export class GmFilterViewItem {
  constructor(public component: Type<any>, public data?: any) {}
}
