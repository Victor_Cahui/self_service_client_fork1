import { Injectable } from '@angular/core';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { IItemMarker } from '@mx/statemanagement/models/google-map.interface';
import { BehaviorSubject, Observable } from 'rxjs';

export enum GmFilterEvents {
  FILTER = 'filter',
  FAVORITE_MARKER = 'favorite_marker',
  CLEAR_SEARCH_INPUT = 'clear_search_input',
  FILTER_NOTIFY = 'filter_notify',
  FILTERS_ACTIVED = 'filters_actived'
}

export enum KEY_STORAGE_FILTER {
  OFFICE_NOTIFIED = 'office_filter_notified',
  CLINIC_NOTIFIED = 'clinic_filter_notified'
}

@Injectable()
export class GmFilterService {
  events: BehaviorSubject<{ event: GmFilterEvents; data: any }> = new BehaviorSubject<any>(null);
  private readonly _openedFilterRx = new BehaviorSubject(false);

  private items: Array<IItemMarker>;
  private modifiedItems: Array<IItemMarker>;

  constructor(private readonly storageService: StorageService) {}

  setItems(items: Array<IItemMarker>): void {
    this.items = items;
  }
  getItems(): Array<IItemMarker> {
    return this.items;
  }

  getModifiedItems(): Array<IItemMarker> {
    return this.modifiedItems;
  }

  setModifiedItems(modifiedItems: Array<IItemMarker>): void {
    this.modifiedItems = modifiedItems;
  }

  eventEmit(event: GmFilterEvents, data?: any): void {
    this.events.next({ event, data });
  }

  onEvent(): BehaviorSubject<{ event: GmFilterEvents; data?: any }> {
    return this.events;
  }

  resetEvent(): void {
    this.events = new BehaviorSubject<any>(null);
  }

  setFilteredOfficesNotified(value: boolean): void {
    this.storageService.saveStorage(KEY_STORAGE_FILTER.OFFICE_NOTIFIED, value);
  }

  getFilteredOfficesNotified(): boolean {
    return this.storageService.getStorage(KEY_STORAGE_FILTER.OFFICE_NOTIFIED);
  }

  setFilteredClinicsNotified(value: boolean): void {
    this.storageService.saveStorage(KEY_STORAGE_FILTER.CLINIC_NOTIFIED, value);
  }

  getFilteredClinicsNotified(): boolean {
    return this.storageService.getStorage(KEY_STORAGE_FILTER.CLINIC_NOTIFIED);
  }

  getOpenedFilterRx(): Observable<boolean> {
    return this._openedFilterRx.asObservable();
  }

  emitOpenedFilterRx(state: boolean): void {
    this._openedFilterRx.next(state);
  }
}
