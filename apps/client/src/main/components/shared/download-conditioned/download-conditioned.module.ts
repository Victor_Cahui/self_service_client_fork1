import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DirectivesModule } from '@mx/core/ui/lib/directives/directives.module';
import { DownLoadConditionedComponent } from './download-conditioned.component';

@NgModule({
  imports: [CommonModule, DirectivesModule],
  exports: [DownLoadConditionedComponent],
  declarations: [DownLoadConditionedComponent]
})
export class DownLoadConditionedModule {}
