import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardHowToWorkComponent } from './card-how-to-work.component';

@NgModule({
  imports: [CommonModule],
  exports: [CardHowToWorkComponent],
  declarations: [CardHowToWorkComponent]
})
export class CardHowToWorkModule {}
