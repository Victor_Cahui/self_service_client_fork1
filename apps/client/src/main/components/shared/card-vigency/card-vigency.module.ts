import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DirectivesModule, MfCardModule, MfInputDatepickerModule, MfShowErrorsModule, SwitchModule } from '@mx/core/ui';
import { CardVigencyComponent } from './card-vigency.component';

@NgModule({
  imports: [CommonModule, MfCardModule, DirectivesModule, SwitchModule, MfInputDatepickerModule, MfShowErrorsModule],
  declarations: [CardVigencyComponent],
  exports: [CardVigencyComponent]
})
export class CardVigencyModule {}
