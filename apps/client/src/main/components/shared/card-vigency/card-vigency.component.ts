import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { AddDays, AddYears, dateToStringFormatYYYYMMDD, stringDDMMYYYYToDate } from '@mx/core/shared/helpers/util/date';
import { GeneralService } from '@mx/services/general/general.service';
import { PaymentService } from '@mx/services/payment.service';
import { PoliciesQuoteService } from '@mx/services/policy/policy-quote.service';
import { MethodPaymentLang } from '@mx/settings/lang/payment.lang';
import { IParameterApp } from '@mx/statemanagement/models/general.models';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'client-card-vigency',
  templateUrl: './card-vigency.component.html'
})
export class CardVigencyComponent implements OnInit {
  @HostBinding('class') class = 'w-px-lg--36 w-10 order-1 order-lg-2';

  @Input() key: string;

  collapse = false;
  lang = MethodPaymentLang;

  limitDateMin: Date;
  limitDateMax: Date;
  endDate: string;
  private _startDate: Date;
  parametersSubject: Observable<Array<IParameterApp>>;

  doingPaymentSub: Subscription;

  doingPayment: boolean;

  constructor(
    private readonly generalService: GeneralService,
    private readonly paymentService: PaymentService,
    private readonly quoteService: PoliciesQuoteService
  ) {
    this.doingPaymentSub = this.quoteService.doingPaymentSubject.subscribe(
      (value: boolean) => (this.doingPayment = value),
      () => {},
      () => {
        this.doingPaymentSub.unsubscribe();
      }
    );
  }

  get startDate(): Date {
    return this._startDate;
  }

  set startDate(currentDate: Date) {
    this.endDate = currentDate ? dateToStringFormatYYYYMMDD(AddYears(currentDate, 1)) : '';
    this.paymentService.setVigencyDate(dateToStringFormatYYYYMMDD(currentDate), this.endDate);
    this._startDate = currentDate;
  }

  ngOnInit(): void {
    const quote = this.paymentService.getCurrentQuote();
    this.limitDateMin = quote && quote.expiredDate ? stringDDMMYYYYToDate(quote.expiredDate) : new Date();
    this.limitDateMin.setHours(0, 0, 0, 0);
    this.startDate = this.limitDateMin;
    this.getParameter();
  }

  getParameter(): void {
    this.parametersSubject = this.generalService.getParametersSubject();
    this.parametersSubject.subscribe(params => {
      const daysFuture = this.generalService.getValueParams(this.key);
      this.limitDateMax = AddDays(this.limitDateMin, Number(daysFuture));
    });
  }
}
