import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormComponent } from '@mx/components/shared/utils/form';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { GENERAL_MAX_LENGTH, GENERAL_MIN_LENGTH } from '@mx/settings/constants/general-values';
import { ProfileLang } from '@mx/settings/lang/profile.lang';

@Component({
  selector: 'client-input-identity',
  templateUrl: './input-identity.component.html'
})
export class InputIdentityComponent implements OnInit {
  @Input() form: FormGroup;
  @Output() changeData: EventEmitter<boolean> = new EventEmitter<boolean>();

  docNumber: AbstractControl;
  labelDocumentNumber = ProfileLang.Labels.DocumentNumber;
  maxLengthDocNumber = `${GENERAL_MAX_LENGTH.numberDocumentGeneric}`;
  minLengthDocNumber = `${GENERAL_MIN_LENGTH.numberDocumentGeneric}`;

  constructor(private readonly fb: FormBuilder) {}

  ngOnInit(): void {
    this.initForm();
  }

  // Formulario
  initForm(): void {
    const group = this.fb.group({
      docNumber: ['', Validators.required]
    });

    this.form.addControl('identity', group);
    this.docNumber = this.form.controls['identity'].get('docNumber');
  }

  showErrors(controlKey: string): boolean {
    return FormComponent.showBasicErrors(this.form.get('identity') as FormGroup, controlKey);
  }

  resetForm(): void {
    FormComponent.setControl(this.docNumber, '');
  }

  focusDocumentType(): void {
    this.changeData.emit(true);
  }

  onPasteNumberDoc(event: any): void {
    event.stopPropagation();
    event.preventDefault();
    this.docNumber.setValue(
      StringUtil.getClipboardText(event)
        .trim()
        .replace(/\s/g, '')
    );
  }

  removeSpaces(): void {
    FormComponent.removeAllSpaces(this.docNumber);
  }
}
