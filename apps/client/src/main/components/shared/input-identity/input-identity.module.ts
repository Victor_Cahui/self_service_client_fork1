import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MfInputModule, MfShowErrorsModule } from '@mx/core/ui';
import { InputIdentityComponent } from './input-identity.component';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, MfInputModule, MfShowErrorsModule],
  declarations: [InputIdentityComponent],
  exports: [InputIdentityComponent]
})
export class InputIdentityModule {}
