import { getDDMMYYYY } from '@mx/core/shared/helpers/util/date';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { IClientInformationResponse, IObtenerConfiguracionResponse } from '@mx/statemanagement/models/client.models';
import { IProfileBasicInfo } from '@mx/statemanagement/models/profile.interface';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { takeUntil } from 'rxjs/operators';

export class ConfigurationBase extends GaUnsubscribeBase {
  policyType: string;
  auth: IdDocument;
  data: IClientInformationResponse;

  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService,
    protected gaService?: GAService
  ) {
    super(gaService);
    this.auth = this.authService.retrieveEntity();
  }

  loadInfoProfile(): void {
    if (!this.policiesInfoService.existProfileBasicInfo()) {
      const auth = this.authService.retrieveEntity();
      this.clientService.getClientInformation({}).subscribe(
        (response: IClientInformationResponse) => {
          if (response) {
            this.data = response;
            this.setProfileData({
              fullName: response.nombreCompletoCliente,
              birthDay: getDDMMYYYY(response.fechaNacimiento),
              typeDocument: auth.type,
              document: auth.number,
              phone: response.telefonoMovil,
              email: response.emailCliente,
              registrationDate: getDDMMYYYY(response.fechaAsegurado)
            });
          }
        },
        () => {},
        () => {
          // Guarda en storage
          if (this.data) {
            this.clientService.setUserData(this.data);
          }
        }
      );
    }
  }

  setProfileData(profileBasicInfo: IProfileBasicInfo): void {
    this.policiesInfoService.setProfileBasicInfo(profileBasicInfo);
  }

  loadConfiguration(): void {
    if (this.policyType) {
      const configuration = this.configurationService.getConfigurationByType(this.policyType);
      const hasDigitalClinic = this.configurationService.getConfigurationDigitalClinic() || '';
      if (!configuration) {
        this.clientService
          .getConfiguration({
            codigoApp: APPLICATION_CODE,
            seccion: this.policyType,
            esClinicaDigital: hasDigitalClinic
          })
          .pipe(takeUntil(this.unsubscribeDestroy$))
          .subscribe((response: IObtenerConfiguracionResponse) => {
            this.configurationService.setConfiguration(response, this.policyType);
          });
      } else {
        this.configurationService.setConfiguration(configuration, this.policyType);
      }
    }
  }
}
