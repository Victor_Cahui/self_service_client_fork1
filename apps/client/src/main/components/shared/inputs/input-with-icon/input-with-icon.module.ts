import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MfInputModule } from '@mx/core/ui';
import { InputWithIconComponent } from './input-with-icon.component';

@NgModule({
  imports: [CommonModule, MfInputModule, ReactiveFormsModule],
  declarations: [InputWithIconComponent],
  exports: [InputWithIconComponent]
})
export class InputWithIconModule {}
