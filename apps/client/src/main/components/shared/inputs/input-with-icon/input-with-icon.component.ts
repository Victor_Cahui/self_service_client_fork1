import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'client-input-with-icon',
  template: `
    <div class="position-relative g-input-icon--rigth">
      <ng-content></ng-content>
      <div
        class="input-icon--rigth"
        *ngIf="(icon || classIcon) && showIcon"
        [ngClass]="{ 'g-c--pointer': pointer }"
        (click)="click()"
      >
        <img class="g-box--shadow1 g-b-radius--5" *ngIf="icon" [src]="icon" alt="" />
        <span
          *ngIf="classIcon"
          class="g-pl--5 g-form-icon g-font--21 {{ classIcon }}"
          [ngClass]="{ 'g-c--red1': error }"
        ></span>
      </div>
    </div>
  `
})
export class InputWithIconComponent {
  @Output() clickIcon: EventEmitter<any> = new EventEmitter<any>();

  @Input() frm: FormGroup;
  @Input() name: string;
  @Input() icon: string;
  @Input() classIcon: string;
  @Input() error: boolean;
  @Input() pointer: boolean;
  @Input() showIcon = true;

  click(): void {
    if (!this.frm) {
      this.clickIcon.next();

      return void 0;
    }

    this.frm.get(this.name).reset();
  }
}
