import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  HostBinding,
  Input,
  Output,
  Renderer2,
  ViewChild
} from '@angular/core';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';

@Component({
  selector: 'client-input-search',
  template: `
    <div class="d-flex g-b--1 g-bc--gray11 g-p--5 g-b-radius--5" [ngClass]="margin ? ['m-' + margin] : []">
      <span class="icon-mapfre_099_search g-font--21 pr-2 g-c--gray9"></span>
      <input
        #input
        type="text"
        value=""
        class="no-spinners w-10 text-truncate"
        [spellcheck]="spellcheck"
        [placeholder]="placeholder"
        [(ngModel)]="searchText"
        [maxlength]="maxLength"
        [Alphanumeric]="alphanumeric"
        (focus)="onFocusInput($event)"
        (blur)="onBlurInput($event)"
        ctrl="inputSearch"
        [attr.data-searchtext]="searchText"
        (domChange)="onDomChange($event, {})"
      />
      <div *ngIf="!hideClear" class="g-ml--5 g-flex" [ngClass]="{ 'd-none': (searchText || '').length === 0 }">
        <span class="icon-mapfre_121_close g-font--21 g-c--gray9 g-c--pointer" (click)="searchText = ''"></span>
      </div>
    </div>
  `
})
export class InputSearchComponent extends GaUnsubscribeBase implements AfterViewInit {
  @HostBinding('class') class = `g-px-lg--0`;
  @ViewChild('input', { read: ElementRef })
  input_native: ElementRef;

  @Output() search: EventEmitter<string>;
  @Output() cancel: EventEmitter<any>;
  @Output() onfocus: EventEmitter<string>;
  @Output() onblur: EventEmitter<string>;
  @Input() placeholder = 'Buscar';
  @Input() spellcheck = false;
  @Input() width = 35;
  @Input() full: boolean;
  @Input() margin: number;
  @Input() maxLength: string;
  @Input() alphanumeric: boolean;
  @Input() hideClear: boolean;
  @Input()
  get searchText(): string {
    return this._searchText;
  }

  set searchText(searchText: string) {
    if (searchText && searchText !== this._searchText) {
      this.search.next(searchText);
    } else if (!searchText) {
      this.cancel.next();
    }
    this._searchText = searchText;
  }

  private _searchText: string;

  constructor(
    private readonly renderer2: Renderer2,
    private readonly elementRef: ElementRef,
    protected gaService: GAService
  ) {
    super(gaService);
    this.search = new EventEmitter<string>();
    this.cancel = new EventEmitter<any>();
    this.onfocus = new EventEmitter<any>();
    this.onblur = new EventEmitter<any>();
  }

  ngAfterViewInit(): void {
    if (!this.full) {
      this.renderer2.addClass(this.elementRef.nativeElement, `w-px-lg--${this.width}`);
    }
    this.renderer2.addClass(this.elementRef.nativeElement, `w-10`);
  }

  clearText(): void {
    this.searchText = '';
  }

  onFocusInput(event: any): void {
    this.onfocus.emit(event);
  }

  onBlurInput(event: any): void {
    this.onblur.emit(event);
  }
}
