import { Component, Input, OnInit } from '@angular/core';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google/google.gabase';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { MFP_Poliza_de_Seguro_de_Salud_30A } from '@mx/settings/constants/events.analytics';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import { ITabItem } from '@mx/statemanagement/models/router.interface';

@Component({
  selector: 'client-card-content-coverage',
  templateUrl: './card-content-coverage.component.html'
})
export class CardContentCoverageComponent extends GaUnsubscribeBase implements OnInit {
  @Input() policyType: string;
  @Input() policyNumber: string;
  @Input() viewTrace: false;
  @Input() routeFaq: string;
  @Input() optionsRoutes: Array<ITabItem>;

  titleCard: string;
  textFaqs = GeneralLang.Titles.Faq;
  selectLabel = GeneralLang.Labels.Select;
  foilAttached = GeneralLang.Labels.FoilAttached;

  constructor(private readonly policiesInfoService: PoliciesInfoService, protected gaService?: GAService) {
    super(gaService);
    this.titleCard = PolicyLang.Titles.MyCoverages;
    this.ga = [MFP_Poliza_de_Seguro_de_Salud_30A()];
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.setFoilsAttached();
    });
  }

  setFoilsAttached(): void {
    const isSctr = this.policyType === POLICY_TYPES.MD_SCTR.code;
    const clientPolicy = isSctr
      ? this.policiesInfoService.getClientSctrPolicy(this.policyNumber)
      : this.policiesInfoService.getClientPolicy(this.policyNumber);
    const option = this.optionsRoutes.find(el => el.name === this.foilAttached);
    option && (option.visible = clientPolicy.tieneHojaAnexa);
  }
}
