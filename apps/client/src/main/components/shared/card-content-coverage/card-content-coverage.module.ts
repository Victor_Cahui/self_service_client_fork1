import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { MfCardModule } from '@mx/core/ui';
import { MfTabVerticalModule } from '@mx/core/ui/lib/components/tabs/tabs-vertical/tab-vertical.module';
import { CardContentCoverageComponent } from './card-content-coverage.component';

@NgModule({
  imports: [CommonModule, RouterModule, MfCardModule, MfTabVerticalModule, GoogleModule],
  declarations: [CardContentCoverageComponent],
  exports: [CardContentCoverageComponent]
})
export class CardContentCoverageModule {}
