import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { NotificationModule } from '@mx/core/shared/helpers';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AutoserviciosMock } from '@mx/core/shared/providers/services/proxy/Autoservicios.mock';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { PERIODS_LANAG } from '@mx/settings/lang/sctr.lang';

import { CardSctrPeriodRisksComponent } from './card-sctr-period-risks.component';

describe('CardSctrPeriodRisksComponent', () => {
  let component: CardSctrPeriodRisksComponent;
  let fixture: ComponentFixture<CardSctrPeriodRisksComponent>;
  let el: DebugElement;
  const lang = PERIODS_LANAG;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule, MfLoaderModule, NotificationModule],
      declarations: [CardSctrPeriodRisksComponent],
      providers: [{ provide: Autoservicios, useClass: AutoserviciosMock }]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(CardSctrPeriodRisksComponent);
        component = fixture.debugElement.componentInstance;
        el = fixture.debugElement;
      })
      .then(() => {
        component.policyNumber = '7022000001086';
        component.isDeclaration = true;
      })
      .then(() => {
        fixture.detectChanges();
      });
  }));

  it('should create', async(() => {
    expect(component).toBeTruthy();
  }));

  it('to validate component properties', async(() => {
    component.policyNumber = '7022000001086';
    component.isDeclaration = true;
    expect(component.policyNumber).toBeDefined();
    expect(component.isDeclaration).toBeDefined();
  }));

  it('should load info risks', async(() => {
    component.policyNumber = '7022000001086';
    component.isDeclaration = false;
    component.loadInfoRiskSctr();
  }));

  it('card title should be RisksToInclude', async(() => {
    component.isDeclaration = true;
    fixture.detectChanges();
    fixture
      .whenStable()
      .then(() => {
        const element = el.query(By.css('.g-items-text'));
        expect(element.nativeElement.innerHTML).toBe(lang.RisksToInclude);
      })
      .catch(e => {
        console.error(e);
      });
  }));
});
