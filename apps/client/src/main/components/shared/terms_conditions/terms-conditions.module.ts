import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfCustomAlertModule } from '@mx/core/ui/lib/components/alerts/custom-alert';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { TermsConditionsComponent } from './terms-conditions.component';

@NgModule({
  imports: [CommonModule, MfLoaderModule, MfCustomAlertModule],
  exports: [TermsConditionsComponent],
  declarations: [TermsConditionsComponent]
})
export class TermsConditionsModule {}
