import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Comun } from '@mx/core/shared/providers/services';
import { NotificationStatus } from '@mx/core/ui/lib/components/alerts/custom-alert/custom-alert.component';
import { APPLICATION_CODE, STATE_TERMS_AND_CONDITIONS, TYPE_OF_MODULE } from '@mx/settings/constants/general-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';

@Component({
  selector: 'client-terms-conditions',
  templateUrl: './terms-conditions.component.html'
})
export class TermsConditionsComponent {
  @Input() typeModule: string = TYPE_OF_MODULE.CDM;
  btnToCancel = GeneralLang.Buttons.Cancel;
  btnToAccept = GeneralLang.Buttons.Confirm;
  @Output() changeTermsAndConditions: EventEmitter<any> = new EventEmitter();
  showLoading = true;
  textAlertInfo = GeneralLang.Alert.changeTermsAndConditions;
  statusAlertInfo = NotificationStatus.INFO;
  textTermAndConditions = '';
  idTermAndCondition: string;
  routeTermAndConditions: string;

  constructor(protected readonly _comun: Comun, protected readonly router: Router) {
    this.getTermsAndConditions(this.typeModule);
  }

  public toRefuse(): void {
    this.router.navigate([this.routeTermAndConditions]);
  }

  public toAccept(): void {
    this._comun
      .AprobarTerminosCondiciones(
        { id: this.idTermAndCondition },
        { codigoApp: APPLICATION_CODE },
        { estado: STATE_TERMS_AND_CONDITIONS.APPROVED }
      )
      .subscribe(
        response => {
          this.changeTermsAndConditions.emit(null);
        },
        error => {}
      );
  }

  private getTermsAndConditions(typeModule: string): void {
    this.showLoading = true;
    const queryReq = {
      codigoApp: APPLICATION_CODE,
      modulo: typeModule,
      estadoVigencia: 'VIGENTE',
      proceso: 'SERVICIOS'
    };
    this._comun.ObtenerTerminosCondicionesCliente(queryReq).subscribe(
      response => {
        this.showLoading = false;
        if (response.length > 0) {
          const term = response[0];
          if (term.estado !== STATE_TERMS_AND_CONDITIONS.APPROVED) {
            this.idTermAndCondition = term.id;
            this.textTermAndConditions = term.descripcion;
            this.routeTermAndConditions = term.rutaCancelar;
          } else {
            this.changeTermsAndConditions.emit(null);
          }
        } else {
          this.changeTermsAndConditions.emit(null);
        }
      },
      error => {
        this.showLoading = false;
        console.error(error);
        this.router.navigate(['/home']);
      }
    );
  }
}
