import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DirectivesModule, MfCardModule, MfModalMessageModule } from '@mx/core/ui';

import { ModalHowWorksModule } from '@mx/components/shared/modals/modal-how-works/modal-how-works.module';
import { NgDropModule } from '@mx/core/ui/lib/directives/ng-drop/ng-drop.module';
import { NgxUploaderModule } from 'ngx-uploader';
import { CardSctrPeriodUploadComponent } from './card-sctr-period-upload.component';

@NgModule({
  imports: [
    CommonModule,
    DirectivesModule,
    MfCardModule,
    NgDropModule,
    NgxUploaderModule,
    MfModalMessageModule,
    ModalHowWorksModule
  ],
  declarations: [CardSctrPeriodUploadComponent],
  exports: [CardSctrPeriodUploadComponent]
})
export class CardSctrPeriodUploadModule {}
