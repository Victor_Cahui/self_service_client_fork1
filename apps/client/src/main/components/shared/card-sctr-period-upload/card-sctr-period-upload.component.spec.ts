import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ModalHowWorksModule } from '@mx/components/shared/modals/modal-how-works/modal-how-works.module';
import { NotificationModule } from '@mx/core/shared/helpers';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AutoserviciosMock } from '@mx/core/shared/providers/services/proxy/Autoservicios.mock';
import { DirectivesModule, MfCardModule, MfModalMessageModule } from '@mx/core/ui';
import { MfButtonModule } from '@mx/core/ui/lib/components/forms/button';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { NgDropModule } from '@mx/core/ui/lib/directives/ng-drop/ng-drop.module';
import { LinksModule } from '@mx/core/ui/lib/links/links.module';
import { NgxUploaderModule } from 'ngx-uploader';

import { CardSctrPeriodUploadComponent } from './card-sctr-period-upload.component';

describe('CardSctrPeriodUploadComponent', () => {
  let component: CardSctrPeriodUploadComponent;
  let fixture: ComponentFixture<CardSctrPeriodUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        MfLoaderModule,
        NotificationModule,
        DirectivesModule,
        MfCardModule,
        MfModalMessageModule,
        MfButtonModule,
        MfLoaderModule,
        LinksModule,
        ModalHowWorksModule,
        NgDropModule,
        NgxUploaderModule
      ],
      declarations: [CardSctrPeriodUploadComponent],
      providers: [{ provide: Autoservicios, useClass: AutoserviciosMock }]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(CardSctrPeriodUploadComponent);
        component = fixture.debugElement.componentInstance;
      })
      .then(() => {
        component.isDeclaration = false;
      });
  }));

  it('should create', async(() => {
    expect(component).toBeTruthy();
  }));
});
