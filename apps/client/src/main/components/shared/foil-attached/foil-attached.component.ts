import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PoliciesService } from '@mx/services/policies.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { Content } from '@mx/statemanagement/models/general.models';
import { IFoilAttachedRequest, IFoilAttachedResponse } from '@mx/statemanagement/models/policy.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-foil-attached',
  templateUrl: './foil-attached.component.html'
})
export class FoilAttachedComponent implements OnInit {
  @HostBinding('class') class = 'w-100';

  @Input() content: Content;

  policyNumber: string;
  foilAttachedSub: Subscription;
  showLoading: boolean;

  constructor(private readonly policiesService: PoliciesService, private readonly activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((res: any) => {
      this.policyNumber = res.params.policyNumber;
    });

    this.showLoading = true;
    this.foilAttachedSub = this.policiesService
      .getFoildAttachedPolicy({
        codigoApp: APPLICATION_CODE,
        numeroPoliza: this.policyNumber
      } as IFoilAttachedRequest)
      .subscribe(
        (res: IFoilAttachedResponse) => {
          this.content.content = res.hojaAnexa;
          this.showLoading = false;
        },
        () => {
          this.showLoading = false;
        },
        () => {
          this.foilAttachedSub.unsubscribe();
        }
      );
  }
}
