import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ItemNotFoundModule } from '@mx/components/shared/item-not-found/item-not-found.module';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';
import { ServiceListItemModule } from '../service-list-item/service-list-item.module';
import { ServiceListComponent } from './service-list.component';

@NgModule({
  imports: [CommonModule, DirectivesModule, MfCardModule, ItemNotFoundModule, ServiceListItemModule],
  exports: [ServiceListComponent],
  declarations: [ServiceListComponent]
})
export class ServiceListModule {}
