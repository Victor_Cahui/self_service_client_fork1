import { Component, Input } from '@angular/core';
import { NO_PENDING_PROCEDURES_ICON } from '@mx/settings/constants/images-values';
import { IServiceHistoryView } from '@mx/statemanagement/models/service.interface';

@Component({
  selector: 'client-service-list',
  templateUrl: './service-list.component.html'
})
export class ServiceListComponent {
  @Input() serviceList: Array<IServiceHistoryView>;
  @Input() messageNotFound: string;
  @Input() title: string;
  @Input() filter: boolean;
  @Input() historical: boolean;

  icon = NO_PENDING_PROCEDURES_ICON;
}
