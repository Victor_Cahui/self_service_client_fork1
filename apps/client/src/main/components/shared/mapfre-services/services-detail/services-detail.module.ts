import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfCustomAlertModule } from '@mx/core/ui/lib/components/alerts/custom-alert';
import { ServicesDetailComponent } from './services-detail.component';

@NgModule({
  imports: [CommonModule, MfCustomAlertModule],
  declarations: [ServicesDetailComponent],
  exports: [ServicesDetailComponent]
})
export class ServicesDetailModule {}
