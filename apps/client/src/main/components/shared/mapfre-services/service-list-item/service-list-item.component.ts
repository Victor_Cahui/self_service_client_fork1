import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppointmentFacade } from '@mx/core/shared/state/appointment';
import { environment } from '@mx/environments/environment';
import { GeneralService } from '@mx/services/general/general.service';
import * as KEYS_VALUES from '@mx/settings/constants/key-values';
import { SERVICES_TYPE } from '@mx/settings/constants/services-mf';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { IItemView } from '@mx/statemanagement/models/general.models';
import { IServiceHistoryView } from '@mx/statemanagement/models/service.interface';

@Component({
  selector: 'client-service-list-item',
  templateUrl: './service-list-item.component.html'
})
export class ServiceListItemComponent implements OnInit {
  @Input() item: IServiceHistoryView;
  @Input() historical: boolean;

  viewDetailsLink = environment.ACTIVATE_VIEW_DETAIL_LINK_SERVICE;
  citas = SERVICES_TYPE.MEDICAL_APPOINTMENT;
  labelViewDetail: string;
  isLoadingReschedule: boolean;
  visibleCovid = true;
  visibleTextCovid = true;
  module = 'CDM';
  especialityCovid = [104, 503];

  constructor(
    private readonly _AppointmentFacade: AppointmentFacade,
    public router: Router,
    private readonly _GeneralService: GeneralService
  ) {}

  ngOnInit(): void {
    this.visibleCovid =
      this.especialityCovid.includes(this.item.payload.cita.especialidadId) && this.historical ? false : true;
    this.visibleTextCovid = this.especialityCovid.includes(this.item.payload.cita.especialidadId);
    this._AppointmentFacade.CleanAppointment();
    this.labelViewDetail =
      this.item.code === SERVICES_TYPE.MEDICAL_APPOINTMENT && this.historical
        ? GeneralLang.Buttons.Reschedule
        : GeneralLang.Buttons.ViewDetail;
    if (this.especialityCovid.includes(this.item.payload.cita.especialidadId)) {
      this.item.items = this.item.items.filter((x: IItemView) => x.label !== 'Médico:');
    }
  }

  // Detalle
  viewDetail(item: IServiceHistoryView): void {
    if (item.viewDetail) {
      switch (item.code) {
        // Chofer de Reemplazo
        case SERVICES_TYPE.SUBSTITUTE_DRIVER:
          this.router.navigate([`/mapfre-services/substitute-driver/detail/${item.number}`]);
          break;
        // Médico a Domicilio
        case SERVICES_TYPE.HOME_DOCTOR:
          this.router.navigate([`/mapfre-services/home-doctor/detail/${item.number}`]);
          break;
        // Citas Médicas
        case SERVICES_TYPE.MEDICAL_APPOINTMENT:
          this._AppointmentFacade.GetAppointmentForDetail(this.item.payload);
          if (this.labelViewDetail === GeneralLang.Buttons.Reschedule) {
            this._GeneralService.saveUserMovements(
              this.module,
              this._GeneralService.action(KEYS_VALUES.CDM_PROCEDURES.PROC_REAGENDAR_CITA),
              this._GeneralService.action(KEYS_VALUES.CDM_DESCRIPTION.DSC_INICIO)
            );
            this._GeneralService.rescheduleAppointment.next(true);
          }
          this.isLoadingReschedule = true;
          break;
        default:
          break;
      }
    }
  }

  // Title de items
  getTitle(text: IItemView): string {
    return `${text.label ? text.label : ''} ${text.text ? text.text : ''}`;
  }
}
