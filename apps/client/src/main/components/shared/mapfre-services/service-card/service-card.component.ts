import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { SERVICES_DEFAULT_ICON, SERVICES_PATH } from '@mx/settings/constants/images-values';
import { MAPFRE_SERVICES } from '@mx/settings/constants/services-mf';
import { ServicesLang } from '@mx/settings/lang/services.lang';
import { IClientServiceDetail } from '@mx/statemanagement/models/service.interface';

@Component({
  selector: 'client-service-card',
  templateUrl: './service-card.component.html'
})
export class ServiceCardComponent extends GaUnsubscribeBase implements OnInit {
  @Input('item') item: IClientServiceDetail;
  @Input() type: string;
  icon: string;
  route: string;
  textOneService = ServicesLang.Texts.OneService;
  textNoService = ServicesLang.Texts.NoService;
  textServices = ServicesLang.Texts.Services;

  constructor(private readonly router: Router, protected gaService: GAService) {
    super(gaService);
  }

  ngOnInit(): void {
    const types = {
      app: this._mapApps.bind(this),
      service: this._mapService.bind(this)
    };

    types[this.type] && types[this.type]();
  }

  onAction(item): void {
    if (this.route && item.accion && !this.item.onClick) {
      item.maneraContacto === 'REDIRIGIR' ? (window.location.href = this.route) : this.router.navigate([this.route]);
    }

    if (this.item.onClick) {
      this.item.onClick(item);
    }
  }

  private _mapService(): void {
    const item = MAPFRE_SERVICES.find(c => {
      return c.key === this.item.llave;
    }) || { icon: SERVICES_DEFAULT_ICON, route: null };
    this.icon = `${SERVICES_PATH}${item.icon}.svg`;
    this.route = item.route;
  }

  private _mapApps(): void {
    this.icon = this.item.icon;
  }
}
