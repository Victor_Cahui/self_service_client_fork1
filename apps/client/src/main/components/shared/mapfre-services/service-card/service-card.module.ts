import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { ServiceCardComponent } from './service-card.component';

@NgModule({
  imports: [CommonModule, RouterModule, GoogleModule],
  exports: [ServiceCardComponent],
  declarations: [ServiceCardComponent]
})
export class ServiceCardModule {}
