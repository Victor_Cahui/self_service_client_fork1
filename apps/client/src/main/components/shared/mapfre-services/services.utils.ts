import { dateToStringFormatMMDDYYYY, FormatDDMMMYYYY, ValidateStringLikeDate } from '@mx/core/shared/helpers/util/date';
import { IHeader } from '@mx/services/general/header-helper.service';
import { COLOR_STATUS } from '@mx/settings/constants/general-values';
import { SERVICES_TYPE, STATUS_SERVICE } from '@mx/settings/constants/services-mf';
import { HomeDoctorLang, ServiceFormLang, SubstituteDriverLang } from '@mx/settings/lang/services.lang';
import { IServicesItemResponse } from '@mx/statemanagement/models/service.interface';

export class ServicesUtils {
  // Titulo detalle del servicio
  static getStatusTitle(values: IServicesItemResponse): string {
    const lang = values.codigoTipoSolicitud === SERVICES_TYPE.SUBSTITUTE_DRIVER ? SubstituteDriverLang : HomeDoctorLang;
    const request = ServiceFormLang;
    const status = values.codigoEstado;
    let text: string;

    switch (status) {
      case STATUS_SERVICE.ATTENDED:
        text = request.AttendedService;
        break;
      case STATUS_SERVICE.REJECTED:
        text = request.RejectedService;
        break;
      default:
        text = lang.ScheduledService;
        break;
    }

    return text;
  }

  // Texto de detalle de servicio
  static getStatusText(values: IServicesItemResponse): string {
    const lang = values.codigoTipoSolicitud === SERVICES_TYPE.SUBSTITUTE_DRIVER ? SubstituteDriverLang : HomeDoctorLang;

    return values.codigoEstado !== STATUS_SERVICE.ATTENDED && values.codigoEstado !== STATUS_SERVICE.REJECTED
      ? lang.MessageConfirmService
      : '';
  }

  // Rechazado
  static isRejected(status: string): boolean {
    return STATUS_SERVICE.REJECTED === status;
  }

  // Aceptado
  static isAttended(status: string): boolean {
    return STATUS_SERVICE.ATTENDED === status || STATUS_SERVICE.ACCEPTED === status;
  }

  // Color del led
  static getStatusColor(status: string): string {
    let color: string;
    switch (status) {
      case STATUS_SERVICE.ACCEPTED:
        color = COLOR_STATUS.GREEN;
        break;
      case STATUS_SERVICE.INPROGRESS:
        color = COLOR_STATUS.YELLOW;
        break;
      case STATUS_SERVICE.REJECTED:
        color = COLOR_STATUS.RED;
        break;
      default:
        color = COLOR_STATUS.GRAY;
        break;
    }

    return color;
  }

  // Fecha Agendada o URGENTE
  static getScheduledText(scheduled: string, status: string, emergency = false): string {
    if (!emergency && scheduled) {
      const dateTime = scheduled.split('-');
      const validDate = ValidateStringLikeDate(dateTime[0]);

      if (validDate) {
        const validDateString = dateToStringFormatMMDDYYYY(validDate);
        const dateSplit = validDateString.split('/');
        const date = `${dateSplit[2]}/${dateSplit[1]}/${dateSplit[0]}`;

        return `${FormatDDMMMYYYY(date)} - ${dateTime[1].toUpperCase()}`;
      }
    }

    return ServicesUtils.isRejected(status) ? ServiceFormLang.Unconfirmed : ServiceFormLang.ToBeConfirmed;
  }

  // Titulo Pagina
  static getHeader(header: IHeader, values: IServicesItemResponse): IHeader {
    return {
      subTitle: header.subTitle,
      title: `${values.descripcionTipoSolicitud.toUpperCase()} #${values.identificadorSolicitud}`,
      icon: '',
      back: true
    };
  }

  // Warning solo si esta en curso
  static getWarning(values: IServicesItemResponse): string {
    const lang = values.codigoTipoSolicitud === SERVICES_TYPE.SUBSTITUTE_DRIVER ? SubstituteDriverLang : HomeDoctorLang;

    return values.codigoEstado === STATUS_SERVICE.ACCEPTED ? lang.WarningDetails : '';
  }

  // Ver Detalle
  static viewDetail(status: string): boolean {
    return status && status !== STATUS_SERVICE.INPROGRESS;
  }
}
