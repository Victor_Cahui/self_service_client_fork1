import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NO_PENDING_PROCEDURES_ICON } from '@mx/settings/constants/images-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { IOutputEvent, ItemTimeline } from '@mx/statemanagement/models/timeline.interface';

@Component({
  selector: 'client-view-timeline',
  templateUrl: './view-timeline.component.html'
})
export class ViewTimelineComponent implements OnInit {
  @Input() title: string;
  @Input() itemList: Array<ItemTimeline>;
  @Input() showLoading: boolean;
  @Output() outputEvent: EventEmitter<IOutputEvent> = new EventEmitter<IOutputEvent>();

  icon = NO_PENDING_PROCEDURES_ICON;
  messageNotFound = GeneralLang.Messages.ItemNotFound;

  ngOnInit(): void {}

  eventFromTimeLine(data: IOutputEvent): void {
    this.outputEvent.emit(data);
  }
}
