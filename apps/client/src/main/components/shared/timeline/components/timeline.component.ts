import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  convertMonth,
  dateToStringFormatYYYYMMDD,
  splitDate,
  ValidateStringLikeDate
} from '@mx/core/shared/helpers/util/date';
import { IOutputEvent, ItemTimeline } from '@mx/statemanagement/models/timeline.interface';

@Component({
  selector: 'client-timeline',
  templateUrl: './timeline.component.html'
})
export class TimelineComponent implements OnInit {
  @Input() title: string;
  @Input() itemList: Array<ItemTimeline>;
  @Output() outputEvent: EventEmitter<IOutputEvent> = new EventEmitter<IOutputEvent>();

  ngOnInit(): void {
    if (this.itemList) {
      this.itemList.map(item => {
        if (item.date) {
          const validDate = ValidateStringLikeDate(item.date);
          if (validDate) {
            const validDateString = dateToStringFormatYYYYMMDD(validDate);
            const date = splitDate(validDateString);
            item.day = date[2];
            item.month = convertMonth(date[1]);
            item.year = date[0];

            return item;
          }
        }
      });
    }
  }

  eventCard(cardType: string, payload: any): void {}

  eventFooter(status: string, payload: any): void {
    this.outputEvent.emit({ card: status, data: payload });
  }
}
