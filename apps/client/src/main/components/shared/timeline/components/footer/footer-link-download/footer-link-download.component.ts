import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ModalDownLoadComponent } from '@mx/components/shared/modals/modal-download/modal-download.component';
import { FileUtil } from '@mx/core/shared/helpers/util/file';
import { IFileList } from '@mx/statemanagement/models/general.models';

@Component({
  selector: 'client-footer-link-download',
  templateUrl: './footer-link-download.component.html'
})
export class FooterLinkDownloadComponent implements OnInit {
  @ViewChild(ModalDownLoadComponent) modalDownLoad: ModalDownLoadComponent;

  @Output() outputEvent: EventEmitter<any> = new EventEmitter<any>();
  @Input() content: any;

  fileUtil = new FileUtil();
  fileList: Array<IFileList> = [];
  showModalDownLoad: boolean;

  ngOnInit(): void {}

  downloadFile(): void {
    // Un archivo
    if (this.content.action && this.content.action.base64) {
      this.fileUtil.download(
        this.content.action.base64,
        this.content.action.base64Type || 'pdf',
        this.content.action.fileName
      );
    } else if (this.content.action && this.content.action.fileList) {
      // Varios archivos
      this.fileList = this.content.action.fileList;
      this.showModalDownLoad = true;
      setTimeout(() => {
        this.modalDownLoad.open();
      }, 50);
    } else {
      this.outputEvent.emit();
    }
  }

  downloadFileEvent(file: IFileList): void {
    this.outputEvent.emit(file);
  }
}
