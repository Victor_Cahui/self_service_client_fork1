import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '@mx/services/auth/auth.service';
import { VehicleService } from '@mx/services/vehicle.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { IRateWorkshopRequestBody } from '@mx/statemanagement/models/vehicle.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-footer-star-rating',
  templateUrl: './footer-star-rating.component.html'
})
export class FooterStarRatingComponent implements OnInit, OnDestroy {
  @Input() content: any;

  lang = GeneralLang;

  auth: IdDocument;
  ratingSub: Subscription;
  label: string;
  enableRating: boolean;
  showButtons: boolean;
  rating: number;

  constructor(
    private readonly authService: AuthService,
    private readonly vehicleService: VehicleService,
    private readonly activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.auth = this.authService.retrieveEntity();
    if (this.content.action) {
      this.setRatingContent();
    }
  }

  setRatingContent(): void {
    this.label = this.content.action.rated ? this.lang.Messages.WorkshopRated : this.lang.Messages.WorkshopRating;
    this.rating = this.content.action.rated ? this.content.action.rate : 0;
    this.enableRating = !this.content.action.rated;
  }

  rateWorkshop(): void {
    const body: IRateWorkshopRequestBody = {
      codigoApp: APPLICATION_CODE,
      numeroAsistencia: this.activatedRoute.snapshot.params['number'],
      identificadorTaller: this.content.itemId,
      calificacion: this.rating
    };
    this.ratingSub = this.vehicleService.rateWorkshop(body).subscribe(
      res => {
        if (res) {
          this.label = this.lang.Messages.WorkshopRated;
          this.enableRating = this.showButtons = false;
        }
      },
      err => {
        this.ratingSub.unsubscribe();
      },
      () => {
        this.ratingSub.unsubscribe();
      }
    );
  }

  onSelection(stars: number): void {
    this.rating = stars;
    this.showButtons = true;
  }

  ngOnDestroy(): void {
    if (this.ratingSub) {
      this.ratingSub.unsubscribe();
    }
  }

  cancelRate(): void {
    this.rating = 0;
    this.showButtons = false;
  }
}
