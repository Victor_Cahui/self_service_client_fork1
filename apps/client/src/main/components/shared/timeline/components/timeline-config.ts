// Accidente vehicular
export const KEY_STATUS_VEHICLE = {
  DECLARED: 'AUTOS_ASIST_ESTADO_DECLARADO',
  ATTENDED: 'AUTOS_ASIST_ESTADO_ATENDIDO',
  IN_WORKSHOP: 'AUTOS_ASIST_ESTADO_EN_TALLER',
  ISSUED_BUDGET: 'AUTOS_ASIST_ESTADO_PR_EMITIDO',
  ACCEPTED_BUDGET: 'AUTOS_ASIST_ESTADO_PR_ACEPTADO',
  IN_REPAIR: 'AUTOS_ASIST_ESTADO_EN_REPARACION',
  VEHICLE_DELIVERED: 'AUTOS_ASIST_ESTADO_ENTREGADO',
  IN_EVALUATION: 'AUTOS_ASIST_ESTADO_EN_EVALUACION',
  APPROVED: 'AUTOS_ASIST_ESTADO_APROBACION_PT',
  DOCUMENT_DELIVERED: 'AUTOS_ASIST_ESTADO_ENTREG_DOCUM',
  COMPENSATION: 'AUTOS_ASIST_ESTADO_INDEMNIZACION',
  REFUSED: 'AUTOS_ASIST_ESTADO_RECHAZADO',
  RECOVERED: 'AUTOS_ASIST_ESTADO_RECUPERADO',
  ATTENDED_SEND: 'ENVIO_PROCURADOR',
  ATTENDED_OK: 'LLEGADA_PROCURADOR'
};

export enum CARD_TYPES {
  ICON = 'card_icon',
  ICON_LIST = 'card_icon_list',
  TABLE = 'card_table',
  TEXT = 'card_text'
}

export enum FOOTER_TYPES {
  LINK = 'footer_link',
  DOWNLOAD = 'footer_link_download',
  RATING = 'footer_rating'
}

export const CARD_STATUS_VEHICLE = {
  AUTOS_ASIST_ESTADO_DECLARADO: {
    card: CARD_TYPES.ICON_LIST,
    footer: '',
    title: 'Declaraste tu accidente vehicular',
    title2: 'Declaraste tu robo'
  },
  AUTOS_ASIST_ESTADO_ATENDIDO: {
    ENVIO_PROCURADOR: {
      card: CARD_TYPES.ICON_LIST,
      footer: '',
      title: 'Te enviamos al procurador y una grúa'
    },
    LLEGADA_PROCURADOR: {
      card: CARD_TYPES.ICON_LIST,
      footer: FOOTER_TYPES.DOWNLOAD,
      title: 'El procurador llegó a tu ubicación'
    }
  },
  AUTOS_ASIST_ESTADO_EN_TALLER: {
    card: CARD_TYPES.ICON_LIST,
    footer: '',
    title: 'Tu auto entró al taller'
  },
  AUTOS_ASIST_ESTADO_PR_EMITIDO: {
    card: CARD_TYPES.TEXT,
    footer: '',
    title: 'El taller emitió el presupuesto',
    title2: 'El taller ha emitido un nuevo presupuesto'
  },
  AUTOS_ASIST_ESTADO_PR_ACEPTADO: {
    card: CARD_TYPES.TABLE,
    footer: '',
    title: 'MAPFRE aprobó el presupuesto',
    title2: 'MAPFRE aprobó el nuevo presupuesto'
  },
  AUTOS_ASIST_ESTADO_EN_REPARACION: {
    card: CARD_TYPES.ICON,
    footer: '',
    title: 'El auto entró en reparación'
  },
  AUTOS_ASIST_ESTADO_ENTREGADO: {
    ENTREGA_ESTIMADA: {
      card: '',
      footer: '',
      title: 'Entrega estimada'
    },
    PUEDE_RECOGER: {
      card: CARD_TYPES.ICON,
      footer: '',
      title: 'Puedes ir a recoger tu auto'
    },
    ENTREGADO: {
      card: CARD_TYPES.ICON,
      footer: FOOTER_TYPES.RATING,
      title: 'Recogiste tu auto'
    }
  },
  AUTOS_ASIST_ESTADO_EN_EVALUACION: {
    card: CARD_TYPES.ICON_LIST,
    footer: '',
    title: 'Evaluación de pérdida total'
  },
  AUTOS_ASIST_ESTADO_APROBACION_PT: {
    footer: FOOTER_TYPES.DOWNLOAD,
    title: 'Aprobación de pérdida total'
  },
  AUTOS_ASIST_ESTADO_ENTREG_DOCUM: {
    card: CARD_TYPES.ICON_LIST,
    footer: FOOTER_TYPES.LINK,
    title: 'Documentación para la notaría'
  },
  AUTOS_ASIST_ESTADO_INDEMNIZACION: {
    card: CARD_TYPES.TEXT,
    footer: FOOTER_TYPES.DOWNLOAD,
    title: 'Indemnización'
  },
  AUTOS_ASIST_ESTADO_RECHAZADO: {
    title: 'Caso Rechazado'
  },
  AUTOS_ASIST_ESTADO_RECUPERADO: {
    title: 'Recuperamos tu vehículo'
  }
};

// Reembolso
export const KEY_STATUS_REFUND = {
  REQUEST: 'SOLICITUD',
  REVIEW: 'EN_REVISION',
  FOR_ANSWER: 'POR_RESPONDER',
  REFUSED: 'RECHAZADO',
  APPROVED: 'APROBADO'
};

export const CARD_STATUS_REFUND = {
  SOLICITUD: {
    card: CARD_TYPES.ICON_LIST,
    footer: FOOTER_TYPES.DOWNLOAD,
    title: 'Solicitaste un reembolso'
  },
  EN_REVISION: {
    title: 'La solicitud se encuentra en revisión'
  },
  POR_RESPONDER: {
    title: 'Aprobación del reembolso estimada'
  },
  RECHAZADO: {
    card: CARD_TYPES.TEXT,
    title: 'Reembolso rechazado'
  },
  APROBADO: {
    card: CARD_TYPES.TEXT,
    title: 'Reembolso aprobado'
  }
};
