import { Component, Input } from '@angular/core';
import { Content } from '@mx/statemanagement/models/general.models';

@Component({
  selector: 'client-card-text',
  templateUrl: './card-text.component.html'
})
export class CardTextComponent {
  @Input() content: Content;
}
