import {
  ComponentFactoryResolver,
  ComponentRef,
  Directive,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewContainerRef
} from '@angular/core';
import { CardIconListComponent } from './cards/card-icon-list/card-icon-list.component';
import { CardIconComponent } from './cards/card-icon/card-icon.component';
import { CardTableComponent } from './cards/card-table/card-table.component';
import { CardTextComponent } from './cards/card-text/card-text.component';
import { FooterLinkDownloadComponent } from './footer/footer-link-download/footer-link-download.component';
import { FooterLinkComponent } from './footer/footer-link/footer-link.component';
import { FooterStarRatingComponent } from './footer/footer-star-rating/footer-star-rating.component';

const cardComponents = {
  card_table: CardTableComponent,
  card_icon: CardIconComponent,
  card_icon_list: CardIconListComponent,
  card_text: CardTextComponent,
  footer_link: FooterLinkComponent,
  footer_link_download: FooterLinkDownloadComponent,
  footer_rating: FooterStarRatingComponent
};

@Directive({
  selector: '[clientDynamicCard]'
})
export class DynamicCardDirective implements OnInit {
  @Output() outputEvent: EventEmitter<any> = new EventEmitter<any>();

  @Input() card: string;
  @Input() content: any;

  component: ComponentRef<any>;

  constructor(private readonly resolver: ComponentFactoryResolver, private readonly container: ViewContainerRef) {}

  ngOnInit(): void {
    const component = cardComponents[this.card];
    if (component) {
      const factory = this.resolver.resolveComponentFactory<any>(component);
      this.container.clear();
      this.component = this.container.createComponent(factory);
      this.component.instance.content = this.content;
      if (this.component.instance.outputEvent) {
        this.component.instance.outputEvent.subscribe(payload => {
          this.outputEvent.next(payload);
        });
      }
    }
  }
}
