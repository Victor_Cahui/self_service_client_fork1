import { Component, EventEmitter, HostBinding, Input, OnInit, Output } from '@angular/core';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';

@Component({
  selector: 'client-form-base',
  templateUrl: './form-base.component.html'
})
export class FormBaseComponent extends GaUnsubscribeBase implements OnInit {
  @HostBinding('attr.class') class = 'w-100';
  @Input() btnCancel = GeneralLang.Buttons.Cancel;
  @Input() btnCancelClass = 'w-50';
  @Input() btnConfirm = GeneralLang.Buttons.Confirm;
  @Input() btnConfirmClass = 'w-50';
  @Input() disableConfirm = false;
  @Input() iconClass: string;
  @Input() isVisibledBtnCancel = true;
  @Input() isVisibledBtnConfirm = true;
  @Input() policyType: string;
  @Input() showActions = true;
  @Input() showLoading: boolean;
  @Input() size = 'md';
  @Input() noticeBeforeConfirming;
  @Output() actionCancel: EventEmitter<any> = new EventEmitter<any>();
  @Output() actionConfirm: EventEmitter<any> = new EventEmitter<any>();

  constructor(protected gaService: GAService) {
    super(gaService);
  }

  ngOnInit(): void {
    if (this.policyType) {
      this.iconClass = POLICY_TYPES[this.policyType] ? POLICY_TYPES[this.policyType].icon : '';
    }
  }

  cancel(): void {
    this.actionCancel.emit();
  }

  confirm(): void {
    this.actionConfirm.emit();
  }
}
