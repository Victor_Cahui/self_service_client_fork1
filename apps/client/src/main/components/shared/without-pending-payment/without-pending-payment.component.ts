import { Component, OnInit } from '@angular/core';
import { NO_PENDING_PAYMENTS_ICON } from '@mx/settings/constants/images-values';
import { PaymentLang } from '@mx/settings/lang/payment.lang';

@Component({
  selector: 'client-without-pending-payment',
  templateUrl: './without-pending-payment.component.html',
  styles: []
})
export class WithoutPendingPaymentComponent implements OnInit {
  messageWithoutPendingPayment = PaymentLang.Messages.WithoutPendingPayment;
  noPendingPaymentsIcon = NO_PENDING_PAYMENTS_ICON;

  ngOnInit(): void {}
}
