import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { WithoutPendingPaymentComponent } from './without-pending-payment.component';

@NgModule({
  imports: [CommonModule],
  declarations: [WithoutPendingPaymentComponent],
  exports: [WithoutPendingPaymentComponent]
})
export class WithoutPendingPaymentModule {}
