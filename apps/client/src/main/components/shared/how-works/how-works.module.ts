import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfCustomAlertModule } from '@mx/core/ui/lib/components/alerts/custom-alert';
import { HowWorksComponent } from './how-works.component';

@NgModule({
  imports: [CommonModule, MfCustomAlertModule],
  exports: [HowWorksComponent],
  declarations: [HowWorksComponent]
})
export class HowWorksModule {}
