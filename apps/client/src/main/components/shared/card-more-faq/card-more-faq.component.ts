import { DOCUMENT } from '@angular/common';
import { Component, Inject, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { MFP_FAQS_11A } from '@mx/settings/constants/events.analytics';
import { Faq } from '@mx/statemanagement/models/general.models';
declare var SimpleBar: any;

@Component({
  selector: 'client-card-more-faq',
  templateUrl: './card-more-faq.component.html'
})
export class CardMoreFaqComponent extends GaUnsubscribeBase implements OnInit {
  @Input() faqList: Array<Faq>;
  @Input() faqGroup: Array<Array<Faq>>;
  @Input() faqGroupConfig: any;
  frmSelect: FormGroup;
  simpleBarModal: any;

  constructor(
    @Inject(DOCUMENT) private readonly document,
    protected formBuilder: FormBuilder,
    protected gaService?: GAService
  ) {
    super(gaService);
  }

  ngOnInit(): void {
    this.faqGroup && this._createFormSelection();
  }

  onChangeSelection(value): void {
    this.faqList = this.faqGroup[value];
  }

  expand(selected, collapse): void {
    // Cierra las demás
    if (!collapse) {
      this.faqList.map(faq => (faq.collapse = true));
    }
    selected.collapse = collapse;

    const scroll = this.document.getElementById('modal_scrollbar');
    if (scroll) {
      if (!this.simpleBarModal) {
        this.simpleBarModal = new SimpleBar(scroll);
      }
      this.simpleBarModal.recalculate();
    }
    this.addGAEvent(MFP_FAQS_11A(), { faqTitle: selected.title });
  }

  private _createFormSelection(): void {
    this.frmSelect = this.formBuilder.group({ select: new FormControl('0') });
    this.onChangeSelection(0);
  }
}
