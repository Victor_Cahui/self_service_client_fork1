import { ActivatedRoute, Router } from '@angular/router';
import { getDDMMYYYY, isExpired } from '@mx/core/shared/helpers/util/date';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { coerceBooleanProp } from '@mx/core/ui/common/helpers';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { GROUP_TYPE_ID_COMPANY } from '@mx/settings/auth/auth-values';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { LOCAL_STORAGE_KEYS } from '@mx/settings/constants/key-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { ILogguedUser } from '@mx/statemanagement/models/auth.interface';
import {
  IPoliciesByClientRequest,
  IPoliciesByClientResponse,
  IPolicyNotification,
  ISctrByClientResponse
} from '@mx/statemanagement/models/policy.interface';
import { IProfileBasicInfo } from '@mx/statemanagement/models/profile.interface';
import { ConfigurationBase } from '../configuration/configuration-base';
import { PolicyUtil } from '../utils/policy.util';

export class CardDetailPolicyBase extends ConfigurationBase {
  policyNumber: string;
  subTitle: string;
  hidenLinkMap: boolean;
  isEps: boolean;
  isEpsContractor: boolean;
  isContractor: boolean;
  isEnterprise: boolean;
  isPPFM: boolean;
  showPaymentCard: boolean;
  namePolice: string;
  policyNotification: IPolicyNotification;
  policyFound = false;
  showLoader = false;
  textNotFound = GeneralLang.Messages.ItemNotFound;
  mpKeys: any;
  info?: IProfileBasicInfo;
  titleHeadlineCard = GeneralLang.Titles.Headline;
  codCia: number;
  codRamo: number;
  numSpto: number;
  showLoadingCardHeadline = true;
  noContentCardHeadline: boolean;

  constructor(
    protected clientService: ClientService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    protected authService: AuthService,
    protected policiesService: PoliciesService,
    protected headerHelperService: HeaderHelperService,
    protected activatedRoute: ActivatedRoute,
    protected localStorageService: LocalStorageService,
    protected router: Router,
    protected isSctr: boolean = false,
    protected _Autoservicios: Autoservicios
  ) {
    super(clientService, policiesInfoService, configurationService, authService);

    this.activatedRoute.params.subscribe(params => {
      this.policyNumber = params.policyNumber;
      if (this.isSctr) {
        this.loadInfoDetailSctr();
      } else {
        this.loadInfoDetail();
      }
    });

    const data: ILogguedUser = this.localStorageService.getData(LOCAL_STORAGE_KEYS.USER);
    if (data && data.groupTypeId) {
      this.isEnterprise = +data.groupTypeId === GROUP_TYPE_ID_COMPANY;
    }
  }

  loadHeadlineData(): void {
    if (!this.isPPFM) {
      return;
    }

    const pathParams = {
      numeroPoliza: this.policyNumber
    };
    const queryParams = {
      codigoApp: APPLICATION_CODE,
      codCia: this.codCia
    };
    this._Autoservicios.ObtenerDatosTitular(pathParams, queryParams).subscribe(
      response =>
        response ? (this.info = this._formatTitularInfoCard(response)) : (this.noContentCardHeadline = true),
      error => {
        console.error(error);
      },
      () => (this.showLoadingCardHeadline = false)
    );
  }

  protected _formatTitularInfoCard(response: any): IProfileBasicInfo {
    const birthDay = getDDMMYYYY(response.fechaNacimiento);
    const registrationDate = getDDMMYYYY(response.clienteDesde);

    return {
      fullName: response.nombreCompleto,
      typeDocument: response.tipoDocumento,
      document: response.documento,
      phone: response.telefono,
      birthDay,
      registrationDate,
      email: response.correo,
      payload: response
    };
  }

  setSubTitle(): void {
    this.subTitle = POLICY_TYPES[this.policyType].title || '';
  }

  // Polizas (excepto SCTR)
  loadInfoDetail(): void {
    const clientPolicy = this.policiesInfoService.getClientPolicy(this.policyNumber);
    this.mpKeys = this.policiesInfoService.getMPKeys();
    if (!clientPolicy) {
      this.setClearInfoDetail();
      this.showLoader = true;
      this.policiesService
        .searchByClientService({
          codigoApp: APPLICATION_CODE,
          numeroPoliza: this.policyNumber
        } as IPoliciesByClientRequest)
        .subscribe((response: Array<IPoliciesByClientResponse>) => {
          this.policyFound = response && response.length > 0;
          if (this.policyFound) {
            const clientPolicyResponse = response[0];
            this.policiesInfoService.setClientPolicyResponse(clientPolicyResponse);
            this.mpKeys = this.policiesInfoService.getMPKeys();
            this.setInfoDetail(clientPolicyResponse);
          } else {
            this.setSubTitle();
            this.namePolice = this.subTitle.toUpperCase();
            this.subTitle = '';
            this.setHeader();
          }
          this.showLoader = false;
        });
    } else {
      this.policyFound = true;
      this.setInfoDetail(clientPolicy);
    }
  }

  setInfoDetail(data: IPoliciesByClientResponse): void {
    this.policiesService.setPolicyBaseComponent(data);
    this.policyType = data.tipoPoliza;
    this.isEps = PolicyUtil.isEps(this.policyType);
    this.isEpsContractor = this.isEps && !!data.esContratante;
    this.isContractor = data.esContratante;
    this.isPPFM = data.esBeneficioAdicional;
    this.codCia = data.codCia;
    this.codRamo = data.codRamo;
    this.numSpto = data.numSpto;
    this.showPaymentCard = !this.isEps && !this.isPPFM;
    this.setSubTitle();
    this.namePolice = data.descripcionPoliza.toUpperCase();
    this.setHeader();
    // Data para Pagos
    this.policiesInfoService.setPaymentPolicy({
      insuranceCost: data.prima,
      moneyCode: StringUtil.getMoneyDescription(data.codigoMoneda),
      automaticRenewal: data.tieneRenovacionAutomatica,
      renewalType: data.tipoRenovacionDescripcion,
      renewalTypeId: data.tipoRenovacionId,
      paymentType: data.tipoPagoDescripcion,
      paymentTypeId: data.tipoPagoId,
      quotas: data.cuotas,
      quotaNumber: data.numeroCuota,
      amountDue: data.montoCuota,
      hasPendingPayment: coerceBooleanProp(data.tienePagoPendiente),
      policyType: data.tipoPoliza
    });
    // Data General
    this.policiesInfoService.setPolicyBasicInfo({
      coveragesPpfm: data.coberturasPpfm,
      policyType: data.tipoPoliza,
      policyName: data.descripcionPoliza,
      policyNumber: data.numeroPoliza,
      insuredNumber: (data.beneficiarios && data.beneficiarios.length) || 0,
      sumAssured: data.sumaAsegurada,
      moneyCode: StringUtil.getMoneyDescription(data.codigoMonedaSumaAsegurada),
      startDateValidity: getDDMMYYYY(data.fechaInicio),
      endValidityDate: data.fechaFin ? getDDMMYYYY(data.fechaFin) : null,
      planEps: data.planEps
    });

    this.hidenLinkMap = data.tipoPoliza === POLICY_TYPES.MD_EPS.code ? false : isExpired(data.fechaFin);
    this.loadConfiguration();
    this.loadHeadlineData();
    // this.doExtraAsyncActions();
  }

  setHeader(): void {
    this.headerHelperService.set({
      title: this.namePolice,
      subTitle: this.subTitle,
      icon: '',
      back: true
    });
  }

  setPolicyNotification(notification: IPolicyNotification): void {
    this.policyNotification = notification;
  }

  setClearInfoDetail(): void {
    this.policiesInfoService.setPaymentPolicy(null);
    this.policiesInfoService.setPolicyBasicInfo(null);
  }

  // SCTR
  loadInfoDetailSctr(): void {
    const clientPolicy = this.policiesInfoService.getClientSctrPolicy(this.policyNumber);
    this.mpKeys = this.policiesInfoService.getMPKeys();
    if (!clientPolicy) {
      this.setClearInfoDetail();
      this.showLoader = true;
      this.policiesService
        .sctrSearchByClient({
          codigoApp: APPLICATION_CODE,
          numeroPoliza: this.policyNumber
        } as IPoliciesByClientRequest)
        .subscribe((response: Array<ISctrByClientResponse>) => {
          this.policyFound = response && response.length > 0;
          if (this.policyFound) {
            const clientPolicyResponse = response[0];
            this.policiesInfoService.setClientSctrPolicyResponse(clientPolicyResponse);
            this.mpKeys = this.policiesInfoService.getMPKeys();
            this.setInfoDetailSctr(clientPolicyResponse);
          }
          this.showLoader = false;
        });
    } else {
      this.policyFound = true;
      this.setInfoDetailSctr(clientPolicy);
    }
  }

  setInfoDetailSctr(data: ISctrByClientResponse): void {
    this.policyType = data.tipoPoliza;
    this.setSubTitle();
    this.namePolice = data.descripcionPoliza.toUpperCase();
    this.isContractor = data.esContratante;
    this.codCia = data.codCia;
    this.headerHelperService.set({
      title: this.namePolice,
      subTitle: this.subTitle,
      icon: '',
      back: true
    });
    this.policiesInfoService.setPaymentPolicy({
      insuranceCost: data.prima,
      moneyCode: StringUtil.getMoneyDescription(data.codigoMoneda),
      automaticRenewal: true,
      paymentType: data.tipoPagoDescripcion,
      paymentTypeId: data.tipoPagoId,
      amountDue: data.montoPlanilla,
      amountMax: data.montoTopado,
      hasPendingPayment: false,
      policyType: data.tipoPoliza,
      frequencyStatement: data.frecuenciaDeclaracion,
      numberInsured: data.aseguradosDeclaracion,
      fractionationDesc: data.descripcionFraccionamiento
    });
    this.policiesInfoService.setPolicyBasicInfo({
      policyType: data.tipoPoliza,
      policyName: data.descripcionPoliza,
      policyNumber: data.numeroPoliza,
      sumAssured: data.sumaAsegurada,
      moneyCode: StringUtil.getMoneyDescription(data.codigoMonedaSumaAsegurada),
      startDateValidity: getDDMMYYYY(data.fechaInicio),
      endValidityDate: data.fechaFin ? getDDMMYYYY(data.fechaFin) : null,
      activityRisk: data.actividadesRiesgo,
      frequencyStatement: data.frecuenciaDeclaracion || ''
    });

    this.loadConfiguration();
  }

  // Va a configuracion de pagos
  goPaymentSetting(): void {
    this.router.navigate([`/payments/settings/${this.policyNumber}`], { preserveQueryParams: true });
  }

  // Va a lista de pagos
  goPayment(policyNumber: string): void {
    this.router.navigate([`/payments/list/${this.policyType}/${policyNumber}`]);
  }

  doExtraAsyncActions(): void {}
}
