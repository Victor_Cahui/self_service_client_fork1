import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DirectivesModule, MfCardModule, MfInputModule, MfShowErrorsModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { CardSctrPeriodSummaryBillingModule } from './card-sctr-period-summary-billing/card-sctr-period-summary-billing.module';
import { CardSctrPeriodSummaryTableModule } from './card-sctr-period-summary-table/card-sctr-period-summary-table.module';

import { CardSctrPeriodSummaryComponent } from './card-sctr-period-summary.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MfCardModule,
    DirectivesModule,
    MfInputModule,
    MfShowErrorsModule,
    MfLoaderModule,
    CardSctrPeriodSummaryBillingModule,
    CardSctrPeriodSummaryTableModule
  ],
  declarations: [CardSctrPeriodSummaryComponent],
  exports: [CardSctrPeriodSummaryComponent]
})
export class CardSctrPeriodSummaryModule {}
