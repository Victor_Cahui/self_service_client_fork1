import { CurrencyPipe, DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { FORMAT_DATE_DDMMYYYY, REG_EX } from '@mx/settings/constants/general-values';
import { XLS_ICON } from '@mx/settings/constants/images-values';
import { PERIODS_LANAG } from '@mx/settings/lang/sctr.lang';
import {
  ICardOperationSummary,
  IOperationSummaryRisk,
  IOperationSummaryRiskDetail
} from '@mx/statemanagement/models/sctr.interface';
import { isEmpty } from 'lodash-es';

@Component({
  selector: 'client-card-sctr-period-summary',
  templateUrl: './card-sctr-period-summary.component.html',
  providers: [CurrencyPipe, DatePipe]
})
export class CardSctrPeriodSummaryComponent implements OnInit {
  @Input() isDeclaration: boolean;
  @Input() movement: number;

  @Output() showSummary: EventEmitter<boolean> = new EventEmitter<boolean>();

  lang = PERIODS_LANAG;
  xlsIcon = XLS_ICON;
  showLoading = true;
  showLoadingOperation: boolean;
  canContinue: boolean;
  isBilled: boolean;
  showSpinnerPayroll: boolean;
  formAditional: FormGroup;
  summary: ICardOperationSummary;

  constructor(
    protected currencyPipe: CurrencyPipe,
    protected datePipe: DatePipe,
    protected authService: AuthService,
    protected _Autoservicios: Autoservicios,
    private readonly storageService: StorageService,
    private readonly formBuilder: FormBuilder,
    private readonly router: Router
  ) {}

  ngOnInit(): void {
    this.formAditional = this.formBuilder.group({
      jobCenter: ['', Validators.pattern(REG_EX.alphaNumericText)],
      costCenter: ['', Validators.pattern(REG_EX.alphaNumericText)]
    });
    this.getSummary();
  }

  getSummary(): void {
    const numMovimiento = this.movement;
    const summaryService$ = this.isDeclaration
      ? this._Autoservicios.ListarDeclaracionResumenSCTR({ numMovimiento })
      : this._Autoservicios.ListarInclusionResumenSCTR({ numMovimiento });

    summaryService$.subscribe(this._mapSummary.bind(this), () => {
      this.showSummary.emit(false);
      this.showLoading = false;
    });
  }

  // Se generara la declaración o inclusión
  generateOperation(): void {
    const auth = this.authService.retrieveEntity();
    const queryReq = {
      tipoDocumento: auth.type,
      documento: auth.number,
      numMovimiento: this.movement
    };
    const bodyReq = {
      informacionOperacion: {
        centroTrabajo: this.formAditional.controls['jobCenter'].value,
        centroCosto: this.formAditional.controls['costCenter'].value
      },
      incluirFacturacion: this.isBilled
    };
    this.showLoadingOperation = true;
    this.canContinue = false;

    const operationService$ = this.isDeclaration
      ? this._Autoservicios.GenerarDeclaracionSCTR(queryReq, bodyReq)
      : this._Autoservicios.GenerarInclusionSCTR(queryReq, bodyReq);

    operationService$.subscribe(
      response => {
        if (!response) {
          return void 0;
        }
        const operation = {
          fechaOperacion: this.datePipe.transform(response.fechaOperacion, FORMAT_DATE_DDMMYYYY),
          numConstancia: `${response.numConstancia}`,
          totalTrabajadores: `${response.totalTrabajadores} ${
            response.totalTrabajadores === 1 ? 'trabajador' : 'trabajadores'
          }`
        };
        this.storageService.setItem('SCTR_OPERATION_SUCCESS', operation);
        this.router.navigate([`${this.router.url}/confirm`]);
      },
      () => {
        this.showLoadingOperation = false;
        this.canContinue = true;
      },
      () => {
        this.showLoadingOperation = false;
      }
    );
  }

  onPaste(event: any, control: string): void {
    let clipboard = event.clipboardData.getData('text/plain');
    clipboard = clipboard.replace(/<[^>]*>/g, '');
    setTimeout(() => {
      this.formAditional.controls[control].setValue(clipboard);
    });
  }

  downloadPendingPayroll(): void {}

  private _mapSummary(response: any): void {
    if (!response || isEmpty(response || response.status !== 200)) {
      this.showSummary.emit(false);
      this.showLoading = false;

      return void 0;
    }

    const info = response.informacionOperacion;
    const riskDetailList = response.detalleRiesgos.map(this._mapRiskDetail.bind(this));
    const pendingPayroll =
      response.planillaPendiente && response.planillaPendiente.detalle
        ? {
            billingPeriod: response.planillaPendiente.periodoFacturacion,
            pendingPayrollList: response.planillaPendiente.detalle.map(this._mapRiskDetail.bind(this))
          }
        : null;
    this.summary = { info, riskDetailList, ...(pendingPayroll && { pendingPayroll }) };
    this.formAditional.controls['costCenter'].setValue(info.centroCosto);
    this.formAditional.controls['jobCenter'].setValue('');
    this.showLoading = false;
    this.canContinue = true;
  }

  private _mapRiskDetail(riskDetail: any): IOperationSummaryRiskDetail {
    const title = riskDetail.titulo;
    const currencyCode = `${StringUtil.getMoneyDescription(riskDetail.codMoneda)} `;
    const topSalary = `${this.lang.TopSalary}: ${this.currencyPipe.transform(riskDetail.sueldoTope, currencyCode)}`;
    const showTopSalary = !!riskDetail.sueldoTope;
    const vigencyDate = this.datePipe.transform(riskDetail.fechaFinVigencia, FORMAT_DATE_DDMMYYYY);
    const vigency = `${this.lang.Vigency}: ${vigencyDate}`;
    const totalWorkers = `${riskDetail.totalTrabajadores}`;
    const totalPayroll = `${this.currencyPipe.transform(riskDetail.totalPlanilla, currencyCode)}`;
    const totalBonus = `${this.currencyPipe.transform(riskDetail.totalPrima, currencyCode)}`;
    const riskList = riskDetail.riesgos.map(this._mapRiskList.bind(this, currencyCode));

    return { title, topSalary, showTopSalary, vigency, totalWorkers, totalPayroll, totalBonus, riskList };
  }

  private _mapRiskList(currencyCode: string, risk: any): IOperationSummaryRisk {
    const number = `${risk.numRiesgo}`;
    const categry = risk.descRiesgo;
    const workersQty = `${risk.nroTrabajadores}`;
    const payroll = this.currencyPipe.transform(risk.montoPlanilla, currencyCode);
    const bonus = this.currencyPipe.transform(risk.montoPrima, currencyCode);

    return { number, categry, workersQty, payroll, bonus };
  }
}
