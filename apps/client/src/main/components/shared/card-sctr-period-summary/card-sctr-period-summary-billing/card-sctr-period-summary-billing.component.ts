import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UnsubscribeOnDestroy } from '@mx/core/shared/helpers/util/unsubscribe-on-destroy';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { GeneralService } from '@mx/services/general/general.service';
import { PERIODS_LANAG } from '@mx/settings/lang/sctr.lang';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'client-card-sctr-period-summary-billing',
  templateUrl: 'card-sctr-period-summary-billing.component.html'
})
export class CardSctrPeriodSummaryBillingComponent extends UnsubscribeOnDestroy implements OnInit {
  @ViewChild('nonBilled') nonBilled: ElementRef;

  @Input() movement: number;
  @Output() isBilled: EventEmitter<boolean> = new EventEmitter<boolean>();

  lang = PERIODS_LANAG;
  showLoading = true;
  canSelectNonBilling: boolean;
  tooltip = '';
  frm: FormGroup;

  constructor(
    protected _Autoservicios: Autoservicios,
    protected generalService: GeneralService,
    protected formBuilder: FormBuilder
  ) {
    super();
  }

  ngOnInit(): void {
    this.getBillingInfo();
    this.initForm();
  }

  getBillingInfo(): void {
    const queryReq = { numMovimiento: this.movement };
    this.showLoading = true;
    this._Autoservicios.InclusionFacturacionSCTR(queryReq).subscribe(
      (res: any) => {
        if (!res) {
          return void 0;
        }
        this.canSelectNonBilling = res.puedeNoFacturar;
        if (!this.canSelectNonBilling) {
          this.setTooltip(res.llaveMensaje);
          setTimeout(() => (this.nonBilled.nativeElement.disabled = !this.canSelectNonBilling));
        }
      },
      () => (this.showLoading = false),
      () => (this.showLoading = false)
    );
  }

  setTooltip(key: string): void {
    this.generalService
      .getParametersSubject()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(() => (this.tooltip = this.generalService.getValueParams(key)));
  }

  initForm(): void {
    this.frm = this.formBuilder.group({
      billed: ['', []]
    });
    this.frm.valueChanges.subscribe(values => {
      this.isBilled.emit(values.billed);
    });
    this.frm.controls['billed'].setValue(true);
  }
}
