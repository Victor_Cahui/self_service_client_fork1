import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CardSctrPeriodSummaryTableComponent } from './card-sctr-period-summary-table.component';

@NgModule({
  imports: [CommonModule],
  declarations: [CardSctrPeriodSummaryTableComponent],
  exports: [CardSctrPeriodSummaryTableComponent]
})
export class CardSctrPeriodSummaryTableModule {}
