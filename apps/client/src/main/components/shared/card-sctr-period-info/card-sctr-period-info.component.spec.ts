import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HELMET_OUT_ICON } from '@mx/settings/constants/images-values';
import { ISctrPeriodInfoCard } from '@mx/statemanagement/models/policy.interface';

import { CardSctrPeriodInfoComponent } from './card-sctr-period-info.component';

describe('CardSctrPeriodInfoComponent', () => {
  let component: CardSctrPeriodInfoComponent;
  let fixture: ComponentFixture<CardSctrPeriodInfoComponent>;

  const periodInfoMock: ISctrPeriodInfoCard = {
    title: 'SCTR SALUD: 7022000001086',
    insuredGroup: 'HUANUCO',
    perdiod: 'Periodo de inclusión: SEPTIEMBRE 2020',
    start: 'Inicio de periodo: 29/09/2020',
    end: 'Fin de periodo: 31/10/2020'
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CardSctrPeriodInfoComponent]
    });

    fixture = TestBed.createComponent(CardSctrPeriodInfoComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    component.periodInfo = periodInfoMock;
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('to validate component properties', () => {
    component.periodInfo = periodInfoMock;
    expect(component.periodInfo).toBeDefined();
    expect(component.fileIcon).toEqual(HELMET_OUT_ICON);
  });
});
