import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IconPaymentTypeComponent } from './icon-payment-type.component';

@NgModule({
  imports: [CommonModule],
  declarations: [IconPaymentTypeComponent],
  exports: [IconPaymentTypeComponent]
})
export class IconPaymentTypeModule {}
