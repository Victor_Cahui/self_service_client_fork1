import { CurrencyPipe, DecimalPipe } from '@angular/common';
import { OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { PaymentMode } from '@mx/components/shared/utils/payment';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { HeaderHelperService, IHeader } from '@mx/services/general/header-helper.service';
import { PaymentService } from '@mx/services/payment.service';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { PAYMENTS_HEADER, SOAT_ELECT_HEADER } from '@mx/settings/constants/router-titles';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import {
  ConfirmBankPaymentSoatLang,
  ConfirmBankPaymentVehicleLang,
  ConfirmCardPaymentFreeSoatLang,
  ConfirmCardPaymentFreeVehicleLang,
  ConfirmCardPaymentLang,
  ConfirmCardPaymentSoatLang,
  ConfirmCardPaymentVehicleLang,
  MethodPaymentLang,
  PaymentLang
} from '@mx/settings/lang/payment.lang';
import { Content } from '@mx/statemanagement/models/general.models';
import { IPaymentQuoteCard } from '@mx/statemanagement/models/payment.models';
import { Subscription } from 'rxjs';

export abstract class PaymentQuotaBase extends GaUnsubscribeBase implements OnDestroy {
  header: IHeader;
  isSoat: boolean;
  quote: IPaymentQuoteCard;
  content: Content;
  paymentNumber: string;
  documentData: string;
  paymentServiceSub: Subscription;
  vigencyDateSub: Subscription;
  useMapfreDollarsSub: Subscription;
  insuredTypes = GeneralLang.Titles.InsuredTypes;

  constructor(
    protected headerHelperService: HeaderHelperService,
    protected paymentService: PaymentService,
    protected router: Router,
    protected decimalPipe: DecimalPipe,
    protected currencyPipe: CurrencyPipe,
    protected gaService: GAService
  ) {
    super(gaService);
    this.header = {
      title: '',
      subTitle: '',
      back: true
    };
  }

  ngOnDestroy(): void {
    if (this.paymentServiceSub) {
      this.paymentServiceSub.unsubscribe();
    }
    if (this.vigencyDateSub) {
      this.vigencyDateSub.unsubscribe();
    }
    if (this.useMapfreDollarsSub) {
      this.useMapfreDollarsSub.unsubscribe();
    }
  }

  // Encabezado para pagos y SOAT
  setHeader(): void {
    this.isSoat = this.quote.policyType && this.quote.policyType.startsWith(POLICY_TYPES.MD_SOAT.code);
    this.header.title = this.isSoat ? SOAT_ELECT_HEADER.title : MethodPaymentLang.Header.payQuote;
    this.header.subTitle = this.quote.comesSoat ? POLICY_TYPES.MD_SOAT.title : PAYMENTS_HEADER.title;
    this.header.back = !this.quote.confirm;
    this.headerHelperService.set(this.header);
  }

  getCurrentQuote(): void {
    this.quote = this.paymentService.getCurrentQuote();
  }

  // Configurar Vista
  setView(vehicle?: boolean): void {
    if (this.quote) {
      const total = this.quote.total;
      const quota = this.quote.quota;
      const totalFormat = this.formatValue(total);
      this.documentData = `${this.quote.documentType || ''} ${this.quote.document || ''}`;
      // SOAT o Compra de Vehiculo
      const isSoat = this.quote.policyType.startsWith(POLICY_TYPES.MD_SOAT.code);
      if ((isSoat && !this.quote.billNumber) || vehicle) {
        // Contenido según tipo de pago
        switch (this.quote.paymentMode) {
          case PaymentMode.FREE:
            this.content = isSoat ? ConfirmCardPaymentFreeSoatLang : ConfirmCardPaymentFreeVehicleLang;
            break;
          case PaymentMode.CARD:
            this.content = isSoat ? ConfirmCardPaymentSoatLang : ConfirmCardPaymentVehicleLang;
            break;
          default:
            this.paymentNumber = this.quote.billNumber;
            this.content = isSoat ? ConfirmBankPaymentSoatLang : ConfirmBankPaymentVehicleLang;
            break;
        }
        this.content.amountText =
          total === 0
            ? PaymentLang.Texts.Free.toUpperCase()
            : vehicle
            ? quota
            : `${this.quote.currencySymbol} ${totalFormat}`;
        this.content.warning = total === 0 ? '' : this.content.warning;
      } else {
        this.content = ConfirmCardPaymentLang;
        this.content.amountText = this.quote.totalAmount;
        this.content.confirmFormData = this.quote.confirmFormData;
      }
      this.content.quote = this.quote;
      this.content.isSoat = this.isSoat;
      this.content.emailText = this.quote.email;
      // Descuento con Mapfre Dollars
      const dataQuote = vehicle ? this.quote.dataQuoteVehicle : this.quote.dataQuote;
      this.content.table = this.quote.withMPD
        ? {
            title: PaymentLang.Labels.PaymentDetail.title,
            rows: [
              {
                label: PaymentLang.Labels.PaymentDetail.netPrice.replace('{{policy}}', this.quote.policy),
                amount: `${this.quote.currencySymbol} ${this.formatValue(dataQuote.cotizacionConMPD.monto)}`
              },
              {
                label: PaymentLang.Labels.PaymentDetail.savingMafreDollars,
                amount: `- ${this.quote.currencySymbol} ${this.formatValue(dataQuote.cotizacionConMPD.montoAhorro)}`
              },
              {
                label: vehicle
                  ? PaymentLang.Labels.PaymentDetail.totalPolicyPrice
                  : PaymentLang.Labels.PaymentDetail.total,
                amount: `${this.quote.currencySymbol} ${totalFormat}`,
                bold: vehicle
              }
            ]
          }
        : null;
      if (this.quote.withMPD && vehicle) {
        this.content.table.rows.push({
          label: PaymentLang.Labels.PaymentDetail.totalQuotaPrice,
          amount: quota,
          bold: true
        });
      }
    }
  }

  formatValue(value: number): string {
    return this.decimalPipe.transform(value, '1.2-2');
  }
}
