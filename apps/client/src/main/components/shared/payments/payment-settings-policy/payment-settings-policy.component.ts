import { CurrencyPipe } from '@angular/common';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { BaseCardMyPolicies } from '@mx/components/card-my-policies/base-card-my-policies';
import { gaEventDecorator, GAService } from '@mx/core/shared/helpers/util/google';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { MfModalAlertComponent } from '@mx/core/ui/lib/components/modals/modal-alert/modal-alert.component';
import { GeneralService } from '@mx/services/general/general.service';
import { InitModalService } from '@mx/services/general/init-modal.service';
import { PaymentService } from '@mx/services/payment.service';
import { PoliciesService } from '@mx/services/policies.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { PORCENTAGE_ICON, WARNING_ICON } from '@mx/settings/constants/images-values';
import { InsurancePrime, PAYMENT_SETTINGS_RENEWAL, PaymentLang } from '@mx/settings/lang/payment.lang';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import { IPaymentConfigurationResponse } from '@mx/statemanagement/models/payment.models';

@Component({
  selector: 'client-payment-settings-policy',
  templateUrl: './payment-settings-policy.component.html',
  providers: [CurrencyPipe]
})
export class PaymentSettingsPolicyComponent extends BaseCardMyPolicies implements OnInit {
  @ViewChild(MfModalAlertComponent) mfModalAlert: MfModalAlertComponent;

  @Input() payment: IPaymentConfigurationResponse;
  @Input() canEdit: boolean;

  renewalLang = PAYMENT_SETTINGS_RENEWAL;
  porcentageIcon = PORCENTAGE_ICON;

  primeAmount: string;
  primeTooltip = InsurancePrime;

  modalTitle = PolicyLang.Labels.CancelAutomaticDebt;
  modalSuggestion = PolicyLang.Messages.PaymentSuggestion;
  btnConfirm = PolicyLang.Buttons.AutomaticDebit.confirmButtonText;
  btnCancel = PolicyLang.Buttons.AutomaticDebit.cancelButtonText;
  iconModal = WARNING_ICON;
  flagConfirmModal: boolean;
  useDCC: boolean;
  alertIconModal: string;

  constructor(
    protected generalService: GeneralService,
    protected policiesService: PoliciesService,
    private readonly currencyPipe: CurrencyPipe,
    private readonly router: Router,
    private readonly initModalService: InitModalService,
    private readonly _Autoservicios: Autoservicios,
    protected paymentService: PaymentService,
    protected gaService?: GAService
  ) {
    super(generalService, policiesService, gaService);
  }

  ngOnInit(): void {
    this.alertIconModal = 'Phone Alert';
    this.flagConfirmModal = true;
    if (this.payment) {
      const symbol = `${StringUtil.getMoneyDescription(this.payment.codigoMoneda)} `;
      this.primeAmount = this.currencyPipe.transform(this.payment.prima, symbol);
    }

    if (this.payment.tipoRenovacionId === 1) {
      this.useDCC = true;
    }
  }

  @gaEventDecorator()
  goToEdit(payment: IPaymentConfigurationResponse): void {
    const path = `${this.router.url}/${payment.numeroPoliza}/edit`;
    this.router.navigate([path]);
  }

  @gaEventDecorator()
  goToAssociate(payment: IPaymentConfigurationResponse): void {
    const path = `${this.router.url}/${payment.numeroPoliza}/associate`;
    this.router.navigate([path]);
  }

  doAction(event): void {
    if (this.payment.puedeDesafiliarDebito) {
      this.cancelAutomaticDebt(event);
    } else {
      if (this.payment.tipoRenovacionId === 1) {
        this.useDCC = true;
      }
    }
  }

  cancelAutomaticDebt(event): void {
    if (event) {
      this.disenrollCard();
    } else {
      if (this.payment.tipoRenovacionId === 1) {
        this.useDCC = true;
      }
    }
  }

  changeCheckBox(event): void {
    if (event) {
      this.goToAssociate(this.payment);
    } else {
      if (this.payment.puedeDesafiliarDebito) {
        this.modalTitle = PolicyLang.Labels.CancelAutomaticDebt;
        this.modalSuggestion = PolicyLang.Messages.PaymentSuggestion;
        this.mfModalAlert.open();
      } else {
        this.modalTitle = PolicyLang.Labels.ActionNotAllowed;
        this.modalSuggestion = PolicyLang.Messages.NotAllowSuggestion;
        this.flagConfirmModal = false;
        this.mfModalAlert.open();
        this.useDCC = true;
      }
    }
  }

  disenrollCard(): void {
    const queryReq = {
      codigoApp: APPLICATION_CODE,
      codCia: this.payment.codCia,
      numeroPoliza: this.payment.numeroPoliza
    };

    this._Autoservicios
      .DesafiliarTarjeta(queryReq)
      .toPromise()
      .then(response => {
        if (response) {
          this.useDCC = false;
          this.paymentService.notificactionForPS.next({ value: true, text: PaymentLang.Texts.DisableDebitCard });
        }
      })
      .catch(error => {
        this.useDCC = true;
        console.error(error);
      });
  }

  closeModal(): void {
    this.initModalService.emitCloseModalEvent();
  }
}
