import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DirectivesModule, MfCardModule, SwitchModule } from '@mx/core/ui';
import { CardQuoteSoatComponent } from './card-quote-soat.component';

@NgModule({
  imports: [CommonModule, MfCardModule, DirectivesModule, SwitchModule],
  declarations: [CardQuoteSoatComponent],
  exports: [CardQuoteSoatComponent]
})
export class CardQuoteSoatModule {}
