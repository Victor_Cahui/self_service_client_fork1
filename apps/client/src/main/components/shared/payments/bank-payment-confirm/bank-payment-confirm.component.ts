import { HttpErrorResponse } from '@angular/common/http';
import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { PaymentMode } from '@mx/components/shared/utils/payment';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { NotificationStatus } from '@mx/core/ui/lib/components/alerts/custom-alert/custom-alert.component';
import { MfModalAlertComponent } from '@mx/core/ui/lib/components/modals/modal-alert/modal-alert.component';
import { PaymentService } from '@mx/services/payment.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { BANK_PAYMENT_ICON, IMAGE_CARDS } from '@mx/settings/constants/images-values';
import { PaymentLang } from '@mx/settings/lang/payment.lang';
import { Content } from '@mx/statemanagement/models/general.models';

@Component({
  selector: 'client-bank-payment-confirm',
  templateUrl: './bank-payment-confirm.component.html'
})
export class BankPaymentConfirmComponent implements OnInit, AfterViewInit {
  @Input() content: Content;
  @Input() paymentNumber: string;
  @Input() documentData: string;
  @ViewChild('checkBoxAddress') checkBoxAddress: any;
  labelBankPaymentCode = PaymentLang.Labels.BankPaymentCode;
  labelDocument = PaymentLang.Labels.PaymentDocument;
  digitalPlatform = PaymentLang.Labels.DigitalPlatform;
  autoDebitTitle = 'Registra esta tarjeta al pago automático';
  autoDebitDesc = 'Podrás ahorrar tiempo y no volverte a preocupar por fechas limite';
  titleModal1: string;
  descrModal1: string;
  autoDebitCounter = 0;
  withImageModal = true;
  sizeModal = 'g-modal--medium';
  outlineModal = false;
  selectedAutomaticDebit = false;
  disableAutomaticDebit = false;
  quote: any;
  status = NotificationStatus.INFO;

  @ViewChild(MfModalAlertComponent) mfModalAlert: MfModalAlertComponent;

  constructor(
    private readonly router: Router,
    private readonly _Autoservicios: Autoservicios,
    private readonly paymentService: PaymentService
  ) {}

  ngOnInit(): void {
    if (!this.content.icon) {
      this.content.icon = BANK_PAYMENT_ICON;
    }
  }

  ngAfterViewInit(): any {
    if (this.content.quote.response) {
      this.selectedAutomaticDebit = this.content.quote.response.tienePagoAutomatico;
      if (this.checkBoxAddress) {
        this.checkBoxAddress.checked = this.selectedAutomaticDebit;
      }
    }
  }

  showCardAfiliation(): boolean {
    const paymentMode = this.content.quote.response
      ? this.content.quote.response.modalidadPago
      : this.content.quote.paymentMode;
    const canMakeAutomaticPayment = this.content.quote.canMakeAutomaticPayment;
    const hasPendingPayments = this.content.quote.hasPendingPayments;
    const doesNotHaveAutomaticDebit =
      this.content.quote.response && this.content.quote.response.recibo
        ? this.content.quote.response.recibo.tiposPagosLlave !== 'TP_DEBITO_AUTOMATICO'
        : false;
    if (this.content.confirmFormData) {
      return (
        this.content.confirmFormData.chbSaveCard &&
        !this.content.confirmFormData.chbAutoDebit &&
        canMakeAutomaticPayment &&
        hasPendingPayments &&
        doesNotHaveAutomaticDebit
      );
    }

    return paymentMode === PaymentMode.TOKN && doesNotHaveAutomaticDebit;
  }

  goToAction(): void {
    if (this.content.action.routeLink) {
      this.router.navigate([this.content.action.routeLink]);
    }
  }

  getImageCardByType(type: string): string {
    switch (type) {
      case 'VISA':
      case 'VISA_ELECTRON':
      case 'VISA_DEBIT':
        return IMAGE_CARDS.VISA;
      case 'MASTERCARD':
      case 'MASTERCARD_DEBIT':
        return IMAGE_CARDS.MASTER_CARD;
      case 'DINERS_CLUB':
      case 'DINERS':
        return IMAGE_CARDS.DINNER_CLUB;
      case 'AMERICAN_EXPRESS':
      case 'AMERICAN_EXPRESS_AMEX':
      case 'AMERICAN EXPRESS':
      case 'AMEX':
        return IMAGE_CARDS.AMERICAN_EXPRESS;
      default:
        return '';
    }
  }

  changeCheckBox(event): void {
    this.disableAutomaticDebit = true;
    const titleOn = 'Débito automático activado';
    const descOn = '¡Muy bien! Desde ahora podrás disfrutar de la comodidad de este servicio.';
    const titleOff = 'Registra esta tarjeta al pago automático';
    const descOff = 'Podrás ahorrar tiempo y no volverte a preocupar por fechas limite';
    if (event) {
      const body = {
        tarjetaToken: this.content.confirmFormData.tokenCard,
        codCia: this.content.quote.response.recibo.codCia,
        numeroPoliza: this.content.quote.response.recibo.numeroPoliza
      };
      this._Autoservicios.AfiliarTarjeta({ codigoApp: APPLICATION_CODE }, body).subscribe(
        () => {
          this.disableAutomaticDebit = false;
          this.autoDebitTitle = titleOn;
          this.autoDebitDesc = descOn;
          this.selectedAutomaticDebit = true;
          this.checkBoxAddress.checked = this.selectedAutomaticDebit;
          this.quote = this.content.quote;
          this.quote.response.tienePagoAutomatico = true;
          this.paymentService.setCurrentQuote(this.quote);
        },
        (error: HttpErrorResponse) => {
          this.disableAutomaticDebit = false;
          this.selectedAutomaticDebit = false;
          this.checkBoxAddress.checked = this.selectedAutomaticDebit;
        }
      );
    } else {
      const params = {
        codigoApp: APPLICATION_CODE,
        tarjetaToken: this.content.confirmFormData.tokenCard,
        codCia: this.content.quote.response.recibo.codCia,
        numeroPoliza: this.content.quote.response.recibo.numeroPoliza
      };
      this._Autoservicios.DesafiliarTarjeta(params).subscribe(
        () => {
          this.disableAutomaticDebit = false;
          this.autoDebitTitle = titleOff;
          this.autoDebitDesc = descOff;
          this.selectedAutomaticDebit = false;
          this.checkBoxAddress.checked = this.selectedAutomaticDebit;
          this.quote = this.content.quote;
          this.quote.response.tienePagoAutomatico = false;
          this.paymentService.setCurrentQuote(this.quote);
        },
        (error: HttpErrorResponse) => {
          this.disableAutomaticDebit = false;
          this.selectedAutomaticDebit = true;
          this.checkBoxAddress.checked = this.selectedAutomaticDebit;
        }
      );
    }
  }

  confirmModal(event): void {
    this.mfModalAlert.close();
  }

  showTermsAndConditions(): void {
    this.titleModal1 = 'TÉRMINOS Y CONDICIONES DE LA AFILIACIÓN AL PAGO AUTOMÁTICO';
    this.descrModal1 = `
    <ul class="text-left g-list-bullet li-red">
      <li class="g-font--13 g-pl--5 g-font--light mb-2">Los cargos se realizarán de acuerdo a su cronograma de pagos.</li>
      <li class="g-font--13 g-pl--5 g-font--light mb-2">Si el cliente cuenta con cuotas atrasadas, <strong>MAPFRE Perú</strong> o <strong>MAPFRE Perú Vida</strong> podrán realizar tantos intentos de cobro como necesarios sin tener que
      informar previamente esta acción.</li>
      <li class="g-font--13 g-pl--5 g-font--light mb-2">En caso de reemplazo de la tarjeta de por pérdida, robo o vencimiento, el Banco emisor de la tarjeta puede informar el nuevo número de la tarjeta
      a <strong>MAPFRE Perú</strong> o <strong>MAPFRE Vida</strong> y continuar con el cargo automático; sin perjuicio de ello, es oblicación del tarjetahabiente informar el cambio del
      número de la tarjeta, sino se realiza dicha comunicación, <strong>MAPFRE Perú</strong> o <strong>MAPFRE Perú Vida</strong> no serán responsables por la falta de cargo de la prima,
      pudiendo quedar suspendida la cobertura del seguro.</li>
      <li class="g-font--13 g-pl--5 g-font--light mb-2">Las solicitudes de suspensión temporal, modificación y/o desafiliación, deberán ser presentadas con una anticipación no menor de 72 horas a la fecha
      del cobro. El titular del seguro es responsable del seguimiento de los pagos del seguro a fin de evitar la suspensión de la cobertura del mismo</li>
    </ul>
    `;
    this.withImageModal = false;
    this.sizeModal = 'g-modal--large';
    this.outlineModal = true;
    this.mfModalAlert.open();
  }
}
