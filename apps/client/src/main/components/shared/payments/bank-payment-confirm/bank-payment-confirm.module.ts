import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardItemDetailModule } from '@mx/components/shared/card-item-detail/card-item-detail.module';
import { BankPaymentModule } from '@mx/components/shared/payments/bank-payment/bank-payment.module';
import { MfCustomAlertModule } from '@mx/core/ui/lib/components/alerts/custom-alert';
import { MfModalAlertModule } from '@mx/core/ui/lib/components/modals/modal-alert/modal-alert.module';
import { SwitchModule } from '@mx/core/ui/lib/components/switch/switch.module';
import { TooltipsModule } from '@mx/core/ui/lib/components/tooltips/tooltips.module';
import { CardQuotaModule } from './../card-quota/card-quota.module';
import { BankPaymentConfirmComponent } from './bank-payment-confirm.component';

@NgModule({
  imports: [
    CommonModule,
    BankPaymentModule,
    MfCustomAlertModule,
    TooltipsModule,
    CardItemDetailModule,
    SwitchModule,
    MfModalAlertModule,
    CardQuotaModule
  ],
  declarations: [BankPaymentConfirmComponent],
  exports: [BankPaymentConfirmComponent]
})
export class BankPaymentConfirmModule {}
