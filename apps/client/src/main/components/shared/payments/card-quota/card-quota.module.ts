import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';
import { CardQuotaComponent } from './card-quota.component';

@NgModule({
  imports: [CommonModule, MfCardModule, DirectivesModule],
  declarations: [CardQuotaComponent],
  exports: [CardQuotaComponent]
})
export class CardQuotaModule {}
