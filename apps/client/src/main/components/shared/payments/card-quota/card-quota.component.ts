import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { ClientService } from '@mx/services/client.service';
import { MethodPaymentLang } from '@mx/settings/lang/payment.lang';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import { IPaymentQuoteCard } from '@mx/statemanagement/models/payment.models';
import { PolicyUtil } from '../../utils/policy.util';

@Component({
  selector: 'client-card-quota',
  templateUrl: './card-quota.component.html'
})
export class CardQuotaComponent implements OnInit {
  @HostBinding('class') class = 'w-px-lg--36 w-10 order-1 order-lg-2';
  @Input() quote: IPaymentQuoteCard;
  collapse = false;
  paymentLang = MethodPaymentLang;
  policyLang = PolicyLang;
  headerTitle: string;
  hiddenCuota: boolean;

  constructor(private readonly clientService: ClientService) {}

  ngOnInit(): void {
    this.headerTitle = !this.quote.isNotice
      ? this.paymentLang.SummaryPay
      : `${this.policyLang.Labels.NoticeNumber}: ${this.quote.noticeNumber}`;
    this.hiddenCuota = this.clientService.isGroupTypeCompany() && PolicyUtil.isSctr(this.quote.policyType);
  }
}
