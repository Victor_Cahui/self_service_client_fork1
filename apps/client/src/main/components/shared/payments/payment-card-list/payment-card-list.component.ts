import { DOCUMENT, Location } from '@angular/common';
import {
  Component,
  EventEmitter,
  HostBinding,
  Inject,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  ViewChild
} from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { Autoservicios, FrmProvider } from '@mx/core/shared/providers/services';
import { MfButton } from '@mx/core/ui';
import { MfModalAlertComponent } from '@mx/core/ui/lib/components/modals/modal-alert/modal-alert.component';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { PaymentService } from '@mx/services/payment.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { IMAGE_CARDS, WARNING_ICON } from '@mx/settings/constants/images-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { MethodPaymentLang, PaymentLang } from '@mx/settings/lang/payment.lang';
import { IPaymentConfigurationResponse, IPaymentMethod, ItooltipPM } from '@mx/statemanagement/models/payment.models';
import { isEmpty } from 'lodash-es';
import { FormComponent } from '../../utils/form';
import { PaymentMode } from '../../utils/payment';
import { ITarjeta } from './../../../../../statemanagement/models/payment.models';

import { CardPayment } from '@mx/core/shared/helpers/util/card-payment';
import { FormLyraObjects, lyraScripts } from '@mx/core/shared/providers/constants/lyra';
import { SCRIPT_TYPE, ScriptModel } from '@mx/core/shared/providers/models/script.interface';
import { ScriptService } from '@mx/core/shared/providers/services/script.service';
import { NotificationStatus } from '@mx/core/ui/lib/components/alerts/custom-alert/custom-alert.component';
import { MfModalTermsComponent } from '@mx/core/ui/lib/components/modals/modal-terms/modal-terms.component';
import { environment } from '@mx/environments/environment';
import { InitModalService } from '@mx/services/general/init-modal.service';
import { BehaviorSubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

declare const KR: any;

export enum SETTING_CARD_METHOD {
  ADD = 'ADD',
  AFFILIATE = 'AFFILIATE',
  PAYMENT = 'PAYMENT'
}

@Component({
  selector: 'client-payment-card-list',
  templateUrl: './payment-card-list.component.html'
})
export class PaymentCardListComponent extends GaUnsubscribeBase implements OnInit, OnDestroy {
  @HostBinding('class') class = 'd-flex w-10 g-mr-lg--30 g-ie-flex--0-1-auto order-2 order-lg-1';
  @ViewChild(MfModalAlertComponent) mfModalAlert: MfModalAlertComponent;
  @ViewChild('buttonPay') emitPayButton: MfButton;

  @ViewChild(MfModalTermsComponent) mfModalTerms: MfModalTermsComponent;
  showModalTerms: boolean;

  @Output() readonly processing: EventEmitter<boolean>;

  @Input() method: string;
  @Input() payment: IPaymentConfigurationResponse;
  @Input() showLoading: boolean;
  @Input() listButtonText = '+ Añadir tarjeta';
  @Input() saveButtonText = 'Guardar';
  @Input() cancelButtonText = 'Cancelar';
  @Input() placeholderCardHolderName = 'Nombre del titular';
  @Input() allowExtraButton = false;
  @Input() showTitle = true;
  @Input() title: string = MethodPaymentLang.MethodPayment;
  @Input() manualPayment = false;
  @Input() allowToUseOtherCards = true;
  @Input() allowCardSelection = true;
  @Input() allowToDeleteCards = false;
  @Input() showExpirationDate = false;
  @Input() onlyForm: boolean;
  @Input() selected: string;
  @Output() cardsLength: EventEmitter<number> = new EventEmitter<number>();
  @Output() cancelEvent: EventEmitter<void> = new EventEmitter<void>();
  @Output() formMode: EventEmitter<boolean> = new EventEmitter<boolean>();
  showTerms = true;
  // Tooltip
  @Input() showDetail = false;
  @Input() tooltip: ItooltipPM = {};

  public frm: FormGroup;
  public apiPublicKey: string = environment.lyraPublicKeySoles;
  ownerName: AbstractControl;
  cardNumber: AbstractControl;
  cardExpired: AbstractControl;
  cvv: AbstractControl;
  email: AbstractControl;
  thereAreSavedCards: boolean;
  paymentMode = PaymentMode;
  lang = MethodPaymentLang;
  generalLang = GeneralLang;
  imagesCards = IMAGE_CARDS;
  form: FormGroup;
  modalText: string;
  modalSuggestion: string;
  modalImg: string = WARNING_ICON;
  urlLyraFrame;
  apiFormToken = '';
  eventResponseTransactionLyra: any;
  chbSaveCard: HTMLInputElement;
  chbAutoDebit: HTMLInputElement;
  chbSaveCardFromLyra: HTMLInputElement;
  listSavedCards: ITarjeta[] = [];
  titleSavedCards = GeneralLang.Titles.SelectedSavedCards;
  textManualPayment = PaymentLang.Texts.ManualPay;
  textNoSavedCards = PaymentLang.Texts.NoSavedCard;
  textTerms = GeneralLang.Texts.termsAndConditions;
  selectedCard: ITarjeta;
  useForm: boolean;
  private _scripts: ScriptModel[];
  private readonly _divHolderName: HTMLDivElement;
  private readonly _idDivHolderName: string;

  private readonly notification = new BehaviorSubject<object>({});
  onSuccess = false;
  text: string = PaymentLang.Texts.DeletedCard;

  flagConfirmModal = false;
  cardWillBeDeleted: ITarjeta;
  // Terms & Conditions
  titleTerms = GeneralLang.Titles.TermsAndConditions;
  terminos = false;
  successStatus = NotificationStatus.SUCCESS;

  constructor(
    @Inject(DOCUMENT) readonly document: Document,
    private readonly _FrmProvider: FrmProvider,
    private readonly _Autoservicios: Autoservicios,
    private readonly _Location: Location,
    private readonly _PaymentService: PaymentService,
    private readonly initModalService: InitModalService,
    protected gaService: GAService,
    private readonly _AuthService: AuthService,
    private readonly _NgZone: NgZone,
    private readonly _ScriptService: ScriptService,
    private readonly clientService: ClientService,
    private readonly _Renderer2: Renderer2
  ) {
    super(gaService);
    this.showLoading = true;
    this._idDivHolderName = 'acme-holder-name-data';
  }

  ngOnInit(): void {
    this.frm = this._FrmProvider.DirectPayFormComponent();

    this.thereAreSavedCards = false;
    this.useForm = true;
    this.loadThereSavedCards();

    // to show alert
    this.notification.subscribe((resp: any) => {
      if (resp.value === true) {
        this.onSuccess = resp.value;
        this.text = resp.text;
      } else {
        setTimeout(() => {
          this.onSuccess = false;
          this.notification.next({ value: false });
        }, 5000);
      }
    });
  }

  ngOnDestroy(): void {
    const KRWind = (window as any).KR;
    if (KRWind === undefined || (KRWind && !Object.keys(KR).length)) {
      return void 0;
    }

    KR.removeForms()
      .then(this._removeKR.bind(this))
      .catch(this._removeKR.bind(this));
  }

  private _mapSavedCards(response: any): ITarjeta {
    return {
      numero: response.numero,
      tipo: response.tipo,
      tarjetaToken: response.tarjetaToken,
      fechaVencimiento: response.fechaVencimiento
    };
  }

  private _removeKR(): void {
    try {
      this._ScriptService.remove(this._scripts);
    } catch (error) {}
  }

  async loadThereSavedCards(): Promise<void> {
    this.showLoading = true;
    this._Autoservicios
      .ObtenerTarjetas({ codigoApp: APPLICATION_CODE })
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(
        response => {
          if (this.method === SETTING_CARD_METHOD.ADD) {
            this.useForm = false;
            this.thereAreSavedCards = true;
            if (response) {
              this.cardsLength.emit(response.length);
              this.listSavedCards = response.map(this._mapSavedCards.bind(this));
              if (this.selected) {
                this.getSelectedCard(this.selected, this.listSavedCards);
              }
              if (this.selectedCard) {
                this.showTerms = true;
              }
            } else {
              this.cardsLength.emit(0);
              this.listSavedCards = [];
            }
            this.showLoading = false;
          } else {
            if (response) {
              this.useForm = false;
              this.thereAreSavedCards = true;
              this.listSavedCards = response.map(this._mapSavedCards.bind(this));
              if (this.selected) {
                this.getSelectedCard(this.selected, this.listSavedCards);
              }
              if (this.selectedCard) {
                this.showTerms = true;
              }
              this.showLoading = false;
            } else {
              this.showLoading = false;
              this.useOtherCard();
            }
          }
        },
        err => {
          console.error(err);
          this.showLoading = false;
        }
      );

    return void 0;
  }

  async loadLyraForm(): Promise<void> {
    this.showLoading = true;

    const body: any = FormLyraObjects.getTokenizer(this.payment.codigoMoneda, this.clientService);

    const response = await this._Autoservicios
      .PasarelaFormularioToken({ codigoApp: APPLICATION_CODE }, body)
      .toPromise();

    this.apiFormToken = response.formToken;
    const formPayment: HTMLElement = this.document.getElementById('formPayment');
    if (!formPayment) {
      return void 0;
    }
    this._scripts = FormLyraObjects.getLyraScripts(lyraScripts, SCRIPT_TYPE, this.apiPublicKey);
    formPayment.setAttribute('kr-form-token', this.apiFormToken);
    this._ScriptService.load(this._scripts).then(() => {
      this._initForm();
      this.showLoading = false;
    });
  }

  _initForm(): void {
    this._NgZone.run(() => {
      if (KR && !Object.keys(KR).length) {
        return void 0;
      }
      KR.setFormConfig({
        formToken: this.apiFormToken,
        'kr-placeholder-card-holder-name': this.placeholderCardHolderName
      });
      this._KREvents();
    });
  }

  private _KREvents(): any {
    this._NgZone.run(() => {
      KR.onSubmit(this._onSubmit.bind(this));
      KR.onError(this._onError.bind(this));
      KR.onFormReady(this._onFormReady.bind(this));
    });
  }

  showErrors(control: AbstractControl): boolean {
    return (control.dirty || control.touched) && !isEmpty(control.errors);
  }

  getImageCardByType(type: string): string {
    let imageCard = '';
    switch (type) {
      case 'VISA':
      case 'VISA_ELECTRON':
      case 'VISA_DEBIT':
        imageCard = IMAGE_CARDS.VISA;
        break;
      case 'MASTERCARD':
      case 'MASTERCARD_DEBIT':
        imageCard = IMAGE_CARDS.MASTER_CARD;
        break;
      case 'DINERS_CLUB':
      case 'DINERS':
        imageCard = IMAGE_CARDS.DINNER_CLUB;
        break;
      case 'AMERICAN_EXPRESS':
      case 'AMERICAN_EXPRESS_AMEX':
      case 'AMERICAN EXPRESS':
      case 'AMEX':
        imageCard = IMAGE_CARDS.AMERICAN_EXPRESS;
        break;
      default:
        imageCard = '';
        break;
    }

    return imageCard;
  }

  private _onSubmit(event: any): void {
    this._NgZone.run(() => {
      if (this.method === SETTING_CARD_METHOD.AFFILIATE) {
        this._affiliateResponse(event);
      } else {
        this._addCard(event);
      }
    });
  }

  public _affiliateResponse(event): void {
    if (event.clientAnswer.orderStatus) {
      this.eventResponseTransactionLyra = event.clientAnswer;

      const bodyReq: any = {
        tarjetaToken: this.eventResponseTransactionLyra.transactions[0].paymentMethodToken,
        codCia: this.payment.codCia,
        numeroPoliza: this.payment.numeroPoliza
      };

      this._Autoservicios
        .AfiliarTarjeta({ codigoApp: APPLICATION_CODE }, bodyReq)
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(res => {
          try {
            this.showLoading = false;
            this._Location.back();
            this._PaymentService.notificactionForPS.next({ value: true, text: PaymentLang.Texts.ChangeDebitCard });
          } catch (error) {
            this.showLoading = false;
            console.error(error);
          }
        });
    }
  }

  public _addCard(event): void {
    if (event.clientAnswer.orderStatus) {
      this.showLoading = true;
      this.eventResponseTransactionLyra = event.clientAnswer;
      const cardDetail = this.eventResponseTransactionLyra.transactions[0].transactionDetails.cardDetails;

      const bodyReq: ITarjeta = {
        tarjetaToken: this.eventResponseTransactionLyra.transactions[0].paymentMethodToken,
        numero: cardDetail.pan,
        fechaVencimiento: this.setFechaVencimiento(cardDetail.expiryMonth, cardDetail.expiryYear),
        tipo: cardDetail.effectiveBrand
      };

      this._Autoservicios
        .GuardarTarjeta({ codigoApp: APPLICATION_CODE }, bodyReq)
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(
          res => {
            try {
              this.showLoading = false;
              this.useForm = false;
              this.showTerms = false;
              this.loadThereSavedCards();
              this.formMode.emit(false);
              this.notification.next({ value: true, text: PaymentLang.Texts.AddedCard });
            } catch (error) {
              console.error(error);
              this.showLoading = false;
            }
          },
          err => {
            console.error(err);
            this.showLoading = false;
          }
        );
    }
  }

  public useOtherCard(): void {
    this.useForm = true;
    this.showTerms = true;
    this.terminos = this.method === SETTING_CARD_METHOD.ADD ? true : false;
    this.loadLyraForm();
    this.formMode.emit(true);
  }

  private _onError(event: any): void {
    this._NgZone.run(() => {
      if (event && event.errorCode && event.errorCode.startsWith('CLIENT')) {
        return void 0;
      }
    });
  }

  private _onFormReady(): void {
    this._NgZone.run(() => {
      this._captureCheckbox();
      this._hideLyraCheckbox();
      setTimeout(() => {
        FormLyraObjects.setLabelForm(this.document);
      }, 100);
    });
  }

  public _emitSubmit(): void {
    this._NgZone.run(() => {
      this.showLoading = true;
      const button: any = this.document.getElementsByClassName('kr-payment-button')[0];

      KR.validateForm()
        .then(() => {
          if (this.frm.invalid) {
            this.showLoading = false;
            this._handlerErorClassInput(true, 1);
          } else {
            button.click();
          }
        })
        .catch(() => {
          button.click();
          if (this.frm.invalid) {
            this.showLoading = false;
            this._handlerErorClassInput(true, 1);
          }
        });
    });
  }

  public _editPaymentCard(): void {
    this.showLoading = true;
    if (this.selectedCard.numero) {
      const bodyReq = {
        tarjetaToken: this.selectedCard.tarjetaToken,
        codCia: this.payment.codCia,
        numeroPoliza: this.payment.numeroPoliza
      };

      this._Autoservicios.AfiliarTarjeta({ codigoApp: APPLICATION_CODE }, bodyReq).subscribe(
        res => {
          try {
            this.showLoading = false;
            this._Location.back();
            this._PaymentService.notificactionForPS.next({ value: true, text: PaymentLang.Texts.ChangeDebitCard });
          } catch (error) {
            this.showLoading = false;
            console.error(error);
          }
        },
        err => {
          console.error(err);
          this.showLoading = false;
        }
      );
    } else {
      const queryReq = {
        codigoApp: APPLICATION_CODE,
        codCia: this.payment.codCia,
        numeroPoliza: this.payment.numeroPoliza
      };

      this._Autoservicios
        .DesafiliarTarjeta(queryReq)
        .toPromise()
        .then(response => {
          if (response) {
            this._PaymentService.notificactionForPS.next({ value: true, text: PaymentLang.Texts.DisableDebitCard });
            this._Location.back();
          }
        })
        .catch(error => {
          console.error(error);
        });
    }
  }

  public eliminarTarjeta(tarjeta: ITarjeta): void {
    this.showLoading = true;
    if (tarjeta) {
      const bodyReq: any = {
        codigoApp: APPLICATION_CODE,
        tarjetaToken: tarjeta.tarjetaToken
      };

      this._Autoservicios
        .EliminarTarjeta(bodyReq)
        .toPromise()
        .then(res => {
          this.showLoading = false;
          this.loadThereSavedCards();
          this.notification.next({ value: true, text: PaymentLang.Texts.DeletedCard });
        })
        .catch(error => {
          console.error(error);
          this.showLoading = false;
          this.modalSuggestion = null;
          this.flagConfirmModal = false;
        });
    }
  }

  private _handlerErorClassInput(add: boolean = false, input: number): void {
    if (input !== 1) {
      this._Renderer2[add ? 'addClass' : 'removeClass'](this._divHolderName.firstElementChild, 'kr-error');
    }
  }

  private _hideLyraCheckbox(): void {
    const krChbBox = document.getElementsByClassName('kr-field kr-checkbox kr-checkbox-type-default')[0];
    if (krChbBox) {
      krChbBox.setAttribute('style', 'display:none');
      this.chbSaveCardFromLyra = krChbBox.children[0].children[0].children[0].children[0] as HTMLInputElement;
    }
  }

  private _captureCheckbox(): void {
    const chb1 = document.getElementById('chbSaveCards') as HTMLInputElement;
    const chb2 = document.getElementById('chbAutoDebit') as HTMLInputElement;
    if (chb1) {
      this.chbSaveCard = chb1;
      chb1.disabled = true;
      chb1.addEventListener('change', event => {
        if (chb1.checked) {
          if (chb2) {
            chb2.removeAttribute('disabled');
          }
          this.chbSaveCardFromLyra.click();
        } else {
          this.chbSaveCardFromLyra.checked = false;
          if (chb2) {
            chb2.checked = false;
            chb2.setAttribute('disabled', 'disabled');
          }
        }
      });
    }
    if (chb2) {
      this.chbAutoDebit = chb2;
      chb2.addEventListener('change', event => {});
    }
  }

  formatValue(control): void {
    FormComponent.removeSpaces(control);
  }

  emitCard(): IPaymentMethod {
    return CardPayment.getCardInfo(this.eventResponseTransactionLyra, PaymentMode.CARD);
  }

  emitCardToken(): IPaymentMethod {
    return CardPayment.getCardInfoByToken(this.selectedCard, PaymentMode.TOKN);
  }

  onConfirmModal(event): void {
    if (!this.flagConfirmModal) {
      if (this.useForm || (!this.useForm && this.thereAreSavedCards && this.selectedCard)) {
        this._Location.back();
      }
    } else {
      if (event && this.cardWillBeDeleted) {
        this.eliminarTarjeta(this.cardWillBeDeleted);
      }
    }
  }

  closeModal(): void {
    this.initModalService.emitCloseModalEvent();
  }

  onSelectedCard(event): void {
    if (event.target.value === 'MANUAL') {
      this.showTerms = false;
      this.terminos = true;
    } else {
      this.showTerms = true;
      this.terminos = false;
    }
  }

  _doEvent(): void {
    if (this.useForm) {
      this._emitSubmit();
    } else {
      this._editPaymentCard();
    }
  }

  getSelectedCard(item, arr: ITarjeta[]): ITarjeta {
    return (this.selectedCard = arr.find(x => x.numero === item));
  }

  cancel(): void {
    this.cancelEvent.emit();
    if (this.method === SETTING_CARD_METHOD.ADD) {
      if (this.useForm && this.thereAreSavedCards) {
        this.useForm = false;
        this.showTerms = false;
      } else {
        this._Location.back();
      }
    } else {
      if (this.useForm && this.thereAreSavedCards) {
        this.useForm = false;
      } else {
        this._Location.back();
      }
    }
  }

  deleteCard(card: ITarjeta): void {
    this.cardWillBeDeleted = card;
    this.flagConfirmModal = true;
    this.modalText = PaymentLang.Titles.DeleteCard;
    this.modalSuggestion = PaymentLang.Messages.DeleteCard;
    this.mfModalAlert.open();
  }

  openModalTerms(): void {
    this.showModalTerms = true;
    setTimeout(() => {
      this.mfModalTerms.open();
    });
  }

  checkTerms(ev: any): void {
    this.terminos = ev.target.checked;
  }

  setFechaVencimiento(month, year): string {
    return `${month}/${year}`;
  }

  // tslint:disable-next-line: max-file-line-count
}
