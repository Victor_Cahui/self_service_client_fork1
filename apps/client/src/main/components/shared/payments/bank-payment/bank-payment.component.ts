import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { IMAGE_BANKS } from '@mx/settings/constants/images-values';
import { MethodPaymentLang } from '@mx/settings/lang/payment.lang';

@Component({
  selector: 'client-bank-payment',
  templateUrl: './bank-payment.component.html'
})
export class BankPaymentComponent implements OnInit {
  @HostBinding('class') class = 'tab-content';
  @Input() confirm: boolean;
  @Input() withoutTexts: boolean;

  imagesBanks = IMAGE_BANKS;
  lang = MethodPaymentLang.PaymentBank;
  messageText1: string;
  messageText2: string;

  ngOnInit(): void {
    if (!this.withoutTexts) {
      this.messageText1 = this.confirm ? '' : this.lang.Text1;
      this.messageText2 = this.confirm ? this.lang.TextConfirm : this.lang.Text2;
    }
  }
}
