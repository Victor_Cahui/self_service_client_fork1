import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BankPaymentComponent } from './bank-payment.component';

@NgModule({
  imports: [CommonModule],
  declarations: [BankPaymentComponent],
  exports: [BankPaymentComponent]
})
export class BankPaymentModule {}
