import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TabsPaymentsComponent } from './tabs-payments.component';

@NgModule({
  imports: [CommonModule],
  declarations: [TabsPaymentsComponent],
  exports: [TabsPaymentsComponent]
})
export class TabsPaymentsModule {}
