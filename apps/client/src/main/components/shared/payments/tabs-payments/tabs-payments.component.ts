import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MethodPaymentLang } from '@mx/settings/lang/payment.lang';
import { PaymentMode } from '../../utils/payment';

@Component({
  selector: 'client-tabs-payments',
  templateUrl: './tabs-payments.component.html'
})
export class TabsPaymentsComponent implements OnInit {
  @Input() quoataEmited: boolean;
  @Output() tabChange: EventEmitter<PaymentMode> = new EventEmitter<PaymentMode>();
  tabSelected: PaymentMode = PaymentMode.CARD;
  paymentMode = PaymentMode;
  lang = MethodPaymentLang.Methods;

  ngOnInit(): void {}

  onTabSelected(tabSelected: PaymentMode): void {
    if (this.tabSelected !== tabSelected) {
      this.tabSelected = tabSelected;
      this.tabChange.next(tabSelected);
    }
  }
}
