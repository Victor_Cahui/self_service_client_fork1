import { CommonModule } from '@angular/common';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { DirectivesModule, MfButtonModule, MfInputModule, MfShowErrorsModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { TooltipsModule } from '@mx/core/ui/lib/components/tooltips/tooltips.module';
import { MfModalAlertModule } from '@mx/core/ui/public-api';
import { InputWithIconModule } from '../../inputs';
import { BankPaymentModule } from '../bank-payment';
import { BankPaymentSoatModule } from '../bank-payment-soat/bank-payment-soat.module';
import { TabsPaymentsModule } from '../tabs-payments';
import { PaymentMethodComponent } from './payment-method.component';

import { ReactiveFormsModule } from '@angular/forms';
import { AutoserviciosMock } from '@mx/core/shared/providers/services/proxy/Autoservicios.mock';

describe('PaymentMethodComponent', () => {
  let component: PaymentMethodComponent;
  let fixture: ComponentFixture<PaymentMethodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        TabsPaymentsModule,
        MfButtonModule,
        MfInputModule,
        BankPaymentModule,
        InputWithIconModule,
        TooltipsModule,
        ReactiveFormsModule,
        MfShowErrorsModule,
        MfLoaderModule,
        MfModalAlertModule,
        DirectivesModule,
        BankPaymentSoatModule
      ],
      declarations: [PaymentMethodComponent],
      providers: [
        {
          provide: Autoservicios,
          useClass: AutoserviciosMock
        }
      ]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(PaymentMethodComponent);
        component = fixture.debugElement.componentInstance;
      })
      .then(() => {
        component.totalAmount = '60.00';
        component.showLoading = false;
        component.quoataEmited = false;
        component.onlyForm = true;
        component.paymentButtonText = 'Pagar';
        component.placeholderCardHolderName = 'Nombre del titular';
        component.apiFormToken = '123456';
      })
      .then(() => {
        fixture.detectChanges();
      });
  }));

  it('should create the app', async(() => {
    expect(component).toBeTruthy();
  }));

  it('to validate component properties', async(() => {
    component.totalAmount = '60.00';
    component.showLoading = false;
    component.quoataEmited = false;
    component.onlyForm = true;
    component.paymentButtonText = 'Pagar';
    component.placeholderCardHolderName = 'Nombre del titular';
    component.apiFormToken = '123456';
    expect(component.totalAmount).toBeDefined();
    expect(component.showLoading).toBeDefined();
    expect(component.quoataEmited).toBeDefined();
    expect(component.onlyForm).toBeDefined();
    expect(component.paymentButtonText).toBeDefined();
    expect(component.placeholderCardHolderName).toBeDefined();
    expect(component.apiFormToken).toBeDefined();
  }));

  it('should load info lyra', async(() => {
    component.apiFormToken = '123456';
    component._initForm();
  }));
});
