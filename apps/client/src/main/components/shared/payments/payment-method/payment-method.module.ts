import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DirectivesModule, MfButtonModule, MfInputModule, MfShowErrorsModule } from '@mx/core/ui';
import { CardModule } from '@mx/core/ui/lib/components/card/card.module';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { TooltipsModule } from '@mx/core/ui/lib/components/tooltips/tooltips.module';
import { MfModalAlertModule } from '@mx/core/ui/public-api';
import { InputWithIconModule } from '../../inputs';
import { BankPaymentModule } from '../bank-payment';
import { BankPaymentSoatModule } from '../bank-payment-soat/bank-payment-soat.module';
import { TabsPaymentsModule } from '../tabs-payments';
import { PaymentMethodComponent } from './payment-method.component';

@NgModule({
  imports: [
    CommonModule,
    TabsPaymentsModule,
    MfButtonModule,
    MfInputModule,
    BankPaymentModule,
    InputWithIconModule,
    TooltipsModule,
    ReactiveFormsModule,
    MfShowErrorsModule,
    MfLoaderModule,
    MfModalAlertModule,
    DirectivesModule,
    BankPaymentSoatModule,
    FormsModule,
    CardModule
  ],
  declarations: [PaymentMethodComponent],
  exports: [PaymentMethodComponent]
})
export class PaymentMethodModule {}
