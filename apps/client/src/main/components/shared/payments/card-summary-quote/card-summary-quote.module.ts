import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DirectivesModule, MfCardModule, SwitchModule } from '@mx/core/ui';
import { CardSummaryQuoteComponent } from './card-summary-quote.component';

@NgModule({
  imports: [CommonModule, MfCardModule, DirectivesModule, SwitchModule],
  declarations: [CardSummaryQuoteComponent],
  exports: [CardSummaryQuoteComponent]
})
export class CardSummaryQuoteModule {}
