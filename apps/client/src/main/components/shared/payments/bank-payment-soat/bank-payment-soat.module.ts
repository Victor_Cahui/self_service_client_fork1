import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TooltipsModule } from '@mx/core/ui/lib/components/tooltips/tooltips.module';
import { BankPaymentModule } from '../bank-payment/bank-payment.module';
import { BankPaymentSoatComponent } from './bank-payment-soat.component';

@NgModule({
  imports: [CommonModule, BankPaymentModule, TooltipsModule],
  declarations: [BankPaymentSoatComponent],
  exports: [BankPaymentSoatComponent]
})
export class BankPaymentSoatModule {}
