import { Component, EventEmitter, HostBinding, Input, OnInit, Output } from '@angular/core';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { ConfirmCardPaymentFreeSoatLang, MethodPaymentLang, PaymentLang } from '@mx/settings/lang/payment.lang';

@Component({
  selector: 'client-payment-free',
  templateUrl: './payment-free.component.html'
})
export class PaymentFreeComponent implements OnInit {
  @HostBinding('class') class = 'd-flex w-10 g-mr-lg--30 g-ie-flex--0-1-auto order-2 order-lg-1';

  @Input() contracting: string;
  @Output() confirm: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() cancel: EventEmitter<boolean> = new EventEmitter<boolean>();

  iconFree = ConfirmCardPaymentFreeSoatLang.icon;
  labelFree = PaymentLang.Texts.Free;
  title = MethodPaymentLang.MethodPayment;
  messageFree: string;
  btnNoMPD = PaymentLang.Buttons.NoMPD;
  btnPurchase = PaymentLang.Buttons.ConfirmPurchase;

  ngOnInit(): void {
    if (!this.contracting) {
      this.contracting = GeneralLang.Messages.EstimatedUser;
    }
    this.messageFree = PaymentLang.Messages.WithMapfreDollarsFree.replace('{{user}}', this.contracting);
  }

  emitNoMPD(): void {
    this.cancel.emit();
  }

  emitConfirmMPD(): void {
    this.confirm.emit();
  }
}
