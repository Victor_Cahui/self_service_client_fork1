import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfButtonModule } from '@mx/core/ui';
import { PaymentFreeComponent } from './payment-free.component';

@NgModule({
  imports: [CommonModule, MfButtonModule],
  declarations: [PaymentFreeComponent],
  exports: [PaymentFreeComponent]
})
export class PaymentFreeModule {}
