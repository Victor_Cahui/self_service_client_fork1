import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LinksModule } from '@mx/core/ui/lib/links/links.module';
import { IconCoverageModule } from '../icon-coverage/icon-coverage.module';
import { CardIconsCoverageComponent } from './card-icons-coverage.component';

@NgModule({
  imports: [CommonModule, IconCoverageModule, LinksModule],
  exports: [CardIconsCoverageComponent],
  declarations: [CardIconsCoverageComponent]
})
export class CardIconsCoverageModule {}
