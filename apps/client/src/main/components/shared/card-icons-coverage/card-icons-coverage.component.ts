import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ICoverageItem } from '@mx/statemanagement/models/coverage.interface';

@Component({
  selector: 'client-icons-coverage',
  templateUrl: './card-icons-coverage.component.html'
})
export class CardIconsCoverageComponent {
  @Input() itemsList: Array<ICoverageItem>;
  @Input() title: string;
  @Input() text?: string;
  @Input() groupBool: boolean;
  @Input() link: string;
  @Output() actions?: EventEmitter<any> = new EventEmitter<any>();

  actionLink(event): void {
    this.actions.emit();
  }
}
