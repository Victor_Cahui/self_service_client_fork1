import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';
import { IconPoliciesModule } from '../icon-policies/icon-polices.module';
import { CardItemDetailComponent } from './card-item-detail.component';

@NgModule({
  imports: [CommonModule, DirectivesModule, MfCardModule, IconPoliciesModule],
  exports: [CardItemDetailComponent],
  declarations: [CardItemDetailComponent]
})
export class CardItemDetailModule {}
