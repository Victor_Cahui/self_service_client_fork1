import { Component, Input } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { environment } from '@mx/environments/environment';
import { FILE_ICON, PERSON_ICON } from '@mx/settings/constants/images-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { PERIODS_LANAG } from '@mx/settings/lang/sctr.lang';
import { ICardPeriodPreview, IPeriodDetails } from '@mx/statemanagement/models/sctr.interface';

@Component({
  selector: 'client-card-sctr-period-item-detail',
  templateUrl: './card-sctr-period-item-detail.component.html'
})
export class CardSctrPeriodItemDetailComponent {
  @Input() items: Array<IPeriodDetails>;
  @Input() isRecipes: boolean;
  @Input() showPayroll: boolean;
  @Input() isAdditional: boolean;
  @Input() period: ICardPeriodPreview;

  policyNumberLabel = GeneralLang.Texts.PolicyNumber;
  personIcon = PERSON_ICON;
  fileIcon = `${FILE_ICON} pl-1 pr-2 pr-lg-1 pl-lg-0`;
  periodsLang = PERIODS_LANAG;
  environment = environment;

  constructor(private readonly router: Router, private readonly activatedRoute: ActivatedRoute) {}

  goToPeriodsPayroll(period: ICardPeriodPreview): void {
    const policyNumber = this.activatedRoute.snapshot.params['policyNumber'];
    const periodType = this.isAdditional ? 'MA' : 'PE';
    const periodId = `${period.payload.fechaInicioVigencia}|${period.payload.fechaFinVigencia}|${periodType}`;
    const queryParams = { periodId } as Params;

    this.router.navigate([`/sctr/sctr-insurance/${policyNumber}/periods/payroll`], { queryParams });
  }
}
