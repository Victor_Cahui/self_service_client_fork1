import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { LinkGreenComponent } from '@mx/core/ui/lib/links/link-green/link-green.component';
import { IconPoliciesComponent } from '../../icon-policies/icon-policies.component';
import { CardSctrPeriodItemDetailComponent } from './card-sctr-period-item-detail.component';

describe('CardSctrPeriodItemDetailComponent', () => {
  let component: CardSctrPeriodItemDetailComponent;
  let fixture: ComponentFixture<CardSctrPeriodItemDetailComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [CardSctrPeriodItemDetailComponent, IconPoliciesComponent, LinkGreenComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardSctrPeriodItemDetailComponent);
    component = fixture.debugElement.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', async(() => {
    expect(component).toBeTruthy();
  }));

  it('validate all component properties', async(() => {
    expect(component.policyNumberLabel).toEqual('Nro. de póliza');
    expect(component.personIcon).toEqual('icon-mapfre_234_person');
    expect(component.fileIcon).toEqual('icon-mapfre_058_file pl-1 pr-2 pr-lg-1 pl-lg-0');
    expect(component.periodsLang).toBeDefined();
    expect(component.environment).toBeDefined();
  }));

  it('should correctly render the passed @input value', async(() => {
    component.items = [];
    fixture.detectChanges();
    expect(true).toBe(true);
  }));
});
