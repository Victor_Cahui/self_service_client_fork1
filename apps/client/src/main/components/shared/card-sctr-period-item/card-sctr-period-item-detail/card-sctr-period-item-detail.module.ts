import { CommonModule, registerLocaleData } from '@angular/common';
import localeEsPe from '@angular/common/locales/es-PE';
import { LOCALE_ID, NgModule } from '@angular/core';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';
import { LinksModule } from '@mx/core/ui/lib/links/links.module';
import { IconPoliciesModule } from '../../icon-policies/icon-polices.module';

import { CardSctrPeriodItemDetailComponent } from './card-sctr-period-item-detail.component';

registerLocaleData(localeEsPe, 'es-Pe');

@NgModule({
  imports: [CommonModule, DirectivesModule, MfCardModule, IconPoliciesModule, LinksModule],
  declarations: [CardSctrPeriodItemDetailComponent],
  exports: [CardSctrPeriodItemDetailComponent],
  providers: [{ provide: LOCALE_ID, useValue: 'es-Pe' }]
})
export class CardSctrPeriodItemDetailModule {}
