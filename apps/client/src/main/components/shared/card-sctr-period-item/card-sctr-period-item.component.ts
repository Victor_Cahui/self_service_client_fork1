import { Component, Input, OnInit } from '@angular/core';
import { Params, Router } from '@angular/router';
import { environment } from '@mx/environments/environment';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { ADD_ICON, FILE_ICON } from '@mx/settings/constants/images-values';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import { PERIODS_LANAG } from '@mx/settings/lang/sctr.lang';
import { ISctrPeriodResponse } from '@mx/statemanagement/models/policy.interface';
import { ICardPeriodPreview } from '@mx/statemanagement/models/sctr.interface';
import { SCTR_PERIOD_STATUS } from '../utils/policy';

@Component({
  selector: 'client-card-sctr-period-item',
  templateUrl: './card-sctr-period-item.component.html'
})
export class CardSctrPeriodItemComponent implements OnInit {
  @Input() period: ICardPeriodPreview;
  @Input() policyNumber: string;
  @Input() isAdditional: boolean;
  @Input() showPayroll = true;
  @Input() showReceipts = true;

  fileIcon = FILE_ICON;
  lang = PERIODS_LANAG;
  periodStatus = SCTR_PERIOD_STATUS;
  environment = environment;
  tooltipButton = PolicyLang.Messages.MonthDeclare;
  addIcon: string;

  constructor(protected policiesInfoService: PoliciesInfoService, protected router: Router) {}

  ngOnInit(): void {
    this.addIcon = `${ADD_ICON} g-font--20`;
  }

  collapseDetail(item: ICardPeriodPreview): void {
    item.collapse = !item.collapse;
  }

  goToDeclare(period: ICardPeriodPreview): void {
    this.setClientSctrPeriodResponse(period.payload);
    this.router.navigate([`/sctr/sctr-insurance/${this.policyNumber}/periods/${this.lang.DeclarationPath}`]);
  }

  goToInclusion(period: ICardPeriodPreview): void {
    this.setClientSctrPeriodResponse(period.payload);
    this.router.navigate([`/sctr/sctr-insurance/${this.policyNumber}/periods/${this.lang.InclusionPath}`]);
  }

  goToPeriodReceipts(period: ICardPeriodPreview): void {
    this.setClientSctrPeriodResponse(period.payload);
    const periodType = this.isAdditional ? 'MA' : 'PE';
    const periodId = `${period.payload.fechaInicioVigencia}|${period.payload.fechaFinVigencia}|${periodType}`;
    const queryParams = { periodId } as Params;
    this.router.navigate([`/sctr/sctr-insurance/${this.policyNumber}/periods/receipts`], { queryParams });
  }

  private setClientSctrPeriodResponse(period: ISctrPeriodResponse): void {
    this.policiesInfoService.setClientSctrPeriodResponse(period);
  }
}
