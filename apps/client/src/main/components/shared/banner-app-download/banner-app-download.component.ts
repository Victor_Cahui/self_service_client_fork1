import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google/google.gabase';
import { GeneralService } from '@mx/services/general/general.service';
import * as KEYS from '@mx/settings/constants/key-values';
import { IParameterApp, IPathImageSize } from '@mx/statemanagement/models/general.models';
import { Observable } from 'rxjs';

@Component({
  selector: 'client-banner-app-download',
  templateUrl: './banner-app-download.component.html'
})
export class BannerAppDownloadComponent extends GaUnsubscribeBase implements OnInit {
  existUrlImage = true;
  pathImgBanner: IPathImageSize = { sm: '', lg: '' };

  appstore_app_url: string;
  playstore_app_url: string;
  playstore_button: string;
  playstore_button_hover: string;
  appstore_button: string;
  appstore_button_hover: string;
  banner_app_sm: string;
  banner_app_md: string;
  banner_app_lg: string;
  banner_app_xl: string;
  parametersSubject: Observable<Array<IParameterApp>>;

  @ViewChild('banner') banner: ElementRef;

  constructor(
    private readonly generalService: GeneralService,
    private readonly renderer2: Renderer2,
    protected gaService: GAService
  ) {
    super(gaService);
  }

  ngOnInit(): void {
    this.parametersSubject = this.generalService.getParametersSubject();
    this.parametersSubject.subscribe(params => {
      this.appstore_app_url = this.generalService.getValueParams(KEYS.APPSTORE_APP_LINK);
      this.playstore_app_url = this.generalService.getValueParams(KEYS.PLAYSTORE_APP_LINK);
      this.playstore_button = this.generalService.getValueParams(KEYS.PLAYSTORE_BOTON);
      this.playstore_button_hover = this.generalService.getValueParams(KEYS.PLAYSTORE_BOTON_HOVER);
      this.appstore_button = this.generalService.getValueParams(KEYS.APPSTORE_BOTON);
      this.appstore_button_hover = this.generalService.getValueParams(KEYS.APPSTORE_BOTON_HOVER);
      this.pathImgBanner = {
        sm: this.generalService.getValueParams(KEYS.BANNER_APP_SM),
        md: this.generalService.getValueParams(KEYS.BANNER_APP_MD),
        lg: this.generalService.getValueParams(KEYS.BANNER_APP_LG),
        xl: this.generalService.getValueParams(KEYS.BANNER_APP_XL)
      };
    });
  }

  notLoading(error?: boolean): void {
    this.existUrlImage = error;
    if (error) {
      this.renderer2.addClass(this.banner.nativeElement, 'd-block');
    } else {
      this.renderer2.removeClass(this.banner.nativeElement, 'd-block');
    }
  }
}
