import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule } from '@mx/core/ui';
import { BannerAppDownloadComponent } from './banner-app-download.component';

@NgModule({
  imports: [CommonModule, DirectivesModule, GoogleModule],
  declarations: [BannerAppDownloadComponent],
  exports: [BannerAppDownloadComponent]
})
export class BannerAppDownloadModule {}
