import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ResponsiveCarouselComponent } from './responsive-carousel.component';

@NgModule({
  imports: [CommonModule],
  declarations: [ResponsiveCarouselComponent],
  exports: [ResponsiveCarouselComponent]
})
export class ResponsiveCarouselModule {}
