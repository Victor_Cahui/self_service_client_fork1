import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'client-responsive-carousel',
  templateUrl: './responsive-carousel.component.html',
  styleUrls: ['./responsive-carousel.component.scss']
})
export class ResponsiveCarouselComponent implements OnInit {
  @Input() header: string;
  @Input() data: any = [];

  ngOnInit(): void {}
}
