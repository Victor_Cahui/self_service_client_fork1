import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PageTitleComponent } from './page-title.component';

@NgModule({
  imports: [CommonModule],
  exports: [PageTitleComponent],
  declarations: [PageTitleComponent]
})
export class PageTitleModule {}
