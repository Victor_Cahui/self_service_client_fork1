import { Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'client-page-title',
  templateUrl: './page-title.component.html'
})
export class PageTitleComponent {
  @HostBinding('attr.class') class = 'w-100';
  @Input() title: string;
  @Input() message: string;
}
