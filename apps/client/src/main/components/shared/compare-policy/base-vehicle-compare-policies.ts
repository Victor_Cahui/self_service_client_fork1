import { DecimalPipe } from '@angular/common';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PolicyListService } from '@mx/services/policy/policy-list.service';
import { PoliciesQuoteService } from '@mx/services/policy/policy-quote.service';
import { VehicleService } from '@mx/services/vehicle.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import {
  ICompareDetailView,
  ICompareHeaderResponse,
  ICompareHeaderView,
  ICoverageDetailsResponse,
  ITextCoveragesCompare
} from '@mx/statemanagement/models/policy.interface';
import { IClientVehicleDetail, ICompareVehicleRequest } from '@mx/statemanagement/models/vehicle.interface';
import { zip } from 'rxjs';
import { BaseComparePolicies } from './base-compare-policies';

export abstract class BaseVehicleComparePolicies extends BaseComparePolicies {
  vehicleData: IClientVehicleDetail;
  body: ICompareVehicleRequest;

  constructor(
    protected policyListService: PolicyListService,
    protected headerHelperService: HeaderHelperService,
    protected quoteService: PoliciesQuoteService,
    protected vehicleService: VehicleService,
    protected decimalPipe: DecimalPipe,
    protected gaService?: GAService
  ) {
    super(policyListService, headerHelperService, quoteService, decimalPipe, gaService);
  }

  // Vista Inicial
  viewInitial(): void {
    this.policySelected = this.policyListService.getPolicySelected();
    this.params = {
      codigoApp: APPLICATION_CODE,
      numeroPoliza: this.policySelected
    };
    this.vehicleData = this.quoteService.getVehicleData();
    if (this.vehicleData && !this.policySelected) {
      this.body = {
        marcaId: this.vehicleData.codigoMarca,
        modeloId: this.vehicleData.codigoModelo,
        anio: this.vehicleData.anio,
        tipoVehiculoId: this.vehicleData.codigoTipoVehiculo,
        valorSugerido: this.vehicleData.valorSugerido,
        usoVehiculoId: this.vehicleData.codigoUso,
        ciudadId: this.vehicleData.codigoCiudad,
        tienePlaca: !this.vehicleData.placaEnTramite,
        placa: this.vehicleData.placa,
        es0KM: this.vehicleData.es0Km,
        codigoMoneda: this.vehicleData.codigoMonedaValorSugerido
      };
    } else {
      this.body = null;
      if (this.compare) {
        this.quoteService.setVehicleData(null);
        this.quoteService.setHeaderData(null);
        this.quoteService.setDetailsData(null);
      }
    }

    this.compareHeader$ = this.vehicleService.getCompareHeader(this.params, this.body);
    this.compareDetails$ = this.vehicleService.getCompareDetails(this.params, this.body);

    // Datos de cuadro
    this.getDataCompare();
  }

  // Buscar datos para cuadro si hay cambios en la data
  getDataCompare(): void {
    const header = this.quoteService.getHeaderData();
    const details = this.quoteService.getDetailsData();
    if (header && header.length && details && details.coberturas.length) {
      this.setSelectMonth(header);
      this.setHeader(header);
      this.setDetails(details);
    } else {
      // Va a servicio
      this.showLoading = true;
      this.quoteService.setHeaderData(null);
      this.quoteService.setDetailsData(null);
      // tslint:disable-next-line: deprecation
      this.zipSub = zip(this.compareHeader$, this.compareDetails$).subscribe(
        res => {
          if (res[0] && res[0].length) {
            // Select
            this.setSelectMonth(res[0]);
            // Header
            this.quoteService.setHeaderData(res[0]);
            this.setHeader(res[0]);
          }
          // Detalle
          if (res[1]) {
            this.quoteService.setDetailsData(res[1]);
            this.setDetails(res[1]);
          }
        },
        () => {
          this.showLoading = false;
        },
        () => {
          this.zipSub.unsubscribe();
          this.showLoading = false;
        }
      );
    }
  }

  // Header
  setHeader(data): void {
    this.setChangeModalityFrom();
    this.coveragesHeader = data.map((modality: ICompareHeaderResponse, index: number) => {
      return this.setHeaderModality(modality, index);
    });
    this.headerLength = this.coveragesHeader ? this.coveragesHeader.length : 0;
  }

  setHeaderModality(modality: ICompareHeaderResponse, index: number): ICompareHeaderView {
    const symbol = `${StringUtil.getMoneyDescription(modality.duracion[0].cuotas[0].codigoMoneda)} `;
    const year = parseInt(this.timeSelected[index], 10);
    const cost = this.getMonthlyCost(modality.duracion, this.monthSelected, year);
    const quoteValue = this.getQuoteCost(modality.duracion, this.monthSelected, year);
    const policyCurrent = modality.flagIndicadorPoliza === this.policyIndicator.MY_POLICY;

    return {
      company: modality.codigoCompania,
      branch: modality.codigoRamo,
      modality: this.getModality(modality.duracion, year) || '',
      title: modality.nombreProducto,
      moneySymbol: symbol,
      monthlyCost: cost,
      quote: quoteValue,
      requestYears: this.getHiringTime(modality.duracion),
      titleTooltips: modality.observaciones ? modality.observaciones.titulo : '',
      contentTooltips: modality.observaciones ? modality.observaciones.detalles : [],
      years: year,
      amountFee: this.monthSelected,
      textValue: this.getTextValue(),
      fractionationCode: this.getFractionationCode(modality.duracion, this.monthSelected, year),
      textSumaryValue: this.getTextSumaryValue(symbol, cost),
      current: policyCurrent,
      recommended: modality.flagIndicadorPoliza === this.policyIndicator.RECOMMENDED,
      changeModality: policyCurrent ? false : this.showChangeModality,
      textChangeModality: this.policyLang.Buttons.ChangeMe,
      textChangeModalityFrom: policyCurrent ? '' : this.textChangeModalityFrom,
      textPaymentFrecuency: policyCurrent ? '' : this.getTextPaymentFrecuency(),
      quoteNumber: modality.numeroCotizacion
    };
  }

  // Detalle
  setDetails(data): void {
    // Coberturas
    if (data.coberturas) {
      this.coverageList = [];
      data.coberturas.forEach((coverages: ICoverageDetailsResponse) => {
        this.coverageList.push(this.setDetailsModality(coverages));
      });
      this.coverageGroupList = [
        {
          name: this.policyLang.Titles.Coverages,
          coverages: this.coverageList,
          collapse: true
        }
      ];
    }
    // Deducibles
    if (data.otrosDeducibles) {
      this.deductibleList = [];
      data.otrosDeducibles.forEach((deducibles: ICoverageDetailsResponse) => {
        this.deductibleList.push(this.setDetailsModality(deducibles));
      });
    }
  }

  setDetailsModality(coverages: ICoverageDetailsResponse): ICompareDetailView {
    const optionsArray = [];
    let coveragesOptions: Array<ITextCoveragesCompare> = [];
    let deduciblesOptions: Array<ITextCoveragesCompare> = [];
    let deductibles: boolean;
    const products = coverages.productos.slice(0, this.headerLength);
    products.forEach(producto => {
      producto.duracion.forEach(periodo => {
        if (periodo.tiempo === 1) {
          coveragesOptions = [];
          deduciblesOptions = [];
          periodo.deducibles.forEach((valor, i) => {
            // Primera a la vista
            if (i === 0) {
              coveragesOptions.push({
                descripcion: valor.descripcion.titulo,
                tipo: valor.tipo,
                detalles: valor.descripcion.detalles
              });
              // Resto collapsed
            } else {
              deductibles = true;
              deduciblesOptions.push({
                descripcion: valor.descripcion.titulo,
                tipo: valor.tipo,
                detalles: valor.descripcion.detalles
              });
            }
          });
        }
      });
      optionsArray.push({ coverages: coveragesOptions, deductibles: deduciblesOptions });
    });

    return {
      codCoverages: coverages.codigo,
      title: coverages.nombre,
      titleTooltips: coverages.observaciones ? coverages.observaciones.titulo : '',
      contentTooltips: coverages.observaciones ? coverages.observaciones.detalles : [],
      options: optionsArray,
      hasDeductibles: deductibles
    };
  }

  // Datos para Select de numero de cuotas y radio button de cobertura
  setSelectMonth(data): void {
    this.monthlyFees = data[0].duracion[0].cuotas.map(fee => {
      return {
        text: fee.descripcion,
        value: fee.numero.toString()
      };
    });
    const index = data.findIndex(item => item.duracion.length > 1);
    let year = this.yearDefault;
    if (index > -1) {
      this.timeList = data[index].duracion.map(duration => {
        return {
          text: duration.descripcion,
          value: duration.tiempo.toString()
        };
      });
      year = data[index].duracion[0].tiempo.toString();
    } else {
      this.timeList = this.coverageTimeList;
    }
    this.setDataSelect(data[0].duracion[0].cuotas[0].numero, year);
  }

  // Cambiar número de cuotas
  changeMonthlyFee(month): void {
    this.monthSelected = parseInt(month, 10);
    const data = this.quoteService.getHeaderData();
    if (data) {
      this.setHeader(data);
    }
  }

  // Cambiar duracion
  changeTime(value): void {
    this.timeSelected[value.index] = value.years;
    const data = this.quoteService.getHeaderData();
    if (data) {
      this.setHeader(data);
    }
  }
}
