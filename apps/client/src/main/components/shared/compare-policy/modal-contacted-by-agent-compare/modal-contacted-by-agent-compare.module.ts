import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormContactedByAgentModule } from '@mx/components/shared/form-contacted-by-agent/form-contacted-by-agent.module';
import { MfButtonModule } from '@mx/core/ui/lib/components/forms/button';
import { MfModalErrorModule } from '@mx/core/ui/lib/components/modals/modal-error';
import { MfModalModule } from '@mx/core/ui/lib/components/modals/modal/modal.module';
import { ModalContactedByAgentCompareComponent } from './modal-contacted-by-agent-compare.component';

@NgModule({
  imports: [CommonModule, RouterModule, MfModalModule, MfButtonModule, FormContactedByAgentModule, MfModalErrorModule],
  exports: [ModalContactedByAgentCompareComponent],
  declarations: [ModalContactedByAgentCompareComponent]
})
export class ModalContactedByAgentCompareModule {}
