import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { NavbarFixedModule } from '@mx/core/ui';
import { CarouselModule } from '@mx/core/ui/lib/components/carousel/carousel.module';
import { MfButtonModule } from '@mx/core/ui/lib/components/forms/button';
import { MfSelectModule } from '@mx/core/ui/lib/components/forms/select';
import { MfTabTopModule } from '@mx/core/ui/lib/components/tabs/tabs-top/tab-top.module';
import { TooltipsModule } from '@mx/core/ui/lib/components/tooltips/tooltips.module';
import { DirectivesModule } from '@mx/core/ui/lib/directives/directives.module';
import { PoliciesCompareComponent } from './policies-compare.component';

@NgModule({
  imports: [
    CarouselModule,
    CommonModule,
    DirectivesModule,
    FormsModule,
    GoogleModule,
    MfButtonModule,
    MfSelectModule,
    MfTabTopModule,
    NavbarFixedModule,
    ReactiveFormsModule,
    TooltipsModule,
    CarouselModule,
    NavbarFixedModule,
    GoogleModule
  ],
  exports: [PoliciesCompareComponent],
  declarations: [PoliciesCompareComponent]
})
export class PoliciesCompareModule {}
