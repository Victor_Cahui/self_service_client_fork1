import { DecimalPipe } from '@angular/common';
import { ArrayUtil } from '@mx/core/shared/helpers/util/array';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PolicyListService } from '@mx/services/policy/policy-list.service';
import { PoliciesQuoteService } from '@mx/services/policy/policy-quote.service';
import { ARROW_DOWN_ICON, ARROW_RIGHT_ICON } from '@mx/settings/constants/images-values';
import { POLICY_INDICATOR } from '@mx/settings/constants/policy-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { COVERAGES_TIME_LIST, PolicyLang, YEAR_DEFAULT } from '@mx/settings/lang/policy.lang';
import { ISelectList } from '@mx/statemanagement/models/general.models';
import {
  ICompareDetailView,
  ICompareHeaderView,
  ICoverageGroupView,
  ICoveragesPolicyRequest
} from '@mx/statemanagement/models/policy.interface';
import { ITabItem } from '@mx/statemanagement/models/router.interface';
import { Subscription } from 'rxjs';
import { isArray } from 'util';

export abstract class BaseComparePolicies extends GaUnsubscribeBase {
  policySelected: string;
  policyCurrent: string;
  policyNew: string;
  monthlyFees: Array<ISelectList>;
  coveragesHeader: Array<ICompareHeaderView>;
  coverageList: Array<ICompareDetailView>;
  deductibleList: Array<ICompareDetailView>;
  coverageGroupList: Array<ICoverageGroupView>;
  timeList: Array<ISelectList>;
  list: Array<ITabItem>;
  policyIndicator = POLICY_INDICATOR;
  monthSelected: number;
  timeSelected: Array<string>;
  compare: boolean;

  yes = GeneralLang.Buttons.Yes;
  no = GeneralLang.Buttons.No;
  policyLang = PolicyLang;
  yearDefault = YEAR_DEFAULT;
  coverageTimeList = COVERAGES_TIME_LIST;
  showLoading: boolean;
  showSpinner: boolean;
  openModalConfirm: boolean;
  openModalDone: boolean;
  showChangeModality: boolean;
  done: boolean;
  headerLength: number;
  titleDone: string;
  messageDone: string;
  labelModality: string;
  textChangeModalityFrom: string;
  textPaymentFrecuency: string;
  compareHeaderSub: Subscription;
  compareDetailsSub: Subscription;
  zipSub: Subscription;
  params: ICoveragesPolicyRequest;
  compareHeader$: any;
  compareDetails$: any;

  arrowRigthIcon = ARROW_RIGHT_ICON;
  arrowDownIcon = ARROW_DOWN_ICON;

  constructor(
    protected policyListService: PolicyListService,
    protected headerHelperService: HeaderHelperService,
    protected quoteService: PoliciesQuoteService,
    protected decimalPipe: DecimalPipe,
    protected gaService?: GAService
  ) {
    super(gaService);
  }

  trackByFn(index): any {
    return index;
  }

  setDataSelect(month: number, year: string): void {
    const choise = this.quoteService.getChoiseData();
    if (choise) {
      this.monthSelected = choise.month;
      this.timeSelected = choise.years;
    } else {
      this.monthSelected = month;
      this.timeSelected = [year, year, year];
    }
  }

  // Monto de Cuotas
  getMonthlyCost(modality, month, year): number {
    const quote = this.getSelected(modality, month, year);

    return quote.monto || 0;
  }

  getQuoteCost(modality, month, year): number {
    const quote = this.getSelected(modality, month, year);

    return quote.prima || 0;
  }

  getSelected(modality, month, year): any {
    let data;
    if (isArray(modality)) {
      const time = modality.findIndex(t => t.tiempo === year) || 0;
      if (time < 0) {
        return 0;
      }
      if (!modality[time]) {
        return 0;
      }
      data = modality[time];
    } else {
      data = modality;
    }

    const fee = data.cuotas.findIndex(f => f.numero === month) || 0;
    if (fee < 0) {
      return 0;
    }

    return data.cuotas[fee] || 0;
  }

  getModality(modality, year): string {
    let data;
    if (isArray(modality)) {
      const time = modality.findIndex(t => t.tiempo === year) || 0;
      if (time < 0) {
        return '';
      }
      data = modality[time];
    } else {
      data = modality;
    }

    return data.codigoModalidad || '';
  }

  getFractionationCode(modality, month, year): string {
    const quote = this.getSelected(modality, month, year);

    return quote.codigoFraccionamiento || '';
  }

  // Tiempo de contratacion para pintar radio button
  getHiringTime(time): boolean {
    return time && time.length > 1;
  }

  // Texto Valor Mensual: Valor Mensual
  getTextValue(): string {
    return PolicyLang.Labels.MonthlyCost.replace('{{periodo}}', this.getTextFrecuency()) || '';
  }

  // Text Periodo
  getTextFrecuency(): string {
    return ArrayUtil.getTextfromList(this.monthlyFees, this.monthSelected.toString()).toLowerCase();
  }

  getTextPaymentFrecuency(): string {
    const text =
      this.monthSelected === 1
        ? `${PolicyLang.Texts.Fee.toLowerCase()} ${this.getTextFrecuency()}`
        : `${PolicyLang.Texts.Fees} ${this.getTextFrecuency()}es`;

    return `${this.monthSelected} ${text}`;
  }

  // Texto par Comparacion: $ 85 - Valor mensual
  getTextSumaryValue(symbol, cost): string {
    return `${symbol} ${this.decimalPipe.transform(cost, '1.2-2')} - ${this.getTextValue()}`;
  }

  // Texto Cambiar a partir de
  setChangeModalityFrom(): void {
    this.textChangeModalityFrom = '';
    this.showChangeModality = true;
    const data = this.policyListService.getDataPolicies();
    if (data) {
      const policy = data.filter(item => item.policyNumber === this.policySelected);
      if (policy && policy.length) {
        this.showChangeModality = policy[0].compare && policy[0].compare.changeModality;
        this.textChangeModalityFrom = this.showChangeModality
          ? ''
          : `${PolicyLang.Texts.changeModalityFrom} ${policy[0].compare.nextChangeFrom}`;
      }
    }
  }
}
