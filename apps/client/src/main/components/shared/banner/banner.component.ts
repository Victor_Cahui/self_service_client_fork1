import { Component, ElementRef, Input, OnInit, Renderer2, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { validURL } from '@mx/core/shared/helpers/functions/general-functions';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { UnsubscribeOnDestroy } from '@mx/core/shared/helpers/util/unsubscribe-on-destroy';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { MFP_Promo_Click_6D, MFP_Promo_View_Load_6C } from '@mx/settings/constants/events.analytics';
import { IObtenerConfiguracionResponse } from '@mx/statemanagement/models/client.models';
import { IPathImageSize } from '@mx/statemanagement/models/general.models';
import { Subscription, timer } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'client-banner',
  templateUrl: './banner.component.html'
})
export class BannerComponent extends UnsubscribeOnDestroy implements OnInit {
  urlLink: string;
  staticBannersPath: string;
  dataBanner: IObtenerConfiguracionResponse;
  eventResizeSub: Subscription;
  existUrlImage = true;
  pathImgBanner: IPathImageSize = { sm: '', lg: '' };
  isValidLink: boolean;
  showBanner: boolean;

  @ViewChild('linkBanner') linkBanner: ElementRef;
  @Input() staticlImgs: IPathImageSize;
  @Input() urlRedirect: string;
  @Input() openInCurrentTab: boolean;

  constructor(
    protected elmentRef: ElementRef,
    private readonly configurationService: ConfigurationService,
    private readonly renderer2: Renderer2,
    protected gaService: GAService,
    private readonly router: Router
  ) {
    super();
  }

  ngOnInit(): void {
    if (this.staticlImgs) {
      this.pathImgBanner = this.staticlImgs;
      this.isValidLink = true;
      this.showBanner = true;
    } else {
      this.configurationService
        .getConfiguration()
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(
          (response: IObtenerConfiguracionResponse) => {
            this.dataBanner = response;
            if (this.dataBanner) {
              if (this.dataBanner.carruseles && this.dataBanner.carruseles[0].listaBanners) {
                this.urlLink = this.dataBanner.carruseles[0].listaBanners[0].enlaceUrl || null;
                this.pathImgBanner = {
                  sm: this.dataBanner.carruseles[0].listaBanners[0].mobile,
                  md: this.dataBanner.carruseles[0].listaBanners[0].tablet,
                  lg: this.dataBanner.carruseles[0].listaBanners[0].desktop,
                  xl: this.dataBanner.carruseles[0].listaBanners[0].desktopFull
                };
              }

              this.isValidLink = this.urlLink && validURL(this.urlLink);
              this.showBanner = !this.urlLink || this.isValidLink;

              // Si tiene URL valido
              if (this.isValidLink) {
                timer(100)
                  .toPromise()
                  .then(() => {
                    if (this.linkBanner && this.linkBanner.nativeElement) {
                      this.renderer2.setAttribute(this.linkBanner.nativeElement, 'href', this.urlLink);
                      this.renderer2.setAttribute(this.linkBanner.nativeElement, 'target', '_blank');
                    }
                  });
              }
            }
          },
          () => {}
        );
    }
  }

  loadEventGa(click?: boolean): void {
    const image = this.elmentRef.nativeElement.querySelector('img')['src'];
    const name = image.match(/[^/]*$/);
    const section = this.gaService.getSection();
    const data = { id: image, name: name[0], creative: section, position: '1' };
    this.gaService.add(click ? MFP_Promo_Click_6D(data) : MFP_Promo_View_Load_6C(data));
  }

  clickBanner(): void {
    this.loadEventGa(true);
    if (!this.urlRedirect) {
      return void 0;
    }

    timer(100)
      .toPromise()
      .then(() => {
        const to = this.openInCurrentTab ? '_selft' : '_blank';
        window.open(this.urlRedirect, to);
      });
  }

  notLoading(error?: boolean): void {
    if (this.linkBanner && this.linkBanner.nativeElement) {
      this.existUrlImage = error;
      if (error) {
        this.renderer2.addClass(this.linkBanner.nativeElement, 'd-block');
      } else {
        this.renderer2.removeClass(this.linkBanner.nativeElement, 'd-block');
      }
    } else {
      if (this.existUrlImage) {
        timer(100)
          .toPromise()
          .then(() => {
            this.existUrlImage = error;
          });
      }
    }

    if (error && this.existUrlImage) {
      this.loadEventGa();
    }
  }
}
