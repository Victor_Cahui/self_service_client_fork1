import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule } from '@mx/core/ui';
import { BannerComponent } from './banner.component';

@NgModule({
  imports: [CommonModule, DirectivesModule, GoogleModule],
  declarations: [BannerComponent],
  exports: [BannerComponent]
})
export class BannerModule {}
