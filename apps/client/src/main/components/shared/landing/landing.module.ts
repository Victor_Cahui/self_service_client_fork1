import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { MfSelectModule } from '@mx/core/ui/lib/components/forms/select';
import { TravelEmergencyPhonesModule } from '../travel-emergency-phones/travel-emergency-phones.module';
import { LandingComponent } from './landing.component';

@NgModule({
  imports: [CommonModule, MfSelectModule, TravelEmergencyPhonesModule, GoogleModule],
  exports: [LandingComponent],
  declarations: [LandingComponent],
  providers: []
})
export class LandingModule {}
