import { Component, EventEmitter, Input, Output } from '@angular/core';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google/google.gabase';
import { ContentInsurancePlans } from '@mx/statemanagement/models/general.models';

@Component({
  selector: 'client-insurance-plans',
  templateUrl: './card-insurance-plans.component.html',
  styles: [
    `
      /deep/ .carousel-indicators {
        bottom: -15px;
        padding-bottom: 5px;
      }
      /deep/ .mx-carousel-item {
        margin-bottom: 40px;
      }
    `
  ]
})
export class CardInsurancePlansComponent extends GaUnsubscribeBase {
  @Input() content: ContentInsurancePlans;
  @Output() eventQuote: EventEmitter<any>;
  @Output() eventInfo: EventEmitter<any>;

  constructor(protected gaService: GAService) {
    super(gaService);
    this.eventQuote = new EventEmitter<any>();
    this.eventInfo = new EventEmitter<any>();
  }

  eventQuoteClick(): void {
    this.eventQuote.next();
  }

  eventInfoClick(): void {
    this.eventInfo.next();
  }
}
