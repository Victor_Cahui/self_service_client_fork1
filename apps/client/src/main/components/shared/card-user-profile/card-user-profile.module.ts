import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ModalChangePasswordModule } from '@mx/components/profile/modal-change-password/modal-change-password.module';
import { TruncateDecimalsPipe } from '@mx/core/shared/common/pipes/truncatedecimals.pipe';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { TooltipsModule } from '@mx/core/ui/lib/components/tooltips/tooltips.module';
import { EditEmailModule } from '../../profile/edit-email/edit-email.module';
import { CardUserProfileComponent } from './card-user-profile.component';
import { UserMapfreDolarsComponent } from './user-mapfre-dolars/user-mapfre-dolars.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserTypeComponent } from './user-type/user-type.component';

@NgModule({
  imports: [CommonModule, EditEmailModule, MfLoaderModule, TooltipsModule, ModalChangePasswordModule, GoogleModule],
  exports: [CardUserProfileComponent],
  declarations: [
    CardUserProfileComponent,
    UserProfileComponent,
    UserTypeComponent,
    UserMapfreDolarsComponent,
    TruncateDecimalsPipe
  ],
  providers: []
})
export class CardUserProfileModule {}
