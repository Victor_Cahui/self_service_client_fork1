import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MfInputModule, MfSelectModule, MfShowErrorsModule } from '@mx/core/ui';
import { InputPasswordComponent } from './input-password.component';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, MfInputModule, MfSelectModule, MfShowErrorsModule],
  declarations: [InputPasswordComponent],
  exports: [InputPasswordComponent]
})
export class InputPasswordModule {}
