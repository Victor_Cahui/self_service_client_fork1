import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule, MfButtonModule, MfInputModule, MfSelectModule, MfShowErrorsModule } from '@mx/core/ui';
import { FormRequestRefundComponent } from './form-request-refund.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DirectivesModule,
    MfInputModule,
    MfButtonModule,
    MfSelectModule,
    MfShowErrorsModule,
    GoogleModule
  ],
  exports: [FormRequestRefundComponent],
  declarations: [FormRequestRefundComponent]
})
export class FormRequestRefundModule {}
