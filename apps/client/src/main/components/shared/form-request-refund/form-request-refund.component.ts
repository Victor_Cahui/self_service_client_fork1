import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FileUtil } from '@mx/core/shared/helpers/util/file';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google/google.gabase';
import { SelectListItem } from '@mx/core/ui';
import { MfInputComponent } from '@mx/core/ui/lib/components/forms/input/input.component';
import { AuthService } from '@mx/services/auth/auth.service';
import { HealthRefundService } from '@mx/services/health/health-refund.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { HealthLang } from '@mx/settings/lang/health.lang';
import { GeneralRequest } from '@mx/statemanagement/models/general.models';
import {
  IGetPdfRefundFormatRequest,
  IRefundHealth,
  IRefundPolicyListResponse
} from '@mx/statemanagement/models/health.interface';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { Subject, Subscription } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/internal/operators/distinctUntilChanged';
import { takeUntil } from 'rxjs/internal/operators/takeUntil';

@Component({
  selector: 'client-form-request-refund',
  templateUrl: './form-request-refund.component.html'
})
export class FormRequestRefundComponent extends GaUnsubscribeBase implements OnInit, OnDestroy {
  @ViewChild(MfInputComponent) inputPlate: MfInputComponent;
  @Output() getSelectedPolicy: EventEmitter<any> = new EventEmitter<any>();

  policyOptions: Array<SelectListItem> = [];
  beneficiaryOptions: Array<SelectListItem> = [];

  getRefundPolicyListSub: Subscription;
  getPDFRefundFormatSub: Subscription;
  policyListResponse: Array<IRefundPolicyListResponse> = [];
  currentPolicySelected: IRefundPolicyListResponse;
  fileUtil: FileUtil;

  formRefund: FormGroup;

  showLoading: boolean;

  formTitle = HealthLang.Titles.RefundForm;
  policyLabel = HealthLang.Labels.PolicySelection;
  patientLabel = HealthLang.Labels.PatientSelection;
  downaloadFormatButton = HealthLang.Buttons.FormatDownload;
  requestRefundButton = HealthLang.Buttons.RequestRefund;

  auth: IdDocument;

  isFirstTime: boolean;

  private readonly _destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private readonly authService: AuthService,
    private readonly healthRefundService: HealthRefundService,
    private readonly formBuilder: FormBuilder,
    private readonly router: Router,
    protected gaService?: GAService
  ) {
    super(gaService);
    this.fileUtil = new FileUtil();
  }

  ngOnInit(): void {
    this.createForm();
    this.auth = this.authService.retrieveEntity();
    this.onPolicyChange();
    this.getRefundPolicies();
  }

  createForm(): void {
    this.formRefund = this.formBuilder.group({
      policy: ['', Validators.required],
      patient: ['', Validators.required]
    });
  }

  private setDefaultData(): void {
    const refund = this.healthRefundService.getRefund();
    if (refund && refund.policy && refund.beneficiary && refund.beneficiary.document && refund.policy.policyNumber) {
      this.formRefund.controls['policy'].setValue(refund.policy.policyNumber);
      if (this.currentPolicySelected) {
        this.currentPolicySelected.codCia = refund.policy.codCia;
        this.currentPolicySelected.codRamo = refund.policy.codRamo;
        this.currentPolicySelected.codModalidad = refund.policy.codModality;
        this.formRefund.controls['patient'].setValue(refund.beneficiary.document);
      }
    }
    this.healthRefundService.setRefund(null);
  }

  getRefundPolicies(): void {
    const params: GeneralRequest = {
      codigoApp: APPLICATION_CODE
    };
    this.getRefundPolicyListSub = this.healthRefundService.getRefundPolicyList(params).subscribe(
      response => {
        this.policyListResponse = response || [];
        this.policyListResponse.forEach(policy => {
          const option = {
            text: `${policy.descripcionPoliza} - ${policy.numeroPoliza}`,
            value: policy.numeroPoliza,
            selected: true,
            disabled: false
          };

          this.policyOptions.push(option);
        });
        if (this.policyOptions.length === 1) {
          this.formRefund.controls['policy'].setValue(this.policyOptions[0].value);
        }
        this.setDefaultData();
      },
      error => {
        this.getRefundPolicyListSub.unsubscribe();
      }
    );
  }

  onPolicyChange(): void {
    this.formRefund.controls['policy'].valueChanges
      .pipe(
        takeUntil(this._destroy$),
        distinctUntilChanged()
      )
      .subscribe(value => {
        this.beneficiaryOptions = [];
        if (this.policyListResponse && this.policyListResponse.length) {
          this.currentPolicySelected = this.policyListResponse.find(policy => policy.numeroPoliza === value);
          if (this.currentPolicySelected) {
            this.getSelectedPolicy.emit(this.currentPolicySelected);
            const beneficiaries = this.currentPolicySelected.beneficiarios || [];
            beneficiaries.forEach((beneficiary, index) => {
              const type = {
                text: beneficiary.nombreBeneficiario,
                value: beneficiary.numeroDocumento,
                selected: true,
                disabled: false
              };

              this.beneficiaryOptions.push(type);
              if (!index) {
                this.formRefund.controls['patient'].setValue(type.value);
                this.formRefund.controls['patient'].enable();
              }
            });
          }
        }
      });
  }

  downloadPDF(): void {
    const params: IGetPdfRefundFormatRequest = {
      codigoApp: APPLICATION_CODE,
      codCia: this.currentPolicySelected.codCia,
      codRamo: this.currentPolicySelected.codRamo,
      codModalidad: this.currentPolicySelected.codModalidad
    };
    this.showLoading = true;
    this.getPDFRefundFormatSub = this.healthRefundService.getPDFRefundFormat(params).subscribe(
      record => {
        const fileName = `${HealthLang.Texts.RefundPdfFile}${this.currentPolicySelected.numeroPoliza}`;
        this.fileUtil.download(record.base64, 'pdf', fileName);
        this.pdfRefundUnsubscribe();
      },
      () => {
        this.pdfRefundUnsubscribe();
      }
    );
  }

  pdfRefundUnsubscribe(): void {
    this.showLoading = false;
    this.getPDFRefundFormatSub.unsubscribe();
  }

  requestRefund(): void {
    const refund: IRefundHealth = {};

    refund.policy = {
      policyNumber: this.formRefund.controls['policy'].value,
      codCia: this.currentPolicySelected.codCia,
      codRamo: this.currentPolicySelected.codRamo,
      codModality: this.currentPolicySelected.codModalidad,
      description: this.currentPolicySelected.descripcionPoliza,
      type: this.currentPolicySelected.tipoPoliza
    };

    const codeBeneficiary = this.formRefund.controls['patient'].value;
    const beneficiary = this.currentPolicySelected.beneficiarios.find(
      beneficiario => beneficiario.numeroDocumento === codeBeneficiary
    );

    refund.beneficiary = {
      documentType: beneficiary.tipoDocumento,
      document: beneficiary.numeroDocumento,
      name: beneficiary.nombreBeneficiario,
      type: beneficiary.tipoBeneficiario
    };

    this.healthRefundService.setRefund(refund);
    this.router.navigate(['/health/refund/request/step1']);
  }

  ngOnDestroy(): void {
    this._destroy$.next(true);
    this._destroy$.complete();

    if (this.getRefundPolicyListSub) {
      this.getRefundPolicyListSub.unsubscribe();
    }
    if (this.getPDFRefundFormatSub) {
      this.getPDFRefundFormatSub.unsubscribe();
    }
  }
}
