import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BankPaymentModule } from '@mx/components/shared/payments/bank-payment/bank-payment.module';
import { MfAlertWarningModule } from '@mx/core/ui/lib/components/alerts/alert-warning/alert-warning.module';
import { TooltipsModule } from '@mx/core/ui/lib/components/tooltips/tooltips.module';
import { CardSctrPeriodConfirmComponent } from './card-sctr-period-confirm.component';

@NgModule({
  imports: [CommonModule, BankPaymentModule, MfAlertWarningModule, TooltipsModule],
  declarations: [CardSctrPeriodConfirmComponent],
  exports: [CardSctrPeriodConfirmComponent]
})
export class CardSctrPeriodConfirModule {}
