import { Component, Input } from '@angular/core';
import { COVERAGES_SUBLIMITS } from '@mx/settings/lang/household.lang';
import { IHomeCoverageSublimitResponse } from '@mx/statemanagement/models/home.interface';

@Component({
  selector: 'client-sublimits-table',
  templateUrl: './sublimits-table.component.html'
})
export class SublimitsTableComponent {
  @Input() sublimitList: Array<IHomeCoverageSublimitResponse>;

  content = COVERAGES_SUBLIMITS;

  trackByFn(index): number {
    return index;
  }
}
