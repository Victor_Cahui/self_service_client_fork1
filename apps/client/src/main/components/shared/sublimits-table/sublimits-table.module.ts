import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SublimitsTableComponent } from './sublimits-table.component';

@NgModule({
  imports: [CommonModule, RouterModule],
  exports: [SublimitsTableComponent],
  declarations: [SublimitsTableComponent]
})
export class SublimitsTableModule {}
