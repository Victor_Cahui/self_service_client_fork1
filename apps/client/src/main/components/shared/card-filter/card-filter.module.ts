import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';
import { LinksModule } from '@mx/core/ui/lib/links/links.module';
import { CardFilterComponent } from './card-filter.component';

@NgModule({
  imports: [CommonModule, MfCardModule, DirectivesModule, FormsModule, LinksModule, ReactiveFormsModule, GoogleModule],
  exports: [CardFilterComponent],
  declarations: [CardFilterComponent]
})
export class CardFilterModule {}
