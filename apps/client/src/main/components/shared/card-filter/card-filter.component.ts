import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';

import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { ICardFilterOption, IConfigUICardViewItemList } from '@mx/statemanagement/models/general.models';

@Component({
  selector: 'client-card-filter-component',
  templateUrl: './card-filter.component.html'
})
export class CardFilterComponent extends GaUnsubscribeBase implements OnDestroy, OnInit {
  @Input() titleCard: string;
  @Input() nameSelect: string;
  @Input() options: Array<ICardFilterOption>;
  @Input() configUI: IConfigUICardViewItemList;
  @Input() totalRecords: number;
  @Output() value: EventEmitter<any>;
  changesSub: Subscription;
  formFilter: FormGroup;
  typeSelect: AbstractControl;
  collapse = false;
  lengthRecords = 5;
  generalLang = GeneralLang;

  constructor(private readonly formBuilder: FormBuilder, protected gaService: GAService) {
    super(gaService);
    this.value = new EventEmitter<any>();
  }

  ngOnInit(): void {
    this.createForm();
    this.onChanges();
  }

  ngOnDestroy(): void {
    if (!!this.changesSub) {
      this.changesSub.unsubscribe();
    }
  }

  createForm(): void {
    this.formFilter = this.formBuilder.group({
      typeSelect: ['']
    });
    this.typeSelect = this.formFilter.controls['typeSelect'];
  }

  onChanges(): void {
    this.changesSub = this.typeSelect.valueChanges.subscribe(val => {
      this.value.emit(val);
    });
  }
}
