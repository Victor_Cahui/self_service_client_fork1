import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardPaymentOptionsComponent } from './card-payment-options.component';

@NgModule({
  imports: [CommonModule],
  exports: [CardPaymentOptionsComponent],
  declarations: [CardPaymentOptionsComponent]
})
export class CardPaymentOptionsModule {}
