import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule, MfButtonModule, MfCheckboxModule, MfInputModule, MfShowErrorsModule } from '@mx/core/ui';
import { FormEditPlateQuoteComponent } from './form-edit-plate-quote.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    GoogleModule,
    ReactiveFormsModule,
    DirectivesModule,
    MfInputModule,
    MfButtonModule,
    MfCheckboxModule,
    MfShowErrorsModule
  ],
  exports: [FormEditPlateQuoteComponent],
  declarations: [FormEditPlateQuoteComponent]
})
export class FormEditPlateQuoteModule {}
