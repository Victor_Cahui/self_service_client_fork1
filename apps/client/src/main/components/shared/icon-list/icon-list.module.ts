import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IconListComponent } from './icon-list.component';

@NgModule({
  imports: [CommonModule],
  exports: [IconListComponent],
  declarations: [IconListComponent]
})
export class IconListModule {}
