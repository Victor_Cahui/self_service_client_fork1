import { Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

export interface BtnConfig {
  disabled?: boolean;
  txt: string;
  class?: string;
  iconClass?: string;
  action(): void;
}

export abstract class ButtonBaseComponent implements OnInit, OnChanges {
  @Input() public config: BtnConfig;
  @Input() public disabled: boolean;
  @Input() public action: VoidFunction;

  public ngOnInit(): void {
    const isDisableFromConfig: boolean = this.config ? this.config.disabled : false;
    // tslint:disable-next-line: no-unbound-method
    const actionFromConfig: VoidFunction = this.config ? this.config.action : () => void 0;
    const isDisabledBtn: boolean = this.disabled ? this.disabled : isDisableFromConfig;
    const action: VoidFunction = this.action ? this.action : actionFromConfig;
    this.config = { ...this.config, disabled: isDisabledBtn, action };
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.disabled) {
      this.config = { ...this.config, disabled: changes.disabled.currentValue };
    }
  }
}
