import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DirectivesModule } from '@mx/core/ui/lib/directives/directives.module';
import { BannerAccordionComponent } from './banner-accordion.component';

@NgModule({
  imports: [CommonModule, DirectivesModule],
  exports: [BannerAccordionComponent],
  declarations: [BannerAccordionComponent]
})
export class BannerAccordionModule {}
