import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { IBannerAccordion } from '@mx/statemanagement/models/general.models';

@Component({
  selector: 'client-banner-accordion',
  templateUrl: './banner-accordion.component.html'
})
export class BannerAccordionComponent implements OnInit {
  collapse = true;
  generalLang = GeneralLang;

  @Input() bannerAccordion: IBannerAccordion;

  @HostBinding('attr.class') attr_class = 'map-acordion';

  constructor(protected router: Router) {}

  ngOnInit(): void {}

  toogle(): void {
    this.collapse = !this.collapse;
  }

  onClick(): void {
    if (this.bannerAccordion.endLink) {
      const url = this.bannerAccordion.endLink.url || '';
      this.bannerAccordion.endLink.isExternal ? window.open(url, '_blank') : this.router.navigate([url]);
    }
  }
}
