import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LinksModule } from '@mx/core/ui/lib/links/links.module';
import { GmFilterNotifierComponent } from './gm-filter-notifier.component';

@NgModule({
  imports: [CommonModule, LinksModule],
  exports: [GmFilterNotifierComponent],
  declarations: [GmFilterNotifierComponent]
})
export class GmFilterNotifierModule {}
