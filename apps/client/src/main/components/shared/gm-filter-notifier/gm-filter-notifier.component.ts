import { Component, Input, OnInit } from '@angular/core';
import { GeneralLang } from '@mx/settings/lang/general.lang';

@Component({
  selector: 'client-gm-filter-notifier',
  templateUrl: './gm-filter-notifier.component.html'
})
export class GmFilterNotifierComponent implements OnInit {
  @Input() text = GeneralLang.Messages.FiltersActived;
  @Input() icon = 'icon-mapfre_057_circle-check';
  @Input() customClass = '';

  /**
   * Duración del popup en segundos
   */
  @Input() time = 5;

  ngOnInit(): void {}
}
