import { Component, Input, OnInit } from '@angular/core';
import { getDay, getMonth } from '@mx/core/shared/helpers/util/date';
import { FileUtil } from '@mx/core/shared/helpers/util/file';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { ITimelimeStep } from '@mx/statemanagement/models/timeline.interface';

@Component({
  selector: 'client-card-timeline',
  templateUrl: './card-timeline.component.html'
})
export class CardTimelineComponent implements OnInit {
  @Input() steps: Array<ITimelimeStep>;
  viewMoreLang = GeneralLang.Buttons.ViewMore;
  fileUtil: FileUtil;

  constructor() {
    this.fileUtil = new FileUtil();
  }

  ngOnInit(): void {}

  calculateDay(myDate: string): string {
    return getDay(myDate);
  }

  calculateMonth(myDate: string): string {
    // tslint:disable-next-line: no-parameter-reassignment
    myDate = getMonth(myDate);

    return myDate ? myDate.toLowerCase() : '';
  }

  downloadFiles(files: Array<any> = []): void {
    files.forEach(file => {
      const type = file.fileName.split('.')[1] || 'pdf';
      const name = file.fileName.split('.')[0];
      this.fileUtil.download(file.base64, type, `${name}.${type}`);
    });
  }
}
