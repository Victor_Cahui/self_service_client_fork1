import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule } from '@mx/core/ui';
import { ItemRequestComponent } from './item-request.component';

@NgModule({
  imports: [CommonModule, RouterModule, DirectivesModule, GoogleModule],
  exports: [ItemRequestComponent],
  declarations: [ItemRequestComponent]
})
export class ItemRequestModule {}
