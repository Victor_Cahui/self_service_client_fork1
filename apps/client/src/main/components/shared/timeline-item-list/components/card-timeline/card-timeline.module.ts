import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardTimelineComponent } from './card-timeline.component';

@NgModule({
  imports: [CommonModule],
  declarations: [CardTimelineComponent],
  exports: [CardTimelineComponent]
})
export class CardTimelineModule {}
