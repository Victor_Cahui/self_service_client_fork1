import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { MFP_Accidentes_25B } from '@mx/settings/constants/events.analytics';
import { NO_PENDING_PROCEDURES_ICON } from '@mx/settings/constants/images-values';
import { ASSISTANCE } from '@mx/settings/lang/vehicles.lang';
import { IRequestItemView } from '@mx/statemanagement/models/timeline.interface';

@Component({
  selector: 'client-card-request-process',
  templateUrl: './card-request-process.component.html'
})
export class CardRequestProcessComponent implements OnInit {
  ga: Array<IGaPropertie>;
  @Input() items: Array<IRequestItemView>;
  @Input() numberLabel: string;
  @Input() textNotFoundProcess = ASSISTANCE.NoRegister;
  @Output() viewDetail: EventEmitter<IRequestItemView>;

  lang = ASSISTANCE;
  collapse = false;
  icon = NO_PENDING_PROCEDURES_ICON;

  constructor() {
    this.ga = [MFP_Accidentes_25B()];
    this.viewDetail = new EventEmitter<IRequestItemView>();
  }

  ngOnInit(): void {}

  viewDetailFn(item: IRequestItemView): void {
    this.viewDetail.next(item);
  }

  collapseFn(index: number): void {
    this.items.forEach((item, i) => {
      if (i !== index) {
        item.collapse = true;
      }
    });
  }
}
