import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ItemNotFoundModule } from '@mx/components/shared/item-not-found/item-not-found.module';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { CardRequestHistoryModule } from '../components/card-request-history/card-request-history.module';
import { CardRequestProcessModule } from '../components/card-request-process/card-request-process.module';
import { ViewItemListComponent } from './view-item-list.component';

@NgModule({
  imports: [CommonModule, ItemNotFoundModule, MfLoaderModule, CardRequestHistoryModule, CardRequestProcessModule],
  declarations: [ViewItemListComponent],
  exports: [ViewItemListComponent]
})
export class ViewItemListModule {}
