import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google/google.gabase';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { Faq } from '@mx/statemanagement/models/general.models';

@Component({
  selector: 'client-faqs',
  templateUrl: './card-faq.component.html',
  styles: [
    `
      .masonry {
        column-count: 2;
        column-gap: 1em;
      }
      .item {
        display: inline-block;
        margin: 0 0 1em;
        width: 100%;
      }
      @media only screen and (max-width: 967px) {
        .masonry {
          -moz-column-count: 1;
          -webkit-column-count: 1;
          column-count: 1;
        }
      }
      @media only screen and (min-width: 968px) {
        .masonry {
          -moz-column-count: 2;
          -webkit-column-count: 2;
          column-count: 2;
        }
      }
    `
  ]
})
export class CardFaqComponent extends GaUnsubscribeBase implements OnInit {
  @Output() more: EventEmitter<any>;
  @Input() faqs: Array<Faq>;
  @Input() masonry: boolean;

  title = GeneralLang.Titles.Faq;
  linkMoreFaq = GeneralLang.Buttons.MoreFaq;

  constructor(protected gaService: GAService) {
    super(gaService);
    this.more = new EventEmitter<any>();
  }

  ngOnInit(): void {
    this.faqs = this.faqs.filter(faq => faq.landing).sort((a, b) => a.order - b.order);
  }

  moreFaqs(): void {
    this.more.next();
  }
}
