import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MfCustomAlertModule } from '@mx/core/ui/lib/components/alerts/custom-alert';
import { LinksModule } from '@mx/core/ui/lib/links/links.module';
import { CardContentComponent } from './card-content.component';

@NgModule({
  imports: [CommonModule, RouterModule, MfCustomAlertModule, LinksModule],
  exports: [CardContentComponent],
  declarations: [CardContentComponent]
})
export class CardContentModule {}
