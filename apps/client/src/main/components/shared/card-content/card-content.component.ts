import { Component, HostBinding, Input } from '@angular/core';
import { Params, Router } from '@angular/router';
import { NotificationStatus } from '@mx/core/ui/lib/components/alerts/custom-alert/custom-alert.component';
import { Content } from '../../../../statemanagement/models/general.models';

@Component({
  selector: 'client-card-content',
  templateUrl: './card-content.component.html'
})
export class CardContentComponent {
  @HostBinding('attr.class') class = 'w-100';
  @Input() content: Content;
  @Input() hidenLinkMap: Boolean;
  @Input() queryParams: Params;
  status = NotificationStatus.INFO;

  constructor(private readonly router: Router) {}

  trackByFn(index, item): number {
    return item;
  }

  routerTo(): void {
    this.router.navigate([this.content.action.routeLink], { queryParams: this.queryParams });
  }
}
