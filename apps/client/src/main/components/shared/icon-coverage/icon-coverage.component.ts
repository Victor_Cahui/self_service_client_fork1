import {
  AfterViewInit,
  Component,
  ElementRef,
  HostBinding,
  Input,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild
} from '@angular/core';
import { Router } from '@angular/router';
import { ResizeService } from '@mx/core/shared/helpers/events/resize.service';
import { COVERAGES_ACTIONS } from '@mx/settings/constants/coverage-values';
import { ICoverageItem } from '@mx/statemanagement/models/coverage.interface';
import { Subscription, timer } from 'rxjs';

@Component({
  selector: 'client-icon-coverage',
  templateUrl: './icon-coverage.component.html'
})
export class IconCoverageComponent implements AfterViewInit, OnDestroy, OnInit {
  @HostBinding('class') classContent = 'w-50 w-md-2-5 mb-4';
  @Input('item') item: ICoverageItem;
  @ViewChild('title') title: ElementRef;

  resizeSub: Subscription;
  windowWidth: number;
  titleText: string;
  ellipsisBool: boolean;

  constructor(
    private readonly resizeService: ResizeService,
    public renderer2: Renderer2,
    private readonly router: Router
  ) {}

  ngOnInit(): void {
    this.resizeSub = this.resizeService.getEvent().subscribe(width => {
      this.windowWidth = width;
      const timer$ = timer(100).subscribe(() => {
        this.setEllipsis();
        timer$.unsubscribe();
      });
    });
  }

  ngOnDestroy(): void {
    if (!!this.resizeSub) {
      this.resizeSub.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    this.titleText = this.title.nativeElement.textContent;
    this.setEllipsis();
  }

  hasOverflow(): boolean {
    const el: HTMLElement = this.title.nativeElement;

    return el.scrollHeight > 66;
  }

  setEllipsis(): void {
    this.title.nativeElement.textContent = this.titleText;
    const el: HTMLElement = this.title.nativeElement;
    let text = el.textContent.split(' ');
    while (this.hasOverflow() && text.length > 0) {
      text = text.slice(0, -1);
      el.textContent = `${text.join(' ')} ...`;
      this.ellipsisBool = false;
    }
  }

  showFullTitle(): void {
    if (!this.ellipsisBool) {
      this.title.nativeElement.textContent = this.titleText;
      this.ellipsisBool = true;
    } else {
      this.setEllipsis();
      this.ellipsisBool = false;
    }
  }

  // Rutas para los link
  actionKey(action): void {
    if (action && action.length) {
      const actions = COVERAGES_ACTIONS.find(c => c.key === action);
      if (actions.route.length) {
        this.router.navigate([actions.route]);
      }
    }
  }
}
