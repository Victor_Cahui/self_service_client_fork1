import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UnsubscribeOnDestroy } from '@mx/core/shared/helpers/util/unsubscribe-on-destroy';
import { NOTIFICATION_ICON } from '@mx/settings/constants/images-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { ServiceFormLang, ServicesLang } from '@mx/settings/lang/services.lang';

@Component({
  selector: 'client-clinic-no-service',
  templateUrl: './digital-clinic-no-service.component.html'
})
export class DigitalClinicNoServiceComponent extends UnsubscribeOnDestroy implements OnInit {
  btnService = GeneralLang.Buttons.ViewLastServices;
  txtAppointment = GeneralLang.Texts.DontHaveAppointment;

  notificationIcon = NOTIFICATION_ICON;

  lang = ServicesLang;
  requestLang = ServiceFormLang;
  public itemService: any;
  public showLoading: boolean;

  constructor(private readonly router: Router) {
    super();
  }

  ngOnInit(): void {}

  goToServices(): void {
    this.router.navigate(['/digital-clinic/assists']);
  }
}
