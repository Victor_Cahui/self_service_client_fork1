import { Component, Input, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import { convertMonth } from '@mx/core/shared/helpers/util/date';
import { UnsubscribeOnDestroy } from '@mx/core/shared/helpers/util/unsubscribe-on-destroy';
import { AppointmentFacade } from '@mx/core/shared/state/appointment/appointment.facade';
import { GeneralService } from '@mx/services/general/general.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { TYPE_APPOINTMENT } from '@mx/settings/constants/general-values';
import { NOTIFICATION_ICON } from '@mx/settings/constants/images-values';
import { ICON_ITEMS, SERVICES_TYPE } from '@mx/settings/constants/services-mf';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { ServiceFormLang, ServicesLang } from '@mx/settings/lang/services.lang';
import { IServiceHistoryView, IServicesItemResponse } from '@mx/statemanagement/models/service.interface';
import { ServicesUtils } from '../../mapfre-services/services.utils';

@Component({
  selector: 'client-clinic-service',
  templateUrl: './digital-clinic-service.component.html'
})
export class DigitalClinicServiceComponent extends UnsubscribeOnDestroy implements OnChanges {
  @Input() appointments: any = [];

  scheduledAppointments: any = [];

  btnService = GeneralLang.Buttons.ViewLastServices;
  txtAppointment = GeneralLang.Texts.DontHaveAppointment;

  notificationIcon = NOTIFICATION_ICON;
  txtNextService = GeneralLang.Texts.NextService;
  btnViewDetail = GeneralLang.Buttons.ViewDetail;
  lblAppointment = GeneralLang.Labels.Appointment;
  scheduleDate = 'dd/MM';
  scheduleTime = '00:00';
  hasService = false;

  isLoadingReschedule: boolean;

  lang = ServicesLang;
  requestLang = ServiceFormLang;
  public itemService: any;

  constructor(
    private readonly _AppointmentFacade: AppointmentFacade,
    private readonly headerHelperService: HeaderHelperService,
    private readonly _GeneralService: GeneralService,
    private readonly router: Router
  ) {
    super();
  }

  ngOnChanges(): void {
    this.obtenerServicios();
    this._mapState();
  }

  obtenerServicios(): void {
    this.scheduledAppointments = this.appointments.map((item: IServicesItemResponse) => this.setData(item));

    if (this.scheduledAppointments.length > 0) {
      const firstServ = this.scheduledAppointments[0];
      this.itemService = firstServ;
      this.btnService = GeneralLang.Buttons.ViewServices;
      this.txtAppointment =
        this.scheduledAppointments.length > 1
          ? GeneralLang.Texts.HasAppointments.replace('{{n}}', this.scheduledAppointments.length)
          : (this.txtAppointment = GeneralLang.Texts.HaveAppointment);
      this.lblAppointment = GeneralLang.Labels.Appointment.replace('{{nroCita}}', firstServ.number);
      this.scheduleDate = this._GeneralService.getDateWithStringFormat(firstServ.payload.fechaAgendada);
      this.scheduleTime = this._GeneralService.getPeriodOfTime(firstServ.scheduledTime);
      this.hasService = true;
    } else {
      this.btnService = GeneralLang.Buttons.ViewLastServices;
      this.txtAppointment = GeneralLang.Texts.DontHaveAppointment;
      this.hasService = false;
    }
  }

  goToServices(): void {
    this.setHeader();
    this.router.navigate(['/digital-clinic/assists']);
  }

  // Detalle
  viewDetail(item: IServiceHistoryView): void {
    if (item.viewDetail) {
      switch (item.code) {
        // Chofer de Reemplazo
        case SERVICES_TYPE.SUBSTITUTE_DRIVER:
          this.router.navigate([`/mapfre-services/substitute-driver/detail/${item.number}`]);
          break;
        // Médico a Domicilio
        case SERVICES_TYPE.HOME_DOCTOR:
          this.router.navigate([`/mapfre-services/home-doctor/detail/${item.number}`]);
          break;
        // Citas Médicas
        case SERVICES_TYPE.MEDICAL_APPOINTMENT:
          this._AppointmentFacade.GetAppointmentForDetail(item.payload);
          this.isLoadingReschedule = true;
          break;
        default:
          break;
      }
    }
  }

  // Configurar Vista
  // tslint:disable-next-line: cyclomatic-complexity
  setData(item: IServicesItemResponse): IServiceHistoryView {
    const date = item.fechaAgendada.split('-');
    const monthName = convertMonth(parseInt(date[1], 10));
    const statusColor = ServicesUtils.getStatusColor(item.codigoEstado);
    const moreInfo =
      item.cita && item.cita.diasRestantes ? `${item.cita.diasRestantes} ${this.lang.Labels.DaysLeft}` : null;
    const na = GeneralLang.Texts.WithOutInformation;

    const itemView: IServiceHistoryView = {
      code: item.codigoTipoSolicitud,
      day: date[2],
      month: monthName,
      year: date[0],
      scheduledTime: item.horaAgendada,
      type: item.descripcionTipoSolicitud,
      moreInfo,
      number: item.identificadorSolicitud.toString(),
      color: item.codigoEstado ? statusColor : '',
      status: item.codigoEstado ? item.descripcionEstado : '',
      viewDetail: ServicesUtils.viewDetail(item.codigoEstado),
      items: [],
      payload: item
    };

    switch (item.codigoTipoSolicitud) {
      // Chofer de Reemplazo
      case SERVICES_TYPE.SUBSTITUTE_DRIVER:
        itemView.items = [
          {
            label: `${this.requestLang.From}:`,
            text: item.chofer.origenServicio || na
          },
          {
            label: `${this.requestLang.To}:`,
            text: item.destinoServicio || na
          },
          {
            label: `${this.requestLang.Driver}:`,
            text: item.empresaProveedora || na,
            icon: ICON_ITEMS.PERSON
          },
          {
            label: item.chofer.datosVehiculo || na,
            icon: ICON_ITEMS.VEHICLE
          }
        ];
        break;
      // Médico a Domicilio
      case SERVICES_TYPE.HOME_DOCTOR:
        itemView.items = [
          {
            icon: ICON_ITEMS.PIN,
            text: item.destinoServicio || na
          },
          {
            text: item.medico.tipoMedicina || na,
            icon: ICON_ITEMS.COVERAGE
          },
          {
            label: `${this.requestLang.Doctor}:`,
            text: item.empresaProveedora || na,
            icon: ICON_ITEMS.DOCTOR
          },
          {
            label: `${this.requestLang.Patient}:`,
            text: item.medico.paciente || na,
            icon: ICON_ITEMS.PERSON
          }
        ];
        break;
      // Citas Médicas
      case SERVICES_TYPE.MEDICAL_APPOINTMENT:
        const patient = `${item.cita.nombresPaciente} ${item.cita.apellidoPaternoPaciente} ${item.cita.apellidoMaternoPaciente}`;
        itemView.items = [
          {
            icon: ICON_ITEMS.PIN,
            text:
              item.cita.tipoCita === TYPE_APPOINTMENT.VIRTUAL
                ? GeneralLang.Labels.AppointmentVirtual
                : item.cita.clinicaDescripcion
          },
          {
            text: item.cita.especialidadDescripcion.toLowerCase() || na,
            icon: ICON_ITEMS.POLICY
          },
          {
            label: `${this.requestLang.Doctor}:`,
            text: item.cita.nombreCompleto.toLowerCase() || na,
            icon: ICON_ITEMS.DOCTOR
          },
          {
            label: `${this.requestLang.Patient}:`,
            text: patient.toLowerCase(),
            icon: ICON_ITEMS.PERSON
          }
        ];
        break;
      default:
        break;
    }

    return itemView;
  }

  private _mapState(): void {
    const GetAppointmentForDetailSuccess = this._AppointmentFacade.GetAppointmentForDetailSuccess$.subscribe(() => {
      if (this._AppointmentFacade.getState().canReschedule) {
        this.router.navigate([`/digital-clinic/schedule-detail/3`]);

        return void 0;
      }
      this.router.navigate([`/digital-clinic/schedule-detail`]);
    });

    this.arrToDestroy.push(GetAppointmentForDetailSuccess);
  }

  private setHeader(): void {
    this.headerHelperService.set({
      title: 'CLÍNICA DIGITAL',
      subTitle: '',
      icon: '',
      back: true
    });
  }
}
