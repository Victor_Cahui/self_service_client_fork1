import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DigitalClinicServiceComponent } from './digital-clinic-service.component';

@NgModule({
  imports: [CommonModule],
  declarations: [DigitalClinicServiceComponent],
  exports: [DigitalClinicServiceComponent]
})
export class DigitalClinicServiceModule {}
