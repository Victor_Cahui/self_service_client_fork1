import { Component, OnInit } from '@angular/core';
import { GeneralService } from '@mx/services/general/general.service';

@Component({
  selector: 'client-travel-emergency-phones',
  templateUrl: './travel-emergency-phones.component.html'
})
export class TravelEmergencyPhonesComponent implements OnInit {
  countries;
  showLoading: boolean;

  private _count = 0;

  constructor(private readonly _GeneralService: GeneralService) {}

  ngOnInit(): void {
    this._getCountriesPhones();
  }

  private _getCountriesPhones(): void {
    this.showLoading = true;
    this._GeneralService.ObtenerContactoPais().subscribe(r => {
      this.countries = this._sortCountries(r.map(this._addCounter.bind(this)));
      this.showLoading = false;
    });
  }

  private _sortCountries(arr): any[] {
    const toStart = arr.filter(o => o.codigoPais !== 'INT');
    const toEnd = arr.filter(o => o.codigoPais === 'INT');

    return [...toStart, ...toEnd];
  }

  private _addCounter(country): any[] {
    if (country.codigoPais === 'INT') {
      this._count++;

      return { ...country, counter: this._count };
    }

    return { ...country };
  }
}
