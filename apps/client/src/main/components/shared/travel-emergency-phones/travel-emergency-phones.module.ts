import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { TravelEmergencyPhonesComponent } from './travel-emergency-phones.component';

@NgModule({
  imports: [CommonModule, MfLoaderModule],
  exports: [TravelEmergencyPhonesComponent],
  declarations: [TravelEmergencyPhonesComponent]
})
export class TravelEmergencyPhonesModule {}
