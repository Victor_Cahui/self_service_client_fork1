import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DropdownModule } from '@mx/components/shared/dropdown/dropdown.module';
import { DropdownAccesoriesComponent } from './dropdown-accesories.component';

@NgModule({
  imports: [CommonModule, DropdownModule],
  declarations: [DropdownAccesoriesComponent],
  exports: [DropdownAccesoriesComponent]
})
export class DropdownAccesoriesModule {}
