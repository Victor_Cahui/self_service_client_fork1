import { Component, Input, OnInit } from '@angular/core';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { SOLES_SYMBOL_CURRENCY } from '@mx/settings/constants/general-values';
import { VehiclesLang } from '@mx/settings/lang/vehicles.lang';
import { IVehicleAccesory, IVehicleAccesoryDropdown } from '@mx/statemanagement/models/vehicle.interface';

@Component({
  selector: 'client-dropdown-accesories',
  templateUrl: './dropdown-accesories.component.html'
})
export class DropdownAccesoriesComponent implements OnInit {
  @Input() accesories: Array<IVehicleAccesory>;
  @Input() currency: number;
  @Input() path: string;

  titleMyAccesories = VehiclesLang.Titles.MyAccesories;

  accesoriesDropdown: Array<IVehicleAccesoryDropdown>;

  ngOnInit(): void {
    const currencySymbol = this.currency ? StringUtil.getMoneyDescription(this.currency) : SOLES_SYMBOL_CURRENCY;
    this.accesoriesDropdown = this.accesories;
    this.accesoriesDropdown.forEach(accesorio => {
      accesorio.descripcion = `${accesorio.modelo} <br> ${currencySymbol} ${accesorio.monto}`;
    });
  }
}
