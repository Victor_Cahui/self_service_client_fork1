import { DOCUMENT } from '@angular/common';
import { Component, ElementRef, HostBinding, Inject, Input, OnInit, Renderer2, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ResizeService } from '@mx/core/shared/helpers/events/resize.service';
import { DROPDOWN } from '@mx/settings/constants/components-values';
import { INSURED_DROPDOWN_ITEMS, URL_FRAGMENTS } from '@mx/settings/constants/general-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-dropdown',
  templateUrl: './dropdown.component.html'
})
export class DropdownComponent implements OnInit {
  @HostBinding('class') @Input() class = 'g-dropwdown--simple';
  @HostBinding('class.dropdown-relative') @Input() isRelative: boolean;

  @Input() list: Array<any>;
  @Input() title: Array<any>;
  @Input() nameItem: string;
  @Input() descripcionItem?: string;
  @Input() path: string;
  @Input() showIcon = true;
  @Input() hideButton: boolean;
  @Input() simpleList: boolean;
  @Input() sliceLength = INSURED_DROPDOWN_ITEMS;

  @ViewChild('target') target: ElementRef;
  @ViewChild('content') content: ElementRef;
  @ViewChild('arrow') arrow: ElementRef;

  leftDropdown: number;
  topDropdown: number;
  subResize: Subscription;
  labelViewAll = GeneralLang.Buttons.ViewAll;
  windowWidth: number;
  windowMargin = 30;
  droppedDown: boolean;

  activeClass = 'active';
  ttLeft = 'g-tooltip--alternative-message--left';
  ttRight = 'g-tooltip--alternative-message--right';
  ttTop = 'g-tooltip--alternative-message--top';
  ttBottom = 'alternative-message--bottom';

  constructor(
    private readonly elementRef: ElementRef,
    private readonly renderer2: Renderer2,
    public router: Router,
    private readonly resizeService: ResizeService,
    @Inject(DOCUMENT) private readonly document: Document
  ) {}

  ngOnInit(): void {
    this.subResize = this.resizeService.getEvent().subscribe(width => {
      this.windowWidth = width;
      this.getPosition(this.target);
      setTimeout(() => {
        this.initDropDown();
        this.arrowPosition();
      }, 100);
    });
  }

  // Ir a Detalle de Póliza
  public goToDetail(): void {
    const navigationExtras = this.simpleList ? { fragment: URL_FRAGMENTS.INSURED } : {};
    this.router.navigate([this.path], navigationExtras);
  }

  trackByFn(index, item): number {
    return item;
  }

  // Mostrar/Ocultar dropdown
  fnDropDown(): void {
    this.getPosition(this.target);
    this.initDropDown();
    const vHasClass = this.elementRef.nativeElement.classList.contains(this.activeClass);
    if (vHasClass) {
      this.droppedDown = false;
      this.renderer2.removeClass(this.elementRef.nativeElement, this.activeClass);
      this.renderer2.removeClass(this.arrow.nativeElement, this.activeClass);
    } else {
      this.droppedDown = true;
      this.renderer2.addClass(this.elementRef.nativeElement, this.activeClass);
      this.arrowPosition();
    }
  }

  // Posicion del dropdown
  getPosition(el: any): void {
    let offsetLeft = 0;
    let offsetTop = 0;
    offsetLeft = el.nativeElement.offsetLeft;
    offsetTop = el.nativeElement.offsetTop;
    // tslint:disable-next-line: no-parameter-reassignment
    el = el.nativeElement.offsetParent;
    while (el) {
      offsetLeft += el.offsetLeft;
      offsetTop += el.offsetTop;
      // tslint:disable-next-line: no-parameter-reassignment
      el = el.offsetParent;
    }
    this.leftDropdown = offsetLeft;
    this.topDropdown = offsetTop;
  }

  // Ubicacion del Contenido
  initDropDown(): void {
    const posLeft =
      this.leftDropdown +
      this.target.nativeElement.offsetWidth / DROPDOWN.width -
      this.arrow.nativeElement.offsetWidth / DROPDOWN.width;
    let posTop = this.topDropdown - this.target.nativeElement.offsetHeight - DROPDOWN.top;

    // Left
    if (posLeft < DROPDOWN.conditional.left) {
      this.renderer2.addClass(this.content.nativeElement, this.ttLeft);
    } else {
      this.renderer2.removeClass(this.content.nativeElement, this.ttLeft);
    }

    // Right
    if (
      // tslint:disable-next-line: restrict-plus-operands
      posLeft + this.arrow.nativeElement.offsetWidth + DROPDOWN.conditional.width >
      this.windowWidth - this.windowMargin
    ) {
      this.renderer2.addClass(this.content.nativeElement, this.ttRight);
    } else {
      this.renderer2.removeClass(this.content.nativeElement, this.ttRight);
    }

    // Top
    if (posTop < DROPDOWN.conditional.top) {
      // tslint:disable-next-line: restrict-plus-operands
      posTop = this.topDropdown + this.target.nativeElement.offsetHeight + DROPDOWN.height;
      this.renderer2.addClass(this.content.nativeElement, this.ttTop);
    } else {
      this.renderer2.removeClass(this.content.nativeElement, this.ttTop);
    }

    // Bottom (mensaje arriba)
    if (
      // tslint:disable-next-line: restrict-plus-operands
      posTop + this.arrow.nativeElement.offsetHeight + DROPDOWN.conditional.height >
      this.document.body.scrollHeight
    ) {
      // tslint:disable-next-line: restrict-plus-operands
      posTop = this.topDropdown + this.target.nativeElement.offsetHeight + DROPDOWN.height;
      this.renderer2.addClass(this.content.nativeElement, this.ttBottom);
    } else {
      this.renderer2.removeClass(this.content.nativeElement, this.ttBottom);
    }
  }

  // Flecha arriba
  arrowPosition(): void {
    if (this.content.nativeElement.classList.contains(this.ttBottom)) {
      this.renderer2.addClass(this.arrow.nativeElement, this.activeClass);
    } else {
      this.renderer2.removeClass(this.arrow.nativeElement, this.activeClass);
    }
  }

  onClickOutside(): void {
    if (this.droppedDown) {
      delete this.droppedDown;
      this.renderer2.removeClass(this.elementRef.nativeElement, this.activeClass);
    }
  }
}
