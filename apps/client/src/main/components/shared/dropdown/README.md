
## dropdown

Contiene la funcionalidad para mostrar un dropdown de la forma

``` 
  n elementos 
    
    titulo 1
    subtitulo 1

    titulo 2
    subtitulo 2

    titulo 3
    subtitulo 3

    Ver Todos
```

donde titulo puede variar, por ejemplo, contener el nombre de un accesorio. No es un valor fijo como "Vehículo 1", "Beneficiario 1", etc. 

**IMPORTANTE:**
Para esos casos donde el titulo es fijo y depende del tipo de póliza (Beneficiarios, Vehículos, etc.) se debe utilizar *dropdown-items-policy*


## dropdown-accesories

Personaliza el dropdown para accesorios de vehículos. Y muestra por cada accesorio los siguientes datos:
  
  - Nombre del accesorio
  - Modelo y marca
  - Costo


## dropdown-items-policy

Contiene la funcionalidad para mostrar un dropdown de la forma

``` 
  n elementos 
    
    elemento 1
    decripcion 1

    elemento 2
    decripcion 2

    elemento 3
    decripcion 3

    Ver Todos
```

donde el valor de "elemento" es fijo y depende del tipo de póliza (Beneficiarios, Vehículos, Direcciones, etc.). 

Obtiene los valores de */@mx/settings/constants/policy-values.ts* y el array beneficiarios que devuelve el servicio. 

