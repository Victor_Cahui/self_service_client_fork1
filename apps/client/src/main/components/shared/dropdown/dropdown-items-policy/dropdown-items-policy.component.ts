import { Component, Input, OnInit } from '@angular/core';
import { INSURED_DROPDOWN_ITEMS as INSURED_DROPDOWN_MAX_ITEMS } from '@mx/settings/constants/general-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { IPolicyType } from '@mx/statemanagement/models/policy.interface';

@Component({
  selector: 'client-dropdown-items-policy',
  templateUrl: './dropdown-items-policy.component.html'
})
export class DropdownItemsPolicyComponent implements OnInit {
  @Input() items: Array<any>;
  @Input() policyType: string;
  @Input() policyNumber: string;
  @Input() descripcionItem?: string;
  @Input() isRelative: boolean;

  hideButton: boolean;
  title: string;
  subTitle: string;
  dropDownItem: IPolicyType;
  path: string;
  dropdownMaxItems = INSURED_DROPDOWN_MAX_ITEMS;

  ngOnInit(): void {
    this.dropDownItem = POLICY_TYPES[this.policyType];
    this.hideButton = this.items.length <= this.dropdownMaxItems;

    if (this.dropDownItem) {
      this.title = this.dropDownItem.dropDownTitle;
      this.subTitle = this.dropDownItem.dropDownSubTitle;
      this.path = this.dropDownItem.path ? `${this.dropDownItem.path}/${this.policyNumber}` : '';
    }
    this.items.forEach((item, i) => {
      item.nameItem = `${this.subTitle} ${i + 1}`;
      item.descripcionItem = item[this.descripcionItem];
    });
  }
}
