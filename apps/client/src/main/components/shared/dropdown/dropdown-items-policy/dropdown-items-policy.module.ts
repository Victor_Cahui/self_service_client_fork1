import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DropdownModule } from '../../dropdown/dropdown.module';
import { DropdownItemsPolicyComponent } from './dropdown-items-policy.component';

@NgModule({
  imports: [CommonModule, DropdownModule],
  declarations: [DropdownItemsPolicyComponent],
  exports: [DropdownItemsPolicyComponent]
})
export class DropdownItemsPolicyModule {}
