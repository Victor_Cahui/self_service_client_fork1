import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DirectivesModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { BannerCarouselComponent } from './banner-carousel.component';

@NgModule({
  imports: [CommonModule, DirectivesModule, MfLoaderModule],
  declarations: [BannerCarouselComponent],
  exports: [BannerCarouselComponent]
})
export class BannerCarouselModule {}
