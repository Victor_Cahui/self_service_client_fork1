import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UnsubscribeOnDestroy } from '@mx/core/shared/helpers/util/unsubscribe-on-destroy';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { GeneralService } from '@mx/services/general/general.service';
import { DEFAULT_CAROUSELS_TIME_INTERVAL_BANNERS } from '@mx/settings/constants/general-values';
import { CAROUSELS_TIME_INTERVAL_BANNERS } from '@mx/settings/constants/key-values';
import {
  IObtenerConfiguracionResponse,
  IObtenerConfigurationCarouselResponse
} from '@mx/statemanagement/models/client.models';
import { BehaviorSubject, interval, NEVER } from 'rxjs';
import { dematerialize, materialize, switchMap, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'client-banner-carousel',
  templateUrl: './banner-carousel.html'
})
export class BannerCarouselComponent extends UnsubscribeOnDestroy implements OnInit {
  public bannerList: Array<any> = [];
  public screenWidth: number;
  public currentIndex = 0;
  private intervalSeconds: number;
  public showCarousel = true;
  public showLoading: boolean;
  public policyNumber: string;
  private _interval$: any;
  private readonly _pauser$ = new BehaviorSubject<boolean>(false);

  constructor(
    private readonly configurationService: ConfigurationService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    protected generalService: GeneralService
  ) {
    super();
  }

  ngOnInit(): void {
    this.policyNumber = this.activatedRoute.snapshot.params['policyNumber'];
    this._mapPathImages();
  }

  public handleItemClick(url: string): void {
    const parsedUrl = new URL(window.location.href);
    const baseUrl = parsedUrl.origin;
    if (url.indexOf(baseUrl) >= 0) {
      window.location.href = url;
    } else {
      window.open(url, '_blank');
    }
  }

  public pause(): void {
    this._pauser$.next(true);
  }

  public resume(): void {
    this._pauser$.next(false);
  }

  public handleArrow(direction): void {
    this.resume();
    direction === 'left' &&
      (this.currentIndex = this.currentIndex === 0 ? this.bannerList.length - 1 : this.currentIndex - 1);
    direction === 'right' &&
      (this.currentIndex = this.currentIndex === this.bannerList.length - 1 ? 0 : this.currentIndex + 1);
  }

  private _mapPathImages(): void {
    this.showLoading = true;
    this.configurationService
      .getConfiguration()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(this._mapConfiguration.bind(this), () => (this.showLoading = false));
  }

  private _mapConfiguration(response: IObtenerConfiguracionResponse): void {
    if (!response) {
      return void 0;
    }

    const carousels = response.carruseles.map((c: IObtenerConfigurationCarouselResponse) => {
      const rutaPagina = c.rutaPagina.includes('{numPoliza}') && c.rutaPagina.replace('{numPoliza}', this.policyNumber);

      return { ...c, ...(rutaPagina && { rutaPagina }) };
    });

    const carousel = carousels.find(c => c.rutaPagina === this.router.url);

    if (!carousel) {
      this.showCarousel = false;

      return void 0;
    }

    const bannerList =
      carousel.listaBanners && carousel.listaBanners.filter(b => b.desktop != null && b.desktop !== '');

    if (!bannerList) {
      this.showCarousel = false;

      return void 0;
    }

    if (!bannerList.length) {
      this.showCarousel = false;

      return void 0;
    }

    if (bannerList.length > 1) {
      this._setTimerCarousel();
    }

    this.bannerList = bannerList;
    this.showLoading = false;
  }

  private _setTimerCarousel(): void {
    this.intervalSeconds =
      +this.generalService.getValueParams(CAROUSELS_TIME_INTERVAL_BANNERS) || DEFAULT_CAROUSELS_TIME_INTERVAL_BANNERS;
    this._interval$ = interval(+this.intervalSeconds * 1000);
    this._pauser$
      .pipe(
        switchMap(paused => (paused ? NEVER : this._interval$.pipe(materialize()))),
        dematerialize()
      )
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(this.handleArrow.bind(this, 'right'));
  }

  notLoading(index: any): void {
    this.bannerList = this.bannerList.filter((b, i) => i !== index);
    !this.bannerList.length && (this.showCarousel = false);
  }
}
