export function setBtn(disabled: boolean, action?: VoidFunction): any {
  return {
    action,
    disabled
  };
}
