import { ElementRef } from '@angular/core';
import * as $ from 'jquery';

export class AccordionComponent {
  static initAccordeon(): void {
    const item = $('[data-accordion="item"]');

    item.each((index: number, el: HTMLElement) => {
      const open = $(el).data('open');
      const content = $('[data-accordion="content"]');
      const on = $('[data-accordion="on"]');
      const off = $('[data-accordion="off"]');
      const click = $('[data-accordion="click"]');
      const activeClass = 'active';

      if (open === true) {
        $(el)
          .find(click)
          .addClass(activeClass);
        $(el)
          .find(on)
          .hide();
        $(el)
          .find(off)
          .show();
        $(el)
          .find(content)
          .show();
      }
    });
  }

  static toggle(element: ElementRef): void {
    const container = $('[data-accordion="container"]');
    const item = $('[data-accordion="item"]');
    const content = $('[data-accordion="content"]');
    const on = $('[data-accordion="on"]');
    const off = $('[data-accordion="off"]');
    const parent = $(element.nativeElement).closest(container);
    const group = parent.data('group');
    const activeClass = 'active';

    if (group === true) {
      parent
        .find(content)
        .not(element.nativeElement)
        .slideUp();
      parent
        .find(` .${activeClass}`)
        .not(element.nativeElement)
        .removeClass(activeClass);
      parent
        .find(on)
        .show()
        .next()
        .hide();
    }

    $(element.nativeElement).toggleClass(activeClass);

    if ($(element.nativeElement).hasClass(activeClass)) {
      $(element.nativeElement)
        .closest(item)
        .find(content)
        .slideDown();
      $(element.nativeElement)
        .find(on)
        .hide();
      $(element.nativeElement)
        .find(off)
        .show();
    } else {
      $(element.nativeElement)
        .closest(item)
        .find(content)
        .slideUp();
      $(element.nativeElement)
        .find(off)
        .hide();
      $(element.nativeElement)
        .find(on)
        .show();
    }
  }
}
