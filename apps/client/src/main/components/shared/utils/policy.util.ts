import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { COB_SAL_HOSPITAL } from '@mx/settings/constants/coverage-values';
import { PAYMENT_TYPE } from '@mx/settings/constants/images-values';
import { POLICY_TYPES as PT, SCTR_HEALTH, SCTR_PENSION } from '@mx/settings/constants/policy-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { MD_SALUD_AP, PolicyLang } from '@mx/settings/lang/policy.lang';

export class PolicyUtil {
  static isEps(policyType: string): boolean {
    return policyType === PT.MD_EPS.code;
  }

  static isSctr(policyType: string): boolean {
    return policyType === PT.MD_SCTR.code;
  }

  static isSctrHealth(policyType: string, codCia: number, codRamo: number): boolean {
    return PolicyUtil.isSctr(policyType) && codCia === SCTR_HEALTH.COD_CIA && codRamo === SCTR_HEALTH.COD_RAMO;
  }

  static isSctrPension(policyType: string, codCia: number, codRamo: number): boolean {
    return PolicyUtil.isSctr(policyType) && codCia === SCTR_PENSION.COD_CIA && codRamo === SCTR_PENSION.COD_RAMO;
  }

  static isSctrDoubleEmission(policyNumber: string): boolean {
    return policyNumber.includes(',');
  }

  static getDeductibleTextForEPS(item): string {
    const coverageOfEPS = [
      'COB_SAL_ODONTOLOGIA',
      'COB_SAL_EMERG_ACCID',
      'COB_SAL_EMERG_MED',
      'COB_SAL_CONS_MED',
      'COB_SAL_HOSPITAL'
    ];

    const coverageOfEPSWhitoutText = ['COB_SAL_EMERG_ACCID', 'COB_SAL_EMERG_MED', 'COB_SAL_CONS_MED'];

    const amountDetail =
      item.montoCopago > -1
        ? `${StringUtil.getMoneyDescription(item.codigoMonedaCopago)} ${item.montoCopago.toFixed(2)}`
        : 'GRATIS';

    return COB_SAL_HOSPITAL === item.llave
      ? `${item.montoCopago > -1 ? item.montoCopago : 0} ${item.textoDeducibleCorto}`
      : coverageOfEPS.includes(item.llave)
      ? coverageOfEPSWhitoutText.includes(item.llave)
        ? amountDetail
        : `${item.textoDeducibleCorto}: ${amountDetail}`
      : PolicyUtil.getDeductibleText(item);
  }

  // Texto deducible p.e. Consulta: S/ 30
  static getDeductibleText(item): string {
    if (!PolicyUtil.hasCopaymentAmount(item)) {
      return void 0;
    }

    if (item.codigoTipoDeducible === 'SD') {
      return 'GRATIS';
    }

    const montoCopagoWithDecimal = item.montoCopago.toFixed(2);

    return item.textoDeducibleCorto && item.codigoMonedaCopago > -1 && item.montoCopago > -1
      ? `${item.textoDeducibleCorto}: ${StringUtil.getMoneyDescription(
          item.codigoMonedaCopago
        )} ${montoCopagoWithDecimal}`
      : item.textoDeducibleCorto && item.montoCopago > -1
      ? `${item.textoDeducibleCorto}: ${montoCopagoWithDecimal}`
      : item.codigoMonedaCopago > -1 && item.montoCopago > -1
      ? `${StringUtil.getMoneyDescription(item.codigoMonedaCopago)} ${montoCopagoWithDecimal}`
      : item.textoDeducibleCorto;
  }

  // LLenar lista de años para Select
  static setYears(maxYears: number): Array<any> {
    const yearList = [];
    const year = new Date().getFullYear();
    const year2 = year - maxYears;
    for (let i = year; i > year2; i--) {
      yearList.push({ text: i.toString(), value: i });
    }

    return yearList;
  }

  // Labels para detalle de polizas
  static setDataCardMyPolicy(): any {
    return {
      titleMyPolicies: PolicyLang.Titles.MyPolicies,
      subtitleListPolicies: PolicyLang.Titles.SubTitleListPolicies,
      labelViewAll: GeneralLang.Buttons.ViewAll,
      labelUpdate: GeneralLang.Buttons.Update,
      labelViewDetail: GeneralLang.Buttons.ViewDetail,
      labelExpiredPolicy: PolicyLang.Texts.ExpiredPolicy,
      labelCurrentPolicy: PolicyLang.Texts.CurrentPolicy,
      labelVigencyPolicy: PolicyLang.Texts.VigencyPolicy,
      labelStartsPolicy: PolicyLang.Texts.StartsPolicy,
      labelFee: PolicyLang.Texts.Fee,
      labelFees: PolicyLang.Texts.Fees,
      labelRenew: PolicyLang.Texts.Renew,
      labelNoFees: PolicyLang.Texts.WithOutFees,
      labelQuote: PolicyLang.Texts.Quote,
      labelQuoteBase: PolicyLang.Texts.QuoteBase,
      labelNumberPolicy: PolicyLang.Texts.NumberPolicy,
      labelNumberContract: PolicyLang.Texts.NumberContract,
      labelEditCard: GeneralLang.Buttons.EditCard,
      labelDisabled: PolicyLang.Labels.DisabledDebit,
      labelEndOfTerms: PolicyLang.Texts.EndOfTerms
    };
  }

  static hasCopaymentAmount(item): boolean {
    return !!item.montoCopago;
  }

  // Icono según tipo de pago
  static getPaymentTypeIcon(key: string): string {
    return key && PAYMENT_TYPE[key] ? PAYMENT_TYPE[key].icon : PAYMENT_TYPE.DEFAULT.icon;
  }

  // Verifica si es uns poliza de accidentes personales del ramo MD_SALUD
  static verifyPolicyType(policy: any): string {
    return (policy.tipoPoliza === PT.MD_SALUD.code || policy.policyType === PT.MD_SALUD.code) &&
      policy.iconoRamo === MD_SALUD_AP
      ? PT.MD_ACCIDENTES.code
      : policy.tipoPoliza || policy.policyType;
  }
}
