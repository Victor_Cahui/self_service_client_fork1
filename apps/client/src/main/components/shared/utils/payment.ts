export enum PaymentMode {
  CARD = 'PAGO_TARJETA',
  BANK = 'PAGO_BANCARIO',
  FREE = 'PAGO_GRATIS',
  TOKN = 'PAGO_TOKEN'
}

export enum PaymentFormMode {
  SIMPL = 'RP',
  TOKEN = 'AR'
}

export enum PaymentCardType {
  VISA = 'VISA',
  MASTER_CARD = 'MASTERCARD',
  DINNER_CLUB = 'DINERS_CLUB',
  AMERICAN_EXPRESS = 'AMERICAN_EXPRESS'
}

export const PaymentCardTypeValue = {
  VISA: ['4'],
  MASTER_CARD: ['51', '52', '53', '54', '55'],
  DINNER_CLUB: ['36'],
  AMERICAN_EXPRESS: ['34', '37']
};

export class PaymentCardUtil {
  static getCardType(card: string): PaymentCardType {
    let cardType: PaymentCardType;
    Object.keys(PaymentCardTypeValue).forEach(key => {
      const item = PaymentCardTypeValue[key].find((value: string) => card.startsWith(value));
      if (item) {
        cardType = PaymentCardType[key];

        return;
      }
    });

    return cardType;
  }
}

export function getTextTranslate(data: any): string {
  const textField: any = {
    'Card Number': 'Número de tarjeta',
    Expiration: 'Fecha de vencimiento',
    'Security code': 'Código de seguridad',
    'Card holder name': 'Nombre del titular'
  };

  return textField[data] || data;
}

export enum PaymentSoatType {
  RE = 'RE'
}

export enum PaymentDocumentType {
  AVISO = 'AVISO',
  RECIBO = 'RECIBO'
}
