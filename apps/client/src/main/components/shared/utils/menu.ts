export class MenuComponent {
  static closeNav(): void {
    document.querySelector('.g-box-nav').classList.remove('g-box-nav--active');
    const body = document.querySelector('body');
    const html = document.querySelector('html');
    body.classList.remove('g-no-scroll');
    html.classList.remove('g-no-scroll');
    document.querySelector('.g-box-nav--close').classList.remove('d-block');
    const hambOpen = document.querySelector('.js-hamb-open');
    hambOpen.classList.remove('d-none');
    hambOpen.classList.add('d-block');
    document.querySelector('.js-hamb-close').classList.remove('d-block');
    const headerOim = document.querySelector('.g-oim--header');
    headerOim.classList.remove('g-oim--header--open-menu');
  }

  static openNav(): void {
    document.querySelector('.g-box-nav').classList.add('g-box-nav--active');
    const body = document.querySelector('body');
    const html = document.querySelector('html');
    body.classList.add('g-no-scroll');
    html.classList.add('g-no-scroll');
    document.querySelector('.g-box-nav--close').classList.add('d-block');
    document.querySelector('.js-hamb-close').classList.add('d-block');
    const hambOpen = document.querySelector('.js-hamb-open');
    hambOpen.classList.add('d-none');
    hambOpen.classList.remove('d-block');
    const headerOim = document.querySelector('.g-oim--header');
    headerOim.classList.add('g-oim--header--open-menu');
  }
}
