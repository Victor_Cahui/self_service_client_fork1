export class NumberComponent {
  static numberToLetter(num): string {
    const data = {
      numero: num,
      enteros: Math.floor(num)
    };
    if (data.enteros === 0) {
      return `CERO`;
    }
    if (data.enteros === 1) {
      return NumberComponent.millones(data.enteros);
    }

    return NumberComponent.millones(data.enteros);
  }

  static unidades(num: number): string {
    switch (num) {
      case 1:
        return 'UNO';
      case 2:
        return 'DOS';
      case 3:
        return 'TRES';
      case 4:
        return 'CUATRO';
      case 5:
        return 'CINCO';
      case 6:
        return 'SEIS';
      case 7:
        return 'SIETE';
      case 8:
        return 'OCHO';
      case 9:
        return 'NUEVE';
      default:
        break;
    }
  }

  static decenas(num: number): string {
    const decena = Math.floor(num / 10);
    const unidad = num - decena * 10;

    switch (decena) {
      case 1:
        switch (unidad) {
          case 0:
            return 'DIEZ';
          case 1:
            return 'ONCE';
          case 2:
            return 'DOCE';
          case 3:
            return 'TRECE';
          case 4:
            return 'CATORCE';
          case 5:
            return 'QUINCE';
          default:
            return `DIECI ${NumberComponent.unidades(unidad)}`;
        }
      case 2:
        switch (unidad) {
          case 0:
            return 'VEINTE';
          default:
            return `VEINTI ${NumberComponent.unidades(unidad)}`;
        }
      case 3:
        return NumberComponent.decenasY('TREINTA', unidad);
      case 4:
        return NumberComponent.decenasY('CUARENTA', unidad);
      case 5:
        return NumberComponent.decenasY('CINCUENTA', unidad);
      case 6:
        return NumberComponent.decenasY('SESENTA', unidad);
      case 7:
        return NumberComponent.decenasY('SETENTA', unidad);
      case 8:
        return NumberComponent.decenasY('OCHENTA', unidad);
      case 9:
        return NumberComponent.decenasY('NOVENTA', unidad);
      case 0:
        return NumberComponent.unidades(unidad);
      default:
        break;
    }
  }

  static decenasY(strSin, numUnidades): string {
    if (numUnidades > 0) {
      return `${strSin} Y  ${NumberComponent.unidades(numUnidades)}`;
    }

    return strSin;
  }

  static centenas(num): string {
    const centenas = Math.floor(num / 100);
    const decenas = num - centenas * 100;

    switch (centenas) {
      case 1:
        if (decenas > 0) {
          return `CIENTO ${NumberComponent.decenas(decenas)}`;
        }

        return 'CIEN';
      case 2:
        return `DOSCIENTOS ${NumberComponent.decenas(decenas)}`;
      case 3:
        return `TRESCIENTOS ${NumberComponent.decenas(decenas)}`;
      case 4:
        return `CUATROCIENTOS ${NumberComponent.decenas(decenas)}`;
      case 5:
        return `QUINIENTOS ${NumberComponent.decenas(decenas)}`;
      case 6:
        return `SEISCIENTOS ${NumberComponent.decenas(decenas)}`;
      case 7:
        return `SETECIENTOS ${NumberComponent.decenas(decenas)}`;
      case 8:
        return `OCHOCIENTOS ${NumberComponent.decenas(decenas)}`;
      case 9:
        return `NOVECIENTOS ${NumberComponent.decenas(decenas)}`;
      default:
        break;
    }

    return NumberComponent.decenas(decenas);
  }

  static seccion(num, divisor, strSingular, strPlural): string {
    const cientos = Math.floor(num / divisor);
    const resto = num - cientos * divisor;

    let letras = '';

    if (cientos > 0) {
      letras = cientos > 1 ? `${NumberComponent.centenas(cientos)} ${strPlural}` : strSingular;
    }

    if (resto > 0) {
      letras += '';
    }

    return letras;
  }

  static miles(num): string {
    const divisor = 1000;
    const cientos = Math.floor(num / divisor);
    const resto = num - cientos * divisor;

    const strMiles = NumberComponent.seccion(num, divisor, 'UN MIL', 'MIL');
    const strCentenas = NumberComponent.centenas(resto);

    if (strMiles === '') {
      return strCentenas;
    }

    return `${strMiles} ${strCentenas}`;
  }

  static millones(num): string {
    const divisor = 1000000;
    const cientos = Math.floor(num / divisor);
    const resto = num - cientos * divisor;

    const strMillones = NumberComponent.seccion(num, divisor, 'UN MILLON DE', 'MILLONES DE');
    const strMiles = NumberComponent.miles(resto);

    if (strMillones === '') {
      return strMiles;
    }

    return `${strMillones} ${strMiles}`;
  }
}
