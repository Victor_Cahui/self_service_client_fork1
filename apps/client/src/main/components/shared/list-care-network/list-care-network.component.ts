import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PolicyUtil } from '@mx/components/shared/utils/policy.util';
import { AuthService } from '@mx/services/auth/auth.service';
import { PoliciesService } from '@mx/services/policies.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { IViewOnMapOptions } from '@mx/statemanagement/models/google-map.interface';
import { ICareNetworksRequest, ICareNetworksResponse } from '@mx/statemanagement/models/policy.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-list-care-network',
  templateUrl: './list-care-network.component.html'
})
export class ListCareNetworkComponent implements OnInit {
  @Input() coverageType: string;
  @Input() titleTable = [];
  @Input() viewOnMapOptions: IViewOnMapOptions;

  showLoading: boolean;
  networkCareSub: Subscription;
  policyNumber: string;
  location: string;
  careNetworksList: Array<ICareNetworksResponse>;

  constructor(
    private readonly policiesService: PoliciesService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly authService: AuthService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((res: any) => {
      this.policyNumber = res.params.policyNumber;
    });
    this.showLoading = true;
    this.networkCareSub = this.policiesService
      .getNetworksCare({
        codigoApp: APPLICATION_CODE,
        numeroPoliza: this.policyNumber,
        tipoCobertura: this.coverageType
      } as ICareNetworksRequest)
      .subscribe(
        (response: Array<ICareNetworksResponse>) => {
          if (response && response.length) {
            this.careNetworksList = response;
            this.careNetworksList.forEach(item => {
              this.location = '';
              item.redes.forEach(red => {
                if (this.location !== red.tipoUbicacion) {
                  this.location = red.tipoUbicacion;
                  red.tipoUbicacion = this.location;
                } else {
                  red.tipoUbicacion = '';
                }
                red.collapse = true;
                red.total = red.clinicas.length;
                const deducible = PolicyUtil.getDeductibleText(red).split(':');
                if (deducible[1]) {
                  deducible[0] += ':';
                }
                red.textDeducible = deducible[0];
                red.textMonto = deducible[1];
              });
            });
          }
          this.networkCareSub.unsubscribe();
          this.showLoading = false;
        },
        () => {
          this.showLoading = false;
        }
      );
  }

  toogle(item): void {
    item.collapse = !item.collapse;
  }

  getQueryMap(index: number): any {
    const queryParams = { policy: this.policyNumber, coverage: this.viewOnMapOptions.coverages[index] };
    this.router.navigate([this.viewOnMapOptions.urlLink], { queryParams });
  }
}
