import { DOCUMENT } from '@angular/common';
import { Component, ElementRef, HostBinding, Inject, Input, OnChanges, OnInit, Renderer2 } from '@angular/core';
import { GmHandlerShow } from '@mx/components/shared';
import { ResizeService } from '@mx/core/shared/helpers/events/resize.service';
import { GeolocationService } from '@mx/core/shared/helpers/util/geolocation.service';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { PlatformService } from '@mx/core/shared/helpers/util/platform.service';
import { GoogleMapService, GoogleMapsEvents } from '@mx/services/google-map/google-map.service';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { KeyServiceOffice, OfficeLang } from '@mx/settings/lang/office.lang';
import { IOffice } from '@mx/statemanagement/models/office.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-office-detail',
  templateUrl: './office-detail.component.html'
})
export class OfficeDetailComponent extends GmHandlerShow implements OnInit, OnChanges {
  @HostBinding('attr.class') attr_class = 'h-100 d-none';
  @Input() office: IOffice;
  hasPermission: boolean;
  lang = GeneralLang;
  keyServices = KeyServiceOffice;
  officeAddress: string;
  officeDistance: string;
  geolocatonSub: Subscription;
  howToGetThere: string;
  showLoading: boolean;

  constructor(
    protected render2: Renderer2,
    protected elementRef: ElementRef,
    protected platformService: PlatformService,
    protected resizeService: ResizeService,
    private readonly geolocationService: GeolocationService,
    private readonly googlemapService: GoogleMapService,
    protected gaService: GAService,
    @Inject(DOCUMENT) protected document: Document
  ) {
    super(render2, elementRef, platformService, resizeService, gaService, document);
  }

  ngOnInit(): void {
    this.showLoading = true;
  }

  ngOnChanges(): void {
    if (this.office) {
      const noAddress = GeneralLang.Texts.WithOutAddress;
      const district = this.office.distritoDescripcion ? `, Distrito de ${this.office.distritoDescripcion}` : '';
      const province = this.office.provinciaDescripcion ? `, ${this.office.provinciaDescripcion}` : '';
      const department = this.office.departamentoDescripcion ? `, ${this.office.departamentoDescripcion}` : '';

      this.officeAddress = `${this.office.direccion || noAddress}${district}${province}${department}`;
      this.officeDistance = `${this.office.distancia} ${OfficeLang.Labels.distance}`;
      this.geolocatonSub = this.geolocationService.getCurrentPosition().subscribe(
        position => {
          this.hasPermission = true;
          this.howToGetThere = `
              ${OfficeLang.Links.gmapsDir}${position.latitude},${position.longitude}/${this.office.latitud},${this.office.longitud}`;
        },
        () => {
          this.hasPermission = false;
        },
        () => {
          this.geolocatonSub.unsubscribe();
        }
      );
    }
  }

  closeDetail(): void {
    this.googlemapService.eventEmit(GoogleMapsEvents.RESET_MARKERS);
    this.close();
  }

  mfOnDestroy(): void {
    if (this.geolocatonSub) {
      this.geolocatonSub.unsubscribe();
    }
  }

  imgLoaded(): void {
    this.showLoading = false;
  }

  openGmapDir(): void {
    window.open(this.howToGetThere, '_blank');
  }
}
