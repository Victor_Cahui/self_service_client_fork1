import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { IconPoliciesModule } from '@mx/components/shared/icon-policies/icon-polices.module';
import { InputSearchModule } from '@mx/components/shared/input-search/input-search.module';
import { ItemNotFoundModule } from '@mx/components/shared/item-not-found/item-not-found.module';
import { ModalMoreFaqsModule } from '@mx/components/shared/modals/modal-more-faqs/modal-more-faqs.module';
import { ModalSelectPolicyModule } from '@mx/components/shared/modals/modal-select-policy/modal-select-policy.module';
import { DirectivesModule, MfShowErrorsModule } from '@mx/core/ui';
import { CardModule } from '@mx/core/ui/lib/components/card/card.module';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { ClinicListCoverageComponent } from './clinic-list-coverage.component';

@NgModule({
  imports: [
    CardModule,
    CommonModule,
    DirectivesModule,
    IconPoliciesModule,
    InputSearchModule,
    ItemNotFoundModule,
    MfLoaderModule,
    ModalMoreFaqsModule,
    ModalSelectPolicyModule,
    MfShowErrorsModule,
    ReactiveFormsModule
  ],
  exports: [ClinicListCoverageComponent],
  declarations: [ClinicListCoverageComponent],
  providers: []
})
export class ClinicListCoverageModule {}
