import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalMoreFaqsComponent } from '@mx/components/shared/modals/modal-more-faqs/modal-more-faqs.component';
import { ModalSelectPolicyComponent } from '@mx/components/shared/modals/modal-select-policy/modal-select-policy.component';
import { PolicyUtil } from '@mx/components/shared/utils/policy.util';
import { ArrayUtil } from '@mx/core/shared/helpers/util/array';
import { getDDMMYYYY, isExpired } from '@mx/core/shared/helpers/util/date';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { AlphanumericValidator } from '@mx/core/ui';
import { AuthService } from '@mx/services/auth/auth.service';
import { HealthService } from '@mx/services/health/health.service';
import { APPLICATION_CODE, GENERAL_MAX_LENGTH } from '@mx/settings/constants/general-values';
import {
  COIN_ICON,
  COVERAGES_ICON,
  FILE_ICON,
  QANDA_ICON,
  STETHOSCOPE_ICON
} from '@mx/settings/constants/images-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { EPS_INSURANCE_FAQS } from '@mx/settings/faqs/eps-insurance.faq';
import { HEALTH_INSURANCE_FAQS } from '@mx/settings/faqs/health-insurance.faq';
import { SCTR_SALUD_INSURANCE_FAQS } from '@mx/settings/faqs/sctr-insurance.faq';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { HealthLang } from '@mx/settings/lang/health.lang';
import { Faq } from '@mx/statemanagement/models/general.models';
import {
  IClinicCoverage,
  IClinicCoverageRequest,
  IClinicResponse,
  IPoliciesByClinicRequest
} from '@mx/statemanagement/models/health.interface';
import { ICardPoliciesView, IPoliciesByClientResponse } from '@mx/statemanagement/models/policy.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-clinic-list-coverage',
  templateUrl: './clinic-list-coverage.component.html'
})
export class ClinicListCoverageComponent implements OnDestroy, OnInit {
  @ViewChild(ModalMoreFaqsComponent) modalFaqs: ModalMoreFaqsComponent;
  @ViewChild(ModalSelectPolicyComponent) modalPolicy: ModalSelectPolicyComponent;
  @Input() showLoading: boolean;
  @Input() clinic: IClinicResponse;

  lang = HealthLang;
  generalLang = GeneralLang;
  generalLength = GENERAL_MAX_LENGTH;
  fileIcon = FILE_ICON;
  stethoscopeIcon = STETHOSCOPE_ICON;
  qandaIcon = QANDA_ICON;
  coinIcon = COIN_ICON;
  coveragesIcon = COVERAGES_ICON;
  faqs: Array<Faq>;
  subTitleFaq: string;
  subTitleModal: string;
  currentSearch = '';

  coverages: Array<IClinicCoverage>;
  tmpCoverages: Array<IClinicCoverage>;
  currentPolicy: IPoliciesByClientResponse;
  policies: Array<IPoliciesByClientResponse>;
  policyListModal: Array<ICardPoliciesView>;
  positionSelectedPolicy: number;
  form: FormGroup;
  search: AbstractControl;

  currentPolicySub: Subscription;
  policiesSub: Subscription;
  clinicCoverageByPolicySub: Subscription;
  openModalMoreFaq: boolean;
  openModalSelectPolicy: boolean;

  constructor(
    private readonly healthService: HealthService,
    private readonly authService: AuthService,
    private readonly fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.createForm();
    this.subTitleModal = this.clinic.nombre;

    this.currentPolicySub = this.healthService.getCurrentPolicy().subscribe(policy => {
      this.setCurrentPolicy(policy);
    });

    const params = this.fnParamsSearchByClient();
    this.policiesSub = this.healthService.getPolicyByClinic(params).subscribe(policies => {
      this.policies = policies;
      this.selectedPositionCurrentPolicy();
      this.formatModalPolicies();
    });
  }

  ngOnDestroy(): void {
    if (this.currentPolicySub) {
      this.currentPolicySub.unsubscribe();
    }
    if (this.policiesSub) {
      this.policiesSub.unsubscribe();
    }
    if (this.clinicCoverageByPolicySub) {
      this.clinicCoverageByPolicySub.unsubscribe();
    }
  }

  getCoverages(tipoPoliza: string): void {
    const params: IClinicCoverageRequest = {
      codigoApp: APPLICATION_CODE,
      clinicaId: this.clinic.clinicaId,
      numeroPoliza: this.currentPolicy.numeroPoliza
    };
    this.showLoading = true;
    this.coverages = [];
    this.tmpCoverages = [];
    this.clinicCoverageByPolicySub = this.healthService.getClinicCoverageByPolicy(params).subscribe(coverages => {
      this.showLoading = false;
      // tslint:disable-next-line: no-parameter-reassignment
      coverages = coverages || [];
      this.coverages = coverages.map(cover => ({
        coverageName: cover.nombreCobertura,
        deductible: PolicyUtil.isEps(tipoPoliza)
          ? PolicyUtil.getDeductibleTextForEPS(cover)
          : this.formatDeductible(cover),
        coveragePercent: `${cover.cobertura}%`,
        description: cover.textoDeducibleLargo
      }));
      this.filterCoverages();
    });
  }

  searchFn(text?: string): void {
    this.currentSearch = text || '';
    this.search.setValue(text || '');
    if (this.form.valid || (!this.form.valid && !text)) {
      this.filterCoverages();
    }
    if (text) {
      this.search.markAsTouched();
    } else {
      this.search.markAsUntouched();
    }
  }

  filterCoverages(): void {
    const text = StringUtil.withoutDiacritics(this.currentSearch.toLowerCase()).trim();
    this.coverages = this.coverages || [];
    let coverages = this.coverages;
    if (text && text.length > 2) {
      coverages = coverages.filter(coverage => {
        const coverageName = StringUtil.withoutDiacritics(coverage.coverageName.toLowerCase());

        return coverageName.indexOf(text) > -1;
      });
    }
    this.tmpCoverages = coverages;
  }

  getFaqs(): Array<Faq> {
    this.subTitleFaq = this.currentPolicy.descripcionPoliza.toUpperCase();
    switch (this.currentPolicy.tipoPoliza) {
      case POLICY_TYPES.MD_SALUD.code:
        return HEALTH_INSURANCE_FAQS;
      case POLICY_TYPES.MD_EPS.code:
        return EPS_INSURANCE_FAQS;
      default:
        return SCTR_SALUD_INSURANCE_FAQS;
    }
  }

  formatModalPolicies(): void {
    const policyListModal = this.policies
      .filter(policy => {
        const isEPS = policy.tipoPoliza === POLICY_TYPES.MD_EPS.code;

        return isEPS || !isExpired(policy.fechaFin);
      })
      .map(policy => {
        return {
          policyType: policy.tipoPoliza,
          policyNumber: policy.numeroPoliza,
          beneficiaries: policy.beneficiarios,
          policy: policy.descripcionPoliza,
          endDate: getDDMMYYYY(policy.fechaFin),
          iconoRamo: policy.iconoRamo,
          isVIP: policy.esPolizaVip,
          response: policy
        } as ICardPoliciesView;
      });

    this.policyListModal = ArrayUtil.customSort(
      policyListModal,
      [POLICY_TYPES.MD_SALUD.code, POLICY_TYPES.MD_EPS.code, POLICY_TYPES.MD_SCTR.code],
      'policyType'
    );
  }

  openSelectPolicy(): void {
    this.openModalSelectPolicy = true;
    setTimeout(() => {
      this.modalPolicy.open();
    });
  }

  openModalFaq(): void {
    this.openModalMoreFaq = true;
    setTimeout(() => {
      this.modalFaqs.open();
    });
  }

  onSelectedPolicy(policySelected: ICardPoliciesView): void {
    const currentPolicy = this.policies.find(policy => policy.numeroPoliza === policySelected.policyNumber);
    if (currentPolicy !== this.currentPolicy) {
      this.setCurrentPolicy(currentPolicy);
    }
  }

  setCurrentPolicy(policy: IPoliciesByClientResponse): void {
    this.currentPolicy = policy;
    this.currentSearch = '';
    this.faqs = this.getFaqs();
    this.selectedPositionCurrentPolicy();
    this.getCoverages(policy.tipoPoliza);
  }

  selectedPositionCurrentPolicy(): void {
    if (this.currentPolicy && this.policyListModal && this.policyListModal.length) {
      this.positionSelectedPolicy = this.policyListModal.findIndex(
        policy => policy.policyNumber === this.currentPolicy.numeroPoliza
      );
    }
  }

  formatDeductible(item): string {
    return item ? PolicyUtil.getDeductibleText(item) : null;
  }

  fnParamsSearchByClient(): IPoliciesByClinicRequest {
    const params: IPoliciesByClinicRequest = {
      codigoApp: APPLICATION_CODE,
      identificadorClinica: this.clinic.clinicaId
    };

    return params;
  }

  createForm(): void {
    this.form = this.fb.group({
      search: ['', [Validators.required, AlphanumericValidator.isValid]]
    });
    this.search = this.form.controls['search'];
  }
}
