import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GeolocationService } from '@mx/core/shared/helpers/util/geolocation.service';
import { DirectivesModule } from '@mx/core/ui';
import { ClinicDetailInfoComponent } from './clinic-detail-info.component';

@NgModule({
  imports: [CommonModule, DirectivesModule],
  exports: [ClinicDetailInfoComponent],
  declarations: [ClinicDetailInfoComponent],
  providers: [GeolocationService]
})
export class ClinicDetailInfoModule {}
