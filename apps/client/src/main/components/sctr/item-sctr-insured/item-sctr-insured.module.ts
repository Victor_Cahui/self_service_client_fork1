import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ItemSctrInsuredComponent } from './item-sctr-insured.component';

@NgModule({
  imports: [CommonModule],
  declarations: [ItemSctrInsuredComponent],
  exports: [ItemSctrInsuredComponent]
})
export class ItemSctrInsuredModule {}
