import { Component, Input, OnInit } from '@angular/core';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { IInsuredRecordResponse } from '@mx/statemanagement/models/sctr.interface';

@Component({
  selector: 'client-item-sctr-insured',
  templateUrl: './item-sctr-insured.component.html'
})
export class ItemSctrInsuredComponent implements OnInit {
  @Input() insured: IInsuredRecordResponse;
  @Input() index: number;

  generalLang = GeneralLang;
  fullName: string;
  moneyDescription: string;

  ngOnInit(): void {
    this.fullName = this.getFullName(this.insured);
    this.moneyDescription = `${StringUtil.getMoneyDescription(this.insured.codigoMonedaSueldo)} `;
  }

  getFullName(insured: IInsuredRecordResponse): string {
    if (insured.apellidoPaterno === '' && insured.apellidoMaterno === '' && insured.nombres === '') {
      return insured.nombreCompleto;
    }

    return `${insured.apellidoPaterno || ''} ${insured.apellidoMaterno || ''} ${insured.nombres || ''}`;
  }
}
