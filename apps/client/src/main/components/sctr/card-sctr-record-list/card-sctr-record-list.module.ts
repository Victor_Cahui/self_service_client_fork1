import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ItemNotFoundModule } from '@mx/components/shared/item-not-found/item-not-found.module';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { LinksModule } from '@mx/core/ui/lib/links/links.module';
import { ItemSctrRecordModule } from '../item-sctr-record/item-sctr-record.module';
import { CardSctrRecordListComponent } from './card-sctr-record-list.component';

@NgModule({
  imports: [
    CommonModule,
    MfCardModule,
    MfLoaderModule,
    DirectivesModule,
    LinksModule,
    ItemSctrRecordModule,
    ItemNotFoundModule
  ],
  declarations: [CardSctrRecordListComponent],
  exports: [CardSctrRecordListComponent]
})
export class CardSctrRecordListModule {}
