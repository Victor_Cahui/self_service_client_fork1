import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from '@mx/services/auth/auth.service';
import { SctrService } from '@mx/services/sctr.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { FILE_ICON } from '@mx/settings/constants/images-values';
import { RECORD_LANG } from '@mx/settings/lang/sctr.lang';
import { IRecordResponse, IRecordsRequest } from '@mx/statemanagement/models/sctr.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-card-sctr-record-list',
  templateUrl: './card-sctr-record-list.component.html'
})
export class CardSctrRecordListComponent implements OnDestroy, OnInit {
  @Input() policyNumber: string;
  @Input() isContractor: boolean;
  @Input() mpKeys: any;

  collapse = false;
  showLoading: boolean;
  hasMoreRecords: boolean;
  records: Array<IRecordResponse>;

  getRecordsSub: Subscription;
  routerLinkViewAll: string;
  lang = RECORD_LANG;
  fileIcon = FILE_ICON;

  constructor(private readonly sctrService: SctrService, private readonly _AuthService: AuthService) {}

  ngOnInit(): void {
    this.routerLinkViewAll = `/sctr/sctr-insurance/${this.policyNumber}/records`;
    this.getRecords();
  }

  ngOnDestroy(): void {
    if (this.getRecordsSub) {
      this.getRecordsSub.unsubscribe();
    }
  }

  getRecords(): void {
    this.showLoading = true;
    const params = this.getParams();
    this.getRecordsSub = this.sctrService.getRecords(params).subscribe(
      (response: any) => {
        this.records = response.constancias || [];
        this.hasMoreRecords = response.informacionAdicional && response.informacionAdicional.tieneMasConstancia;
        this.showLoading = false;
      },
      err => {
        this.getRecordsSub.unsubscribe();
        this.showLoading = false;
      }
    );
  }

  getParams(): IRecordsRequest {
    const auth = this._AuthService.retrieveEntity();
    const params: IRecordsRequest = {
      codigoApp: APPLICATION_CODE,
      numeroPoliza: this.policyNumber,
      registros: 6,
      pagina: 1,
      documento: auth.number,
      tipoDocumento: auth.type
    };

    return params;
  }
}
