import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { PaymentButtonModule } from '@mx/components/payment/payment-button/payment-button.module';
import { SendReceiptModule } from '@mx/components/payment/send-receipt/send-receipt.module';
import { CardSctrPeriodItemModule } from '@mx/components/shared/card-sctr-period-item/card-sctr-period-item.module';
import { IconPaymentTypeModule } from '@mx/components/shared/icon-payment-type/icon-payment-type.module';
import { IconPoliciesModule } from '@mx/components/shared/icon-policies/icon-polices.module';
import { NotificationModule } from '@mx/core/shared/helpers';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AutoserviciosMock } from '@mx/core/shared/providers/services/proxy/Autoservicios.mock';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';
import { MfButtonModule } from '@mx/core/ui/lib/components/forms/button';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { LinksModule } from '@mx/core/ui/lib/links/links.module';
import { CardSctrPeriodsComponent } from './card-sctr-periods.component';

describe('CardSctrPeriodsComponent', () => {
  let component: CardSctrPeriodsComponent;
  let fixture: ComponentFixture<CardSctrPeriodsComponent>;
  let el: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        IconPaymentTypeModule,
        DirectivesModule,
        MfCardModule,
        MfButtonModule,
        LinksModule,
        PaymentButtonModule,
        MfLoaderModule,
        SendReceiptModule,
        GoogleModule,
        CardSctrPeriodItemModule,
        IconPoliciesModule,
        BrowserAnimationsModule,
        NotificationModule
      ],
      declarations: [CardSctrPeriodsComponent],
      providers: [{ provide: Autoservicios, useClass: AutoserviciosMock }]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(CardSctrPeriodsComponent);
        component = fixture.componentInstance;
        el = fixture.debugElement;
      })
      .then(() => {
        component.isAdditional = false;
        component.policyType = 'MD_SCTR';
        component.policyNumber = '7022000001018';
      })
      .then(() => {
        fixture.detectChanges();
      });
  }));

  it('should create the app', async(() => {
    expect(component).toBeTruthy();
  }));

  it('to validate component properties', async(() => {
    expect(component.titlePeriods).toEqual('Periodos');
    expect(component.footerText).toEqual('Ver todos los periodos');
    // expect(component.addIcon).toEqual('icon-mapfre_052_agregar g-font--20');
    // expect(component.labelQuota).toEqual('Cuota');
    // expect(component.billNumber).toEqual('Nro. de recibo');
    // expect(component.noticeNumber).toEqual('Grupo de recibos');
    expect(component.calendarIcon).toEqual('icon-mapfre_063_agendar');
    // expect(component.fileIcon).toEqual('icon-mapfre_058_file');
    // expect(component.personIcon).toEqual('icon-mapfre_234_person');
    expect(component.noPeriodsIcon).toEqual('./assets/images/icons/sintramites-pendientes.svg');
    // expect(component.periodStatus).toEqual(SCTR_PERIOD_STATUS);
    expect(component.messageWithoutData).toEqual('No tiene períodos pendientes.');
    // expect(component.tooltipButton).toEqual('Para declarar este mes no deben existir periodos con recibos vencidos');
    // expect(component.routerLinkViewAll).toEqual('/sctr/sctr-insurance/7022000001018/periods');
  }));

  it('count 3 period records', async(() => {
    const elements = el.queryAll(
      By.css('.d-flex.justify-content-between.flex-wrap.flex-lg-nowrap.g-mh--8.position-relative.w-100')
    );
    expect(elements.length).toEqual(3);
  }));

  it('count 3 period additionals records', async(() => {
    component.isAdditional = true;
    component.policyType = 'MD_SCTR';
    component.policyNumber = '7022000000853';
    fixture.detectChanges();
    const elements = el.queryAll(
      By.css('.d-flex.justify-content-between.flex-wrap.flex-lg-nowrap.g-mh--8.position-relative.w-100')
    );
    expect(elements.length).toEqual(3);
  }));
});
