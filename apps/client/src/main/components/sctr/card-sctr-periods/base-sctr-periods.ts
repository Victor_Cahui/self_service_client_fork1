import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { SITUATION_TYPE_SCTR_PERIODS, TRAFFIC_LIGHT_COLOR } from '@mx/components/shared/utils/policy';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { APPLICATION_CODE, FORMAT_DATE_DDMMYYYY, FORMAT_NAME_MONTH_YEAR } from '@mx/settings/constants/general-values';
import { PERIODS_LANAG } from '@mx/settings/lang/sctr.lang';
import { ISctrDetailsPeriodResponse, ISctrPeriodResponse } from '@mx/statemanagement/models/policy.interface';
import { ICardPeriodPreview, IPeriodDetails } from '@mx/statemanagement/models/sctr.interface';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';

export abstract class BaseSctrPeriods extends GaUnsubscribeBase {
  auth: IdDocument;
  showLoading: boolean;
  periodList: Array<ICardPeriodPreview> = [];
  hasMorePeriods: boolean;
  policyNumber: string;
  lang = PERIODS_LANAG;

  constructor(
    protected datePipe: DatePipe,
    protected activatedRoute: ActivatedRoute,
    protected authService: AuthService,
    protected _Autoservicios: Autoservicios,
    protected gaService?: GAService
  ) {
    super(gaService);
    this.auth = this.authService.retrieveEntity();
    this.policyNumber = this.activatedRoute.snapshot.params['policyNumber'];
  }

  getPeriods(
    registros?: number,
    pagina?: number,
    fechaInicioVigencia?: string,
    fechaFinVigencia?: string,
    additionals?: boolean
  ): void {
    this.showLoading = true;
    const numeroPoliza = this.policyNumber;
    const getQueryReq = {
      codigoApp: APPLICATION_CODE,
      tipoDocumento: this.auth.type,
      documento: this.auth.number,
      ...(fechaInicioVigencia && { fechaInicioVigencia }),
      ...(fechaFinVigencia && { fechaFinVigencia }),
      ...(registros && { registros }),
      ...(pagina && { pagina })
    };
    const periodService$ = additionals
      ? this._Autoservicios.ListarPeridoAdicionalesSCTR({ numeroPoliza }, getQueryReq)
      : this._Autoservicios.ListarPeridoSCTR({ numeroPoliza }, getQueryReq);
    periodService$.subscribe(this._mapResponsePeriods.bind(this), () => (this.showLoading = false));
  }

  protected _mapResponsePeriods(response: any): void {
    if (!response || (response && !response.listaResultados.length)) {
      this.showLoading = false;
      this.doActionsWithoutPeriods();

      return void 0;
    }

    this.periodList = response.listaResultados.map(this._mapPeriods.bind(this));
    this.hasMorePeriods = response.informacionAdicional.tieneMasPeriodos || false;
    this.showLoading = false;
    this.doActionsAfterPeriodsAsyncEnds();
  }

  private _mapPeriods(period: ISctrPeriodResponse): ICardPeriodPreview {
    const situationType = period.tipoSituacion === SITUATION_TYPE_SCTR_PERIODS.PAGADO ? null : period.tipoSituacion;
    const situationTypeRed =
      period.tipoSituacion === SITUATION_TYPE_SCTR_PERIODS.RECIBO_VENCIDO ||
      period.tipoSituacion === SITUATION_TYPE_SCTR_PERIODS.RECIBOS_VENCIDOS;
    const trafficLight = period.semaforo ? TRAFFIC_LIGHT_COLOR[period.semaforo] : null;
    const dateStartString = this.datePipe.transform(period.fechaInicioVigencia, FORMAT_DATE_DDMMYYYY);
    const dateEndString = this.datePipe.transform(period.fechaFinVigencia, FORMAT_DATE_DDMMYYYY);
    const dateStart = this.datePipe.transform(period.fechaInicioVigencia, FORMAT_NAME_MONTH_YEAR);
    const dateEnd = this.datePipe.transform(period.fechaFinVigencia, FORMAT_NAME_MONTH_YEAR);
    const fullPeriodDate = dateStart === dateEnd ? `${dateStart}` : `${dateStart} - ${dateEnd}`;

    return {
      month: period.fechaPeriodo.split(' ')[0].trim(),
      year: period.fechaPeriodo.split(' ')[1].trim(),
      periodStatus: period.estadoPeriodo,
      workersQty: period.cantTrabajadores,
      situationType,
      situationTypeRed,
      trafficLight,
      insuredGroup: period.colectivoAsegurado,
      dateStart: dateStartString,
      dateEnd: dateEndString,
      policies: period.polizas.map(this._mapPolicies.bind(this)),
      hasDeclaration: period.tieneDeclaracion,
      hasInclusion: period.tieneInclusion,
      canDeclare: period.puedeDeclarar,
      canBreakdown: period.puedeDesglosar,
      collapse: true,
      fullPeriodDate,
      payload: { ...period }
    };
  }

  private _mapPolicies(policy: ISctrDetailsPeriodResponse): IPeriodDetails {
    return {
      policyType: policy.tipoPoliza,
      policyDescription: policy.descripcionRamo,
      customIcon: policy.tipoPoliza,
      periodStatus: policy.estadoPeriodo,
      workersQty: policy.cantTrabajadores,
      hasOptions: policy.tieneOpciones
    };
  }

  doActionsAfterPeriodsAsyncEnds(): void {}

  doActionsWithoutPeriods(): void {}
}
