import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SendReceiptComponent } from '@mx/components/payment/send-receipt/send-receipt.component';
import { BaseSctrPeriods } from '@mx/components/sctr/card-sctr-periods/base-sctr-periods';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { environment } from '@mx/environments/environment';
import { AuthService } from '@mx/services/auth/auth.service';
import { CALENDAR_ICON, NO_PENDING_PROCEDURES_ICON } from '@mx/settings/constants/images-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';

@Component({
  selector: 'client-card-sctr-periods',
  templateUrl: './card-sctr-periods.component.html',
  providers: [DatePipe]
})
export class CardSctrPeriodsComponent extends BaseSctrPeriods implements OnInit {
  @ViewChild(SendReceiptComponent) modalEmail: SendReceiptComponent;

  @Input() policyType: string;
  @Input() policyNumber: string;
  @Input() mpKeys: string;
  @Input() isAdditional: boolean;

  @Output() hidePeriodCard: EventEmitter<boolean> = new EventEmitter();

  titlePeriods: string;
  footerText: string;
  calendarIcon = CALENDAR_ICON;
  noPeriodsIcon = NO_PENDING_PROCEDURES_ICON;
  messageWithoutData = GeneralLang.Messages.WithoutPeriods;
  collapse: boolean;
  environment = environment;

  constructor(
    protected datePipe: DatePipe,
    protected activatedRoute: ActivatedRoute,
    protected authService: AuthService,
    protected _Autoservicios: Autoservicios,
    protected gaService: GAService,
    protected router: Router
  ) {
    super(datePipe, activatedRoute, authService, _Autoservicios, gaService);
  }

  ngOnInit(): void {
    this.getPeriods(3, 1, null, null, this.isAdditional);
    this.titlePeriods = this.isAdditional ? GeneralLang.Titles.PeriodsAdditionals : GeneralLang.Titles.Periods;
    this.footerText = this.isAdditional ? this.lang.ViewAllAdditionalPeriods : this.lang.ViewAllPeriods;
  }

  goToPeriodReceipts(): void {
    this.router.navigate([`/sctr/sctr-insurance/${this.policyNumber}/periods/receipts`]);
  }

  doActionsWithoutPeriods(): void {
    this.hidePeriodCard.emit(true);
  }
}
