import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FileUtil } from '@mx/core/shared/helpers/util/file';
import { SctrService } from '@mx/services/sctr.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { FILE_ICON } from '@mx/settings/constants/images-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { RECORD_LANG } from '@mx/settings/lang/sctr.lang';
import { IRecordDownloadRequest, IRecordResponse } from '@mx/statemanagement/models/sctr.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-item-sctr-record',
  templateUrl: './item-sctr-record.component.html'
})
export class ItemSctrRecordComponent implements OnDestroy, OnInit {
  @Input() record: IRecordResponse;
  @Input() index: number;
  @Input() isContractor: number;
  @Input() mpKeys: any;

  generalLang = GeneralLang;
  lang = RECORD_LANG;
  fileIcon = FILE_ICON;
  showSpinner: boolean;
  insuredText: string;
  fileUtil: FileUtil;
  routerLink: string;
  policyNumber: string;
  downloadRecordsSub: Subscription;

  constructor(private readonly sctrService: SctrService, private readonly activatedRoute: ActivatedRoute) {
    this.fileUtil = new FileUtil();
  }

  ngOnInit(): void {
    this.insuredText = this.record.cantidadAsegurados === 1 ? 'Asegurado' : 'Asegurados';
    this.formatRouter();
  }

  ngOnDestroy(): void {
    if (this.downloadRecordsSub) {
      this.downloadRecordsSub.unsubscribe();
    }
  }

  downloadRecord(): void {
    if (!this.showSpinner) {
      this.showSpinner = true;
      const params: IRecordDownloadRequest = {
        codigoApp: APPLICATION_CODE,
        numeroPoliza: this.policyNumber,
        numeroConstancia: this.record.numeroConstancia.replace(/\//g, '_')
      };
      this.downloadRecordsSub = this.sctrService.downloadRecords(params).subscribe(
        res => {
          this.fileUtil.download(res.base64, 'pdf', this.record.numeroConstancia);
          this.showSpinner = false;
          this.downloadRecordsSub.unsubscribe();
        },
        err => {
          this.showSpinner = false;
          this.downloadRecordsSub.unsubscribe();
        }
      );
    }
  }

  formatRouter(): void {
    this.activatedRoute.paramMap.subscribe((res: any) => {
      this.policyNumber = res.params.policyNumber;
      const numberRecord = encodeURIComponent(this.record.numeroConstancia);
      this.routerLink = `/sctr/sctr-insurance/${this.policyNumber}/records/${numberRecord}`;
    });
  }
}
