import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GmFilterNotifierModule } from '@mx/components/shared/gm-filter-notifier/gm-filter-notifier.module';
import { GeolocationService } from '@mx/core/shared/helpers/util/geolocation.service';
import { GOOGLE_MAP_API_KEY } from '@mx/settings/constants/general-values';
import { GoogleMapMyServiceComponent } from './google-map-my-service.component';

@NgModule({
  imports: [
    CommonModule,
    GmFilterNotifierModule,
    AgmCoreModule.forRoot({
      apiKey: GOOGLE_MAP_API_KEY
    })
  ],
  providers: [GoogleMapsAPIWrapper, GeolocationService],
  declarations: [GoogleMapMyServiceComponent],
  exports: [GoogleMapMyServiceComponent]
})
export class GoogleMapMyServiceModule {}
