import { Component, Input, NgZone, OnInit } from '@angular/core';
import { GmFilterEvents, GmFilterService } from '@mx/components/shared';
import { GeolocationService } from '@mx/core/shared/helpers/util/geolocation.service';
import { GoogleMapService, GoogleMapsEvents } from '@mx/services/google-map/google-map.service';
import { PIN_END_MARK_ICON, PIN_START_MARK_ICON } from '@mx/settings/constants/images-values';
import { ALLOWED_AREAS_COLOR, MAPFRE_LOCATION } from '@mx/settings/constants/services-mf';
import {
  IAbrevCoords,
  IAddress,
  ICoords,
  IFilterNotificacion,
  ISearchPlace,
  IUserMarker
} from '@mx/statemanagement/models/google-map.interface';
import { Subscription } from 'rxjs';
import { BaseGoogleMaps } from '../base-google-maps';
declare var google;

@Component({
  selector: 'client-google-map-my-service',
  templateUrl: './google-map-my-service.component.html'
})
export class GoogleMapMyServiceComponent extends BaseGoogleMaps implements OnInit {
  @Input() poligons: Array<Array<IAbrevCoords>>;
  @Input() areasColor = ALLOWED_AREAS_COLOR;
  @Input() fullMap: boolean;
  @Input() startPosition: IUserMarker;
  @Input() endPosition: IUserMarker;
  @Input() useFilter = true;
  filterNotification: IFilterNotificacion = { showNotification: false };
  temporalPosition: IUserMarker;
  showTemporalPosition: boolean;
  changeTemporalPosition = false;
  startMarker: IUserMarker = { pinUrl: PIN_START_MARK_ICON };
  endMarker: IUserMarker = { pinUrl: PIN_END_MARK_ICON };
  hasPermission: boolean;
  showStartMarker = true;
  showEndMarker = true;
  geolocationSub: Subscription;

  constructor(
    private readonly geolocationService: GeolocationService,
    protected googleMapService: GoogleMapService,
    private readonly gmFilterService: GmFilterService,
    private readonly ngZone: NgZone
  ) {
    super(googleMapService);
  }

  ngOnInit(): void {
    this.defaultValues();
    this.handlerEvents();
  }

  handlerEvents(): void {
    this.googleMapEventSub = this.googleMapService.onEvent().subscribe(res => {
      if (res) {
        switch (res.event) {
          case GoogleMapsEvents.SET_START_MARKER:
            this.hanlderMarker(
              this.startMarker,
              res.data,
              res.data.hasOwnProperty('goAddress') ? res.data.goAddress : true
            );
            break;
          case GoogleMapsEvents.SET_END_MARKER:
            this.hanlderMarker(
              this.endMarker,
              res.data,
              res.data.hasOwnProperty('goAddress') ? res.data.goAddress : true
            );
            break;
          case GoogleMapsEvents.DEFAULT_MAP:
            this.defaultValues();
            break;
          case GoogleMapsEvents.APPLY_BOUNCE:
            setTimeout(
              () => {
                this.applyBounce();
              },
              res.data ? 10 : 0
            );
            break;
          case GoogleMapsEvents.ZOOM_CONTROL:
            this.options.zoomControl = res.data;
            break;
          case GoogleMapsEvents.DRAG_END:
            this.dragendChange();
            break;
          case GoogleMapsEvents.DRAG_START:
            this.dragstartChange();
            break;
          case GoogleMapsEvents.CHANGE_OPTIONS_MAPS:
            this.options = { ...this.options, ...res.data };
            break;
          default:
            break;
        }
      }
    });

    this.googleTemporalPositionSub = this.googleMapService.onTemporalPosition().subscribe(res => {
      const currentMarker = this.googleMapService.getSearchPlaceEventMarker();
      if (currentMarker === GoogleMapsEvents.SET_START_MARKER) {
        this.temporalPosition = this.startMarker;
      } else if (currentMarker === GoogleMapsEvents.SET_END_MARKER) {
        this.temporalPosition = this.endMarker;
      }
      this.changeTemporalPosition = true;
      this.showTemporalPosition = true;
    });

    this.gmFilterServiceSub = this.gmFilterService.onEvent().subscribe(res => {
      if (res) {
        switch (res.event) {
          case GmFilterEvents.FILTER_NOTIFY:
            this.filterNotification.showNotification = false;
            this.filterNotification.textNotification = res.data.text;
            this.filterNotification.iconNotification = res.data.icon;
            setTimeout(() => (this.filterNotification.showNotification = true), 10);
            break;
          default:
            break;
        }
      }
    });
  }

  onDestroy(): void {
    this.startMarker = { pinUrl: PIN_START_MARK_ICON };
    this.endMarker = { pinUrl: PIN_END_MARK_ICON };
    if (this.geolocationSub) {
      this.geolocationSub.unsubscribe();
    }
  }

  defaultValues(): void {
    // Valores por defecto
    this.options.latitude = MAPFRE_LOCATION.latitude;
    this.options.longitude = MAPFRE_LOCATION.longitude;
    this.options.zoom = MAPFRE_LOCATION.minZoom;
    this.options.zoomControl = true;
    this.zoomMarker = MAPFRE_LOCATION.markerZoom;

    // Marcas para detalle de servicios
    if (this.startPosition) {
      this.startMarker = this.startPosition;
    }
    if (this.endPosition) {
      this.endMarker = this.endPosition;
    }
  }

  applyBounce(): void {
    const listCoords: Array<ICoords> = [];

    if (this.startMarker.latitude || this.endMarker.latitude) {
      listCoords.push(this.startMarker);
      listCoords.push(this.endMarker);
      listCoords.push(MAPFRE_LOCATION);

      this.boundsMap(listCoords);
    } else {
      this.defaultValues();
    }
  }

  hanlderMarker(marker: IUserMarker, data: ISearchPlace, goAddress: boolean): void {
    const coords: ICoords = {
      latitude: ((data && data.coords) || {}).latitude,
      longitude: ((data && data.coords) || {}).longitude
    };
    const isValidUbication = BaseGoogleMaps.isInsidePoligon(coords, this.poligons);
    const applyZoom = this.zoomMarker > this.options.zoom;
    if (this.googleMapService.getUseFocusedSearch() && !isValidUbication && data.coords) {
      this.temporalPosition = marker;
      this.getAddress(coords.latitude, coords.longitude);
      this.setPosition(coords.latitude, coords.longitude, applyZoom);
    } else {
      marker.latitude = ((data && data.coords) || {}).latitude;
      marker.longitude = ((data && data.coords) || {}).longitude;
      this.temporalPosition = marker;
      if (data && data.coords) {
        if (goAddress) {
          this.getAddress(marker.latitude, marker.longitude);
        }
        this.setPosition(data && data.coords.latitude, data && data.coords.longitude, applyZoom);
      }
    }
  }

  // Ubicacion del Usuario
  getUserLocation(): void {
    this.geolocationSub = this.geolocationService.getCurrentPosition().subscribe(
      position => {
        this.options.latitude = position.latitude;
        this.options.longitude = position.longitude;
      },
      () => {}
    ); // Para que no se muestre error en consola
  }

  mapClick(event: any): void {
    this.googleMapService.eventEmit(GoogleMapsEvents.MAP_CLICK);
    if (!this.googleMapService.getUseFocusedSearch()) {
      this.temporalPosition = {
        latitude: (event.coords && event.coords.lat) || (event.latLng && event.latLng.lat()),
        longitude: (event.coords && event.coords.lng) || (event.latLng && event.latLng.lng()),
        pinUrl: this.temporalPosition ? this.temporalPosition.pinUrl : ''
      };
      this.sendLocation(this.temporalPosition, true);
    }
  }

  centerChange(event: any): void {
    this.googleMapService.eventEmit(GoogleMapsEvents.CENTER_CHANGED);
    if (this.changeTemporalPosition) {
      this.temporalPosition = {
        latitude: event.lat,
        longitude: event.lng,
        pinUrl: this.temporalPosition ? this.temporalPosition.pinUrl : ''
      };
    }
  }

  idleChange(): void {
    if (this.googleMapService.getUseFocusedSearch()) {
      this.visibleMarker(true);
      this.sendLocation(this.temporalPosition, false);
    }
  }

  sendLocation(position: ICoords, goAddress: boolean): void {
    const currentMarker = this.googleMapService.getSearchPlaceEventMarker();
    if (currentMarker) {
      this.googleMapService.eventEmit(currentMarker, { coords: position, goAddress });
    }
  }

  dragendChange(): void {
    if (this.googleMapService.getUseFocusedSearch()) {
      this.changeTemporalPosition = false;
      this.showTemporalPosition = false;
      this.visibleMarker(true);
      this.sendLocation(this.temporalPosition, true);
    }
  }

  dragstartChange(): void {
    if (this.googleMapService.getUseFocusedSearch()) {
      this.changeTemporalPosition = true;
      this.showTemporalPosition = true;
      this.visibleMarker(false);
    }
  }

  private visibleMarker(value: boolean): void {
    this.showStartMarker = value;
    this.showEndMarker = value;
  }

  getAddress(latitude: number, longitude: number): void {
    const geocoder = new google.maps.Geocoder();
    const latlng = new google.maps.LatLng(latitude, longitude);
    const request = { latLng: latlng };
    geocoder.geocode(request, (results, status) => {
      this.ngZone.run(() => {
        this.formatAddress(results, status, latitude, longitude);
      });
    });
  }

  formatAddress(results: Array<any>, status: string, latitude: number, longitude: number): void {
    if (status === google.maps.GeocoderStatus.OK) {
      const result = results[0];
      const coords = { latitude, longitude };

      if (result) {
        const address = result.formatted_address;
        const addressComplete: IAddress = {};
        const streets_numbers = result.address_components.filter(x => x.types.includes('street_number'));
        const refOnes = result.address_components.filter(x => x.types.includes('route'));
        const localitys = result.address_components.filter(x => x.types.includes('locality'));
        const subLocalitys = result.address_components.filter(x => x.types.includes('sublocality'));
        const postalCodes = result.address_components.filter(x => x.types.includes('postal_code'));
        const departments = result.address_components.filter(x => x.types.includes('administrative_area_level_1'));
        const provinces = result.address_components.filter(x => x.types.includes('administrative_area_level_2'));

        if (streets_numbers.length >= 1) {
          addressComplete.streetNumber = streets_numbers[0].long_name;
        }
        if (refOnes.length >= 1) {
          addressComplete.refOne = refOnes[0].long_name;
        }

        if (subLocalitys.length >= 1) {
          addressComplete.refTwo = subLocalitys[0].long_name;
        }

        if (localitys.length >= 1) {
          addressComplete.district = localitys[0].long_name;
        }

        if (departments.length >= 1) {
          addressComplete.departament = departments[0].long_name;
        }

        if (provinces.length >= 1) {
          addressComplete.province = provinces[0].long_name;
        }

        if (postalCodes.length >= 1) {
          addressComplete.postalCode = postalCodes[0].long_name;
        }
        this.googleMapService.setSearchMarker({
          coords,
          address,
          addressComplete,
          valid: BaseGoogleMaps.isInsidePoligon(coords, this.poligons)
        });
      } else {
        this.googleMapService.setSearchMarker({ coords: {} });
      }
    } else {
      this.googleMapService.setSearchMarker({ coords: {} });
    }
  }

  getGoogleMapService(): GoogleMapService {
    return this.googleMapService;
  }
}
