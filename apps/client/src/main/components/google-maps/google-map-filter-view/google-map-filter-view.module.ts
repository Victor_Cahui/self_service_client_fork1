import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { GOOGLE_MAP_API_KEY } from '@mx/settings/constants/general-values';
import { GmFilterNotifierModule } from '../../shared/gm-filter-notifier/gm-filter-notifier.module';
import { GoogleMapFilterViewComponent } from './google-map-filter-view.component';

@NgModule({
  imports: [
    CommonModule,
    AgmCoreModule.forRoot({
      apiKey: GOOGLE_MAP_API_KEY
    }),
    AgmJsMarkerClustererModule,
    GmFilterNotifierModule,
    MfLoaderModule
  ],
  providers: [GoogleMapsAPIWrapper],
  declarations: [GoogleMapFilterViewComponent],
  exports: [GoogleMapFilterViewComponent]
})
export class GoogleMapFilterViewModule {}
