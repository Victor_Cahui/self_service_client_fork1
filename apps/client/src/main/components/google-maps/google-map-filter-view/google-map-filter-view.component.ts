import { ClusterStyle } from '@agm/js-marker-clusterer/services/google-clusterer-types';
import { DOCUMENT } from '@angular/common';
import { Component, EventEmitter, Inject, Input, OnChanges, OnInit, Output, Renderer2 } from '@angular/core';
import { PolicyUtil } from '@mx/components/shared/utils/policy.util';
import { GeolocationService } from '@mx/core/shared/helpers/util/geolocation.service';
import { PLATFORMS, PlatformService } from '@mx/core/shared/helpers/util/platform.service';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { environment } from '@mx/environments/environment';
import { GeneralService } from '@mx/services/general/general.service';
import { GoogleMapService, GoogleMapsEvents } from '@mx/services/google-map/google-map.service';
import { HealthService } from '@mx/services/health/health.service';
import { STORAGE_KEYS } from '@mx/services/policies.service';
import { APPLICATION_CODE, DEFAULT_ZOOM_MAP } from '@mx/settings/constants/general-values';
import {
  GEOLOCATION_ICON,
  HEART_ICON,
  HEART_OUT_ICON,
  MARKER_CLUSTER_ICON,
  PIN_MAP_ICON
} from '@mx/settings/constants/images-values';
import { ZOOM_LEVEL_MARKER_MAP } from '@mx/settings/constants/key-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { IParameterApp } from '@mx/statemanagement/models/general.models';
import { ICoords, IItemMarker, IUserMarker } from '@mx/statemanagement/models/google-map.interface';
import { IPoliciesByClientResponse } from '@mx/statemanagement/models/policy.interface';
import { Observable } from 'rxjs';
import { GmFilterEvents, GmFilterService } from '../../shared/gm-filter';
import { BaseGoogleMaps } from '../base-google-maps';

@Component({
  selector: 'client-google-map-filter-view',
  templateUrl: './google-map-filter-view.component.html',
  providers: [GeolocationService]
})
export class GoogleMapFilterViewComponent extends BaseGoogleMaps implements OnChanges, OnInit {
  @Input() markers: Array<IItemMarker>;
  @Input() geolocationButtonClass: string;
  @Input() configView: any;
  @Input() policy: IPoliciesByClientResponse;

  @Output() viewDetail: EventEmitter<IItemMarker>;
  @Output() userPosition: EventEmitter<ICoords>;
  @Output() getClinicsAsGuest: EventEmitter<any>;
  @Output() favorite: EventEmitter<IItemMarker>;
  @Output() goToAppointment: EventEmitter<IItemMarker> = new EventEmitter();

  noInfo = GeneralLang.Texts.WithOutInformation;

  viewDetailText = GeneralLang.Buttons.ViewDetail;
  deductibles: string;
  btnTexts = GeneralLang.Buttons;
  userMarker: IUserMarker = { pinUrl: PIN_MAP_ICON };
  hasPermisionGPS: boolean;
  previousIndex: number;
  clusterStyles: Array<ClusterStyle>;
  heartIcon = HEART_ICON;
  heartOutIcon = HEART_OUT_ICON;
  geolocationIcon = GEOLOCATION_ICON;

  isIE: boolean;
  filterNotification: boolean;
  parametersSubject: Observable<Array<IParameterApp>>;

  activateSearchClinicKey = environment.ACTIVATE_SEARCH_CLINIC_CLD;
  codRamoClinicDigital = 275;

  constructor(
    protected googleMapService: GoogleMapService,
    private readonly geolocationService: GeolocationService,
    private readonly renderer2: Renderer2,
    private readonly generalService: GeneralService,
    private readonly platformService: PlatformService,
    private readonly gmFilterService: GmFilterService,
    private readonly storageService: StorageService,
    private readonly _Autoservicios: Autoservicios,
    private readonly healthService: HealthService,
    @Inject(DOCUMENT) private readonly document: Document
  ) {
    super(googleMapService);
    this.isIE = this.platformService.is(PLATFORMS.IE);
    this.viewDetail = new EventEmitter<IItemMarker>();
    this.userPosition = new EventEmitter<ICoords>();
    this.getClinicsAsGuest = new EventEmitter<any>();
    this.favorite = new EventEmitter<IItemMarker>();
    this.previousIndex = -1;
    this.clusterStyles = [
      {
        textColor: 'white',
        url: `${MARKER_CLUSTER_ICON}?`,
        height: 60,
        width: 60,
        textSize: 14
      }
    ];
  }

  ngOnInit(): void {
    this.getParams();
    this.setMapOptions();
    this.setPosition(this.latitude, this.longitude);
    this.goToMyPosition();
    this.googleMapSub = this.googleMapService.getItemMarker().subscribe((item: IItemMarker) => {
      if (item) {
        const index = this.markers.findIndex(marker => marker.id === item.id);
        this.fnClickMarker(item, index);
      }
    });

    this.googleMapEventSub = this.googleMapService.onEvent().subscribe(res => {
      if (res) {
        switch (res.event) {
          case GoogleMapsEvents.VIEW_DETAILS:
            if (res.data) {
              this.viewDetailEvent(res.data, false);
            }
            break;
          case GoogleMapsEvents.CHANGE_MARKERS:
            this.changeAllMarkers(res.data || []);
            break;
          case GoogleMapsEvents.RESET_MARKERS:
            this.changeMarkersToDefaultPin(this.markers);
            break;
          default:
            break;
        }
      }
    });

    this.gmFilterServiceSub = this.gmFilterService.onEvent().subscribe(res => {
      if (res) {
        switch (res.event) {
          case GmFilterEvents.FILTER_NOTIFY:
            this.filterNotification = res.data;
            break;
          default:
            break;
        }
      }
    });
  }

  ngOnChanges(): void {
    if (this.latitude && this.longitude) {
      this.setPosition(this.latitude, this.longitude);
    }
  }

  setMapOptions(): void {
    this.options.styles = [
      {
        featureType: 'poi',
        stylers: [{ visibility: 'off' }]
      }
    ];
  }

  fnClickMarker(item: IItemMarker, index: number): void {
    const clinicImagePortraitStorage = this.healthService.getImagePortraitClinic(item.payload.clinicaId.toString());
    if (clinicImagePortraitStorage === null) {
      this._Autoservicios
        .ObtenerImagenClinica({ codigoApp: APPLICATION_CODE }, { codigoImg: item.payload.imagenPortraitUrl })
        .subscribe(r => {
          this.healthService.setImagePortraitClinic(item.payload.clinicaId.toString(), r.base64);
          item.imageUrl = r.base64;
          item.payload.imagenPortraitUrl = r.base64;
        });
      this._Autoservicios
        .ObtenerImagenClinica({ codigoApp: APPLICATION_CODE }, { codigoImg: item.payload.imagenLandscapeUrl })
        .subscribe(r => {
          this.healthService.setImageLandScapeClinic(item.payload.clinicaId.toString(), r.base64);
          item.payload.imagenLandscapeUrl = r.base64;
        });
    } else {
      item.imageUrl = clinicImagePortraitStorage;
      item.payload.imagenPortraitUrl = clinicImagePortraitStorage;
      item.payload.imagenLandscapeUrl = this.healthService.getImageLandScapeClinic(item.payload.clinicaId.toString());
    }
    this.setPosition(item.latitude, item.longitude);
    this.autoCloseInfoWindow(index);
    this.handlerSelected(item, !item.openInfoWindow);
  }

  handlerSelected(item: IItemMarker, isOpen: boolean): void {
    this.markers.forEach(marker => {
      const pinUrl = marker.pinUrl || '';
      const isGray = pinUrl.indexOf('-gray') > 0;

      if ((isOpen && isGray) || (marker.id === item.id && isGray)) {
        marker.pinUrl = pinUrl.split('-gray').join('');
      } else if (marker.id !== item.id && !isGray) {
        marker.pinUrl = pinUrl.split('.png').join('-gray.png');
      }
    });
  }

  autoCloseInfoWindow(index: number): void {
    if (this.previousIndex > -1 && this.previousIndex !== index) {
      this.markers[this.previousIndex].openInfoWindow = false;
    }
    if (this.markers[index]) {
      const openInfoWindow = !this.markers[index].openInfoWindow;
      this.markers[index].openInfoWindow = openInfoWindow;
    }
    this.previousIndex = index;
  }

  closeInfoWindowEvent(item: IItemMarker): void {
    if (item.openInfoWindow) {
      this.handlerSelected(item, item.openInfoWindow);
      item.openInfoWindow = false;
    }
  }

  viewDetailEvent(marker: IItemMarker, isMap: boolean = true): void {
    if (this.previousIndex > -1) {
      this.markers[this.previousIndex].openInfoWindow = false;
    }
    if (!isMap && marker) {
      this.setPosition(marker.latitude, marker.longitude);
      this.handlerSelected(marker, marker.openInfoWindow);
    } else if (isMap) {
      this.viewDetail.next(marker);
    }
  }

  appointment(marker: IItemMarker): void {
    this.goToAppointment.next(marker);
  }

  changeAllMarkers(markers: Array<IItemMarker>): void {
    if (this.previousIndex > -1) {
      this.markers[this.previousIndex].openInfoWindow = false;
      this.previousIndex = -1;
    }
    // tslint:disable-next-line: no-parameter-reassignment
    markers = this.changeMarkersToDefaultPin(markers);

    if (markers.length > 1 || (markers.length === 1 && this.userMarker.latitude)) {
      if (this.userMarker.latitude) {
        markers.push(this.userMarker as IItemMarker);
      }

      this.boundsMap(markers as Array<ICoords>);
    } else if (markers.length === 1) {
      this.setPosition(markers[0].latitude, markers[0].longitude);
    }
    setTimeout(() => (this.markers = markers), 0);
  }

  addClassMyPosition(): void {
    const elements: any = this.document.getElementsByTagName('img');
    const imgs = [];
    for (const element of elements) {
      imgs.push(element);
    }
    const userMarkers = imgs.filter(
      img => (img.currentSrc || img.src) === `${location.origin}/${this.userMarker.pinUrl}`
    );
    const userMarker = userMarkers.length ? userMarkers[0] : null;
    if (userMarker) {
      this.renderer2.addClass(userMarker.parentElement, 'g-map-filter--user-marker');
    }
  }

  addClassInfoWindows(): void {
    const isOpenInfoWindow = this.markers.find(marker => marker.openInfoWindow);
    if (isOpenInfoWindow) {
      try {
        const element = this.document.getElementsByClassName('gm-style-iw')[0];
        this.renderer2.addClass(element.parentElement, 'gm-info-windows-parent');
      } catch (error) {
        setTimeout(() => {
          this.addClassInfoWindows();
        }, 500);
      }
    }
  }

  trackByIndex(index: number, item: IItemMarker): number {
    return index;
  }

  changeMarkersToDefaultPin(markers: Array<IItemMarker>): Array<IItemMarker> {
    if (!markers) {
      return void 0;
    }

    return markers.map(marker => {
      marker.openInfoWindow = false;
      marker.pinUrl = marker.pinUrl.split('-gray').join('');

      return marker;
    });
  }

  goToMyPosition(): void {
    const hasHealthPolicies = this.storageService.getStorage(STORAGE_KEYS.HAS_HEALTH_POLICIES);
    this.googleMapService.eventEmit(GoogleMapsEvents.RESET_MARKERS, this.markers);
    this.geolocatonSub = this.geolocationService.getCurrentPosition().subscribe(
      position => {
        this.hasPermisionGPS = true;

        if (!this.userMarker.latitude && !this.userMarker.longitude) {
          this.userPosition.next({ latitude: position.latitude, longitude: position.longitude });
        }

        this.userMarker.latitude = position.latitude;
        this.userMarker.longitude = position.longitude;
        setTimeout(
          () => {
            this.addClassMyPosition();
          },
          this.isIE ? 2000 : 1000
        );
        this.setPosition(position.latitude, position.longitude);
      },
      err => {
        this.hasPermisionGPS = false;
        if (!hasHealthPolicies) {
          this.getClinicsAsGuest.next();
        }
      },
      () => {
        this.geolocatonSub.unsubscribe();
      }
    );
  }

  // Parametros Generales
  getParams(): void {
    this.parametersSubject = this.generalService.getParametersSubject();
    this.parametersSubject.subscribe(params => {
      this.zoomMarker = Number(this.generalService.getValueParams(ZOOM_LEVEL_MARKER_MAP)) || DEFAULT_ZOOM_MAP;
    });
  }

  favoriteFn(marker: IItemMarker): void {
    marker.isFavorite = !marker.isFavorite;
    this.favorite.next(marker);
  }

  getDeductibles(item): string {
    return item ? PolicyUtil.getDeductibleText(item) : null;
  }
}
