import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { MfCheckboxModule, MfSelectModule } from '@mx/core/ui';
import { GmFilterViewClinicComponent } from './gm-filter-view-clinic.component';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, MfCheckboxModule, MfSelectModule, GoogleModule],
  entryComponents: [GmFilterViewClinicComponent],
  exports: [GmFilterViewClinicComponent],
  declarations: [GmFilterViewClinicComponent]
})
export class GmFilterViewClinicModule {}
