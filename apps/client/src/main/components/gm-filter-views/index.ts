export { GmFilterViewOfficeModule } from './gm-filter-view-office/gm-filter-view-office.module';
export { GmFilterViewOfficeComponent } from './gm-filter-view-office/gm-filter-view-office.component';

export { GmFilterViewClinicModule } from './gm-filter-view-clinic/gm-filter-view-clinic.module';
export { GmFilterViewClinicComponent } from './gm-filter-view-clinic/gm-filter-view-clinic.component';
