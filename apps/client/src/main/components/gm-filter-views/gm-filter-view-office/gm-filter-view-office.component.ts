import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl } from '@angular/forms';
import { GmFilterBase } from '@mx/components/shared/gm-filter/extends/gm-filter-base';
import { GmFilterEvents, GmFilterService } from '@mx/components/shared/gm-filter/providers/gm-filter.service';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { UbigeoService } from '@mx/services/ubigeo.service';
import { OficceFormFilter } from '@mx/settings/lang/office.lang';
import { ProfileLang } from '@mx/settings/lang/profile.lang';
import { IItemMarker } from '@mx/statemanagement/models/google-map.interface';
import { IUbigeoResponse } from '@mx/statemanagement/models/ubigeo.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-gm-filter-view-office',
  templateUrl: './gm-filter-view-office.component.html'
})
export class GmFilterViewOfficeComponent extends GmFilterBase implements OnInit, OnDestroy {
  services = OficceFormFilter.firstFilter;
  location = OficceFormFilter.secondFilter;
  allServices: boolean;
  someUbigeo: boolean;
  selectedServices: number;
  checkboxStates: Array<boolean>;
  filterNotified: boolean;

  labelDepartment = ProfileLang.Labels.Department;
  labelProvince = ProfileLang.Labels.Province;
  labelDistrict = ProfileLang.Labels.District;

  departamento: AbstractControl;
  provincia: AbstractControl;
  distrito: AbstractControl;
  ubigeoApplied: Array<string>;
  ubigeo = ['departamento', 'provincia', 'distrito'];

  departamentSub: Subscription;
  provinceSub: Subscription;
  districtSub: Subscription;

  constructor(
    public formBuilder: FormBuilder,
    public gmFilterService: GmFilterService,
    public ubigeoService: UbigeoService,
    protected gaService: GAService
  ) {
    super(formBuilder, gmFilterService, ubigeoService, gaService);
    this.checkboxStates = [];
    const controls = this.services.items.map(c => new FormControl(false));
    this.formFilter = this.formBuilder.group({
      services: new FormArray(controls)
    });
    this.formFilter.addControl('todos', new FormControl(''));
    this.departamentos = this.provincias = this.distritos = [];
    this.ubigeoApplied = ['', '', ''];
  }

  ngOnInit(): void {
    this.formFilter.controls['todos'].setValue(true);
    this.applyFilter.subscribe(() => {
      this.onFilter();
    });
    // restablecer
    this.applyReset.subscribe(() => {
      this.selectedServices = 0;
      this.allServices = false;
      this.filterButtonState.next(true);
      this.checkAllServices();
      this.clearUbigeo();
    });
    this.selectedServices = this.services.items.length;
    this.checkAllServices();
    this.addUbigeo();
    this.getDepartamentos();
    this._getOpenedFilterRx$ = this.gmFilterService.getOpenedFilterRx().subscribe(st => {
      if (st) {
        this.restoreDepartment('departamento');
        this.restoreProvince('departamento', 'provincia');
        this.restoreDistrict('provincia', 'distrito');
      }
    });
  }

  addUbigeo(): void {
    this.formFilter.addControl(this.ubigeo[0], new FormControl(''));
    this.formFilter.addControl(this.ubigeo[1], new FormControl(''));
    this.formFilter.addControl(this.ubigeo[2], new FormControl(''));
    this.departamento = this.formFilter.controls[this.ubigeo[0]];
    this.provincia = this.formFilter.controls[this.ubigeo[1]];
    this.distrito = this.formFilter.controls[this.ubigeo[2]];
  }

  clearUbigeo(): void {
    this.departamento.setValue('');
    this.provincia.setValue('');
    this.distrito.setValue('');
    // si reestablece
    this.provincias = this.distritos = [];
  }

  ngOnDestroy(): void {
    if (this._getOpenedFilterRx$) {
      this.gmFilterService.emitOpenedFilterRx(false);
      this._getOpenedFilterRx$.unsubscribe();
    }
    this.gmFilterService.eventEmit(GmFilterEvents.FILTERS_ACTIVED, false);
    if (this.departamentSub) {
      this.departamentSub.unsubscribe();
    }
    if (this.provinceSub) {
      this.provinceSub.unsubscribe();
    }
    if (this.districtSub) {
      this.districtSub.unsubscribe();
    }
  }

  checkAllServices(): void {
    this.allServices = !this.allServices;
    this.selectedServices = this.allServices ? this.services.items.length : 0;
    this.formFilter.controls['services'].setValue(
      this.formFilter.controls['services'].value.map(() => this.allServices)
    );
    this.filterButtonState.next(this.allServices);
  }

  // apply filter
  onFilter(): void {
    this._appliedFrm = { ...this.formFilter.value };
    this._appliedOptionsUbigeo = {
      province: [...this.provincias],
      district: [...this.distritos]
    };
    this.items = this.gmFilterService.getItems();
    const selectedKeyServices = this.formFilter.value.services
      .map((officeService: boolean, i: number) => {
        this.checkboxStates[i] = !!officeService;

        return officeService ? this.services.items[i].value : null;
      })
      .filter(officeService => officeService);
    const filteredOfficesByServices = this.items.filter(item =>
      item.payload.servicios.find((el: any) => selectedKeyServices.indexOf(el.codigo) !== -1)
    );
    // filtrar por ubigeo
    const filteredOffices = this.filterByUbigeo(filteredOfficesByServices);
    this.gmFilterService.eventEmit(GmFilterEvents.FILTER, filteredOffices);
    this.ubigeoApplied = this.ubigeoApplied.map((item, i) => this.formFilter.controls[this.ubigeo[i]].value);
    if (this.ubigeoApplied[0] === '') {
      this.ubigeoApplied[0] = '';
      this.someUbigeo = false;
    }
    this.showFiltersIndicator();
  }

  filterByUbigeo(offices: Array<any>): Array<IItemMarker> {
    const departamento = this.departamento.value;
    const provincia = this.provincia.value;
    const distrito = this.distrito.value;

    return offices
      .filter(of => (departamento ? +of.payload.departamentoId === +departamento : true))
      .filter(of => (provincia ? +of.payload.provinciaId === +provincia : true))
      .filter(of => (distrito ? +of.payload.distritoId === +distrito : true));
  }

  serviceClicked(event): void {
    event.target.checked ? this.selectedServices++ : this.selectedServices--;
    if (this.allServices) {
      this.allServices = false;
    } else {
      if (this.selectedServices === this.services.items.length) {
        this.allServices = true;
      }
    }
    this.filterButtonState.next(!!this.selectedServices);
  }

  // back button
  resetState(): void {
    this.gmFilterService.emitOpenedFilterRx(false);
    if (!!this.checkboxStates && !!this.checkboxStates.length) {
      this.selectedServices = this.checkboxStates.filter(e => e).length;
      this.allServices = this.selectedServices === this.services.items.length;
      this.formFilter.controls['services'].setValue(
        this.formFilter.controls['services'].value.map((officeService: boolean, i: number) => this.checkboxStates[i])
      );
    } else {
      this.allServices = true;
      this.formFilter.controls['services'].setValue(this.formFilter.controls['services'].value.map(() => true));
    }

    this.filterButtonState.next(true);
  }

  showFiltersIndicator(): void {
    this.filterNotified = this.gmFilterService.getFilteredOfficesNotified();
    if (!this.filterNotified && (!this.allServices || this.someUbigeo)) {
      this.gmFilterService.setFilteredOfficesNotified(true);
      this.gmFilterService.eventEmit(GmFilterEvents.FILTER_NOTIFY, true);
    }
    this.gmFilterService.eventEmit(GmFilterEvents.FILTERS_ACTIVED, !this.allServices || this.someUbigeo);
  }

  selectDepartment(): void {
    this.setNullUbigeo(this.ubigeo[1]);
    this.setControl(this.provincia);
    this.setControl(this.distrito);
    this.getProvincias(this.departamento.value);
  }

  selectProvince(): void {
    this.setNullUbigeo(this.ubigeo[2]);
    this.setControl(this.distrito);
    this.getDistritos(this.provincia.value);
  }

  getDepartamentos(departamentoId?: string, provinciaId?: string): void {
    if (!this.departamentos.length && departamentoId !== '') {
      this.departamentSub = this.ubigeoService.getDepartaments().subscribe((res: Array<IUbigeoResponse>) => {
        this.departamentos = this.setUbigeoData(res);
        this.departamento.setValue('');
        if (!!departamentoId) {
          this.getProvincias(departamentoId, provinciaId);
        }
        this.departamentSub.unsubscribe();
      });
    } else {
      if (!!departamentoId) {
        this.getProvincias(departamentoId, provinciaId);
      }
    }
  }

  getProvincias(departamentoId: string, provinciaId?: string): void {
    if (!!departamentoId && departamentoId !== '' && (!this.provincias.length || !!provinciaId)) {
      this.provinceSub = this.ubigeoService.getProvinces(departamentoId).subscribe((res: Array<IUbigeoResponse>) => {
        this.provincias = this.setUbigeoData(res);
        this.provincia.setValue('');
        if (!!provinciaId) {
          this.getDistritos(provinciaId);
        }
        this.provinceSub.unsubscribe();
      });
    }
  }

  getDistritos(provinciaId: string): void {
    if (!!provinciaId && provinciaId !== '' && !this.distritos.length) {
      this.districtSub = this.ubigeoService.getDistricts(provinciaId).subscribe((res: Array<IUbigeoResponse>) => {
        this.distritos = this.setUbigeoData(res);
        this.distrito.setValue('');
        this.districtSub.unsubscribe();
      });
    }
  }

  resetUbigeo(): void {
    this.ubigeoService.getProvinces(this.ubigeoApplied[0]).subscribe((res: Array<IUbigeoResponse>) => {
      this.provincias = this.setUbigeoData(res);
      this.provincia.setValue(this.ubigeoApplied[1]);
    });
    this.ubigeoService.getDistricts(this.ubigeoApplied[1]).subscribe((res: Array<IUbigeoResponse>) => {
      this.distritos = this.setUbigeoData(res);
      this.distrito.setValue(this.ubigeoApplied[2]);
    });
  }

  setNullUbigeo(name: string): void {
    if (name === this.ubigeo[1] && this.provincias.length) {
      this.provincias = this.distritos = [];
    }
    if (name === this.ubigeo[2] && this.distritos.length) {
      this.distritos = [];
    }
  }

  setControl(control: AbstractControl, value?: string): void {
    this.someUbigeo = true;
    if (!!value) {
      control.setValue(value);
    } else {
      control.setValue('');
      control.markAsUntouched();
      control.markAsPristine();
    }
  }
}
