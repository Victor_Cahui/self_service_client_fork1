import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { MfCheckboxModule, MfSelectModule } from '@mx/core/ui';
import { GmFilterViewOfficeComponent } from './gm-filter-view-office.component';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, MfCheckboxModule, MfSelectModule, GoogleModule],
  entryComponents: [GmFilterViewOfficeComponent],
  exports: [GmFilterViewOfficeComponent],
  declarations: [GmFilterViewOfficeComponent]
})
export class GmFilterViewOfficeModule {}
