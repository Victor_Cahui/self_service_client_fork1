import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DropdownItemsPolicyModule } from '@mx/components/shared/dropdown/dropdown-items-policy/dropdown-items-policy.module';
import { IconPoliciesModule } from '@mx/components/shared/icon-policies/icon-polices.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { TooltipsModule } from '@mx/core/ui/lib/components/tooltips/tooltips.module';
import { LinksModule } from '@mx/core/ui/lib/links/links.module';
import { CardAffiliatesComponent } from './card-affiliates.component';

@NgModule({
  imports: [
    CommonModule,
    DirectivesModule,
    DropdownItemsPolicyModule,
    GoogleModule,
    IconPoliciesModule,
    LinksModule,
    MfCardModule,
    MfLoaderModule,
    RouterModule,
    TooltipsModule
  ],
  declarations: [CardAffiliatesComponent],
  exports: [CardAffiliatesComponent]
})
export class CardAffiliatesModule {}
