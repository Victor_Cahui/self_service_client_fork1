import { CurrencyPipe, DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import {
  COVERAGE_STATUS,
  coverageStatus,
  POLICY_STATUS,
  TRAFFIC_LIGHT_COLOR
} from '@mx/components/shared/utils/policy';
import { PolicyUtil } from '@mx/components/shared/utils/policy.util';
import { hasWarning } from '@mx/core/shared/helpers/util/date';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { Autoservicios, FrmProvider } from '@mx/core/shared/providers/services';
import { coerceBooleanProp } from '@mx/core/ui/common/helpers';
import { environment } from '@mx/environments/environment';
import { GeneralService } from '@mx/services/general/general.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { PolicyListService } from '@mx/services/policy/policy-list.service';
import {
  APPLICATION_CODE,
  COLOR_STATUS,
  FORMAT_DATE_DDMMYYYY,
  GENERAL_MAX_LENGTH,
  PPFM_ICON,
  REG_EX,
  SEARCH_ORDEN_ASC,
  SEARCH_ORDEN_DESC
} from '@mx/settings/constants/general-values';
import { DOCUMENT_ICON, FILE_ICON, SEARCH_ICON } from '@mx/settings/constants/images-values';
import { ESTADO_POLIZA, SOAT_ANTIC_RENOVAR_DIAS } from '@mx/settings/constants/key-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import { ICardPoliciesView } from '@mx/statemanagement/models/policy.interface';
import { Observable } from 'rxjs/Observable';
import { BaseCardMyPolicies } from './../base-card-my-policies';

@Component({
  providers: [DatePipe, CurrencyPipe],
  selector: 'client-card-my-policies',
  templateUrl: './card-my-policies.component.html'
})
export class CardMyPoliciesComponent extends BaseCardMyPolicies implements OnInit {
  @Input() disableCollapse = false;
  @Input() isVisibleFilterHeader: boolean;
  @Input() hasPaginator: boolean;
  @Input() isHome: boolean;
  @Input()
  get disableIcon(): boolean {
    return this._disableIcon;
  }
  set disableIcon(value: boolean) {
    this._disableIcon = coerceBooleanProp(value);
  }
  protected _disableIcon = true;

  listPolicies: Array<ICardPoliciesView>;
  itemsPerPage = 10;
  slicePolicies: number;
  showLoading: boolean;
  renewDays = +this.generalService.getValueParams(SOAT_ANTIC_RENOVAR_DIAS) || 0;
  btnConfirm = GeneralLang.Buttons.Confirm;
  collapse = false;
  page = 1;
  labelFilterBy = GeneralLang.Labels.FilterBy;
  labelInsuranceType = GeneralLang.Labels.InsuranceType;
  btnCleanFilter = GeneralLang.Buttons.CleanFilter;
  total: number;
  lang = GeneralLang;
  btnFilter = GeneralLang.Buttons.Filter;
  frm: FormGroup;
  showFilter: boolean;
  titleMyPolicies: string;
  isFirstRequest = true;
  trafficLightColor = TRAFFIC_LIGHT_COLOR;
  colorStatus = COLOR_STATUS;
  coverageStatus = COVERAGE_STATUS;
  maxLengthPolice = GENERAL_MAX_LENGTH.numberPolice;
  numericRegExp = REG_EX.numeric;
  isFilteredFrm: boolean;
  expiredStatusSoat = POLICY_STATUS.EXPIRED;
  iconNotFound = DOCUMENT_ICON;
  fileIcon = FILE_ICON;
  msgNotFound = GeneralLang.Messages.InitPolicyItemNotFound;
  codRamoClinicaDigital = 275;
  detailPolicyCldKey = environment.ACTIVATE_DETAIL_POLICY_CLD;

  constructor(
    protected generalService: GeneralService,
    protected policiesService: PoliciesService,
    protected policiesInfoService: PoliciesInfoService,
    protected gaService: GAService,
    private readonly policyListService: PolicyListService,
    private readonly _Autoservicios: Autoservicios,
    private readonly _FrmProvider: FrmProvider,
    private readonly currencyPipe: CurrencyPipe,
    private readonly datePipe: DatePipe,
    private readonly router: Router
  ) {
    super(generalService, policiesService, gaService);
  }

  ngOnInit(): void {
    this._initValues();
    this.getParams();
    this.getPolicyList();
  }

  getPolicyList(): void {
    this.showFilter = false;
    this.showLoading = true;
    this.listPolicies = [];
    this.total = 0;

    this._getService().subscribe(this._mapResponsePolicies.bind(this), () => {
      this.showLoading = false;
    });
  }

  goToDetail(policyNumber, policyType, policy?): void {
    const isPolicyWithouthVigency = policy.coverageStatus === coverageStatus.sinVigencia;
    const queryParams = isPolicyWithouthVigency ? { queryParams: { historical: true } } : {};
    const thePolicyNumber = policy.isSctrDoubleEmission ? policy.policyNumberHeath : policyNumber;

    // ruta depende del tipo de poliza
    const path = `${POLICY_TYPES[policyType].path}/${thePolicyNumber || policyNumber}`;
    this.policiesInfoService.setClientPolicyResponse(policy.response);
    this.policyListService.setPolicyListData({
      policyType,
      list: [policy]
    });
    this.router.navigate([path], queryParams);
  }

  itemsPerPageChange(itemsPerPage?: number): void {
    this.page = 1;
    this.itemsPerPage = itemsPerPage || this.itemsPerPage;
    this._configNotFound();
    this.getPolicyList();
  }

  pageChange(page: number): void {
    this.page = page;
    this._configNotFound();
    this.getPolicyList();
  }

  filter(): void {
    this.isFirstRequest = false;
    this.itemsPerPageChange();
    this.isFilteredFrm = this._isFilteredFrm();
  }

  resetFilter(): void {
    this.frm.reset();
    this.frm.get('activarSoatVencidos').setValue(true);
    this.itemsPerPageChange();
    this.isFilteredFrm = false;
  }

  private _configNotFound(): void {
    this.iconNotFound = SEARCH_ICON;
    this.msgNotFound = this.lang.Messages.ItemNotFound;
  }

  private _getService(): Observable<any> {
    const pathReq = { codigoApp: APPLICATION_CODE };
    const queryReq = {
      criterio: 'fechaFin',
      orden: SEARCH_ORDEN_DESC,
      pagina: this.page,
      registros: this.itemsPerPage,
      pSeccion: this.isHome ? 'HOME' : 'MisPolizas',
      ...this._getObjWithData(this.frm.value),
      estadoPoliza: this._getNoCurrentPolicy(this.frm.value.estadoPoliza)
    };
    const queryReqPaginado = {
      activarSoatVencidos: 'true',
      criterio: 'fechaFin',
      estadoPoliza: ESTADO_POLIZA.vigente.code,
      orden: SEARCH_ORDEN_ASC,
      pagina: 1,
      pSeccion: this.isHome ? 'HOME' : 'MisPolizas',
      registros: 5
    };

    const services = {
      list: this._Autoservicios.ListarPolizasPaginado(pathReq, queryReqPaginado),
      paginator: this._Autoservicios.ListarPolizasPaginado(pathReq, queryReq)
    };

    return this.hasPaginator ? services.paginator : services.list;
  }
  private _isFilteredFrm(): boolean {
    const fk = Object.keys(this.frm.value);

    return !(
      this.frm.value.activarSoatVencidos === true &&
      fk.every(k => (k === 'activarSoatVencidos' ? true : !this.frm.value[k]))
    );
  }

  private _getNoCurrentPolicy(isChecked: boolean): string {
    return isChecked ? undefined : ESTADO_POLIZA.vigente.code;
  }

  private _mapResponsePolicies(res: any): void {
    if (!res) {
      this.showLoading = false;

      return void 0;
    }

    const policies = res.listPoliza;
    if (!policies.length) {
      this.showLoading = false;

      return void 0;
    }

    this.listPolicies = policies.map(this._mapPolicies.bind(this));
    this.hasPaginator && (this.total = res.totalRegistros);

    this.slicePolicies = this.disableCollapse ? this.listPolicies.length : 4;
    this.showLoading = false;
  }

  private _mapPolicies(policy): ICardPoliciesView {
    policy.policyType = policy.tipoPoliza;
    const expiredPolicy = this.fnIsExpiredPolicy(policy.fechaFin);
    const symbol = `${StringUtil.getMoneyDescription(policy.codigoMoneda)} `;
    const semaforo = TRAFFIC_LIGHT_COLOR[policy.semaforo];
    const isEPS = PolicyUtil.isEps(policy.policyType);
    const isSCTR = PolicyUtil.isSctr(policy.policyType);
    const isSctrDoubleEmission = PolicyUtil.isSctrDoubleEmission(policy.numeroPoliza);
    const policyNumberHeath = isSctrDoubleEmission ? policy.numeroPoliza.split(',')[0].trim() : null;
    const policyNumberPension = isSctrDoubleEmission ? policy.numeroPoliza.split(',')[1].trim() : null;
    const isContractor = policy.esContratante;
    const isEPSContractor = policy.tipoPoliza === POLICY_TYPES.MD_EPS.code && policy.esContratante;
    const constanciesLabel = isSCTR
      ? policy.numeroConstancias === 1
        ? `${policy.numeroConstancias} ${PolicyLang.Labels.Constancy}`
        : `${policy.numeroConstancias} ${PolicyLang.Labels.Constancies}`
      : null;
    const affiliates = isEPSContractor && this.getAffiliatesDescription(policy);
    const isExpiredEps = this.isExpiredEps(policy);
    const isPPFM = policy.esBeneficioAdicional;
    const customIcon = isPPFM && PPFM_ICON[policy.policyType];

    return {
      policyType: PolicyUtil.verifyPolicyType(policy),
      policyNumber: policy.numeroPoliza,
      policyNumberHeath,
      policyNumberPension,
      beneficiaries: policy.beneficiarios,
      policy: policy.descripcionPoliza,
      expired: expiredPolicy,
      estado: policy.estado,
      warning: hasWarning(policy.fechaFin, this.renewalDays),
      endDate: this.datePipe.transform(policy.fechaFin, FORMAT_DATE_DDMMYYYY),
      isSoat: this.isSoat(policy.tipoPoliza),
      txtEndDate: this.isEPS(policy)
        ? this.lang.Labels.PolizaVigente
        : this.isVitalicia(policy)
        ? this.lang.Labels.PolizaVitalicia
        : '',
      labelEndDate: this.isCurrentPolicy(policy) ? this.setLabelEndDate(semaforo) : PolicyLang.Texts.ExpiredPolicy,
      isCurrent: this.isCurrentPolicy(policy),
      isExpiredEps,
      insuranceCost: this.currencyPipe.transform(policy.prima, symbol),
      quotaNumber: policy.numeroCuota,
      quotas: policy.cuotas,
      amountFee: this.currencyPipe.transform(policy.montoCuota, symbol),
      showIconPdf: this._isVisiblePDF(policy),
      hiddenViewDetail: this._isHiddenViewDetail(policy),
      semaforo,
      bulletColor: this.colorStatus[semaforo],
      isVitalicia: this.isVitalicia(policy),
      contractorName: policy.nombreContratante,
      isVIP: policy.esPolizaVip,
      isContractor,
      isEPS,
      isSCTR,
      isSctrDoubleEmission,
      isEPSContractor,
      affiliates,
      constanciesLabel,
      isVisibleCoverage: this.isVisibleCoverage(policy),
      fractionationDesc: policy.descripcionFraccionamiento,
      coverageStatus: this.isCurrentPolicy(policy) ? `${policy.estadoCobertura}` : coverageStatus.sinVigencia,
      isPPFM,
      ...(customIcon && { customIcon }),
      response: policy
    };
  }

  setLabelEndDate(color: string): string {
    return color === this.trafficLightColor.ROJO || color === this.trafficLightColor.NINGUNO
      ? this.data.labelExpiredPolicy
      : color === this.trafficLightColor.PLOMO
      ? this.data.labelStartsPolicy
      : this.data.labelCurrentPolicy;
  }

  private _isHiddenViewDetail(policy): boolean {
    return this.fnHideViewDetail(policy);
  }

  private _isVisiblePDF(policy): boolean {
    if (!policy.puedeDescargarPoliza) {
      return false;
    }

    if (POLICY_TYPES.MD_SOAT_ELECTRO.code === policy.tipoPoliza && policy.estado !== this.expiredStatusSoat) {
      return true;
    }

    if (POLICY_TYPES.MD_SOAT.code !== policy.tipoPoliza && this._isHiddenViewDetail(policy)) {
      return true;
    }

    return false;
  }

  private _initValues(): void {
    this.frm = this._FrmProvider.CardMyPoliciesComponent();
    this.isVisibleFilterHeader || (this.titleMyPolicies = this.data.titleMyPolicies);
  }

  private _getObjWithData(obj): any {
    const ok = Object.keys(obj);

    return ok.reduce((acc, k) => (['', null].includes(obj[k]) ? acc : { ...acc, [k]: obj[k] }), {});
  }
}
