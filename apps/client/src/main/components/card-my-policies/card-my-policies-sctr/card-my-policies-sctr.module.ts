import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DropdownItemsPolicyModule } from '@mx/components/shared/dropdown/dropdown-items-policy/dropdown-items-policy.module';
import { IconPoliciesModule } from '@mx/components/shared/icon-policies/icon-polices.module';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';
import { MfButtonModule } from '@mx/core/ui/lib/components/forms/button';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { LinksModule } from '@mx/core/ui/lib/links/links.module';
import { CardMyPoliciesSctrComponent } from './card-my-policies-sctr.component';

@NgModule({
  imports: [
    CommonModule,
    MfCardModule,
    LinksModule,
    IconPoliciesModule,
    MfButtonModule,
    DirectivesModule,
    MfLoaderModule,
    DropdownItemsPolicyModule
  ],
  declarations: [CardMyPoliciesSctrComponent],
  exports: [CardMyPoliciesSctrComponent]
})
export class CardMyPoliciesSctrModule {}
