import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { COVERAGE_STATUS, TRAFFIC_LIGHT_COLOR } from '@mx/components/shared/utils/policy';
import { PolicyUtil } from '@mx/components/shared/utils/policy.util';
import { hasWarning } from '@mx/core/shared/helpers/util/date';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { AuthService } from '@mx/services/auth/auth.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { GeneralService } from '@mx/services/general/general.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import {
  APPLICATION_CODE,
  COLOR_STATUS,
  FORMAT_DATE_DDMMYYYY,
  SEARCH_ORDEN_DESC
} from '@mx/settings/constants/general-values';
import { ESTADO_POLIZA } from '@mx/settings/constants/key-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import {
  IItemPolicySctr,
  ISctrByClientRequest,
  ISctrByClientResponse
} from '@mx/statemanagement/models/policy.interface';
import { Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { BaseCardMyPolicies } from '../base-card-my-policies';

@Component({
  selector: 'client-card-my-policies-sctr',
  templateUrl: './card-my-policies-sctr.component.html',
  providers: [DatePipe]
})
export class CardMyPoliciesSctrComponent extends BaseCardMyPolicies implements OnInit {
  listPolicies: Array<IItemPolicySctr>;
  messageWithoutPolicies = PolicyLang.Messages.WithoutPolicies;
  disableCollapse = true;
  policyType = POLICY_TYPES.MD_SCTR.code;
  btnConfirm = GeneralLang.Buttons.Confirm;
  colorStatus = COLOR_STATUS;
  coverageStatus = COVERAGE_STATUS;
  showLoading: boolean;
  policiesSub: Subscription;

  constructor(
    protected generalService: GeneralService,
    protected policiesService: PoliciesService,
    protected policiesInfoService: PoliciesInfoService,
    protected configurationService: ConfigurationService,
    private readonly authService: AuthService,
    public router: Router,
    private readonly datePipe: DatePipe,
    protected gaService: GAService
  ) {
    super(generalService, policiesService, gaService);
    this.listPolicies = [];
    this.data.titleMyPolicies = PolicyLang.Titles.MyPolicies;
  }

  fnParamsSearchByClient(): ISctrByClientRequest {
    const params: ISctrByClientRequest = {
      codigoApp: APPLICATION_CODE
    };

    return params;
  }

  ngOnInit(): void {
    this.getParams();
    this.getPolicyList();
  }

  getPolicyList(): void {
    const params: ISctrByClientRequest = {
      codigoApp: APPLICATION_CODE,
      criterio: 'fechaInicio',
      estadoPoliza: ESTADO_POLIZA.vigente.code,
      orden: SEARCH_ORDEN_DESC
    };
    this.showLoading = true;
    this.policiesSub = this.policiesService
      .sctrSearchByClient(params)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(
        (response: Array<ISctrByClientResponse> = []) => {
          this.listPolicies = response.map(policy => {
            const semaforo = TRAFFIC_LIGHT_COLOR[policy.semaforo];
            const policyNumber = policy.numeroPoliza;
            const isSctrDoubleEmission = PolicyUtil.isSctrDoubleEmission(policyNumber);
            const policyNumberHeath = isSctrDoubleEmission ? policyNumber.split(',')[0].trim() : null;
            const policyNumberPension = isSctrDoubleEmission ? policyNumber.split(',')[1].trim() : null;
            const policyNumberLabel = `${this.data.labelNumberPolicy}: ${policyNumber}`;
            const policyNumberTitle = isSctrDoubleEmission
              ? `Salud: ${policyNumberHeath}\nPensión: ${policyNumberPension}`
              : policyNumberLabel;

            return {
              insured:
                (policy.asegurados && policy.asegurados.map(insured => ({ fullName: insured.nombreAsegurado }))) || [],
              moneyCode: `${StringUtil.getMoneyDescription(policy.codigoMoneda)} `,
              insuranceCost: policy.prima,
              policy: policy.descripcionPoliza,
              expired: this.fnIsExpiredPolicy(policy.fechaFin),
              warning: hasWarning(policy.fechaFin, this.renewalDays),
              endDate: this.datePipe.transform(policy.fechaFin, FORMAT_DATE_DDMMYYYY),
              startDate: this.datePipe.transform(policy.fechaInicio, FORMAT_DATE_DDMMYYYY),
              policyNumber,
              policyNumberHeath,
              policyNumberPension,
              policyNumberTitle,
              policyNumberLabel,
              policyType: policy.tipoPoliza,
              activityRisk: policy.actividadesRiesgo,
              constancyCount: policy.numeroConstancias,
              coverageStatus: policy.estadoCobertura || '',
              semaforo,
              bulletColor: COLOR_STATUS[semaforo],
              isContractor: policy.esContratante,
              isVisibleCoverage: this.isVisibleCoverage(policy),
              contractorName: policy.nombreContratante,
              isSctrDoubleEmission,
              response: policy
            } as IItemPolicySctr;
          });

          // Ocultar iconos si no tiene SCTR Salud
          const sctrHealth = response.filter(item =>
            PolicyUtil.isSctrHealth(item.tipoPoliza, item.codCia, item.codRamo)
          );

          this.configurationService.setHideServicesIcons(!sctrHealth.length);
          this.showLoading = false;
        },
        () => {
          this.showLoading = false;
          this.listPolicies = [];
          this.policiesSub.unsubscribe();
        },
        () => {
          this.policiesSub.unsubscribe();
        }
      );
  }

  goToDetail(policy: IItemPolicySctr): void {
    const type = this.policyType;
    const policyNumber = policy.isSctrDoubleEmission ? policy.policyNumberHeath : policy.policyNumber;
    const path = `${POLICY_TYPES[type].path}/${policyNumber}`;
    this.policiesInfoService.setClientSctrPolicyResponse(policy.response);
    this.router.navigate([path]);
  }

  constancy(value): string {
    return `${value} ${value === 1 ? PolicyLang.Labels.Constancy : PolicyLang.Labels.Constancies}`;
  }
}
