import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ComparePolicyModule } from '@mx/components/shared/compare-policy/compare-policy.module';
import { DropdownItemsPolicyModule } from '@mx/components/shared/dropdown/dropdown-items-policy/dropdown-items-policy.module';
import { IconPoliciesModule } from '@mx/components/shared/icon-policies/icon-polices.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader';
import { LinksModule } from '@mx/core/ui/lib/links/links.module';
import { CardMyPoliciesByTypeComponent } from './card-my-policies-by-type.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MfCardModule,
    LinksModule,
    IconPoliciesModule,
    DirectivesModule,
    DropdownItemsPolicyModule,
    MfLoaderModule,
    ComparePolicyModule,
    GoogleModule
  ],
  declarations: [CardMyPoliciesByTypeComponent],
  exports: [CardMyPoliciesByTypeComponent]
})
export class CardMyPoliciesByTypeModule {}
