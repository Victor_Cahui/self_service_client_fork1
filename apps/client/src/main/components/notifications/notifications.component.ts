import { Component } from '@angular/core';

@Component({
  selector: 'client-notifications-component',
  template: '<ng-content></ng-content>'
})
export class NotificationsComponent {}
