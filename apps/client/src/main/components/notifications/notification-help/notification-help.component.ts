import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { GeneralService } from '@mx/services/general/general.service';
import * as KEYS_VALUES from '@mx/settings/constants/key-values';
import { IParameterApp } from '@mx/statemanagement/models/general.models';
import { Observable } from 'rxjs';
import { TEXT_WHATSAPP, WHATSAPPPHONE, WHATSAPPWEB } from './../../../../settings/constants/general-values';

@Component({
  selector: 'client-notification-help',
  templateUrl: './notification-help.component.html'
})
export class NotificationHelpComponent implements OnInit {
  @HostBinding('attr.class') class = 'w-100';
  @Input() message: string;
  @Input() phone: string;
  @Input() configPhoneNumber: any;
  @Input() styleWhatsapp: false;
  @Input() textMsgWhatsapp = '';

  phoneNumber: string;
  phoneNumberWhatsapp: string;
  urlPhoneNumberWhatsapp: string;
  parametersSubject: Observable<Array<IParameterApp>>;
  module = 'CDM';

  constructor(private readonly _GeneralService: GeneralService) {}

  ngOnInit(): void {
    this.parametersSubject = this._GeneralService.getParametersSubject();
    this.parametersSubject.subscribe(params => {
      this.phoneNumber = this._GeneralService.getPhoneNumber(this.phone);
    });
    this.urlPhoneNumberWhatsapp = `${WHATSAPPWEB}?phone=${WHATSAPPPHONE}&text=${this.textMsgWhatsapp}`;
    this.phoneNumberWhatsapp = TEXT_WHATSAPP;
  }

  redirectToWhatsapp(): void {
    if (this.styleWhatsapp) {
      this._GeneralService.saveUserMovements(
        this.module,
        this._GeneralService.action(KEYS_VALUES.CDM_PROCEDURES.PROC_AMBULANCIA_CDM),
        this._GeneralService.action(KEYS_VALUES.CDM_DESCRIPTION.DSC_LLAMADA_WHATSAPP)
      );
      window.open(this.urlPhoneNumberWhatsapp, '_blank');
    }

    return void 0;
  }

  callPhone(): void {
    this._GeneralService.saveUserMovements(
      this.module,
      this._GeneralService.action(KEYS_VALUES.CDM_PROCEDURES.PROC_AMBULANCIA_CDM),
      this._GeneralService.action(KEYS_VALUES.CDM_DESCRIPTION.DSC_LLAMADA_TELEFONO)
    );
    window.location.href = `tel:${this.phoneNumber}`;
  }
}
