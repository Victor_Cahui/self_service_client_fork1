import { Component, Input, OnInit } from '@angular/core';
import { CIRCLE_CHECK_ICON } from '@mx/settings/constants/images-values';
import { IPolicyNotification } from '@mx/statemanagement/models/policy.interface';

@Component({
  selector: 'client-notifications-policy-component',
  templateUrl: './notifications-policy.component.html'
})
export class NotificationsPolicyComponent implements OnInit {
  @Input() policyNotification: IPolicyNotification;

  ngOnInit(): void {
    if (!this.policyNotification.icon) {
      this.policyNotification.icon = { iconName: CIRCLE_CHECK_ICON };
    }
  }
}
