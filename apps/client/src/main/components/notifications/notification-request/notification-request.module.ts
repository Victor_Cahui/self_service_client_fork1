import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfCardModule } from '@mx/core/ui';
import { NotificationRequestComponent } from './notification-request.component';

@NgModule({
  imports: [CommonModule, MfCardModule],
  exports: [NotificationRequestComponent],
  declarations: [NotificationRequestComponent]
})
export class NotificationRequestModule {}
