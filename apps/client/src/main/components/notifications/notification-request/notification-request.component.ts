import { Component, HostBinding, Input } from '@angular/core';
import { WARNING_2_ICON } from '@mx/settings/constants/images-values';

@Component({
  selector: 'client-notification-request',
  templateUrl: './notification-request.component.html'
})
export class NotificationRequestComponent {
  @HostBinding('attr.class') class = 'w-100';
  @Input() title: string;
  @Input() text: string;
  @Input() icon = WARNING_2_ICON;
}
