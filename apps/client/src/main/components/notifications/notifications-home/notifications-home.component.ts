import { Component, OnInit } from '@angular/core';
import { NumberComponent } from '@mx/components/shared/utils/number';
import { AuthService } from '@mx/services/auth/auth.service';
import { NotificactionsService } from '@mx/services/notifications/notifications.service';
import { APPLICATION_CODE, NOTIFICATION_SERVICE } from '@mx/settings/constants/general-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { INotificationHome, INotificationRequest } from '@mx/statemanagement/models/notification.interface';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-notifications-home-component',
  templateUrl: './notifications-home.component.html'
})
export class NotificationsHomeComponent implements OnInit {
  labelViewDetail = GeneralLang.Buttons.ViewDetail;
  messageScheduledServices = GeneralLang.Messages.ScheduledServices;
  messageOneScheduledServices = GeneralLang.Messages.OneScheduledService;

  userDocument: IdDocument;
  notificationRequest: INotificationRequest;
  notificationLength: string;
  notificationList: Array<INotificationHome>;

  constructor(
    private readonly notificationsService: NotificactionsService,
    private readonly authService: AuthService
  ) {}

  ngOnInit(): void {
    this.userDocument = this.authService.retrieveEntity();
    // TODO: (temporal) descomentar la llamada al servicio de notificaciones
    // this.getNotifications();
  }

  getNotifications(): void {
    const noti: Subscription = this.notificationsService
      .getNotificationsforHome({
        codigoApp: APPLICATION_CODE,
        tipoNotificacion: NOTIFICATION_SERVICE
      } as INotificationRequest)
      .subscribe(res => {
        this.notificationList = res;

        if (this.notificationList.length > 1) {
          this.messageScheduledServices = this.messageScheduledServices.replace(
            '%',
            NumberComponent.numberToLetter(this.notificationList.length).toLowerCase()
          );
        }

        noti.unsubscribe();
      });
  }

  notificationsTrackByFn(index: number, notification: INotificationHome): number {
    return notification.codigo;
  }
}
