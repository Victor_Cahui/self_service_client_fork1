import { OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Seguridad } from '@mx/core/shared/providers/services';
import { UserAdminService } from '@mx/services/auth/user-admin.service';
import { GeneralService } from '@mx/services/general/general.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { PHONE_CONTACT_PROV } from '@mx/settings/constants/key-values';
import { AuthLang } from '@mx/settings/lang/auth.lang';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { AdminUserResponse } from '@mx/statemanagement/models/auth.interface';
import { GeneralRequestEn } from '@mx/statemanagement/models/general.models';
import { of, Subscription } from 'rxjs';
import { finalize, map, switchMap } from 'rxjs/operators';
import { getStatusEmailSended, getUserType, mapResponse } from './base-user-admin.utils';

export abstract class BaseUserAdmin implements OnDestroy {
  form: FormGroup;
  messageError: string;
  messageEmailInvalid: string;
  messageNotFound = AuthLang.Messages.DataInvalid;
  showLoading: boolean;
  showMessageError: boolean;
  btnNext = GeneralLang.Buttons.Next;
  btnBack = GeneralLang.Buttons.Back;
  userSub: Subscription;
  parametersSub: Subscription;
  recoverPasswordSub: Subscription;

  constructor(
    protected adminService: UserAdminService,
    protected generalService: GeneralService,
    protected fb: FormBuilder,
    protected router: Router,
    protected seguridadService: Seguridad
  ) {}

  ngOnDestroy(): void {
    if (this.userSub) {
      this.userSub.unsubscribe();
    }
    if (this.parametersSub) {
      this.parametersSub.unsubscribe();
    }
    if (this.recoverPasswordSub) {
      this.recoverPasswordSub.unsubscribe();
    }
  }

  // Inicializar forma, resto de campos en componente input-identity
  initForm(): void {
    this.getErrorMessage();
    this.form = this.fb.group({});
  }

  // Parametros para servicio
  getParams(): GeneralRequestEn {
    const identity = this.form.getRawValue().identity;

    return {
      applicationCode: APPLICATION_CODE,
      documentType: identity.docType,
      documentNumber: identity.docNumber
    };
  }

  // Parametros Generales
  getErrorMessage(): void {
    this.parametersSub = this.generalService.getParametersSubject().subscribe(() => {
      const whatsappNumber = '999 919 133';
      const lima = this.generalService.getPhoneNumber();
      const provinces = this.generalService.getValueParams(PHONE_CONTACT_PROV);
      this.messageEmailInvalid = AuthLang.Messages.EmailInvalid.replace('{{lima}}', lima)
        .replace('{{provinces}}', provinces)
        .replace('{{whatsapp}}', whatsappNumber);
    });
  }

  // Inicializar valores si cambia datos
  changeData(changed: boolean): void {}

  // Volver a login
  goBack(): void {
    this.router.navigate(['/login']);
  }

  // Acciones según respuesta del servicio
  verifyResponse(res: AdminUserResponse): void {
    this.recoverPasswordSub = of(res)
      .pipe(
        map(mapResponse),
        switchMap((r: any) =>
          getUserType(r).isRegistered
            ? this.seguridadService.RecuperarContraseniaMail(this.getParams()).pipe(map(mapResponse))
            : of(r)
        ),
        finalize(() => {
          this.showLoading = false;
        })
      )
      .subscribe(r => {
        getStatusEmailSended(r.operationCode).success
          ? this.goToSendSuccessEmail(r.email)
          : this.showErrorMessage(r.operationCode);
      });
  }

  goToSendSuccessEmail(email: string): void {
    this.adminService.setEmailConfirm(email);
    this.router.navigate(['/send-success']);
  }

  showErrorMessage(code: number): void {
    this.showMessageError = true;
    this.messageError = getStatusEmailSended(code).failure ? this.messageEmailInvalid : this.messageNotFound;
  }
}
