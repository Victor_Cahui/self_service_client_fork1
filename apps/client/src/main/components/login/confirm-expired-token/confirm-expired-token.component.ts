import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginWelcomeComponent } from '@mx/layouts/login-layout/login-top/login-welcome/login-welcome.component';
import { UserAdminService } from '@mx/services/auth/user-admin.service';
import { GeneralService } from '@mx/services/general/general.service';
import { KEY_ACTIONS } from '@mx/settings/auth/auth-values';
import { MAPFRE_ICON, TIME_ALERT_ICON, WARNING_ICON } from '@mx/settings/constants/images-values';
import { PHONE_CONTACT_PROV } from '@mx/settings/constants/key-values';
import { AuthLang } from '@mx/settings/lang/auth.lang';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-confirm-expired-token',
  templateUrl: './confirm-expired-token.component.html'
})
export class ConfirmExpiredTokenComponent implements OnInit, OnDestroy {
  user: string;
  lang = AuthLang;
  parametersSubject: Subscription;
  failIcon: string;
  mapfreIcon = MAPFRE_ICON;
  helpContact: string;
  message: string;
  tryAgain: boolean;
  buttonUpdate: string;
  enableUser: boolean;

  constructor(
    private readonly router: Router,
    private readonly generalService: GeneralService,
    private readonly adminService: UserAdminService
  ) {
    this.getParameters();
  }

  ngOnInit(): void {
    LoginWelcomeComponent.updateView.next(false);
    const data = this.adminService.getChangePasswordConfirm();
    this.tryAgain = data && data.tryAgain;
    this.enableUser = data && data.action === KEY_ACTIONS.ENABLE;
    this.failIcon = this.tryAgain ? WARNING_ICON : TIME_ALERT_ICON;
    this.buttonUpdate = this.enableUser ? this.lang.Buttons.EnableUser : this.lang.Buttons.UpdatePassword;
    this.message = data && data.tryAgain ? this.lang.Messages.TryAgain : this.lang.Messages.ExpiredTokenRecover;
  }

  ngOnDestroy(): void {
    if (this.parametersSubject) {
      this.parametersSubject.unsubscribe();
    }
  }

  // Telefonos de contacto
  getParameters(): void {
    this.parametersSubject = this.generalService.getParametersSubject().subscribe(() => {
      const lima = this.generalService.getPhoneNumber();
      const provinces = this.generalService.getValueParams(PHONE_CONTACT_PROV);
      this.helpContact = this.lang.Messages.HelpContact.replace('{{lima}}', lima).replace('{{provinces}}', provinces);
    });
  }

  goToUpdate(): void {
    this.adminService.clearChangePasswordConfirm();
    this.adminService.clearEmailConfirm();
    if (this.enableUser) {
      this.router.navigate(['/enable-user']);
    } else {
      this.router.navigate(['/recover-user']);
    }
  }
}
