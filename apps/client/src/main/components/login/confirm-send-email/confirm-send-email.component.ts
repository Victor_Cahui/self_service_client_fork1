import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GAService, GaUnsubscribeBase, IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { UserAdminService } from '@mx/services/auth/user-admin.service';
import { GeneralService } from '@mx/services/general/general.service';
import { MFP_Recuperar_Contrasenha_3B } from '@mx/settings/constants/events.analytics';
import { COMPLETE_ICON } from '@mx/settings/constants/images-values';
import { PHONE_CONTACT_PROV } from '@mx/settings/constants/key-values';
import { AuthLang } from '@mx/settings/lang/auth.lang';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-confirm-send-email',
  templateUrl: './confirm-send-email.component.html'
})
export class ConfirmSendEmailComponent extends GaUnsubscribeBase implements OnInit, OnDestroy {
  email: string;
  userId: string;
  lang = AuthLang;
  success = GeneralLang.Messages.Done;
  contactEmailInvalid: string;
  parametersSubject: Subscription;
  completeIcon = COMPLETE_ICON;

  ga: Array<IGaPropertie>;

  constructor(
    private readonly router: Router,
    private readonly adminService: UserAdminService,
    private readonly generalService: GeneralService,
    protected gaService: GAService
  ) {
    super(gaService);
    this.email = this.adminService.getEmailConfirm();
    this.ga = [MFP_Recuperar_Contrasenha_3B()];
  }

  ngOnInit(): void {
    this.getParameters();
  }

  ngOnDestroy(): void {
    if (this.parametersSubject) {
      this.parametersSubject.unsubscribe();
    }
  }

  // Parametros Generales
  getParameters(): void {
    this.parametersSubject = this.generalService.getParametersSubject().subscribe(() => {
      const lima = this.generalService.getPhoneNumber();
      const provinces = this.generalService.getValueParams(PHONE_CONTACT_PROV);
      this.contactEmailInvalid = this.lang.Messages.ContactEmailInvalid.replace('{{lima}}', lima).replace(
        '{{provinces}}',
        provinces
      );
    });
  }

  goToLogin(): void {
    this.router.navigate(['/login']);
  }
}
