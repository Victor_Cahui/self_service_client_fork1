import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { localStore } from '@mx/core/shared/helpers';
import { NotificationService } from '@mx/core/shared/helpers/notification/service/notification.service';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { Autoservicios, Comun } from '@mx/core/shared/providers/services';
import { SelectListItem } from '@mx/core/ui';
import { environment } from '@mx/environments/environment';
import { AuthService } from '@mx/services/auth/auth.service';
import { UserAdminService } from '@mx/services/auth/user-admin.service';
import { ClientService } from '@mx/services/client.service';
import { GeneralService } from '@mx/services/general/general.service';
import { PaymentService } from '@mx/services/payment.service';
import { PoliciesService } from '@mx/services/policies.service';
import { GROUP_TYPE, GROUP_TYPES, KEY_PERMISSIONS, KEY_USER_TOKEN } from '@mx/settings/auth/auth-values';
import { MFP_Iniciar_Sesion_2A } from '@mx/settings/constants/events.analytics';
import {
  APPLICATION_CODE,
  GENERAL_MAX_LENGTH,
  GENERAL_MIN_LENGTH,
  SEARCH_ORDEN_DESC
} from '@mx/settings/constants/general-values';
import { ESTADO_POLIZA, LOCAL_STORAGE_KEYS } from '@mx/settings/constants/key-values';
import { AuthLang } from '@mx/settings/lang/auth.lang';
import { ProfileLang } from '@mx/settings/lang/profile.lang';
import { AuthInterface, ILogguedUser, IUserGroupType } from '@mx/statemanagement/models/auth.interface';
import { IClientInformationResponse, IVerificacionDocumentoResponse } from '@mx/statemanagement/models/client.models';
import { IParameterApp } from '@mx/statemanagement/models/general.models';
import { IPaymentTypeResponse } from '@mx/statemanagement/models/payment.models';
import { IPoliciesByClientRequest } from '@mx/statemanagement/models/policy.interface';
import { IdDocumentRequest, TypeDocument } from '@mx/statemanagement/models/typedocument.interface';
import { Subscription, zip } from 'rxjs';

export abstract class BaseLogin extends GaUnsubscribeBase {
  formLogin: FormGroup;
  numberDoc: AbstractControl;
  password: AbstractControl;

  maxLengthNumberDoc: string;
  minLengthNumberDoc: string;
  showLoading: boolean;
  userInvalid: boolean;
  typesDocument: Array<SelectListItem>;
  typesDocumentValidators: Array<TypeDocument>;
  paymentsTypes: Array<IPaymentTypeResponse>;

  welcomeMessage = AuthLang.Messages.Welcome;
  chooseAcountType = AuthLang.Messages.ChooseAcountType;
  messageUserInvalid = AuthLang.Messages.UserInvalid;
  labelDocumentType = ProfileLang.Labels.DocumentType;
  labelDocumentNumber = ProfileLang.Labels.DocumentNumber;
  labelPassword = AuthLang.Labels.Password;
  linkForgotPassword = AuthLang.Buttons.ForgotPassword;
  btnLogin = AuthLang.Buttons.Login;
  btnEnableUser = AuthLang.Buttons.EnableUser;
  btnAnotherProfile = AuthLang.Buttons.AnotherProfile;
  passwordMaxLength = GENERAL_MAX_LENGTH.password;
  updatePasswordMinLength = GENERAL_MIN_LENGTH.updatePassword;
  passwordMinLength = GENERAL_MIN_LENGTH.password;

  groupTypes = GROUP_TYPE;

  clientID: IdDocumentRequest;
  userData: IClientInformationResponse;
  documentVerification: IVerificacionDocumentoResponse;
  paramsData: Array<IParameterApp>;
  subLoginOne: Subscription;
  subLoginTwo: Subscription;
  subTypesDocument: Subscription;
  paymentTypeSub: Subscription;
  policesByClientSub: Subscription;
  zipSub: Subscription;

  showForgotPasswordLink = environment.ACTIVATE_FORGOT_PASSWORD_LINK;
  showEnableUserLink = environment.ACTIVATE_ENABLE_USER_LINK;

  constructor(
    protected router: Router,
    protected policiesService: PoliciesService,
    protected clientService: ClientService,
    protected generalService: GeneralService,
    protected authService: AuthService,
    protected paymentService: PaymentService,
    protected fb: FormBuilder,
    protected localStorage: LocalStorageService,
    protected adminService: UserAdminService,
    protected _Comun: Comun,
    protected _Autoservicios: Autoservicios,
    protected notifyService: NotificationService,
    protected gaService?: GAService
  ) {
    super(gaService);
  }

  // verificación de documento y Login
  userValidate(data: AuthInterface, cbSuccess?: VoidFunction, cbError?: VoidFunction): void {
    this.showLoading = true;
    this.clientService.setUserData(null);
    const params = {
      codigoApp: APPLICATION_CODE,
      documento: data.username
    };
    this._Autoservicios.ObtenerEstadoMantenimiento(params).subscribe(
      response => {
        if (response && response.mantenimiento) {
          this.showLoading = false;
          const error = {
            status: '503'
          };
          this.notifyService.addErrorWithData('', error);
        } else {
          this.loginApplication(data, cbSuccess, cbError);
        }
      },
      error => {
        console.error(error);
        this.showLoading = false;
      }
    );
  }

  private loginApplication(data: AuthInterface, cbSuccess?: VoidFunction, cbError?: VoidFunction): void {
    this.authService.login({ ...data, osa: 1 }).subscribe(
      authResponse => {
        this.authService.getUsers(authResponse.access_token).subscribe(
          usersGroupTypeResponse => {
            const userGroupTypes = usersGroupTypeResponse.data.filter(user => GROUP_TYPES.includes(user.groupType));
            if (userGroupTypes.length) {
              if (userGroupTypes.length === 1) {
                // aqui se realiza el logueo automatico
                this.localStorage.setData('url_list', []);
                this.loginGroupType(authResponse.access_token, userGroupTypes[0], cbSuccess);
              } else {
                // ir a pagina de selección de tipo de persona
                this.localStorage.setData(LOCAL_STORAGE_KEYS.TOKEN_TEMP, authResponse.access_token);
                this.localStorage.setData(LOCAL_STORAGE_KEYS.GROUP_TYPE, userGroupTypes);
                this.localStorage.removeItem(LOCAL_STORAGE_KEYS.USER);
                this.router.navigate(['/login-type']);
              }
            } else {
              this.userInvalid = true;
              this.showLoading = false;
              this._addGAEvent('Fallido');
            }
          },
          error => {
            cbError && cbError();
            this.showLoading = false;
            this.userInvalid = true;
            if (error.errors.error.error === 'blocked_user') {
              this.messageUserInvalid = error.errors.error.error_description;
            }
            this._addGAEvent('Fallido');
          }
        );
      },
      error => {
        this.showLoading = false;
        this.userInvalid = true;
        if (error.errors.error.error === 'blocked_user') {
          this.messageUserInvalid = error.errors.error.error_description;
        }
        this._addGAEvent('Fallido');
      }
    );
  }

  getUsersGroupType(): Array<IUserGroupType> {
    return this.localStorage.getData(LOCAL_STORAGE_KEYS.GROUP_TYPE);
  }

  getTokenTemp(): string {
    return this.localStorage.getData(LOCAL_STORAGE_KEYS.TOKEN_TEMP);
  }

  removeDataLogin(): void {
    this.localStorage.removeItem(LOCAL_STORAGE_KEYS.TOKEN_TEMP);
    this.localStorage.removeItem(LOCAL_STORAGE_KEYS.GROUP_TYPE);
  }

  loginGroupType(access_token: string, userGroupType: IUserGroupType, cbSuccess?: VoidFunction): void {
    this.showLoading = true;
    this.authService.accessByGroupType(access_token, userGroupType.groupType).subscribe(
      authResponse => {
        this.authService.setExpirationTimeToken(authResponse.expires_in);
        this.authService.claims(authResponse.access_token).subscribe(claims => {
          // almacenamiento de datos
          this.clientID = {
            documento: claims.documentNumber,
            tipoDocumento: claims.documentType,
            groupTypeId: userGroupType.groupType.toString(),
            userType: userGroupType.userType.toString()
          };

          this.authService.loginComplete(
            authResponse.access_token,
            claims.documentType,
            claims.documentNumber,
            authResponse.refresh_token || '',
            userGroupType
          );
          this.settingSession(cbSuccess);
        });
      },
      error => {
        this.showLoading = false;
        this.userInvalid = true;
        this._addGAEvent('Fallido');
      }
    );
  }

  userValidateFromOIM(data, cbSuccess?: VoidFunction, cbError?: VoidFunction): void {
    this.authService.getUsers(data.token).subscribe(
      usersGroupTypeResponse => {
        const userGroupTypes = usersGroupTypeResponse.data.filter(user => GROUP_TYPES.includes(user.groupType));
        if (userGroupTypes.length) {
          if (userGroupTypes.length === 1) {
            // aqui se realiza el logueo automatico
            this.loginGroupType(data.token, userGroupTypes[0], cbSuccess);
          } else {
            // ir a pagina de selección de tipo de persona
            this.localStorage.setData(LOCAL_STORAGE_KEYS.TOKEN_TEMP, data.token);
            this.localStorage.setData(LOCAL_STORAGE_KEYS.GROUP_TYPE, userGroupTypes);
            this.localStorage.removeItem(LOCAL_STORAGE_KEYS.USER);
            this.router.navigate(['/login-type']);
          }
        } else {
          this.userInvalid = true;
          this.showLoading = false;
          this._addGAEvent('Fallido');
        }
      },
      error => {
        cbError && cbError();
        this.showLoading = false;
        this.userInvalid = true;
        if (error.errors.error.error === 'blocked_user') {
          this.messageUserInvalid = error.errors.error.error_description;
        }
        this._addGAEvent('Fallido');
      }
    );
  }

  // Carga datos generales y va a Home
  settingSession(cbSuccess?: VoidFunction): void {
    this._addGAEvent('Exitoso');
    // Tipo de documentos
    this.generalService.setDocumentTypes(this.typesDocumentValidators);

    const params: IPoliciesByClientRequest = {
      codigoApp: APPLICATION_CODE,
      criterio: 'fechaInicio',
      orden: SEARCH_ORDEN_DESC,
      estadoPoliza: ESTADO_POLIZA.vigente.code,
      activarSoatVencidos: true
    };

    const app = { codigoApp: APPLICATION_CODE };
    const parameters$ = this.generalService.getParameters();
    const user$ = this.clientService.getClientInformation({});
    const policies$ = this.policiesService.searchByClientService(params);
    const paymentTypes$ = this.paymentService.paymentType(app);
    const permissions$ = this._Comun.ObtenerFunciones({
      codigoApp: APPLICATION_CODE
    });

    this.zipSub = zip(parameters$, user$, policies$, paymentTypes$, permissions$).subscribe(
      res => {
        // Parametros Generales
        if (res[0]) {
          this.paramsData = res[0];
        }
        // Usuario
        if (res[1]) {
          this.userData = res[1];
          this.userData.tipoDocumento = this.clientID.tipoDocumento;
          this.userData.documento = this.clientID.documento;
        }
        // Polizas del cliente
        if (res[2]) {
          this.policiesService.setSearchByClientLocal(res[2]);
        }
        // Tipo de pago
        if (res[3]) {
          this.paymentsTypes = res[3];
        }
        const permissions = res[4];
        if (permissions) {
          localStore().set(KEY_PERMISSIONS, permissions);
          this.decorateEnvironment(permissions);
        }
      },
      err => {
        this.localStorage.removeItem(KEY_USER_TOKEN);
        this.showLoading = false;
      },
      () => {
        this.zipSub.unsubscribe();
        this.generalService.setParameters(this.paramsData);
        this.paymentService.setPaymentTypes(this.paymentsTypes);
        cbSuccess && cbSuccess();
        if (this.userData) {
          this.clientService.setUserData(this.userData);
          this.setDataLocal();
        }
        this.router.navigate(['/home']);
        this.showLoading = false;
      }
    );
  }

  // Tipos de Documento
  getDocumentTypes(): void {
    this.subTypesDocument = this.generalService.getDocumentTypes().subscribe(
      (types: Array<TypeDocument>) => {
        this.typesDocument = types.map(type => new SelectListItem({ text: type.codigo, value: type.codigo }));
        this.typesDocumentValidators = types;
        this.initForm();
      },
      () => {},
      () => {
        this.subTypesDocument.unsubscribe();
      }
    );
  }

  initForm(): void {}

  // Datos de usuario en local
  setDataLocal(): void {
    const name =
      (this.userData.nombreCliente && this.userData.nombreCliente.split(' ')[0]) || this.userData.razonSocial;
    const data: ILogguedUser = {
      documentType: this.clientID.tipoDocumento,
      documentNumber: this.clientID.documento,
      groupTypeId: this.clientID.groupTypeId,
      userType: this.clientID.userType,
      name: name || ''
    };
    this.localStorage.setData(LOCAL_STORAGE_KEYS.USER, data);
  }

  getDataLocal(): ILogguedUser {
    return this.localStorage.getData(LOCAL_STORAGE_KEYS.USER);
  }

  // Habilitar usuario
  enableUser(): void {
    this.adminService.clearEmailConfirm();
    this.router.navigate(['/enable-user']);
  }

  // Recuperar contraseña
  recoverUser(): void {
    this.adminService.clearEmailConfirm();
    this.router.navigate(['/recover-user']);
  }

  decorateEnvironment(arrPermissions: any[] = []): void {
    arrPermissions.forEach(p => {
      environment[p.codigo] = p.valor;
    });
  }

  private _addGAEvent(state: string): void {
    this.addGAEvent(MFP_Iniciar_Sesion_2A(), { state });
  }
}
