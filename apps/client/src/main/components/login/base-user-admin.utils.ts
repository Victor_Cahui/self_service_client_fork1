import {
  ERROR_CODE,
  ERROR_CODE_EMAIL_NOT_REGISTER,
  INTERNAL_ERROR,
  SUCCESS_CODE
} from '@mx/settings/constants/general-values';

export function getUserType(res: any): any {
  return {
    isNew: res.operationCode === SUCCESS_CODE,
    isRegistered: res.operationCode === ERROR_CODE,
    notExist: res.operationCode === INTERNAL_ERROR
  };
}

export function getStatusEmailSended(code: any): any {
  return {
    success: code === SUCCESS_CODE,
    failure: code === ERROR_CODE || code === ERROR_CODE_EMAIL_NOT_REGISTER
  };
}

export function mapResponse(res: any): any {
  return {
    operationCode: res.operationCode,
    email: res.data.txt_Email
  };
}
