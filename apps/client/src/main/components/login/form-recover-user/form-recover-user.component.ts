import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { Seguridad } from '@mx/core/shared/providers/services';
import { NotificationStatus } from '@mx/core/ui/lib/components/alerts/custom-alert/custom-alert.component';
import { LoginFormComponent } from '@mx/layouts/login-layout/login-top/login-form/login-form.component';
import { LoginWelcomeComponent } from '@mx/layouts/login-layout/login-top/login-welcome/login-welcome.component';
import { UserAdminService } from '@mx/services/auth/user-admin.service';
import { GeneralService } from '@mx/services/general/general.service';
import { MFP_Recuperar_Contrasenha_3A } from '@mx/settings/constants/events.analytics';
import { BaseUserAdmin } from '../base-user-admin';

@Component({
  selector: 'client-form-recover-user',
  templateUrl: './form-recover-user.component.html'
})
export class FormRecoverUserComponent extends BaseUserAdmin implements OnInit {
  gaNext: IGaPropertie;
  gaBack: IGaPropertie;
  status = NotificationStatus.WARNING;
  constructor(
    protected adminService: UserAdminService,
    protected generalService: GeneralService,
    protected fb: FormBuilder,
    protected router: Router,
    protected seguridadService: Seguridad
  ) {
    super(adminService, generalService, fb, router, seguridadService);
    this.gaNext = MFP_Recuperar_Contrasenha_3A(this.btnNext);
    this.gaBack = MFP_Recuperar_Contrasenha_3A(this.btnBack);
  }

  ngOnInit(): void {
    LoginFormComponent.updateView.next(true);
    LoginWelcomeComponent.updateView.next(true);
    this.initForm();
  }

  // Recuperar Password
  recoverUser(): void {
    if (!this.form.valid) {
      return;
    }
    this.showLoading = true;
    this.userSub = this.adminService.enableUser(this.getParams()).subscribe(
      res => {
        this.verifyResponse(res);
      },
      () => {
        this.showLoading = false;
      },
      () => {
        this.userSub.unsubscribe();
      }
    );
  }
}
