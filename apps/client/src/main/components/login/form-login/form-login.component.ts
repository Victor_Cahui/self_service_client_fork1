import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { FormComponent } from '@mx/components/shared/utils/form';
import { NotificationService } from '@mx/core/shared/helpers/notification/service/notification.service';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { Autoservicios, Comun } from '@mx/core/shared/providers/services';
import { NotificationStatus } from '@mx/core/ui/lib/components/alerts/custom-alert/custom-alert.component';
import { LoginFormComponent } from '@mx/layouts/login-layout/login-top/login-form/login-form.component';
import { AuthService } from '@mx/services/auth/auth.service';
import { UserAdminService } from '@mx/services/auth/user-admin.service';
import { ClientService } from '@mx/services/client.service';
import { GeneralService } from '@mx/services/general/general.service';
import { PaymentService } from '@mx/services/payment.service';
import { PoliciesService } from '@mx/services/policies.service';
import { MFP_Iniciar_Sesion_2B, MFP_Iniciar_Sesion_2C } from '@mx/settings/constants/events.analytics';
import { GENERAL_MAX_LENGTH, GENERAL_MIN_LENGTH } from '@mx/settings/constants/general-values';
import { MEDIKTOR_URLS_REDIRECT } from '@mx/settings/constants/mediktor-values';
import { AuthInterface } from '@mx/statemanagement/models/auth.interface';
import { AllValidationErrors } from '@mx/statemanagement/models/form.interface';
import { isEmpty } from 'lodash-es';
import { BaseLogin } from '../base-login';

@Component({
  selector: 'client-form-login',
  templateUrl: './form-login.component.html'
})
export class FormLoginComponent extends BaseLogin implements OnInit {
  urlMediktor: SafeResourceUrl;
  urlHostMediktor = MEDIKTOR_URLS_REDIRECT.MediktorDiagnostic;
  status = NotificationStatus.WARNING;
  constructor(
    protected router: Router,
    protected policiesService: PoliciesService,
    protected clientService: ClientService,
    protected generalService: GeneralService,
    protected authService: AuthService,
    protected paymentService: PaymentService,
    protected fb: FormBuilder,
    protected localStorage: LocalStorageService,
    protected adminService: UserAdminService,
    protected _Comun: Comun,
    protected _Autoservicios: Autoservicios,
    protected notifyService: NotificationService,
    protected gaService?: GAService,
    protected sanitizer?: DomSanitizer
  ) {
    super(
      router,
      policiesService,
      clientService,
      generalService,
      authService,
      paymentService,
      fb,
      localStorage,
      adminService,
      _Comun,
      _Autoservicios,
      notifyService,
      gaService
    );
    this.maxLengthNumberDoc = `${GENERAL_MAX_LENGTH.numberDocumentGeneric}`;
    this.minLengthNumberDoc = `${GENERAL_MIN_LENGTH.numberDocumentGeneric}`;
    this.ga = [MFP_Iniciar_Sesion_2B(), MFP_Iniciar_Sesion_2C()];
    this.urlMediktor = this.sanitizer.bypassSecurityTrustResourceUrl(this.urlHostMediktor);
  }

  ngOnInit(): void {
    LoginFormComponent.updateView.next(true);
    this.authService.logout();
    this.getDocumentTypes();
    this.initForm();
  }

  // Formulario
  initForm(): void {
    this.createForm();
  }

  createForm(): void {
    this.formLogin = this.fb.group({
      numberDoc: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.numberDoc = this.formLogin.controls['numberDoc'];
    this.password = this.formLogin.controls['password'];
  }

  resetForm(): void {
    FormComponent.setControl(this.numberDoc, '');
    FormComponent.setControl(this.password, '');
  }

  // Login
  login(): void {
    this.clientService.canShowModalElectronicPolicy = true;
    const error: AllValidationErrors = FormComponent.getFormValidationErrors(this.formLogin.controls).shift();
    if (isEmpty(error)) {
      const data: AuthInterface = {
        client_id: '',
        grant_type: '',
        username: this.numberDoc.value,
        password: this.password.value
      };
      this.clientID = {
        tipoDocumento: '',
        documento: this.numberDoc.value
      };
      // Validar Usuario y hacer Login
      this.userValidate(data);
    } else {
      FormComponent.validateAllFormFields(this.formLogin);
    }
  }

  onPasteNumberDoc(event: any): void {
    event.stopPropagation();
    event.preventDefault();
    this.numberDoc.setValue(
      StringUtil.getClipboardText(event)
        .trim()
        .replace(/\s/g, '')
    );
  }

  removeSpaces(controlName: string): void {
    FormComponent.removeAllSpaces(this.formLogin.controls[controlName]);
  }
}
