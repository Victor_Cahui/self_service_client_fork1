import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginWelcomeComponent } from '@mx/layouts/login-layout/login-top/login-welcome/login-welcome.component';
import { UserAdminService } from '@mx/services/auth/user-admin.service';
import { COMPLETE_ICON } from '@mx/settings/constants/images-values';
import { AuthLang } from '@mx/settings/lang/auth.lang';
import { GeneralLang } from '@mx/settings/lang/general.lang';

@Component({
  selector: 'client-confirm-restore-password',
  templateUrl: './confirm-restore-password.component.html'
})
export class ConfirmRestorePasswordComponent implements OnInit {
  user: string;
  lang = AuthLang;
  success = GeneralLang.Messages.Done;
  contactEmailInvalid: string;
  completeIcon = COMPLETE_ICON;

  constructor(private readonly router: Router, private readonly adminService: UserAdminService) {}

  ngOnInit(): void {
    LoginWelcomeComponent.updateView.next(true);
  }

  goToLogin(): void {
    this.adminService.clearChangePasswordConfirm();
    this.router.navigate(['/login']);
  }
}
