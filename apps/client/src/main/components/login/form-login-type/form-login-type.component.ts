import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { NotificationService } from '@mx/core/shared/helpers/notification/service/notification.service';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { Autoservicios, Comun } from '@mx/core/shared/providers/services';
import { LoginFormComponent } from '@mx/layouts/login-layout/login-top/login-form/login-form.component';
import { AuthService } from '@mx/services/auth/auth.service';
import { UserAdminService } from '@mx/services/auth/user-admin.service';
import { ClientService } from '@mx/services/client.service';
import { GeneralService } from '@mx/services/general/general.service';
import { PaymentService } from '@mx/services/payment.service';
import { PoliciesService } from '@mx/services/policies.service';
import { GROUP_TYPE_ID_PERSON, GROUP_TYPES } from '@mx/settings/auth/auth-values';
import { IUserGroupType } from '@mx/statemanagement/models/auth.interface';
import { BaseLogin } from '../base-login';

@Component({
  selector: 'client-form-login-type',
  templateUrl: './form-login-type.component.html'
})
export class FormLoginTypeComponent extends BaseLogin implements OnInit {
  constructor(
    protected router: Router,
    protected policiesService: PoliciesService,
    protected clientService: ClientService,
    protected generalService: GeneralService,
    protected authService: AuthService,
    protected paymentService: PaymentService,
    protected fb: FormBuilder,
    protected localStorage: LocalStorageService,
    protected adminService: UserAdminService,
    protected _Comun: Comun,
    protected _Autoservicios: Autoservicios,
    protected notifyService: NotificationService,
    protected _GAService?: GAService
  ) {
    super(
      router,
      policiesService,
      clientService,
      generalService,
      authService,
      paymentService,
      fb,
      localStorage,
      adminService,
      _Comun,
      _Autoservicios,
      notifyService,
      _GAService
    );
  }

  ngOnInit(): void {
    LoginFormComponent.updateView.next(true);
  }

  getGroupType(): Array<IUserGroupType> {
    const usersGroupType = this.getUsersGroupType();

    return GROUP_TYPES.map(gt => usersGroupType.find(ugt => ugt.groupType === gt));
  }

  getIcon(groupType: number): string {
    return groupType === GROUP_TYPE_ID_PERSON ? 'icon-mapfre_014_perfil' : 'icon-mapfre_018_building';
  }

  accessGroupType(userGroupType: IUserGroupType): void {
    const token = this.getTokenTemp();

    this.loginGroupType(token, userGroupType);
  }

  returnLogin(): void {
    this.removeDataLogin();
    this.router.navigate(['/login']);
  }
}
