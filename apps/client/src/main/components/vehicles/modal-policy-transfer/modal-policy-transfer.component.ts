import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { FormComponent } from '@mx/components/shared/utils/form';
import { EmailValidator, MfModal, SelectListItem } from '@mx/core/ui';
import { MfModalMessage } from '@mx/core/ui/lib/components/modals/modal-message';
import { SameDocNumberValidator } from '@mx/core/ui/public-api';
import { AuthService } from '@mx/services/auth/auth.service';
import { GeneralService } from '@mx/services/general/general.service';
import { HeaderHelperService, IHeader } from '@mx/services/general/header-helper.service';
import { PoliciesService } from '@mx/services/policies.service';
import {
  APPLICATION_CODE,
  GENERAL_MAX_LENGTH,
  GENERAL_MIN_LENGTH,
  REG_EX
} from '@mx/settings/constants/general-values';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import { IdDocument, TypeDocument } from '@mx/statemanagement/models/typedocument.interface';
import { isEmpty } from 'lodash-es';
import { Observable, Subscription, timer } from 'rxjs';

@Component({
  selector: 'client-modal-policy-transfer',
  templateUrl: './modal-policy-transfer.component.html'
})
export class ModalPolicyTransferComponent implements OnInit, OnDestroy {
  @ViewChild(MfModal) mfModal: MfModal;
  @ViewChild(MfModalMessage) modalMessage: MfModalMessage;

  types$: Observable<Array<TypeDocument>>;

  loaded: boolean;
  generalMaxLength = GENERAL_MAX_LENGTH;
  generalMinLength = GENERAL_MIN_LENGTH;
  arrTypesDocuments: Array<SelectListItem>;
  typesDocumentValidators: Array<TypeDocument>;
  documentTypeLabel: string;
  documentNumberLabel: string;
  firstNameLabel: string;
  lastNameLabel: string;
  namesLabel: string;
  phoneLabel: string;
  emailLabel: string;
  maxLengthNumberDoc: String;
  minLengthNumberDoc: String;
  changedNumberDocument: boolean;
  successPolicyTransfer: boolean;
  documentTypeChanged = true;
  showModalMessage: boolean;
  showLoading: boolean;

  entity: IdDocument;
  policyTransferForm: AbstractControl;

  documentType: AbstractControl;
  documentNumber: AbstractControl;
  firstName: AbstractControl;
  lastName: AbstractControl;
  names: AbstractControl;
  phone: AbstractControl;
  email: AbstractControl;

  messageDone: string;
  messageSuccessUpdate: string;
  formPolicyTransfer: FormGroup;

  detailRoute: IHeader;
  buttonState: boolean;
  documentTypesSub: Subscription;
  formSub: Subscription;
  headerHelperSub: Subscription;
  policyTransferSub: Subscription;

  constructor(
    private readonly generalService: GeneralService,
    private readonly headerHelperService: HeaderHelperService,
    private readonly formBuilder: FormBuilder,
    private readonly authService: AuthService,
    private readonly policiesService: PoliciesService,
    private readonly route: ActivatedRoute
  ) {
    this.loaded = false;
    this.arrTypesDocuments = [];
    this.policyTransferForm = new FormControl(null, Validators.required);
    this.maxLengthNumberDoc = '0';
    this.minLengthNumberDoc = '0';
    this.documentTypeLabel = PolicyLang.Labels.PolicyTransfer.documentType;
    this.documentNumberLabel = PolicyLang.Labels.PolicyTransfer.documentNumber;
    this.firstNameLabel = PolicyLang.Labels.PolicyTransfer.firstName;
    this.lastNameLabel = PolicyLang.Labels.PolicyTransfer.lastName;
    this.namesLabel = PolicyLang.Labels.PolicyTransfer.names;
    this.phoneLabel = PolicyLang.Labels.PolicyTransfer.phone;
    this.emailLabel = PolicyLang.Labels.PolicyTransfer.email;
    this.changedNumberDocument = true;
  }

  ngOnInit(): void {
    this.headerHelperSub = this.headerHelperService.header.subscribe(header => {
      this.detailRoute = header;
    });
    this.initForm();
    this.documentTypesSub = this.generalService.getDocumentTypes().subscribe(
      (types: Array<TypeDocument>) => {
        if (types) {
          this.arrTypesDocuments = types.map(type => new SelectListItem({ text: type.codigo, value: type.codigo }));
          this.typesDocumentValidators = types;
          this.valueChanges();
          this.loaded = true;
        }
      },
      () => {},
      () => {
        this.generalService.setDocumentTypes(this.typesDocumentValidators);
        this.documentTypesSub.unsubscribe();
      }
    );
    this.entity = this.authService.retrieveEntity();
  }

  initForm(): void {
    if (this.formSub) {
      this.formSub.unsubscribe();
    }
    this.createForm();
    this.valueChanges();
    this.buttonState = !this.formPolicyTransfer.valid;
  }

  createForm(): void {
    this.maxLengthNumberDoc = '0';
    const nameRegEx = REG_EX.name;
    const lastnameRegEx = REG_EX.lastname;
    this.formPolicyTransfer = this.formBuilder.group({
      documentType: [null, Validators.required],
      documentNumber: [null, Validators.required],
      firstName: [null, Validators.compose([Validators.required, Validators.pattern(lastnameRegEx)])],
      lastName: [null, Validators.compose([Validators.required, Validators.pattern(lastnameRegEx)])],
      names: [null, Validators.compose([Validators.required, Validators.pattern(nameRegEx)])],
      phone: [null, Validators.required],
      email: [null, EmailValidator.isValid]
    });
    this.documentType = this.formPolicyTransfer.controls['documentType'];
    this.documentNumber = this.formPolicyTransfer.controls['documentNumber'];
    this.firstName = this.formPolicyTransfer.controls['firstName'];
    this.lastName = this.formPolicyTransfer.controls['lastName'];
    this.names = this.formPolicyTransfer.controls['names'];
    this.phone = this.formPolicyTransfer.controls['phone'];
    this.email = this.formPolicyTransfer.controls['email'];
  }

  valueChanges(): void {
    this.formSub = this.formPolicyTransfer.valueChanges.subscribe(() => {
      this.buttonState = !this.formPolicyTransfer.valid;
    });
    this.documentType.valueChanges.subscribe(val => {
      if (val) {
        this.documentTypeChanged = false;
        const sub = timer(0).subscribe(() => {
          this.maxLengthNumberDoc = FormComponent.getMaxMinLength('maxlength', this.typesDocumentValidators, val);
          this.minLengthNumberDoc = FormComponent.getMaxMinLength('minlength', this.typesDocumentValidators, val);

          const validators = FormComponent.getValidatorTypeDocument(val, this.typesDocumentValidators);
          validators.push(SameDocNumberValidator.validate(val, this.entity.type, this.entity.number));
          this.documentNumber.setValidators(validators);

          this.documentTypeChanged = true;
          sub.unsubscribe();
        });
        this.resetForm();
      }
    });
  }

  open(edit: boolean): void {
    this.initForm();
    this.showModalMessage = true;
    setTimeout(() => {
      this.mfModal.open();
    });
  }

  getModalTitle(): string {
    const PolicyTransferModalTitle = PolicyLang.Titles.PolicyTransferModal;

    return PolicyTransferModalTitle;
  }

  getCancelButtonTitle(): string {
    const cancelButtonTextButton = PolicyLang.Buttons.PolicyTransfer.cancelButtonText;

    return cancelButtonTextButton;
  }

  getConfirmButtonTitle(): string {
    const confirmButtonTextButton = PolicyLang.Buttons.PolicyTransfer.confirmButtonText;

    return confirmButtonTextButton;
  }

  showErrors(control: AbstractControl): boolean {
    return !!control && !isEmpty(control.errors) && (control.touched || control.dirty);
  }

  savePolicyTransfer(response: boolean): void {
    if (response) {
      if (this.formPolicyTransfer.valid) {
        const body = {
          tipoDocumento: this.documentType.value,
          documento: this.documentNumber.value,
          apellidoPaterno: this.firstName.value,
          apellidoMaterno: this.lastName.value,
          nombre: this.names.value,
          telefono: this.phone.value,
          email: this.email.value || ''
        };
        const request = {
          codigoApp: APPLICATION_CODE,
          numeroPoliza: this.route.snapshot.paramMap.get('policyNumber')
        };
        this.showLoading = true;
        this.policyTransferSub = this.policiesService.sendPolicyTransferRequest(body, request).subscribe(
          res => {
            this.messageDone = PolicyLang.Messages.PolicyTransferDone;
            this.messageSuccessUpdate = PolicyLang.Messages.PolicyTransferSuccess;
            this.successPolicyTransfer = true;
            this.donePolicyTranfer();
            this.policyTransferSub.unsubscribe();
          },
          error => {
            this.successPolicyTransfer = false;
            this.donePolicyTranfer();
          }
        );
      } else {
        this.markErrors();
      }
    }
  }

  markErrors(): void {
    this.markAsTouchedAndDirty(this.documentType);
    this.markAsTouchedAndDirty(this.documentNumber);
    this.markAsTouchedAndDirty(this.firstName);
    this.markAsTouchedAndDirty(this.lastName);
    this.markAsTouchedAndDirty(this.names);
    this.markAsTouchedAndDirty(this.phone);
    this.markAsTouchedAndDirty(this.email);
  }

  private donePolicyTranfer(): void {
    this.mfModal.close();
    this.showLoading = false;
    this.initForm();
    if (this.successPolicyTransfer) {
      this.modalMessage.open();
    }
  }

  markAsTouchedAndDirty(control: AbstractControl): void {
    control.markAsTouched();
  }

  eventChangedDocument($event: any): void {
    this.changedNumberDocument = false;
    const timer$ = timer(0).subscribe(() => {
      this.changedNumberDocument = true;
      timer$.unsubscribe();
    });
  }

  ngOnDestroy(): void {
    if (!!this.headerHelperSub) {
      this.headerHelperSub.unsubscribe();
    }
  }

  resetForm(): void {
    FormComponent.setControl(this.documentNumber, '');
  }
}
