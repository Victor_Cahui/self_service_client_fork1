import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardPolicyDeductibleComponent } from './card-policy-deductible.component';

@NgModule({
  imports: [CommonModule],
  declarations: [CardPolicyDeductibleComponent],
  exports: [CardPolicyDeductibleComponent]
})
export class CardPolicyDeductibleModule {}
