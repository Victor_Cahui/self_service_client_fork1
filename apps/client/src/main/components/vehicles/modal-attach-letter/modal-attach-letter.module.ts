import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UploadFilesModule } from '@mx/components/shared/upload-files/upload-files.module';
import { MfModalMessageModule, MfModalModule } from '@mx/core/ui';
import { ModalAttachLetterComponent } from './modal-attach-letter.component';

@NgModule({
  imports: [CommonModule, MfModalModule, UploadFilesModule, MfModalMessageModule],
  declarations: [ModalAttachLetterComponent],
  exports: [ModalAttachLetterComponent]
})
export class ModalAttachLetterModule {}
