import { Component, EventEmitter, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { IUploadFileList, UploadFilesComponent } from '@mx/components/shared/upload-files/upload-files.component';
import { MfModal, MfModalMessage } from '@mx/core/ui';
import { AuthService } from '@mx/services/auth/auth.service';
import { HeaderHelperService, IHeader } from '@mx/services/general/header-helper.service';
import { VehicleService } from '@mx/services/vehicle.service';
import { APPLICATION_CODE, Y_AXIS } from '@mx/settings/constants/general-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { VehicleTimelineLang } from '@mx/settings/lang/vehicles.lang';
import { GeneralRequest } from '@mx/statemanagement/models/general.models';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-modal-attach-letter',
  templateUrl: './modal-attach-letter.component.html'
})
export class ModalAttachLetterComponent implements OnInit, OnDestroy {
  @ViewChild(MfModal) modal: MfModal;
  @ViewChild(MfModalMessage) modalMessage: MfModalMessage;
  @ViewChild(UploadFilesComponent) uploadFile: UploadFilesComponent;

  generalLang = GeneralLang;
  yAxis = Y_AXIS;
  lang = VehicleTimelineLang.Compensation;
  listFile: Array<IUploadFileList> = [];
  auth: IdDocument;

  assistanceNumber: number;

  header: IHeader;
  headerSub: Subscription;
  uploadSub: Subscription;
  onFinishUpload: EventEmitter<File> = new EventEmitter<File>();

  showModalMessage: boolean;
  messageSuccessUpdate = this.lang.modal.successDescription;
  messageDone = this.lang.modal.successTitle;

  constructor(
    private readonly headerHelperService: HeaderHelperService,
    private readonly vehicleService: VehicleService,
    private readonly authService: AuthService
  ) {}

  ngOnInit(): void {
    this.auth = this.authService.retrieveEntity();
    this.headerSub = this.headerHelperService.get().subscribe(header => (this.header = header));
  }

  ngOnDestroy(): void {
    if (this.headerSub) {
      this.headerSub.unsubscribe();
    }
    if (this.uploadSub) {
      this.uploadSub.unsubscribe();
    }
  }

  open(assistanceNumber: number): void {
    this.assistanceNumber = assistanceNumber;
    this.uploadFile.showError = false;
    this.showModalMessage = false;
    this.listFile = [];
    this.modal.open();
  }

  confirm(confirm?: boolean): void {
    if (confirm) {
      const formData = new FormData();
      formData.append('archivos', this.listFile[0].file);
      formData.append('numeroAsistencia', `${this.assistanceNumber}`);
      this.listFile[0].load = true;

      const params: GeneralRequest = {
        codigoApp: APPLICATION_CODE
      };

      this.uploadSub = this.vehicleService.addLetter(params, formData).subscribe(
        res => {
          this.onConfirm();
          this.uploadSub.unsubscribe();
        },
        () => {
          this.uploadSub.unsubscribe();
        }
      );
    } else {
      if (this.uploadSub) {
        this.uploadSub.unsubscribe();
      }
    }
  }

  private onConfirm(): void {
    this.modal.close();
    this.showModalMessage = true;
    this.onFinishUpload.next(this.listFile[0].file);
    setTimeout(() => {
      this.modalMessage.open();
    }, 100);
  }

  cancelUpload(): void {
    if (this.uploadSub) {
      this.uploadSub.unsubscribe();
    }
  }
}
