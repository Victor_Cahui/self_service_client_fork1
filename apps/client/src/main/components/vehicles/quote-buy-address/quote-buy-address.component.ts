import { TitleCasePipe } from '@angular/common';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormComponent } from '@mx/components/shared/utils/form';
import { ArrayUtil } from '@mx/core/shared/helpers/util/array';
import { SelectListItem } from '@mx/core/ui';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { PoliciesQuoteService } from '@mx/services/policy/policy-quote.service';
import { UbigeoService } from '@mx/services/ubigeo.service';
import { GENERAL_MAX_LENGTH, GENERAL_MIN_LENGTH } from '@mx/settings/constants/general-values';
import { CALLAO, LIMA } from '@mx/settings/constants/key-values';
import { ProfileLang } from '@mx/settings/lang/profile.lang';
import { VehicleQuoteBuyLang } from '@mx/settings/lang/vehicles.lang';
import { IAddress, IClientAddressResponse, IClientInformationResponse } from '@mx/statemanagement/models/client.models';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { IClientVehicleDetail } from '@mx/statemanagement/models/vehicle.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-quote-buy-address',
  templateUrl: './quote-buy-address.component.html',
  providers: [TitleCasePipe]
})
export class QuoteBuyAddressComponent implements OnInit, OnDestroy {
  @Input() title: string;
  @Input() form: FormGroup;
  @Input() defaultAddress: IAddress;

  lang = VehicleQuoteBuyLang;
  labelsFormAddress = ProfileLang.Labels;
  generalMaxLength = GENERAL_MAX_LENGTH;
  generalMinLength = GENERAL_MIN_LENGTH;

  correspondenceAddress: IClientAddressResponse;
  clientInformation: IClientInformationResponse;

  authEntity: IdDocument;

  vehicleData: IClientVehicleDetail;

  useCorrespondenceAddress: AbstractControl;
  departments: AbstractControl;
  provinces: AbstractControl;
  districts: AbstractControl;
  address: AbstractControl;
  reference: AbstractControl;
  correspondenceAddressForm: AbstractControl;
  departmentName: AbstractControl;
  provinceName: AbstractControl;
  districtName: AbstractControl;

  departamentList: Array<SelectListItem> = [];
  provinceList: Array<SelectListItem> = [];
  districtList: Array<SelectListItem> = [];

  showLoading: boolean;
  useCurrentAddress: boolean;
  hasCorresponseAddress: boolean;
  isFirstTime = true;

  correspondenceSub: Subscription;
  departamentsSub: Subscription;
  provincesSub: Subscription;
  districtsSub: Subscription;

  prevProv: number;

  constructor(
    private readonly fb: FormBuilder,
    private readonly authService: AuthService,
    private readonly quoteService: PoliciesQuoteService,
    private readonly clientService: ClientService,
    private readonly ubigeoService: UbigeoService,
    private readonly titleCasePipe: TitleCasePipe
  ) {}

  ngOnInit(): void {
    this.authEntity = this.authService.retrieveEntity();
    this.clientInformation = this.clientService.getUserData();
    this.vehicleData = this.quoteService.getVehicleData();
    this.createForm();
    this.getCorrespondenceAddress();
  }

  ngOnDestroy(): void {
    if (this.correspondenceSub) {
      this.correspondenceSub.unsubscribe();
    }
    if (this.departamentsSub) {
      this.departamentsSub.unsubscribe();
    }
    if (this.provincesSub) {
      this.provincesSub.unsubscribe();
    }
    if (this.districtsSub) {
      this.districtsSub.unsubscribe();
    }
  }

  createForm(): void {
    const group = this.fb.group({
      useCorrespondenceAddress: [],
      correspondenceAddress: [],
      departments: [{ value: null, disabled: true }],
      provinces: [],
      districts: [],
      address: [],
      reference: [],
      departmentName: [],
      provinceName: [],
      districtName: []
    });

    this.form.addControl('address', group);

    this.useCorrespondenceAddress = this.form.controls['address'].get('useCorrespondenceAddress');
    this.correspondenceAddressForm = this.form.controls['address'].get('correspondenceAddress');
    this.departments = this.form.controls['address'].get('departments');
    this.provinces = this.form.controls['address'].get('provinces');
    this.districts = this.form.controls['address'].get('districts');
    this.address = this.form.controls['address'].get('address');
    this.reference = this.form.controls['address'].get('reference');
    this.departmentName = this.form.controls['address'].get('departmentName');
    this.provinceName = this.form.controls['address'].get('provinceName');
    this.districtName = this.form.controls['address'].get('districtName');

    this.addValidators(true);
    this.setDefaultAddress();
    this.onProvinceChange();
    this.onDistrictChange();
  }

  getCorrespondenceAddress(): void {
    const data = this.quoteService.getCorrespondenceAddress();
    if (data) {
      this.setDataAddress(data);
    } else {
      this.showLoading = true;
      this.correspondenceSub = this.clientService.getCorresponseAddress({}).subscribe(
        (clientAddress: IClientAddressResponse) => {
          this.setDataAddress(clientAddress);
          this.quoteService.setCorrespondenceAddress(clientAddress);
          this.showLoading = false;
          this.correspondenceSub.unsubscribe();
        },
        () => {
          this.showLoading = false;
          this.correspondenceSub.unsubscribe();
        }
      );
    }
  }

  setDataAddress(clientAddress: IClientAddressResponse): void {
    this.hasCorresponseAddress = !!(clientAddress && clientAddress.direccion);
    this.correspondenceAddress = clientAddress;
    this.correspondenceAddressForm.setValue(clientAddress);
    this.useCurrentAddress = this.defaultAddress ? this.defaultAddress.useDefault : this.hasCorresponseAddress;
    this.addValidators(!this.useCurrentAddress);

    if (!this.useCurrentAddress) {
      this.setDepartament();
    }

    this.useCorrespondenceAddress.setValue(this.useCurrentAddress);
  }

  setDefaultAddress(): void {
    if (this.defaultAddress && !this.defaultAddress.useDefault) {
      this.provinces.setValue(this.defaultAddress.provinceId);
      this.districts.setValue(this.defaultAddress.districtId);
      this.address.setValue(this.defaultAddress.address);
      this.reference.setValue(this.defaultAddress.reference);
    }
  }

  // Ubigeo
  setDepartament(): void {
    if (!this.departamentList.length) {
      const text = this.vehicleData.ciudad.toLowerCase() === CALLAO ? LIMA : this.vehicleData.ciudad;
      this.departamentList.push({
        text: this.titleCasePipe.transform(text),
        value: this.vehicleData.ubigeoId
      } as any);
      this.departments.setValue(this.vehicleData.ubigeoId);
      this.departmentName.setValue(text);
      this.getProvinces();
    }
  }

  getProvinces(): void {
    if (this.departments.value) {
      this.provincesSub = this.ubigeoService.getProvinces(this.departments.value).subscribe(
        provinces => {
          this.provinceList = [];
          (provinces || []).forEach(province => this.provinceList.push(this.setSelectListItem(province)));
        },
        () => {},
        () => {
          this.provincesSub.unsubscribe();
        }
      );
    }
  }

  getDistricts(): void {
    if (this.provinces.value) {
      this.districtList = [];
      this.districtsSub = this.ubigeoService.getDistricts(this.provinces.value).subscribe(
        districts => {
          (districts || []).forEach(district => this.districtList.push(this.setSelectListItem(district)));
        },
        () => {},
        () => {
          this.districtsSub.unsubscribe();
        }
      );
    }
  }

  setSelectListItem(ubigeo: any): SelectListItem {
    return new SelectListItem({
      text: ubigeo.descripcion,
      value: ubigeo.ubigeoId.toString()
    });
  }

  changeCurrentAdress(value: any): void {
    this.useCurrentAddress = value.target.checked;
    this.addValidators(!this.useCurrentAddress);
    this.useCorrespondenceAddress.setValue(this.useCurrentAddress);
    if (!this.useCurrentAddress) {
      this.setDepartament();
    }
  }

  onProvinceChange(): void {
    this.provinces.valueChanges.subscribe(value => {
      if (value && value !== this.prevProv) {
        this.prevProv = value;
        if (!this.isFirstTime) {
          setTimeout(() => {
            this.districts.setValue(null);
          });
        } else {
          this.isFirstTime = false;
        }
        this.provinceName.setValue(ArrayUtil.getTextfromList(this.provinceList, value));
        this.getDistricts();
      }
    });
  }

  onDistrictChange(): void {
    this.districts.valueChanges.subscribe(value => {
      if (value) {
        this.districtName.setValue(ArrayUtil.getTextfromList(this.districtList, value));
      }
    });
  }

  addValidators(required: boolean): void {
    if (required) {
      this.departments.setValidators(Validators.required);
      this.provinces.setValidators(Validators.required);
      this.districts.setValidators(Validators.required);
      this.address.setValidators(Validators.required);
      this.reference.setValidators(Validators.required);
    } else {
      this.departments.clearValidators();
      this.provinces.clearValidators();
      this.districts.clearValidators();
      this.address.clearValidators();
      this.reference.clearValidators();
    }
    this.departments.updateValueAndValidity();
    this.provinces.updateValueAndValidity();
    this.districts.updateValueAndValidity();
    this.address.updateValueAndValidity();
    this.reference.updateValueAndValidity();
  }

  showErrors(controlKey: string): boolean {
    return FormComponent.showBasicErrors(this.form.get('address') as FormGroup, controlKey);
  }
}
