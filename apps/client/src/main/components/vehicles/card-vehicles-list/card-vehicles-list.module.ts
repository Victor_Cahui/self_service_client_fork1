import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DropdownAccesoriesModule } from '@mx/components/shared/dropdown/dropdown-accesories/dropdown-accesories.module';
import { IconPoliciesModule } from '@mx/components/shared/icon-policies/icon-polices.module';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { LinksModule } from '@mx/core/ui/lib/links/links.module';
import { ScrollService } from '@mx/services/scroll.service';
import { CardVehiclesListComponent } from './card-vehicles-list.component';

@NgModule({
  imports: [
    CommonModule,
    DirectivesModule,
    MfCardModule,
    LinksModule,
    IconPoliciesModule,
    RouterModule,
    DropdownAccesoriesModule,
    MfLoaderModule
  ],
  declarations: [CardVehiclesListComponent],
  exports: [CardVehiclesListComponent],
  providers: [ScrollService]
})
export class CardVehiclesListModule {}
