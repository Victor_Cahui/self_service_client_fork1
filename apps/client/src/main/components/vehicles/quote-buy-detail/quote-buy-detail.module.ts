import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardVigencyModule } from '@mx/components/shared/card-vigency/card-vigency.module';
import { CardSummaryQuoteModule } from '@mx/components/shared/payments/card-summary-quote';
import { QuoteBuyDetailComponent } from './quote-buy-detail.component';

@NgModule({
  imports: [CommonModule, CardSummaryQuoteModule, CardVigencyModule],
  declarations: [QuoteBuyDetailComponent],
  exports: [QuoteBuyDetailComponent]
})
export class QuoteBuyDetailModule {}
