import { ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  CARD_STATUS_VEHICLE,
  CARD_TYPES,
  KEY_STATUS_VEHICLE
} from '@mx/components/shared/timeline/components/timeline-config';
import { dateToStringFormatMMDDYYYY } from '@mx/core/shared/helpers/util/date';
import { FileUtil } from '@mx/core/shared/helpers/util/file';
import { GeolocationService } from '@mx/core/shared/helpers/util/geolocation.service';
import { ObjectUtil } from '@mx/core/shared/helpers/util/object';
import { UnsubscribeOnDestroy } from '@mx/core/shared/helpers/util/unsubscribe-on-destroy';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { VehicleService } from '@mx/services/vehicle.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import {
  CAR_ICON,
  CIRCLE_ICON,
  CLOCK_ICON,
  DOWNLOAD_ICON,
  IDEA_ICON,
  MAIL_ICON,
  PHONE_ICON,
  PIN_ICON,
  PROFILE_ICON,
  TOOL_ICON
} from '@mx/settings/constants/images-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { OfficeLang } from '@mx/settings/lang/office.lang';
import { ASSISTANCE, VehicleTimelineLang } from '@mx/settings/lang/vehicles.lang';
import { IItemView } from '@mx/statemanagement/models/general.models';
import { IOutputEvent, ItemTimeline } from '@mx/statemanagement/models/timeline.interface';
import { IAssistanceDetailRequest } from '@mx/statemanagement/models/vehicle.interface';
import { Observable, Subscription } from 'rxjs';
import { isNullOrUndefined } from 'util';
import { ModalAttachLetterComponent } from '../modal-attach-letter/modal-attach-letter.component';

export abstract class TimelineVehiclesBase extends UnsubscribeOnDestroy {
  @ViewChild(ModalAttachLetterComponent) modalAttachLetter: ModalAttachLetterComponent;

  lang = ASSISTANCE;
  itemList: Array<ItemTimeline> = [];
  assistanceType$: Observable<any>;
  assistanceSub: Subscription;
  geolocatonSub: Subscription;
  onFinishUpload: Subscription;
  fileToBase64Sub: Subscription;
  fileUtil: FileUtil = new FileUtil();
  showLoading: boolean;
  issuedBudgetItem = 1;
  acceptedBudgetItem = 1;
  inRepairItem = 1;
  assistanceNumber: number;
  cards: any;
  keys = KEY_STATUS_VEHICLE;
  titleHeader: string;
  subtitleHeader: string;
  titleCardDeclared: string;

  constructor(
    protected headerHelperService: HeaderHelperService,
    protected activePath: ActivatedRoute,
    protected vehicleService: VehicleService,
    protected geolocationService: GeolocationService
  ) {
    super();
  }

  // Configurar vista
  setView(): void {
    this.assistanceNumber = this.activePath.snapshot.params.number;

    const header = {
      title: `${this.subtitleHeader} ${this.assistanceNumber}`,
      subTitle: this.titleHeader,
      back: true
    };
    this.headerHelperService.set(header);

    this.cards = CARD_STATUS_VEHICLE;
  }

  getParams(): IAssistanceDetailRequest {
    return {
      codigoApp: APPLICATION_CODE,
      numeroAsistencia: this.assistanceNumber
    };
  }

  // Buscar data
  getData(): void {
    this.showLoading = true;
    this.assistanceSub = this.assistanceType$.subscribe(
      res => {
        if (res && res.estados) {
          const statusList = res.estados;
          const cardList = res.cards;

          // Configurar Cards
          statusList.forEach(status => {
            const char = status.indexOf('-');
            // tslint:disable-next-line: no-parameter-reassignment
            status = char !== -1 ? status.substring(0, char) : status;

            let item = null;
            const cardData = cardList[status] || {};

            switch (status) {
              case this.keys.DECLARED:
                item = this.setDeclared(status, cardData);
                break;
              case this.keys.ATTENDED:
                this.setAttended(status, cardData);
                break;
              case this.keys.IN_WORKSHOP:
                item = this.setInWorkShop(status, cardData);
                break;
              case this.keys.ISSUED_BUDGET:
                this.setIssuedBudget(status, cardList);
                break;
              case this.keys.ACCEPTED_BUDGET:
                this.setAcceptedBudget(status, cardList);
                break;
              case this.keys.IN_REPAIR:
                this.setInRepair(status, cardList);
                break;
              case this.keys.VEHICLE_DELIVERED:
                this.setVehicleDelivery(status, cardData);
                break;
              case this.keys.REFUSED:
                item = this.setRefused(status, cardData);
                break;
              case this.keys.IN_EVALUATION:
                item = this.setInEvaluation(status, cardData);
                break;
              case this.keys.APPROVED:
                item = this.setApproved(status, cardData);
                break;
              case this.keys.DOCUMENT_DELIVERED:
                item = this.setDocumentDelivered(status, cardData);
                break;
              case this.keys.COMPENSATION:
                item = this.setCompensation(status, cardData);
                break;
              case this.keys.RECOVERED:
                item = this.setRecovered(status, cardData);
                break;
              default:
                break;
            }

            if (item) {
              this.itemList.push(item);
            }
          });
        }
        this.showLoading = false;
      },
      () => {
        this.showLoading = false;
      }
    );
  }

  // Declarado
  setDeclared(key: string, cardData: any): ItemTimeline {
    const cardConfig = this.cards[key];
    const data: ItemTimeline = {
      date: cardData.fecha || '',
      title: this.titleCardDeclared,
      status: key
    };

    if (this.hasData(cardData)) {
      data.card = cardConfig.card;
      data.content = {
        itemList: [
          { text: this.lang.Labels.DeclaredPlace },
          this.getAddress(cardData),
          this.getVehicle(cardData),
          this.getHour(cardData.hora)
        ]
      };
      data.content.itemList = this.filterNull(data.content.itemList);
    }

    return data;
  }

  // Atendido
  setAttended(key: string, cardData: any): void {
    const cardConfig = this.cards[key];
    let data: ItemTimeline;

    // Envia al procurador
    if (cardData && this.hasData(cardData.ENVIO_PROCURADOR)) {
      data = {
        date: cardData.ENVIO_PROCURADOR.fecha || '',
        title: cardConfig.ENVIO_PROCURADOR.title,
        status: key,
        rigth: true,
        card: cardConfig.ENVIO_PROCURADOR.card,
        content: {
          itemList: [this.getHour(cardData.ENVIO_PROCURADOR.horaEnvioProcurador)]
        }
      };
      data.content.itemList = this.filterNull(data.content.itemList);
      this.itemList.push(data);
    }

    // Llego procurador
    if (cardData && this.hasData(cardData.LLEGADA_PROCURADOR)) {
      data = {
        date: cardData.LLEGADA_PROCURADOR.fecha || '',
        title: cardConfig.LLEGADA_PROCURADOR.title,
        status: key,
        rigth: true,
        card: cardConfig.LLEGADA_PROCURADOR.card,
        content: {
          itemList: [
            {
              text: this.lang.Labels.Place
            },
            this.getAddress(cardData.LLEGADA_PROCURADOR),
            this.getPeople(this.lang.Attorney, cardData.LLEGADA_PROCURADOR.procurador),
            this.getHour(cardData.LLEGADA_PROCURADOR.horaLlegadaProcurador)
          ]
        }
      };
      data.content.itemList = this.filterNull(data.content.itemList);

      if (cardData.LLEGADA_PROCURADOR.informeProcurador) {
        data.footer = cardConfig.LLEGADA_PROCURADOR.footer;
        data.content.action = {
          text: this.lang.Labels.ViewReport,
          icon: DOWNLOAD_ICON,
          base64: cardData.LLEGADA_PROCURADOR.informeProcurador,
          fileName: this.lang.Labels.ReportFileName
        };
      }

      this.itemList.push(data);
    }
  }

  // En el Taller
  setInWorkShop(key: string, cardData: any): ItemTimeline {
    const cardConfig = this.cards[key];
    const data: ItemTimeline = {
      date: cardData.fecha,
      title: cardConfig.title,
      status: key
    };

    if (this.hasData(cardData)) {
      data.card = cardConfig.card;
      data.content = {
        itemList: [
          cardData.taller
            ? {
                text: cardData.taller || ''
              }
            : null,
          this.getAddress(cardData),
          this.getBusinessHours(cardData),
          this.getPhone(cardData.telefono),
          this.getEmail(cardData.correo)
        ]
      };
      data.content.itemList = this.filterNull(data.content.itemList);
    }

    return data;
  }

  // Presupuesto emitido
  setIssuedBudget(key: string, cardList: any): void {
    let cardData = cardList[key];
    if (this.issuedBudgetItem > 1) {
      cardData = cardList[`${key}-${this.issuedBudgetItem}`];
    }
    const cardConfig = this.cards[key];
    cardConfig.card = this.issuedBudgetItem > 1 ? CARD_TYPES.TEXT : '';
    const data = {
      date: cardData.fecha,
      title: this.issuedBudgetItem > 1 ? cardConfig.title2 : cardConfig.title,
      status: key,
      card: cardConfig.card,
      content: {
        content: this.lang.Labels.AnotherBudget.replace('{{n}}', (this.issuedBudgetItem - 1).toString())
      }
    };

    this.issuedBudgetItem++;
    this.itemList.push(data);
  }

  // Presupuesto aceptado
  setAcceptedBudget(key: string, cardList: any): void {
    let cardData = cardList[key];
    if (this.acceptedBudgetItem > 1) {
      cardData = cardList[`${key}-${this.acceptedBudgetItem}`];
    }
    const cardConfig = this.cards[key];
    const data: ItemTimeline = {
      date: cardData.fecha,
      title: this.acceptedBudgetItem > 1 ? cardConfig.title2 : cardConfig.title,
      status: key,
      rigth: true
    };

    if (this.hasData(cardData)) {
      data.card = cardConfig.card;
      data.content = {
        table2: [
          { column: `${this.lang.Labels.Cost}:`, row: cardData.costo },
          { column: `${this.lang.Labels.Deductible}:`, row: cardData.deducible, tooltips: cardData.tooltip },
          { column: `${this.lang.Labels.YouPay}:`, row: cardData.montoCliente }
        ]
      };
    }

    this.acceptedBudgetItem++;
    this.itemList.push(data);
  }

  // En reparación
  setInRepair(key: string, cardList: any): void {
    let cardData = cardList[key];
    if (this.inRepairItem > 1) {
      cardData = cardList[`${key}-${this.inRepairItem}`];
    }
    const cardConfig = this.cards[key];
    const data: ItemTimeline = {
      date: cardData.fecha,
      title: cardConfig.title,
      status: key
    };

    if (this.hasData(cardData)) {
      data.card = cardConfig.card;
      data.content = {
        title: `${this.lang.Labels.Phase}:`,
        content: cardData.fase,
        icon: TOOL_ICON,
        itemList: [
          {
            text: this.lang.Remember,
            icon: IDEA_ICON
          }
        ]
      };
    }

    this.inRepairItem++;
    this.itemList.push(data);
  }

  // Entregado
  setVehicleDelivery(key: string, cardData: any): void {
    const cardConfig = this.cards[key];
    let data: ItemTimeline;

    // Entrega estimada
    if (cardData && this.hasData(cardData.ENTREGA_ESTIMADA) && cardData.ENTREGA_ESTIMADA.fecha) {
      data = {
        date: cardData.ENTREGA_ESTIMADA.fecha || '',
        title: cardConfig.ENTREGA_ESTIMADA.title,
        status: key,
        estimated: true
      };
      this.itemList.push(data);
    }

    // Puede recoger
    if (cardData && this.hasData(cardData.PUEDE_RECOGER)) {
      data = {
        date: cardData.PUEDE_RECOGER.fecha || '',
        title: cardConfig.PUEDE_RECOGER.title,
        status: key,
        card: cardConfig.PUEDE_RECOGER.card,
        content: {
          title: `${this.lang.Labels.Phase}:`,
          content: `${this.lang.Delivery}`,
          icon: CAR_ICON
        }
      };
      this.itemList.push(data);
    }

    // Entregado
    if (cardData && this.hasData(cardData.ENTREGADO)) {
      data = {
        date: cardData.ENTREGADO.fecha || '',
        title: cardConfig.ENTREGADO.title,
        status: key,
        footer: cardConfig.ENTREGADO.footer,
        finish: false,
        card: cardConfig.ENTREGADO.card,
        content: {
          title: `${this.lang.Labels.Phase}:`,
          content: `${this.lang.Labels.Delivery} ${dateToStringFormatMMDDYYYY(cardData.ENTREGADO.fecha)}`,
          icon: CAR_ICON
        }
      };

      data.content.action = {
        itemId: cardData.ENTREGADO.identificadorTaller,
        rated: cardData.ENTREGADO.calificoTaller,
        rate: cardData.ENTREGADO.calificacion
      };

      this.itemList.push(data);
    }
  }

  // Rechazado
  setRefused(key: string, cardData: any): ItemTimeline {
    const cardConfig = this.cards[key];
    const data = {
      date: cardData.fecha,
      title: cardConfig.title,
      status: key,
      rigth: true,
      finish: true
    };

    return data;
  }

  // Siniestro o Pérdida Total en Evaluación
  setInEvaluation(key: string, cardData: any): ItemTimeline {
    const cardConfig = this.cards[key];
    const data: ItemTimeline = {
      date: cardData.fecha || '',
      title: cardConfig.title,
      status: key,
      rigth: true
    };

    if (this.hasData(cardData)) {
      data.card = cardConfig.card;
      data.content = {
        itemList: [
          { text: `${this.lang.Labels.SinisterExecutive}: ${cardData.ejecutivo}`, icon: PROFILE_ICON },
          { text: `${this.lang.Labels.RequestedDocuments}:` }
        ]
      };

      data.content.itemList = data.content.itemList.concat(this.getRequestedDocuments(cardData.documentos));
      data.content.itemList = this.filterNull(data.content.itemList);
    }

    return data;
  }

  // Aprobado Siniestro o Pérdida Total
  setApproved(key: string, cardData: any): ItemTimeline {
    const cardConfig = this.cards[key];
    const data: ItemTimeline = {
      date: cardData.fecha || '',
      title: cardConfig.title,
      status: key,
      rigth: true
    };

    if (cardData.cartaPerdidaTotal) {
      data.footer = cardConfig.footer;
      data.content = {
        action: {
          text: this.lang.Labels.ViewTotalLost,
          icon: DOWNLOAD_ICON,
          base64: cardData.cartaPerdidaTotal,
          fileName: this.lang.Labels.TotalLostFileName
        }
      };
    }

    return data;
  }

  // Entrega de documentos
  setDocumentDelivered(key: string, cardData: any): ItemTimeline {
    const cardConfig = this.cards[key];
    const data: ItemTimeline = {
      date: cardData.fecha || '',
      title: cardConfig.title,
      status: key
    };
    if (this.hasData(cardData)) {
      data.card = cardConfig.card;
      data.content = {
        itemList: [{ text: this.lang.Texts.NotaryDocuments }]
      };
      data.content.itemList = data.content.itemList.concat(this.getRequestedDocuments(cardData.documentos));
      data.content.itemList = data.content.itemList.concat([
        { text: cardData.notaria, textBold: true },
        this.getContactPerson(cardData),
        this.getAddress(cardData),
        this.getPhone(cardData.telefono),
        this.getBusinessHours(cardData)
      ]);

      data.content.itemList = this.filterNull(data.content.itemList);

      if (cardData.latitud && cardData.longitud) {
        this.getGoogleUrl(cardData.latitud, cardData.longitud);
        data.footer = cardConfig.footer;
        data.content.action = {
          text: this.lang.Labels.AddressIssue,
          textLink: this.lang.Labels.GoToMap
          // url: this.urlGoogle
        };
      }
    }

    return data;
  }

  // Indemnización
  setCompensation(key: string, cardData: any): ItemTimeline {
    const cardConfig = this.cards[key];
    const data: ItemTimeline = {
      date: cardData.fecha || '',
      title: cardData.chequeEmitido ? VehicleTimelineLang.Compensation.titile2 : cardConfig.title,
      status: key,
      content: {}
    };
    data.card = cardConfig.card;
    if (!cardData.chequeEmitido) {
      data.content = ObjectUtil.clone(VehicleTimelineLang.Compensation);
      data.content.tooltips.title += ` ${cardData.propietarioVehiculo}`;

      data.content.content =
        cardData.necesitaSubirCartaNoAdeudo && !cardData.cartaNoAdeudo ? data.content.content : data.content.content2;

      data.content.action = !cardData.necesitaSubirCartaNoAdeudo
        ? null
        : cardData.necesitaSubirCartaNoAdeudo && !cardData.cartaNoAdeudo
        ? data.content.action
        : cardData.cartaNoAdeudo
        ? VehicleTimelineLang.Compensation.action2
        : null;

      if (data.content.action) {
        data.footer = cardConfig.footer;
        data.content.action.base64 = cardData.cartaNoAdeudo;
        data.content.action.base64Type = cardData.tipoArchivo;
        data.content.action.fileName = `${cardConfig.title.toLowerCase()}`;
      }
    }

    return data;
  }

  // Recuperado
  setRecovered(key: string, cardData: any): ItemTimeline {
    const cardConfig = this.cards[key];
    const data = {
      date: cardData.fecha || '',
      title: cardConfig.title,
      status: key,
      rigth: true
    };

    return data;
  }

  // Verifica si tiene data y el objeto viene lleno
  hasData(cardData): boolean {
    return cardData && !!Object.keys(cardData).length;
  }

  filterNull(itemList: Array<IItemView>): Array<IItemView> {
    return itemList.filter(item => !isNullOrUndefined(item));
  }

  // Iconos y textos

  getContactPerson(data: any): IItemView {
    return data.personaContacto
      ? {
          text: data.personaContacto,
          icon: PROFILE_ICON
        }
      : null;
  }

  getAddress(data: any): IItemView {
    return data.direccion || data.distrito
      ? {
          text: `${data.direccion}${data.direccion && data.distrito ? ', ' : ''}${data.distrito}`,
          icon: PIN_ICON
        }
      : null;
  }

  getVehicle(data: any): IItemView {
    return data.placa || data.marca || data.modelo || data.anio
      ? {
          text: `${data.placa}${data.placa && data.marca ? ' - ' : ''}${data.marca}
          ${data.modelo}${data.modelo && data.anio ? ' - ' : ''}${data.anio}`,
          icon: CAR_ICON
        }
      : null;
  }

  getHour(hora: string): IItemView {
    return hora
      ? {
          text: `${GeneralLang.Labels.Hour}: ${(hora && hora.toUpperCase()) || ''}`,
          icon: CLOCK_ICON
        }
      : null;
  }

  getBusinessHours(data: any): IItemView {
    return data.horarioAtencion
      ? {
          text: data.horarioAtencion || '',
          icon: CLOCK_ICON
        }
      : null;
  }

  getPhone(phone: string): IItemView {
    return phone
      ? {
          text: phone || '',
          icon: PHONE_ICON
        }
      : null;
  }

  getEmail(email: string): IItemView {
    return email
      ? {
          text: email || '',
          icon: MAIL_ICON
        }
      : null;
  }

  getPeople(title: string, name: string): IItemView {
    return name
      ? {
          text: `${title} ${name}`,
          icon: PROFILE_ICON
        }
      : null;
  }

  getRequestedDocuments(items: Array<string>): Array<IItemView> {
    return items.map(item => ({ text: item, icon: CIRCLE_ICON, iconBullet: true }));
  }

  getGoogleUrl(lat: string, lng: string): void {
    let url: string;
    this.geolocatonSub = this.geolocationService.getCurrentPosition().subscribe(
      position => {
        url = position ? `${OfficeLang.Links.gmapsDir}${position.latitude},${position.longitude}/${lat},${lng}` : '';
        this.itemList.find(i => i.status === this.keys.DOCUMENT_DELIVERED).content.action.url = url;
      },
      () => {
        url = `${OfficeLang.Links.gmapsDir}${lat},${lng}`;
        this.itemList.find(i => i.status === this.keys.DOCUMENT_DELIVERED).content.action.url = url;
        this.geolocatonSub.unsubscribe();
      },
      () => {
        this.geolocatonSub.unsubscribe();
      }
    );
  }

  eventTimeline(event: IOutputEvent): void {
    const card = this.itemList.find(item => item.status === event.card);
    if (card.status === KEY_STATUS_VEHICLE.COMPENSATION && !card.content.action.base64) {
      this.modalAttachLetter.open(this.assistanceNumber);
      this.onFinishUpload = this.modalAttachLetter.onFinishUpload.subscribe((file: File) => {
        //  Escuchas cambios del modal. Y cambiar estado de card
        this.fileToBase64Sub = this.fileUtil.fileToBase64(file).subscribe(base64 => {
          card.content.content = VehicleTimelineLang.Compensation.content2;
          card.content.action = VehicleTimelineLang.Compensation.action2;
          card.content.action.base64 = base64;
          card.content.action.base64Type = file.type;
          card.content.action.fileName = `${card.title.toLowerCase()}`;
          this.onFinishUpload.unsubscribe();
          this.fileToBase64Sub.unsubscribe();
        });
      });
    }
  }
  // tslint:disable-next-line: max-file-line-count
}
