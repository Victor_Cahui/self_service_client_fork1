import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ItemNotFoundModule } from '@mx/components/shared/item-not-found/item-not-found.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule, MfCardModule, MfInputModule, MfPaginatorModule, MfSelectModule } from '@mx/core/ui';
import { MfButtonModule } from '@mx/core/ui/lib/components/forms/button';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { LinksModule } from '@mx/core/ui/lib/links/links.module';
import { CardSinistersInProgressComponent } from './card-sinisters-in-progress.component';

@NgModule({
  imports: [
    CommonModule,
    DirectivesModule,
    FormsModule,
    LinksModule,
    ItemNotFoundModule,
    MfButtonModule,
    MfCardModule,
    MfPaginatorModule,
    MfInputModule,
    MfLoaderModule,
    MfSelectModule,
    ReactiveFormsModule,
    GoogleModule
  ],
  declarations: [CardSinistersInProgressComponent],
  exports: [CardSinistersInProgressComponent, MfCardModule]
})
export class CardSinistersInProgressModule {}
