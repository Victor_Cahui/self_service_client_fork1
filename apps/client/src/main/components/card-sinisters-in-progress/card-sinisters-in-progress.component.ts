import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { convertMonth } from '@mx/core/shared/helpers/util/date';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { Autoservicios, FrmProvider } from '@mx/core/shared/providers/services';
import { environment } from '@mx/environments/environment';
import { AuthService } from '@mx/services/auth/auth.service';
import {
  APPLICATION_CODE,
  GENERAL_MAX_LENGTH,
  REG_EX,
  SEARCH_ORDEN_DESC,
  SINISTER_TYPE
} from '@mx/settings/constants/general-values';
import {
  CAR_ICON,
  DOCUMENT_ICON,
  FILE_ICON,
  HOUSE_ICON,
  NO_PENDING_PROCEDURES_ICON,
  PERSON_ICON,
  SEARCH_ICON
} from '@mx/settings/constants/images-values';
import { ESTADO_SINIESTRO } from '@mx/settings/constants/key-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { SINISTERS_TYPE, SinistersLang } from '@mx/settings/lang/sinisters.lang';
import { ASSISTANCE } from '@mx/settings/lang/vehicles.lang';
import { isEmpty } from 'lodash-es';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'client-card-sinisters-in-progress',
  templateUrl: './card-sinisters-in-progress.component.html'
})
export class CardSinistersInProgressComponent extends GaUnsubscribeBase implements OnInit {
  @Input() isInHome: boolean;
  @Input() disableCollapse: boolean;
  @Input() disableIcon: boolean;
  @Input() disableViewAll: boolean;
  @Input() isVisibleFilterHeader: boolean;
  @Input() hasPaginator: boolean;
  @Input() isHistorical: boolean;
  @Input() stateClass = 'w-px-lg--15 pr-lg-3 text-truncate';
  @Output() withoutSinisters: EventEmitter<boolean> = new EventEmitter<boolean>();
  collapse = false;
  lang = ASSISTANCE;
  fileIcon = FILE_ICON;
  generalLang = GeneralLang;
  showFilter: boolean;
  titleCard: string;
  total: number;

  labelViewAll = GeneralLang.Buttons.ViewAll;
  labelViewDetail = GeneralLang.Buttons.ViewDetail;
  page = 1;
  itemsPerPage = 10;
  sinistersLang = SinistersLang;
  maxLengthPolice = GENERAL_MAX_LENGTH.numberPolice;
  messageWithoutProceduresOnGoing: string;
  numericRegExp = REG_EX.numeric;
  labelFilterBy = GeneralLang.Labels.FilterBy;
  btnFilter = GeneralLang.Buttons.Filter;
  showLoading: boolean;
  showViewDetail = environment.ACTIVATE_PROCEDURES_DETAIL;
  frm: FormGroup;
  iconNotFound = DOCUMENT_ICON;
  msgNotFound = GeneralLang.Messages.InitSinisterItemNotFound;
  listSinisterInProgress: Array<any>;
  iconNoPending = NO_PENDING_PROCEDURES_ICON;
  isFirstRequest = true;
  isFilteredFrm: boolean;

  constructor(
    private readonly _AuthService: AuthService,
    private readonly _Autoservicios: Autoservicios,
    private readonly _FrmProvider: FrmProvider,
    public router: Router,
    protected gaService: GAService
  ) {
    super(gaService);
  }

  ngOnInit(): void {
    this._initValues();
    this.getSinister();

    this.messageWithoutProceduresOnGoing = GeneralLang.Messages.WithoutInProgressSinisters;
  }

  getSinister(): void {
    this.showFilter = false;
    this.showLoading = true;
    this.listSinisterInProgress = [];
    this.total = 0;

    this._getService().subscribe(this._mapResponse.bind(this), (error: HttpErrorResponse) => {
      this.showLoading = false;
      console.error(error);
    });
  }

  setLabelSinisterType(sinisterType: string): string {
    return sinisterType === SINISTERS_TYPE.ACCIDENT
      ? this.lang.AssistanceNumber
      : sinisterType === SINISTERS_TYPE.STOLEN
      ? this.lang.StolenNumber
      : this.lang.SinisterNumber;
  }

  setRiskIcon(policyType: string): string {
    let icon = PERSON_ICON;
    if (
      policyType === POLICY_TYPES.MD_AUTOS.code ||
      policyType === POLICY_TYPES.MD_SOAT.code ||
      policyType === POLICY_TYPES.MD_SOAT_ELECTRO.code
    ) {
      icon = CAR_ICON;
    } else {
      if (policyType === POLICY_TYPES.MD_HOGAR.code) {
        icon = HOUSE_ICON;
      }
    }

    return icon;
  }

  calculateDay(myDate: string): string {
    return !isEmpty(myDate) ? myDate.split('-')[2] : '';
  }

  calculateMonth(myDate: string): string {
    return !isEmpty(myDate) ? convertMonth(parseInt(myDate.split('-')[1], 10)) : '';
  }

  calculateYear(myDate: string): string {
    return !isEmpty(myDate) ? myDate.split('-')[0] : '';
  }

  goToSinisterDetail(sinisterNumber: number, expedientType: string): void {
    if (expedientType !== SINISTERS_TYPE.OTHERS) {
      const type = expedientType === SINISTERS_TYPE.ACCIDENT ? 'accidents' : 'stolen';
      const path = `vehicles/${type}/assists/detail/${sinisterNumber}`;
      this.router.navigate([path]);
    }
  }

  itemsPerPageChange(itemsPerPage?: number): void {
    this.page = 1;
    this.itemsPerPage = itemsPerPage || this.itemsPerPage;
    this._configNotFound();
    this.getSinister();
  }

  pageChange(page: number): void {
    this.page = page;
    this._configNotFound();
    this.getSinister();
  }

  filter(): void {
    this.isFirstRequest = false;
    this.itemsPerPageChange();
    this.isFilteredFrm = this._isFilteredFrm();
  }

  resetFilter(): void {
    this.frm.reset();
    this.itemsPerPageChange();
    this.isFilteredFrm = false;
  }

  private _isFilteredFrm(): boolean {
    const fk = Object.keys(this.frm.value);

    return !fk.every(k => !this.frm.value[k]);
  }

  private _mapResponse(res): any[] {
    this.showLoading = false;

    if (res) {
      (!res.siniestros.length || !res.totalRegistros) && this.withoutSinisters.next(true);
    } else {
      this.withoutSinisters.next(true);

      return void 0;
    }

    this.listSinisterInProgress = (res.siniestros || []).map(sinister => ({
      ...sinister,
      labelTipoSinistro: this.setLabelSinisterType(sinister.tipoExpediente),
      riskIcon: this.setRiskIcon(sinister.tipoPoliza),
      hasDeail: sinister.tipoExpediente === SINISTERS_TYPE.ACCIDENT || sinister.tipoExpediente === SINISTERS_TYPE.STOLEN
    }));
    this.total = res.totalRegistros;
  }

  private _getService(): Observable<any> {
    const pathReq = { codigoApp: APPLICATION_CODE };
    const queryReq = {
      orden: SEARCH_ORDEN_DESC
    };
    const services = {
      list: this._Autoservicios.ObtenerSiniestro(pathReq, {
        ...queryReq,
        estado: ESTADO_SINIESTRO.pendiente.code,
        pagina: 1,
        registros: 2,
        tipo: SINISTER_TYPE.INPROGRESS
      }),
      paginator: this._Autoservicios.ObtenerSiniestro(pathReq, {
        ...queryReq,
        ...this.frm.value,
        pagina: this.page,
        registros: this.itemsPerPage
      })
    };

    return this.hasPaginator ? services.paginator : services.list;
  }

  private _configNotFound(): void {
    this.iconNotFound = SEARCH_ICON;
    this.msgNotFound = this.generalLang.Messages.ItemNotFound;
  }

  private _initValues(): void {
    this.frm = this._FrmProvider.CardSinistersInProgressComponent();
    this.isVisibleFilterHeader || (this.titleCard = this.generalLang.Titles.Sinister);
  }
}
