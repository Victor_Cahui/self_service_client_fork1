import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google/google.gabase';
import { Autoservicios, FrmProvider } from '@mx/core/shared/providers/services';
import { AffiliatesFacade, getEPSAffiliateRequest, getUpdatedAffiliateEPS } from '@mx/core/shared/state/affiliates';
import { AuthService } from '@mx/services/auth/auth.service';
import { InsuredDependentEditService } from '@mx/services/health/insured-dependent-edit.service';
import { PoliciesService } from '@mx/services/policies.service';
import { ScrollService } from '@mx/services/scroll.service';
import { APPLICATION_CODE, URL_FRAGMENTS } from '@mx/settings/constants/general-values';
import { INSURED_TYPE, POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { IPolicyInsuredResponse } from '@mx/statemanagement/models/policy.interface';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-card-policy-insured-grouped',
  templateUrl: './card-policy-insured-grouped.component.html'
})
export class CardPolicyInsuredGroupedComponent extends GaUnsubscribeBase implements OnInit {
  @Input() policyNumber: string;
  @Input() policyType?: string;
  @Input() numSpto?: string;
  @Input() codRamo?: string;
  @Input() codCia?: string;
  @Input() namePolice?: string;
  @Input() mpKeys: any = {};
  @Input() isPPFM = false;
  policyTypes = POLICY_TYPES;
  insuredTypes = INSURED_TYPE;
  iconCard = 'icon-mapfre_014_perfil';
  searchByClientSub: Subscription;
  listInsuredGrouped: Array<{
    insuredTypeId: number;
    insuredType: string;
    listInsured: Array<IPolicyInsuredResponse>;
    collapse: boolean;
    theyAreEditing: boolean;
  }>;
  data = {
    labelViewDetail: GeneralLang.Buttons.ViewDetail
  };
  insuredFragment = URL_FRAGMENTS.INSURED;
  showLoading: boolean;

  frmHolder: FormGroup;
  shownDetail = null;

  constructor(
    private readonly policiesService: PoliciesService,
    private readonly insuredDependentEditService: InsuredDependentEditService,
    private readonly router: Router,
    private readonly authService: AuthService,
    private readonly scrollService: ScrollService,
    protected gaService: GAService,
    private readonly _FrmProvider: FrmProvider,
    private readonly _AffiliatesFacade: AffiliatesFacade,
    private readonly autoService: Autoservicios
  ) {
    super(gaService);
  }

  ngOnInit(): void {
    this.getInsuredList();
  }

  getInsuredList(): void {
    this.listInsuredGrouped = [];
    this.showLoading = true;
    this.searchByClientSub = this.policiesService
      .insured({
        codigoApp: APPLICATION_CODE,
        numeroPoliza: this.policyNumber
      })
      .subscribe(
        (response: Array<IPolicyInsuredResponse>) => {
          this.showLoading = false;
          this.scrollService.checkScrollTo(this.insuredFragment);
          if (response && response.length > 0) {
            this.listInsuredGrouped = response
              .filter((insured, index, self) => {
                return self.map(insuredX => insuredX.tipoAseguradoId).indexOf(insured.tipoAseguradoId) === index;
              })
              .map(insured => {
                const list = response
                  .filter(insuredY => insuredY.tipoAseguradoId === insured.tipoAseguradoId)
                  .map(insuredZ => ({ ...this.getPropsForUI(insuredZ), isEdit: false, showLoading: false }));

                return {
                  insuredTypeId: insured.tipoAseguradoId,
                  insuredType: this.getTitleGrouped(insured.tipoAseguradoId, list.length),
                  listInsured: list,
                  collapse: false,
                  theyAreEditing: false
                };
              })
              .sort((a, b) => {
                return a.insuredTypeId - b.insuredTypeId;
              });
          } else {
            this.listInsuredGrouped = [];
          }
          this.searchByClientSub.unsubscribe();
        },
        () => (this.showLoading = false)
      );
  }

  getTitleGrouped(insuredTypeId: number, cant: number): string {
    let titleGoup = '';
    switch (insuredTypeId) {
      case INSURED_TYPE.INSURED_CODE:
        titleGoup = cant > 1 ? GeneralLang.Titles.InsuredTypes.Insureds : GeneralLang.Titles.InsuredTypes.Insured;
        break;
      case INSURED_TYPE.BENEFICIARY_CODE:
        titleGoup = GeneralLang.Titles.InsuredTypes.Beneficiary;
        break;
      case INSURED_TYPE.DEPENDENT_CODE:
        titleGoup = GeneralLang.Titles.InsuredTypes.Dependent;
        break;
      default:
        break;
    }

    return titleGoup;
  }

  getFullName(insured: IPolicyInsuredResponse): string {
    return `${insured.nombre} ${insured.apellidoPaterno} ${insured.apellidoMaterno}`;
  }

  getRelation(insured: IPolicyInsuredResponse): string {
    return insured.tipoAseguradoId === INSURED_TYPE.INSURED_CODE && `${this.policyType}` === POLICY_TYPES.MD_VIDA.code
      ? ''
      : insured.relacionDescripcion.toUpperCase() === POLICY_TYPES.MD_OTROS.title.toUpperCase()
      ? ''
      : this.capitalizeRelation(insured.relacionDescripcion.toLowerCase());
  }

  capitalizeRelation(relation: string): string {
    return relation.charAt(0).toUpperCase() + relation.slice(1);
  }

  checkByTypeOfPolice(): boolean {
    return this.policyType === POLICY_TYPES.MD_EPS.code || this._otherPolicies();
  }

  getEmail(insured: IPolicyInsuredResponse): string {
    return insured.correoElectronico || GeneralLang.Texts.WithOutEmail;
  }

  getPhone(phone): string {
    return phone || GeneralLang.Texts.WithOutPhone;
  }

  edit(insured: IPolicyInsuredResponse, insuredGrouped: any): void {
    this._initFrm(insured);
    this._activeEdit(insured, insuredGrouped, true);
  }

  save(insured: IPolicyInsuredResponse, insuredGrouped: any): void {
    const data = getUpdatedAffiliateEPS(insured, this.frmHolder.value);
    this._updateInsured(data, insured, insuredGrouped);
    this._activeEdit(insured, insuredGrouped, false);
  }

  _activeEdit(insured: IPolicyInsuredResponse, insuredGrouped: any, flagEdit: boolean): void {
    insured.isEdit = flagEdit;
    insuredGrouped.theyAreEditing = flagEdit;
  }

  _updateInsured(data: any, insured: IPolicyInsuredResponse, insuredGrouped: any): void {
    const entity = this.authService.retrieveEntity();
    if (this.policyType === POLICY_TYPES.MD_EPS.code) {
      this._updateInsuredAffilitiesEps(data, entity, insured, insuredGrouped);
    } else if (this._otherPolicies()) {
      this._updateInsuredTron(data, insured, insuredGrouped);
    }
  }

  _otherPolicies(): boolean {
    return [
      POLICY_TYPES.MD_SALUD.code,
      POLICY_TYPES.MD_VIDA.code,
      POLICY_TYPES.MD_DECESOS.code,
      POLICY_TYPES.MD_VIAJES.code
    ].includes(this.policyType);
  }

  _updateInsuredAffilitiesEps(
    data: any,
    entity: IdDocument,
    insured: IPolicyInsuredResponse,
    insuredGrouped: any
  ): void {
    insured.showLoading = true;
    this.autoService
      .ActualizarEpsContratanteAfiliados(
        { nroContrato: this.policyNumber },
        { codigoApp: APPLICATION_CODE, tipoDocumento: entity.type, documento: entity.number, ...data },
        { afiliadosAActualizar: getEPSAffiliateRequest(data) }
      )
      .toPromise()
      .then(resp => {
        insured.showLoading = false;
        insured.correoElectronico = resp[0].correo;
        insured.telefono = resp[0].telefono;
      })
      .catch(err => {
        this.reset(insured, insuredGrouped);
        insured.showLoading = false;
        console.error('catch', err);
      });
  }

  _updateInsuredTron(data: any, insured: IPolicyInsuredResponse, insuredGrouped: any): void {
    insured.showLoading = true;
    const body = {
      codAfiliado: data.codAfiliado,
      telefono: data.telefono,
      correo: data.correo,
      polizas: [
        {
          codCia: this.codCia,
          codRamo: this.codRamo,
          numSpto: this.numSpto,
          numeroPoliza: this.policyNumber,
          tipoPoliza: this.policyType
        }
      ]
    };
    this.autoService.ActualizarAseguradosPoliza({ codigoApp: APPLICATION_CODE }, body).subscribe(
      response => {
        insured.correoElectronico = response.correo;
        insured.telefono = response.telefono;
      },
      () => {
        this.reset(insured, insuredGrouped);
        insured.showLoading = false;
      },
      () => (insured.showLoading = false)
    );
  }

  reset(insured: IPolicyInsuredResponse, insuredGrouped: any): void {
    this._activeEdit(insured, insuredGrouped, false);
  }

  private _initFrm(data): void {
    this.frmHolder = this._FrmProvider.AffiliateComponent([data]);
  }

  // Utils to edit insureds
  private getPropsForUI(a): any {
    return {
      ...a,
      frmNameEmail: `email_${a.documento}`,
      frmNamePhone: `phone_${a.documento}`,
      uiEmail: this.getLabelWithoutInfo(a.correoElectronico),
      uiPhone: this.getLabelWithoutInfo(a.telefono)
    };
  }

  private getLabelWithoutInfo(data): string {
    return data || 'Sin información';
  }
}
