import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputWithIconModule } from '@mx/components/shared';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule, MfCardModule, MfInputModule, MfShowErrorsModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { LinksModule } from '@mx/core/ui/lib/links/links.module';
import { ScrollService } from '@mx/services/scroll.service';
import { CardPolicyInsuredGroupedComponent } from './card-policy-insured-grouped.component';

@NgModule({
  imports: [
    CommonModule,
    DirectivesModule,
    GoogleModule,
    MfCardModule,
    MfInputModule,
    MfShowErrorsModule,
    MfLoaderModule,
    LinksModule,
    FormsModule,
    ReactiveFormsModule,
    InputWithIconModule
  ],
  declarations: [CardPolicyInsuredGroupedComponent],
  exports: [CardPolicyInsuredGroupedComponent],
  providers: [ScrollService]
})
export class CardPolicyInsuredGroupedModule {}
