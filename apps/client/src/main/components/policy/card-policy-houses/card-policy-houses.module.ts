import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { ScrollService } from '@mx/services/scroll.service';
import { CardPolicyHousesComponent } from './card-policy-houses.component';

@NgModule({
  imports: [CommonModule, DirectivesModule, MfCardModule, MfLoaderModule],
  declarations: [CardPolicyHousesComponent],
  exports: [CardPolicyHousesComponent],
  providers: [ScrollService]
})
export class CardPolicyHousesModule {}
