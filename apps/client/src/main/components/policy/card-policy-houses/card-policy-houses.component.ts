import { CurrencyPipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { PoliciesService } from '@mx/services/policies.service';
import { ScrollService } from '@mx/services/scroll.service';
import { APPLICATION_CODE, URL_FRAGMENTS } from '@mx/settings/constants/general-values';
import { HOUSE_ICON } from '@mx/settings/constants/images-values';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import { IItemHousePolicyView, IPolicyHousesResponse } from '@mx/statemanagement/models/policy.interface';

@Component({
  selector: 'client-card-policy-houses',
  templateUrl: './card-policy-houses.component.html',
  providers: [CurrencyPipe]
})
export class CardPolicyHousesComponent implements OnInit {
  @Input() policyNumber: string;
  @Input() isPPFM: boolean;

  titleCard: string;
  iconCard = HOUSE_ICON;
  collapse = false;
  listHouses: Array<IItemHousePolicyView>;
  policyLang = PolicyLang;
  insuredFragment = URL_FRAGMENTS.INSURED;
  showLoading: boolean;

  constructor(
    private readonly policiesService: PoliciesService,
    private readonly scrollService: ScrollService,
    public currencyPipe: CurrencyPipe
  ) {
    this.titleCard = this.policyLang.Titles.DetailMyHome;
  }

  ngOnInit(): void {
    this.listHouses = [];
    this.showLoading = true;
    const searchByClientSub = this.policiesService
      .houses({
        codigoApp: APPLICATION_CODE,
        numeroPoliza: this.policyNumber
      })
      .subscribe(
        (response: Array<IPolicyHousesResponse>) => {
          if (response && response.length) {
            this.listHouses = response.map((house: IPolicyHousesResponse) => {
              const symbolContent = `${StringUtil.getMoneyDescription(house.codigoMonedaContenido)} `;
              const symbolBuild = `${StringUtil.getMoneyDescription(house.codigoMonedaEdificacion)} `;

              return {
                yearContruction: house.anioConstruccion,
                department: house.departamentoDescripcion,
                province: house.provinciaDescripcion,
                district: house.distritoDescripcion,
                address: house.direccion,
                homeType: house.tipoHogarDescripcion,
                contentValue: this.currencyPipe.transform(house.valorContenido, symbolContent),
                buildingValue: this.currencyPipe.transform(house.valorEdificacion, symbolBuild)
              } as IItemHousePolicyView;
            });
          } else {
            this.listHouses = [];
          }
          this.showLoading = false;
          this.scrollService.checkScrollTo(this.insuredFragment);
          searchByClientSub.unsubscribe();
        },
        () => {
          this.showLoading = false;
        }
      );
  }

  getAddress(house: IItemHousePolicyView): string {
    return `${house.address}, ${house.district}, ${house.province}, ${house.department}`;
  }
}
