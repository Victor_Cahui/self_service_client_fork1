import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { getDDMMYYYY } from '@mx/core/shared/helpers/util/date';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { CALENDAR_ICON, FILE_ICON, MPD_RENEWAL_ICON } from '@mx/settings/constants/images-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import { IEpsContractCard } from '@mx/statemanagement/models/client.models';
import { IPoliciesByClientResponse, IPolicyBasicInfo } from '@mx/statemanagement/models/policy.interface';

@Component({
  selector: 'client-card-eps-basic-info',
  templateUrl: './card-eps-basic-info.component.html'
})
export class CardEpsBasicInfoComponent implements OnInit {
  @HostBinding('attr.class') attr_class = 'w-10';

  @Input() info?: IPolicyBasicInfo;

  withOutInformation = GeneralLang.Texts.WithOutInformation;
  calendarIcon = CALENDAR_ICON;
  shieldIcon = MPD_RENEWAL_ICON;
  collapse = false;
  policyNumber: string;
  epsContract: IEpsContractCard;
  clientPolicy: IPoliciesByClientResponse;
  showLoading: boolean;
  noContent: boolean;

  constructor(
    private readonly policiesInfoService: PoliciesInfoService,
    private readonly activePath: ActivatedRoute,
    private readonly _AuthService: AuthService,
    private readonly _Autoservicios: Autoservicios
  ) {}

  ngOnInit(): void {
    this.getClientContract();
    this.getContractCardInfo();
  }

  getClientContract(): void {
    this.policyNumber = this.activePath.snapshot.params['policyNumber'];
    this.clientPolicy = this.policiesInfoService.getClientPolicy(this.policyNumber);
  }

  getContractCardInfo(): void {
    this.showLoading = true;
    const pathParams = {
      nroContrato: this.policyNumber
    };
    const queryParams = {
      codigoApp: APPLICATION_CODE
    };
    this._Autoservicios.ObtenerEpsContratanteDatosCardContrato(pathParams, queryParams).subscribe(
      response => (response ? (this.epsContract = this._formatEpsContractCard(response)) : (this.noContent = true)),
      error => {
        console.error(error);
      },
      () => (this.showLoading = false)
    );
  }

  private _formatEpsContractCard(response: any): IEpsContractCard {
    return {
      icon: FILE_ICON,
      title: response.nombre || this.withOutInformation,
      number: `${PolicyLang.Texts.NumberContract}: ${response.numeroContrato || this.withOutInformation}`,
      vigencyStart: `${PolicyLang.Texts.VigencyStart}: ${getDDMMYYYY(response.inicioVigencia)}`,
      lastRenewal: response.ultimaRenovacion
        ? `${PolicyLang.Texts.LastRenewal}: ${getDDMMYYYY(response.ultimaRenovacion)}`
        : '',
      vigencyEnd: `${PolicyLang.Texts.VigencyEnd}: ${getDDMMYYYY(response.finVigencia)}`
    };
  }
}
