import { CurrencyPipe } from '@angular/common';
import { Component, HostBinding, Input, OnDestroy, OnInit } from '@angular/core';
import { PolicyUtil } from '@mx/components/shared/utils/policy.util';
import { ClientService } from '@mx/services/client.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import {
  CALENDAR_ICON,
  COIN_ICON,
  FILE_ICON,
  PORCENTAGE_ICON,
  SHIELD_MORE_ICON
} from '@mx/settings/constants/images-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { TOOLTIPS_HEALTH } from '@mx/settings/lang/health.lang';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import { Content } from '@mx/statemanagement/models/general.models';
import { IPolicyBasicInfo } from '@mx/statemanagement/models/policy.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-card-policy-basic-info',
  templateUrl: './card-policy-basic-info.component.html',
  providers: [CurrencyPipe]
})
export class CardPolicyBasicInfoComponent implements OnInit, OnDestroy {
  @HostBinding('attr.class') attr_class = 'w-10';
  @Input() info?: IPolicyBasicInfo;
  @Input() isPPFM: boolean;
  @Input() mpKeys: any;

  cardIcon = FILE_ICON;
  coinIcon = COIN_ICON;
  calendarIcon = CALENDAR_ICON;
  shieldIcon = SHIELD_MORE_ICON;
  porcentageIcon = PORCENTAGE_ICON;
  collapse = false;
  isEps: boolean;
  isSctr: boolean;
  isSctrDoubleEmission: boolean;
  frequency: string;
  plan: string;
  toolTips: Content;
  policyTypes = POLICY_TYPES;
  policyBasicInfoSub: Subscription;
  titleMyRisk = PolicyLang.Labels.ActivityRisk;
  hiddenFieldsByPersonClient: boolean;

  constructor(
    private readonly policiesInfoService: PoliciesInfoService,
    private readonly currencyPipe: CurrencyPipe,
    private readonly clientService: ClientService
  ) {}

  ngOnInit(): void {
    this.policyBasicInfoSub = this.policiesInfoService.getPolicyBasicInfo().subscribe((result: IPolicyBasicInfo) => {
      this.info = result;
      this.isEps = PolicyUtil.isEps(this.info.policyType);
      this.isSctr = this.info && this.info.policyType && PolicyUtil.isSctr(this.info.policyType);
      this.isSctrDoubleEmission = PolicyUtil.isSctrDoubleEmission(this.info.policyNumber);
      this.frequency = this.isSctr && this.getFrequency();
      this.plan = `${PolicyLang.Labels.Plan}: ${
        result.planEps ? result.planEps : GeneralLang.Texts.WithOutInformation
      }`;
      // Tooltips en suma asegurada solo para salud
      if (
        this.info &&
        (this.info.policyType === POLICY_TYPES.MD_SALUD.code || this.info.policyType === POLICY_TYPES.MD_EPS.code)
      ) {
        this.toolTips = TOOLTIPS_HEALTH;
      }

      this.hiddenFieldsByPersonClient = this.clientService.isGroupTypePerson() && this.isSctr;
    });
    this.isSctr = this.info && this.info.policyType && PolicyUtil.isSctr(this.info.policyType);
  }

  titleCard(): string {
    return (this.info && this.info.policyName) || '';
  }

  policyLabel(): string {
    const label = this.isEps ? PolicyLang.Texts.NumberContract : PolicyLang.Texts.NumberPolicy;

    return `${label} ${(this.info && this.info.policyNumber) || ''}`;
  }

  policyLabels(): string {
    const policyNumbers = this.info.policyNumber.split(',');
    const label = `
      ${PolicyLang.Texts.NumberPolicy} salud: ${policyNumbers[0]}<br>
      ${PolicyLang.Texts.NumberPolicy} pensión: ${policyNumbers[1]}`;

    return label;
  }

  activityRisk(): string {
    if (this.info && this.info.activityRisk) {
      return `${PolicyLang.Labels.ActivityRisk}: ${(this.info && this.info.activityRisk[0].actRiesgoDescripcion) ||
        ''}`;
    }

    return '';
  }

  sumAssured(): string {
    return this.hiddenSumAssured()
      ? ''
      : `${PolicyLang.Texts.InsuredRate}:
      ${this.info &&
        this.info.sumAssured &&
        this.currencyPipe.transform(this.info.sumAssured, `${this.info.moneyCode} `)}`;
  }

  sumAssuredCoveragesPpfm(coverage): any {
    return `${coverage &&
      coverage.sumaAsegurada &&
      this.currencyPipe.transform(coverage.sumaAsegurada, `${this.info.moneyCode} `)} ${coverage.descripcion}`;
  }

  startDateValidity(): string {
    return `${PolicyLang.Labels.StartValidityDate}:
     ${(this.info && this.info.startDateValidity) || ''}`;
  }

  endValidityDate(): string {
    return `${PolicyLang.Labels.EndValidityDate}:
      ${(this.info && this.info.endValidityDate) || ''}`;
  }

  ngOnDestroy(): void {
    if (this.policyBasicInfoSub) {
      this.policyBasicInfoSub.unsubscribe();
    }
  }

  hiddenDateValidity(): boolean {
    if (this.isPPFM) {
      return true;
    }

    if (
      (this.info.policyType === POLICY_TYPES.MD_SALUD.code &&
        this.policiesInfoService.getClientPolicy(this.info.policyNumber).esPolizaVip) ||
      !this.info.endValidityDate
    ) {
      // se oculta la fecha de fin de vigencia para polizas VIP (solo hay polizas VIP en el ramo MD_SALUD)
      return true;
    }

    return this.info && this.info.policyType === POLICY_TYPES.MD_EPS.code;
  }

  hiddenSumAssured(): boolean {
    if (!this.info) {
      return false;
    }

    if ([POLICY_TYPES.MD_SCTR.code, POLICY_TYPES.MD_DECESOS.code].includes(this.info.policyType)) {
      return true;
    }

    return (
      [POLICY_TYPES.MD_AUTOS.code, POLICY_TYPES.MD_HOGAR.code].includes(this.info.policyType) &&
      this.info.insuredNumber > 1
    );
  }

  getFrequency(): string {
    return (
      this.info &&
      ` ${PolicyLang.Texts.Frequency}: ${
        this.info.frequencyStatement ? this.info.frequencyStatement : GeneralLang.Texts.WithOutInformation
      }`
    );
  }
}
