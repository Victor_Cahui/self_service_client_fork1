import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DropdownModule } from '@mx/components/shared/dropdown/dropdown.module';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';
import { TooltipsModule } from '@mx/core/ui/lib/components/tooltips/tooltips.module';
import { CardPolicyBasicInfoComponent } from './card-policy-basic-info.component';

@NgModule({
  imports: [CommonModule, DirectivesModule, MfCardModule, TooltipsModule, DropdownModule],
  declarations: [CardPolicyBasicInfoComponent],
  exports: [CardPolicyBasicInfoComponent]
})
export class CardPolicyBasicInfoModule {}
