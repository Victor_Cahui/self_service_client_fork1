import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule, MfCardModule, MfInputModule, MfShowErrorsModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { LinksModule } from '@mx/core/ui/lib/links/links.module';
import { ScrollService } from '@mx/services/scroll.service';
import { InputWithIconModule } from './../../shared/inputs/input-with-icon/input-with-icon.module';
import { CardPolicyInsuredPPFMComponent } from './card-policy-insured-ppfm.component';

@NgModule({
  imports: [
    CommonModule,
    DirectivesModule,
    GoogleModule,
    MfCardModule,
    MfLoaderModule,
    LinksModule,
    InputWithIconModule,
    MfInputModule,
    MfShowErrorsModule,
    ReactiveFormsModule
  ],
  declarations: [CardPolicyInsuredPPFMComponent],
  exports: [CardPolicyInsuredPPFMComponent],
  providers: [ScrollService]
})
export class CardPolicyInsuredPPFMModule {}
