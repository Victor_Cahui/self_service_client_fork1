import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { CardContractingInfoComponent } from './card-contracting-info.component';

@NgModule({
  imports: [CommonModule, DirectivesModule, MfCardModule, MfLoaderModule],
  declarations: [CardContractingInfoComponent],
  exports: [CardContractingInfoComponent]
})
export class CardContractingInfoModule {}
