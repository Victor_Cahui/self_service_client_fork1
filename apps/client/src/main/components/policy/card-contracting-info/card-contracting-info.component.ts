import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import * as ICONS from '@mx/settings/constants/images-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { TYPE_DOCUMENT_RUC } from '@mx/settings/constants/types-documents';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { ProfileLang } from '@mx/settings/lang/profile.lang';
import { IContractorCard } from '@mx/statemanagement/models/client.models';
import { IPoliciesByClientResponse, ISctrByClientResponse } from '@mx/statemanagement/models/policy.interface';

@Component({
  selector: 'client-card-contracting-info',
  templateUrl: './card-contracting-info.component.html'
})
export class CardContractingInfoComponent implements OnInit {
  @HostBinding('attr.class') attr_class = 'w-10';
  @Input() policyType: string;

  withOutInformation = GeneralLang.Texts.WithOutInformation;
  profileLang = ProfileLang;
  ICONS = ICONS;
  collapse = false;
  policyNumber: string;
  contractor: IContractorCard;
  showLoading: boolean;
  noContent: boolean;
  clientPolicy: IPoliciesByClientResponse | ISctrByClientResponse;

  constructor(
    private readonly policiesInfoService: PoliciesInfoService,
    private readonly activePath: ActivatedRoute,
    private readonly _Autoservicios: Autoservicios
  ) {}

  ngOnInit(): void {
    this.getClientPolicy();
    this.getContractorInfo();
  }

  getClientPolicy(): void {
    this.policyNumber = this.activePath.snapshot.params['policyNumber'];
    this.clientPolicy =
      this.policyType === POLICY_TYPES.MD_SCTR.code
        ? this.policiesInfoService.getClientSctrPolicy(this.policyNumber)
        : this.policiesInfoService.getClientPolicy(this.policyNumber);
  }

  getContractorInfo(): void {
    this.showLoading = true;
    const pathParams = {
      numeroPoliza: this.policyNumber
    };
    const queryParams = {
      codigoApp: APPLICATION_CODE,
      codCia: this.clientPolicy.codCia
    };
    this._Autoservicios.ObtenerDatosContratante(pathParams, queryParams).subscribe(
      response => (response ? (this.contractor = this._formatContractorInfoCard(response)) : (this.noContent = true)),
      error => {
        console.error(error);
      },
      () => (this.showLoading = false)
    );
  }

  private _formatContractorInfoCard(response: any): IContractorCard {
    const icon =
      response.tipoDocumento === TYPE_DOCUMENT_RUC && response.numeroDocumento.startsWith('20')
        ? this.ICONS.BUILDING_ICON
        : this.ICONS.PROFILE_ICON;

    return {
      icon,
      title: this.profileLang.Titles.Contractor,
      contractor: response.razonSocial ? response.razonSocial : response.nombreCompleto,
      document: `${response.tipoDocumento}: ${response.numeroDocumento}`,
      phone: `${this.profileLang.Labels.Phone}: ${response.telefono || this.withOutInformation}`,
      address: `${this.profileLang.Labels.Address}: ${response.direccion || this.withOutInformation}`
    };
  }
}
