import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google/google.gabase';
import { IDEA_ICON } from '@mx/settings/constants/images-values';
import { IBannerNotification } from '@mx/statemanagement/models/general.models';

@Component({
  selector: 'client-banner-payment-configuration',
  templateUrl: './banner-payment-configuration.component.html'
})
export class BannerPaymentConfigurationComponent extends GaUnsubscribeBase implements OnInit {
  @ViewChild('iconNotification') iconNotification: ElementRef;

  @Input() bannerData: IBannerNotification;
  @Output() bannerAction: EventEmitter<any>;

  constructor(protected gaService?: GAService) {
    super(gaService);
    this.bannerAction = new EventEmitter<any>();
  }

  ngOnInit(): void {
    this.iconNotification.nativeElement.classList.add(this.bannerData.icon || IDEA_ICON);
  }

  doBannerAction(): void {
    this.bannerAction.next();
  }
}
