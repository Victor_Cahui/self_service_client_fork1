import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { MfModalMessageComponent } from '@mx/core/ui/lib/components/modals/modal-message/modal-message.component';
import { PaymentService } from '@mx/services/payment.service';
import { PolicyListService } from '@mx/services/policy/policy-list.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { PAYMENT_SETTINGS_RENEWAL } from '@mx/settings/lang/payment.lang';
import { IPaymentConfigurationResponse, IPaymentRenewalRequest } from '@mx/statemanagement/models/payment.models';
import { ICardPoliciesView, IPolicyList } from '@mx/statemanagement/models/policy.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-setting-renewal-change',
  templateUrl: 'setting-renewal-change.component.html'
})
export class SettingRenewalChangeComponent extends GaUnsubscribeBase implements OnInit, OnDestroy {
  @Input() payment: IPaymentConfigurationResponse;

  @ViewChild(MfModalMessageComponent) mfModalConfirm: MfModalMessageComponent;

  renewalLang = PAYMENT_SETTINGS_RENEWAL.options;
  generalLang = GeneralLang;

  paymentServiceSub: Subscription;
  editing: boolean;
  currentSelection: boolean;
  currentMPD: boolean;
  isAutomatic: boolean;
  useMPD: boolean;
  showModal: boolean;
  disableConfirm: boolean;
  showLoading: boolean;

  constructor(
    private readonly paymentService: PaymentService,
    private readonly policyListService: PolicyListService,
    protected gaService?: GAService
  ) {
    super(gaService);
  }

  ngOnInit(): void {
    this.setCurrentValues();
  }

  setCurrentValues(): void {
    this.currentSelection = this.isAutomatic = this.payment.tipoRenovacionId === 1;
    this.currentMPD = this.useMPD = this.payment.usarMapfreDolares;
    this.disableConfirm = true;
  }

  onEdit(value: boolean): void {
    this.editing = value;
    if (!this.editing) {
      this.setCurrentValues();
    }
  }

  onAutomaticSelection(): void {
    if (!this.isAutomatic) {
      this.updateValues(true, false, this.currentMPD);
    }
  }

  onManualSelection(): void {
    if (this.isAutomatic) {
      this.updateValues(false, false, false);
    }
  }

  updateValues(auto: boolean, disable: boolean, mpd: boolean): void {
    this.isAutomatic = auto;
    this.disableConfirm = disable;
    this.useMPD = mpd;
    this.setDisableConfirm();
  }

  toogleMPD(e): void {
    this.useMPD = e.target.checked;
    this.setDisableConfirm();
  }

  setDisableConfirm(): void {
    this.disableConfirm = this.isAutomatic === this.currentSelection && this.useMPD === this.currentMPD;
  }

  onConfirm(): void {
    this.showLoading = true;
    const params = {
      codigoApp: APPLICATION_CODE
    };
    const body: IPaymentRenewalRequest = {
      numeroPoliza: this.payment.numeroPoliza,
      tipoRenovacionId: this.isAutomatic ? 1 : 2,
      usarMapfreDolares: this.useMPD
    };

    this.paymentServiceSub = this.paymentService.paymentConfigurationRenewal(body, params).subscribe(
      () => {
        this.showModal = true;
        this.showLoading = false;
        setTimeout(() => {
          this.mfModalConfirm.open();
        });

        this.payment.tipoRenovacionId = this.isAutomatic ? 1 : 2;
        this.payment.tipoRenovacionDescripcion = this.isAutomatic
          ? this.renewalLang.automatic.title
          : this.renewalLang.manual.title;
        this.paymentService.updatePaymenConfiguration(this.payment);

        // Actualizar 'Tipo de renovación' en el card de pagos (si viene de ahi)
        const policyList: IPolicyList = this.policyListService.getPolicyListLocal();
        if (policyList) {
          const policy = policyList.list.find((p: ICardPoliciesView) => p.policyNumber === this.payment.numeroPoliza);
          if (policy) {
            policy.response.tipoRenovacionId = this.payment.tipoRenovacionId;
            policy.response.tipoRenovacionDescripcion = this.payment.tipoRenovacionDescripcion;
            this.policyListService.setPolicyListData(policyList);
          }
        }

        this.paymentServiceSub.unsubscribe();
      },
      err => {
        this.paymentServiceSub.unsubscribe();
        this.showLoading = false;
      }
    );
  }

  confirmResponse(): void {
    this.editing = false;
    this.currentSelection = this.isAutomatic;
    this.currentMPD = this.useMPD;
    this.disableConfirm = true;
  }

  ngOnDestroy(): void {
    if (this.paymentServiceSub) {
      this.paymentServiceSub.unsubscribe();
    }
  }
}
