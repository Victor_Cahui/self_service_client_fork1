import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { MfModalMessageModule } from '@mx/core/ui';
import { MfButtonModule } from '@mx/core/ui/lib/components/forms/button';
import { SettingRenewalChangeComponent } from './setting-renewal-change.component';

@NgModule({
  imports: [CommonModule, MfButtonModule, MfModalMessageModule, GoogleModule],
  exports: [SettingRenewalChangeComponent],
  declarations: [SettingRenewalChangeComponent],
  providers: []
})
export class SettingRenewalChangeModule {}
