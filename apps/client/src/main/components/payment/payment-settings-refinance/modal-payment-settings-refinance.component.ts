import { DatePipe } from '@angular/common';
import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { NotificationStatus } from '@mx/core/ui/lib/components/alerts/custom-alert/custom-alert.component';
import { MfModal } from '@mx/core/ui/lib/components/modals/modal/public-api';
import { AuthService } from '@mx/services/auth/auth.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PaymentService } from '@mx/services/payment.service';
import { APPLICATION_CODE, FORMAT_DATE_DDMMYYYY } from '@mx/settings/constants/general-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { PAYMENT_SETTINGS_QUOTA } from '@mx/settings/lang/payment.lang';
import {
  IPaymentRefinancingRequestRequest,
  IRefinancingQuotaResponse
} from '@mx/statemanagement/models/payment.models';
import { Subscription } from 'rxjs';
import { BasePaymentSettings } from '../card-payments-setting/base-payments-settings';

@Component({
  selector: 'client-modal-payment-settings-refinance',
  templateUrl: 'modal-payment-settings-refinance.component.html'
})
export class PaymentSettingsRefinanceComponent extends BasePaymentSettings implements OnInit {
  @ViewChild(MfModal) mfModal: MfModal;

  @Output() showRefinancing: EventEmitter<any>;

  quotasLang = PAYMENT_SETTINGS_QUOTA;
  confirmButtonText = GeneralLang.Buttons.Confirm;

  selectedIndex: number;
  refinanceAmount: string;
  paymentRefinancingRequestSub: Subscription;
  selectedQuota: IRefinancingQuotaResponse;
  refinancingAmount: string;
  refinancingFrecuency: string;
  warningRefinancing: string;
  status = NotificationStatus.INFO;

  constructor(
    protected paymentService: PaymentService,
    protected authService: AuthService,
    protected datePipe: DatePipe,
    protected activatedRoute: ActivatedRoute,
    protected headerHelperService: HeaderHelperService,
    protected gaService: GAService
  ) {
    super(paymentService, authService, activatedRoute, headerHelperService, datePipe, gaService);
    this.showRefinancing = new EventEmitter<any>();
  }

  ngOnInit(): void {
    this.getRefinancingQuotas();
  }

  onSelected(data: IRefinancingQuotaResponse, enabled: boolean): void {
    if (data && enabled) {
      this.selectedIndex = data ? this.selectedIndex : null;
      this.selectedQuota = data;
      this.updateRefinancingPayment();
    }
  }

  onLoadRefinancing(): void {
    this.refinanceAmount = `
      ${this.quotasLang.labels.refinance}
      ${StringUtil.getMoneyDescription(this.refinancing.codigoMonedaMontoRefinanciar)}
      ${this.refinancing.montoRefinanciar}
      ${this.quotasLang.labels.in}:`;
    if (!!this.refinancing.fechaUltimoRefinanciamiento) {
      const date = this.datePipe.transform(this.refinancing.fechaUltimoRefinanciamiento, FORMAT_DATE_DDMMYYYY);
      this.warningRefinancing = `${this.quotasLang.warnings.refinancedReminder} ${date}`;
    }
    this.updateRefinancingPayment();
  }

  updateRefinancingPayment(): void {
    this.refinancingFrecuency = this.selectedQuota
      ? this.selectedQuota.descripcionFrecuenciaPago
      : this.refinancing.cuotaVigente.descripcionFrecuenciaPago;
    this.refinancingAmount = this.selectedQuota
      ? this.setPaymentFormat(this.selectedQuota.codigoMonedaMonto, this.selectedQuota.montoCuota)
      : this.setPaymentFormat(
          this.refinancing.cuotaVigente.codigoMonedaMonto,
          this.refinancing.cuotaVigente.montoCuota
        );
  }

  getInterestLabel(quota: IRefinancingQuotaResponse): string {
    return !quota.permiteRefinanciar
      ? this.quotasLang.labels.noRefinance
      : quota.tieneInteres
      ? `${quota.interes}% ${this.quotasLang.labels.interest}`
      : this.quotasLang.labels.noInterest;
  }

  open(): void {
    this.mfModal.open();
  }

  closeEvent(b: boolean): void {
    if (!b) {
      const btn = this.ga.find(fv => fv.control === 'btnModalPaymentSettingRefinance') || {};
      this.addGAEvent(btn, { lbl: 'cancelar' });
    }
    this.selectedQuota = this.selectedIndex = null;
  }

  onSend(confirm: boolean): void {
    const btn = this.ga.find(fv => fv.control === 'btnModalPaymentSettingRefinance') || {};
    this.addGAEvent(btn, { lbl: this.confirmButtonText });
    if (confirm) {
      const params = {
        codigoApp: APPLICATION_CODE,
        numeroPoliza: this.activatedRoute.snapshot.paramMap.get('policyNumber')
      };
      const body: IPaymentRefinancingRequestRequest = {
        cuotaActual: this.refinancing.cuotaVigente.cuota,
        montoRefinanciar: this.refinancing.montoRefinanciar,
        codigoMonedaMontoRefinanciar: this.refinancing.codigoMonedaMontoRefinanciar,
        tipoCuotaNueva: this.selectedQuota.tipoCuotaNueva,
        tieneInteres: this.selectedQuota.tieneInteres,
        interes: this.selectedQuota.interes,
        montoCuota: this.selectedQuota.montoCuota,
        codigoMonedaMonto: this.selectedQuota.codigoMonedaMonto
      };

      this.paymentRefinancingRequestSub = this.paymentService
        .paymentConfigurationRefinancingRequest(body, params)
        .subscribe(
          res => {
            if (res) {
              this.mfModal.close(true);
              this.showRefinancing.next();
            }
          },
          err => {
            this.paymentRefinancingRequestSub.unsubscribe();
          }
        );
    }
  }
}
