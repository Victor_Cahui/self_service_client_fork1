import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MfCustomAlertModule } from '@mx/core/ui/lib/components/alerts/custom-alert';
import { MfModalModule } from '@mx/core/ui/lib/components/modals/modal/modal.module';
import { PaymentSettingsRefinanceComponent } from './modal-payment-settings-refinance.component';

@NgModule({
  imports: [CommonModule, MfModalModule, FormsModule, MfCustomAlertModule],
  exports: [PaymentSettingsRefinanceComponent],
  declarations: [PaymentSettingsRefinanceComponent]
})
export class PaymentSettingsRefinanceModule {}
