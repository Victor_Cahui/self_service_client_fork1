import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CardItemDetailModule } from '@mx/components/shared/card-item-detail/card-item-detail.module';
import { IconPoliciesModule } from '@mx/components/shared/icon-policies/icon-polices.module';
import { BankPaymentModule } from '@mx/components/shared/payments/bank-payment/bank-payment.module';
import { WithoutPendingPaymentModule } from '@mx/components/shared/without-pending-payment/without-pending-payment.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule, MfCardModule, MfInputModule } from '@mx/core/ui';
import { MfButtonModule } from '@mx/core/ui/lib/components/forms/button';
import { MfSelectModule } from '@mx/core/ui/lib/components/forms/select';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { MfModalModule } from '@mx/core/ui/lib/components/modals/modal';
import { LinksModule } from '@mx/core/ui/lib/links/links.module';
import { PaymentButtonModule } from '../payment-button/payment-button.module';
import { CardPaymentsDetailComponent } from './card-payments-detail.component';

@NgModule({
  imports: [
    CommonModule,
    DirectivesModule,
    GoogleModule,
    FormsModule,
    IconPoliciesModule,
    LinksModule,
    MfButtonModule,
    MfCardModule,
    MfInputModule,
    MfLoaderModule,
    MfSelectModule,
    PaymentButtonModule,
    ReactiveFormsModule,
    WithoutPendingPaymentModule,
    CardItemDetailModule,
    MfModalModule,
    BankPaymentModule
  ],
  declarations: [CardPaymentsDetailComponent],
  exports: [CardPaymentsDetailComponent]
})
export class CardPaymentsDetailModule {}
