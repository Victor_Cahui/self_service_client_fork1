import { CurrencyPipe } from '@angular/common';
import { POLICY_QUOTA_TYPE } from '@mx/components/shared/utils/policy';
import { PolicyUtil } from '@mx/components/shared/utils/policy.util';
import { FileUtil } from '@mx/core/shared/helpers/util/file';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google/google.gabase';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { PERSON_ICON } from '@mx/settings/constants/images-values';
import { POLICY_TYPES as PT } from '@mx/settings/constants/policy-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import {
  ICardPaymentDetailView,
  IPaymentItemDetailView,
  IPaymentResponse
} from '@mx/statemanagement/models/payment.models';
import { isEmpty } from 'lodash-es';

export abstract class BasePayments extends GaUnsubscribeBase {
  isVisibleDownloadSpinner: boolean;
  fileUtil: FileUtil;
  insuredTypes = GeneralLang.Titles.InsuredTypes;

  constructor(
    protected _Autoservicios: Autoservicios,
    protected authService: AuthService,
    protected currencyPipe: CurrencyPipe,
    protected gaService?: GAService
  ) {
    super(gaService);
  }

  downloadAccountStatus(): void {
    const auth = this.authService.retrieveEntity();
    this.isVisibleDownloadSpinner = true;
    this._Autoservicios
      .DescargarEcu({
        codigoApp: APPLICATION_CODE
      })
      .subscribe(
        (res: any) => {
          this.isVisibleDownloadSpinner = false;

          try {
            this.fileUtil.download(res.base64, 'pdf', StringUtil.slugify(`Estado de cuenta - ${auth.number}`));
          } catch (e) {
            console.error(e);
          }
        },
        () => {
          this.isVisibleDownloadSpinner = false;
        }
      );
  }

  protected setPaymentReceiptList(payment: IPaymentResponse): Array<IPaymentItemDetailView> {
    const isNotice = payment.tipoDocumentoPago === POLICY_QUOTA_TYPE.AVISO;
    const isInvoice = payment.tipoDocumentoPago === POLICY_QUOTA_TYPE.FACTURA;
    const noticeData = payment.listaRecibosAsociados;
    const receiptData = [payment.datosPoliza || {}];
    const invoiceData = [payment];
    const data: any = isNotice ? noticeData : isInvoice ? invoiceData : receiptData;
    const isUnique = data.length === 1;
    const paymentReceiptArray: Array<IPaymentItemDetailView> = [];

    data.forEach(item => {
      const policyType = item.tipoPoliza;
      const customIcon = isInvoice ? PERSON_ICON : null;
      const descripcionPoliza = isInvoice ? item.glosa : item.descripcionPoliza;
      const descripcionPolizaDetalle = this.getDescripcionPolizaLabel(policyType, item, isNotice);
      const symbol = `${StringUtil.getMoneyDescription(item.codigoMoneda)} `;
      const totalAmount = this.currencyPipe.transform(item.montoTotal, symbol);

      paymentReceiptArray.push({
        tipoPoliza: PolicyUtil.verifyPolicyType(item),
        customIcon,
        isInvoice,
        descripcionPoliza,
        descripcionPolizaDetalle,
        numeroPoliza: item.numeroPoliza,
        totalAmount: isNotice && !isUnique ? totalAmount : null
      });
    });

    return paymentReceiptArray;
  }

  private getDescripcionPolizaLabel(policyType: string, receipts: any, isNotice: boolean): string {
    let label = GeneralLang.Texts.WithOutInformation;
    let risks = [];

    if ([PT.MD_AUTOS.code, PT.MD_SOAT.code, PT.MD_SOAT_ELECTRO.code, PT.MD_HOGAR.code].includes(policyType)) {
      risks = isNotice ? receipts.datosPoliza.listaRiesgos : receipts.listaRiesgos;
      label = isEmpty(risks)
        ? label
        : risks.length > 1
        ? `${risks.length} ${
            policyType === PT.MD_HOGAR.code ? this.insuredTypes.Households : this.insuredTypes.Vehicles
          }`
        : `${risks[0].descripcion}`;
    }

    if (
      [PT.MD_SALUD.code, PT.MD_VIAJES.code, PT.MD_DECESOS.code, PT.MD_EPS.code, PT.MD_VIDA.code].includes(policyType)
    ) {
      risks = isNotice ? receipts.datosPoliza.listaAsegurados : receipts.listaAsegurados;
      if (!isEmpty(risks)) {
        label =
          risks.length > 1
            ? `${risks.length} ${this.insuredTypes.Insureds}`
            : `${risks[0].nombre} ${risks[0].apellidoPaterno} ${risks[0].apellidoMaterno}`;
      }
    }

    if (PT.MD_SCTR.code === policyType) {
      risks = isNotice ? receipts.datosPoliza.listaConstancias : receipts.listaConstancias;
      if (!isEmpty(risks)) {
        label = risks.length > 1 ? `${risks.length} ${this.insuredTypes.Constancies}` : `${risks[0].numeroConstancia}`;
      }
    }

    return label;
  }

  collapseDetail(item: ICardPaymentDetailView): void {
    item.collapse = !item.collapse;
  }
}
