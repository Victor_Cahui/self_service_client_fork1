import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BankPaymentModule } from '@mx/components/shared/payments/bank-payment/bank-payment.module';
import { WithoutPendingPaymentModule } from '@mx/components/shared/without-pending-payment/without-pending-payment.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { MfModalModule } from '@mx/core/ui/lib/components/modals/modal';
import { LinksModule } from '@mx/core/ui/lib/links/links.module';
import { PaymentButtonModule } from '../payment-button/payment-button.module';
import { CardPendingPaymentComponent } from './card-pending-payment.component';

@NgModule({
  imports: [
    CommonModule,
    DirectivesModule,
    MfCardModule,
    MfModalModule,
    BankPaymentModule,
    LinksModule,
    WithoutPendingPaymentModule,
    MfLoaderModule,
    PaymentButtonModule,
    GoogleModule
  ],
  declarations: [CardPendingPaymentComponent],
  exports: [CardPendingPaymentComponent]
})
export class CardPendingPaymentModule {}
