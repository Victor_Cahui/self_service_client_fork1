import { CurrencyPipe, DatePipe } from '@angular/common';
import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { COVERAGE_STATUS, POLICY_QUOTA_TYPE, TRAFFIC_LIGHT_COLOR } from '@mx/components/shared/utils/policy';
import { isExpired } from '@mx/core/shared/helpers/util/date';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { MfModal } from '@mx/core/ui';
import { environment } from '@mx/environments/environment';
import { AuthService } from '@mx/services/auth/auth.service';
import { GeneralService } from '@mx/services/general/general.service';
import { PaymentService } from '@mx/services/payment.service';
import {
  APPLICATION_CODE,
  COLOR_STATUS,
  FORMAT_DATE_DDMMYYYY,
  SEARCH_ORDEN_ASC
} from '@mx/settings/constants/general-values';
import { WALLET_ICON } from '@mx/settings/constants/images-values';
import { SOAT_ANTIC_RENOVAR_DIAS } from '@mx/settings/constants/key-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { HealthLang } from '@mx/settings/lang/health.lang';
import { PaymentLang } from '@mx/settings/lang/payment.lang';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import { IParameterApp } from '@mx/statemanagement/models/general.models';
import {
  IAssociatedReceipt,
  ICardPaymentPendingView,
  IPaymentResponse
} from '@mx/statemanagement/models/payment.models';
import { Observable } from 'rxjs';

@Component({
  selector: 'client-card-pending-payment',
  templateUrl: './card-pending-payment.component.html',
  providers: [DatePipe, CurrencyPipe]
})
export class CardPendingPaymentComponent extends GaUnsubscribeBase implements OnInit {
  @ViewChild(MfModal) mfModal: MfModal;

  @Output() withoutPendingPayments: EventEmitter<boolean> = new EventEmitter<boolean>();

  labelTitle = PaymentLang.Titles.MyPendingPayment;
  labelViewAll = GeneralLang.Buttons.ViewAll;
  labelFee = PolicyLang.Texts.Fee;
  labelPolicies = PolicyLang.Labels.Policies;
  labelWithOutFees = PolicyLang.Texts.WithOutFees;
  labelPayment = GeneralLang.Buttons.Payment;
  healthLang = HealthLang;
  coverageStatus = COVERAGE_STATUS;
  walletIcon = WALLET_ICON;
  showLoading: boolean;
  showModalPaymentPlaces: boolean;
  collapse = false;
  renewDays: number;

  listPendingPayments: Array<ICardPaymentPendingView>;
  activatePayments = environment.ACTIVATE_PAYMENTS;
  parametersSubject: Observable<Array<IParameterApp>>;

  constructor(
    private readonly paymentService: PaymentService,
    public datePipe: DatePipe,
    public currencyPipe: CurrencyPipe,
    public authService: AuthService,
    public router: Router,
    private readonly generalService: GeneralService,
    protected gaService: GAService
  ) {
    super(gaService);
  }

  ngOnInit(): void {
    this.getParameters();
    this.listPendingPayments = [];
    this.showLoading = true;
    const searchByClientSub = this.paymentService
      .paymentsByClient({
        codigoApp: APPLICATION_CODE,
        criterio: 'fechaExpiracion',
        pSeccion: 'HOME',
        orden: SEARCH_ORDEN_ASC,
        pagina: 1,
        registros: 2
      })
      .subscribe(
        (response: Array<IPaymentResponse>) => {
          if (response) {
            this.listPendingPayments = response.map((payment: IPaymentResponse) => {
              const policy = payment.descripcionPoliza;
              const list = payment.listaRecibosAsociados;
              const expired = isExpired(payment.fechaExpiracion);
              const symbol = `${StringUtil.getMoneyDescription(payment.codigoMoneda)} `;
              const semaforo = TRAFFIC_LIGHT_COLOR[payment.semaforo];
              const coverageStatus = `${payment.estadoCobertura}`;
              const isWithoutCoverage = coverageStatus === this.coverageStatus.SIN_COBERTURA;
              const showQuota = coverageStatus === COVERAGE_STATUS.CON_COBERTURA;
              const isNotice = payment.tipoDocumentoPago === POLICY_QUOTA_TYPE.AVISO;
              const isInvoice = payment.tipoDocumentoPago === POLICY_QUOTA_TYPE.FACTURA;

              const listLength = (list || []).length;
              const isUnique = listLength === 1;
              const noticeLabel = isUnique ? `${list[0].descripcionPoliza}` : `${listLength} ${this.labelPolicies}`;
              const invoiceLabel = `${policy.toUpperCase()}`;
              const receiptLabel = `${policy.toUpperCase()}`;
              const noticeArray = list.map(p => `${p.descripcionPoliza}\nNro. de póliza: ${p.numeroPoliza}`);
              const noticeTooltip = isUnique
                ? `${noticeLabel}\nNro. de póliza: ${list[0].numeroPoliza}\nGrupo de recibos: ${payment.numeroAviso}`
                : `${noticeLabel}\n${noticeArray.join('\n')}\nGrupo de recibos: ${payment.numeroAviso}`;
              // tslint:disable-next-line: max-line-length
              const invoiceTooltip = `${invoiceLabel}\nNro. de factura: ${payment.numRecibo}\nNro. de prefactura: ${payment.nroPreFactura}\nNro. de contrato: ${payment.numeroPoliza}`;
              const receiptTooltip = `${receiptLabel}\nNro. de póliza: ${payment.numeroPoliza}\nNro. de recibo: ${payment.numRecibo}`;
              const paymentLabel = `${isNotice ? noticeLabel : isInvoice ? invoiceLabel : receiptLabel}`;
              const tooltipLabel = `${isNotice ? noticeTooltip : isInvoice ? invoiceTooltip : receiptTooltip}`;

              return {
                paymentLabel,
                tooltipLabel,
                policy,
                policyNumber: payment.numeroPoliza,
                billNumber: payment.numRecibo,
                policyStatusLabel: expired ? PaymentLang.Texts.ExpiredPayment : PaymentLang.Texts.CurrentPayment,
                expired,
                isNotice,
                isInvoice,
                noticeNumber: payment.numeroAviso,
                expiredDate: this.datePipe.transform(payment.fechaExpiracion, FORMAT_DATE_DDMMYYYY),
                totalAmount: this.currencyPipe.transform(payment.montoTotal, symbol),
                quota: payment.cuota,
                policyType: payment.tipoPoliza,
                policyStatus: payment.estado,
                debitRenovation: this.datePipe.transform(payment.fechaDebitoRenovacion, FORMAT_DATE_DDMMYYYY),
                semaforo,
                bulletColor: COLOR_STATUS[semaforo],
                coverageStatus,
                isWithoutCoverage,
                showQuota,
                associatedReceipts: payment.listaRecibosAsociados.map(this._mapAssociatedReceipts.bind(this)),
                response: payment
              } as ICardPaymentPendingView;
            });
          } else {
            this.withoutPendingPayments.next(true);
            this.listPendingPayments = [];
          }

          !this.listPendingPayments.length && this.withoutPendingPayments.next(true);
          this.showLoading = false;
          searchByClientSub.unsubscribe();
        },
        error => {
          this.showLoading = false;
        }
      );
  }

  // Parametros Generales
  getParameters(): void {
    this.parametersSubject = this.generalService.getParametersSubject();
    this.parametersSubject.subscribe(params => {
      this.renewDays = Number(this.generalService.getValueParams(SOAT_ANTIC_RENOVAR_DIAS));
    });
  }

  getDateString(date: string): string {
    return date;
  }

  public myPayments(): void {
    this.router.navigate(['/payments']);
  }

  private _mapAssociatedReceipts(associatedReceipts: IAssociatedReceipt): IAssociatedReceipt {
    const symbol = `${StringUtil.getMoneyDescription(associatedReceipts.codigoMoneda)} `;
    const totalAmount = this.currencyPipe.transform(associatedReceipts.montoTotal, symbol);

    return { ...associatedReceipts, totalAmount };
  }

  openModalPaymentPlaces(): void {
    this.showModalPaymentPlaces = true;
    setTimeout(() => {
      this.mfModal.open();
    });
  }
}
