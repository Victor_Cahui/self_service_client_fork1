import { CurrencyPipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { POLICY_QUOTA_TYPE } from '@mx/components/shared/utils/policy';
import { PolicyUtil } from '@mx/components/shared/utils/policy.util';
import { FileUtil } from '@mx/core/shared/helpers/util/file';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { Autoservicios, FrmProvider } from '@mx/core/shared/providers/services';
import { SelectListItem } from '@mx/core/ui';
import { AuthService } from '@mx/services/auth/auth.service';
import { GeneralService } from '@mx/services/general/general.service';
import {
  APPLICATION_CODE,
  DEFAULT_PAYMENT_RECEIPTS_MONTHS,
  GENERAL_MAX_LENGTH,
  REG_EX,
  SEARCH_ORDEN_DESC,
  STORAGE
} from '@mx/settings/constants/general-values';
import {
  CALENDAR_ICON,
  FILE_ICON as invoiceIcon,
  NO_PENDING_PAYMENTS_ICON,
  NOTICE_ICON as noticeIcon
} from '@mx/settings/constants/images-values';
import { MAX_YEARS_RECEIPTS, TIPO_SEGURO } from '@mx/settings/constants/key-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { HealthLang } from '@mx/settings/lang/health.lang';
import { PaymentLang } from '@mx/settings/lang/payment.lang';
import { PolicyLang } from '@mx/settings/lang/policy.lang';
import { IParameterApp } from '@mx/statemanagement/models/general.models';
import {
  IGroupReceiptMonth,
  IGroupReceiptResponse,
  IMonthReceipt,
  IPaymentsGroupByMonth,
  IPaymentTypeResponse,
  IReceiptView
} from '@mx/statemanagement/models/payment.models';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { Observable, Subscription } from 'rxjs';
import { BasePayments } from '../card-payments-detail/base-payments-detail';
import { SendReceiptComponent } from '../send-receipt/send-receipt.component';

@Component({
  selector: 'client-receipts-list',
  templateUrl: './card-receipts-list.component.html',
  providers: [CurrencyPipe]
})
export class CardReceiptsListComponent extends BasePayments implements OnInit {
  @ViewChild(SendReceiptComponent) modalEmail: SendReceiptComponent;

  maxYears = 5;
  page = 1;
  paymentLang = PaymentLang;
  policyLang = PolicyLang;
  labels = GeneralLang.Labels;
  buttons = GeneralLang.Buttons;
  noticeNumber = PolicyLang.Labels.NoticeNumber;
  billNumber = PolicyLang.Labels.BillNumber;
  invoiceNumber = HealthLang.Labels.InvoiceNumber;
  preInvoiceNumber = HealthLang.Labels.PreInvoiceNumber;
  contractorNumber = PolicyLang.Texts.NumberContract;
  emissionDateLabel = HealthLang.Labels.DateEmission;
  labelPolicy = PolicyLang.Labels.Policy;
  labelPolicies = PolicyLang.Labels.Policies;
  numericRegExp = REG_EX.numeric;
  maxLengthPolice = GENERAL_MAX_LENGTH.numberPolice;
  noPendingPaymentsIcon = NO_PENDING_PAYMENTS_ICON;
  calendarIcon = CALENDAR_ICON;

  auth: IdDocument;
  formFilter: FormGroup;
  listReceiptsOrigin: Array<IReceiptView>;
  listFiltered: Array<IReceiptView> = [];
  listReceiptsGroupByMonth: Array<IPaymentsGroupByMonth>;
  paymentChannelList: Array<any>;
  paymentsTypes: Array<IPaymentTypeResponse>;
  yearList: Array<SelectListItem>;

  insuranceType: AbstractControl;
  paymentChannel: AbstractControl;
  lastYears: AbstractControl;

  receiptSelected: string;
  pdfSub: Subscription;

  showLoading: boolean;
  showFilter: boolean;
  filtered: boolean;
  openEmail: boolean;
  showSpinnerReceipts: boolean;
  showSendBillToEmail: boolean;
  showViewMoreGroups: boolean;
  isLoadingMoreGroups: boolean;
  isLoadingGroupDetail: boolean;

  parametersSubject: Observable<Array<IParameterApp>>;
  isFilteredFrm: boolean;
  listGroupedReceipts: Array<IGroupReceiptMonth>;

  constructor(
    protected authService: AuthService,
    public router: Router,
    private readonly generalService: GeneralService,
    protected _Autoservicios: Autoservicios,
    private readonly _FrmProvider: FrmProvider,
    private readonly activePath: ActivatedRoute,
    private readonly _StorageService: StorageService,
    protected currencyPipe: CurrencyPipe,
    protected gaService?: GAService
  ) {
    super(_Autoservicios, authService, currencyPipe, gaService);
    this.paymentChannelList = [];
    this.listGroupedReceipts = [];
    this.fileUtil = new FileUtil();
  }

  ngOnInit(): void {
    this.auth = this.authService.retrieveEntity();
    this.listReceiptsOrigin = [];
    this.paymentsTypes = [];
    this.formFilter = this._FrmProvider.CardReceiptsListComponent();

    this.insuranceType = this.formFilter.get('tipoPoliza');
    this.paymentChannel = this.formFilter.get('tiposPagosId');
    this.lastYears = this.formFilter.get('anioPago');
    this.getYearLast();
    this.showFilter = false;

    // Verifica si trae parámetros para filtrar por tipo de seguro
    this._setPolicyFromNavParams();

    // Carga de recibos agrupados (paginados)
    this.getGroupedReceipts();

    // Canales de pago: llama a servicio o carga de localstorage
    const paymentChannelStorage = this._StorageService.getStorage(STORAGE.PAYMENT_CHANNEL_PER_USER);
    if (paymentChannelStorage && paymentChannelStorage.length) {
      this.paymentChannelList = paymentChannelStorage;
    }
  }

  // Obtiene Recibos de Pagos (meses)
  public getGroupedReceipts(): void {
    if (!this.isLoadingMoreGroups) {
      this.showLoading = true;
      this.listGroupedReceipts = [];
      this.showViewMoreGroups = false;
    }
    this.isFilteredFrm = this._isFilteredFrm();
    this.listReceiptsGroupByMonth = [];
    this.showFilter = false;

    this._Autoservicios
      .ObtenerGrupoRecibos(
        { codigoApp: APPLICATION_CODE },
        {
          criterio: 'fechaPago',
          orden: SEARCH_ORDEN_DESC,
          pagina: this.page,
          registros: DEFAULT_PAYMENT_RECEIPTS_MONTHS,
          ...this.formFilter.value
        }
      )
      .subscribe(
        (response: IGroupReceiptResponse) => {
          this.showViewMoreGroups = response.flagVerMas;

          const months: IGroupReceiptMonth[] = response.lista.map(this._mapMonth.bind(this));
          this.listGroupedReceipts = [...this.listGroupedReceipts, ...months];

          if (response && response.lista && response.lista[0]) {
            if (!this.isLoadingMoreGroups) {
              this.listGroupedReceipts = [
                { ...this.listGroupedReceipts[0], loading: true },
                ...this.listGroupedReceipts.slice(1)
              ];
              this.getReceiptsDetail(response.lista[0].periodo);
            }
          }
          if (this.isLoadingMoreGroups) {
            this.isLoadingMoreGroups = false;
          }
        },
        error => {
          console.error(error);
        },
        () => {
          this.showLoading = false;
        }
      );
  }

  expand(action: boolean, group: IGroupReceiptMonth): void {
    group.collapse = action;
    if (!group.payments && !group.loading) {
      group.loading = true;
      this.getReceiptsDetail(group.periodo);
    }
  }

  // Obtiene Recibos de Pagos (detalle)
  public getReceiptsDetail(period: string): void {
    this.listReceiptsGroupByMonth = [];
    this.showFilter = false;

    this._Autoservicios
      .ObtenerDetalleRecibos(
        { codigoApp: APPLICATION_CODE },
        {
          anioMes: period,
          criterio: 'fechaPago',
          orden: SEARCH_ORDEN_DESC,
          ...this.formFilter.value
        }
      )
      .subscribe(this._mapResponsePayment.bind(this, period), err => {
        console.error(err);
      });
  }

  setSubtitle(total: any): string {
    const text = total > 1 ? PaymentLang.Texts.Payments : PaymentLang.Texts.Payment;

    return `-  ${total} ${text}`;
  }

  getPaymentTypeName(id: any): string {
    const findPaymentsType = this.paymentChannelList.filter(data => +data.tiposPagosId === +id);

    return findPaymentsType.length ? findPaymentsType[0].tiposPagosDescripcion : '';
  }

  // Llenar combo últimos n años
  getYearLast(): void {
    this.yearList = PolicyUtil.setYears(this.maxYears);
    this.parametersSubject = this.generalService.getParametersSubject();
    this.parametersSubject.subscribe(() => {
      const years = parseInt(this.generalService.getValueParams(MAX_YEARS_RECEIPTS), 10) || 0;
      this.maxYears = years > 0 && years < 11 ? years : this.maxYears;
      this.yearList = PolicyUtil.setYears(this.maxYears);
    });
  }

  loadMore(): void {
    if (!this.isLoadingMoreGroups) {
      this.page += 1;
      this.isLoadingMoreGroups = true;
      this.getGroupedReceipts();
    }
  }

  filter(): void {
    this.page = 1;
    this.getGroupedReceipts();
  }

  // Mostrar Todos
  resetFilter(): void {
    this.page = 1;
    this.formFilter.reset();
    this.getGroupedReceipts();
    this.isFilteredFrm = false;
  }

  private _isFilteredFrm(): boolean {
    const fk = Object.keys(this.formFilter.value);

    return !fk.every(k => !this.formFilter.value[k]);
  }

  // Descargar PDF de Recibo
  downloadPDF(receipt): void {
    if (this.showSpinnerReceipts) {
      return;
    }
    this.receiptSelected = receipt.numRecibo;
    const isNotice = receipt.tipoDocumentoPago === POLICY_QUOTA_TYPE.AVISO;
    const numberReciboOrAviso = isNotice ? receipt.numeroAviso : receipt.numRecibo;
    const pathReq = {
      codigoApp: APPLICATION_CODE,
      codCia: receipt.codCia,
      numeroRecibo: numberReciboOrAviso,
      tipoDocumentoPago: receipt.tipoDocumentoPago
    };
    this.showSpinnerReceipts = true;
    this.pdfSub = this._Autoservicios.ObtenerComprobantePagoHpexstream(pathReq).subscribe(
      (res: any) => {
        try {
          const fileName = isNotice ? `Aviso ${receipt.numeroAviso}` : `Recibo ${receipt.numRecibo}`;
          this.fileUtil.download(res.base64, 'pdf', fileName);
        } catch (error) {}
      },
      () => {
        this.showSpinnerReceipts = false;
      },
      () => {
        this.showSpinnerReceipts = false;
        this.pdfSub.unsubscribe();
      }
    );
  }

  // Enviar recibo por correo
  openEmailModal(receipt: string): void {
    this.openEmail = true;
    setTimeout(() => {
      this.modalEmail.receiptNumber = receipt;
      this.modalEmail.open();
    });
  }

  private _setPolicyFromNavParams(): void {
    const typeInsurance = this.activePath.snapshot.params.typeInsurance;
    if (typeInsurance) {
      const arrPolicies = this._StorageService.getStorage(TIPO_SEGURO) || [];
      const id = (arrPolicies.find(p => p.codigo.includes(typeInsurance)) || {}).codigo;
      this.formFilter.get('tipoPoliza').setValue(id);
    }
  }

  private _mapMonth(month, idx): IGroupReceiptMonth {
    const soles = month.montoSoles
      ? `${this.currencyPipe.transform(month.montoSoles, `${StringUtil.getMoneyDescription(1)} `)}`
      : '';
    const dolares = month.montoDolares
      ? `${this.currencyPipe.transform(month.montoDolares, `${StringUtil.getMoneyDescription(2)} `)}`
      : '';
    const pluralize = month.cantidadPagos
      ? month.cantidadPagos === 1
        ? PaymentLang.Texts.Payment
        : PaymentLang.Texts.Payments
      : '';

    return {
      ...month,
      subtitle: pluralize ? `- ${month.cantidadPagos} ${pluralize}` : '',
      textRight: soles && dolares ? `${soles} / ${dolares}` : soles ? `${soles}` : `${dolares}`,
      collapse: !(idx === 0 && this.page === 1)
    };
  }

  private _mapResponsePayment(period: string, res: any[]): void {
    if (!res) {
      return;
    }

    const payments: IMonthReceipt[] = res.map(this._mapPayments.bind(this));

    this.listGroupedReceipts = this.listGroupedReceipts.map(group => {
      return group.periodo === period ? { ...group, payments, loading: false } : group;
    });
  }

  private _mapPayments(receipt, index): IMonthReceipt {
    const symbol = `${StringUtil.getMoneyDescription(receipt.codigoMoneda)} `;
    const isNotice = receipt.tipoDocumentoPago === POLICY_QUOTA_TYPE.AVISO;
    const isInvoice = receipt.tipoDocumentoPago === POLICY_QUOTA_TYPE.FACTURA;
    const customIcon = isNotice ? noticeIcon : isInvoice ? invoiceIcon : null;
    const billNumber = receipt.numRecibo;
    const noticeNumber = receipt.numeroAviso;
    const preInvoiceNumber = receipt.nroPreFactura;

    const paymentlabel = isInvoice ? this.invoiceNumber : this.billNumber;
    const label1 = isNotice ? `${this.noticeNumber}: ${noticeNumber}` : `${paymentlabel}: ${billNumber}`;
    const label2 = isInvoice ? `${this.preInvoiceNumber}: ${preInvoiceNumber}` : null;
    const paymentNumberLabels = [label1, label2];

    const associatedReceipts = (receipt.listaRecibosAsociados || []).length;
    const noticeLabel = `${associatedReceipts} ${associatedReceipts === 1 ? this.labelPolicy : this.labelPolicies}`;
    const invoiceLabel = `${this.contractorNumber}: ${receipt.numeroPoliza}`;
    const receiptLabel = `1 ${this.labelPolicy}`;
    const paymentDetailLabel = `${isNotice ? noticeLabel : isInvoice ? invoiceLabel : receiptLabel}`;

    return {
      ...receipt,
      customIcon,
      paymentNumberLabels,
      paymentDetailLabel,
      tipoPoliza: PolicyUtil.verifyPolicyType(receipt),
      totalAmount: `${this.currencyPipe.transform(receipt.montoTotal, symbol)}`,
      tiposPagosNombre: this.getPaymentTypeName(receipt.tiposPagosId),
      isNotice,
      isInvoice,
      associatedReceipts: receipt.listaRecibosAsociados,
      paymentReceiptList: this.setPaymentReceiptList(receipt),
      collapse: index > 0
    };
  }
}
