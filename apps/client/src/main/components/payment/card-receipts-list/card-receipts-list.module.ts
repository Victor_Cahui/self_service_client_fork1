import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SendReceiptModule } from '@mx/components/payment/send-receipt/send-receipt.module';
import { CardItemDetailModule } from '@mx/components/shared/card-item-detail/card-item-detail.module';
import { IconPaymentTypeModule } from '@mx/components/shared/icon-payment-type/icon-payment-type.module';
import { IconPoliciesModule } from '@mx/components/shared/icon-policies/icon-polices.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule, MfCardModule, MfInputModule } from '@mx/core/ui';
import { MfButtonModule } from '@mx/core/ui/lib/components/forms/button';
import { MfSelectModule } from '@mx/core/ui/lib/components/forms/select';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { LinksModule } from '@mx/core/ui/lib/links/links.module';
import { CardReceiptsListComponent } from './card-receipts-list.component';

@NgModule({
  imports: [
    CommonModule,
    DirectivesModule,
    FormsModule,
    MfCardModule,
    LinksModule,
    MfInputModule,
    IconPoliciesModule,
    IconPaymentTypeModule,
    MfSelectModule,
    ReactiveFormsModule,
    MfButtonModule,
    MfLoaderModule,
    SendReceiptModule,
    CardItemDetailModule,
    GoogleModule
  ],
  declarations: [CardReceiptsListComponent],
  exports: [CardReceiptsListComponent]
})
export class CardReceiptsListModule {}
