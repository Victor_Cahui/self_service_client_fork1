import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';
import { TooltipsModule } from '@mx/core/ui/lib/components/tooltips/tooltips.module';
import { CardPaymentPolicyComponent } from './card-payment-policy.component';

@NgModule({
  imports: [CommonModule, DirectivesModule, GoogleModule, MfCardModule, TooltipsModule],
  declarations: [CardPaymentPolicyComponent],
  exports: [CardPaymentPolicyComponent]
})
export class CardPaymentPolicyModule {}
