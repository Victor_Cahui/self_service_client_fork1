import { Component, ViewChild } from '@angular/core';
import { ModalHowWorksComponent } from '@mx/components/shared/modals/modal-how-works/modal-how-works.component';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { MFP_Mis_Pagos_43A } from '@mx/settings/constants/events.analytics';
import { PAYMENTS_SETTING_POLICY_LIST } from '@mx/settings/constants/router-titles';
import { PAYMENT_SETTINGS_NOTIFICATION, PaymentLang } from '@mx/settings/lang/payment.lang';
import { HowAutomaticDebitWorks } from '@mx/settings/lang/services.lang';

@Component({
  selector: 'client-card-payments-setting',
  templateUrl: './card-payments-setting.component.html'
})
export class CardPaymentsSettingComponent {
  @ViewChild(ModalHowWorksComponent) mfModalHowWorks: ModalHowWorksComponent;

  titlePaymentsSettings = PaymentLang.Titles.PaymentsSetting;
  bannerNotification = PAYMENT_SETTINGS_NOTIFICATION;
  howAutomaticDebitWorks = HowAutomaticDebitWorks;
  header = PAYMENTS_SETTING_POLICY_LIST;
  openModalHowWorks: boolean;

  ga: Array<IGaPropertie>;

  constructor() {
    this.ga = [MFP_Mis_Pagos_43A()];
  }

  bannerAction(): void {
    this.openModalHowWorks = true;
    setTimeout(() => {
      this.mfModalHowWorks.open();
    });
  }
}
