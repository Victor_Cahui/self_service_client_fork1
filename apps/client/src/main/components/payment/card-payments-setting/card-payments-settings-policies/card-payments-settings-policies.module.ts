import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PaymentSettingsPolicyModule } from '@mx/components/shared';
import { DropdownItemsPolicyModule } from '@mx/components/shared/dropdown/dropdown-items-policy/dropdown-items-policy.module';
import { IconPoliciesModule } from '@mx/components/shared/icon-policies/icon-polices.module';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';
import { MfCustomAlertModule } from '@mx/core/ui/lib/components/alerts/custom-alert';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { PaymentsSettingsPoliciesComponent } from './card-payments-settings-policies.component';

@NgModule({
  imports: [
    CommonModule,
    DirectivesModule,
    MfCardModule,
    MfLoaderModule,
    GoogleModule,
    IconPoliciesModule,
    DropdownItemsPolicyModule,
    PaymentSettingsPolicyModule,
    MfCustomAlertModule
  ],
  declarations: [PaymentsSettingsPoliciesComponent],
  exports: [PaymentsSettingsPoliciesComponent]
})
export class PaymentsSettingsPoliciesModule {}
