import { DatePipe } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { NotificationStatus } from '@mx/core/ui/lib/components/alerts/custom-alert/custom-alert.component';
import { AuthService } from '@mx/services/auth/auth.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { KEY_STORAGE_PAYMENT, PaymentService } from '@mx/services/payment.service';
import { MFP_Mis_Pagos_43B } from '@mx/settings/constants/events.analytics';
import { FILE_ICON } from '@mx/settings/constants/images-values';
import { PaymentLang } from '@mx/settings/lang/payment.lang';
import { IPaymentConfigurationResponse } from '@mx/statemanagement/models/payment.models';
import { BasePaymentSettings } from '../base-payments-settings';

@Component({
  selector: 'client-card-payments-settings-policies',
  templateUrl: './card-payments-settings-policies.component.html',
  providers: [DatePipe]
})
export class PaymentsSettingsPoliciesComponent extends BasePaymentSettings implements OnInit, OnDestroy {
  fileIcon = FILE_ICON;
  ga: Array<IGaPropertie>;

  text = PaymentLang.Texts.DisableDebitCard;

  onSuccess = false;
  successStatus = NotificationStatus.SUCCESS;

  constructor(
    private readonly storageService: StorageService,
    protected paymentService: PaymentService,
    protected authService: AuthService,
    protected datePipe: DatePipe,
    protected activatedRoute: ActivatedRoute,
    protected headerHelperService: HeaderHelperService,
    private readonly router: Router
  ) {
    super(paymentService, authService, activatedRoute, headerHelperService, datePipe);
    this.ga = [MFP_Mis_Pagos_43B()];
  }

  ngOnInit(): void {
    this.storageService.removeItem(KEY_STORAGE_PAYMENT.PAYMENT_LIST);
    this.getPaymentList();

    this.paymentService.notificactionForPS.subscribe((resp: any) => {
      if (resp.value === true) {
        this.onSuccess = resp.value;
        this.text = resp.text;
        this.storageService.removeItem(KEY_STORAGE_PAYMENT.PAYMENT_LIST);
        this.getPaymentList();
        setTimeout(() => {
          this.onSuccess = false;
          this.paymentService.notificactionForPS.next({ value: false });
        }, 5000);
      }
    });
  }

  goToDetail(payment: IPaymentConfigurationResponse): void {
    const path = `${this.router.url}/${payment.numeroPoliza}/`;
    this.router.navigate([path]);
  }
}
