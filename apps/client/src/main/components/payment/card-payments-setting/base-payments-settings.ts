import { DatePipe } from '@angular/common';
import { OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PolicyUtil } from '@mx/components/shared/utils/policy.util';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { AuthService } from '@mx/services/auth/auth.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { PaymentService } from '@mx/services/payment.service';
import { APPLICATION_CODE, FORMAT_DATE_DDMMYYYY } from '@mx/settings/constants/general-values';
import { PAYMENTS_SETTING_POLICY_LIST } from '@mx/settings/constants/router-titles';
import { PAYMENT_SETTINGS_RENEWAL } from '@mx/settings/lang/payment.lang';
import { IPaymentConfigurationResponse, IPaymentRefinancingResponse } from '@mx/statemanagement/models/payment.models';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { Subscription } from 'rxjs';

export abstract class BasePaymentSettings extends GaUnsubscribeBase implements OnDestroy {
  auth: IdDocument;
  data = PolicyUtil.setDataCardMyPolicy();
  showLoading: boolean;
  payment: IPaymentConfigurationResponse;
  refinancing: IPaymentRefinancingResponse;
  policyNumber: string;
  collapse: boolean;
  refinancingStatus: boolean;
  paymentsList: Array<IPaymentConfigurationResponse>;
  paymentConfigurationSub: Subscription;
  paymentConfigurationRefinancingSub: Subscription;

  currentPayment: string;

  // pago seleccionado
  renewalLang = PAYMENT_SETTINGS_RENEWAL;

  constructor(
    protected paymentService: PaymentService,
    protected authService: AuthService,
    protected activatedRoute: ActivatedRoute,
    protected headerHelperService: HeaderHelperService,
    protected datePipe: DatePipe,
    protected gaService?: GAService
  ) {
    super(gaService);
    this.auth = this.authService.retrieveEntity();
  }

  ngOnDestroy(): void {
    if (this.paymentConfigurationSub) {
      this.paymentConfigurationSub.unsubscribe();
    }
    this.onDestroy();
  }

  // Lista de pagos
  getPaymentList(): void {
    this.paymentsList = this.paymentService.getPaymentListConfiguration();
    if (!this.paymentsList) {
      this.showLoading = true;
      this.paymentsList = [];
      const params = {
        codigoApp: APPLICATION_CODE
      };
      this.paymentConfigurationSub = this.paymentService.paymentConfiguration(params).subscribe(
        (response: Array<IPaymentConfigurationResponse>) => {
          if (response) {
            this.paymentsList = response.map((payment: IPaymentConfigurationResponse) => {
              payment.unBeneficiario = payment.beneficiarios.length === 1;
              payment.fechaRenovacion = this.datePipe.transform(payment.fechaRenovacion, FORMAT_DATE_DDMMYYYY);
              payment.tieneTipoPago =
                payment.puedeHacerPagoManual || payment.puedeHacerDebitoAutomatico || payment.puedeHacerCargoCuenta;

              return payment;
            });
            this.paymentService.setPaymentListConfiguration(this.paymentsList);
            this.onLoadPayments();
          }
          this.showLoading = false;
          this.paymentConfigurationSub.unsubscribe();
        },
        () => (this.showLoading = false),
        () => {
          this.showLoading = false;
          this.paymentConfigurationSub.unsubscribe();
        }
      );
    } else {
      this.onLoadPayments();
    }
  }

  // Pago seleccionado
  getPaymentData(): void {
    this.payment = this.paymentsList.find(p => p.numeroPoliza === this.policyNumber);
  }

  // Titulo
  setTitle(): void {
    this.headerHelperService.set(PAYMENTS_SETTING_POLICY_LIST);
  }

  onLoadPayments(): void {}

  getRefinancingQuotas(): void {
    this.showLoading = true;
    this.paymentConfigurationRefinancingSub = this.paymentService
      .paymentConfigurationRefinancing({
        codigoApp: APPLICATION_CODE,
        numeroPoliza: this.activatedRoute.snapshot.params['policyNumber']
      })
      .subscribe(
        (response: IPaymentRefinancingResponse) => {
          if (response) {
            this.refinancing = response;
            this.refinancingStatus = response.enProcesoRefinanciamiento;
            this.showLoading = false;
            this.currentPayment = this.setPaymentFormat(
              this.refinancing.cuotaVigente.codigoMonedaMonto,
              this.refinancing.cuotaVigente.montoCuota
            );
            this.onLoadRefinancing();
          }
          this.paymentConfigurationRefinancingSub.unsubscribe();
        },
        () => (this.showLoading = false),
        () => {
          this.showLoading = false;
          this.paymentConfigurationRefinancingSub.unsubscribe();
        }
      );
  }

  setPaymentFormat(currencyCode: number, amount: number): string {
    return `${StringUtil.getMoneyDescription(currencyCode)} ${amount}`;
  }

  onLoadRefinancing(): void {}

  onDestroy(): void {}
}
