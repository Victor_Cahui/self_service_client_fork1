import { Component } from '@angular/core';
import { IGaPropertie } from '@mx/core/shared/helpers/util/google';
import { MFP_Mis_Pagos_41A } from '@mx/settings/constants/events.analytics';
import { PAYMENTS_TAB } from '@mx/settings/constants/router-tabs';

@Component({
  selector: 'client-tabs-payments',
  templateUrl: './tabs-payments.component.html'
})
export class TabsPaymentsComponent {
  ga: Array<IGaPropertie>;
  list = PAYMENTS_TAB;

  constructor() {
    this.ga = [MFP_Mis_Pagos_41A()];
  }
}
