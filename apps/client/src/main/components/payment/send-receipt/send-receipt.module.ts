import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MfInputModule } from '@mx/core/ui/lib/components/forms/input';
import { MfModalModule } from '@mx/core/ui/lib/components/modals/modal';
import {
  MfAlertWarningModule,
  MfButtonModule,
  MfModalAlertModule,
  MfModalMessageModule,
  MfShowErrorsModule
} from '@mx/core/ui/public-api';
import { SendReceiptComponent } from './send-receipt.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MfModalModule,
    MfInputModule,
    MfModalAlertModule,
    MfShowErrorsModule,
    MfModalMessageModule,
    MfAlertWarningModule,
    MfButtonModule
  ],
  exports: [SendReceiptComponent],
  declarations: [SendReceiptComponent]
})
export class SendReceiptModule {}
