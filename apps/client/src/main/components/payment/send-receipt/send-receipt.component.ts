import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmailValidator, MfModal } from '@mx/core/ui';
import { MfModalMessage } from '@mx/core/ui/lib/components/modals/modal-message';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { PaymentService } from '@mx/services/payment.service';
import { APPLICATION_CODE, GENERAL_MAX_LENGTH } from '@mx/settings/constants/general-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { PaymentLang } from '@mx/settings/lang/payment.lang';
import { IClientInformationResponse } from '@mx/statemanagement/models/client.models';
import { isEmpty } from 'lodash-es';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-send-receipt',
  templateUrl: './send-receipt.component.html'
})
export class SendReceiptComponent implements OnInit {
  @Input() receiptNumber: string;

  @ViewChild(MfModal) mfModal: MfModal;
  @ViewChild(MfModalMessage) modalMessage: MfModalMessage;
  titleSendReceipt = PaymentLang.Titles.SendReceipt;
  btnFooterEdit = PaymentLang.ButtonsSend;
  labelEmail = PaymentLang.Labels.Email;
  btnSend = GeneralLang.Buttons.Send;
  messageHelp = PaymentLang.Messages.EmailSendReceipt;
  messageText = '';
  messageTitle = '';

  formEmail: FormGroup;
  email: AbstractControl;
  generalMaxLength = GENERAL_MAX_LENGTH;

  userData: IClientInformationResponse;
  successEmailSave: boolean;
  successDataSend: boolean;
  isSubmitting: boolean;
  showLoading: boolean;
  showModal: boolean;
  userDataSub: Subscription;
  sendReceiptSub: Subscription;

  constructor(
    protected formBuilder: FormBuilder,
    public authService: AuthService,
    private readonly clientService: ClientService,
    private readonly paymentService: PaymentService
  ) {}

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.formEmail = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, EmailValidator.isValid])]
    });
    this.email = this.formEmail.controls['email'];
  }

  showErrors(control: string): boolean {
    return (
      (this.formEmail.controls[control].dirty || this.formEmail.controls[control].touched) &&
      !isEmpty(this.formEmail.controls[control].errors)
    );
  }

  sendData(confirm: boolean): void {
    if (confirm) {
      this.isSubmitting = true;
      const body = [{ correo: this.email.value }];
      const params = {
        codigoApp: APPLICATION_CODE,
        numeroRecibo: this.receiptNumber
      };
      this.showLoading = true;
      this.sendReceiptSub = this.paymentService.sendReceiptPdf(body, params).subscribe(
        (response: any) => {
          this.messageTitle = GeneralLang.Alert.titleSuccess;
          this.messageText = GeneralLang.Alert.sendData.success.message;
          this.successDataSend = true;
          this.showLoading = false;
          this.doneEmailSend();
        },
        () => {
          this.showLoading = false;
          this.successDataSend = false;
          this.doneEmailSend();
          this.isSubmitting = false;
          this.sendReceiptSub.unsubscribe();
        },
        () => {
          this.isSubmitting = false;
          this.sendReceiptSub.unsubscribe();
        }
      );
    } else {
      this.resetForm();
    }
  }

  public resetForm(): void {
    this.formEmail.reset();
  }

  private doneEmailSend(): void {
    this.mfModal.close();
    this.initForm();
    if (this.successDataSend) {
      this.modalMessage.open();
    }
  }

  open(): void {
    this.showModal = true;
    this.getClientEmail();
    setTimeout(() => {
      this.mfModal.open();
    });
  }

  // Obtiene email del cliente
  getClientEmail(): void {
    const email = this.clientService.getUserEmail();
    if (email) {
      this.email.setValue(email);
    } else {
      this.userDataSub = this.clientService.getClientInformation({}).subscribe(
        (data: IClientInformationResponse) => {
          this.email.setValue(data.emailCliente);
          this.userData = data;
        },
        () => {},
        () => {
          this.clientService.setUserData(this.userData);
          this.userDataSub.unsubscribe();
        }
      );
    }
  }

  closeModalSend(): void {
    this.showModal = false;
    this.mfModal.close();
  }
}
