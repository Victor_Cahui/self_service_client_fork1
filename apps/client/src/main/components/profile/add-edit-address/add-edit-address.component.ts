import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { FormComponent } from '@mx/components/shared/utils/form';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { MfModal, SelectListItem } from '@mx/core/ui';
import { MfModalMessage } from '@mx/core/ui/lib/components/modals/modal-message';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { GeneralService } from '@mx/services/general/general.service';
import { HeaderHelperService, IHeader } from '@mx/services/general/header-helper.service';
import { STORAGE_KEYS } from '@mx/services/policies.service';
import { UbigeoService } from '@mx/services/ubigeo.service';
import { GENERAL_MAX_LENGTH } from '@mx/settings/constants/general-values';
import { MAX_LENGTH_CORRESPONDENCE_ADDRESS } from '@mx/settings/constants/key-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { ProfileLang } from '@mx/settings/lang/profile.lang';
import { IClientAddressResponse } from '@mx/statemanagement/models/client.models';
import { IPoliciesByClientResponse } from '@mx/statemanagement/models/policy.interface';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-add-edit-address',
  templateUrl: './add-edit-address.component.html'
})
export class AddEditAddressComponent implements OnInit, OnDestroy, OnChanges {
  @Input() correspondenceAddress: IClientAddressResponse;
  @Output() successAddressEdit: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild(MfModal) mfModal: MfModal;
  @ViewChild('checkBoxAddress') checkBoxAddress: any;
  @ViewChild(MfModalMessage) modalMessage: MfModalMessage;

  entity: IdDocument;
  policyAddress: Array<IClientAddressResponse> = [];
  addressCorrespondenceList: Array<SelectListItem>;
  selectedAddress: IClientAddressResponse;
  editAddress: boolean;
  loadSave: boolean;
  showLoading: boolean;
  chekboxAddressState: boolean;
  chekboxUpdateAllAddress: boolean;
  canUpdateAllAddress: boolean;
  detailRoute: IHeader;
  buttonState: boolean;
  showModalMessage: boolean;
  successSaveAddress = true;
  formSub: Subscription;
  headerHelperSub: Subscription;
  policyAddressSub: Subscription;
  departamentsSub: Subscription;
  provincesSub: Subscription;
  districtsSub: Subscription;
  updateAddressSub: Subscription;
  maxlengthAddressProfile: number;

  formAddress: FormGroup;
  addressCorrespondenceForm: AbstractControl;
  address: AbstractControl;
  departments: AbstractControl;
  provinces: AbstractControl;
  districts: AbstractControl;

  departamentList: Array<SelectListItem>;
  provinceList: Array<SelectListItem>;
  districtList: Array<SelectListItem>;

  labelAddress = ProfileLang.Labels.Address;
  labelAddressSelect = ProfileLang.Labels.AddressSelect;
  labelAddressEdit = ProfileLang.Labels.AddressEdit;
  labelDepartment = ProfileLang.Labels.Department;
  labelProvince = ProfileLang.Labels.Province;
  labelDistrict = ProfileLang.Labels.District;
  labelUpdateAllAddress = ProfileLang.Labels.UpdateForAllAddress;
  titleModal = '';
  textPolicyAddress = ProfileLang.Texts.PoliciesAddress;
  messageSuccessUpdate = GeneralLang.Messages.SuccessUpdate;
  messageDone = GeneralLang.Messages.Done;
  generalMaxLength = GENERAL_MAX_LENGTH;

  constructor(
    private readonly ubigeoService: UbigeoService,
    private readonly formBuilder: FormBuilder,
    private readonly clientService: ClientService,
    private readonly authService: AuthService,
    private readonly headerHelperService: HeaderHelperService,
    private readonly storageService: StorageService,
    private readonly generalService: GeneralService
  ) {
    this.addressCorrespondenceForm = new FormControl(null, Validators.required);
    this.loadSave = false;
  }

  ngOnInit(): void {
    this.headerHelperSub = this.headerHelperService.header.subscribe(header => {
      this.detailRoute = header;
    });
    this.initForm();
    this.getParameters();
    this.getEntity();
    this.getAddress();
    this.getPoliciesAsContractor();
    this.titleModal = this.getModalTitle();
  }

  initForm(): void {
    if (this.formSub) {
      this.formSub.unsubscribe();
    }
    this.createForm();
    this.valueChanges();
    this.buttonState = !(this.chekboxAddressState && !!this.selectedAddress);
    if (this.correspondenceAddress && this.correspondenceAddress.departamento) {
      this.address.setValue(this.correspondenceAddress.direccion);
    }
  }

  getParameters(): void {
    this.generalService.getParametersSubject().subscribe(() => {
      this.maxlengthAddressProfile =
        +this.generalService.getValueParams(MAX_LENGTH_CORRESPONDENCE_ADDRESS) || this.generalMaxLength.addressProfile;
    });
  }

  createForm(): void {
    this.formAddress = this.formBuilder.group({
      address: [null, Validators.required],
      departments: [null, Validators.required],
      provinces: [null, Validators.required],
      districts: [null, Validators.required]
    });
    this.address = this.formAddress.get('address');
    this.departments = this.formAddress.get('departments');
    this.provinces = this.formAddress.get('provinces');
    this.districts = this.formAddress.get('districts');
  }

  valueChanges(): void {
    this.formSub = this.formAddress.valueChanges.subscribe(() => {
      if (this.editAddress) {
        this.buttonState = !this.formAddress.valid;
      }
    });
  }

  getEntity(): void {
    this.entity = this.authService.retrieveEntity();
  }

  // Obtener direccion de polizas
  getAddress(): void {
    const clientAddress = {
      tipoPoliza: POLICY_TYPES.MD_HOGAR.code
    };
    this.policyAddressSub = this.clientService.getAddress(clientAddress).subscribe(
      address => {
        if (!(address && address.length)) {
          this.editAddress = true;
        } else {
          this.policyAddress = address;
          this.addressCorrespondenceList = this.policyAddress.map((v, i) => {
            return new SelectListItem({ text: this.addressComplex(v), value: i.toString(), selected: i === 0 });
          });
          this.addressCorrespondenceForm.setValue('0');
          this.selectedAddress = this.policyAddress[0];
        }
      },
      () => {},
      () => {
        this.policyAddressSub.unsubscribe();
      }
    );
  }

  addressComplex(address: IClientAddressResponse): string {
    return `${address.direccion}, ${address.distrito}, ${address.provincia}, ${address.departamento}`;
  }

  open(edit: boolean): void {
    if (this.policyAddress && this.policyAddress.length) {
      this.setCheckBoxState(true, true);
      this.editAddress = false;
    }
    if (this.correspondenceAddress && this.correspondenceAddress.identificadorDepartamento) {
      this.getDepartaments(this.correspondenceAddress.identificadorDepartamento.toString());
    } else {
      this.getDepartaments();
    }
    if (edit) {
      this.disableAddress();
    }
    this.mfModal.open();
  }

  disableAddress(): void {
    this.editAddress = true;
    if (this.editAddress && this.checkBoxAddress) {
      this.checkBoxAddress.checked = false;
      this.setCheckBoxState(false);
      this.buttonState = !this.formAddress.valid;
    }
  }

  getModalTitle(): string {
    const action = this.correspondenceAddress ? GeneralLang.Texts.Edit : GeneralLang.Texts.Add;

    return `${action} ${ProfileLang.Titles.MailAddress}`;
  }

  showErrors(controlKey: string): boolean {
    return FormComponent.showBasicErrors(this.formAddress, controlKey);
  }

  // Obtener Ubigeo
  getDepartaments(defaultDepartamento?: string): void {
    this.departamentsSub = this.ubigeoService.getDepartaments().subscribe(
      departaments => {
        this.departamentList = departaments.map(this.setSelectListItem.bind(this));
        this.initForm();
        if (defaultDepartamento) {
          this.departments.setValue(defaultDepartamento);
          this.getProvincias(this.correspondenceAddress.identificadorProvincia.toString());
        }
      },
      () => {},
      () => {
        this.departamentsSub.unsubscribe();
      }
    );
  }

  getProvincias(defaultProvince?: string): void {
    const deparment = this.departments.value;
    this.provincesSub = this.ubigeoService.getProvinces(deparment).subscribe(
      provinces => {
        this.provinces.markAsTouched();
        this.districts.markAsTouched();
        this.provinceList = provinces.map(this.setSelectListItem.bind(this));
        if (defaultProvince) {
          this.provinces.setValue(defaultProvince);
          this.getDistritos(this.correspondenceAddress.identificadorDistrito.toString());
        } else {
          this.provinces.setValue(null);
          this.districts.setValue(null);
        }
        this.districtList = [];
      },
      () => {},
      () => {
        this.provincesSub.unsubscribe();
      }
    );
  }

  getDistritos(defaultDistrict?: string): void {
    const province = this.provinces.value;
    this.districtsSub = this.ubigeoService.getDistricts(province).subscribe(
      district => {
        this.districtList = district.map(this.setSelectListItem.bind(this));
        if (defaultDistrict) {
          this.districts.setValue(defaultDistrict);
        } else {
          this.districts.setValue(null);
        }
      },
      () => {},
      () => {
        this.districtsSub.unsubscribe();
      }
    );
  }

  changeCheckBox(event: any): void {
    this.setCheckBoxState(event);
    this.editAddress = !event;
    this.formAddress.updateValueAndValidity();
  }

  setCheckBoxState(state: boolean, setStateToElement?: boolean): void {
    this.chekboxAddressState = state;
    this.buttonState = !(this.chekboxAddressState && !!this.selectedAddress);
    if (setStateToElement && this.checkBoxAddress) {
      this.checkBoxAddress.checked = state;
    }
  }

  setSelectListItem(ubigeo): SelectListItem {
    return new SelectListItem({
      text: ubigeo.descripcion,
      value: ubigeo.ubigeoId.toString()
    });
  }

  // Actualizar
  saveAddress(response: boolean): void {
    if (response && !this.loadSave) {
      if (this.formAddress.valid || (this.checkBoxAddress && this.checkBoxAddress.checked)) {
        this.loadSave = true;
        const correspondenceAddress = { ...this.prepareAddress() };
        delete correspondenceAddress.departamento;
        delete correspondenceAddress.provincia;
        delete correspondenceAddress.distrito;
        const identityDocumento = {};
        this.showLoading = true;
        this.updateAddressSub = this.clientService
          .updateCorrespondenceAddress(identityDocumento, correspondenceAddress)
          .subscribe(
            address => {
              this.onSuccessAddressEdit(address);
              this.successSaveAddress = true;
            },
            () => {
              this.showLoading = false;
              this.successSaveAddress = false;
            },
            () => {
              this.showLoading = false;
              this.loadSave = false;
              this.updateAddressSub.unsubscribe();
            }
          );
      }
    }
  }

  prepareAddress(): IClientAddressResponse {
    let correspondenceAddress: IClientAddressResponse;
    const checked = this.checkBoxAddress && this.checkBoxAddress.checked;
    correspondenceAddress = {
      identificadorDepartamento: !checked
        ? Number(this.departments.value)
        : this.selectedAddress.identificadorDepartamento,
      identificadorProvincia: !checked ? Number(this.provinces.value) : this.selectedAddress.identificadorProvincia,
      identificadorDistrito: !checked ? Number(this.districts.value) : this.selectedAddress.identificadorDistrito,
      direccion: !checked ? this.address.value : this.selectedAddress.direccion,
      departamento: !checked
        ? this.getTextfromUbigeoList(this.departamentList, this.departments.value)
        : this.selectedAddress.departamento,
      provincia: !checked
        ? this.getTextfromUbigeoList(this.provinceList, this.provinces.value)
        : this.selectedAddress.provincia,
      distrito: !checked
        ? this.getTextfromUbigeoList(this.districtList, this.districts.value)
        : this.selectedAddress.distrito,
      todasLasPolizas: this.canUpdateAllAddress ? this.chekboxUpdateAllAddress : false
    } as IClientAddressResponse;

    return correspondenceAddress;
  }

  private getTextfromUbigeoList(list: Array<SelectListItem>, value: string): string {
    return list.find(v => v.value === value).text;
  }

  private onSuccessAddressEdit(response): void {
    this.editAddress = false;
    this.buttonState = false;
    this.createForm();
    this.messageSuccessUpdate = GeneralLang.Messages.SuccessUpdate;
    this.messageDone = GeneralLang.Messages.Done;
    this.mfModal.close();
    this.showModalMessage = true;
    setTimeout(() => {
      this.modalMessage.open();
    });
    this.successAddressEdit.emit(response);
  }

  setAddress(event: any): void {
    this.selectedAddress = this.policyAddress[+event.target.value];
  }

  onUpdateAllAddress(event: any): void {
    this.chekboxUpdateAllAddress = event.srcElement.checked;
  }

  getPoliciesAsContractor(): void {
    const policiesAsContractor = this.storageService
      .getStorage(STORAGE_KEYS.POLICIES_BY_CLIENT)
      .find((p: IPoliciesByClientResponse) => p.esContratante);
    policiesAsContractor && (this.canUpdateAllAddress = this.chekboxUpdateAllAddress = true);
  }

  ngOnChanges(): void {
    this.chekboxUpdateAllAddress = true;
  }

  ngOnDestroy(): void {
    if (this.headerHelperSub) {
      this.headerHelperSub.unsubscribe();
    }
  }
}
