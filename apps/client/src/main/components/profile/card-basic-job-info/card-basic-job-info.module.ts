import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';
import { CardBasicJobInfoComponent } from './card-basic-job-info.component';

@NgModule({
  imports: [CommonModule, DirectivesModule, MfCardModule],
  declarations: [CardBasicJobInfoComponent],
  exports: [CardBasicJobInfoComponent]
})
export class CardBasicJobInfoModule {}
