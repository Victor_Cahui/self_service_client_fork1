import { Component, EventEmitter, HostBinding, Input, OnInit, Output } from '@angular/core';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { KEY_PERMISSIONS } from '@mx/settings/auth/auth-values';
import { BRIEFCASE_ICON, PHONE_ICON, PIN_ICON, WALLET_ICON } from '@mx/settings/constants/images-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { ProfileLang } from '@mx/settings/lang/profile.lang';
import { IJobBasicInfo } from '@mx/statemanagement/models/profile.interface';

@Component({
  selector: 'client-card-basic-job-info',
  templateUrl: './card-basic-job-info.component.html',
  styles: []
})
export class CardBasicJobInfoComponent implements OnInit {
  @HostBinding('attr.class') attr_class = 'w-10';
  @Input() info?: IJobBasicInfo;
  @Output() edit?: EventEmitter<IJobBasicInfo>;
  collapse = false;
  titleCard = ProfileLang.Titles.MyWork;
  withOutWorkInformation = GeneralLang.Texts.WithOutProfession;
  iconCard = BRIEFCASE_ICON;
  pinIcon = PIN_ICON;
  walletIcon = WALLET_ICON;
  phoneIcon = PHONE_ICON;
  canShowSlaryRange: boolean;

  constructor(private readonly localStorageService: LocalStorageService) {
    this.edit = new EventEmitter<IJobBasicInfo>();
  }

  ngOnInit(): void {
    this.canShowSalary();
  }

  workData(): string {
    return !!this.info
      ? !!this.info.workstation
        ? this.info.workstation
        : this.withOutWorkInformation
      : this.withOutWorkInformation;
  }

  companyName(): string {
    return (this.info && this.info.companyName) || ProfileLang.Texts.WithoutCompanyName;
  }

  companyAddress(): string {
    return this.info && this.info.companyAddress;
  }

  salaryRangeDescription(): string {
    return this.info && this.info.salaryRangeDescription
      ? `${ProfileLang.Labels.SalaryRange} ${this.info.salaryRangeDescription}`
      : ProfileLang.Texts.WithoutSalaryRange;
  }

  phone(): string {
    return this.info && this.info.phone
      ? `${ProfileLang.Labels.Phone}: ${this.info.phone}`
      : ProfileLang.Texts.WithoutPhoneJob;
  }

  editClick(): void {
    if (this.edit) {
      this.edit.next(this.info);
    }
  }

  canShowSalary(): void {
    const SALARY_RANGE = 'ACTIVATE_SALARY_RANGE';
    const salaryRangePermission = JSON.parse(this.localStorageService.getItem(KEY_PERMISSIONS)).find(
      permission => permission.codigo === SALARY_RANGE
    );
    this.canShowSlaryRange = salaryRangePermission && salaryRangePermission.valor;
  }
}
