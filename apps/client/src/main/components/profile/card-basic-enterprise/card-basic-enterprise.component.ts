import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { UnsubscribeOnDestroy } from '@mx/core/shared/helpers/util/unsubscribe-on-destroy';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { BUILDING_ICON, MAPFRE_CIRCLE_ICON, PHONE_ICON, PIN_ICON } from '@mx/settings/constants/images-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { ProfileLang } from '@mx/settings/lang/profile.lang';
import { IJobBasicInfo, IProfileBasicInfo } from '@mx/statemanagement/models/profile.interface';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'client-card-enterprise',
  templateUrl: './card-basic-enterprise.component.html'
})
export class CardBasicEnterpriseComponent extends UnsubscribeOnDestroy implements OnInit {
  @HostBinding('attr.class') attr_class = 'w-10';
  @Input() infoJob?: IJobBasicInfo;
  @Input() policyType: string;

  info: IProfileBasicInfo;
  iconCard = BUILDING_ICON;
  collapse = false;
  titleCard: string;
  withOutInformation = GeneralLang.Texts.WithOutInformation;
  withOutName = ProfileLang.Texts.WithoutName;
  pinIcon = PIN_ICON;
  phoneIcon = PHONE_ICON;
  mapfreIcon = MAPFRE_CIRCLE_ICON;

  constructor(private readonly policiesInfoService: PoliciesInfoService) {
    super();
  }

  ngOnInit(): void {
    this.titleCard = ProfileLang.Titles.Enterprise;
    this.policiesInfoService
      .getProfileBasicInfo()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((result: IProfileBasicInfo) => {
        this.info = result;
      });
  }

  getPhone(): string {
    return `${ProfileLang.Labels.Phone}: ${(this.info && this.info.payload.trabajo.telefonoTrabajo) ||
      this.withOutInformation}`;
  }

  getCompanyAddress(): string {
    return this.infoJob && this.infoJob.companyAddress;
  }

  getRegistrationDate(): string {
    return `${ProfileLang.Labels.RegistrationDate}
      ${(this.info && this.info.registrationDate) || this.withOutInformation}`;
  }
}
