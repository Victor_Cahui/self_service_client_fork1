import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';
import { CardBasicEnterpriseComponent } from './card-basic-enterprise.component';

@NgModule({
  imports: [CommonModule, DirectivesModule, MfCardModule],
  declarations: [CardBasicEnterpriseComponent],
  exports: [CardBasicEnterpriseComponent]
})
export class CardBasicEnterpriseModule {}
