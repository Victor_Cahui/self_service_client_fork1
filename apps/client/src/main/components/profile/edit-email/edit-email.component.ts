import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { EmailValidator, MatchValidator, MfModal } from '@mx/core/ui';
import { NotificationStatus } from '@mx/core/ui/lib/components/alerts/custom-alert/custom-alert.component';
import { MfModalMessage } from '@mx/core/ui/lib/components/modals/modal-message';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { HeaderHelperService, IHeader } from '@mx/services/general/header-helper.service';
import { GENERAL_MAX_LENGTH } from '@mx/settings/constants/general-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { ProfileLang } from '@mx/settings/lang/profile.lang';
import { IClientInformationResponse } from '@mx/statemanagement/models/client.models';
import { isEmpty } from 'lodash-es';
import { Subscription } from 'rxjs';

@Component({
  selector: 'client-edit-email',
  templateUrl: './edit-email.component.html'
})
export class EditEmailComponent implements OnInit, OnDestroy {
  @Input() clientProfile: IClientInformationResponse;
  @Output() changeEmail: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild(MfModal) mfModal: MfModal;
  @ViewChild(MfModalMessage) modalMessage: MfModalMessage;

  titleUpdateEmail = ProfileLang.Titles.UpdateEmail;
  labelEmail = ProfileLang.Labels.Email;
  labelConfirmEmail = ProfileLang.Labels.ConfirmEmail;
  messageSuccessUpdate = GeneralLang.Messages.SuccessUpdate;
  messageDone = GeneralLang.Messages.Done;
  messageEmail = ProfileLang.Messages.EmailNoMatch;
  messageEmailExistFail = ProfileLang.Messages.EmailAlreadyExist;

  formEmail: FormGroup;
  email: AbstractControl;
  validEmail: AbstractControl;
  emailExistFail: boolean;
  showLoading: boolean;
  showModalMessage: boolean;
  detailRoute: IHeader;

  validFormSub: Subscription;
  allowInputChecks = false;
  successEmailSave: boolean;
  headerHelperSub: Subscription;

  generalMaxLength = GENERAL_MAX_LENGTH;
  status = NotificationStatus.WARNING;

  constructor(
    protected formBuilder: FormBuilder,
    private readonly clientService: ClientService,
    private readonly authService: AuthService,
    private readonly route: ActivatedRoute,
    private readonly headerHelperService: HeaderHelperService
  ) {}

  ngOnInit(): void {
    this.headerHelperSub = this.headerHelperService.header.subscribe(header => {
      this.detailRoute = header;
    });
    this.initForm();
  }

  createForm(): void {
    this.formEmail = this.formBuilder.group(
      {
        email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
        validEmail: ['', Validators.compose([Validators.required, EmailValidator.isValid])]
      },
      {
        validator: MatchValidator.validate('email', 'validEmail')
      }
    );
    this.email = this.formEmail.controls['email'];
    this.validEmail = this.formEmail.controls['validEmail'];
  }

  valueChanges(): void {
    this.validFormSub = this.formEmail.valueChanges.subscribe(() => {
      this.emailExistFail = false;
      this.allowInputChecks = this.formEmail.valid;
    });
  }

  showErrors(control: string): boolean {
    return (
      (this.formEmail.controls[control].dirty || this.formEmail.controls[control].touched) &&
      !isEmpty(this.formEmail.controls[control].errors)
    );
  }

  saveForm(confirm: boolean): void {
    if (confirm) {
      if (this.allowInputChecks) {
        this.saveEmail();
      }
    } else {
      this.formEmail.reset();
      this.emailExistFail = false;
      this.initForm();
    }
  }

  saveEmail(): void {
    const identityDocument = {};
    this.showLoading = true;
    this.clientService.updateClientEmail(identityDocument, { nuevoEmailUsuario: this.email.value }).subscribe(
      () => {
        this.successEmailSave = true;
        this.emailExistFail = false;
        this.changeEmail.emit(this.email.value);
        this.doneEmailSave();
        this.showLoading = false;
      },
      (error: HttpErrorResponse) => {
        this.showLoading = false;
        if (error.status === 900) {
          this.emailExistFail = true;
        } else {
          this.emailExistFail = false;
          this.successEmailSave = false;
          this.doneEmailSave();
        }
      }
    );
  }

  private doneEmailSave(): void {
    this.mfModal.close();
    this.allowInputChecks = false;
    this.destroySubscriptions();
    this.initForm();
    if (this.successEmailSave) {
      this.modalMessage.open();
    }
  }

  initForm(): void {
    this.createForm();
    this.valueChanges();
  }

  destroySubscriptions(): void {
    if (this.validFormSub) {
      this.validFormSub.unsubscribe();
    }
    if (!!this.headerHelperSub) {
      this.headerHelperSub.unsubscribe();
    }
  }

  ngOnDestroy(): void {
    this.destroySubscriptions();
  }

  open(): void {
    this.showModalMessage = true;
    setTimeout(() => {
      this.mfModal.open();
    });
    this.destroySubscriptions();
    this.initForm();
  }
}
