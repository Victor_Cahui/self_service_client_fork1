import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DirectivesModule, MfCardModule, MfSelectModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { LinksModule } from '@mx/core/ui/lib/links/links.module';
import { AddEditAddressModule } from '../add-edit-address/add-edit-address.module';
import { CardCorrespondenceAddressComponent } from './card-correspondence-address.component';
import { WithoutCorrespondenceAddressComponent } from './without-correspondence-address.component';

@NgModule({
  imports: [
    CommonModule,
    DirectivesModule,
    MfCardModule,
    LinksModule,
    MfSelectModule,
    AddEditAddressModule,
    MfLoaderModule
  ],
  declarations: [CardCorrespondenceAddressComponent, WithoutCorrespondenceAddressComponent],
  exports: [CardCorrespondenceAddressComponent, WithoutCorrespondenceAddressComponent]
})
export class CardCorrespondenceAddressModule {}
