import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { ProfileLang } from '@mx/settings/lang/profile.lang';
import { IClientAddressResponse } from '@mx/statemanagement/models/client.models';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { isEmpty } from 'lodash-es';
import { timer } from 'rxjs';
import { isNullOrUndefined } from 'util';
import { AddEditAddressComponent } from '../add-edit-address/add-edit-address.component';

@Component({
  selector: 'client-card-correspondence-address',
  templateUrl: './card-correspondence-address.component.html'
})
export class CardCorrespondenceAddressComponent implements OnInit {
  @ViewChild(AddEditAddressComponent) addEditAddress: AddEditAddressComponent;

  labelTitle = ProfileLang.Titles.MailAddress;
  btnAddAddress = ProfileLang.Buttons.AddAddress;
  correspondenceAddress: IClientAddressResponse;

  disableCollapse = true;
  openModal = false;
  authEntity: IdDocument;
  collapse = false;
  showLoading: boolean;

  constructor(private readonly clientService: ClientService, private readonly authService: AuthService) {}

  ngOnInit(): void {
    this.correspondenceAddress = null;
    this.authEntity = this.authService.retrieveEntity();
    this.getCorrespondenceAddress();
  }

  getCorrespondenceAddress(): void {
    this.showLoading = true;
    this.clientService.getCorresponseAddress({}).subscribe(
      (clientAddress: IClientAddressResponse) => {
        this.showLoading = false;
        if (clientAddress && !isEmpty(clientAddress)) {
          this.correspondenceAddress = clientAddress;
        }
      },
      () => {
        this.showLoading = false;
      }
    );
  }

  setCorrespondenceAddress(address): void {
    // tslint:disable-next-line: no-parameter-reassignment
    address = address || {};
    this.correspondenceAddress = {
      identificadorDepartamento: address.identificadorDepartamento,
      identificadorProvincia: address.identificadorProvincia,
      identificadorDistrito: address.identificadorDistrito,
      departamento: address.departamento,
      provincia: address.provincia,
      distrito: address.distrito,
      direccion: address.direccion
    } as IClientAddressResponse;
  }

  existCorrespondenceAddress(): boolean {
    return !isNullOrUndefined(this.correspondenceAddress) && this.correspondenceAddress.direccion !== '';
  }

  getAddress(): string {
    return `${this.correspondenceAddress.distrito}, ${this.correspondenceAddress.provincia}, ${this.correspondenceAddress.departamento}`;
  }

  openModalAddress(isEdit: boolean = false): void {
    this.openModal = true;
    const timer$ = timer(100).subscribe(() => {
      this.addEditAddress.open(isEdit);
      timer$.unsubscribe();
    });
  }
}
