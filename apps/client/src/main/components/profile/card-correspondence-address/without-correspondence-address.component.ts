import { Component, OnInit } from '@angular/core';
import { ProfileLang } from '@mx/settings/lang/profile.lang';

@Component({
  selector: 'client-without-correspondence-address',
  templateUrl: './without-correspondence-address.component.html'
})
export class WithoutCorrespondenceAddressComponent implements OnInit {
  messageWithoutMailAddress = ProfileLang.Messages.WithoutMailAddress;
  messageAddMailAddress = ProfileLang.Messages.AddMailAddress;

  ngOnInit(): void {}
}
