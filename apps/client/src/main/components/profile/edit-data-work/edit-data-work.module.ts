import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MfInputModule } from '@mx/core/ui/lib/components/forms/input/input.module';
import { MfSelectModule } from '@mx/core/ui/lib/components/forms/select/select.module';
import { MfShowErrorsModule } from '@mx/core/ui/lib/components/global/show-errors/show-errors.module';
import { MfModalErrorModule } from '@mx/core/ui/lib/components/modals/modal-error/modal-error.module';
import { MfModalModule } from '@mx/core/ui/lib/components/modals/modal/modal.module';
import { EditDataWorkComponent } from './edit-data-work.component';

@NgModule({
  imports: [
    CommonModule,
    MfModalModule,
    MfModalErrorModule,
    MfInputModule,
    MfShowErrorsModule,
    MfSelectModule,
    ReactiveFormsModule
  ],
  exports: [EditDataWorkComponent],
  declarations: [EditDataWorkComponent],
  providers: []
})
export class EditDataWorkModule {}
