import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { toInteger } from '@mx/core/shared/helpers/util/number';
import { SelectListItem } from '@mx/core/ui/lib/components/forms/select/select-item';
import { MfModalComponent } from '@mx/core/ui/lib/components/modals/modal/modal.component';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { UbigeoService } from '@mx/services/ubigeo.service';
import { APPLICATION_CODE, GENERAL_MAX_LENGTH, GENERAL_MIN_LENGTH } from '@mx/settings/constants/general-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { ProfileLang } from '@mx/settings/lang/profile.lang';
import { IClientJobInformationResponse, IUpdateDataWorkerRequest } from '@mx/statemanagement/models/client.models';
import { IProfessionsDataRequest, IProfessionsDataResponse } from '@mx/statemanagement/models/profile.model';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { IUbigeoResponse } from '@mx/statemanagement/models/ubigeo.interface';
import { isEmpty } from 'lodash-es';
import { Subscription } from 'rxjs';

@Component({
  selector: './client-edit-data-work-component',
  templateUrl: './edit-data-work.component.html'
})
export class EditDataWorkComponent implements OnInit {
  @Input() salaryRange: Array<any>;
  @Input() data: IClientJobInformationResponse;
  @Output() dataUpdate: EventEmitter<IClientJobInformationResponse>;
  @Output() errorUpdate: EventEmitter<any>;

  @ViewChild(MfModalComponent) modalEditar: MfModalComponent;

  entity: IdDocument;
  maxJ = new Date();
  ranges: Array<SelectListItem>;
  formCreate: FormGroup;
  lugarTrabajo: AbstractControl;
  puesto: AbstractControl;
  rangoSalarialId: AbstractControl;
  direccion: AbstractControl;
  telefono: AbstractControl;
  departamento: AbstractControl;
  provincia: AbstractControl;
  distrito: AbstractControl;
  title: string;
  titleMobile: string;
  secondTitleMobile: string;
  showLoading: boolean;

  departamentos: Array<SelectListItem>;
  provincias: Array<SelectListItem>;
  distritos: Array<SelectListItem>;
  professions: Array<SelectListItem>;
  professionDescriptionSelect: string;

  generalMaxLength = GENERAL_MAX_LENGTH;
  generalMinLength = GENERAL_MIN_LENGTH;

  btnSave = GeneralLang.Buttons.Save;
  labelLoading = GeneralLang.Messages.Loading;
  labelDepartment = ProfileLang.Labels.Department;
  labelProvince = ProfileLang.Labels.Province;
  labelDistrict = ProfileLang.Labels.District;
  labelCompany = ProfileLang.Labels.Company;
  labelPosition = ProfileLang.Labels.Position;
  labelAddressCompany = ProfileLang.Labels.AddressCompany;
  labelEntryDate = ProfileLang.Labels.EntryDate;
  labelSalaryRange = ProfileLang.Labels.SalaryRangeS;
  labelPhoneNumber = ProfileLang.Labels.PhoneNumber;

  formSub: Subscription;
  professionsSub: Subscription;
  departamentSub: Subscription;
  provinceSub: Subscription;
  districtSub: Subscription;

  constructor(
    private readonly authService: AuthService,
    private readonly fb: FormBuilder,
    private readonly clientService: ClientService,
    private readonly ubigeoService: UbigeoService
  ) {
    this.title = ProfileLang.Titles.UpdateWork;
    this.titleMobile = ProfileLang.TitlesMobile.UpdateWork;
    this.secondTitleMobile = ProfileLang.SubtitlesMobile.UpdateWork;
    this.dataUpdate = new EventEmitter<IClientJobInformationResponse>();
    this.errorUpdate = new EventEmitter<boolean>();
    this.ranges = [];
    this.departamentos = [];
    this.provincias = [];
    this.distritos = [];
  }

  ngOnInit(): void {
    this.entity = this.authService.retrieveEntity();
    this.getProfessions();
    this.createForm();
  }

  initialiceForm(): void {
    this.modalEditar.open();
    this.ranges = this.setRange(this.salaryRange);
    this.createForm(this.data);
    this.getDepartamentos(this.departamento.value, this.provincia.value);
  }

  onSubmit($event): void {
    if ($event) {
      if (this.formCreate.valid) {
        const data = { ...this.formCreate.value };
        data.rangoSalarialId = toInteger(data.rangoSalarialId);
        const identityDocument = {};

        const jobBodyRequest: IUpdateDataWorkerRequest = {
          lugarTrabajo: data.lugarTrabajo,
          profesionId: parseInt(data.puesto, null),
          profesionDescripcion: this.professionDescriptionSelect,
          rangoSalarialId: data.rangoSalarialId,
          identificadorDepartamento: data.departamento ? parseInt(data.departamento, 10) : 0,
          identificadorProvincia: data.provincia ? parseInt(data.provincia, 10) : 0,
          identificadorDistrito: data.distrito ? parseInt(data.distrito, 10) : 0,
          direccion: data.direccion,
          telefono: data.telefono
        };
        this.showLoading = true;
        this.formSub = this.clientService.updateDataWork(identityDocument, jobBodyRequest).subscribe(
          (clientJobInformation: IClientJobInformationResponse) => {
            this.dataUpdate.next(clientJobInformation);
            this.closeModal();
            this.formSub.unsubscribe();
            this.showLoading = false;
          },
          () => {
            this.showLoading = false;
            this.formSub.unsubscribe();
          }
        );
      } else {
        this.markAsError();
      }
    }
  }

  markAsError(): void {
    this.markAsTouchedAndDirty(this.lugarTrabajo);
    this.markAsTouchedAndDirty(this.puesto);
    this.markAsTouchedAndDirty(this.rangoSalarialId);
    this.markAsTouchedAndDirty(this.direccion);
    this.markAsTouchedAndDirty(this.telefono);
    this.markAsTouchedAndDirty(this.departamento);
    this.markAsTouchedAndDirty(this.provincia);
    this.markAsTouchedAndDirty(this.distrito);
  }

  markAsTouchedAndDirty(control: AbstractControl): void {
    control.markAsTouched();
  }

  setRange(range: Array<any>): Array<SelectListItem> {
    const arr = range.map(
      (obj: any) =>
        new SelectListItem({
          text: obj.descripcion,
          value: obj.id
        })
    );

    return [...arr];
  }

  selectDepartment(): void {
    this.setNullUbigeo('provincia');
    this.setControl(this.provincia);
    this.setControl(this.distrito);
    this.getProvincias(this.departamento.value);
  }

  selectProvince(): void {
    this.setNullUbigeo('distrito');
    this.setControl(this.distrito);
    this.getDistritos(this.provincia.value);
  }

  selectProfession($event: string): void {
    const idProfession = parseInt($event, null);
    const index = this.professions.findIndex((obj: SelectListItem) => obj.value === idProfession);
    this.professionDescriptionSelect = index !== -1 ? this.professions[index].text : '';
  }

  getDepartamentos(departamentoId?: string, provinciaId?: string): void {
    if (!this.departamentos.length) {
      this.departamentSub = this.ubigeoService.getDepartaments().subscribe((res: Array<IUbigeoResponse>) => {
        this.departamentos = this.setUbigeoData(res);
        if (!!departamentoId) {
          this.getProvincias(departamentoId, provinciaId);
        }
        this.departamentSub.unsubscribe();
      });
    } else {
      if (!!departamentoId) {
        this.getProvincias(departamentoId, provinciaId);
      }
    }
  }

  getProvincias(departamentoId: string, provinciaId?: string): void {
    if (!!departamentoId && !this.provincias.length && !this.provincias.find(item => item.value !== provinciaId)) {
      this.provinceSub = this.ubigeoService.getProvinces(departamentoId).subscribe((res: Array<IUbigeoResponse>) => {
        this.provincias = this.setUbigeoData(res);
        if (!!provinciaId) {
          this.getDistritos(provinciaId);
        }
        this.provinceSub.unsubscribe();
      });
    }
  }

  getDistritos(provinciaId: string): void {
    if (!!provinciaId && !this.distritos.length && !this.distritos.find(item => item.value !== this.distrito.value)) {
      this.setNullUbigeo('distrito');
      this.districtSub = this.ubigeoService.getDistricts(provinciaId).subscribe((res: Array<IUbigeoResponse>) => {
        this.distritos = this.setUbigeoData(res);
        this.districtSub.unsubscribe();
      });
    }
  }

  getProfessions(): void {
    this.professionsSub = this.clientService
      .getProfessions({
        codigoApp: APPLICATION_CODE
      } as IProfessionsDataRequest)
      .subscribe((res: Array<IProfessionsDataResponse>) => {
        this.professions = this.setProfessionData(res);
        this.professionsSub.unsubscribe();
      });
  }

  setControl(control: AbstractControl, value?: string): void {
    if (!!value) {
      control.setValue(value);
    } else {
      control.setValue('');
      control.markAsUntouched();
      control.markAsPristine();
    }
  }

  setNullUbigeo(name: string): void {
    switch (name) {
      case 'provincia':
        if (this.provincias.length) {
          this.provincias = [];
          this.distritos = [];
        }
        break;
      case 'distrito':
        if (this.distritos.length) {
          this.distritos = [];
        }
        break;
      default:
        break;
    }
  }

  setUbigeoData(ubigeos: Array<any>): Array<SelectListItem> {
    const arr = ubigeos.map(
      (obj: any) =>
        new SelectListItem({
          text: obj.descripcion,
          value: obj.ubigeoId
        })
    );

    return [...arr];
  }

  setProfessionData(professions: Array<IProfessionsDataResponse>): Array<SelectListItem> {
    const arr = professions.map(
      (obj: IProfessionsDataResponse) =>
        new SelectListItem({
          text: obj.descripcion,
          value: obj.id
        })
    );

    return [...arr];
  }

  // tslint:disable-next-line:cyclomatic-complexity
  createForm(job?: IClientJobInformationResponse): void {
    this.formCreate = this.fb.group({
      lugarTrabajo: [{ value: (job && job.nombreEmpresa) || '', disabled: false }],
      puesto: [{ value: (job && job.profesionId) || '', disabled: false }],
      rangoSalarialId: [{ value: (job && job.ingresosClienteId) || '', disabled: false }],
      direccion: [{ value: (job && job.direccionTrabajo) || '', disabled: false }],
      telefono: [{ value: (job && job.telefonoTrabajo) || '', disabled: false }],
      departamento: [{ value: (job && job.departamentoId) || void 0, disabled: false }],
      provincia: [{ value: (job && job.provinciaId) || void 0, disabled: false }],
      distrito: [{ value: (job && job.distritoId) || void 0, disabled: false }]
    });
    this.lugarTrabajo = this.formCreate.controls['lugarTrabajo'];
    this.puesto = this.formCreate.controls['puesto'];
    this.rangoSalarialId = this.formCreate.controls['rangoSalarialId'];
    this.direccion = this.formCreate.controls['direccion'];
    this.telefono = this.formCreate.controls['telefono'];
    this.departamento = this.formCreate.controls['departamento'];
    this.provincia = this.formCreate.controls['provincia'];
    this.distrito = this.formCreate.controls['distrito'];
    this.professionDescriptionSelect = (job && job.profesionDescripcion) || '';
  }

  showErrors(control: AbstractControl): boolean {
    return !!control && !isEmpty(control.errors) && (control.touched || control.dirty);
  }

  closeModal(): void {
    this.modalEditar.close();
  }
}
