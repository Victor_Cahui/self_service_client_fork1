import { Component, OnDestroy, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormComponent } from '@mx/components/shared/utils/form';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { MfModal, MfModalMessage } from '@mx/core/ui';
import { NotificationStatus } from '@mx/core/ui/lib/components/alerts/custom-alert/custom-alert.component';
import { AuthService } from '@mx/services/auth/auth.service';
import { UserAdminService } from '@mx/services/auth/user-admin.service';
import { GENERAL_MAX_LENGTH, GENERAL_MIN_LENGTH, SUCCESS_CODE } from '@mx/settings/constants/general-values';
import { LOCAL_STORAGE_KEYS } from '@mx/settings/constants/key-values';
import { PROFILE_HEADER } from '@mx/settings/constants/router-titles';
import { AuthLang } from '@mx/settings/lang/auth.lang';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { IUpdatePasswordRequest } from '@mx/statemanagement/models/auth.interface';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { of, Subject, Subscription } from 'rxjs';
import { catchError, filter, finalize, map, switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'client-modal-change-password',
  templateUrl: './modal-change-password.component.html'
})
export class ModalChangePasswordComponent implements OnDestroy {
  @ViewChild(MfModal) mfModal: MfModal;
  @ViewChild(MfModalMessage) mfModalMessage: MfModalMessage;

  form: FormGroup;
  currentPassword: AbstractControl;

  lang = AuthLang;
  generalLang = GeneralLang;
  header = PROFILE_HEADER;
  generalMaxLength = GENERAL_MAX_LENGTH.password;
  generalMinLength = GENERAL_MIN_LENGTH.password;
  warningText: string;

  showLoading: boolean;
  showWarming: boolean;
  showModal: boolean;

  showModalMessage: boolean;
  messageSuccessUpdate = GeneralLang.Messages.SuccessUpdate;
  messageDone = GeneralLang.Messages.Done;

  changePassword: Subscription;
  auth: IdDocument;

  private readonly _arrToDestroy = [];
  public isValidCurrentPassword = false;
  public isValidatingPassword = true;
  text: Subject<string> = new Subject<string>();
  private _prevPassword: string;
  public validatingPassword = false;
  status = NotificationStatus.INFO;

  constructor(
    private readonly fb: FormBuilder,
    private readonly _AuthService: AuthService,
    private readonly _LocalStorage: LocalStorageService,
    private readonly userAdminService: UserAdminService,
    private readonly authService: AuthService
  ) {
    this.createForm();
    this.auth = this.authService.retrieveEntity();
  }

  onCurrentPassword(txt: string): void {
    // TODO: previous request can not cancel even if you use switchMap ⚰️
    // Can not use async validation in form control. Angular issue https://github.com/angular/angular/issues/20052
    const { documentNumber: username } = this._LocalStorage.getData(LOCAL_STORAGE_KEYS.USER);
    const login$ = of(txt)
      .pipe(
        tap(() => (this.validatingPassword = false)),
        filter(pass => !!pass && this._prevPassword !== pass && this.generalMinLength <= pass.length),
        tap(curr => (this._prevPassword = curr)),
        switchMap(password =>
          this._AuthService
            .login({ grant_type: '', client_id: '', username, password })
            .pipe(catchError(() => of(false)))
        ),
        map(isValid => !!isValid),
        finalize(() => (this.validatingPassword = true))
      )
      .subscribe(isValid => {
        this.isValidCurrentPassword = isValid;
        isValid
          ? document.querySelector('div.g-input-design').classList.remove('msn-error')
          : document.querySelector('div.g-input-design').classList.add('msn-error');
      });

    this._arrToDestroy.push(login$);
  }

  createForm(): void {
    this.form = this.fb.group({
      currentPassword: ['', Validators.compose([Validators.minLength(this.generalMinLength), Validators.required])]
    });

    this.currentPassword = this.form.get('currentPassword');
  }

  action(confirm: boolean): void {
    if (!confirm) {
      this.showModal = false;
    } else {
      this.updatePassword();
    }
  }

  private updatePassword(): void {
    const body: IUpdatePasswordRequest = {
      documentNumber: this.auth.number,
      documentType: this.auth.type,
      newPassword: this.form.getRawValue().changePassword.newPassword,
      oldPassword: this.form.getRawValue().currentPassword
    };
    this.showLoading = true;
    this.changePassword = this.userAdminService.updatePassword(body).subscribe(
      data => {
        this.showLoading = false;
        if (data && data.operationCode === SUCCESS_CODE) {
          this.showModal = false;
          this.openSuccesModal();
        } else {
          this.showWarming = true;
        }

        this.warningText = data.message;
      },
      () => {
        this.showLoading = false;
        this.showWarming = false;
      }
    );
  }

  ngOnDestroy(): void {
    if (this.changePassword) {
      this.changePassword.unsubscribe();
    }
    this._arrToDestroy.forEach(subscriber => subscriber.unsubscribe && subscriber.unsubscribe());
  }

  showErrors(controlKey: string): boolean {
    return FormComponent.showBasicErrors(this.form, controlKey);
  }

  open(): void {
    this.showModal = true;
    this.showModalMessage = false;
    this.showWarming = false;
    this.createForm();
    setTimeout(() => {
      this.mfModal.open();
    }, 100);
  }

  private openSuccesModal(): void {
    this.showModalMessage = true;
    setTimeout(() => {
      this.mfModalMessage.open();
    }, 100);
  }
}
