import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { InputWithIconModule } from '@mx/components/shared';
import { MfInputModule, MfShowErrorsModule } from '@mx/core/ui';
import { HealthSearchProviderComponent } from './health-search-provider.component';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, MfInputModule, MfShowErrorsModule, InputWithIconModule],
  declarations: [HealthSearchProviderComponent],
  exports: [HealthSearchProviderComponent]
})
export class HealthSearchProviderModule {}
