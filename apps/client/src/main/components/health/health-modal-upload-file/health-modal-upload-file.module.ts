import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UploadFilesModule } from '@mx/components/shared/upload-files/upload-files.module';
import { MfModalModule } from '@mx/core/ui';
import { HealthSearchProviderModule } from '../health-search-provider/health-search-provider.module';
import { HealthModalUploadFileComponent } from './health-modal-upload-file.component';

@NgModule({
  imports: [CommonModule, UploadFilesModule, MfModalModule, HealthSearchProviderModule],
  declarations: [HealthModalUploadFileComponent],
  exports: [HealthModalUploadFileComponent]
})
export class HealthModalUploadFileModule {}
