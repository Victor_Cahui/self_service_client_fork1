import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { IUploadFileList, UploadFilesComponent } from '@mx/components/shared/upload-files/upload-files.component';
import { MfModal } from '@mx/core/ui';
import { AuthService } from '@mx/services/auth/auth.service';
import { HealthRefundService } from '@mx/services/health/health-refund.service';
import { Y_AXIS } from '@mx/settings/constants/general-values';
import { HEALTH_REFUND_REQUEST_HEADER } from '@mx/settings/constants/router-titles';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { REFUND_HEALT_LANG } from '@mx/settings/lang/health.lang';
import { IFileTypeRefund, IRefundImageStorage } from '@mx/statemanagement/models/health.interface';
import { Subscription } from 'rxjs';
import { HealthRefunUploadBase } from '../health-refund-upload-base';
import { HealthSearchProviderComponent } from '../health-search-provider/health-search-provider.component';

@Component({
  selector: 'client-health-modal-upload-file',
  templateUrl: './health-modal-upload-file.component.html'
})
export class HealthModalUploadFileComponent extends HealthRefunUploadBase implements OnInit, OnChanges, OnDestroy {
  @ViewChild(MfModal) modal: MfModal;
  @ViewChild('upload') upload: UploadFilesComponent;
  @ViewChild(HealthSearchProviderComponent) searchProvider: HealthSearchProviderComponent;

  @Output() nextCloseModal: EventEmitter<any> = new EventEmitter<any>();
  @Output() nextConfirmModal: EventEmitter<Array<IRefundImageStorage>> = new EventEmitter<Array<IRefundImageStorage>>();

  @Input() maxSize: number;
  @Input() maxFiles: number;
  @Input() minFiles = 1;
  @Input() type: IFileTypeRefund;
  @Input() inValidFiles: Array<IRefundImageStorage>;

  form: FormGroup;
  formSub: Subscription;
  lang = REFUND_HEALT_LANG.StepTwo;
  generalLang = GeneralLang;
  title = HEALTH_REFUND_REQUEST_HEADER.title;
  yAxis = Y_AXIS;

  numeroRuc: number;

  tmpListFile: Array<IRefundImageStorage> = [];
  inValidFileNames: Array<string>;

  constructor(
    protected healthRefundService: HealthRefundService,
    protected authService: AuthService,
    private readonly fb: FormBuilder
  ) {
    super(healthRefundService, authService);
  }

  ngOnInit(): void {
    this.form = this.fb.group({});
    this.formSub = this.form.valueChanges.subscribe((val: any) => (this.numeroRuc = val.provider.ruc));
    setTimeout(() => {
      this.setDefaultProvider();
    });
  }

  ngOnDestroy(): void {
    if (this.formSub) {
      this.formSub.unsubscribe();
    }
  }

  ngOnChanges(): void {
    this.inValidFileNames = (this.inValidFiles || []).map(file => file.name);
  }

  private setDefaultProvider(): void {
    this.searchProvider.setDefault(
      this.refund.attention.ruc,
      this.refund.attention.social,
      this.refund.attention.verify
    );
  }

  completeLoad(tmpKey: number): void {
    this.processCompleteLoad(tmpKey, this.upload.listFile);
  }

  onCloseModal(): void {
    const numbers = this.tmpListFile.map(file => file.numberFile);
    this.removeFileService(null, this.type, numbers);
    this.nextCloseModal.next();
  }

  afterAddFile(data: IUploadFileList, numberFile: number): void {
    // INFO: Almacena metadata temporalmente.
    const payload = {
      size: data.file.size,
      name: data.file.name,
      numberFile,
      type: data.file.type,
      tmpKey: data.tmpKey
    };

    this.tmpListFile.push(payload);
  }

  afterRemoveFile(tmpKey: number): void {
    const index = this.tmpListFile.findIndex(item => item.tmpKey === tmpKey);
    this.tmpListFile.splice(index, 1);
  }

  onConfirm(res: boolean): void {
    if (res) {
      this.tmpListFile = this.tmpListFile.map(item => {
        item.ruc = Number(this.form.controls['provider'].get('ruc').value);
        item.social = this.form.controls['provider'].get('social').value;

        return item;
      });
      this.nextConfirmModal.next(this.tmpListFile);
      this.nextCloseModal.next();
      this.modal.resetPage();
    }
  }
}
