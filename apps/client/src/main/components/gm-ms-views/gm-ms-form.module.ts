import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { InputWithIconModule } from '@mx/components/shared';
import { ModalHowWorksModule } from '@mx/components/shared/modals/modal-how-works/modal-how-works.module';
import {
  MfButtonModule,
  MfInputDatepickerModule,
  MfInputModule,
  MfSelectModule,
  MfShowErrorsModule
} from '@mx/core/ui';
import { MfCustomAlertModule } from '@mx/core/ui/lib/components/alerts/custom-alert';
import { TooltipsModule } from '@mx/core/ui/lib/components/tooltips/tooltips.module';

@NgModule({
  imports: [
    CommonModule,
    ModalHowWorksModule,
    MfInputDatepickerModule,
    MfInputModule,
    MfSelectModule,
    InputWithIconModule,
    ReactiveFormsModule,
    MfShowErrorsModule,
    MfButtonModule,
    TooltipsModule,
    MfCustomAlertModule
  ],
  exports: [
    CommonModule,
    ModalHowWorksModule,
    MfInputDatepickerModule,
    MfInputModule,
    MfSelectModule,
    InputWithIconModule,
    ReactiveFormsModule,
    MfShowErrorsModule,
    MfButtonModule,
    TooltipsModule,
    MfCustomAlertModule
  ]
})
export class GmMsSubstituteFormModule {}
