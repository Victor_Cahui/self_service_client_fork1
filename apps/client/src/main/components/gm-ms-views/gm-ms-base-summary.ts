import { DatePipe, DecimalPipe, DOCUMENT } from '@angular/common';
import { ElementRef, Inject, OnInit, Renderer2, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ResizeService } from '@mx/core/shared/helpers/events/resize.service';
import { PlatformService } from '@mx/core/shared/helpers/util/platform.service';
import { MfModalMessage } from '@mx/core/ui';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { GeneralService } from '@mx/services/general/general.service';
import { ServicesMFService } from '@mx/services/services-mf.service';
import { WARNING_ICON } from '@mx/settings/constants/images-values';
import { SERVICE_FORM_STYLE } from '@mx/settings/constants/key-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { Subscription } from 'rxjs';
import { GmMsBase } from './gm-ms-base';

export class GmMsBaseSummary extends GmMsBase implements OnInit {
  @ViewChild(MfModalMessage) mfModalSuccess: MfModalMessage;

  generalLang = GeneralLang;
  serviceLang: any;
  confirm: boolean;
  auth: IdDocument;
  showModal: boolean;
  showModalError: boolean;
  invalidTime: boolean;
  serviceSub: Subscription;
  icon: string;
  iconError = WARNING_ICON;

  modalTitle: string;
  modalMessage: string;
  modalTitleError: string;
  modalMessageError: string;

  hourRequest: string;
  dateRequest: Date;
  ampmRequest: string;

  constructor(
    protected render2: Renderer2,
    protected elementRef: ElementRef,
    protected platformService: PlatformService,
    protected resizeService: ResizeService,
    protected generalService: GeneralService,
    protected clientService: ClientService,
    protected authService: AuthService,
    protected servicesMfService: ServicesMFService,
    protected decimalPipe: DecimalPipe,
    protected datePipe: DatePipe,
    protected router: Router,
    @Inject(DOCUMENT) protected document: Document
  ) {
    super(
      render2,
      elementRef,
      platformService,
      resizeService,
      generalService,
      document,
      SERVICE_FORM_STYLE.WORK_DEFAULT
    );
  }

  ngOnInit(): void {
    this.attr_class.concat(' d-none');
    this.auth = this.authService.retrieveEntity();
    this.getHoursDefault();
    this.setClassSizeMap();
  }

  cancelRequest(): void {
    this.close();
  }

  goToServiceList(): void {
    this.close();
    this.router.navigate(['/mapfre-services/assists']);
  }
}
