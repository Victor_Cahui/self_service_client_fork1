import { DOCUMENT, Location } from '@angular/common';
import { AfterViewInit, ElementRef, EventEmitter, Inject, Output, Renderer2, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ResizeService } from '@mx/core/shared/helpers/events/resize.service';
import { PlatformService } from '@mx/core/shared/helpers/util/platform.service';
import { SelectListItem } from '@mx/core/ui';
import { MfInputComponent } from '@mx/core/ui/lib/components/forms/input/input.component';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { GeneralService } from '@mx/services/general/general.service';
import { IHeader } from '@mx/services/general/header-helper.service';
import { GoogleMapService, GoogleMapsEvents } from '@mx/services/google-map/google-map.service';
import {
  AM_PM,
  DEFAULT_HOME_DOCTOR_HOURS,
  DEFAULT_SUBSTITUTE_DRIVER_HOURS,
  GENERAL_MAX_LENGTH,
  GENERAL_MIN_LENGTH
} from '@mx/settings/constants/general-values';
import { HOME_DOCTOR_HOURS, SERVICE_FORM_STYLE, SUBSTITUTE_DRIVER_HOURS } from '@mx/settings/constants/key-values';
import { SERVICES_TYPE } from '@mx/settings/constants/services-mf';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { HowHomeDoctorWorks, HowSubstituteDriverWorks, ServiceFormLang } from '@mx/settings/lang/services.lang';
import { IClientAddressResponse } from '@mx/statemanagement/models/client.models';
import { IModalHowWorks } from '@mx/statemanagement/models/general.models';
import { ISearchPlace } from '@mx/statemanagement/models/google-map.interface';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { isEmpty } from 'lodash-es';
import { Subscription } from 'rxjs';
import { ModalHowWorksComponent } from '../shared/modals/modal-how-works/modal-how-works.component';
import { FormComponent } from '../shared/utils/form';
import { GmMsBase } from './gm-ms-base';
declare var Cleave;

export abstract class GmMsFormBase extends GmMsBase implements AfterViewInit {
  @ViewChild(ModalHowWorksComponent) mfModalHowWorks: ModalHowWorksComponent;
  @ViewChild('inputHour') inputHour: MfInputComponent;

  @Output() openSearch: EventEmitter<any> = new EventEmitter<any>();
  @Output() openSummary: EventEmitter<any> = new EventEmitter<any>();

  serviceType: Array<SelectListItem> = [];

  currentPlaceTypeSearch: string;
  openModalHowWorks: boolean;
  form: FormGroup;

  googleSub: Subscription;
  auth: IdDocument;

  HowItWorks: IModalHowWorks;
  warningText = ServiceFormLang.WarningText;
  lang = ServiceFormLang;
  generalLang = GeneralLang;
  currentTime = AM_PM.AM;
  maxLength = GENERAL_MAX_LENGTH;
  minLength = GENERAL_MIN_LENGTH;
  header: IHeader;

  minDate: Date;
  warningTextDate: string;
  dateError: boolean;

  disableDate: boolean;

  constructor(
    protected render2: Renderer2,
    protected elementRef: ElementRef,
    protected platformService: PlatformService,
    protected resizeService: ResizeService,
    protected generalService: GeneralService,
    protected googleMapService: GoogleMapService,
    protected location: Location,
    protected authService: AuthService,
    protected clientService: ClientService,
    protected formBuilder: FormBuilder,
    @Inject(DOCUMENT) protected document: Document,
    protected formStyle: SERVICE_FORM_STYLE
  ) {
    super(render2, elementRef, platformService, resizeService, generalService, document, formStyle);
    this.auth = this.authService.retrieveEntity();
    this.setClassSizeMap();
    this.getHoursDefault();
    this.createForm();
    this.onChangeDateHour();

    const userData = this.clientService.getUserData();
    if (!isEmpty(this.form.controls['phone'])) {
      this.form.controls['phone'].setValue(userData.telefonoMovil || userData.telefonoFijo || '');
    }

    this.googleSub = this.googleMapService.onEvent().subscribe(res => {
      if (res && res.event === GoogleMapsEvents.CONFIRM_PLACE) {
        this.onSearchConfirm(res.data);
      }
    });
  }

  ngAfterViewInit(): any {
    if (!this.inputHour) {
      return;
    }

    this.inputHour.input_native.nativeElement.classList.add('input-hour');

    return new Cleave('.input-hour', {
      time: true,
      timePattern: ['h', 'm'],
      timeFormat: '12',
      onValueChanged: (e: any) => {
        if (e.target && e.target.value) {
          this.form.controls['hour'].setValue(e.target.value);
        }
      }
    });
  }

  private addAddressCorrespondence(): void {
    const addressCorrespondence = this.generalService.getCorrespondenceAddress();
    if (!addressCorrespondence) {
      this.clientService.getCorresponseAddress({}).subscribe((clientAddress: IClientAddressResponse) => {
        this.generalService.setCorrespondenceAddress(clientAddress);
        if (!isEmpty(this.form.controls['endPoint'])) {
          this.form.controls['endPoint'].setValue(clientAddress.direccion);
        }
      });
    } else {
      if (!isEmpty(this.form.controls['endPoint'])) {
        this.form.controls['endPoint'].setValue(addressCorrespondence.direccion);
      }
    }
  }

  changeAmPm(): void {
    if (!this.disableDate) {
      this.currentTime = this.currentTime === AM_PM.AM ? AM_PM.PM : AM_PM.AM;
      this.form.controls['am_pm'].setValue(this.currentTime);
    }
  }

  showErrors(control: string): boolean {
    return (
      (this.form.controls[control].dirty || this.form.controls[control].touched) &&
      !isEmpty(this.form.controls[control].errors)
    );
  }

  close(): void {
    this.location.back();
  }

  formatValue(controlName: string): void {
    FormComponent.removeSpaces(this.form.controls[controlName]);
  }

  fnOpenSearch(type?: string): void {
    this.currentPlaceTypeSearch = type;
    if (type === 'start') {
      const data: ISearchPlace = {
        coords: this.form.controls['startPointCoords'].value,
        address: this.form.controls['startPoint'].value
      };

      if (data.address) {
        data.valid = true;
        this.googleMapService.setSearchMarker(data);
        this.googleMapService.eventEmit(GoogleMapsEvents.SET_START_MARKER, data);
      }
      this.googleMapService.setSearchMarkerDefault(data);
      this.googleMapService.setSearchPlaceEventMarker(GoogleMapsEvents.SET_START_MARKER);
      this.googleMapService.setSearchPlaceTitle(this.lang.LocateStartPoint);
    } else {
      const data: ISearchPlace = {
        coords: this.form.controls['endPointCoords'].value,
        address: this.form.controls['endPoint'].value
      };
      if (data.address) {
        data.valid = true;
        this.googleMapService.setSearchMarker(data);
        this.googleMapService.eventEmit(GoogleMapsEvents.SET_END_MARKER, data);
      }
      this.googleMapService.setSearchMarkerDefault(data);
      this.googleMapService.setSearchPlaceEventMarker(GoogleMapsEvents.SET_END_MARKER);
      this.googleMapService.setSearchPlaceTitle(this.lang.LocateEndPoint);
    }
    this.openSearch.next();
  }

  onSearchConfirm(data: ISearchPlace): void {
    if (this.currentPlaceTypeSearch === 'start') {
      this.form.controls['startPoint'].setValue(data.address);
      this.form.controls['startPointCoords'].setValue(data.coords);
    } else {
      this.form.controls['endPoint'].setValue(data.address);
      this.form.controls['endPointCoords'].setValue(data.coords);
    }
  }

  // Modal Informativo
  viewInfo(): void {
    this.openModalHowWorks = true;
    setTimeout(() => {
      this.mfModalHowWorks.open();
    }, 100);
  }

  setInfoModal(service: string): void {
    switch (service) {
      case SERVICES_TYPE.SUBSTITUTE_DRIVER:
        this.setValuesModal(HowSubstituteDriverWorks, SUBSTITUTE_DRIVER_HOURS, DEFAULT_SUBSTITUTE_DRIVER_HOURS);
        break;
      case SERVICES_TYPE.HOME_DOCTOR:
        this.setValuesModal(HowHomeDoctorWorks, HOME_DOCTOR_HOURS, DEFAULT_HOME_DOCTOR_HOURS);
        break;
      default:
        this.setValuesModal(HowSubstituteDriverWorks, SUBSTITUTE_DRIVER_HOURS, DEFAULT_SUBSTITUTE_DRIVER_HOURS);
        break;
    }
  }

  setValuesModal(data: IModalHowWorks, key: string, defaultHours: number): void {
    this.HowItWorks = data;
    this.generalService.getParameters().subscribe(() => {
      let hours = this.generalService.getValueParams(key);
      hours = hours ? hours : defaultHours.toString();
      if (this.HowItWorks.warning) {
        this.HowItWorks.warning = this.HowItWorks.warning.replace('{{hours}}', hours);
      }
    });
  }

  onChangeDateHour(): void {
    if (
      isEmpty(this.form.controls['hour']) &&
      isEmpty(this.form.controls['date']) &&
      isEmpty(this.form.controls['am_pm'])
    ) {
      return;
    }

    this.warningTextDate = HowSubstituteDriverWorks.warning.replace('{{hours}}', `${this.maxHours}`);
    const hour = this.form.controls['hour'];
    const date = this.form.controls['date'];
    const am_pm = this.form.controls['am_pm'];

    this.setMinDateWithHours();
    this.minDate = new Date();
    this.minDate.setHours(0, 0, 0, 0);

    date.setValue(new Date(+this.minDateWithHours));

    hour.valueChanges.subscribe(() => {
      this.validateDateHour();
    });

    date.valueChanges.subscribe(() => {
      this.validateDateHour();
    });

    am_pm.valueChanges.subscribe(() => {
      this.validateDateHour();
    });
  }

  validateDateHour(): void {
    const hourF = this.form.controls['hour'];
    const dateF = this.form.controls['date'];
    const am_pmF = this.form.controls['am_pm'];
    const dateError =
      hourF.valid && dateF.valid && am_pmF.valid
        ? this.validateMinHours(hourF.value, dateF.value, am_pmF.value)
        : false;

    setTimeout(() => (this.dateError = dateError));
  }

  abstract createForm(): void;
}
