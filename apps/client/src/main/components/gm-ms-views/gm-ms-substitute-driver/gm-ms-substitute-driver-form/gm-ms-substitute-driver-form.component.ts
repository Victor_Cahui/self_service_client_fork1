import { DOCUMENT, Location } from '@angular/common';
import { Component, ElementRef, Inject, OnInit, Renderer2 } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ResizeService } from '@mx/core/shared/helpers/events/resize.service';
import { PlatformService } from '@mx/core/shared/helpers/util/platform.service';
import { StringUtil } from '@mx/core/shared/helpers/util/string';
import { NotificationStatus } from '@mx/core/ui/lib/components/alerts/custom-alert/custom-alert.component';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { GeneralService } from '@mx/services/general/general.service';
import { GoogleMapService } from '@mx/services/google-map/google-map.service';
import { ServicesMFService } from '@mx/services/services-mf.service';
import { APPLICATION_CODE, REG_EX } from '@mx/settings/constants/general-values';
import { SERVICE_FORM_STYLE } from '@mx/settings/constants/key-values';
import { SUBSTITUTE_DRIVER_HEADER } from '@mx/settings/constants/router-titles';
import { SERVICES_TYPE } from '@mx/settings/constants/services-mf';
import { GeneralRequest } from '@mx/statemanagement/models/general.models';
import { IVehicleSubstituteDriverResponse } from '@mx/statemanagement/models/service.interface';
import { Subscription } from 'rxjs';
import { GmMsFormBase } from '../../gm-ms-base-form';

@Component({
  selector: 'client-gm-ms-substitute-driver-form',
  templateUrl: './gm-ms-substitute-driver-form.component.html'
})
export class GmMsSubstituteDriverFormComponent extends GmMsFormBase implements OnInit {
  currentServiceSelected: IVehicleSubstituteDriverResponse;

  vehiclesTypeResponse: Array<IVehicleSubstituteDriverResponse> = [];
  vehiclesSub: Subscription;
  service = SERVICES_TYPE.SUBSTITUTE_DRIVER;

  warningAlert: boolean;
  errorStatus = NotificationStatus.WARNING;
  warningStatus = NotificationStatus.INFO;

  constructor(
    protected render2: Renderer2,
    protected elementRef: ElementRef,
    protected platformService: PlatformService,
    protected resizeService: ResizeService,
    protected generalService: GeneralService,
    protected location: Location,
    protected googleMapService: GoogleMapService,
    protected authService: AuthService,
    protected clientService: ClientService,
    protected formBuilder: FormBuilder,
    private readonly serviceMFService: ServicesMFService,
    @Inject(DOCUMENT) protected document: Document
  ) {
    super(
      render2,
      elementRef,
      platformService,
      resizeService,
      generalService,
      googleMapService,
      location,
      authService,
      clientService,
      formBuilder,
      document,
      SERVICE_FORM_STYLE.WORK_DEFAULT
    );
  }

  ngOnInit(): void {
    this.header = SUBSTITUTE_DRIVER_HEADER;
    this.setInfoModal(this.service);
    this.getVehicles();
    this.onVehicleTypeChange();
  }

  onDestroy(): void {
    if (this.googleSub) {
      this.googleSub.unsubscribe();
    }
    if (this.vehiclesSub) {
      this.vehiclesSub.unsubscribe();
    }
  }

  createForm(): void {
    this.form = this.formBuilder.group({
      date: [null, Validators.required],
      hour: [null, [Validators.required, Validators.pattern(/(((0[1-9])|(1[0-2])):([0-5])([0-9]))$/)]],
      am_pm: [this.currentTime, Validators.required],
      startPoint: [null, [Validators.required, Validators.minLength(this.minLength.sd_start_point)]],
      startPointCoords: [null],
      referenceStartPoint: [
        null,
        [
          Validators.required,
          Validators.maxLength(this.maxLength.sd_start_point),
          Validators.minLength(this.minLength.sd_start_point),
          Validators.pattern(REG_EX.noEmojis)
        ]
      ],
      endPoint: [null, [Validators.required, Validators.minLength(this.minLength.sd_start_point)]],
      endPointCoords: [null],
      referenceEndPoint: [
        null,
        [
          Validators.required,
          Validators.maxLength(this.maxLength.sd_start_point),
          Validators.minLength(this.minLength.sd_start_point),
          Validators.pattern(REG_EX.noEmojis)
        ]
      ],
      vehicleType: [null, Validators.required],
      phone: [
        null,
        [
          Validators.required,
          Validators.maxLength(this.maxLength.sd_phone_contact),
          Validators.minLength(this.minLength.sd_phone_contact),
          Validators.pattern(/^[+]{0,1}[0-9]{0,18}$/)
        ]
      ]
    });
  }

  getVehicles(): void {
    const params: GeneralRequest = {
      codigoApp: APPLICATION_CODE
    };
    this.vehiclesSub = this.serviceMFService.getVehiclesSubstituteDriver(params).subscribe(response => {
      this.vehiclesTypeResponse = response || [];

      response.forEach(resp => {
        if (resp.producto && resp.producto.identificadorProducto) {
          const type = {
            text: resp.producto.nombreProducto,
            value: resp.producto.identificadorProducto,
            selected: true,
            disabled: false
          };

          this.serviceType.push(type);
        }
      });
    });
  }

  onVehicleTypeChange(): void {
    this.form.controls['vehicleType'].valueChanges.subscribe(value => {
      if (this.vehiclesTypeResponse && this.vehiclesTypeResponse.length) {
        this.currentServiceSelected = this.vehiclesTypeResponse.find(
          vehicle => vehicle.producto.identificadorProducto === Number(value)
        );
        if (this.currentServiceSelected) {
          // TODO: diff swagger/mapfre - condicional para efectos de vista
          if (this.currentServiceSelected.beneficios) {
            this.currentServiceSelected.beneficio = this.currentServiceSelected.beneficios;
          }
          this.currentServiceSelected.precio.monedaSimbolo = StringUtil.getMoneyDescription(
            this.currentServiceSelected.precio.codigoMoneda
          ).concat(' ');
        }
      }
    });
  }

  goToSummary(): void {
    this.openSummary.next({ form: this.form.value, vehicle: this.currentServiceSelected });
  }
}
