import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfAlertWarningModule, MfModalMessageModule } from '@mx/core/ui';
import { MfButtonModule } from '@mx/core/ui/lib/components/forms/button';
import { GmMsSubstituteDriverSummaryComponent } from './gm-ms-substitute-driver-summary.component';

@NgModule({
  imports: [CommonModule, MfButtonModule, MfModalMessageModule, MfAlertWarningModule],
  declarations: [GmMsSubstituteDriverSummaryComponent],
  exports: [GmMsSubstituteDriverSummaryComponent]
})
export class GmMsSubstituteDriverSummaryModule {}
