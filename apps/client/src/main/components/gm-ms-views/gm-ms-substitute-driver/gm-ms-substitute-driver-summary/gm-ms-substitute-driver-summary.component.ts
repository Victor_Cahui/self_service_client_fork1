import { DatePipe, DecimalPipe, DOCUMENT } from '@angular/common';
import { Component, ElementRef, Inject, Renderer2 } from '@angular/core';
import { Router } from '@angular/router';
import { ResizeService } from '@mx/core/shared/helpers/events/resize.service';
import { FormatDDMMMYYYY } from '@mx/core/shared/helpers/util/date';
import { PlatformService } from '@mx/core/shared/helpers/util/platform.service';
import { AuthService } from '@mx/services/auth/auth.service';
import { ClientService } from '@mx/services/client.service';
import { GeneralService } from '@mx/services/general/general.service';
import { ServicesMFService } from '@mx/services/services-mf.service';
import { APPLICATION_CODE, FORMAT_DATE_YYYYMMDD } from '@mx/settings/constants/general-values';
import { DRIVER_ICON } from '@mx/settings/constants/images-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { PaymentLang } from '@mx/settings/lang/payment.lang';
import { ServiceFormLang, ServicesLang } from '@mx/settings/lang/services.lang';
import { GeneralRequest } from '@mx/statemanagement/models/general.models';
import { ISubstituteDriverRequest, ISummaryView } from '@mx/statemanagement/models/service.interface';
import { GmMsBaseSummary } from '../../gm-ms-base-summary';

@Component({
  selector: 'client-gm-ms-substitute-driver-summary',
  templateUrl: './gm-ms-substitute-driver-summary.component.html',
  providers: [DatePipe, DecimalPipe]
})
export class GmMsSubstituteDriverSummaryComponent extends GmMsBaseSummary {
  data: ISummaryView;
  body: ISubstituteDriverRequest;

  constructor(
    protected render2: Renderer2,
    protected elementRef: ElementRef,
    protected platformService: PlatformService,
    protected resizeService: ResizeService,
    protected generalService: GeneralService,
    protected clientService: ClientService,
    protected authService: AuthService,
    protected servicesMfService: ServicesMFService,
    protected decimalPipe: DecimalPipe,
    protected datePipe: DatePipe,
    protected router: Router,
    @Inject(DOCUMENT) protected document: Document
  ) {
    super(
      render2,
      elementRef,
      platformService,
      resizeService,
      generalService,
      clientService,
      authService,
      servicesMfService,
      decimalPipe,
      datePipe,
      router,
      document
    );
    this.icon = DRIVER_ICON;
    this.modalTitle = ServicesLang.Titles.SubstituteDriverRequestSuccess;
    this.modalMessage = ServicesLang.Messages.SubstituteDriverRequestSuccess;
    this.modalTitleError = GeneralLang.Messages.EstimatedUser;
    this.modalMessageError = ServicesLang.Messages.InvalidDate;
    this.serviceLang = ServiceFormLang;
  }

  setViewData(values): void {
    // Para validar fecha
    this.hourRequest = values.form.hour;
    this.dateRequest = values.form.date;
    this.ampmRequest = values.form.am_pm;

    // Vista
    const shortDate = this.datePipe.transform(this.dateRequest, FORMAT_DATE_YYYYMMDD);

    this.data = {
      driver: this.serviceLang.ToBeAssigned,
      vehicle: values.vehicle.producto.nombreProducto,
      date: `${FormatDDMMMYYYY(shortDate)} - ${values.form.hour} ${values.form.am_pm.toUpperCase()}`,
      from: values.form.startPoint,
      referenceFrom: values.form.referenceStartPoint,
      to: values.form.endPoint,
      referenceTo: values.form.referenceEndPoint,
      phone: values.form.phone,
      price:
        values.vehicle.precio.precio === 0
          ? PaymentLang.Texts.Free.toUpperCase()
          : `${values.vehicle.precio.monedaSimbolo} ${this.decimalPipe.transform(
              values.vehicle.precio.precio,
              '1.2-2'
            )}`
    };

    // Request
    this.body = {
      fechaRecojo: shortDate,
      horaRecojo: `${values.form.hour} ${values.form.am_pm.toUpperCase()}`,
      origen: {
        puntoPartida: values.form.startPoint,
        referenciaPartida: values.form.referenceStartPoint,
        latitud: values.form.startPointCoords.latitude,
        longitud: values.form.startPointCoords.longitude
      },
      destino: {
        puntoLlegada: values.form.endPoint,
        referenciaLlegada: values.form.referenceEndPoint,
        latitud: values.form.endPointCoords.latitude,
        longitud: values.form.endPointCoords.longitude
      },
      producto: {
        identificadorProducto: values.vehicle.producto.identificadorProducto,
        numeroPoliza: values.vehicle.producto.numeroPoliza,
        nombreProducto: values.vehicle.producto.nombreProducto,
        tipoCliente: this.clientService.getUserData().tipoUsuario || ''
      },
      pago: {
        codigoMoneda: values.vehicle.precio.codigoMoneda,
        precioReal: values.vehicle.precio.precio,
        precio: values.vehicle.precio.precio,
        esGratis: values.vehicle.precio.precio === 0
      },
      telefono: values.form.phone
    };
  }

  confirmRequest(): void {
    this.showModalError = false;
    this.showModal = false;

    // Fecha válida
    this.setMinDateWithHours();
    this.invalidTime = this.validateMinHours(this.hourRequest, this.dateRequest, this.ampmRequest);

    if (this.invalidTime) {
      const date = new Date();
      const timeText = `${this.datePipe.transform(date, 'shortTime')}`;
      this.modalMessageError = this.modalMessageError.replace('{{dateTime}}', timeText);
      this.showModalError = true;
      setTimeout(() => {
        this.mfModalSuccess.open();
      }, 50);

      return;
    }
    // Enviar Solicitud
    this.auth = this.authService.retrieveEntity();
    const params: GeneralRequest = {
      codigoApp: APPLICATION_CODE
    };
    this.serviceSub = this.servicesMfService.substituteDriverRequest(this.body, params).subscribe(
      response => {
        if (response) {
          this.showModal = true;
          setTimeout(() => {
            this.mfModalSuccess.open();
          }, 50);
        }
      },
      () => {},
      () => {
        this.serviceSub.unsubscribe();
      }
    );
  }
}
