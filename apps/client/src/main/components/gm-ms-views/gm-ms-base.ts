import { DOCUMENT } from '@angular/common';
import { ElementRef, EventEmitter, HostBinding, Inject, Input, OnDestroy, Output, Renderer2 } from '@angular/core';
import { ResizeService } from '@mx/core/shared/helpers/events/resize.service';
import { AddHours, isGreaterThanDate } from '@mx/core/shared/helpers/util/date';
import { PlatformService } from '@mx/core/shared/helpers/util/platform.service';
import { GeneralService } from '@mx/services/general/general.service';
import { AM_PM, DEFAULT_SUBSTITUTE_DRIVER_HOURS } from '@mx/settings/constants/general-values';
import { SERVICE_FORM_STYLE, SUBSTITUTE_DRIVER_HOURS } from '@mx/settings/constants/key-values';
import { IParameterApp } from '@mx/statemanagement/models/general.models';
import { Observable } from 'rxjs';

export class GmMsBase implements OnDestroy {
  @HostBinding('attr.class') attr_class = 'g-bg-c--white1 h-100 g-box--shadow1';

  maxHours: number;
  minDateWithHours: Date;
  parametersSubject: Observable<Array<IParameterApp>>;

  private _show: boolean;
  @Input() lightbox: boolean;

  @Input()
  set show(show: boolean) {
    this._show = show;
    this.handlerShow(show);
  }

  get show(): boolean {
    return this._show;
  }

  @Output() exit: EventEmitter<any>;

  constructor(
    protected render2: Renderer2,
    protected elementRef: ElementRef,
    protected platformService: PlatformService,
    protected resizeService: ResizeService,
    protected generalService: GeneralService,
    @Inject(DOCUMENT) protected document: Document,
    protected formStyle: SERVICE_FORM_STYLE
  ) {
    this.exit = new EventEmitter<any>();
    setTimeout(() => {
      this.handlerSize();
    }, 10);
    setTimeout(() => {
      this.handlerShow(this.show);
    }, 10);
    this.resizeService.resizeEvent.subscribe(() => {
      this.handlerSize();
    });
  }

  ngOnDestroy(): void {
    this.onDestroy();
  }

  onDestroy(): void {}

  handlerSize(): void {
    if (this.formStyle === SERVICE_FORM_STYLE.WORK_DEFAULT) {
      const isDesktop = this.platformService.isDesktop;
      this.render2[isDesktop ? 'addClass' : 'removeClass'](this.elementRef.nativeElement, 'g-map-filter--column');
      this.render2[!isDesktop ? 'addClass' : 'removeClass'](this.elementRef.nativeElement, 'g-map-filter--full');
    }
  }

  handlerShow(show: boolean): void {
    this.render2[show ? 'removeClass' : 'addClass'](this.elementRef.nativeElement, 'd-none');
    this.render2[show ? 'addClass' : 'removeClass'](this.elementRef.nativeElement, 'd-block');
  }

  close(): void {
    this.exit.next();
  }

  handlerBodyClass(className: string, add = true): void {
    const headerElements = this.document.getElementsByTagName('html');
    if (headerElements && headerElements.length) {
      this.render2[add ? 'addClass' : 'removeClass'](headerElements[0], className);
    }
  }

  // Verifica Hora indicada dentro de lo permitido
  validateMinHours(time: string, date: Date, am_pm: string): boolean {
    const timeA = time.split(':');

    let hour = Number(timeA[0]);
    hour = am_pm === AM_PM.AM ? (hour === 12 ? 0 : hour) : hour === 12 ? hour : hour + 12;

    date.setHours(hour);
    date.setMinutes(Number(timeA[1]));

    return !isGreaterThanDate(date, this.minDateWithHours);
  }

  // Obtiene horas de antelacion
  getHoursDefault(): void {
    this.parametersSubject = this.generalService.getParametersSubject();
    this.parametersSubject.subscribe(params => {
      const hours = this.generalService.getValueParams(SUBSTITUTE_DRIVER_HOURS);
      this.maxHours = Number(hours || DEFAULT_SUBSTITUTE_DRIVER_HOURS);
    });
  }

  setMinDateWithHours(): void {
    this.minDateWithHours = AddHours(new Date(), this.maxHours);
  }

  setClassSizeMap(): void {
    const isDesktop = this.platformService.isDesktop;
    this.attr_class = this.formStyle === SERVICE_FORM_STYLE.WORK_DEFAULT ? this.attr_class : '';
    const cssStyleDesktop = this.formStyle === SERVICE_FORM_STYLE.WORK_DEFAULT ? ' g-map-filter--column' : '';
    const cssStyleNotDesktop = this.formStyle === SERVICE_FORM_STYLE.WORK_DEFAULT ? ' g-map-filter--full' : '';
    this.attr_class = this.attr_class.concat(isDesktop ? cssStyleDesktop : cssStyleNotDesktop);
  }
}
