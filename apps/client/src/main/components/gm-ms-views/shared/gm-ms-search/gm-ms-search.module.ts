import { AgmCoreModule } from '@agm/core';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GmFilterNotifierModule } from '@mx/components/shared/gm-filter-notifier/gm-filter-notifier.module';
import { InputSearchModule } from '@mx/components/shared/input-search/input-search.module';
import { MfButtonModule } from '@mx/core/ui';
import { GOOGLE_MAP_API_KEY } from '@mx/settings/constants/general-values';
import { GmMsSubstituteDriverSearchComponent } from './gm-ms-search.component';

@NgModule({
  imports: [
    CommonModule,
    InputSearchModule,
    MfButtonModule,
    GmFilterNotifierModule,
    AgmCoreModule.forRoot({
      apiKey: GOOGLE_MAP_API_KEY,
      libraries: ['places']
    })
  ],
  declarations: [GmMsSubstituteDriverSearchComponent],
  exports: [GmMsSubstituteDriverSearchComponent]
})
export class GmMsSearchModule {}
