import { MapsAPILoader } from '@agm/core';
import {
  AfterViewInit,
  Component,
  EventEmitter,
  HostBinding,
  Input,
  NgZone,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { BaseGoogleMaps } from '@mx/components/google-maps/base-google-maps';
import { InputSearchComponent } from '@mx/components/shared/input-search/input-search.component';
import { PlatformService } from '@mx/core/shared/helpers/util/platform.service';
import { GoogleMapService, GoogleMapsEvents } from '@mx/services/google-map/google-map.service';
import { GENERAL_MAX_LENGTH, GENERAL_MIN_LENGTH } from '@mx/settings/constants/general-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { SearchPlaceLang } from '@mx/settings/lang/services.lang';
import {
  GoogleResultAutocomplete,
  IAbrevCoords,
  ICoords,
  ISearchPlace
} from '@mx/statemanagement/models/google-map.interface';
declare var google;

@Component({
  selector: 'client-gm-ms-search',
  templateUrl: './gm-ms-search.component.html',
  providers: [PlatformService]
})
export class GmMsSubstituteDriverSearchComponent implements OnInit, AfterViewInit {
  @ViewChild(InputSearchComponent) inputSearch: InputSearchComponent;
  @HostBinding('class.g-map--active') inputFocus: boolean;

  @Output() exit: EventEmitter<any> = new EventEmitter<any>();
  @Input() title: string;
  @Input() poligons: Array<Array<IAbrevCoords>>;

  geocodeService: any;
  autocompleteService: any;
  placeService: any;
  coincidences: Array<GoogleResultAutocomplete>;

  showNotify: boolean;
  searchText = '';
  currentCoords: ICoords;

  maxLength = GENERAL_MAX_LENGTH.sd_start_point;
  minLength = GENERAL_MIN_LENGTH.sd_start_point;
  notifyText: string;

  generalLang = GeneralLang;
  lang = SearchPlaceLang;

  defaultPlace: ISearchPlace;

  constructor(
    private readonly mapsAPILoader: MapsAPILoader,
    private readonly googleMapService: GoogleMapService,
    private readonly platformService: PlatformService,
    private readonly ngZone: NgZone
  ) {}

  ngOnInit(): void {
    this.mapsAPILoader.load().then(() => {
      this.loadService();
    });

    this.title = this.title || this.googleMapService.getSearchPlaceTitle();
    this.googleMapService.getSearchMarker().subscribe(res => {
      this.defaultPlace = this.googleMapService.getSearchMarkerDefault();
      if (res) {
        this.coincidences = [];
        if (res.coords && res.coords.latitude && res.valid) {
          this.searchText = res.address;
          this.currentCoords = res.coords;
          this.showNotify = false;
        } else {
          this.searchText = '';
          this.fnShowNotify(res.valid !== undefined && !res.valid);
        }
      }
    });

    this.googleMapService.onEvent().subscribe(res => {
      if (res && res.event === GoogleMapsEvents.MAP_CLICK) {
        this.coincidences = [];
        this.inputSearch.input_native.nativeElement.blur();
        this.googleMapService.eventEmit(GoogleMapsEvents.ZOOM_CONTROL, true);
      }
    });
  }

  ngAfterViewInit(): void {
    this.inputSearch.input_native.nativeElement.focus();
  }

  onFocus(): void {
    if (this.platformService.isMobile) {
      this.googleMapService.eventEmit(GoogleMapsEvents.ZOOM_CONTROL, false);
    }
    setTimeout(() => (this.inputFocus = true));
  }

  onBlur(): void {
    this.googleMapService.eventEmit(GoogleMapsEvents.ZOOM_CONTROL, true);
    setTimeout(() => (this.inputFocus = false));
  }

  fnExit(clear = true): void {
    if (!clear) {
      this.googleMapService.eventEmit(GoogleMapsEvents.CONFIRM_PLACE, {
        coords: this.currentCoords,
        address: this.searchText
      });
    } else {
      this.googleMapService.eventEmit(this.googleMapService.getSearchPlaceEventMarker(), this.defaultPlace);
    }

    this.searchText = '';
    this.currentCoords = null;
    this.googleMapService.setSearchMarker(null);
    this.googleMapService.setSearchPlaceEventMarker(null);
    this.googleMapService.eventEmit(GoogleMapsEvents.APPLY_BOUNCE, true);
    this.exit.next();
  }

  loadService(): void {
    if (!this.geocodeService && google) {
      this.geocodeService = new google.maps.Geocoder(this.googleMapService.getMap());
    }
    if (!this.autocompleteService && google && google.maps.places) {
      this.autocompleteService = new google.maps.places.AutocompleteService();
    }
    if (!this.placeService && google && google.maps.places) {
      this.placeService = new google.maps.places.PlacesService(this.googleMapService.getMap());
    }
  }

  searchInput(text?: string): void {
    // tslint:disable-next-line: no-parameter-reassignment
    text = (text || '').trim();
    this.currentCoords = text ? this.currentCoords : null;

    if (text.length >= this.minLength && text !== this.searchText) {
      this.getPlacePredictions(text);
    }

    if (text.length < this.minLength) {
      this.coincidences = [];
    }

    this.searchText = text;
  }

  getPlacePredictions(text?: string): void {
    this.loadService();
    this.autocompleteService.getPlacePredictions(
      {
        input: text || this.searchText,
        componentRestrictions: { country: 'pe' }
      },
      (response: Array<any>) => {
        this.ngZone.run(() => {
          this.handlerResponseGoogleAutocomplete(response);
        });
      }
    );
  }

  handlerResponseGoogleAutocomplete(response: Array<GoogleResultAutocomplete>): void {
    this.coincidences = response || [];
  }

  searchPlace(): void {
    this.coincidences = [];
    this.loadService();
    this.geocodeService.geocode(
      {
        address: this.searchText,
        componentRestrictions: { country: 'pe' }
      },
      (response: Array<any>) => {
        this.ngZone.run(() => {
          this.handlerResponseGoogle(response);
        });
      }
    );
  }

  handlerResponseGoogle(response: Array<any>): void {
    if (response && response.length && response[0].formatted_address !== 'Perú') {
      const coords: ICoords = {
        latitude: response[0].geometry.location.lat(),
        longitude: response[0].geometry.location.lng()
      };

      if (BaseGoogleMaps.isInsidePoligon(coords, this.poligons)) {
        this.searchText = response[0].formatted_address;
        this.currentCoords = coords;
      } else {
        this.currentCoords = null;
        this.fnShowNotify(true);
      }
      this.googleMapService.eventEmit(this.googleMapService.getSearchPlaceEventMarker(), { coords });
    } else {
      this.currentCoords = null;
      this.fnShowNotify();
    }
  }

  selectAutocomplete(item: GoogleResultAutocomplete): void {
    this.coincidences = [];
    this.placeService.getDetails(
      { placeId: item.place_id, fields: ['formatted_address', 'geometry'] },
      (response: any) => {
        this.ngZone.run(() => {
          response.formatted_address = item.description;
          this.handlerResponseGoogle([response]);
        });
      }
    );
  }

  fnShowNotify(inValidZone?: boolean): void {
    this.showNotify = false;
    this.notifyText = inValidZone ? this.lang.InvalidZone : this.lang.WarmingPlace;
    setTimeout(() => (this.showNotify = true), 10);
  }

  handlerkeyDown(event): void {
    if (event.keyCode === 13 && this.searchText && this.searchText.length >= this.minLength) {
      this.searchPlace();
    }
  }
}
