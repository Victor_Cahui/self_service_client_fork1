import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfAlertWarningModule, MfButtonModule, MfModalMessageModule } from '@mx/core/ui';
import { GmMsHomeDoctorSummaryComponent } from './gm-ms-home-doctor-summary.component';

@NgModule({
  imports: [CommonModule, MfButtonModule, MfModalMessageModule, MfAlertWarningModule],
  declarations: [GmMsHomeDoctorSummaryComponent],
  exports: [GmMsHomeDoctorSummaryComponent]
})
export class GmMsHomeDoctorSummaryModule {}
