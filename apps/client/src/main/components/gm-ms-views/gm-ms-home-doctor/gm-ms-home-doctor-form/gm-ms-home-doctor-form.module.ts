import { AgmCoreModule } from '@agm/core';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GmFilterNotifierModule } from '@mx/components/shared/gm-filter-notifier/gm-filter-notifier.module';
import { GmFilterService } from '@mx/components/shared/gm-filter/providers/gm-filter.service';
import { InputSearchModule } from '@mx/components/shared/input-search/input-search.module';
import { DirectivesModule, MfButtonModule, MfCheckboxModule, MfTextAreaModule } from '@mx/core/ui';
import { StepperModule } from '@mx/core/ui/lib/components/stepper/stepper.module';
import { GOOGLE_MAP_API_KEY } from '@mx/settings/constants/general-values';
import { GmMsSubstituteFormModule } from '../../gm-ms-form.module';
import { GmMsHomeDoctorFormComponent } from './gm-ms-home-doctor-form.component';

@NgModule({
  imports: [
    GmMsSubstituteFormModule,
    MfCheckboxModule,
    MfTextAreaModule,
    StepperModule,
    CommonModule,
    InputSearchModule,
    DirectivesModule,
    MfButtonModule,
    GmFilterNotifierModule,
    AgmCoreModule.forRoot({
      apiKey: GOOGLE_MAP_API_KEY,
      libraries: ['places']
    })
  ],
  declarations: [GmMsHomeDoctorFormComponent],
  exports: [GmMsHomeDoctorFormComponent],
  providers: [GmFilterService]
})
export class GmMsHomeDoctorFormModule {}
