import { CurrencyPipe, DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { TRAFFIC_LIGHT_COLOR } from '@mx/components/shared/utils/policy';
import { GAService, GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { ReqQueryGetObtenerListaCartasGarantia } from '@mx/core/shared/providers/models';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { MfModalAlertComponent } from '@mx/core/ui/lib/components/modals/modal-alert/modal-alert.component';
import { GeneralService } from '@mx/services/general/general.service';
import { PoliciesService } from '@mx/services/policies.service';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import { APPLICATION_CODE, COLOR_STATUS } from '@mx/settings/constants/general-values';
import {
  DOCUMENT_ICON,
  DOCUMENT_NOT_AVAILABLE,
  GUARANTEE_LETTERS,
  SEARCH_ICON
} from '@mx/settings/constants/images-values';
import { LOCAL_STORAGE_KEYS } from '@mx/settings/constants/key-values';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { takeUntil } from 'rxjs/operators';

@Component({
  providers: [DatePipe, CurrencyPipe],
  selector: 'client-card-my-guarantee-letters',
  templateUrl: './card-my-guarantee-letters.component.html',
  styleUrls: ['./card-my-guarantee-letters.component.scss']
})
export class CardMyGuaranteeLettersComponent extends GaUnsubscribeBase implements OnInit {
  @ViewChild(MfModalAlertComponent) mfModalAlert: MfModalAlertComponent;

  listGuaranteeLetters: any[];
  statusList: any[];
  itemsPerPage = 10;
  showLoading = true;
  page = 1;
  total: number;
  lang = GeneralLang;
  showFilter: boolean;
  titleGuaranteeLetters = GeneralLang.Titles.GuaranteeLetters;
  trafficLightColor = TRAFFIC_LIGHT_COLOR;
  colorStatus = COLOR_STATUS;
  iconNotFound = DOCUMENT_ICON;
  msgNotFound = GeneralLang.Messages.GuaranteeLettersItemNotFound;

  guaranteeIcon = GUARANTEE_LETTERS.GUARANTEE_LETTER_DOWNLOAD;
  userIcon = GUARANTEE_LETTERS.USER_ICON;
  clinicIcon = GUARANTEE_LETTERS.CLINIC_ICON;
  calendarIcon = GUARANTEE_LETTERS.CALENDAR_ICON;

  iconTooltip = 'icon-mapfre_048_tooltip g-c--gray7 g-font--20';
  // Filters
  textoBuscar = '';
  estado = 0;
  clinicaId = 0;
  fechaIngresoInicial: any;
  fechaIngresoFinal: any;

  isBusinessAccount = false;
  clinicList: any[] = [];
  minDate: string;
  maxDate: string;

  iconModal = DOCUMENT_NOT_AVAILABLE;
  altTextIconModal = 'Document not available';
  titleModal = this.lang.Titles.DocumentNotAvailable;
  subTitleModal = this.lang.Messages.DownloadNotAvailable;

  constructor(
    protected generalService: GeneralService,
    protected policiesService: PoliciesService,
    protected policiesInfoService: PoliciesInfoService,
    protected gaService: GAService,
    private readonly _Autoservicios: Autoservicios,
    private readonly localStorage: LocalStorageService
  ) {
    super();
  }

  ngOnInit(): void {
    const logged_user = this.localStorage.getData(LOCAL_STORAGE_KEYS.USER);
    if (logged_user.userType === '2') {
      this.isBusinessAccount = true;
    }
    this._initValues();
  }

  itemsPerPageChange(itemsPerPage?: number): void {
    this.page = 1;
    this.itemsPerPage = itemsPerPage || this.itemsPerPage;
    this._configNotFound();
  }

  pageChange(page: number): void {
    this.page = page;
    this._configNotFound();
  }

  onStatusChange(ev: any): void {
    this.estado = ev.target.value;
    this.clinicaId = 0;
    this.getGuaranteeLetters();
  }

  onClinicChange(ev: any): void {
    this.estado = 0;
    this.clinicaId = ev.target.value;
    this.getGuaranteeLetters();
  }

  onStartDateChange(ev: any): void {
    this.fechaIngresoInicial = ev.target.value;
    if (this.fechaIngresoFinal && this.fechaIngresoFinal !== null && this.fechaIngresoFinal !== '') {
      this.estado = 0;
      this.clinicaId = 0;
      this.getGuaranteeLetters();
    }
  }

  onEndDateChange(ev: any): void {
    this.fechaIngresoFinal = ev.target.value;
    if (this.fechaIngresoInicial && this.fechaIngresoInicial !== null && this.fechaIngresoInicial !== '') {
      this.estado = 0;
      this.clinicaId = 0;
      this.getGuaranteeLetters();
    }
  }

  getGuaranteeFilters(): void {
    const bodyReq: any = {
      codigoApp: APPLICATION_CODE
    };

    this._Autoservicios
      .ObtenerFiltros(bodyReq)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(
        res => {
          if (res) {
            this.statusList = res.estados;
            this.clinicList = res.clinicas;
            this.fechaIngresoFinal = res.rangoFecha.fechaFinal;
            this.fechaIngresoInicial = res.rangoFecha.fechaInicial;
            this.maxDate = res.rangoFecha.fechaFinal;
            this.minDate = res.rangoFecha.fechaInicial;
          }
        },
        err => {
          console.error(err);
        }
      );
  }

  getGuaranteeClinics(): void {
    if (this.isBusinessAccount) {
      const bodyReq: any = {
        codigoApp: APPLICATION_CODE
      };

      this._Autoservicios
        .ObtenerClinica(bodyReq)
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(
          res => {
            if (res) {
              this.clinicList = res;
            }
          },
          err => {
            console.error(err);
          }
        );
    }
  }

  getGuaranteeLetters(): void {
    this.showLoading = true;

    const bodyReq: ReqQueryGetObtenerListaCartasGarantia = {
      codigoApp: APPLICATION_CODE,
      textoBuscar: this.textoBuscar,
      estado: this.estado,
      clinicaId: this.clinicaId,
      fechaIngresoInicial: this.fechaIngresoInicial,
      fechaIngresoFinal: this.fechaIngresoFinal
    };

    const cartasGarantia$ = this._Autoservicios
      .ObtenerListaCartasGarantia(bodyReq)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(
        res => {
          if (res) {
            this.listGuaranteeLetters = res.listaResultados;
            this.total = res.totalRegistros;
          } else {
            this.listGuaranteeLetters = [];
            this.total = 0;
            if (this.textoBuscar.length > 0) {
              this._configNotFound();
            }
          }
          this.showLoading = false;
        },
        err => {
          this.showLoading = false;
          console.error(err);
        }
      );
    this.arrToDestroy.push(cartasGarantia$);
  }

  ifDownloadNotAllowed(ev: any): void {
    if (ev === false) {
      this.mfModalAlert.open();
    }
  }

  private _configNotFound(): void {
    this.iconNotFound = SEARCH_ICON;
    this.msgNotFound = this.lang.Messages.OppsItemNotFound;
  }

  onPressEnter(ev): void {
    if (ev.keyCode === 13 && this.textoBuscar.length > 0) {
      this.estado = 0;
      this.clinicaId = 0;
      this.getGuaranteeLetters();
    }
  }

  clearAndSearch(): void {
    this.textoBuscar = '';
    this.listGuaranteeLetters = undefined;
    this.getGuaranteeLetters();
  }

  private _initValues(): void {
    this.showLoading = true;
    this.getGuaranteeFilters();
    this.getGuaranteeLetters();
  }

  confirmModal(ev: any): void {
    if (ev) {
      this.mfModalAlert.close();
    }
  }
}
