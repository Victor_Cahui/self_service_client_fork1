import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DirectivesModule, MfCardModule } from '@mx/core/ui';
import { MfLoaderModule } from '@mx/core/ui/lib/components/loader/loader.module';
import { GuaranteeDownloadOptionComponent } from './donwload-option.component';

@NgModule({
  imports: [CommonModule, DirectivesModule, FormsModule, MfCardModule, MfLoaderModule, ReactiveFormsModule],
  declarations: [GuaranteeDownloadOptionComponent],
  exports: [GuaranteeDownloadOptionComponent]
})
export class GuaranteeDownloadOptionModule {}
