import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ReqPathGetDescargarCarta } from '@mx/core/shared/providers/models';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { GUARANTEE_LETTERS } from '@mx/settings/constants/images-values';

@Component({
  selector: 'client-download-option',
  templateUrl: './donwload-option.component.html'
})
export class GuaranteeDownloadOptionComponent implements OnInit {
  @Input() letter: any;
  @Output() check: EventEmitter<any> = new EventEmitter();

  guaranteeIcon = GUARANTEE_LETTERS.GUARANTEE_LETTER_DOWNLOAD;
  guaranteeIconNotAllow = GUARANTEE_LETTERS.GUARANTEE_LETTER_DOWNLOAD_NOT_ALLOW;
  showLoadingOnIcon = false;

  constructor(private readonly _Autoservicios: Autoservicios) {}

  ngOnInit(): void {}

  descargarCarta(): void {
    if (this.letter.puedeDescargar) {
      this.showLoadingOnIcon = true;

      const bodyReq: ReqPathGetDescargarCarta = {
        codCia: this.letter.codCia,
        anio: this.letter.anio,
        correlativo: this.letter.correlativo,
        version: this.letter.version
      };

      this._Autoservicios.DescargarCarta(bodyReq, { codigoApp: APPLICATION_CODE }).subscribe(
        res => {
          if (res) {
            const fileName = `${this.letter.codCia}/${this.letter.anio}/${this.letter.correlativo}/${this.letter.version}`;
            this.downloadPdf(res.base64, fileName);
          }
          this.showLoadingOnIcon = false;
        },
        err => {
          this.showLoadingOnIcon = false;
          console.error(err);
        }
      );
    } else {
      this.check.emit(false);
    }
  }

  downloadPdf(base64String, fileName): void {
    const source = `data:application/pdf;base64,${base64String}`;
    const link = document.createElement('a');
    link.href = source;
    link.download = `${fileName}.pdf`;
    link.click();
  }
}
