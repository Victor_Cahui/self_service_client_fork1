import { NgModule } from '@angular/core';
import { UnderConstructionComponent } from './under-construction.component';

@NgModule({
  imports: [],
  exports: [UnderConstructionComponent],
  declarations: [UnderConstructionComponent],
  providers: []
})
export class UnderConstructionModule {}
