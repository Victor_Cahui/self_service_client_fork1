import { Faq } from '@mx/statemanagement/models/general.models';

export const VEHICLES_INSURANCE_FAQS: Array<Faq> = [
  {
    title: '¿Puedo reportar el siniestro al día siguiente de ocurrido?',
    content: `Es necesario reportar el siniestro en el más breve plazo
     posible de haber ocurrido para que nuestros asesores evalúen el siniestro
     en el lugar de ocurrencia.`
  },
  {
    title: '¿Cuánto tiempo tomará realizar la reparación de mi vehículo en el taller?',
    content: `El tiempo de reparación promedio de un siniestro de daños medios es
     de 12 días, el cual puede variar dependiendo de la disponibilidad de repuestos.`
  },
  {
    title: '¿Cuánto tiempo podría demorar la importación de repuestos?',
    content: `El tiempo depende de cada concesionario, de la marca y el modelo del vehículo, sin embargo el tiempo mínimo es de 30 días.`
  },
  {
    title: '¿Las reparaciones realizadas tienen garantía?',
    content: `Las reparaciones realizadas por nuestros talleres afiliados cuentan con una garantía de 1 año.`
  },
  {
    title: '¿Se utilizarán repuestos originales en las reparaciones?',
    content: `Se podrán utilizar repuestos originales o alternativos de acuerdo a la evaluación de nuestros peritos.
     Los repuestos alternativos cumplen con los mismos estándares de calidad de los repuestos originales y cuentan con 1 año de garantía.`
  },
  {
    title: '¿Mi deducible es el mismo en cualquier taller?',
    content: `Los deducibles varían de acuerdo a la red donde se encuentre el taller. Contamos con 4 redes: preferente 1, preferente 2,
     concesionario 1 y concesionario 2. Para conocer el deducible que aplica revisa tu póliza. Los talleres no afiliados a ninguna red
     cuentan con deducibles mayores.`
  },
  {
    title: '¿Qué documentos se deben presentar ante un choque?',
    content: `Se deben presentar los siguientes documentos, salvo que MAPFRE exonere la presentación de algunos documentos dependiendo
     de las circunstancias del siniestro:
     <ul class="g-list-bullet"><li>Copia legalizada de la denuncia policial</li>
     <li>Copia legalizada del resultado del examen de dosaje etílico</li>
     <li>Copia legalizada del resultado del examen toxicológico</li>
     <li>Copia simple del peritaje de daños</li>
     <li>Copia legalizada de las conclusiones del atestado policial.</li><ul>`
  },
  {
    title: '¿En caso de pérdida total me indemnizarán el valor de la suma asegurada?',
    content: `El valor de la indemnización depende
     de si la póliza fue contratada a valor pactado o valor comercial. Si fuera a valor comercial se indemniza con el valor comercial
     al momento del siniestro, el cual incluye una depreciación. En caso se indemnice a valor pactado, se indemniza hasta el 120% del
     valor comercial al momento del siniestro.`
  }
];
