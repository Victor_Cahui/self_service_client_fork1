import { Faq } from '@mx/statemanagement/models/general.models';

export const LIFE_INSURANCE_FAQS: Array<Faq> = [
  {
    title: '¿Qué es un seguro de vida?',
    content: `Un seguro de vida es un acuerdo entre una persona y la aseguradora que permite definir una cantidad monetaria,
    la cual será entregada al contratante en caso de sobrevivencia del asegurado o a los beneficiarios en caso de que el asegurado
    fallezca o quede en estado de invalidez.`,
    landing: true
  },
  {
    title: '¿Qué tipos de seguro de vida existen y qué me cubren? ',
    content: `
      <strong>1. SEGURO DE RIESGO:</strong> Cubre muerte natural y accidental además de invalidez permanente y total. Este seguro
      se contrata por la cantidad de años que desees.<br>
      <strong>2. SEGURO DE AHORRO:</strong> Este seguro te permite formar un capital para el futuro. Este tipo de seguros te ofrece
      valores garantizados (préstamos y rescates).<br>
      <strong>3. SEGURO MIXTOS:</strong> Te otorga protección para tu familia y además ahorro con rentabilidad.`,
    landing: true
  },
  {
    title: '',
    content: ``,
    landing: true
  },
  {
    title: '¿Qué son los valores garantizados?',
    content: `<p>Es la parte de retorno/ahorro de la póliza otorgada a la que puedes acceder a partir de la segunda anualidad
    transcurrida y cancelada, la cual se visualiza en las Condiciones Particulares de la póliza. Los valores garantizados son: </p>
    <ul class= "g-list-bullet">
      <li><strong>Préstamo:</strong> Es el monto que puedes solicitar a MAPFRE y está sujeto al pago del interés anual indicado en
      las condiciones de tu póliza.</li>
      <li><strong>Reducción del seguro:</strong> En caso tu póliza se encuentre pendiente de pago por más de 180 días y además
      cuentes con valores garantizados, se procederá a la reducción de tu seguro, es decir que tu suma asegurada disminuirá,
      dejando la póliza vigente hasta su vencimiento y sin recibos pendientes. La nueva suma asegurada está establecida en el cuadro
      de valores garantizados de las condiciones particulares de tu póliza.</li>
      <li><strong>Rescate:</strong> Se activa en caso desees retirar los valores ganados de la póliza. El rescate anula la póliza.</li>
    </ul>`,
    landing: true
  },
  {
    title: '¿Cuáles son los seguros de vida que comercializamos?',
    content: `<ul class= "g-list-bullet">
      <li>Plan Privado de Jubilación</li>
      <li>Plan de Ahorro Garantizado</li>
      <li>Convida</li>
      <li>Convida Oro</li>
      <li>Fondo Universitario</li>
      <li>Vivamás</li>
      <li>Mujer Independiente</li>
    </ul>`
  },
  {
    title: '¿Qué beneficios me da mi seguro de vida?',
    content: `<ul class= "g-list-bullet">
      <li><strong>Rentabilidad:</strong> Tu Bono Actual crece por participación de utilidades.</li>
      <li><strong>Flexibilidad:</strong> Tu eliges la suma asegurada y el plazo del seguro de vida.</li>
      <li><strong>Comodidad:</strong> Podrás fraccionar el pago de la prima (mensual, trimestral o semestral) y para tu seguridad, lo
      cargaremos directamente a tu cuenta o tarjeta bancaria.</li>
      <li><strong>Protección inmediata:</strong> Tu familia estará protegida desde el primer día.</li>
      <li><strong>Liquidez:</strong> Tendrás acceso a préstamos y rescates (valores garantizados).</li>
      <li><strong>Libertad:</strong> Podrás elegir a tus beneficiarios y cambiarlos en cualquier momento.</li>
      <li><strong>Alternativas de ahorro y planes en soles y dólares</strong>.</li>
    </ul>`
  },
  {
    title: '¿Qué es el bono de participación de utilidades?',
    content: `Es el beneficio de la póliza que se genera por la rentabilidad obtenida de la inversión que realiza la compañía.`
  },
  {
    title: '¿Cuánto tiempo tengo para pagar mi póliza?',
    content: `El pago debe realizarse de acuerdo a lo pactado en la póliza, y en caso la póliza cumpla 180 días de impaga se
    procederá a anular sin posibilidad a rehabilitación.`
  },
  {
    title: '¿Qué ocurre si anulo mi póliza?',
    content: `Con una póliza de valores garantizados, al anularlo tienes dos opciones: Solicitar el valor de rescate ganado ó
    solicitar la reducción del seguro. En caso solicite el rescate, al monto establecido se le agregará el bono acumulado por
    participación. Si tu póliza no cuenta con valores garantizados, se procederá con la anulación del seguro según tu solicitud
    presentada.`
  }
];
