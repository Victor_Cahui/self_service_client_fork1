import { Faq } from '@mx/statemanagement/models/general.models';

export const SCTR_PENSION_INSURANCE_FAQS: Array<Faq> = [
  {
    title: '¿Qué es un Accidente de Trabajo?',
    content: `Es toda lesión orgánica o perturbación funcional causada en el centro de trabajo o con ocasión del trabajo, por acción
      imprevista, fortuita u ocasional de una fuerza externa, repentina y violenta que obra súbitamente sobre la persona del trabajador
      o debida al esfuerzo del mismo.`
  },
  {
    title: '¿Qué es una enfermedad profesional?',
    content: `Es toda enfermedad permanente o temporal que sobreviene al trabajador como consecuencia directa de la clase de trabajo
      que desempeña o del medio en que se ha visto obligado a trabajar.`
  },
  {
    title: '¿Qué accidentes no se consideran como Accidente de Trabajo?',
    content: `<ul class="g-list-bullet"><li>El que se produce en el trayecto de ida y retorno al centro de trabajo.</li>
      <li>Por participación en riñas u otra acción ilegal.</li>
      <li>Por incumplimiento del trabajador de orden estricta específica del empleador.</li>
      <li>En ocasión de actividades recreativas, deportivas o culturales.</li>
      <li>El que sobrevenga durante permisos, licencias, vacaciones.</li>
      <li>Por uso de sustancias alcohólicas o estupefacientes.</li>
      <li>Los que sean a consecuencia de guerra civil o internacional, conmoción civil o terrorismo y similares.</li>
      <li>Convulsión de la naturaleza (terremoto, maremoto, etc.) Fusión nuclear.</li>
      <li>Lesiones voluntariamente autoinflingidas o autoeliminación o su tentativa.</li></ul>`
  },
  {
    title: '¿Solo cubre eventos ocurridos en el centro de trabajo?',
    content: `También cubre durante la ejecución de órdenes o bajo autoridad del empleador aun fuera del centro y las horas de trabajo.
      Es decir, antes, durante o después de la jornada laboral, si el trabajador se hallara en cualquier centro de trabajo aunque no se
      trate de un centro de trabajo de riesgo.`
  },
  {
    title: '¿Qué hacer en caso de muerte o declaración de invalidez del asegurado?',
    content: `Los beneficiarios deben acercarse a la compañía de seguros y presentar los documentos requeridos para la calificación.`
  },
  {
    title: '¿Cuándo los beneficiarios de este seguro pueden gozar de las coberturas contratadas?',
    content: `El derecho a las pensiones de invalidez se inicia una vez vencido el periodo máximo de subsidio por incapacidad temporal
      cubierto por ESSALUD (11 meses y 10 días)`
  },
  {
    title: '¿Cómo se calculan las pensiones bajo esta cobertura?',
    content: `Para el cálculo de las pensiones se toma como base el promedio de las remuneraciones de los 12 meses anteriores al
      siniestro. En caso el afiliado tenga una vida laboral activa menor a 12 meses, se tomará el promedio de las remuneraciones que
      haya recibido durante su vida laboral.`
  },
  {
    title: '¿El monto de las remuneraciones a declarar tiene un monto máximo o mínimo?',
    content: `Se debe declarar como mínimo la Remuneración Mínima Vital y como máximo,  la Remuneración Máxima Asegurable que la fija
      trimestralmente la SBS, y es la misma para el sistema de AFP.`
  }
];

export const SCTR_SALUD_INSURANCE_FAQS: Array<Faq> = [
  {
    title: '¿Qué hacer en caso de un accidente de un trabajador?',
    content: `Evacuar al trabajador accidentado a cualquiera de las clínicas o centros médicos afiliados y comunicarse con la central
      de emergencia SI24 al 213-3333 opción 1 (Lima) o al 0801-1-1133 (provincias)`
  },
  {
    title: '¿Qué accidentes no se consideran como Accidente de Trabajo?',
    content: `<ul class="g-list-bullet"><li>El que se produce en el trayecto de ida y retorno al centro de trabajo.</li>
      <li>Por participación en riñas u otra acción ilegal.</li>
      <li>Por incumplimiento del trabajador de orden estricta específica del empleador.</li>
      <li>En ocasión de actividades recreativas, deportivas o culturales.</li>
      <li>El que sobrevenga durante permisos, licencias, vacaciones.</li>
      <li>Por uso de sustancias alcohólicas o estupefacientes.</li>
      <li>Los que sean a consecuencia de guerra civil o internacional, conmoción civil o terrorismo y similares.</li>
      <li>Convulsión de la naturaleza (terremoto, maremoto, etc.) Fusión nuclear.</li>
      <li>Lesiones voluntariamente autoinflingidas o autoeliminación o su tentativa.</li></ul>`
  },
  {
    title: '¿Solo cubre eventos ocurridos en el centro de trabajo?',
    content: `También cubre durante la ejecución de órdenes o bajo autoridad del empleador aun fuera del centro y las horas de trabajo.
      Es decir, antes, durante o después de la jornada laboral, si el trabajador se hallara en cualquier centro de trabajo aunque no se
      trate de un centro de trabajo de riesgo.`
  },
  {
    title: '¿Qué lesiones no cubre este seguro?',
    content: `<ul class="g-list-bullet"><li>Lesiones voluntariamente autoinfligidas o tratativa de autoeliminación.</li>
      <li>Accidente de trabajo o enfermedad profesional de los trabajadores que no hubieren sido declarados por la Entidad Empleadora.</li>
      <li>Procedimientos o terapias que no contribuyen a la recuperación o rehabilitación del paciente de naturaleza cosmética, estética o
      suntuaria; cirugías electivas (no recuperativas ni rehabilitadotas); cirugía plástica, odontología de estética, tratamiento de
      periodoncia y ortodoncia; curas de reposo y del sueño, lentes de contacto.</li></ul>`
  },
  {
    title: 'Mi empresa ya viene aportando mensualmente a ESSALUD, ¿debo afiliarme necesariamente al SCTR Salud?',
    content: `Recordar, que esta es una cobertura complementaria, por lo que en adición al aporte que se brinda a ESSALUD, debe contratar
      con una EPS la correspondiente cobertura de salud por accidentes de trabajo.`
  },
  {
    title: '¿Me puedo atender por ESSALUD en caso de accidente de trabajo a pesar de no estar afiliado a ESSALUD?',
    content: `Por la ley de emergencia, todo centro asistencial de ESSALUD o el MINSA tienen la obligación de atender toda emergencia.
      Los gastos incurridos serán luego rembolsados al cliente.`
  },
  {
    title: '¿Este seguro tiene un monto máximo en la atención de mis asegurados?',
    content: `La cobertura del SCTR Salud es al 100%, hasta la total recuperación del trabajador, no aplicando copagos ni deducibles.`
  },
  {
    title: '¿Mi empresa no está obligada a contratar el SCTR, puedo igualmente afiliarme?',
    content: `En caso no se encuentre obligado a contratar el SCTR, puede afiliarse de manera voluntaria, tendiendo los trabajadores los
      mismos beneficios.`
  }
];

export const SCTR_SALUD_PENSION_INSURANCE_FAQS: Array<Array<Faq>> = [
  SCTR_SALUD_INSURANCE_FAQS,
  SCTR_PENSION_INSURANCE_FAQS
];
