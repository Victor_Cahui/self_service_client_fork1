import { Faq } from '@mx/statemanagement/models/general.models';

export const HEALTH_INSURANCE_FAQS: Array<Faq> = [
  {
    title: '¿Qué es una preexistencia?',
    content: `Son todas las enfermedades diagnosticadas antes de contratar el seguro de salud, las mismas que serán excluidas de la póliza
    en caso el asegurado sea nuevo en el sistema o no haya gozado con la cobertura de dichos diagnósticos en su seguro de salud anterior.`,
    landing: true,
    order: 3
  },
  {
    title: '¿A quiénes se considera clientes nuevos en el sistema?',
    content: `Se consideran nuevos en el sistema aquellos clientes que aún no cuentan con un seguro de salud a través de una
    <strong>Aseguradora o EPS</strong>.`,
    landing: true,
    order: 2
  },
  {
    title: '¿Qué es la Continuidad?',
    content: `La continuidad es un derecho que le corresponde a los clientes que migran desde otro plan de salud contratado con una
    Aseguradora o EPS, dentro de los plazos establecidos. La continuidad implica la <strong>cobertura de las enfermedades preexistentes
    diagnosticadas y cubiertas</strong> en el seguro de salud anterior. Además, se <strong>exoneran o descuentan los períodos de carencia
    y espera</strong> para aquellas coberturas disponibles en el plan de salud anterior.`,
    landing: true,
    order: 1
  },
  {
    title: '¿Cuál es el plazo para gestionar la Continuidad?',
    content: `Para gozar del beneficio de continuidad de preexistencias, debes haber solicitado el seguro considerando
    los siguientes plazos:
    <ul class="g-list-bullet">
    <li>Hasta 120 días posteriores desde el cese de tu Seguro de Salud Colectivo anterior
    (contratado con una Compañia de Seguros).</li>
    <li>Hasta 60 días posteriores desde el cese de tu Seguro de Salud Individual
    (contratado con una Compañia de Seguros) o plan EPS anterior.</li>
    </ul>`
  },
  {
    title: '¿Qué es el período de carencia?',
    content: `Es el plazo transcurrido desde el inicio de vigencia de la póliza durante el cual no se brindará cobertura. Cualquier
    enfermedad diagnosticada durante este período será excluida de la póliza. Este plazo no aplica para emergencias accidentales y las
    siguientes enfermedades: apendicitis, accidente cerebro vascular, torsión testicular e infartos al miocardio. Mayor información está
    disponible en las <strong>condiciones generales del producto</strong>.<br><br>
    *En caso de asegurados con continuidad este período es exonerado.`
  },
  {
    title: '¿Qué es el periodo de espera?',
    content: `Es el plazo contabilizado desde la contratación de la póliza durante el cual una cobertura específica se encuentra
    deshabilitada. Se muestran en las <strong>condiciones generales del producto</strong>.<br><br>
    *En caso de asegurados con continuidad este período puede ser descontado o exonerado totalmente en función al tiempo de aseguramiento
    en el plan de salud anterior para aquellas coberturas disponibles en dicho plan.`
  },
  {
    title: '¿Cuáles son los requisitos de contratación para clientes nuevos en el sistema?',
    content: `Se debe completar la Solicitud de Afiliación y Declaración Personal de Salud.`,
    landing: true,
    order: 0
  },
  {
    title: '¿Cuáles son los requisitos de contratación por continuidad?',
    content: `<p>Adicional a la Solicitud de Afiliación y Declaración Personal de Salud, se debe adjuntar:</p>
    1) Constancia de aseguramiento de la CIA de Seguros o EPS anterior.<br>
    2) Copia de póliza o plan de salud anterior.<br>
    3) Reporte de atenciones de todo el grupo familiar que se desea asegurar.`
  }
];
