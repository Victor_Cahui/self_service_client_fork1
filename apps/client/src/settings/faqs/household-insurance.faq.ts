import { Faq } from '@mx/statemanagement/models/general.models';

export const HOUSEHOLD_INSURANCE_FAQS: Array<Faq> = [
  {
    title: '¿Los inmuebles que son utilizados como oficinas pueden adquirir este seguro?',
    content: `En el Perú es común sobre todo en independientes tener una pequeña oficina dentro de su casa, por ejemplo: un estudio
    de abogado. En ese caso podrían tomar el seguro de hogar sin ningún inconveniente. Sin embargo, si la oficina dentro de la casa
    o departamento tiene tránsito continuo de clientes, intercambio de dinero o pagos, etc.; el asegurado deberá tomar un seguro
    tanto para la casa como para el negocio.`,
    order: 0,
    landing: true
  },
  {
    title: '¿Cuáles son los requisitos que debe presentar el cliente para adquirir este seguro?',
    content: `Es muy sencillo, el cliente solo debe declarar el 100% del valor de su propiedad (edificación) y en caso desee asegurar
    el contenido, podrá declarar el 100% del valor sin embargo asegurar parte del mismo. Adicionalmente de ser necesario debe permitir
    que se realice una inspección para evaluar el riesgo al que está expuesta su casa, esto solo para los casos donde la casa está
    por ejemplo en la playa o cuando el contenido por asegurar tiene un monto muy elevado (mayor o igual a US$ 30,000.00).`,
    order: 1,
    landing: true
  },
  {
    title: '¿Qué forma parte de la materia asegurada dentro de un departamento?',
    content: `Forma parte el departamento, la cochera y el depósito.`
  },
  {
    title: '¿Desde qué monto puede uno asegurar su vivienda?',
    content: `No existe un límite mínimo para asegurar sin embargo difícilmente el monto mínimo está por debajo de los US$25mil para
    la edificación y US$5mil para el contenido. Esto está sujeto a las características de la casa o departamento y a los bienes que
    tiene el cliente. En el caso de zonas residenciales donde encontramos casas y edificios de departamentos podemos observar que en
    distritos como Miraflores, San Isidro, La Molina, Surco y San Borja por ejemplo el promedio de edificación oscila entre
    US$120mil y US$ 160mil; en Magdalena, San Miguel, Pueblo Libre, Jesús María, Barranco, Chorrillos y Lince esta entre US$ 80mil y
    US$ 100mil y en Los Olivos, SJM, Independencia esta entre US$40mil y US$65mil.`,
    order: 2,
    landing: true
  },
  {
    title: '¿Las viviendas en proceso de construcción puede ser aseguradas?',
    content: `La construcción en proceso tiene otro seguro, CAR. Por ejemplo, la construcción de un edificio de departamentos tiene
    este seguro hasta que estos estén terminados. El seguro de hogar es posterior a este, se contrata cuando la vivienda ya está
    terminada y pase hacer habitada por el nuevo propietario.`
  },
  {
    title:
      'El seguro es caro, tengo que pagarlo todos los meses y es muy probable que no vea nada por el dinero pagado.',
    content: `Los seguros domiciliarios cuestan menos que los de un auto. Además, usted puede elegir el tipo de financiamiento
    (al contado, mensual, trimestral, inclusive en 4 cuotas sin intereses) y medio de pago que mas le acomode
    (cargo en cuenta o tarjeta de crédito).<br>
    Nuestro seguro domiciliario tiene una serie de beneficios que puede utilizar durante su vigencia, tales como la consulta médica
    a domicilio, atención de emergencias a domicilio, el apoyo por ausencia del servicio doméstico, el diagnóstico de gastos en
    servicios públicos, así como la asistencia domiciliaria por urgencias en gasfitería, electricidad, cerrajería o vidriería.
    Solo los planes más completos le dan estos beneficios.`
  },
  {
    title: 'De todas maneras, para tomar el seguro me exigen que tenga seguridades.',
    content: `Ya no son requisito para tomar un seguro domiciliario. Si su monto asegurado contra robo es muy alto
    (a partir de US$ 30 mil) MAPFRE enviará un especialista para realizar una inspección, y el informe puede que sugiera algunas
    mejoras en seguridad (si no cuenta con ninguna), siendo facultad de la Cía. indicar que se hagan efectivas o no.`
  },
  {
    title: 'Justamente eso, no me gustaría que vaya a mi casa un desconocido a ver que tengo y que no tengo.',
    content: `MAPFRE Perú, a través de su área de Ingeniería de Riesgos, tiene un staff de asesores profesionales que realizan
    este tipo de actividades. Estas personas están preparadas y a la altura de de nuestros clientes. Además, MAPFRE garantiza la
    honestidad y privacidad de la información recogida en una inspección. Solo usted y el área de Ing. de Riesgos,
    conocen el contenido de los informes.`
  },
  {
    title: 'Pero yo vivo en un departamento / condominio, nunca nos van a robar.',
    content: `Si bien vivir en un departamento o condominio es más seguro que las casas, siempre existe la posibilidad de robos.
    Además, para estos casos la cobertura más importante es la de Responsabilidad Civil, ya que al vivir usted en un condominio
    o departamento puede causar por accidente alguna inundación u otros hechos que deriven en daños materiales o personales a
    sus vecinos y esta cobertura lo respalda ante estos hechos haciéndose cargo de los gastos necesarios para reparar los daños.
    Adicionalmente, contar con el servicio de alarma y monitoreo permitirá tener mayor seguridad y protección ante la intención
    maliciosa de terceros de ingresar a su domicilio.`
  },
  {
    title: `Me han dicho que cuando hay un robo y se tiene que reclamar la indemnización a la Cía., esta le pone muchas trabas
    para pagar (facturas de los objetos, los deprecian, etc.)`,
    content: `La Cía. le pedirá las facturas o boletas de los contenidos perdidos, pero si usted no los tiene, pues hay otras
    formas de demostrar que los tuvo; por ejemplo una fotografía donde aparezca el bien, las marcas que deja este en su lugar
    físico o la declaración de este bien al inicio de su seguro, etc.<br>
    En cuanto a la depreciación, algunas de las compañías de mercado aplican algunos factores. MAPFRE considera que el bien tiene
    que ser repuesto por uno de iguales características y que cumpla con las mismas funciones, por lo que la indemnización se
    hace a valor de reposición a nuevo, salvo en los casos en los que el bien sea obsoleto (que ya no se encuentre en el mercado),
    casos en los que la indemnización se hará con un factor de depreciación igual al 10% anual,
    con máximo de 60% contados desde el año de fabricación.`
  },
  {
    title: `En el caso que desee asegurar objetos valiosos, ¿También es necesario entregar una relación de estos a pesar que se
    haya realizado una inspección?`,
    content: `Es recomendable realizar la relación detallada incluyendo monto cuando los objetos valiosos no superen los
    US$ 2,000. Sin embargo, cuando superan los US$ 2,000 es obligatorio realizar la declaración ya que el área de suscripción
    lo solicitara y esto se detallara en la póliza.<br>
    La mejor forma de demostrarlo es que al inicio de la vigencia de su seguro, usted haga una declaración jurada de bienes,
    es decir, una relación de los bienes que valen por encima de US$ 1,000 de esta manera, esta relación forma parte integrante
    de la póliza y no habrá ningún problema para la indemnización en caso de ocurrir un robo.`
  },
  {
    title: 'El banco me está solicitando un seguro para la edificación de mi departamento. ¿Puedo endosarlo?',
    content: `Claro que sí, si es que se tiene una póliza vigente asegurando sólo contenido se puede realizar una inclusión por
    el valor de la edificación del mismo predio (con prima adicional) y se puede realizar el endoso a favor del banco por el
    monto que este le solicita, adicionalmente nosotros podemos incluir las clausulas básicas que el banco exige (sin costo adicional).<br>
    En el caso que no tuviese póliza de hogar vigente, se puede contratar el seguro Hogar Hipotecario el cuál es un producto diseñado
    para este tipo de escenarios.`
  },
  {
    title: '¿Qué incluye los gastos de extinción, preservación y honorarios?',
    content: `Incluye labores de extinción del fuego incluyendo el costo de los medios de extinción, Honorarios Profesionales y
    algún otro gasto en pro de la preservación de los bienes asegurados.`
  },
  {
    title: `¿Cuáles son los requisitos para el derecho indemnizatorio del Uso fraudulento de las tarjetas de crédito del
    asegurado y/o cónyuge?`,
    content: `Para la solicitar la cobertura de este beneficio es necesario dar aviso a los bancos u operadores de la tarjeta de
    crédito o débito, como máximo, dentro de las cuatro (4) horas de ocurrido el robo o sustracción, y se solicite el bloqueo y
    anulación de esa tarjeta; así como la pérdida haya ocurrido  dentro de las cuarentaiocho (48) horas de haber ocurrido el robo
    o sustracción de la tarjeta de crédito o de débito.`
  },
  {
    title: '¿Puedo contratar un seguro de sólo edificación y contratar alarma?',
    content: `Si es posible, la alarma actuará como un disuasivo en el caso que se presente una emergencia como por ejemplo un robo.
    Es importante recalcar que el contratar la alarma no implica la cobertura del contenido de la vivienda, en este caso sólo se
    encontrará asegurado la edificación y no el contenido.`
  },
  {
    title: '¿Qué es el Monitoreo de Alarmas?',
    content: `Es la conexión (telefónica, radial, celular, satelital) del sistema de seguridad electrónico instalado en el predio
    del asegurado con una Central de Monitoreo de Alarmas, que está siendo supervisada las 24 horas del día ante la ocurrencia de
    eventos como: Robo, Incendio, Emergencia Médica, Asalto, etc. Permite la detección inmediata de toda irregularidad que se inicie
    en un inmueble protegido, mediante dispositivos electrónicos específicos, la comunicación automática vía telefónica (u otro medio)
    del suceso a un Centro de Monitoreo de Alarmas y el aviso inmediato tanto a los titulares como a los organismos que permitan el
    control (Policía, Bomberos, Serenazgo, Emergencias Médicas), de dicha irregularidad o emergencia.`,
    order: 3,
    landing: true
  },
  {
    title: '¿Qué pasa ante la ocurrencia de alguno de estos eventos?',
    content: `Los operadores de la Central de Alarmas, enviarán los equipos de reacción correspondientes: Externos (Policía, Bomberos,
    Ambulancia y Serenazgo) e Internos (personal de la empresa que se encargará de acudir al domicilio del asegurado y realizar la
    supervisión correspondiente de los hechos). Paralelamente, se establecerá contacto con las personas indicadas en el Programa
    de Monitoreo.`
  },
  {
    title: '¿Qué es el programa de Monitoreo?',
    content: `Es el documento en el que se encuentra registrada la información de contacto del asegurado ante la ocurrencia de un
    evento. Deberá ser entregada al momento de la solicitud del seguro.`
  },
  {
    title: '¿De qué consta la reacción del Servicio de Alarma y Monitoreo?',
    content: `Ante la ocurrencia de un evento, la compañía procederá a ejecutar el Programa de Monitoreo; sin embargo, se deja establecido
    que los tiempos de respuesta de los servicios públicos (Bomberos, Serenazgo, Policía Nacional) no tienen límites de tiempo, pues el
    tiempo real en llegar al domicilio del asegurado depende de condiciones absolutamente ajenas al control de la compañía. En los casos
    de emergencia que lo ameriten, la compañía enviará en el menor tiempo posible una fuerza de reacción propia al domicilio del asegurado
    para la atención del evento. La intervención de las fuerzas de reacción propias de la compañía, se limitan a llegar al inmueble
    asegurado y coordinar, desde los exteriores, las incidencias del caso, tanto con la central de la compañía como con las fuerzas
    públicas de reacción y lo especificado en el “Programa de Monitoreo”.<br>
    En este punto cabe aclarar que no utilizamos "resguardos", sino "móviles verificadores", los cuales se ocupan de la verificación
    visual exterior del estado de los inmuebles protegidos en caso de alarmas. Estos no portan armas, ya que en la legislación peruana
    cuyo ente responsable es la Dicscamec, informa que los únicos autorizados a repeler el accionar de los delincuentes con armas de
    fuego son las fuerzas Policiales y Armadas. El servicio de móviles de verificación está disponible sólo en Ciudad Lima.`
  },
  {
    title: 'Ya tengo un sistema de alarma instalado. ¿MAPFRE puede hacer el monitoreo?',
    content: `Sí. MAPFRE puede monitorear todo sistema de alarma de mercado, mediante una simple conexión a nuestra Central de Monitoreo.
    También puede actualizarlo o adaptarlo a las necesidades que se planteen, añadiendo nuevas funciones, detectores, cambio de panel de
    alarmas etc.`
  },
  {
    title: '¿Es suficiente protección un kit básico con dos o tres detectores?',
    content: `No puede responderse anticipadamente a esta pregunta. Es necesario hacer una evaluación del domicilio del asegurado y, en
    base a eso, determinar las necesidades del sitio a proteger. Dichas necesidades no sólo se determinan por el tamaño de la propiedad,
    sino también por las posibilidades de acceso a ella, por su entorno (por ej. un terreno abandonado, una casa ocupada ilegalmente al
    fondo o en construcción, etc.), el tipo de riesgo (personas, bienes, o ambos), la velocidad de detección requerida para evitar un daño
    importante (alarma temprana, asalto), etc. La determinación del tipo de sistema tiene directa relación con la protección que finalmente
    se logrará en el sitio, por lo tanto, no es buena práctica tomarlo a la ligera, o evaluar sólo el precio del equipamiento.`
  },
  {
    title: '¿Qué pasa si ante una intrusión a mi domicilio rompen el teclado?',
    content: `Si alguien rompe el teclado de su alarma, el resto del sistema sigue funcionando y operando normalmente. Si estaba activado
    (conectado) el sistema hará sonar las sirenas (si dispone de ellas y dependiendo del tipo de panel instalado) y transmitirá normalmente
    a la Estación Central de Monitoreo la información de todo aquello que detecte.`
  },
  {
    title: '¿Qué pasa en caso de tener animales? ¿El sistema los detecta?',
    content: `Para este caso usted debe comunicarle al asesor comercial que en la propiedad hay animales domésticos, de esta manera se
    instalarán sensores específicos que, gracias a una combinación de tecnologías, interpretan la presencia de dichos animales, reportando
    sólo las intrusiones de personas.`
  }
];
