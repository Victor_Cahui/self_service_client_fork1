import { Faq } from '@mx/statemanagement/models/general.models';

export const EPS_INSURANCE_FAQS: Array<Faq> = [
  {
    title: '¿Qué es una EPS?',
    content: `Las Entidades Prestadoras de Salud, son empresas e instituciones públicas
    o privadas distintas de ESSALUD, cuyo único fin es el de prestar servicios de atención
    para la salud, con infraestructura propia y de terceros, dentro del régimen del Seguro
    Social de Salud, financiando las prestaciones mediante el crédito contra los aportes
    a que se refiere la Ley 26790. Con la publicación de la Ley 29344 reciben la
    denominación de Institución Administradora de Fondos de Aseguramiento en Salud
    (IAFA), sujetándose a los controles de la Superintendencia Nacional de Aseguramiento
    en Salud.
    `
  },
  {
    title: '¿Qué es la Superintendencia Nacional de Salud (SUSALUD)?',
    content: `SUSALUD es un organismo público técnico especializado del
    Sector Salud, adscrito al Ministerio de Salud, con personería
    jurídica de Derecho Público y con autonomía técnica, funcional,
    administrativa, económica y financiera, que ejerce las funciones,
    competencias y facultades establecidas en la Ley 29344 amplía sus facultades,
    incorporando bajo el ámbito de supervisión a las IAFAS, IPRESS y entidades
    que brindan coberturas de salud bajo pago regular y anticipado.`
  },
  {
    title: '¿Qué es el crédito de ESSALUD?',
    content: `
    <p>Se refiere al 2.25% de la planilla del aporte que se realiza a Essalud que aporta
    la entidad empleadora y que es destinado para el pago de las primas.</p>

    <p>Las entidades empleadoras que otorguen coberturas de salud a sus
    trabajadores en actividad, mediante servicios propios o a través de
    planes o programas de salud contratados con entidades Prestadoras de
    Salud (EPS) gozarán de un crédito respecto de las aportaciones por
    afiliación al Seguro Social de Salud (ESSALUD) a que se refiere el
    inciso a) del Artículo 6º de la LMSSS, modificada por la Ley Nº 28791
    del 21.07.2006, que establece que estos aportes son de carácter mensual.</p>

    <p>El aporte de los trabajadores en actividad, incluyendo tanto los que
    laboran bajo relación de dependencia como los socios de cooperativas,
    equivale al 9% de la remuneración o ingreso mensual.<br>
    El importe del crédito: El importe del crédito será equivalente al 25%
    de los aportes por afiliación al Seguro Social de Salud (ESSALUD)
    correspondientes a los trabajadores que gocen de la cobertura ofrecida
    por la propia empresa (servicios propios) o por la EPS de ser el caso.</p>
    `
  },
  {
    title: '¿Qué es la capa simple? Para Afiliados Regulares.',
    content: `Es el conjunto de intervenciones de salud de mayor frecuencia y
    menor complejidad. Pueden ser prestadas por ESSALUD o por las Entidades
    Empleadoras a través de servicios propios o de planes contratados con una
    Entidad Prestadora de Salud.`
  },
  {
    title: '¿Qué es la capa compleja? Para Afiliados Regulares.',
    content: `Es el conjunto de intervenciones de salud de menor frecuencia y mayor
    complejidad (no están consideradas en la capa simple).`
  },
  {
    title: '¿Qué es Cobertura Obligatoria? Para Afiliados Regulares.',
    content: `Comprende la atención de contingencias correspondientes a la capa simple,
    mediante las prestaciones preventivas, promocionales, de recuperación de la salud y
    emergencias incluidas en el Anexo 1 del Decreto Supremo 0009-97-SA, así como, los
    accidentes de trabajo y las enfermedades profesionales no cubiertos por el Seguro
    Complementario de Trabajo de Riesgo.`
  },
  {
    title: '¿Qué es Cobertura Complementaria? Para Afiliados Regulares.',
    content: `Son todas aquellas prestaciones no comprendidas en la Cobertura Obligatoria
    que permiten dar integridad, oportunidad y continuidad a las prestaciones de salud.
    Se encuentra sujeta a los límites, prestaciones y condiciones estipulados libremente
    por las partes, sin que ello implique la pérdida del derecho del trabajador, de mantener
    las mismas en Essalud.`
  },
  {
    title: '¿Qué es aseguramiento universal en salud - (AUS)?',
    content: `
    <p>Con fecha 09 de abril del 2009 fue publicada la Ley Nº 29344, Ley del Marco de
    Aseguramiento Universal en Salud (AUS), su Reglamento aprobado mediante Decreto
    Supremo Nº008-2010 SA, así como por Decreto Supremo Nº 016-2009-SA que aprobó el
    Plan Esencial de Aseguramiento en Salud (PEAS), y sus normas modificatorias y
    complementarias.</p>
    <p>Esta Ley tiene como objeto garantizar el derecho pleno y progresivo de toda persona
    a la seguridad social en salud, la cual le permita acceder a prestaciones de salud sobre
    la base del PEAS.</p>
    `
  },
  {
    title: '¿Qué es el plan esencial de aseguramiento en salud - (PEAS)?',
    content: `El PEAS consiste en la lista priorizada de condiciones asegurables e
    intervenciones que como mínimo son financiadas a todos los asegurados por las
    Instituciones Administradoras de Fondos de Aseguramiento en Salud (IAFAS),
    sean éstas públicas, privadas o mixtas, y contiene garantías explícitas de
    oportunidad y calidad para todos los beneficiarios.`
  },
  {
    title: '¿Qué es Cobertura Obligatoria PEAS? Para Afiliados Potestativos.',
    content: `Comprende las atenciones de acuerdo a lo establecido en la Ley de
    Aseguramiento Universal en Salud (AUS) Ley 29344, y lo establecido como
    enfermedades cubiertas en el detalle del Plan Esencial de Aseguramiento
    en Salud (PEAS).`
  },
  {
    title: '¿Qué es Cobertura Complementaria? Para Afiliados Potestativos.',
    content: `Comprende todas aquellas prestaciones no comprendidas en la Cobertura Obligatoria PEAS.`
  },
  {
    title: '¿Qué es Preexistencia?',
    content: `Es cualquier condición de alteración del estado de salud diagnosticada
    por un profesional médico colegiado, registrada eh la Historia Clínica del
    asegurado y no resuelta en el momento previo a llenar y suscribir la Declaración
    de Salud o, en su defecto, al de la suscripción del contrato.`
  },
  {
    title: '¿La EPS Cubre los diagnósticos preexistentes?',
    content: `
    <p>MAPFRE cubre los diagnósticos preexistentes que se encuentran detallados en el
    listado de enfermedades de capa simple para los afiliados regulares (titular y
    derechohabientes legales). Para aquellos afiliados que provengan de otra EPS,
    se aplicará lo dispuesto en el Decreto Supremo Nº008-2012-SA, Reglamento de la
    Ley Nº29561, Ley que establece la continuidad en la cobertura de preexistencias,
    no aplicando los periodos de carencia ni de espera.</p>
    <p>Asimismo para los afiliados potestativos, hijos entre 18 y 25 años, y padres,
    toda preexistencia incluida en las condiciones asegurables del PEAS son cubiertas.</p>
    `
  },
  {
    title: '¿Quiénes pueden afiliarse a la EPS?',
    content: `Se pueden afiliar los trabajadores activos de una empresa que se encuentren
     en planilla, y sus derechohabientes: cónyuge o concubina, hijos menores a 18 años,
     así como los hijos mayores de 18 años y padres, siempre y cuando se encuentre estipulado
     en el plan de salud.`
  },
  {
    title: '¿Cuáles son los requisitos para el registro del (la) concubino (a)?',
    content: `<ul class="g-list-bullet">
    <li>Copia del formulario 1010 presentado a EsSalud para la inscripción del
    derechohabiente, con el respectivo sello que acredite la inscripción del
    concubino(a) en dicha entidad, o Constancia de Presentación de Alta de
    Derechohabiente del T-Registro – SUNAT.</li>
    <li>Copia del DNI del titular y del asegurado concubino(a).</li>
    <li>Copia del certificado domiciliario de los concubinos, en caso los DNI no sean iguales.</li>
    <li>Copia de declaración jurada de relación de concubinato, firmado por el
    titular y el asegurado concubino(a) (en caso de no contar con el formulario 1010).</li>
    </ul>`
  },
  {
    title: 'Soy un afiliado regular a la EPS ¿Puedo perder el derecho a seguir atendiéndose en EsSalud?',
    content: `No, aquellos trabajadores que se inscribieron a la EPS, no pierden su derecho
    de atención en EsSalud para los diagnósticos de capa compleja.`
  },
  {
    title: '¿Al inscribirme a una EPS, pierdo las prestaciones económicas que otorga EsSalud?',
    content: `No, los subsidios por maternidad, lactancia, e incapacidad temporal seguirán siendo otorgados por EsSalud.`
  },
  {
    title: '¿Existe algún período de espera para recibir atención médica a través de la EPS?',
    content: `
    <p>Los afiliados regulares y sus derechohabientes legales tienen derecho a las prestaciones
    de MAPFRE EPS en capa simple siempre que cuenten con 3 meses de aportación consecutiva o 4
    meses no consecutivos a EsSalud dentro de los 6 meses anteriores al mes que se inició la
    inscripción con MAPFRE EPS. Para los diagnósticos de capa compleja, el período de carencia
    es de 90 días, el cual se empezará a contabilizar desde le fecha de inscripción a la EPS.</p>
    <p>La maternidad por cesárea, aborto no provocado, y complicaciones orgánicas del embarazo
    tienen un período de carencia de 10 meses computados desde la fecha de inscripción al plan
    de salud. Excepcionalmente los casos de aborto no provocado y/o amenaza de aborto podrán
    ser cubiertos si y solo si la afiliada ha iniciado la gestación durante la vigencia del plan de salud.</p>
    <p>Los períodos de carencia para los afiliados potestativos están indicados en su Plan de Salud.</p>
    `
  },
  {
    title: '¿Cuál es el límite de edad para la afiliación de trabajadores y dependientes en una EPS?',
    content: `
    <p>Titular y cónyuge sin límite de edad.</p>
    <p>Hijos hasta los 18 años de edad; si son discapacitados sin límite de edad.</p>
    <p>De manera adicional el plan de salud contratado podría otorgar cobertura para los hijos
    hasta los 25 años de edad, así como para padres según las condiciones indicados en el plan.</p>
    `
  },
  {
    title: '¿Puedo hacer un cambio de plan EPS?',
    content: `
    <p><strong>Opción 1</strong></p>
    <p>De un plan de mayores beneficios a un menor, una vez al año en cualquier momento.
    De un plan menor a uno mayor, sólo en la fecha de renovación, quedando los diagnósticos
    contraídos en la vigencia del contrato anterior limitados a la cobertura por el monto
    máximo del plan menor u original.</p>
    <p><strong>Opción 2</strong></p>
    <p>El cambio o traslado de un plan a otro, sólo se podrá efectuar en la oportunidad de
    que se produzca la renovación del contrato, manteniéndose como límite de suma asegurada,
    para las enfermedades nacidas antes del traslado o cambio, el límite menor entre ambos
    planes.</p>
    `
  },
  {
    title: '¿Cuáles son los documentos para acceder al reembolso?',
    content: `<ul class="g-list-bullet">
    <li>Originales de las boletas de pago a nombre del paciente.</li>
    <li>Recibos por honorarios profesionales a nombre del paciente.</li>
    <li>Copias de las recetas médicas y detalle de medicamentos e insumos.</li>
    <li>Copias de las indicaciones para la realización de análisis de laboratorio o pruebas especiales.</li>
    <li>Copias de los resultados de los análisis de laboratorio o de pruebas especiales.</li>
    <li>Solicitud de beneficios debidamente llenada por el médico tratante.</li>
    <li>Copia del reporte operatorio en caso de intervención.</li>
    <li>Copia del resultado de anatomía patológica.</li>
    <li>Para el reembolso odontológico se debe presentar: odontograma y todas las
    radiografías realizadas. Para el caso de endodoncias se debe presentar radiografía
    de diagnóstico, radiografía de conductometría y radiografí- a de control.</li>
    </ul>`
  },
  {
    title: '¿Cómo presentar un reclamo ante la EPS?',
    content: `
    <p>La presentación de un reclamo ante la EPS puede ser verbal o escrita.</p>
    <p>
    Verbal, mediante comunicación telefónica o formulando el reclamo de manera personal.
    <br>
    Escrita, a través del correo electrónico, página web, o con la presentación de una carta dirigida al Director de la Unidad de EPS.
    <br>
    También se puede presentar de forma escrita a través del formato de Reclamos que facilitan las oficinas de
    MAPFRE y las coordinadoras de MAPFRE en las clínicas.
    <br>
    Asimismo, puede ser presentada personalmente en la entidad empleadora, quienes trasladarán el reclamo a la EPS para su revisión
    y respuesta.
    </p>
    `
  },
  {
    title: '¿Si no afilio a mis dependientes a mi EPS, pueden atenderse en Essalud con normalidad?',
    content: `No, si el titular de Essalud tiene una EPS vigente, sus dependientes también
    tienen que ser afiliados a su Plan, de lo contrario no podrán tener atenciones ambulatorias en Essalud.`
  },
  {
    title: '¿Si dejo de laborar en la empresa, puedo seguir pagando mi EPS para continuar con mi cobertura?',
    content: `No es posible continuar con una EPS si no sigue laborando para la empresa,
    la EPS es un seguro para los trabajadores dentro de su planilla.`
  },
  {
    title: '¿Qué pasa si llego al límite de cobertura de mi nuevo Plan de Salud?',
    content: `En caso el diagnóstico exceda la cobertura del nuevo Plan, la EPS será
    responsable de coordinar la transferencia del paciente a un Hospital del Seguro
    Social de Salud, para cuyo efecto deberá comunicar en forma oportuna e indubitable a EsSalud.`
  },
  {
    title: '¿Me cubre la extracción de la muela de juicio?',
    content: `La cobertura odontológica cubre atenciones de prevención y recuperación. Se cubre la
    extracción si no incluye un procedimiento complicado.`
  },
  {
    title: '¿Puedo afiliar a mi EPS a los hijos de mi cónyuge?',
    content: `Para este tipo de afiliaciones se deben poseer papeles de adopción, ya que los
    derechohabientes deben ser dependientes legales del asegurado.`
  }
];
