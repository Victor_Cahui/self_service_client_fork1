export const ProfileLang = {
  Buttons: {
    MyProfile: 'Mi perfil',
    AddAddress: 'Agregar dirección',
    MyBenefits: 'Conocer mis beneficios',
    Logout: 'Cerrar sesión'
  },
  Labels: {
    DocumentType: 'Tipo de documento',
    DocumentNumber: 'Número de documento',
    Document: 'Documento',
    Address: 'Dirección',
    AddressCompany: 'Urbanización, calle, número y piso',
    AddressSelect: 'Seleccione dirección',
    AddressEdit: 'Editar dirección actual',
    Department: 'Departamento',
    Province: 'Provincia',
    District: 'Distrito',
    Email: 'Nuevo correo electrónico',
    ConfirmEmail: 'Confirmar correo electrónico',
    Company: 'Nombre de la empresa',
    Position: 'Profesión',
    EntryDate: 'Fecha de entrada',
    SalaryRange: 'Rango salarial en dólares:',
    SalaryRangeS: 'Rango salarial en dólares',
    PhoneNumber: 'Teléfono fijo',
    Phone: 'Teléfono',
    Birthday: 'Fecha de nacimiento:',
    RegistrationDate: 'Cliente desde:',
    User: 'Usuario',
    Client: 'Cliente',
    MapfreDolars: 'MAPFRE DÓLARES',
    ComercialName: 'Nombre comercial',
    Reference: 'Referencia',
    UpdateForAllAddress: 'Utilizar esta dirección para el envío de correspondencia de todas mis pólizas.'
  },
  Texts: {
    Email: 'Correo electrónico',
    PoliciesAddress: 'Quiero usar la dirección de mi póliza de Hogar',
    WithoutName: 'Sin información de nombre',
    WithoutSalaryRange: 'Rango salarial: Sin información',
    WithoutPhoneJob: 'Teléfono: Sin información',
    WithoutCompanyAddress: 'Dirección: Sin información',
    WithoutCompanyName: 'Sin información de empresa',
    Welcome: 'Hola'
  },
  Titles: {
    MyData: 'MIS DATOS',
    MyWork: 'MI TRABAJO',
    Enterprise: 'EMPRESA',
    Contractor: 'CONTRATANTE',
    UpdateEmail: 'ACTUALIZAR CORREO ELECTRÓNICO',
    UpdateWork: 'ACTUALIZAR TRABAJO',
    MailAddress: 'DIRECCIÓN DE CORRESPONDENCIA'
  },
  TitlesMobile: {
    UpdateWork: 'MI PERFIL'
  },
  SubtitlesMobile: {
    UpdateWork: 'ACTUALIZAR TRABAJO'
  },
  Messages: {
    EmailNoMatch: 'El correo electrónico ingresado', // complemento para match validator
    AddMailAddress: 'Para recibir correspondencia por favor agrega una dirección',
    WithoutMailAddress: '¡No tienes una dirección asociada!',
    EmailAlreadyExist: `El correo electrónico ingresado pertenece a otro usuario MAPFRE.
        Por favor utilizar una dirección electrónica disponible`,
    ConfirmLogout: '¿Estás seguro que deseas cerrar sesión?'
  }
};

export const MAPFRE_DOLARS = {
  title: '¿Qué son los MAPFRE dólares?',
  content: `Cada año recibes una cantidad de puntos MAPFRE, que son canjeables por nuevos productos o aplicables
            como descuentos en tu prima durante la renovación. La cantidad de MAPFRE dólares que recibes varían
            según tu antigüedad, la cantidad de pólizas contratadas o la puntualidad en los pagos.`
};
