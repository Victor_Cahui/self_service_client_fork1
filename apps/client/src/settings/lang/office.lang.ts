export const KeyServiceOffice = {
  SALES: { key: 'OF_SRV_VENTAS', description: 'Ventas' },
  SERVICE: { key: 'OF_SRV_ATENCION_CLIENTE', description: 'Servicio al cliente' },
  INSPECT: { key: 'OF_SRV_INSP_AUTOS', description: 'Inspección de Vehículo' },
  PAYMENTS: { key: 'OF_SRV_PAGOS', description: 'Pagos' },
  FUNERAL: { key: 'OF_SRV_FUNERARIO', description: 'Gestión de servicios funerarios' },
  PERSONALDATA: { key: 'OF_ACT_DATOS_PERSO', description: 'Actualización de datos personales' }
};

export const EstablishmentTypeClinic = {
  MEDICAL: { key: 'CLI_TIP_EST_CE_MED_MAPFRE', description: 'Centro médico MAPFRE' },
  ODONTOLOGICAL: { key: 'CLI_TIP_EST_CE_ODON', description: 'Centro odontológico' },
  OPHTHALMOLOGICAL: { key: 'CLI_TIP_EST_CE_OFTAL', description: 'Centro oftalmológico' }
};

export const OficceFormFilter = {
  firstFilter: {
    labels: {
      title: 'Tipo de servicio',
      all: 'Todos'
    },
    items: [
      { value: KeyServiceOffice.SERVICE.key, label: KeyServiceOffice.SERVICE.description },
      { value: KeyServiceOffice.INSPECT.key, label: KeyServiceOffice.INSPECT.description },
      { value: KeyServiceOffice.PAYMENTS.key, label: KeyServiceOffice.PAYMENTS.description },
      { value: KeyServiceOffice.SALES.key, label: KeyServiceOffice.SALES.description },
      { value: KeyServiceOffice.FUNERAL.key, label: KeyServiceOffice.FUNERAL.description },
      { value: KeyServiceOffice.PERSONALDATA.key, label: KeyServiceOffice.PERSONALDATA.description }
    ]
  },
  secondFilter: {
    labels: {
      title: 'Ubicación'
    }
  },
  UbigeoFilter: {
    default: 'Ninguna seleccionada'
  }
};

export const ClinicFormFilter = {
  policyFilter: {
    label: 'Seleccione póliza'
  },
  coverageFilter: {
    label: 'Seleccione un tipo de atención',
    all: 'Ninguna seleccionada'
  },
  establishmentTypeFilter: {
    labels: {
      title: 'Tipo de establecimiento',
      all: 'Todos'
    },
    items: [
      { value: EstablishmentTypeClinic.MEDICAL.key, label: EstablishmentTypeClinic.MEDICAL.description },
      { value: EstablishmentTypeClinic.ODONTOLOGICAL.key, label: EstablishmentTypeClinic.ODONTOLOGICAL.description },
      {
        value: EstablishmentTypeClinic.OPHTHALMOLOGICAL.key,
        label: EstablishmentTypeClinic.OPHTHALMOLOGICAL.description
      }
    ]
  },
  UbigeoFilter: {
    labels: {
      title: 'Ubicación'
    }
  }
};

export const OfficeLang = {
  Labels: {
    km: 'km',
    distance: 'km de distancia',
    copayment: 'Copago',
    district: 'Distrito de'
  },
  Links: {
    gmapsDir: 'https://www.google.com/maps/dir/'
  }
};
