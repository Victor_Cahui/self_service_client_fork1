export const EditProfileLang = {
  step1: {
    title: 'DATOS GENERALES',
    messageDependent: `Para solicitar una modificación de nombres, apellidos, fecha de nacimiento, relación, tipo o
    número de documento necesitaremos fotos del documento de identidad como confirmación.`,
    messageProfile: `Para solicitar una modificación de nombres, apellidos, fecha de nacimiento, tipo o número de documento
    necesitaremos fotos de tu documento de identidad como confirmación.`,
    form: {
      LNames: 'Nombres',
      LLastNameFather: 'Apellido paterno',
      LLastNameMother: 'Apellido materno',
      LDateOfBirth: 'Fecha de nacimiento',
      LTypeDocument: 'Tipo de documento',
      LNumberDocument: 'Número de documento',
      LRelation: 'Relación',
      LPhoneOptional: 'Teléfono móvil (opcional)',
      LPhone: 'Teléfono móvil',
      LEmail: 'Correo electrónico (opcional)',
      BCancel: 'Cancelar',
      BSubmit: 'CAMBIAR DATOS'
    }
  },
  step2: {
    title: 'NUEVOS DATOS',
    messagePhoto: `Hemos detectado que deseas cambiar información sensible, necesitamos fotos de tu
    documento de identidad como confirmación.`,
    messageDay1: `Revisaremos tu solicitud y, si todo está correcto, realizaremos el cambio en un plazo de`,
    messageDay2: `días.`,
    messagePex: `Sube una foto de la cara frontal de tu pasaporte para poder hacer el cambio`,
    messageNPex: `Sube una foto de ambas caras de tu documento de identidad para poder hacer el cambio`,
    LFrontal: 'Frontal',
    LReverse: 'Reverso',
    LAdd: 'Agrega o arrastra las imágenes',
    BCancel: 'Cancelar',
    BSubmit1: 'Continuar',
    BSubmit2: 'Enviando',
    errorImageProcess: 'Hubo un problema al procesar su imagen, por favor intente con otra imagen',
    dontRepeatImage: 'No puedes usar la misma imagen dos veces',
    uploadImageDocument: 'Sube una foto de tu documento de identidad para poder hacer el cambio'
  },
  step3: {
    title: 'Solicitud enviada correctamente',
    message1: 'Te contactaremos en un plazo máximo de',
    message2: 'días para confirmar el cambio',
    BSubmit: 'Aceptar'
  }
};
