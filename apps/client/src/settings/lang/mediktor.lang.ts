export const MediktorLang = {
  Labels: {
    WhatNeed: '¿QUÉ NECESITAS?',
    InitEvaluation: 'Autoevaluador Médico',
    New: 'Nuevo',
    TeleconsultWithDoctor: 'Videoconsulta con un médico ahora',
    ScheduleWithSpecialist: 'Agenda una cita con un especialista',
    DeliveryMedicines: 'Delivery de medicamentos',
    DoctorHome: 'Médico a domicilio',
    RequestAmbulance: 'Solicitar una ambulancia',
    SearchClinics: 'Red de clínicas',
    ChatWithDoctor: 'Chatea con un doctor',
    ScheduleCitaCovid: 'Agenda una prueba de antígenos'
  },
  Texts: {
    WhatsAppInitMessage: 'Quiero chatear con un doctor ahora'
  },
  Modal: {
    TitleMapfreDoc: '¿YA TIENES TU CUENTA MAPFRE DOC?',
    SubTitleMapfreDoc:
      'Si es la primera vez que ingresas, comunicate con nosotros a nuestro Whatsapp para una mayor información.',
    TextButtonYesMapfreDoc: 'SÍ, YA TENGO MI CUENTA',
    TextButtonCreateAccountMapfreDoc: 'NECESITO CREAR MI CUENTA',
    TitleDeliveryMedicines: '',
    SubTitleDeliveryMedicines:
      'Este beneficio solo está disponible después de tener una consulta ambulatoria en una clínica de nuestras redes.',
    SubTitleTwoDeliveryMedicines: 'Cualquier duda, contáctate con nosotros al WhatsApp 999 919 133.',
    TextButtonYesDeliveryMedicines: 'CONTINUAR',
    TextButtonNoDeliveryMedicines: 'CANCELAR'
  },
  ModalCovid: {
    TitleCovid: '¿CUÁNDO DEBES HACERTE LA PRUEBA?',
    SubTitleSpanCovid: 'Si tienes síntomas,',
    SubTitleCovid:
      'de preferencia puedes realizarte la prueba durante los primeros 7 días desde el inicio de los síntomas.',
    SubTitleSpanTwoModal: 'Si no tienes síntomas y tienes contacto con personas positivas,',
    SubTitleTwoCovid: 'hazte la prueba entre el 5to y 7mo día del último contacto.',
    TextButtonYesCovid: 'AGENDAR CITA',
    TextButtonNoCovid: 'CANCELAR'
  }
};
