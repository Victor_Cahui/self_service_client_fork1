import { HOUSEHOLD_COVERAGE_TABS } from '@mx/settings/constants/router-tabs';
import {
  Content,
  ContentHowtoWork,
  ContentInsurancePlans,
  PaymentsOptions
} from '@mx/statemanagement/models/general.models';

export const HouseholdLang = {
  Links: {
    AssistReport: 'REPORTAR'
  },
  Labels: {},
  Texts: {},
  Titles: {
    HurtAndSteal: '¿Has sufrido un daño o un robo en tu hogar? '
  },
  Messages: {
    coverageOneHouse: 'En la póliza <strong>{{policyName}}</strong> se aplican las siguientes coberturas:',
    deductiblesOneHouse: 'En la póliza <strong>{{policyName}}</strong> se aplican los siguientes deducibles:',
    withOutExclusions: 'No tienes exclusiones.',
    HurtAndSteal: 'No te preocupes, nosotros nos encargamos de ayudarte',
    HurtAndStealHelp: `<div class="g-font--medium g-font-lg--20 g-font--18 g-line--10 mb-xs--1 text-center text-md-center
              text-lg-left g-font--medium g-c--gray8">
          Comunícate inmediatamente a nuestra central de asistencia 24 horas
        </div>
        <div class="g-line--10 g-font--light text-center text-md-center text-lg-left ">
          Realiza la denuncia policial en la comisaría más cercana.
        </div>`,
    HurtAndStealReport: `<div class="g-font--medium g-font-lg--20 g-font--18 g-line--10 mb-xs--1 text-center text-md-center
                text-lg-left g-font--medium g-c--gray8">
                También puedes declarar un siniestro a través de nuestro servicio online
          </div>
          <div class="g-line--10 g-font--light text-center text-md-center text-lg-left ">
            Te contestaremos en un período máximo de 2 días, y si es necesario, enviaremos un ajustador para asesorarte
          </div>`
  }
};

export const INSURANCE_TOP: ContentInsurancePlans = {
  title: 'TOP SEGUROS PARA EL HOGAR',
  textBtnInformation: 'CONTACTAR CON UN AGENTE',
  plans: [
    {
      title: 'HOGAR IDEAL',
      subtitle: 'EL MÁS ECONÓMICO',
      content: [
        {
          items: [
            { text: 'Incendio y líneas aliadas.' },
            { text: 'Terremoto.' },
            { text: 'Robo y/o hurto agravado.' },
            { text: 'Daños internos.' },
            { text: 'Responsabilidad Civil.' },
            { text: 'Muerte Accidental.' }
          ]
        }
      ],
      price: 'desde S/ 100 al mes'
    },
    {
      title: 'HOGAR 24 HORAS',
      subtitle: 'SOLO LO QUE NECESITAS',
      content: [
        {
          title: 'Beneficios',
          items: [
            { text: 'Seguro de Hogar.' },
            { text: 'Asistencias para el hogar y la familia.' },
            { text: 'Servicio de alarma y monitoreo.' }
          ]
        },
        {
          title: 'Coberturas',
          items: [
            { text: 'Incendio y líneas aliadas.' },
            { text: 'Robo y/o hurto agravado.' },
            { text: 'Muerte Accidental.' }
          ]
        }
      ],
      price: 'desde S/ 116 al mes'
    },
    {
      title: 'HOGAR SMART 24 HORAS',
      subtitle: 'El más completo',
      content: [
        {
          title: 'Beneficios',
          items: [
            { text: 'Servicio de alarma Smart y monitoreo.' },
            { text: 'Aplicación móvil para auto monitoreo.' },
            { text: 'Seguro de Hogar.' },
            { text: 'Asistencias para el hogar y familia.' }
          ]
        },
        {
          title: 'Coberturas',
          items: [
            { text: 'Incendio y líneas aliadas.' },
            { text: 'Robo y/o hurto agravado.' },
            { text: 'Muerte Accidental.' }
          ]
        }
      ],
      price: 'desde S/ 127 al mes'
    }
  ]
};

export const HOW_TO_WORK: ContentHowtoWork = {
  title: '¿CÓMO FUNCIONA?',
  items: [
    'Ingresa los datos de tu hogar y cotiza',
    'Contrata la opción que más te convenga',
    '¡Y listo! Recibe tu póliza por correo electrónico'
  ]
};

export const PAYMENTS_OPTIONS: PaymentsOptions = {
  title: 'Pago 100% seguro',
  url: './assets/images/banks/',
  images: ['card-visa.svg', 'card-mastercard.svg', 'card-american.svg', 'card-dinners.svg']
};

// Titulos - textos
export const COVERAGES: Content = {
  title: HOUSEHOLD_COVERAGE_TABS[0].name.toUpperCase(),
  content: 'Selecciona una dirección de la póliza {{policyName}} para visualizar su cobertura:'
};

export const COVERAGES_SUBLIMITS: Content = {
  title: 'SUBLÍMITES',
  content: `Son coberturas adicionales, a las que no se aplican deducibles ni se aplica infra seguro.`,
  table: {
    columns: ['SUBLÍMITES', 'LÍMITE INDEMNIZABLE']
  }
};

export const DEDUCIBLES: Content = {
  title: HOUSEHOLD_COVERAGE_TABS[1].name.toUpperCase(),
  content: 'Selecciona una dirección de la póliza {{policyName}} para visualizar sus deducibles:',
  table: {
    title: 'DEDUCIBLES - {{policyName}}',
    columns: ['COBERTURA', 'DEDUCIBLE']
  }
};

export const EXCLUSIONS: Content = {
  title: HOUSEHOLD_COVERAGE_TABS[2].name.toUpperCase(),
  content: `
    <p>Esta Póliza no cubre:<br>
    Pérdidas o daños o destrucción o Accidentes o responsabilidades que, en su origen o extensión, sean causados directa o
    indirectamente por, o surjan o resulten o sean consecuencia de:</p>
    <p>
      <ul class="g-list-bullet">
        <li>Actos de naturaleza fraudulenta o dolosa, o acto intencional, del ASEGURADO.</li>
        <li>Guerra, conflictos armados, invasión, acto de enemigo extranjero, hostilidades u operaciones de guerra, sea que la
        guerra haya sido declarada o no; guerra civil, asonada, sublevación, insurgencia, insubordinación, levantamiento popular,
        levantamiento militar, insurrección, rebelión, sedición, revolución, conspiración, golpe de Estado, poder militar o
        usurpación del poder, o cualquier evento o causa que determine la proclamación o el mantenimiento de estado de sitio;
        destrucción de bienes por orden de cualquier autoridad, excepto cuando dicha orden se haya dado con la finalidad de evitar
        la propagación de un incendio u otro riesgo cubierto por la Póliza; confiscación, requisa, expropiación, nacionalización,
        o incautación.</li>
        <li>Material para armas nucleares o material nuclear; reacción nuclear o radiación nuclear o contaminación radioactiva o
        la emisión de radiaciones ionizantes o contaminación por la radioactividad de cualquier combustible nuclear o de cualquier
        residuo o desperdicio proveniente de la combustión de dicho combustible nuclear.</li>
        <li>Extorsión o chantaje o Secuestro o Secuestro al Paso.</li>
      </ul>
    </p>
    <p>El resto de exclusiones podrás revisarlas en el condicionado del producto.</p>
  `
};

export const FOIL_ATTACHED: Content = {
  title: HOUSEHOLD_COVERAGE_TABS[3].name.toUpperCase(),
  content: ''
};

// Siniestros
export const DECLARE_SINISTER_HOME: any = {
  step1: {
    number: '1',
    text: 'LUGAR Y FECHA'
  },
  step2: {
    number: '2',
    text: 'TIPO DE INCIDENTE'
  },
  step3: {
    number: '3',
    text: 'FOTOS'
  }
};

export const DECLARE_SINISTER_HOME_STEP_1: any = {
  title: 'LUGAR DE INCIDENTE',
  address: 'Dirección',
  safe: 'Seguro',
  dateAndTimeOfTheIncident: 'FECHA Y HORA DEL INCIDENTE',
  form: {
    labelDate: 'Fecha',
    labelHour: 'Hora',
    btnCancel: 'Cancelar',
    btnSuccess: 'Continuar'
  }
};

export const DECLARE_SINISTER_HOME_STEP_2: any = {
  title: 'SELECCIONA EL TIPO DE INCIDENTE',
  titleReason: 'MOTIVOS DE LOS DAÑOS',
  form: {
    btnReturn: 'Regresar',
    btnSuccess: 'Continuar'
  }
};

export const DECLARE_SINISTER_HOME_STEP_3: any = {
  title: 'FOTOS DEL INCIDENTE',
  description: 'Sube 3 fotos como máximo para registrar la asistencia',
  descriptionAdd: 'Agrega o arrastra las imágenes',
  form: {
    btnReturn: 'Regresar',
    btnSuccess: 'Continuar'
  }
};

export const DECLARE_SINISTER_HOME_STEP_4: any = {
  title: 'RESUMEN DE ASISTENCIA',
  description: 'Antes de enviar tu asistencia por favor verifica que los datos sean correctos.',
  step1: {
    title: '1. LUGAR Y FECHA',
    labelAddress: 'Dirección',
    labelDate: 'Fecha',
    labelHour: 'Hora'
  },
  step2: {
    title: '2. TIPO DE INCIDENTE',
    labelType1: 'Incidente',
    labelType2: 'Motivos de los daños'
  },
  step3: {
    title: '3. FOTOS'
  },
  form: {
    btnReturn: 'Regresar',
    btnSuccess: 'Confirmar',
    btnLoading: 'Enviando'
  }
};

export const DECLARE_SINISTER_HOME_FINALIZE: any = {
  title: 'ASISTENCIA REPORTADA',
  message: 'Nos estaremos comunicando contigo en la brevedad posible para programar la asistencia que necesitas.',
  assistanceNumber: 'NÚMERO DE ASISTENCIA',
  address: 'LUGAR',
  contact: 'NÚMERO DE CONTACTO',
  numberContact: '983 342 432',
  btnHome: 'ACEPTAR'
};

export const COVERAGES_WITH_SERVICE: any = {
  FIRE_DAMAGE: {
    key: 'HOG_COB_INCE_DANOS',
    name: 'Incendio y daños materiales'
  },
  EARTHQUAKE: {
    key: 'HOG_COB_TERREMOTO',
    name: 'Terremoto'
  },
  ACCIDENTAL_DEATH: {
    key: 'HOG_COB_MUERTE_ACCID',
    name: 'Muerte accidental'
  },
  INTERNAL_DAMAGE: {
    key: 'HOG_COB_DANOS_INT',
    name: 'Daños Internos'
  },
  ASSAULT: {
    key: 'HOG_COB_ROBO_ASALTO',
    name: 'Robo y/o asalto'
  },
  CIVIL_LIABILITY: {
    key: 'HOG_COB_RESPONS_CIVIL',
    name: 'Responsabilidad civil'
  },
  DISHONESTY: {
    key: 'HOG_COB_DESHO_EMP',
    name: 'Deshonestidad por empleados'
  },
  DISAPPEARANCE: {
    key: 'HOG_COB_HURTO_DESA',
    name: 'Hurto o desaparición'
  }
};
