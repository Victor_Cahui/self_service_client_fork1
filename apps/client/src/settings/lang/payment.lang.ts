import {
  CARD_PAYMENT_ICON,
  CHECK_ICON,
  EDIT_ICON,
  MAPFRE_DOLLARS_ICON,
  POWER_ICON,
  REFRESH_ICON,
  TIME_NONE_ICON,
  WALLET_ICON
} from '@mx/settings/constants/images-values';
import { Content, IBannerNotification, ITooltips } from '@mx/statemanagement/models/general.models';

export const PaymentLang = {
  Buttons: {
    PaymentsSetting: 'Configurar pagos',
    ViewPayment: 'Ver pagos',
    ViewTable: 'Ver cuadro',
    NoMPD: 'No usar MAPFRE Dólares',
    ConfirmPurchase: 'Confirmar Compra'
  },
  Labels: {
    Email: 'Correo electrónico',
    DigitalPlatform: 'plataforma digital',
    BankPaymentCode: 'Número de recibo',
    NoticeNumber: 'Número de aviso',
    PaymentDocument: 'Documento de Pago',
    DocumentNumber: 'Número de documento',
    DocumentNumberAbrev: 'Nro. de documento',
    PaymentDetail: {
      title: 'Detalle de pago',
      netPrice: 'Precio {{policy}}',
      savingMafreDollars: 'Ahorro con MAPFRE dólares',
      total: 'Total a pagar',
      premiumCoveragePrice: 'Precio Full Cobertura Premium',
      totalPolicyPrice: 'Precio total de póliza',
      totalQuotaPrice: 'Precio total de cuota'
    }
  },
  Texts: {
    CurrentPayment: 'Vence',
    ExpiredPayment: 'Atrasada',
    TimelyPayment: 'A tiempo',
    LatePayment: 'Fecha de pago',
    Payment: 'pago',
    Payments: 'pagos',
    PaymentsDefault: 'Débito bancario',
    Free: '¡Gratis!',
    SoatStatus: {
      RenewFrom: 'Renovar a partir de:',
      ProcessingRenovation: { text1: 'Procesando', text2: 'Renovación' },
      SoatRenewed: { text1: 'Soat', text2: 'Renovado' },
      VigencyStarts: 'Vigencia a partir de:',
      AutomaticDebit: 'Débito automático'
    },
    WithNotice: {
      first: 'Pago con',
      second: 'aviso'
    },
    ChangeDebitCard: 'La tarjeta de tu poliza se ha actualizado con éxito',
    ActiveDebitCard: 'Se habilitó con éxito el pago automático',
    DisableDebitCard: 'El débito automatico de tu póliza se ha desactivado con éxito',
    AddedCard: 'La tarjeta se ha agregado con éxito.',
    DeletedCard: 'La tarjeta se ha eliminado con éxito.',
    ManualPay: 'Deseo continuar haciendo el pago manual.',
    NoSavedCard: 'No posees ninguna tarjeta guardada.'
  },
  Titles: {
    CardPayment: 'PAGOS',
    MyPendingPayment: 'PAGOS PENDIENTES',
    NextPayments: 'Próximos pagos',
    PaymentReceipts: 'Pagos realizados',
    PaymentsSetting: {
      title: '¿Quieres cambiar tu tipo de pago?',
      subtitle: 'Nos ajustamos a tus necesidades.'
    },
    SendReceipt: 'ENVIAR RECIBO ELECTRÓNICO',
    DeleteCard: '¿Estás seguro que deseas eliminar?'
  },
  Messages: {
    EmailSendReceipt: 'Ingresa el correo electrónico donde deseas enviar el recibo',
    SendReceipt: 'Pronto recibirá el Recibo de Pago',
    WithoutPendingPayment: '¡No tienes pagos pendientes!',
    WithoutPendingInvoices: 'No tienes facturas pendientes',
    WithoutPaymentReceipts: '¡No tienes recibos registrados!',
    AlertExpiredInvoices:
      // tslint:disable-next-line: max-line-length
      'Tienes {{n}} factura(s) vencida(s). Recuerda que al tener 3 meses pendientes de pago (consecutivos o no) el Contrato estará sujeto a anulación.',
    WithMapfreDollarsFree: '{{user}}, tienes los suficientes MAPFRE dólares para adquirir este producto gratis.',
    WithoutFees: 'Sus cuotas no están disponibles.',
    DeleteCard: 'Si la tarjeta se encuentra afiliada a una póliza , no se podrá eliminar la tarjeta.'
  },
  ButtonsSend: {
    BtnSubmit1: 'CANCELAR',
    BtnSubmit2: 'ENVIANDO'
  }
};

// Forma de Pago
export const MethodPaymentLang = {
  Header: {
    payQuote: 'PAGO DE CUOTA',
    paySoatElect: 'PAGO DE SOAT ELECTRÓNICO'
  },
  MethodPayment: 'FORMA DE PAGO',
  CardDetail: 'Datos de la tarjeta',
  CardNumber: 'Número de la tarjeta',
  OwnerName: 'Nombre del titular',
  Email: 'Correo electrónico',
  PaymentOrder: 'GENERAR ORDEN DE PAGO',
  MM_AA: 'MM / AA',
  tooltip_MM_AA: {
    title: '¿Dónde encontrar la Fecha Vcto.?',
    detail: 'Encontrarás la fecha de vencimiento al frente de tu tarjeta, debajo del número de tarjeta.'
  },
  CVV: 'CVV',
  tooltip_CVV: {
    title: '¿Qué es el CVV?',
    detail: 'Un código de tres dígitos ubicado al reverso de tu tarjeta de crédito/débito.'
  },
  Methods: {
    Card: 'Tarjeta',
    Bank: 'Bancario'
  },
  PaymentBank: {
    Text1: 'Generaremos un número de pago con el que podrás realizar la transacción.',
    Text2: 'Puedes realizar el pago en cualquiera de estos bancos:',
    TextConfirm: 'Bancos afiliados'
  },
  SummaryInsurance: 'Resumen de tu seguro',
  SummaryPay: 'Resumen de pago',
  ViewSummary: 'Ver resumen',
  Price: 'Precio',
  Quota: 'Cuota',
  UserMapfreDolar: 'Usar mis MAPFRE dólares',
  Available: 'Disponible',
  Remaining: 'Restante',
  Discount: 'Descuento',
  DefaultPeriod: '1 año',
  FinalPrice: 'Precio final',
  TotalPrice: 'Precio total',
  Vigency: 'Vigencia',
  AnnualVigency: 'Vigencia anual',
  MonthlyBilling: 'Facturación mensual',
  PlanName: 'Nombre de plan',
  DateStart: 'Fecha de inicio',
  DateEnd: 'Fecha de fin',
  QuotaValue: 'Valor cuota',
  QuotaValueLg: 'Valor de la cuota',
  BuyData: 'Datos de tu compra',
  PaymentFrecuency: 'Frecuencia de pago',
  Product: 'Producto',
  PolicyPrice: 'Precio de la póliza'
};

export const TooltipDigitalPlatform = {
  title: 'Pago en plataforma digital',
  text: 'Realiza los siguientes pasos:',
  items: [
    'Ingresa a tu banca digital web o móvil.',
    'Ingresa a la sección de pago de servicios.',
    'Busca la opción: "MAPFRE recaudación dólares/soles"',
    'Ingresa el documento indicado en el resumen anterior.',
    'Completa el pago siguiendo los pasos de tu banca digital.'
  ]
};

export const ConfirmBankPaymentSoatLang: Content = {
  title: 'Paga en cualquier banco afiliado',
  content: `Para completar la compra de tu SOAT, puedes pagar en tu banco de preferencia o su `,
  content2: `Realiza tu pago en los bancos afiliados con tu documento de identidad o mediante su `,
  warning: 'Tienes 24 horas para realizar el pago antes de que expire la orden.',
  button: 'Ir a inicio',
  action: { routeLink: '/vehicles/soat' },
  amountLabel: 'Monto',
  tooltips: TooltipDigitalPlatform
};

export const ConfirmCardPaymentSoatLang: Content = {
  title: '¡Gracias por tu Compra!',
  content:
    'En instantes enviaremos el comprobante de pago y <br class="d-none d-lg-block">el SOAT Electrónico a tu correo registrado:',
  button: 'Ir a inicio',
  action: { routeLink: '/vehicles/soat' },
  amountLabel: 'Monto',
  emailLabel: 'Correo electrónico:',
  icon: CARD_PAYMENT_ICON
};

export const ConfirmCardPaymentFreeSoatLang: Content = {
  title: '¡Gracias por usar MAPFRE dólares!',
  content:
    'En instantes enviaremos el comprobante de pago y <br class="d-none d-lg-block">el SOAT Electrónico a tu correo registrado:',
  button: 'Ir a inicio',
  action: { routeLink: '/vehicles/soat' },
  amountLabel: 'Monto',
  emailLabel: 'Correo electrónico:',
  icon: MAPFRE_DOLLARS_ICON
};

export const ConfirmCardPaymentLang: Content = {
  title: '¡Gracias por tu pago!',
  content:
    'Tu pago se realizó con éxito. Toda la información ha <br class="d-none d-lg-block">sido enviada a tu correo',
  button: 'Ir a mis pagos',
  action: { routeLink: '/payments/list' },
  amountLabel: 'Total pagado',
  emailLabel: 'Correo electrónico:',
  icon: CARD_PAYMENT_ICON
};

export const ConfirmBankPaymentVehicleLang: Content = {
  title: 'Paga en cualquier banco afiliado',
  content: `Para completar la compra de tu póliza, puedes pagar en tu banco de preferencia o su `,
  content2: `Realiza tu pago en los bancos afiliados con tu documento de identidad o mediante su `,
  warning: 'Tienes 24 horas para realizar el pago antes de que expire la orden.',
  button: 'Ir a inicio',
  action: { routeLink: 'vehicles/vehicularInsurance' },
  amountLabel: 'Monto primera cuota',
  tooltips: TooltipDigitalPlatform
};

export const ConfirmCardPaymentVehicleLang: Content = {
  title: '¡Gracias por tu Compra!',
  content:
    'En instantes enviaremos el comprobante de pago y <br class="d-none d-lg-block">datos del seguro a tu correo registrado:',
  button: 'Ir a inicio',
  action: { routeLink: 'vehicles/vehicularInsurance' },
  amountLabel: 'Monto pagado',
  emailLabel: 'Correo electrónico:',
  icon: CARD_PAYMENT_ICON
};

export const ConfirmCardPaymentFreeVehicleLang: Content = {
  title: '¡Gracias por usar MAPFRE dólares!',
  content:
    'En instantes enviaremos el comprobante de pago y <br class="d-none d-lg-block">datos del seguro a tu correo registrado:',
  button: 'Ir a inicio',
  action: { routeLink: 'vehicles/vehicularInsurance' },
  amountLabel: 'Monto',
  emailLabel: 'Correo electrónico:',
  icon: MAPFRE_DOLLARS_ICON
};

export const PaymentNotAvailableReason = {
  apeseg: { key: 'VALIDACION_APESEG', reason: 'Puedes realizar la renovación de su SOAT a partir del:' },
  default: { reason: 'En estos momentos no se puede realizar tu pago.' }
};

// Configuracion de Pagos
export const PAYMENT_SETTINGS_NOTIFICATION: IBannerNotification = {
  title: 'Sabías que al afiliarte a débito automático:',
  items: ['Tienes mayor seguridad', 'Evitas las colas y ahorras tiempo', 'Mantienes tus pagos siempre al día'],
  link: {
    label: 'Más información'
  }
};

export const InsurancePrime: ITooltips = {
  title: 'Prima de Seguro',
  text: '¿Qué es la Prima de Seguro?',
  items: [
    `Es el pago (periódico o único) que realizas a MAPFRE, con la finalidad de cubrir los riesgos acordados en el contrato.`
  ]
};

export const PAYMENT_SETTINGS_RENEWAL = {
  title: 'Renovación',
  content: `Puedes decidir si la renovación de tu póliza sea automática, o puedes realizar la confirmación a través de un agente
    o de tu área privada.`,
  options: {
    title: 'SELECCIONA UNA MODALIDAD DE RENOVACIÓN',
    automatic: {
      icon: REFRESH_ICON,
      title: 'Automática',
      content: 'Tu póliza se renovará automáticamente hasta que nos indiques lo contrario.',
      contentEditing: 'Tu póliza se renovará automáticamente.'
    },
    manual: {
      icon: POWER_ICON,
      title: 'Manual',
      content: 'Te contactará un agente o podrás hacerlo tu mismo a través de tu área privada.',
      contentEditing: 'La renovación será por un agente o a través de tu área privada.'
    },
    change: {
      icon: EDIT_ICON,
      label: 'Cambiar'
    },
    useMPD: 'Utilizar todos los MAPFRE dólares disponibles',
    confirm: {
      title: '¡Listo!',
      content: 'El cambio de renovación se ha realizado correctamente.'
    }
  }
};

export const PAYMENT_SETTINGS_QUOTA = {
  title: 'Mis cuotas',
  titleMobile: 'CONFIGURACIÓN DE PAGOS',
  subTitleMobile: 'REFINANCIAR MI PÓLIZA',
  features: [
    'Puedes refinanciar las cuotas que te quedan como mejor te convenga.',
    'Nos adaptamos a tus necesidades.',
    'Si tienes un pago pendiente se dividirá entre las cuotas que elijas.',
    'Siempre podrás volver a configurar tus cuotas.'
  ],
  labels: {
    payment: 'Pagas',
    quota: 'Cuota',
    quotas: 'Cuotas',
    interest: 'interés',
    noInterest: 'Sin interés',
    noRefinance: 'No se puede refinanciar',
    paymentStatus: 'ESTADO DE PAGOS:',
    paid: {
      singular: 'pagada',
      plural: 'pagadas'
    },
    toPaid: 'por pagar',
    refinance: 'Refinanciar',
    in: 'en',
    refinanceMyPolicy: 'REFINANCIAR MI PÓLIZA'
  },
  icons: {
    check: CHECK_ICON,
    refinance: WALLET_ICON
  },
  messages: {
    icon: TIME_NONE_ICON,
    title: 'ESTIMADO USUARIO',
    content: 'En un máximo de {{hours}} horas se realizará tu refinanciamiento.',
    time: 'h'
  },
  warnings: {
    policyRefinanced: 'La póliza ya ha sido refinanciada',
    refinancedReminder: 'Te recordamos que la póliza ya ha sido refinanciada el'
  }
};
