export const SinistersLang = {
  Labels: {
    Cause: 'Causa'
  }
};

export const SINISTERS_TYPE = {
  ACCIDENT: 'ACCIDENTE',
  STOLEN: 'ROBO',
  OTHERS: 'OTROS'
};
