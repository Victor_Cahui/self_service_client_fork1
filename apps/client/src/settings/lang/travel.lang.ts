import {
  Content,
  ContentHowtoWork,
  ContentInsurancePlans,
  PaymentsOptions
} from '@mx/statemanagement/models/general.models';

export const TravelLang = {
  Titles: {
    Form: 'Cotiza y compra tu seguro',
    QuoteNow: 'Cotiza ahora'
  },
  Texts: {
    Quote: `Siempre es importante prevenir cualquier eventualidad, por eso nuestros seguros de Viajes buscan darte la tranquilidad
      que necesitas.`,
    CoverageDescription: 'Tu póliza {{policyName}} te cubre en los siguientes casos.',
    VigencyDescription: 'No te olvides que tus coberturas cubren por {{vigency}} días:'
  }
};

export const CoveragesGroup = {
  Main: 'PRINCIPALES',
  Additional: 'ADICIONALES'
};

export const ModalEmergencyPhones = 'modal-emergency-phones';

export const HOW_TO_WORK: ContentHowtoWork = {
  title: '¿CÓMO FUNCIONA?',
  items: [
    'Ingresa los datos de la persona a asegurar',
    'Contrata la opción que más te convenga',
    '¡Y listo! Recibe tu póliza por correo electrónico'
  ]
};

export const PAYMENTS_OPTIONS: PaymentsOptions = {
  title: 'Pago 100% seguro',
  url: './assets/images/banks/',
  images: ['card-visa.svg', 'card-mastercard.svg', 'card-american.svg', 'card-dinners.svg']
};

// Contenido de Landing de Viajes
export const INSURANCE_TOP: ContentInsurancePlans = {
  title: 'TOP SEGUROS VIAJES',
  textBtnGetQuote: 'COTIZAR',
  plans: [
    {
      title: 'EUROSCHENGEN',
      subtitle: 'Sólo lo que necesitas',
      content: [
        {
          title: 'Suma asegurada',
          price: { amount: 'Hasta USD 30,000' }
        },
        {
          title: 'Coberturas',
          items: [
            { text: 'Asistencia médica por enfermedad.' },
            { text: 'Compensación por pérdida de equipaje en vuelo regular.' },
            { text: 'Localización y transporte de equipajes y efectos personales.' },
            { text: 'Gastos hotel por convalecencia Máximo Global.' },
            { text: 'Gastos de hotel por familiar acompañante.' },
            { text: 'Traslado de un familiar.' },
            { text: 'Transporte o repatriación sanitaria en caso de lesiones o enfermedad.' },
            { text: 'Envío medicamentos fuera de Perú.' },
            { text: 'Odontología de urgencia.' }
          ]
        }
      ],
      price: 'desde USD 7.50 al día*',
      conditionPrice: '*Precio aplica por un periodo mínimo de emisión de 3 días.'
    },
    {
      title: 'GOLD',
      subtitle: 'El más completo',
      content: [
        {
          title: 'Suma asegurada',
          price: { amount: 'Hasta USD 50,000' }
        },
        {
          title: 'Coberturas',
          items: [
            { text: 'Compensación por pérdida de equipaje en vuelo regular.' },
            { text: 'Reembolso de gastos por vuelo demorado o cancelado (más de 6 horas).' },
            { text: 'Localización y transporte de equipajes y efectos personales.' },
            { text: 'Gastos hotel por convalecencia Máximo Global.' },
            { text: 'Gastos de hotel por familiar acompañante.' },
            { text: 'Traslado de un familiar.' },
            { text: 'Transporte o repatriación sanitaria en caso de lesiones o enfermedad.' },
            { text: 'Envío medicamentos fuera de Perú.' },
            { text: 'Odontología de urgencia.' }
          ]
        }
      ],
      price: 'desde USD 8.50 al día*',
      conditionPrice: '*Precio aplica por un periodo mínimo de emisión de 3 días.'
    },
    {
      title: 'SILVER',
      subtitle: 'El más económico',
      content: [
        {
          title: 'Suma asegurada',
          price: { amount: 'Hasta USD 20,000' }
        },
        {
          title: 'Coberturas',
          items: [
            { text: 'Compensación por pérdida de equipaje en vuelo regular.' },
            { text: 'Reembolso de gastos por vuelo demorado o cancelado (más de 6 horas).' },
            { text: 'Localización y transporte de equipajes y efectos personales.' },
            { text: 'Gastos hotel por convalecencia Máximo Global.' },
            { text: 'Gastos de hotel por familiar acompañante.' },
            { text: 'Traslado de un familiar.' },
            { text: 'Transporte o repatriación sanitaria en caso de lesiones o enfermedad.' },
            { text: 'Envío medicamentos fuera de Perú.' },
            { text: 'Odontología de urgencia.' }
          ]
        }
      ],
      price: 'desde USD 6.50 al día*',
      conditionPrice: '*Precio aplica por un periodo mínimo de emisión de 3 días.'
    }
  ]
};

export const EXCLUSIONS: Content = {
  title: 'EXCLUSIONES',
  content: `
    <p>Se excluye cualquier siniestro ocasionado a causa o como consecuencia de los siguientes supuestos:</p>
    <ol class="g-list-alpha">
    <li>Cuando el ASEGURADO participe de forma activa en carreras de automóviles, motos acuáticas; participación en corridas de toros,
      novilladas, espectáculos taurinos, carrera de toros, rodeo, incluyendo montar a pelo potros salvajes o reses vacunas bravas;
      combate en artes marciales, boxeo, lucha libre, 'vale todo', esgrima o rugby, fútbol americano, lacrosse, hockey o hurling;
      caminata de montaña ('trekking' o senderismo), escalamiento o descensos, alpinismo o andinismo o montañismo, o rápel; canotaje o
      piragüismo o 'rafting', a partir de nivel de dificultad III; cacería de fieras; pesca en rocas, o caza submarina o subacuática,
      buceo o inmersión en mares, ríos, lagos o lagunas o pozos o pozas o cuevas o cavernas o aguas subterráneas; salto desde trampolines
      o clavados desde cualquier lugar; surf; equitación de salto o carrera de caballos; patinaje, o uso de patineta o 'skateboard';
      ciclismo de montaña o a campo traviesa o en carreteras o en autopistas; paracaidismo, parapente, alas delta; vuelos en avionetas o
      aviones ultraligeros, trapecio, equilibrismo; salto desde puentes, puenting'; halterofilia; o esquí acuático o sobre nieve,
      'snowboard' o 'sandboard'.</li>
    <li>Cualquier enfermedad corporal o mental o tratamientos médicos o quirúrgicos que no sean motivados por accidentes amparados por el
      presente seguro, así como los denominados accidentes médicos, entendidos estos como la apoplejía, los vértigos, los infartos y los
      ataques epilépticos.</li>
    <li>Fallecimiento a consecuencia de un accidente debido a actividades como piloto y/o asistente de vuelos, trabajos en minas, torres
      de alta tensión, comunicaciones.</li>
    <li>El riesgo de aviación, que comprende tripulación, personal en funciones de la aerolínea fabricante o proveedor del operador
      aéreo salvo que EL ASEGURADO esté viajando en calidad de pasajero en aeronaves con capacidad mayor a diez plazas de pasajeros y de
      empresas de transporte público comercial, con itinerarios, escalas y horarios predeterminados, con intervención del organismo
      nacional o internacional competente.</li>
    <li>Como consecuencia de hechos de guerra internacional (declarada o no) o civil, o servicio militar de cualquier clase</li>
    <li>Suicidio o tentativa de suicidio u acto delictuoso provocado por cualquier persona que resultase favorecida con los alcances de
      este seguro.</li>
    <li>Los que tengan origen en actos de imprudencia temeraria o negligencia grave de EL ASEGURADO, así como los derivados de actos
      delictivos o infractorios de leyes y/o reglamentos.</li>
    <li>Accidentes sufridos en situación de enajenación mental o bajo los efectos de bebidas alcohólicas (siempre que supere el límite
      legal permitido para la ingesta de alcohol, es decir, 0.50 gramos por litro de alcohol en la sangre) o de drogas y/o
      estupefacientes, así como por intoxicaciones por ingestión de alimentos o bebidas.</li>
    <li>A consecuencia del VIH: Síndrome de inmunodeficiencia adquirida (SIDA) y VIH en todas sus formas, así como sus agudizaciones,
      secuelas y consecuencias. Enfermedades venéreas o de transmisión sexual.</li>
    <li>Fallecimiento producido como consecuencia de la reacción nuclear o contaminación radioactiva, química o bacteriológica.</li>
    <li>Participación de EL ASEGURADO de forma activa en hechos de carácter político o social, huelgas, alborotos o tumultos populares y
      terrorismo.</li>
    <li>Los gastos y complicaciones por enfermedades crónicas, preexistentes (a excepción de la atención de urgencias y emergencias
      médicas), congénitas o recurrentes que hayan sido diagnosticadas al Asegurado antes del inicio de vigencia de la presente póliza
      y/o viaje. Se excluyentambién, lasenfermedadesbenignasyheridaslevesqueno imposibiliten la continuación normal del viaje no darán
      lugar a esta asistencia.</li>
    <li>Enfermedades endémicas y/o epidémicas en países con emergencia sanitaria – de acuerdo a sus respectivas autoridades
      sanitarias - en caso que el ASEGURADO no haya seguido las sugerencias y/o indicaciones sobre restricciones de viaje y/o tratamiento
      profiláctico y/o vacunación emanadas de autoridades sanitarias.</li>
    <li>Afecciones, enfermedades o lesiones derivadas de una riña (salvo aquellos casos en que se establezca judicialmente, o a través de
      indicios razonables, la legítima defensa), huelga, actos de vandalismo o tumulto popular en que el ASEGURADO hubiese participado como
      elemento activo. El intento de o la comisión de un acto ilegal y, en general, cualquier acto doloso o criminal del ASEGURADO,
      incluido el suministro de información falsa o diferente de la realidad.</li>
    <li>Drogas, narcóticos y/o afines: Tratamiento de enfermedades o estados patológicos producidos por intencional ingestión o
      administración de tóxicos (drogas), narcóticos, o por lo utilización de medicamentos sin orden médica. Asimismo, afecciones,
      enfermedades o lesiones derivadas de la ingestión de bebidas alcohólicas de cualquier tipo.</li>
    <li>Enfermedades, lesiones, afecciones, consecuencias o complicaciones resultantes de tratamientos o asistencias recibidas por el
      ASEGURADO de parte de personas o profesionales no pertenecientes a la red de proveedores de LA COMPAÑÍA.</li>
    <li>Viajes aéreos: Viajes aéreos en aviones no destinados y autorizados como transporte público.</li>
    <li>Partos: Controles, exámenes y complicaciones de gestación. Partos. Abortos, cualquiera sea su etiología.</li>
    <li>Enfermedades mentales y/o afines: Enfermedades psicológicas, mentales, psicosis, neurosis y cualquiera de sus consecuencias
    mediatas o inmediatas.</li>
    <li>Tensión arterial: Controles de tensión arterial. Hipertensión arterial y sus consecuencias.</li>
    <li>Visitas médicas no autorizadas por LA COMPAÑÍA: Las visitas médicas de control, chequeos y tratamientos prolongados, que no sean
      previa y expresamente autorizadas por LA COMPAÑÍA.</li>
    <li>Prótesis y similares: Gastos de prótesis, órtesis, síntesis o ayudas mecánicas de todo tipo, ya sean de uso interno o externo,
      incluyendo pero no limitados a: artículos de ortopedia, prótesis dentales, audífonos, anteojos, lentes de contacto, férulas,
      muletas, nebulizadores, respiradores, etc.</li>
    <li>Los tratamientos odontológicos, oftalmológicos u otorrinolaringológicos, salvo la atención de urgencia descrita en las presentes
      Condiciones Generales.</li>
    <li>Chequeos o exámenes de rutina: Los chequeos o exámenes médicos de rutina, incluso aquellos que no sean relacionados con uno
      enfermedad diagnosticada y comprobada, así como aquellos que no sean consecuencia directa de una enfermedad o accidente.</li>
    <li>Riesgos profesionales: Si el motivo del viaje del ASEGURADO fuese la ejecución de trabajos o tareas que involucren un riesgo
      profesional. En todos los casos, las coberturas descritas serán complementarios de los que deban prestarse por parte de entidades
      de seguros según las normas de segundad industrial y de riesgos laborales aplicables en el país donde se presente la enfermedad o
      accidente objeto del servicio.</li>
    <li>Gastos no autorizados: Gastos de hotel, restaurante, taxis, comunicaciones, etc. que no hayan sido expresamente autorizados por
      LA COMPAÑÍA.</li>
    <li>Acompañantes y gastos extras: En los casos de hospitalización del ASEGURADO, se excluyen expresamente todos los gastos extras así
      como de acompañantes.</li>
    <li>Cáncer, diagnosticado por estudio anatomopatológico.</li>
    </ol>`
};
