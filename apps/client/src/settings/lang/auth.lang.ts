export const AuthLang = {
  Buttons: {
    ForgotPassword: 'He olvidado mi contraseña',
    Login: 'Ingresar',
    EnableUser: 'Crear cuenta',
    AnotherProfile: 'Ingresar con Otro Perfil',
    GoToLogin: 'Iniciar Sesión',
    UpdatePassword: 'Actualizar contraseña',
    ChangeAccount: 'Cambiar cuenta'
  },
  Labels: {
    Password: 'Contraseña',
    NewPassword: 'Nueva contraseña',
    ConfirmNewPassword: 'Confirmar nueva contraseña',
    CurrentPassword: 'Contraseña actual'
  },
  Texts: {
    ExpiredSession: {
      title: 'Estimado usuario(a):',
      description: 'El tiempo de sesión ha expirado.',
      description2: 'Por favor, ingresa tu contraseña nuevamente.'
    }
  },
  Titles: {
    Login: 'Inicia sesión',
    UserType: 'Tipo de usuario',
    NewAccount: 'Registro',
    RememberMe: 'Recordar contraseña',
    Welcome: 'Bienvenido {{user}}',
    ForgotPassword: 'He olvidado mi contraseña',
    EnableUser: 'Crear cuenta',
    RestorePassword: 'Genera tu contraseña',
    MAPFRE: 'MAPFRE PERÚ COMPAÑÍA DE SEGUROS Y REASEGUROS'
  },
  Messages: {
    Welcome: 'Bienvenido, ingresa tu documento (DNI, RUC, Carné de extranjería).',
    ChooseAcountType: 'Elige el tipo de cuenta con el que deseas ingresar.',
    UserInvalid: 'El número de documento o contraseña no coinciden o bien no se encuentran registrados.',
    DataInvalid: 'No se encontraron pólizas vigentes para el número de documento ingresado.',
    PasswordMatch: 'La contraseña ingresada', // complemento para match validators
    NewPasswordMatchCurrent: 'La contraseña ingresada no coincide con la actual.',
    PasswordIncorrectCondition: 'La contraseña debe incluir las condiciones indicadas.',
    PasswordInvalid: 'La contraseña es incorrecta.',
    EmailInvalid: `<div>Estimado cliente, no contamos con tu correo electrónico, por favor comunícate con nosotros:
    <ul class="g-ml--10 g-list-bullet">
      <li>Whatsapp: {{whatsapp}}</li>
      <li>Para Lima: {{lima}}</li>
      <li>Para Provincias: {{provinces}}</li>
    </ul></div>`,
    SendEmail: 'Hemos enviado un enlace a tu correo electrónico:',
    ContactEmailInvalid: `¿No es tu correo electrónico? <br class="d-block d-md-none"> Comunícate con nosotros <br>
      Lima: {{lima}} - <br class="d-block d-md-none">Provincias: {{provinces}}`,
    PasswordFormat: `La contraseña debe contener 6 caracteres como mínimo y cumplir por lo menos con 2 de las siguientes condiciones:
      mayúsculas, minúsculas, números o caracteres especiales.`,
    HelpContact: 'Cualquier duda contáctanos en Lima: {{lima}} y Provincias: {{provinces}}',
    ExpiredTokenRecover: 'El límite de tiempo de la solicitud ha expirado.',
    ExpiredTokenEnable: 'Se ha activado tu cuenta sin embargo, el tiempo de la solicitud ha expirado.',
    UpdatePassword: 'Por favor, actualiza tu contraseña ingresando al siguiente enlace:',
    TryAgain: 'En este momento no es posible completar la acción requerida. Por favor, inténtalo de nuevo.',
    // TODO: Cambiar copy
    ConfirmRestorePassword: 'Se ha generado una nueva contraseña.',
    EnjoyYourPrivateArea: 'Ya puedes disfrutar de tu área privada MAPFRE.'
  }
};

export const Actions = {
  title: 'Dentro del portal de clientes podrás:',
  items: ['Actualizar datos', 'Solicitar servicios', 'Gestionar tus pólizas', 'Pagar recibos']
};

export const WellcomeMessage = ['Portal de clientes', 'La experiencia', 'MAPFRE', 'más cerca de ti'];
