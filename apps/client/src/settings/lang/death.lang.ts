import { DEATH_COVERAGE_TABS } from '@mx/settings/constants/router-tabs';
import {
  Content,
  ContentHowtoWork,
  ContentInsurancePlans,
  PaymentsOptions
} from '@mx/statemanagement/models/general.models';

export const DeathLang = {
  Links: {},
  Labels: {},
  Texts: {},
  Titles: {},
  Messages: {}
};

/***
 *  Contenido de Landing de Decesos
 */
export const INSURANCE_TOP: ContentInsurancePlans = {
  title: 'TOP SEGUROS',
  textBtnInformation: 'CONTACTAR CON UN AGENTE',
  plans: [
    {
      title: 'SEPELIO INTEGRAL (PLAN A)',
      subtitle: 'EL MÁS ECONÓMICO',
      content: [
        {
          items: [
            { text: 'Servicio funerario Finisterre.' },
            { text: 'Sepultura en camposantos MAPFRE.' },
            { text: 'Indemnizaciones.' },
            { text: 'Seguros adicionales: Accidentes Personales y Vida Integral.' }
          ]
        }
      ],
      price: 'desde S/ 50 al mes'
    },
    {
      title: 'SEPELIO INTEGRAL (PLAN B)',
      subtitle: 'LO QUE NECESITAS',
      content: [
        {
          items: [
            { text: 'Servicio funerario Finisterre.' },
            { text: 'Reembolso de sepultura hasta la suma asegurada.' },
            { text: 'Indemnizaciones.' },
            { text: 'Seguros adicionales: Accidentes Personales y Vida Integral.' }
          ]
        }
      ],
      price: 'desde S/ 54 al mes'
    }
  ]
};

export const HOW_TO_WORK: ContentHowtoWork = {
  title: '¿CÓMO FUNCIONA?',
  items: [
    'Solicita el contacto de un agente para obtener más información',
    'Contrata la opción que más te convenga',
    '¡Y listo! Recibe tu póliza y estarás asegurado'
  ]
};

export const PAYMENTS_OPTIONS: PaymentsOptions = {
  title: 'Pago 100% seguro',
  url: './assets/images/banks/',
  images: ['card-visa.svg', 'card-mastercard.svg', 'card-american.svg', 'card-dinners.svg']
};

export const EXCLUSIONS: Content = {
  title: '',
  content: ``
};

export const COVERAGE: Content = {
  title: DEATH_COVERAGE_TABS[0].name,
  content: 'Tu póliza {{typeInsured}} te cubre a ti y a tus asegurados en las siguientes coberturas:'
};
