import { ContentLanding } from '@mx/statemanagement/models/general.models';
import {
  DISABILITY_ICON,
  FUNERAL_EXPENSES_ICON,
  MEDICAL_EXPENSES_ICON,
  NATURAL_DEAD_ICON,
  TEMP_DISABILITY_ICON
} from '../constants/images-values';

export const LANDING_SOAT: ContentLanding = {
  title: '¿Qué hacer en caso <br class="d-none d-xl-block"> de accidente?',
  sections: [
    {
      nameSection: 'PASOS A SEGUIR',
      linkedItems: true,
      items: [
        {
          titleItem: 'MANTÉN LA CALMA',
          textItem: 'No te preocupes, te ayudaremos durante todo el proceso de tu accidente vehicular.'
        },
        {
          titleItem: 'LLAMA A NUESTRA CENTRAL',
          textItem: 'Te brindamos asistencia las 24 horas del día.<br> Lima: (01) 213-3333<br> Provincia: 0801-1-1133.'
        },
        {
          titleItem: 'NO MUEVAS A LOS LESIONADOS',
          textItem:
            'Si hay personas con lesiones no intentes moverlos del lugar, nosotros nos encargaremos de ayudarlos cuanto antes.'
        },
        {
          titleItem: 'NO HAGAS ACUERDOS CON TERCEROS',
          textItem: 'Procura mantener la calma y esperar a que llegue la ayuda.'
        },
        {
          titleItem: 'REALIZA LA DENUNCIA POLICIAL',
          textItem: 'Aproxímate a la comisaría más cercana para declarar lo que sucedió en el accidente.'
        },
        {
          titleItem: 'SOMÉTETE AL DOSAJE ETÍLICO',
          textItem: 'Los oficiales de la PNP te acompañaran para realizar este examen.'
        }
      ]
    },
    {
      nameSection: 'COBERTURAS SOAT',
      linkedItems: false,
      items: [
        {
          icon: NATURAL_DEAD_ICON,
          titleItem: 'POR MUERTE',
          textItem: 'Hasta 4 UIT'
        },
        {
          icon: DISABILITY_ICON,
          titleItem: 'POR INVALIDEZ PERMANENTE',
          textItem: 'Hasta 4 UIT'
        },
        {
          icon: TEMP_DISABILITY_ICON,
          titleItem: 'POR INCAPACIDAD TEMPORAL',
          textItem: 'Hasta 1 UIT'
        },
        {
          icon: MEDICAL_EXPENSES_ICON,
          titleItem: 'POR GASTOS MÉDICOS',
          textItem: 'Hasta 5 UIT'
        },
        {
          icon: FUNERAL_EXPENSES_ICON,
          titleItem: 'POR GASTOS DE SEPELIO',
          textItem: 'Hasta 1 UIT'
        }
      ]
    },
    {
      nameSection: 'EXCLUSIONES',
      linkedItems: false,
      items: [
        {
          bullet: '_',
          titleItem: 'CARRERA DE AUTOMÓVILES',
          textItem: 'Accidentes causados en carrera de automóviles y otras competencias de vehículos automotores.'
        },
        {
          bullet: '_',
          titleItem: 'EL EXTRANJERO',
          textItem: 'Accidentes ocurridos fuera de territorio nacional.'
        },
        {
          bullet: '_',
          titleItem: 'ZONAS RESTRINGIDAS',
          textItem: 'Accidentes ocurridos en lugares no abiertos al tránsito público.'
        },
        {
          bullet: '_',
          titleItem: 'CASOS FORTUITOS O DE FUERZA MAYOR',
          textItem: `Consecuencia de guerras, eventos de la naturaleza u otros casos de fuerza mayor
                     extraños a la circulación del vehículo.`
        },
        {
          bullet: '_',
          titleItem: 'SUICIDIO O AUTOLESIONES',
          textItem: 'El suicidio y la comisión de lesiones auto inferidas utilizando el vehículo automotor asegurado.'
        }
      ]
    }
  ]
};

export const LANDING_ACCIDENTS: ContentLanding = {
  title: '¿Qué hacer en caso <br class="d-none d-xl-block"> de accidente?',
  sections: [
    {
      nameSection: 'PRIMER MOMENTO',
      linkedItems: true,
      items: [
        {
          titleItem: 'MANTÉN LA CALMA',
          textItem: 'Tranquilízate, no estás solo. Te acompañaremos durante todo el proceso.'
        },
        {
          titleItem: 'COMUNÍCATE CON NOSOTROS',
          textItem: `
            Llama a nuestra central de asistencia 24 horas, en Lima al
            <span class="text-nowrap">213-3333</span> y en provincia al
            <span class="text-nowrap">0801-1-1133.</span>`
        },
        {
          titleItem: 'NO MUEVAS A LOS LESIONADOS',
          textItem: 'Si hay lesionados no intentes moverlos del lugar, nosotros nos encargaremos.'
        }
      ]
    },
    {
      nameSection: 'PASOS A SEGUIR',
      linkedItems: true,
      items: [
        {
          titleItem: 'ESPERA AL ASESOR',
          textItem: 'Enviaremos un asesor al lugar del accidente para evaluar los daños y brindarte ayuda.'
        },
        {
          titleItem: 'PERMANECE EN EL LUGAR DEL ACCIDENTE',
          textItem:
            'Espera a la ayuda en el lugar de los hechos a menos que nuestros asesores te indiquen lo contrario.'
        },
        {
          titleItem: 'NO REALICES ACUERDOS CON TERCEROS',
          textItem: 'Un acuerdo con un tercero puede dificultar el trabajo de nuestro asesor.'
        },
        {
          titleItem: 'TRÁMITES QUE DEBES REALIZAR',
          textItem:
            'Debes ir a la comisaría más cercana para presentar una denuncia policial y hacer el dosaje etílico.'
        },
        {
          titleItem: 'REVISIÓN CON EL PÉRITO MAPFRE',
          textItem: `Si tu auto circula puedes acercarte a nuestros puestos de atención rápida
          para que se te entregue la orden de reparación.`
        }
      ]
    }
  ]
};

export const LANDING_VEHICLE_STOLEN: ContentLanding = {
  title: '¿Qué hacer en caso <br class="d-none d-xl-block"> de robo?',
  sections: [
    {
      nameSection: 'PASOS A SEGUIR',
      linkedItems: true,
      items: [
        {
          titleItem: 'MANTÉN LA CALMA',
          textItem: 'Tranquilízate, no estás solo. Te acompañaremos durante todo el proceso.'
        },
        {
          titleItem: 'COMUNÍCATE CON NOSOTROS',
          textItem: `
            Llama a nuestra central de asistencia 24 horas, en Lima al
            <span class="text-nowrap">213-3333</span> y en provincia al
            <span class="text-nowrap">0801-1-1133.</span>`
        },
        {
          titleItem: 'PRESENTA LA DENUNCIA POLICIAL',
          textItem: 'Realiza la denuncia policial en la comisaría más cercana.'
        }
      ]
    }
  ]
};

export const LANDING_HURT_AND_STEAL: ContentLanding = {
  title: 'Qué hacer en <br class="d-none d-xl-block"> caso de...',
  sections: [
    {
      nameSection: 'INCENDIO',
      linkedItems: true,
      items: [
        {
          titleItem: 'ALÉJATE DEL FUEGO',
          textItem: 'No trates de extinguirlo, puedes hacerte daño.'
        },
        {
          titleItem: 'LLAMA A LOS BOMBEROS',
          textItem: 'Marca el 116 para recibir ayuda de la estación de bomberos más cercana.'
        }
      ]
    },
    {
      nameSection: 'TERREMOTO',
      linkedItems: true,
      items: [
        {
          titleItem: 'TÓMALO CON CALMA',
          textItem: 'Párate debajo de la columna más cercana hasta que termine el movimiento.'
        },
        {
          titleItem: 'SI EL TERREMOTO AFECTÓ TU VIVIENDA',
          textItem: 'Comunícate con nosotros al 01 213 3333 para ayudarte.'
        }
      ]
    },
    {
      nameSection: 'ROBO',
      linkedItems: true,
      items: [
        {
          titleItem: 'VERIFICA QUE NO HAYA NADIE DENTRO DE LA CASA',
          textItem: 'Reune a los miembros de tu familia y salgan de la vivienda.'
        },
        {
          titleItem: 'LLAMA A LA POLICÍA',
          textItem: 'Ellos son los primeros que inspeccionarán la escena.'
        },
        {
          titleItem: 'COMUNÍCATE CON NOSOTROS',
          textItem: 'Para iniciar la evaluación de tu caso.'
        }
      ]
    }
  ]
};

// Servicios MAPFRE
export const LANDING_AMBULANCE_SERVICES: ContentLanding = {
  title: '¿Qué hacer en caso <br class="d-none d-xl-block"> de emergencia médica?',
  sections: [
    {
      nameSection: 'PASOS DE REANIMACIÓN',
      linkedItems: true,
      items: [
        {
          titleItem: 'TOMA LOS SIGNOS VITALES DE LA PERSONA',
          textItem:
            'Evalúa si la persona se encuentra conciente, en caso no le esté procede a administrar la reanimación.'
        },
        {
          titleItem: 'PON A LA PERSONA BOCA ARRIBA',
          textItem:
            'Es importante poner a la persona en esta posición para evitar lesionarla mientras realizas las compresiones.'
        },
        {
          titleItem: 'COLOCA TUS MANOS EN EL PECHO DE LA PERSONA',
          textItem: 'Coloca una mano sobre la otra y entrecruza tus dedos cerca del final de la caja toráxica.'
        },
        {
          titleItem: 'POSICIÓNATE SOBRE TUS MANOS CON LOS BRAZOS ESTIRADOS',
          textItem: 'Tienes que mantener tus codos estirados mientras realizas compresiones.'
        },
        {
          titleItem: 'REALIZA 30 COMPRESIONES',
          textItem: 'Presiona con ambas manos sobre el esternón a un ritmo relativamente rápido.'
        },
        {
          titleItem: 'CONTINUA HASTA QUE LLEGUE LA AYUDA',
          textItem: 'Trata de no realizar pausas de más de 10 segundos en lo que llega la ayuda.'
        }
      ]
    },
    {
      nameSection: 'COBERTURAS',
      linkedItems: true,
      items: [
        {
          titleItem: 'TITULO',
          textItem: 'texto texto texto texto texto texto texto texto texto texto texto texto'
        },
        {
          titleItem: 'TITULO',
          textItem: 'texto texto texto texto texto texto texto texto texto texto texto texto'
        },
        {
          titleItem: 'TITULO',
          textItem: 'texto texto texto texto texto texto texto texto texto texto texto texto'
        }
      ]
    },
    {
      nameSection: 'LÍMITES GEOGRÁFICOS',
      linkedItems: true,
      items: [
        {
          titleItem: 'TITULO',
          textItem: 'texto texto texto texto texto texto texto texto texto texto texto texto'
        },
        {
          titleItem: 'TITULO',
          textItem: 'texto texto texto texto texto texto texto texto texto texto texto texto'
        },
        {
          titleItem: 'TITULO',
          textItem: 'texto texto texto texto texto texto texto texto texto texto texto texto'
        },
        {
          titleItem: 'TITULO',
          textItem: 'texto texto texto texto texto texto texto texto texto texto texto texto'
        }
      ]
    }
  ]
};

export const LANDING_TRAVEL: ContentLanding = {
  title: 'Qué hacer en caso de accidentes',
  sections: [
    {
      items: [],
      linkedItems: true,
      nameSection: 'NÚMEROS DE EMERGENCIA',
      nameSectionBody: 'NÚMEROS DE EMERGENCIA INTERNACIONALES'
    }
  ]
};
