export const PolicyLang = {
  Buttons: {
    DownloadAffiliateManual: 'Manual del afiliado',
    ViewPolicyCondition: 'Ver póliza y condicionado',
    DownloadPolicy: 'Descargar póliza',
    DownloadConstancy: 'Descargar constancia',
    DownLoadConditioned: 'Descargar condicionado',
    TracePolicy: 'Traspasar póliza',
    ComparePolicy: 'Comparar pólizas',
    QuotePolicy: 'Cotizar póliza',
    QuotePolicyNew: 'Cotizar una nueva póliza',
    PolicyTransfer: {
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Aceptar'
    },
    ShowDeductibles: 'Ver deducibles',
    ShowSublimits: 'Ver sublímites',
    ShowAsesor: 'Ver asesor',
    ChangeMe: 'Cambiar a esta modalidad',
    ContactToImprove: 'Contactar para mejorar',
    AutomaticDebit: {
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Aceptar'
    }
  },
  Labels: {
    Policy: 'Póliza',
    Policies: 'Pólizas',
    Agent: 'Agente',
    InsuredSelect: 'Selecciona tu asegurado',
    HouseSelect: 'Selecciona tu dirección',
    EndValidityDate: 'Fin vigencia',
    StartValidityDate: 'Inicio vigencia',
    ActivityRisk: 'Actividad de riesgo',
    YearConstruction: 'Año de la construcción',
    Building: 'Edificación',
    PolicyNumber: 'Nro. de póliza o contrato',
    NoticeNumber: 'Grupo de recibos',
    BillNumber: 'Nro. de recibo',
    BillNumberOrInvoice: 'Nro. de recibo o factura',
    Content: 'Contenido',
    Fees: 'CUOTAS',
    Constancy: 'Constancia',
    Constancies: 'Constancias',
    MonthlyCost: 'Valor {{periodo}}',
    Modality: 'Modalidad',
    Plan: 'Plan',
    PaymentPlan: 'Plan de pago',
    PolicyTransfer: {
      documentType: 'Tipo de documento',
      documentNumber: 'Número de documento',
      firstName: 'Apellido paterno',
      lastName: 'Apellido materno',
      names: 'Nombres',
      phone: 'Teléfono',
      email: 'Correo electrónico (opcional)'
    },
    CancelAutomaticDebt: '¿Quieres cancelar el débito automático?',
    DisabledDebit: 'Desactivado',
    ActionNotAllowed: 'Esta acción no esta disponible'
  },
  Texts: {
    VigencyPolicy: 'Vigencia: ',
    CurrentPolicy: 'Vence: ',
    ExpiredPolicy: 'Venció: ',
    StartsPolicy: 'Inicia: ',
    Fee: 'Cuota',
    Fees: 'cuotas',
    Unique: 'Única',
    WithOutFees: 'Pago único',
    DeadLine: 'Fecha límite',
    NumberPolicy: 'Nro. de póliza',
    NumberContract: 'Nro. de contrato',
    VigencyStart: 'Inicio de vigencia',
    VigencyEnd: 'Fin de vigencia',
    LastRenewal: 'Última renovación',
    Quote: 'Prima',
    QuoteBase: 'Prima', // Antes Prima Base
    WithOutCoverage: 'SIN COBERTURA',
    Renew: 'Renovación',
    Renewal: 'Renovación automática',
    Frequency: 'Frecuencia de declaración',
    Amount: 'Monto planilla',
    Ensured: 'asegurados',
    PolicyPaid: 'Póliza pagada',
    InsuredRate: 'Suma asegurada',
    ContactReason: {
      QuotePolicy: 'COTIZACION',
      ComparePolicy: 'COMPARACION'
    },
    changeModalityInProgress: 'Cambio de modalidad en proceso',
    changeModalityFrom: 'Cambiar a partir del ',
    constancyType: 'Tipo de constancias',
    EndOfTerms: 'Fin de vigencia'
  },
  Titles: {
    DetailMyPolicy: 'DETALLES DE MI PÓLIZA',
    DetailMyContract: 'DETALLES DE MI CONTRATO',
    SubTitleListPolicies: 'Lista de Pólizas',
    DetailMyHome: 'DETALLES DE MI HOGAR',
    MyCoverages: 'MIS COBERTURAS',
    MyPolicies: 'MIS PÓLIZAS',
    MySoat: 'MIS SOAT',
    PolicyTransferModal: 'TRASPASAR PÓLIZA A OTRA PERSONA',
    // Comparar Polizas
    ComparePolicy: 'SELECCIONA TU PÓLIZA A COMPARAR',
    SelectPolicy: 'SELECCIONA TU PÓLIZA',
    ChoiseYourInsurance: 'Escoje tu seguro',
    CompareYourPolicy: 'Compara tu póliza',
    Coverages: 'Coberturas',
    MoreDeductibles: 'Otros deducibles',
    MyPolicy: 'Mi póliza',
    Recommended: '¡Te recomendamos!',
    QuotePolicy: 'COTIZA TU SEGURO',
    ContactToImprove: 'Te contactaremos para mejorar tu póliza'
  },
  Messages: {
    PolicyTransferDone: '¡Hemos recibido los datos!',
    PolicyTransferSuccess: 'Nos contactaremos pronto para realizar la transferencia de la póliza.',
    WithoutPolicies: '¡No tienes pólizas asociadas!',
    NoCompareForm: 'No podemos comparar tu póliza.<br>Por favor, déjanos tus datos para comunicarnos contigo.',
    NoCompareAgent: 'No podemos comparar tu póliza.<br>Por favor comunícate con tu asesor MAPFRE.',
    ContactYouCompare: 'Te contactaremos en un breve período de tiempo para confirmar el cambio',
    ConfirmChangeModality: '¿Estás seguro de cambiar de modalidad?',
    TextChangeModality: 'Al cambiar de póliza se modificará tu prima:',
    TimeChangeModality: 'El cambio de modalidad será en {{days}} días.',
    FailChangeModality: `Por el momento no podemos cambiar la modalidad de tu póliza.<br>
      Por favor comunícate con un asesor MAPFRE para realizar el proceso.`,
    SuccessChangeModality: 'El cambio de modalidad de tu póliza se realizará en un promedio de {{days}} días.',
    AmountMax: 'Recuerda que el monto máximo que MAPFRE puede pagar por esta póliza es de:',
    CurrentOwnPolicy: `Todavía estás protegido por tu póliza hasta el {{fecha}}.
        No te preocupes, nos pondremos en contacto contigo pronto.`,
    CurrentPolicy: `El vehículo todavía está protegido por una póliza hasta el {{fecha}}. Mapfre se pondrá en contacto
      con el cliente cuando la póliza esté por vencer.`,
    FailComparePolicy: `Por el momento no podemos realizar la comparación de tu póliza.`,
    FailCompareExpiredPolicy: `No se puede realizar la comparación de tu póliza debido a que se encuentra vencida.`,
    ContactToImprove: 'Por favor, registra tus datos para que uno de nuestros asesores se ponga en contacto contigo.',
    MonthDeclare: 'Para declarar este mes no deben existir periodos con recibos vencidos',
    PaymentSuggestion: 'Podrás pagar manualmente a través de nuestros diferentes canales físicos y digitales.',
    NotAllowSuggestion:
      'Esta acción no esta disponible para este tipo de póliza, si tienes alguna pregunta comunicate con nosotros al 213 3333.'
  }
};

export const COVERAGES_TIME_LIST = [{ text: '1 año', value: '1' }, { text: '2 años', value: '2' }];
export const YEAR_DEFAULT = '1';

// accidentes personales en APP
export const MD_SALUD_AP = 'MD_SALUD_AP';
