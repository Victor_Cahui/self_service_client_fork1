import { PLATFORMS } from '@mx/core/shared/helpers/util/platform.service';

export const ClassNavigator: Array<IClassNavigator> = [
  {
    navigator: PLATFORMS.CHROME,
    body: ['g-chrome'],
    html: ['g-chrome']
  },
  {
    navigator: PLATFORMS.FIREFOX,
    body: ['g-firefox'],
    html: ['g-firefox']
  },
  {
    navigator: PLATFORMS.IE,
    body: ['g-ie'],
    html: ['g-ie']
  },
  {
    navigator: PLATFORMS.IOSMOBILE,
    body: ['g-safari-iphone'],
    html: ['g-safari-iphone']
  }
];

export const ClassDevice: Array<IClassNavigator> = [
  {
    navigator: PLATFORMS.ANDROID,
    body: ['g-android'],
    html: ['g-android']
  }
];

export interface IClassNavigator {
  navigator: string;
  body: Array<string>;
  html: Array<string>;
}
