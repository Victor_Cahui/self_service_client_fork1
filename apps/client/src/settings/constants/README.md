
## policy-values.ts

Contiene datos de configuración según el tipo de póliza.

```
export interface IPolicyType {
  code: string;
  icon: string;
  dropDownTitle: string;
  path: string;
}
```

En *dropdown-beneficiaries* se utiliza para mostrar el complemento del título y la ruta para el link *Ver Detalle* según el tipo de póliza.


## router-titles.ts

Por cada página, define una constante con los datos de configuración para el header. Por ejemplo:

```
export const HOME_HEADER: IHeader = {
  title: 'BIENVENIDO AL PORTAL DE CLIENTES',
  subTitle: '',
  icon: '',
  back: false
};
```
Estos valores se asignan desde app.ts

### Personalizar Titulos de Pagina 

Para asignar los títulos usar:

```
import { HEALTH_INSURANCE_HEADER } from '@mx/settings/constants/router-titles';  // Cambiar por la constante que corresponda
import { HeaderHelperService } from '@mx/services/general/header-helper.service';

:

 constructor(
    :
    private headerHelperService: HeaderHelperService) { }


ngOnInit(): void {
    : 
    this.setHeaderTitle();
    :
  }

  setHeaderTitle(): void {
    // Cambios requeridos, por ejemplo: 
    const vehicleHeader = { ...DETAIL_VEHICLE_HEADER };
    vehicleHeader.subTitle = vehicleHeader.subTitle.replace(`{modelo}`, this.vehicleDetail.modelo);
    this.headerHelperService.set(vehicleHeader);
  }
:

```
