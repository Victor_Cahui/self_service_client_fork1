import {
  CALENDAR_ICON,
  CAR_ICON,
  COIN_ICON,
  COVERAGES_ICON,
  CROSS_ICON,
  MEDICAL_CONSULTATION_ICON,
  PIN_ICON,
  USER_ICON
} from './images-values';

export const MAPFRE_SERVICES = [
  {
    name: 'Médico a domicilio',
    key: 'SRV_MEDICO_DOMICILIO',
    icon: 'icon-doctor',
    route: '/mapfre-services/home-doctor'
  },
  {
    name: 'Chofer de reemplazo',
    key: 'SRV_CHOFER_REEMPLAZO',
    icon: 'icon-chofer',
    route: '/mapfre-services/substitute-driver'
  },
  {
    name: 'Auxilio mecánico',
    key: 'SRV_AUXILIO_MECANICO',
    icon: 'icon-grua'
  },
  {
    name: 'Grúa o Auxilio mecánico',
    key: 'SRV_AUXILIO_MECANICO',
    icon: 'icon-asistencia'
  },
  {
    name: 'Reportar accidente de autos"',
    key: 'ACC_REPORTAR_ROBO_AUTOS',
    icon: 'icon-reportarRobo'
  },
  {
    name: 'Reportar accidente de autos',
    key: 'ACC_REPORTAR_ACCIDENTE_AUTOS',
    icon: 'icon-reportarAccidente'
  },
  {
    name: 'Delivery Medicamentos',
    key: 'ACC_DELIVERY_MEDICAMENTO',
    icon: 'icon-deliveryMedicamentos',
    route: 'https://farmaciadelivery.mapfre.com.pe/'
  },
  {
    name: 'Solicitar asistencia (procuración)',
    key: 'SRV_PROCURACION',
    icon: 'icon-asistencia'
  },
  {
    name: 'Ambulancia',
    key: 'SRV_AMBULANCIA',
    icon: 'icon-ambulancia',
    route: '/mapfre-services/ambulance'
  },
  {
    name: 'Bonos Easy Taxi',
    key: 'SRV_EASY_TAXI',
    icon: 'icon-easytaxi'
  },
  {
    name: 'Vidriero',
    key: 'SRV_VIDRIERO',
    icon: 'icon-vidrieria'
  },
  {
    name: 'Gasfitero',
    key: 'SRV_GASFITERO',
    icon: 'icon-gafistero'
  },
  {
    name: 'Housekeeping',
    key: 'SRV_HOUSEKEEPING',
    icon: 'icon-housekeeping'
  },
  {
    name: 'Electricista',
    key: 'SRV_ELECTRICISTA',
    icon: 'icon-electricista'
  },
  {
    name: 'Experto',
    key: 'SRV_INFORMATICO',
    icon: 'icon-tecnologico'
  },
  {
    name: 'Cerrajería',
    key: 'SRV_CERRAJERO',
    icon: 'icon-cerrejia'
  },
  {
    name: 'default',
    key: 'SRV_DEFAULT',
    icon: 'icon-default'
  },
  {
    name: 'not_found',
    key: 'SRV_NOT_FOUND',
    icon: 'icon-search'
  },
  {
    name: 'Cita médica',
    key: 'ACC_AGENDAR_CITA_MEDICA',
    icon: 'icon-cita-medica',
    route: '/digital-clinic/schedule/steps/1'
  },
  {
    name: 'Cita covid',
    key: 'ACC_AGENDAR_CITA_MEDICA_COVID',
    icon: 'icon-cita-medica',
    route: '/digital-clinic/schedule-covid/steps/1'
  },
  {
    name: 'MAPFRE Doc',
    key: 'ACC_MAPFRE_DOC',
    icon: 'icon-mapfre-doc',
    route: 'https://mapfredoc.pe/login'
  }
];

export const MAPFRE_SERVICES_SECTIONS = [
  {
    key: 'MD_AUTOS',
    name: 'Autos'
  },
  {
    key: 'MD_SALUD',
    name: 'Salud'
  },
  {
    key: 'MD_VIDA',
    name: 'Vida'
  },
  {
    key: 'MD_HOGAR',
    name: 'Hogar'
  },
  {
    key: 'MD_DECESOS',
    name: 'Decesos'
  },
  {
    key: 'MD_SCTR_SALUD',
    name: 'SCTR SALUD'
  },
  {
    key: 'MD_SCTR_PENSION',
    name: 'SCTR PENSION'
  },
  {
    key: 'MD_SOAT',
    name: 'SOAT'
  },
  {
    key: 'MD_SOAT_ELECTRO',
    name: 'SOATe'
  },
  {
    key: 'MD_EPS',
    name: 'EPS'
  },
  {
    key: 'MD_TIPO_USUARIO',
    name: 'tipo_usuario'
  }
];

export const ICON_ITEMS = {
  PRICE: COIN_ICON,
  VEHICLE: CAR_ICON,
  PERSON: USER_ICON,
  POLICY: CROSS_ICON,
  DATE: CALENDAR_ICON,
  PIN: PIN_ICON,
  DOCTOR: MEDICAL_CONSULTATION_ICON,
  COVERAGE: COVERAGES_ICON
};

export const SERVICES_TYPE = {
  SUBSTITUTE_DRIVER: 'CHOFER_REEMPLAZO',
  HOME_DOCTOR: 'MEDICO_DOMICILIO',
  MEDICAL_APPOINTMENT: 'CITA_MEDICA'
};

export const STATUS_SERVICE = {
  ACCEPTED: 'ACEPTADO',
  INPROGRESS: 'ENESPERA',
  REJECTED: 'RECHAZADO',
  ATTENDED: 'ATENDIDO'
};

// Configuracion de poligonos
export const ALLOWED_AREAS_COLOR = {
  BORDER: '#4a90e2',
  WIDTH: 2,
  FILL: 'rgba(5, 92, 216, 0.2)'
};

export const MAPFRE_LOCATION = {
  latitude: -12.128185,
  longitude: -77.0256308,
  minZoom: 10,
  markerZoom: 15
};

export function getAppIcon(codeApp): string {
  const apps = {
    AJUST: '10_web_ajustador.svg',
    ASA: '07_siniestros_agricola.svg',
    CEW2: '03_gestion_virtual_eps.svg',
    default: 'default.svg',
    ECU: '06_estado_cuenta_unificado.svg',
    EMIS2: '04_transportes.svg',
    GPS: '01_web_gps.svg',
    NSCTR: '05_constancias_sctr.svg',
    PCWEPS: '13_programa_cronicos.svg',
    PRI: '14_pago_reembolso_indemnizaciones.svg',
    PVCN: '02_prevencion_sctr.svg',
    RCPS: '08_registro_seguimiento_siniestros.svg',
    SCTRAV: '09_denuncia_siniestros_sctr_vida.svg',
    SVM: '12_web_siniestros_vida_masivos.svg',
    WEBSUSVDA: '11_web_suscripciones.svg'
  };

  return apps[codeApp] || apps.default;
}

export function getObjIcon(codeObj): string {
  const obj = {
    default: 'default.svg',
    TRANSPORTES: '04_transportes.svg'
  };

  return obj[codeObj] || obj.default;
}
