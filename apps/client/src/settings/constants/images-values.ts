export const IMAGE_NOT_FOUND_ICON = 'assets/images/icons/404.svg';
export const MARKER_CLUSTER_ICON = './assets/images/pins/cluster.png';
export const NO_PENDING_PROCEDURES_ICON = './assets/images/icons/sintramites-pendientes.svg';
export const MARKER_HEALTH = 'assets/images/pins/salud.png';
export const MARKER_OFFICE = 'assets/images/pins/oficina.png';
export const BUTTON_TO_TOP = 'assets/images/pngs/up.png';
export const SEARCH_ICON = 'assets/images/pngs/icon-search.png';
export const VIGENCY_ICON = 'assets/images/services/icon-vigencia.svg';
export const BANK_PAYMENT_ICON = 'assets/images/services/icon-payment.svg';
export const CARD_PAYMENT_ICON = 'assets/images/services/icon-card.svg';
export const MAPFRE_DOLLARS_ICON = 'assets/images/services/icon-dolares.svg';
export const DRIVER_ICON = 'assets/images/services/icon-chofer.svg';
export const DRIVER_REJECTED_ICON = 'assets/images/services/icon-chofer-sad.svg';
export const HOME_DOCTOR_ICON = 'assets/images/services/icon-doctor.svg';
export const WARNING_ICON = 'assets/images/services/icon-error.svg';
export const INFO_ICON = 'assets/images/services/icon-info.svg';
export const PIN_MAP_ICON = 'assets/images/pins/dot.png';
export const PIN_END_MARK_ICON = 'assets/images/pins/destination-gray.png';
export const PIN_AMBULANCE_ICON = 'assets/images/pins/ambulance.png';
export const TIME_NONE_ICON = 'assets/images/services/icon-time-none.svg';
export const PIN_START_MARK_ICON = 'assets/images/pins/user.png';
export const DOCUMENT_ICON = 'assets/images/services/icon-document.svg';
export const DOCUMENT_CHECK_ICON = 'assets/images/services/icon-document-check.svg';
export const CAR_PHOTO_ICON = 'assets/images/services/icon-car-photo.svg';
export const RADIO_EDIT_ICON = 'assets/images/services/icon-radio-edit.svg';
export const FAIL_ICON = 'assets/images/services/icon-fail.svg';
export const INSPECTION_ICON = 'assets/images/services/icon-inspection.svg';
export const TECH_CHECK_ICON = 'assets/images/services/icon-tech-check.svg';
export const MONEY_ICON = 'assets/images/services/icon-money.svg';
export const TABLET_PLUS_ICON = 'assets/images/services/icon-tablet-plus.svg';
export const TIME_ICON = 'assets/images/services/icon-time.svg';
export const TIME_ALERT_ICON = 'assets/images/services/icon-time-alert.svg';
export const COMPLETE_ICON = 'assets/images/services/icon-complete.svg';
export const EMAIL_ICON = 'assets/images/services/icon-mail.svg';
export const NO_PENDING_PAYMENTS_ICON = 'assets/images/icons/sinpagos-pendientes.svg';
export const PHONE_ALERT_ICON = 'assets/images/services/icon-phone-notification.svg';
export const EVALUATION_ICON = 'assets/images/services/icon-evaluacion.svg';
export const EVALUATION_COVID_ICON = 'assets/images/services/icon-aplicaciones-cita.svg';
export const AUTOMATIC_DEBIT = 'assets/images/services/icon-debito-automatico.svg';
export const MAINTANCE_ICON = 'assets/images/services/icon-maintance.svg';
export const NOTIFICATION_ICON = 'assets/images/icons/icon_notification.svg';
export const NOTIFICATION_INFO_ICON = 'assets/images/icons/notification-info.svg';
export const NOTIFICATION_SUCCESS_ICON = 'assets/images/icons/notification-success.svg';
export const NOTIFICATION_WARNING_ICON = 'assets/images/icons/notification-warning.svg';
export const DOCUMENT_NOT_AVAILABLE = 'assets/images/icons/icon-document-not-available.svg';

export const ADD_ICON = 'icon-mapfre_052_agregar';
export const SERVICES_DEFAULT_ICON = 'icon-services-default';
export const IDEA_ICON = 'icon-mapfre_218_idea';
export const WALLET_ICON = 'icon-mapfre_055_wallet';
export const REFRESH_ICON = 'icon-mapfre_097_refresh';
export const EDIT_ICON = 'icon-mapfre_035_edit';
export const CHECK_ICON = 'icon-mapfre_003_check';
export const POWER_ICON = 'icon-mapfre_220_power';
export const ARROW_LEFT_ICON = 'icon-mapfre_010_arrow-left';
export const MEDICAL_CONSULTATION_ICON = 'icon-mapfre_168_medical_consultation';
export const CROSS_ICON = 'icon-mapfre_020_cross';
export const PROFILE_ICON = 'icon-mapfre_014_perfil';
export const PROFILE_ADD_ICON = 'icon-mapfre_064_user-add';
export const FILE_ICON = 'icon-mapfre_058_file';
export const CAR_ICON = 'icon-mapfre_019_car';
export const ARROW_RIGHT_ICON = 'icon-mapfre_075_arrow-right';
export const ARROW_DOWN_ICON = 'icon-mapfre_219_arrow_down';
export const MAIL_ICON = 'icon-mapfre_060_mail';
export const COIN_ICON = 'icon-mapfre_104_ico-coin';
export const CLOSE_ICON = 'icon-mapfre_080_close4';
export const PHONE_ICON = 'icon-mapfre_128_telephone';
export const PIN_ICON = 'icon-mapfre_015_location-pin';
export const CLOCK_ICON = 'icon-mapfre_062_reloj';
export const DOWNLOAD_ICON = 'icon-mapfre_059_download';
export const CIRCLE_CLOSE_ICON = 'icon-mapfre_121_close';
export const WARNING_2_ICON = 'icon-mapfre_051_alerta';
export const CIRCLE_CHECK_ICON = 'icon-mapfre_057_circle-check';
export const TOOL_ICON = 'icon-mapfre_085_auxilomecanico';
export const PDF_ICON = 'icon-mapfre_093_pdf';
export const XLS_ICON = 'icon-mapfre_093_xls';
export const PAPER_ICON = 'icon-mapfre_221_paper';
export const UPLOAD_ICON = 'icon-mapfre_076_upload';
export const COVERAGES_ICON = 'icon-mapfre_120_cobertura';
export const CALENDAR_ICON = 'icon-mapfre_063_agendar';
export const USER_ICON = 'icon-mapfre_037_user';
export const HOUSE_ICON = 'icon-mapfre_017_house';
export const CIRCLE_ICON = 'icon-mapfre_066_circle';
export const MAPFRE_ICON = 'icon-mapfre_044_logo_landing';
export const SHIELD_MORE_ICON = 'icon-mapfre_231_shield_more';
export const SHIELD_OUT_ICON = 'icon-mapfre_230_shield_out';
export const STAR_ICON = 'icon-mapfre_125_star';
export const STAR_OUT_ICON = 'icon-mapfre_126_star_out';
export const SOAT_ICON = 'icon-mapfre_077_soat';
export const HEART_OUT_ICON = 'icon-mapfre_016_heart';
export const HEART_ICON = 'icon-mapfre_025_heart2';
export const BIRD_ICON = 'icon-mapfre_148_bird';
export const SERVICES_ICON = 'icon-mapfre_150_services';
export const CARD_ICON = 'icon-mapfre_055_card';
export const RUC_ICON = 'icon-mapfre_240_ruc';
export const HOSPITAL_ICON = 'icon-mapfre_213_hospital';
export const HELMET_OUT_ICON = 'icon-mapfre_073_casco';
export const HELMET_ICON = 'icon-mapfre_116_casco';
export const SHIELD_CHECK_OUT_ICON = 'icon-mapfre_232_shield_check';
export const SHIELD_CHECK_ICON = 'icon-mapfre_233_shield_check_out';
export const PERSON_ICON = 'icon-mapfre_234_person';
export const TRAVEL_ICON = 'icon-mapfre_235_travel';
export const TRAVEL_OUT_ICON = 'icon-mapfre_244_travel_out';
export const BUILDING_ICON = 'icon-mapfre_018_building';
export const MAPFRE_CIRCLE_ICON = 'icon-mapfre_041_iso';
export const BRIEFCASE_ICON = 'icon-mapfre_021_-briefcase';
export const BANKING_ICON = 'icon-mapfre_236_banking';
export const PORCENTAGE_ICON = 'icon-mapfre_096_porcentage';
export const NATURAL_DEAD_ICON = 'icon-mapfre_185_natural_death';
export const DISABILITY_ICON = 'icon-mapfre_141_disability';
export const FUNERAL_EXPENSES_ICON = 'icon-mapfre_237_soat_gastos_sepelio';
export const MEDICAL_EXPENSES_ICON = 'icon-mapfre_238_soat_gastos_medicos';
export const MEDICAL_EXPENSES_WITHOUT_CROSS_ICON = 'icon-cuotas';
export const TEMP_DISABILITY_ICON = 'icon-mapfre_239_soat_incapacidad_temporal';
export const NOTICE_ICON = 'icon-mapfre_241_avisos';
export const APPS_ICON_IN = 'icon-mapfre_242_apps';
export const APPS_ICON_OUT = 'icon-mapfre_243_apps_out';
export const ACCIDENTAL_DEATH_ICON = 'icon-mapfre_184_accidental_death';
export const AMBULANCE_ICON = 'icon-mapfre_178_ambulance_service';
export const TRAVEL_CANCELLATION_ICON = 'icon-mapfre_245_compensacion_cancelacion_viaje';
export const REFUND_TRAVEL_DELAYED_ICON = 'icon-mapfre_246_reembolso_gastos_vuelo_demorado';
export const REPATRIATION_SANITARY_ICON = 'icon-mapfre_247_repatriacion_sanitaria';
export const ODONTOLOGY = 'icon-mapfre_248_odontologia_urgencia';
export const DELAY_EXPENSES_REFUND_ICON = 'icon-mapfre_249_reembolso_gastos_demora';
export const LUGGAGE_LOST_COMPENSATION_ICON = 'icon-mapfre_250_compension_perdida_equipaje';
export const HOTEL_EXPENSES_CONVALESCENCE_ICON = 'icon-mapfre_251_gastos_hotel_convalecencia';
export const HOTEL_EXPENSES_RELATIVE_ICON = 'icon-mapfre_252_gastos_hotel_familiar';
export const REPATRIATION_ICON = 'icon-mapfre_253_repatriacion_restos_mortales';
export const MPD_ANNIVERSARY_ICON = 'icon-mapfre_254_aniversario_mapfre_dolares';
export const MPD_CONTRACT_ICON = 'icon-mapfre_255_contratacion_mapfre_dolares';
export const MPD_ICON = 'icon-mapfre_256_mapfre_dolares';
export const MPD_REACTIVATION_ICON = 'icon-mapfre_257_reactivacion_mapfre_dolares';
export const MPD_RENEWAL_ICON = 'icon-mapfre_258_renovacion_mapfre_dolares';
export const MPD_RESTITUTION_ICON = 'icon-mapfre_259_restitucion_mapfre_dolares';
export const MPD_EXPIRED_CALENDAR_ICON = 'icon-mapfre_260_vencimiento_mapfre_dolares';
export const ACCIDENT_HOME_ICON = 'icon-mapfre_261_regreso_anticipado_home';
export const REFUNDS_ICON = 'icon-mapfre_262_reembolsos';
export const BURIAL_ICON = 'icon-mapfre_263_sepelio';
export const NATURAL_DEATH_COMPENSATION = 'icon-mapfre_264_indemnizacion_muerte_natural';
export const TRAVEL_RATE_DIFFERENCE_ICON = 'icon-mapfre_265_diferencia_tarifario_travel';
export const MEDICINES_ICON = 'icon-mapfre_266_medicamentos_botiquin';
export const TRANSPORT_LOCATION_ICON = 'icon-mapfre_267_localizacion_transporte_world';
export const MESSAGE_TRANSMISSION_ICON = 'icon-mapfre_268_transmision_mensajes';
export const RELATIVE_TRANSFER_ICON = 'icon-mapfre_269_traslado_familiar_travel';
export const FAILED_TRANSFER_ICON = 'icon-mapfre_270_traslado_fallido_travel';
export const FAMILY_ICON = 'icon-mapfre_271_family';
export const ACCIDENT_COMPENSATION_ICON = 'icon-mapfre_272_indemnizacion_accidente';
export const CHILDREN_SCORT_ICON = 'icon-mapfre_273_acompanamiento_menores';
export const PASSPORT_ICON = 'icon-mapfre_274_passport';
export const MEDICAL_ASSIST_ICON = 'icon-mapfre_275_asistencia-medica-enfermedad';
export const MEDICAL_ASSIST_PRE_ICON = 'icon-mapfre_276_asistencia-medica-enfermedad-prexistente';
export const HOSPITAL_MEDICINE_ICON = 'icon-mapfre_277_medicina-hospitalaria';
export const OUTPATIENT_MEDICINE_ICON = 'icon-mapfre_278_medicina-ambulatoria';
export const EPS_ICON = 'icon-mapfre_280_eps';
export const PERSONAL_ACCIDENT_ICON = 'icon-mapfre_281_accidentes_personales';
export const COMPARATE_ICON = 'icon-mapfre_095_comparate';
export const GEOLOCATION_ICON = 'icon-mapfre_100_mask';
export const DOWN_ICON = 'icon-mapfre_101_ico-dow';
export const UP_ICON = 'icon-mapfre_102_ico-up';
export const STETHOSCOPE_ICON = 'icon-mapfre_209_stethoscope';
export const QANDA_ICON = 'icon-mapfre_031_qanda';
export const PPFM_ICON = 'icon-mapfre_283_benefit';
export const PPFM_OUT_ICON = 'icon-mapfre_283_benefit_out';

export const SERVICES_PATH = 'assets/images/services/';
export const APPS_PATH = 'assets/images/apps/';

export const IMAGE_VEHICLE_QUOTE = {
  VEHICLE_BACK: 'assets/images/pngs/vehicle-back.png',
  VEHICLE_STICKER: 'assets/images/pngs/vehicle-sticker.png',
  VEHICLE_CARD: 'assets/images/pngs/vehicle-card.png'
};

// BANCOS
export const IMAGE_BANKS = {
  BBVA: 'assets/images/banks/bbva.svg',
  BCP: 'assets/images/banks/bcp.svg',
  BANBIF: 'assets/images/banks/banbif.svg',
  SCOTIABANK: 'assets/images/banks/scotiabank.svg',
  INTERBANK: 'assets/images/banks/interbank.svg'
};

export const IMAGE_CARDS = {
  MASTER_CARD: 'assets/images/banks/card-mastercard.svg',
  VISA: 'assets/images/banks/card-visa.svg',
  AMERICAN_EXPRESS: 'assets/images/banks/card-american.svg',
  DINNER_CLUB: 'assets/images/banks/card-dinners.svg',
  CVV: 'assets/images/cards/card-back.png',
  MM_AA: 'assets/images/cards/card-front.png'
};

// Incidentes
export const LIST_TYPE_INCIDENTE = {
  ASIST_HOGAR_INCEND_EXPLOS: 'icon-mapfre_193_fire',
  ASIST_HOGAR_ROTURA_VIDRIOS: 'icon-mapfre_156_break_glasses',
  ASIST_HOGAR_LLUVIA: 'icon-mapfre_157_rain',
  ASIST_HOGAR_TODO_RIESGO: 'icon-mapfre_158_alert',
  ASIST_HOGAR_OTRAS_CAUSAS: 'icon-mapfre_162_ruptures',
  ASIST_HOGAR_DESPER_TUB_DEP_TAN: 'icon-mapfre_159_damage',
  ASIST_HOGAR_TERREMOTO_TEMBLOR: 'icon-mapfre_160_earthquake',
  ASIST_HOGAR_MAREMOTO: 'icon-mapfre_161_seaquake',
  ASIST_HOGAR_DAÑO_MALICIOSO: 'icon-mapfre_202_malicious_damage',
  ASIST_HOGAR_ROBO_FRACTURA: 'icon-mapfre_205_broken_window',
  ASIST_HOGAR_ROBO_ESCALONAM: 'icon-mapfre_204_broken_home',
  ASIST_HOGAR_ROBO_ASALTO_ATRACO: 'icon-mapfre_203_thief'
};

// Iconos por tipo de pago
export const PAYMENT_TYPE = {
  TP_RECAUDA_BANCO_VENTANILLA: { icon: BANKING_ICON },
  TP_RECAUDA_BANCO_INTERNET: { icon: BANKING_ICON },
  TP_RECAUDA_BANCO_AGENTE: { icon: BANKING_ICON },
  TP_RECAUDA_BANCO_OTRO_CANAL: { icon: BANKING_ICON },
  TP_PRIMERA_VENTA_DECESO: { icon: MAPFRE_CIRCLE_ICON },
  TP_COBRANZA_SEGUROS_MASIVOS: { icon: MAPFRE_CIRCLE_ICON },
  TP_TARJETA_CREDITO_COMERC_ELECT: { icon: CARD_ICON },
  TP_COBRO_WEB_AUTOS: { icon: CARD_ICON },
  TP_CARGO_RECURRENTE: { icon: CARD_ICON },
  TP_COBRANZA_POS: { icon: CARD_ICON },
  TP_UNIDAD_COBRANZAS: { icon: MAPFRE_CIRCLE_ICON },
  TP_OFIC_MAPFRE: { icon: MAPFRE_CIRCLE_ICON },
  TP_OFIC_MAPFRE_POLIZA_AFILIADA: { icon: MAPFRE_CIRCLE_ICON },
  DEFAULT: { icon: MAPFRE_CIRCLE_ICON }
};

// Iconos Carta de Garantia
export const GUARANTEE_LETTERS = {
  GUARANTEE_LETTER_DOWNLOAD: 'assets/images/icons/icon-cartasgarantia-download.svg',
  GUARANTEE_LETTER_DOWNLOAD_NOT_ALLOW: 'assets/images/icons/icon-cartasgarantia-download-not-allow.svg',
  USER_ICON: 'assets/images/icons/icon-cartasgarantia-user.svg',
  CLINIC_ICON: 'assets/images/icons/icon-cartasgarantia-clinica.svg',
  CALENDAR_ICON: 'assets/images/icons/icon-cartasgarantia-calendar.svg'
};
