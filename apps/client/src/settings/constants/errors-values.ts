import { IMAGE_NOT_FOUND_ICON, MAINTANCE_ICON, WARNING_ICON } from './images-values';

export const HTTP_CODE_GATEWAY_TIMEOUT = 504;

export const ERROR_TYPES = {
  504: {
    title: 'Estimado Usuario',
    message: 'Por el momento nuestros sistemas se encuentran en mantenimiento.',
    suggestion: 'Por favor, inténtalo más tarde.',
    operationCode: 'AUT-A001',
    image: MAINTANCE_ICON
  },
  500: {
    title: 'Estimado Usuario',
    message: 'Ha ocurrido un error.',
    suggestion: 'Por favor, inténtalo más tarde.',
    image: IMAGE_NOT_FOUND_ICON
  },
  900: {
    title: 'Estimado Usuario',
    message: 'Por el momento, la acción requerida no esta disponible.',
    suggestion: 'Por favor, inténtalo más tarde.',
    image: WARNING_ICON
  },
  901: {
    title: 'Estimado Usuario',
    message: 'La acción realizada no se ha completado.',
    suggestion: 'Por favor, inténtalo más tarde.',
    icon: 'icon--fail'
  },
  404: {
    title: 'Estimado Usuario',
    message: 'Por el momento nuestros sistemas se encuentran en mantenimiento.',
    suggestion: 'Por favor, inténtalo más tarde.',
    operationCode: 'AUT-A001',
    image: MAINTANCE_ICON
  },
  503: {
    title: 'Estimado Usuario',
    message: 'Por el momento nuestros sistemas se encuentran en mantenimiento.',
    suggestion: 'Por favor, inténtalo más tarde.',
    operationCode: 'AUT-A001',
    image: MAINTANCE_ICON
  }
};

export const ERROR_UPLOAD_FILE = {
  Repeat: {
    title: '¡Oops!',
    message: 'No puedes usar el mismo archivo dos veces.'
  },
  Format: {
    title: '¡Oops!',
    message: 'Solo puedes adjuntar archivos con formato JPEG, PNG o PDF.'
  },
  Size: {
    title: '¡Oops!',
    message: 'El archivo ha superado el límite máximo de subida'
  }
};
