// tslint:disable: max-file-line-count

import { environment } from '@mx/environments/environment';
import { PPFM_OUT_ICON } from '@mx/settings/constants/images-values';
import { IButtonsLink, IClientAction } from '@mx/statemanagement/models/general.models';
import { ModalEmergencyPhones } from '../lang/travel.lang';

export const SYSTEM_CODE_SELF_SERVICE = 'AUWEB';
export const SYSTEM_CODE_OIM = 'OIM';
export const APP_ORIGIN = 'AUWEB';
export const HOME = 'HOME';
export const ALL_LINKS = 'ALL_LINKS';
export const ALL_SERVICES = 'ALL_SERVICES';
export const ALL_COVERAGES = 'ALL';
export const APPLICATION_CODE = 'APC';
export const SEARCH_ORDEN_ASC = 'ASC';
export const SEARCH_ORDEN_DESC = 'DESC';
export const NOTIFICATION_SERVICE = 'SERVICIO';
export const FORMAT_DATE_DDMMYYYY = 'dd/MM/yyyy';
export const FORMAT_DATE_SEP_DDMMYYYY = 'dd-MM-yyyy';
export const FORMAT_DATE_DDMMYYYYHHMM = 'dd/MM/yyyy - HH:mm';
export const FORMAT_DATE_YYYYMMDD = 'yyyy/MM/dd';
export const GOOGLE_MAP_API_KEY = 'AIzaSyD9BbgaxmHww8sgf9pj4IJqiSxalsBvTHE';
export const OTHER_CITY = 'OTROS';
export const DEFAULT_FILE_SIZE_UNIT = 'mb';
export const FORMAT_NAME_MONTH_YEAR = 'MMMM yyyy';
export const SOLES_SYMBOL_CURRENCY = 'S/';
export const DEFAULT_MPD_DECIMALS = 2;
export const DEFAULT_MPD_CURRENCY = 2;
export const DEFAULT_SUBSTITUTE_DRIVER_HOURS = 24;
export const DEFAULT_HOME_DOCTOR_HOURS = 24;
export const DEFAULT_ZOOM_MAP = 18;
export const MAX_NUMBER_DAYS_TO_INSPECTION = 7;
export const DEFAULT_RUC_LENGTH = 11;
export const SUCCESS_CODE = 200;
export const ERROR_CODE = 900;
export const ERROR_CODE_EMAIL_NOT_REGISTER = 202;
export const INTERNAL_ERROR = 500;
export const DEFAULT_EARLY_POLICY_RENEWAL_DAYS = 60;
export const DEFAULT_PAYMENT_RECEIPTS_MONTHS = 12;
export const DEFAULT_MAX_SIZE_MB_TEMPLATE_DECLARE = 20;
export const LOGIN_RECURRENT_SOCIAL_REASON_MAX_LENGTH = 50;
export const DEFAULT_CAROUSELS_TIME_INTERVAL_BANNERS = 5;

export const VERIFY_TOKEN_TIME_M = 1;
export const ATTEMPTS_VERIFY_TOKEN = 3;

export const MAX_NUMBER_ITEMS_HORIZONTAL_TIMELINE = 8;

export const INSURED_DROPDOWN_ITEMS = 5;

export const GMAPS_DIR = 'https://www.google.com/maps/dir/';
export const MAPFRE_VEHICLES_QUOTE_DIR = 'https://seguro-vehicular.mapfre.com.pe/';
export const LIFE_INSURANCE_QUOTE_DIR = 'https://apps.mapfre.com.pe/seguro-de-vida-con-ahorro/';
export const TRAVEL_INSURANCE_QUOTE_DIR = 'https://seguro-de-viaje.mapfre.com.pe/';
export const MY_BENEFITS_DIR = 'https://club.mapfre.com.pe/';

export const CENTER_VIRTUAL = '451-0';
export const NO_CENTER = '0-0';
export const COMPARE_KEY = 'compare';
export const EDIT_DATA_KEY = 'edit_data';
export const QUOTE = 'quote';
export const MY_PAYMENTS = 'my_payments';
export const TYPE_APPOINTMENT = {
  VIRTUAL: 'V',
  PRESENCIAL: 'P'
};

export const WHATSAPPWEB = 'https://api.whatsapp.com/send';
export const WHATSAPPPHONE = '51999919133';
export const TEXT_WHATSAPP = 'WhatsApp';

export const COLOR_STATUS = {
  RED: 'g-c--red1',
  YELLOW: 'g-c--yellow1',
  GREEN: 'g-c--green4',
  GRAY: 'g-c--gray18',
  NONE: ''
};

export const URL_FRAGMENTS = {
  INSURED: 'insuredList'
};

export const STORAGE = {
  PAYMENT_CHANNEL: 'payment_channel',
  PAYMENT_CHANNEL_PER_USER: 'payment_channel_per_user',
  CITA_COVID: 'cita_covid',
  CITA_COVID_ESPECIALITY: 'cita_covid_speciality',
  CITA_COVID_SCHEDULE: 'cita_covid_schedule',
  CITA_COVID_REVIEW: 'cita_covid_review',
  CITA_COVID_SUCCESS: 'cita_covid_review',
  HOME_DOCTOR_REQUEST_STEP_ONE: 'home_doctor_request_step_one',
  HOME_DOCTOR_REQUEST_STEP_TWO: 'home_doctor_request_step_two',
  HOME_DOCTOR_REQUEST_STEP_TWO_DATA: 'home_doctor_request_step_two_data'
};

export const SINISTER_TYPE = {
  INPROGRESS: 'enCurso',
  HISTORICAL: 'historicos'
};

export enum AM_PM {
  AM = 'am',
  PM = 'pm'
}

export enum X_AXIS {
  RIGHT = 'right',
  LEFT = 'left'
}

export enum Y_AXIS {
  TOP = 'top',
  BOTTOM = 'bottom'
}

export const RAMOS = {
  VIDA_ENTERA: 604
};

export const PPFM_ICON = {
  MD_SALUD: PPFM_OUT_ICON,
  MD_HOGAR: PPFM_OUT_ICON,
  MD_VIDA: PPFM_OUT_ICON,
  MD_AUTOS: PPFM_OUT_ICON
};

// Para cuando se obtenga la configuración del cliente
export const CLIENT_ACTIONS: Array<IClientAction> = [
  {
    code: 'SRV_CHOFER_REEMPLAZO',
    circleBgc: 'g-bg-c--green2',
    icon: 'icon-mapfre_088_choferdereemplazo',
    visible: true,
    route: '/mapfre-services/substitute-driver'
  },
  {
    code: 'SRV_AMBULANCIA',
    circleBgc: 'g-bg-c--orange1',
    icon: 'icon-mapfre_082_ambulancia',
    visible: true,
    route: '/mapfre-services/ambulance'
  },
  {
    code: 'SRV_MEDICO_DOMICILIO',
    circleBgc: 'g-bg-c--yellow1',
    icon: 'icon-mapfre_091_medico',
    visible: true,
    route: '/mapfre-services/home-doctor'
  },
  {
    code: 'ACC_REPORTAR_ROBO_AUTOS',
    circleBgc: 'g-bg-c--purple1',
    icon: 'icon-mapfre_145_stole',
    visible: true,
    route: '/vehicles/stolen/reports'
  },
  {
    code: 'ACC_REPORTAR_ACCIDENTE_AUTOS',
    circleBgc: 'g-bg-c--red5',
    icon: 'icon-mapfre_069_reportar-siniestro',
    visible: true,
    route: '/vehicles/accidents/reports'
  },
  {
    code: 'ACC_BUSCAR_CLINICA',
    circleBgc: 'g-bg-c--red6',
    icon: 'icon-mapfre_070_buscar-clinicas',
    visible: true,
    route: '/clinics/search'
  },
  {
    code: 'ACC_REPORTAR_DANIO_HOGAR',
    circleBgc: 'g-bg-c--green5',
    icon: 'icon-mapfre_084_hogar',
    visible: true,
    route: '/household/hurt-and-steal/reports'
  },
  {
    code: 'ACC_TELEMEDICINA',
    circleBgc: 'g-bg-c--green4',
    icon: 'icon-mapfre_068_telemedicina',
    visible: false
  },
  {
    code: 'ALL_SERVICES',
    circleBgc: 'g-bg-c--purple2',
    icon: 'icon-mapfre_039_hand_house',
    visible: true,
    route: '/mapfre-services'
  },
  {
    code: 'ACC_ASISTENCIA',
    circleBgc: 'g-bg-c--green5',
    icon: 'icon-mapfre_068_telemedicina',
    visible: true,
    route: ModalEmergencyPhones
  },
  {
    code: 'ACC_DELIVERY_MEDICAMENTO',
    circleBgc: 'g-bg-c--green4',
    icon: 'icon-ACC_DELIVERY_MEDICAMENTO',
    visible: true,
    route: 'https://turecetafarmapfre.mapfre.com.pe/'
  },
  {
    code: 'ACC_MAPFRE_DOC',
    circleBgc: 'g-bg-c--red1',
    icon: 'icon-televideo',
    visible: true,
    route: 'https://mapfredoc.pe/login'
  }
];

// What do you to do
export const BUTTONS_LINK: Array<IButtonsLink> = [
  {
    key: EDIT_DATA_KEY,
    description: 'Modificar mis datos',
    policyTypes: [
      'HOME',
      'MD_SCTR',
      'MD_AUTOS',
      'MD_SALUD',
      'MD_EPS',
      'MD_HOGAR',
      'MD_VIDA',
      'MD_VIAJES',
      'MD_DECESOS'
    ],
    route: '/profile',
    visible: true
  },
  {
    key: QUOTE,
    description: 'Cotizar una nueva póliza',
    policyTypes: ['MD_AUTOS', 'MD_SALUD', 'MD_EPS', 'MD_HOGAR', 'MD_VIDA', 'MD_VIAJES', 'MD_DECESOS', 'MD_VIAJES'],
    routeByType: [
      '/vehicles/quote',
      '/health/quote',
      '/health/quote',
      '/household/quote',
      '/life/quote',
      'https://seguro-de-viaje.mapfre.com.pe',
      '/death/quote'
    ],
    visible: true
  },
  {
    description: 'Modificar los datos de mis pólizas',
    policyTypes: ['HOME'],
    route: '/myPolicies',
    visible: true
  },
  {
    description: 'Ver mis servicios MAPFRE',
    policyTypes: ['HOME'],
    route: '/mapfre-services',
    visible: true
  },
  {
    description: 'Contratar SCTR',
    policyTypes: ['MD_SCTR'],
    route: 'https://landing.mapfre.com.pe/sctr/',
    external: true,
    visible: true
  },
  {
    description: 'Ver red de oficinas',
    policyTypes: ['HOME'],
    route: '/offices',
    visible: true
  },
  {
    key: COMPARE_KEY,
    description: 'Comparar mi póliza con otros productos',
    policyTypes: ['MD_AUTOS'],
    openModal: true, // Abre Modal para seleccionar la póliza o va a la ruta si es una sola
    routeByType: ['/vehicles/vehicularInsurance/compare', '/life/compare'],
    visible: environment.ACTIVATE_COMPARE_POLICY
  },
  {
    key: MY_PAYMENTS,
    description: 'Ver mis pagos',
    policyTypes: [
      'HOME',
      'MD_AUTOS',
      'MD_SALUD',
      'MD_EPS',
      'MD_HOGAR',
      'MD_VIDA',
      'MD_VIAJES',
      'MD_DECESOS',
      'MD_SCTR'
    ],
    route: '/payments/list',
    paramsPolicyType: true,
    visible: true
  },
  {
    description: 'Hacer una reclamación',
    policyTypes: [
      'HOME',
      'MD_SCTR',
      'MD_AUTOS',
      'MD_SALUD',
      'MD_EPS',
      'MD_HOGAR',
      'MD_VIDA',
      'MD_VIAJES',
      'MD_DECESOS'
    ],
    route: 'https://www.mapfre.com.pe/sobre-mapfre-peru/contacto/reclamaciones/',
    external: true,
    visible: true
  }
];

// Longitud/Formato de Campos
export const GENERAL_MAX_LENGTH = {
  card_expired_date_max: 19,
  email: 60,
  nameCompany: 20,
  job: 50,
  // TODO: Trabajar con una sola constante para las direcciones (refactor)
  address: 100,
  addressProfile: 80,
  name: 80,
  last_name_father: 30,
  last_name_mother: 30,
  cellPhone: 12,
  numberPlate: 6,
  accesoryModel: 70,
  searchCoveragesClinic: 20,
  searchRecord: 40,
  searchInsured: 40,
  searchInsureds: 60,
  password: 30,
  amount: 6,
  owner_card: 80,
  number_card_vs_mc: 19,
  number_card_dc: 16,
  number_card_ae: 17,
  card_email: 80,
  card_mm_aa: 5,
  card_cvv_vs_mc_dc: 3,
  card_cvv_ae: 4,
  sd_start_point: 100,
  interior_department_number: 25,
  hd_symptom: 150,
  chassis: 18,
  motor: 15,
  vehicle_accesories: 2,
  correspondence_address: 100,
  correspondence_another_name: 50,
  numberPolice: 13,
  // TODO: uno solo
  sd_phone_contact: 18,
  phone: 18,
  correspondence_another_phone: 18,
  reason_social: 80,
  numberDocumentGeneric: 13
};

export const GENERAL_MIN_LENGTH = {
  cellPhone: 9,
  numberPlate: 6,
  searchRecord: 7,
  searchInsured: 3,
  searchInsureds: 3,
  updatePassword: 6,
  password: 3,
  owner_card: 4,
  card_email: 8,
  sd_start_point: 5,
  hd_symptom: 5,
  chassis: 7,
  motor: 5,
  correspondence_address: 5,
  correspondence_another_name: 3,
  // TODO: uno solo
  phone: 7,
  sd_phone_contact: 7,
  correspondence_another_phone: 7,
  reason_social: 4,
  numberDocumentGeneric: 7
};

export const MASK_CARD = {
  VISA_MASTERCARD: [
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    ' ',
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    ' ',
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    ' ',
    /\d/,
    /\d/,
    /\d/,
    /\d/
  ],
  DINNER_CLUB: [/\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/],
  AMERICAN_EXPRESS: [
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    ' ',
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    ' ',
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/
  ],
  EXPIRED_DATE: [/\d/, /\d/, '/', /\d/, /\d/],
  CVV: [/\d/, /\d/, /\d/]
};

export const REG_EX = {
  name: '[A-Za-zÁÉÍÓÚáéíóúÑñü\\s]{1,80}',
  lastname: '[A-Za-zÁÉÍÓÚáéíóúÑñü\\s]{1,30}',
  phone: '[0-9]{8,12}',
  numeric: '^[0-9]*$',
  alphaNumeric: '^[0-9A-Za-z]*$',
  records: '[0-9a-zA-Z/-]*$',
  plate: /^(?=\w*\d)(?=\w*[A-Z])\S{0,6}$/,
  amount: /^(?![A-Za-z]*$)[0-9+]+$/,
  card_expired_date: /[0-9]{2}\/[0-9]{2}/i,
  noEmojis: /^[a-zA-Z\d$-/:-?{-~!"^°@#_ñÑáéíóúÁÉÍÓÚ` \\\[\]]*$/,
  url: /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)+[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/,
  alphaNumericText: '[a-zA-Z0-9.\\sáéíóúÁÉÍÓÚÑñü]*'
};

export enum EPaymentProcess {
  BUY_VEHICLE = 'payment_vehicle'
}

export const movementsTypes = {
  Aniversario: {
    code: 'Aniversario',
    ico: 'icon-mapfre_254_aniversario_mapfre_dolares'
  },
  Campaña: {
    code: 'Campaña',
    ico: 'icon-mapfre_256_mapfre_dolares'
  },
  Contratación: {
    code: 'Contratación',
    ico: 'icon-mapfre_255_contratacion_mapfre_dolares'
  },
  Reactivación: {
    code: 'Reactivación',
    ico: 'icon-mapfre_257_reactivacion_mapfre_dolares'
  },
  Renovación: {
    code: 'Renovación',
    ico: 'icon-mapfre_258_renovacion_mapfre_dolares'
  },
  Restitución: {
    code: 'Restitución',
    ico: 'icon-mapfre_259_restitucion_mapfre_dolares'
  },
  Vencimiento: {
    code: 'Vencimiento',
    ico: 'icon-mapfre_260_vencimiento_mapfre_dolares'
  }
};

export const MovementStatus = {
  GANADO: {
    code: 'GANADO'
  },
  USADO: {
    code: 'USADO'
  },
  VENCIDO: {
    code: 'VENCIDO'
  }
};

export const ConfigMapView = {
  canViewDeductibles: true,
  canViewDistance: true,
  canViewFooterButtons: true,
  canViewFavorite: true,
  inDesktop: {
    hasFilter: true,
    hasSeeker: true
  },
  inResponsive: {
    hasFilter: true,
    hasSeeker: true
  }
};

export const StatusAppointment = {
  pasada: 'PASADA',
  pendiente: 'PENDIENTE'
};

export const OperationAppointment = {
  anular: 'ANULAR',
  reagendar: 'REAGENDAR',
  reprogramar: 'REPROGRAMAR'
};

export const SumaAseguradaInfo = {
  ILIMITADA: {
    body: 'Beneficio máximo anual por persona',
    code: 'ILIMITADA',
    title: 'Ilimitada'
  },
  LIMITADA: {
    body: 'Beneficio máximo anual por persona',
    code: 'LIMITADA',
    title: 'Limitada'
  }
};

export const ErrorCodes = {
  'AUT-150': {
    code: 'AUT-150',
    message: 'En estos momentos nos encontramos en mantenimiento, se procederá con el cierre de sesión.'
  }
};

export enum TYPE_OF_MODULE {
  CDM = 'CDM'
}

export enum STATE_TERMS_AND_CONDITIONS {
  APPROVED = 'APROBADO',
  TO_BE_APPROVED = 'POR_APROBAR'
}

export const CodigoMoneda = {
  SOLES: 1,
  DOLARES: 2
};

export const ServicesList = [
  {
    description: '<b>Podras realizar un triaje</b> antes de tu consulta médica.',
    icon: '../../assets/images/services/icon-evaluacion.svg'
  },
  {
    description: '<b>Consulta meédica online sin costo</b> las 24 horas, todos los días.',
    icon: '../../assets/images/services/icon-mapfre-doc.svg'
  },
  {
    description: '<b>Consulta médica online por especialidad</b> sin costo, según disponibilidad.',
    icon: '../../assets/images/services/icon-cita-medica.svg'
  },
  {
    description: '<b>Asesoría médica telefónica</b> todos los días, las 24 horas.',
    icon: '../../assets/images/services/icon-call-doctor.svg'
  },
  {
    description:
      '<b>Asesoría médica por Chat todos los días</b> las 24 horas a través de nuestro Portal clientes o App MAPFRE.',
    icon: '../../assets/images/services/icon-chatea-doctor.svg'
  },
  {
    description: '<b>Envío de médico a domicilio para consultar ambulatorias.</b> Costo por atención S/ 70.00',
    icon: '../../assets/images/services/icon-doctor.svg'
  },
  {
    description:
      '<b>Cubre al 100% los gastos de ambulancia</b> por emergencia médica o accidental del paciente. <p>*Aplican límites geográficos</p>',
    icon: '../../assets/images/services/icon-ambulancia.svg'
  },
  {
    description:
      '<b>Reserva de cita online para la prueba de antigenos para descarte COVID-19</b> en Centros Médicos MAPFRE. <p>*Costo de S/ 50.00 por prueba.</p>',
    icon: '../../assets/images/services/icon-aplicaciones-cita.svg'
  },
  {
    description: '<b>Atención en Centros Médicos MAPFRE.</b>',
    icon: '../../assets/images/services/icon-medic-center.svg'
  }
];

export const BenefitsList = [
  {
    description: 'Más de 200 clinicas a nivel nacional'
  },
  {
    description: '100% de cobertura por cáncer'
  },
  {
    description: 'Atenciones en nuestra Red de centros Médicos MAPFRE'
  },
  {
    description: 'Consultas médicas desde S/ 10.00'
  },
  {
    description: 'Médico a domicilio a S/ 60.00'
  },
  {
    description: 'Gratis 2 servicios de ambulancia'
  },
  {
    description: 'Gratis chequeos preventivos'
  }
];

export enum KEY_SERVICE_DIGITAL_CLINIC {
  AUT_MED = 'AUT_MED',
  CIT_COV = 'CIT_COV',
  VID_CON = 'VID_CON',
  CHA_DOC = 'CHA_DOC',
  CIT_ESP = 'CIT_ESP',
  DEL_MED = 'DEL_MED',
  MED_DOM = 'MED_DOM',
  SOL_AMB = 'SOL_AMB',
  RED_CLI = 'RED_CLI'
}

export enum ACTION_SERVICE_DIGITAL_CLINIC {
  MODAL = 'MODAL',
  REDIRECTION = 'REDIRECCION',
  REDIRECTION_OUT = 'REDIRECCION_FUERA'
}
