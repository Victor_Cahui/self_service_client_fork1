// tslint:disable: max-line-length max-file-line-count
// ORIGIN: https://docs.google.com/spreadsheets/d/1x1Az0UO43jCo3CNsHLc2e2iQU0gQRDcbzN-QRSX-MyM/edit#gid=1972121035

export const MFP_Iniciar_Sesion_2A: any = () => ({
  control: '',
  controlAction: 'custom',
  event: 'ga_event_login',
  category: 'Login',
  action: 'MFP - Iniciar Sesion',
  label: 'Ingresar - {{state}}',
  dimentions: 'userId:{{g.userId}}'
});

export const MFP_Iniciar_Sesion_2B: any = () => ({
  control: 'forgotPasswordLink',
  controlAction: 'click',
  event: 'ga_event_login',
  category: 'Login',
  action: 'MFP - Iniciar Sesion',
  label: '{{forgotPasswordLinkText}}'
});

export const MFP_Iniciar_Sesion_2C: any = () => ({
  control: 'secondButton',
  controlAction: 'click',
  event: 'ga_event_login',
  category: 'Login',
  action: 'MFP - Iniciar Sesion',
  label: '{{buttonText}}'
});

export const MFP_Recuperar_Contrasenha_3A: any = (lbl: string) => ({
  event: 'ga_event_login',
  category: 'Login',
  action: 'MFP - Recuperar Contraseña',
  label: `${lbl}`
});

export const MFP_Recuperar_Contrasenha_3B: any = () => ({
  control: 'goToLogin',
  controlAction: 'click',
  event: 'ga_event_login',
  category: 'Login',
  action: 'MFP - Recuperar Contraseña',
  label: '{{label}}',
  dimentions: ''
});

export const MFP_Crear_Cuenta_4A: any = (lbl: string) => ({
  event: 'ga_event_login',
  category: 'Login',
  action: 'MFP - Crear Cuenta',
  label: `${lbl}`
});

export const MFP_Mi_Perfil_5A: any = (lbl: string) => ({
  event: 'ga_event',
  category: 'Portal Del Cliente',
  action: 'MFP - Mi Perfil',
  label: `Ir a: ${lbl}`
});

export const MFP_Mi_Cuenta_5B: any = (lbl: string) => ({
  event: 'ga_event',
  category: 'Header',
  action: 'MFP - Mi Cuenta',
  label: `Menu Desplegable - ${lbl}`
});

export const MFP_Conocer_mis_Beneficios_5C: any = (lbl: string) => ({
  event: 'ga_event',
  category: 'Portal Del Cliente',
  action: 'MFP - Conocer mis Beneficios',
  label: `Ir a: ${lbl}`
});

export const MFP_Add_To_Cart_5D: any = () => ({
  control: 'buttonPayPolicy',
  controlAction: 'custom',
  event: 'ga_event',
  category: 'Portal Del Cliente',
  action: 'MFP - Pagos Pendientes',
  label: 'Pagar - {{descripcionPoliza}}',
  dimentions: 'numeroPoliza:{{policyNumber}},categoriaPoliza:{{policyType}}'
});

export const MFP_Siniestros_5E: any = () => ({
  control: 'linkViewDetailBox',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Portal Del Cliente',
  action: 'MFP - Siniestros',
  label: '{{label}}',
  dimentions: 'numeroAccidente:{{numeroAccidente}}'
});

export const MFP_ViewAll_5F: any = () => ({
  control: 'linkViewAllBox',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Portal Del Cliente',
  action: 'MFP - {{boxTitle}}',
  label: '{{label}}'
});

export const MFP_Mis_Polizas_6A_Detail: any = () => ({
  control: 'linkViewDetail',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Portal Del Cliente',
  action: 'MFP - Mis Polizas',
  label: '{{label}} - {{policy}}',
  dimentions: 'numeroPoliza:{{policyNumber}},estadoPoliza:{{coverageStatus}}'
});

export const MFP_Mis_Polizas_6A_SoatRenew: any = () => ({
  control: 'buttonRenewSoat',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Portal Del Cliente',
  action: 'MFP - Mis Polizas',
  label: '{{label}} - {{policy}}',
  dimentions: 'numeroPoliza:{{policyNumber}},estadoPoliza:{{coverageStatus}},categoriaPoliza:{{policyType}}'
});

export const MFP_Mis_Polizas_6B: any = () => ({
  control: 'linkViewAll',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Portal Del Cliente',
  action: 'MFP - Mis Polizas',
  label: `Ver Todos`
});

export const MFP_Promo_View_Load_6C: any = (promotion: {
  id: string;
  name: string;
  creative: string;
  position: string;
}) => {
  return {
    event: 'promo-view',
    ecommerce: { promoView: { promotions: [promotion] } }
  };
};

export const MFP_Promo_Click_6D: any = (promotion: {
  id: string;
  name: string;
  creative: string;
  position: string;
}) => {
  return {
    event: 'promo-click',
    ecommerce: { promoView: { promotions: [promotion] } }
  };
};

export const MFP_Que_quieres_hacer_Iconos_6E: any = () => ({
  control: 'linkViewItem',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Servicios',
  action: 'MFP - Que quieres hacer - Iconos',
  label: '{{label}}'
});

export const MFP_Que_quieres_hacer_Mas_Servicios_6F: any = () => ({
  control: 'linkViewMoreServices',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Servicios',
  action: 'MFP - Que quieres hacer - Mas Servicios',
  label: '{{label}}'
});

export const MFP_Menu_Principal_7A: any = () => ({
  control: 'linkMenu',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Menu',
  action: 'MFP - Menu Principal',
  label: '{{label}}'
});

export const MFP_Menu_Principal_Submenu_7B: any = () => ({
  control: 'linkSubmenu',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Menu',
  action: 'MFP - {{g.section}} - Submenu',
  label: '{{label}}'
});

export const MFP_Mis_Polizas_8A: any = () => ({
  control: 'linkActions',
  controlAction: 'click',
  category: '{{g.section}}',
  action: `MFP - {{g.subsection}} - Mis Polizas`,
  label: '{{label}}'
});

export const MFP_Mis_Polizas_8B: any = () => ({
  control: 'linkViewDetail',
  controlAction: 'click',
  category: '{{g.section}}',
  action: `MFP - {{g.subsection}} - Mis Polizas`,
  label: '{{label}} - {{policy}}',
  dimentions: 'numeroPoliza:{{policyNumber}},estadoPoliza:{{coverageStatus}}'
});

export const MFP_Cotizacion_9A: any = (lbl: string) => ({
  event: 'ga_event',
  category: 'Vehiculos',
  action: 'MFP - Cotizacion',
  label: `${lbl}`
});

export const MFP_Cotizacion_Plan_10A: any = () => ({
  control: 'linkPlanQuote',
  controlAction: 'click',
  event: 'ga_event',
  category: '{{g.section}}',
  action: 'MFP - {{g.subsection}}',
  label: '{{label}} - {{button}}',
  dimentions: 'valorPaquete:{{amount}}'
});

export const MFP_Faqs_10B: any = () => ({
  control: 'linkMoreFaq',
  controlAction: 'click',
  event: 'ga_event',
  category: '{{g.section}}',
  action: 'MFP - {{g.subsection}}',
  label: '{{label}}'
});

export const MFP_FAQS_11A: any = () => ({
  control: '',
  controlAction: 'custom',
  event: 'ga_event',
  category: 'Preguntas Frecuentes',
  action: 'MFP - {{g.section}}',
  label: '{{faqTitle}}',
  dimentions: ''
});

export const MFP_FAQS_11B: any = () => ({
  control: '',
  controlAction: 'custom',
  event: 'ga_event',
  category: 'Preguntas Frecuentes',
  action: 'MFP - {{g.section}}',
  label: '{{buttonText}}',
  dimentions: ''
});

export const MFP_Cotiza_Tu_Seguro_Paso_1_12A: any = () => ({
  control: 'btnConfirm',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Seguro Vehicular',
  action: 'MFP - Cotiza Tu Seguro - Paso 1',
  label: '{{label}}',
  dimentions: 'categoriaPoliza:MD_AUTOS'
});

export const MFP_Cotiza_Tu_Seguro_Paso_1_12B: any = () => ({
  control: 'btnCancel',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Seguro Vehicular',
  action: 'MFP - Cotiza Tu Seguro - Paso 1',
  label: '{{label}}'
});

export const MFP_Cotiza_Tu_Seguro_Paso_2_13A: any = () => ({
  control: 'quoteEdit',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Seguro Vehicular',
  action: 'MFP - Cotiza Tu Seguro - Paso 2',
  label: '{{label}}'
});

export const MFP_Cotiza_Tu_Seguro_Paso_2_Comprar_13B: any = () => ({
  control: 'buttonComparePay',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Seguro Vehicular',
  action: 'MFP - Cotiza Tu Seguro - Paso 2',
  label: 'Comprar - {{cobertura}}',
  value: '{{valorCobertura}}',
  dimentions: 'PlanDePago:{{planDePago}},currency:{{currency}},categoriaPoliza:MD_AUTOS'
});

export const MFP_Compra_Tu_Seguro_Paso_1_14A: any = () => ({
  control: 'buttonsStep1',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Seguro Vehicular',
  action: 'MFP - Compra Tu Seguro - Paso 1',
  label: '{{label}}',
  dimentions: 'categoriaPoliza:MD_AUTOS'
});

export const MFP_Compra_Tu_Seguro_Paso_2_15A: any = () => ({
  control: 'buttonsStep2',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Seguro Vehicular',
  action: 'MFP - Compra Tu Seguro - Paso 2',
  label: '{{label}}',
  dimentions: 'categoriaPoliza:MD_AUTOS'
});

export const MFP_Compra_Tu_Seguro_Paso_3_16A: any = () => ({
  control: 'vehiclesQuoteBuyButtons',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Seguro Vehicular',
  action: 'MFP - Compra Tu Seguro - Paso 3',
  label: '{{label}}',
  dimentions: 'categoriaPoliza:MD_AUTOS'
});

export const MFP_Compra_Tu_Seguro_Paso_3_16B: any = () => ({
  control: 'vehiclesQuoteBuySelect',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Seguro Vehicular',
  action: 'MFP - Compra Tu Seguro - Paso 3',
  label: `Tipo Inspección: {{label}}`
});

export const MFP_Compra_Tu_Seguro_Paso_3_17A: any = (lbl: string) => ({
  event: 'ga_event',
  category: 'Seguro Vehicular',
  action: 'MFP - Compra Tu Seguro - Paso 3',
  label: `Inspección presencial - ${lbl}`
});

export const MFP_Compra_Tu_Seguro_Paso_3_18A: any = (lbl: string) => ({
  event: 'ga_event',
  category: 'Seguro Vehicular',
  action: 'MFP - Compra Tu Seguro - Paso 3',
  label: `Autoinspección - ${lbl}`
});

export const MFP_Forma_De_Pago_19A: any = () => ({
  control: 'backButton',
  controlAction: 'click',
  event: 'ga_event',
  category: '{{g.section}}',
  action: 'MFP - Forma De Pago',
  label: '{{qs(client-tabs-payments li.on)}} - {{backButtonText}}',
  dimentions: ''
});

export const MFP_Compra_Tu_Seguro_Paso_4_19B: any = () => ({
  control: 'buttonPaymentOrGenerate',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Seguro Vehicular',
  action: 'MFP - Compra Tu Seguro - Paso 4',
  label: `Forma de pago {{method}} - {{txtBtn}}`,
  dimentions: 'categoriaPoliza:MD_AUTOS'
});

// export const _19B: any = (lbl: string, extraData: any) => ({
//   event: 'CheckOut Option',
//   category:
//     'https://docs.google.com/spreadsheets/d/1x1Az0UO43jCo3CNsHLc2e2iQU0gQRDcbzN-QRSX-MyM/edit#gid=558504515&range=D8',
//   action: '',
//   label: `${lbl}`,
//   ...extraData
// });

// export const _: any = (lbl: string, extraData: any) => ({
//   event: 'Purchase',
//   category:
//     'https://docs.google.com/spreadsheets/d/1x1Az0UO43jCo3CNsHLc2e2iQU0gQRDcbzN-QRSX-MyM/edit#gid=558504515&range=D5',
//   action: '',
//   label: `${lbl}`,
//   ...extraData
// });

export const MFP_Comparar_Poliza_20A: any = () => ({
  control: '',
  controlAction: 'custom',
  event: 'ga_event',
  category: '{{g.subsection}}',
  action: 'MFP - Comparar Poliza',
  label: '{{buttonText}} - {{policy}}',
  dimentions: 'numeroPoliza:{{policyNumber}},estadoPoliza:{{policyStatus}}'
});

export const MFP_Comparar_Poliza_20B: any = () => ({
  control: '',
  controlAction: 'custom',
  event: 'ga_event',
  category: '{{g.subsection}}',
  action: 'MFP - Comparar Poliza',
  label: '{{buttonText}}'
});

export const MFP_Comparar_Poliza_21A: any = () => ({
  control: 'linkChangeModality',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Seguro Vehicular',
  action: 'MFP - Comparar Poliza',
  label: `Cambiar a esta modalidad - {{lbl}}`
});

export const MFP_Comparar_Poliza_22A: any = () => ({
  control: 'btnModalPaymentSettingRefinance',
  controlAction: 'custom',
  event: 'ga_event',
  category: 'Seguro Vehicular',
  action: 'MFP - Comparar Poliza',
  label: '¿Desea cambiar de modalidad? - {{lbl}}'
});

export const MFP_SOAT_23A: any = () => ({
  control: 'linkSetPayments',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Vehiculos',
  action: 'MFP - SOAT',
  label: '{{label}}'
});

export const MFP_SOAT_23B: any = () => ({
  control: 'linkDownloadPDF',
  controlAction: 'custom',
  event: 'ga_event',
  category: 'Vehiculos',
  action: 'MFP - SOAT',
  label: 'Descargar PDF - {{label}}'
});

export const MFP_SOAT_23C: any = () => ({
  control: 'buttonPayPolicy',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Vehiculos',
  action: 'MFP - SOAT',
  label: 'Renovar - {{descripcionPoliza}}',
  dimentions: 'numeroPoliza:{{policyNumber}},estadoPoliza:{{policyStatus}},categoriaPoliza:{{policyType}}'
});

export const MFP_SOAT_23C_XXX: any = () => ({
  control: 'buttonPayPolicy',
  controlAction: 'custom',
  custom: {
    event: 'transaction',
    ecommerce: {
      currencyCode: '',
      purchase: {
        actionField: { id: null, affiliation: '{{g.site}}', revenue: '{{prima}}', tax: '0', shipping: '0' },
        products: [
          {
            name: '{{descripcionPoliza}}',
            id: null,
            price: '{{prima}}',
            brand: '{{descripcionPoliza}}',
            category: '{{tipoPoliza}}',
            quantity: '1',
            dimension28: '{{tipoPagoDescripcion}}',
            metrica1: '{{prima}}'
          }
        ]
      }
    }
  }
});

export const MFP_SECTION_24A: any = (section: string) => ({
  control: 'liTabVertical',
  controlAction: 'click',
  event: 'ga_event',
  category: '{{g.section}}',
  action: 'MFP - {{g.subsection}}',
  label: '{{label}}'
});

export const MFP_Accidentes_25A: any = () => ({
  control: 'liTabTop',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Vehiculos',
  action: 'MFP - Accidentes',
  label: '{{tabTopText}}',
  dimentions: ''
});

export const MFP_Accidentes_25B: any = () => ({
  control: 'linkViewDetailCar',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Vehiculos',
  action: 'MFP - Accidentes - {{qs(mf-tab-top ul li.g-tablist__tab--active)}}',
  label: `Ver detalle - {{lbl}}`,
  dimentions: 'numeroPoliza:{{numeroPoliza}}'
});

export const MFP_Accidentes_25C: any = () => ({
  control: 'linkViewAll',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Vehiculos',
  action: 'MFP - Accidentes - {{qs(mf-tab-top ul li.g-tablist__tab--active)}}',
  label: '{{viewAllText}}'
});

export const MFP_Accidentes_26A: any = () => ({
  control: 'linkFiles',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Vehiculos',
  action: 'MFP - Accidentes - {{qs(mf-tab-top ul li.g-tablist__tab--active)}}',
  label: `Descargar PDF - {{lbl}}`
});

export const MFP_Accidentes_26B: any = () => ({
  control: 'linkStore',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Vehiculos',
  action: 'MFP - Accidentes - {{qs(mf-tab-top ul li.g-tablist__tab--active)}}',
  label: `Descarga el App - {{label}}`
});

export const MFP_Poliza_de_Seguro_de_Salud_27A: any = () => ({
  control: 'liTabTop',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Seguro de Salud',
  action: 'MFP - Poliza de Seguro de Salud',
  label: '{{tabTopText}}',
  dimentions: ''
});

export const MFP_Poliza_de_Seguro_de_Salud_27B: any = () => ({
  control: 'linkViewPoliza',
  controlAction: 'click',
  event: 'ga_event',
  category: '{{g.subsection}}',
  action: 'MFP - Poliza de Seguro de Salud - {{qs(mf-tab-top ul li.g-tablist__tab--active)}}',
  label: 'Descargar PDF - {{lbl}}',
  dimentions: ''
});

export const MFP_Poliza_de_Seguro_de_Salud_27C: any = () => ({
  control: 'linkViewPayment',
  controlAction: 'click',
  event: 'ga_event',
  category: '{{g.section}}',
  action: 'MFP - Poliza de Seguro de Salud - {{qs(mf-tab-top ul li.g-tablist__tab--active)}}',
  label: 'Ver Pagos',
  dimentions: ''
});

export const MFP_Poliza_de_Seguro_de_Salud_28A: any = (tabActive: string) => ({
  control: 'linkViewDetail',
  controlAction: 'click',
  event: 'ga_event',
  category: '{{g.subsection}}',
  action: `MFP - Poliza de Seguro de Salud - ${tabActive}`,
  label: 'Ver Detalle - {{lbl}}'
});

export const MFP_Poliza_de_Seguro_de_Salud_28B: any = (tabActive: string) => ({
  control: 'linkDownloadPdf',
  controlAction: 'click',
  event: 'ga_event',
  category: '{{g.subsection}}',
  action: `MFP - Poliza de Seguro de Salud - ${tabActive}`,
  label: 'Descargar PDF - {{titulo}} - {{numeroRecibo}}'
});

export const MFP_Cuotas_de_Poliza_Pagar_28C: any = (tipoPoliza: string) => ({
  control: 'buttonPayPolicy',
  controlAction: 'custom',
  event: 'ga_event',
  category: `Seguro de ${tipoPoliza}`,
  action: `MFP - Poliza de Seguro de ${tipoPoliza} - DETALLE`,
  label: 'Pagar - Cuotas de mi poliza - {{numRecibo}}',
  dimentions: 'numeroPoliza:{{policyNumber}},categoriaPoliza:{{policyType}}'
});

export const MFP_Modificar_Datos_Asegurado_29A: any = (lbl: string) => ({
  event: 'ga_event',
  category: 'Seguro de Salud',
  action: 'MFP - Modificar Datos Asegurado',
  label: `${lbl}`
});

export const MFP_Poliza_de_Seguro_de_Salud_30A: any = () => ({
  control: 'linkCardAction',
  controlAction: 'click',
  event: 'ga_event',
  category: '{{g.section}}',
  action: 'MFP - {{g.subsection}} - {{qs(mf-tab-top ul li.g-tablist__tab--active)}}',
  label: '{{label}}'
});

export const MFP_Poliza_de_Seguro_de_Salud_30B: any = () => ({
  control: 'liTabVertical',
  controlAction: 'click',
  event: 'ga_event',
  category: '{{g.section}}',
  action: 'MFP - {{g.subsection}} - {{qs(mf-tab-top ul li.g-tablist__tab--active)}}',
  label: '{{label}}'
});

export const MFP_Cotizacion_31A: any = (lbl: string) => ({
  event: 'ga_event',
  category: 'Salud',
  action: 'MFP - Cotizacion',
  label: `${lbl}`
});

export const MFP_Reembolsos_32A: any = () => ({
  control: 'liTabTop',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Salud',
  action: 'MFP - Reembolsos',
  label: '{{tabTopText}}'
});

export const MFP_Reembolsos_Solicitud_32B: any = () => ({
  control: 'linkFormRefund',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Salud',
  action: 'MFP - Reembolsos - {{qs(mf-tab-top ul li.g-tablist__tab--active)}}',
  label: '{{label}}'
});

export const MFP_Danhos_y_Robos_33A: any = () => ({
  control: 'liTabTop',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Hogar',
  action: 'MFP - Daños y Robos',
  label: '{{tabTopText}}'
});

export const MFP_Danhos_y_Robos_33B: any = () => ({
  control: 'btnAssitReport',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Hogar',
  action: 'MFP - Daños y Robos - {{qs(mf-tab-top ul li.g-tablist__tab--active)}}',
  label: '{{label}}'
});

export const MFP_Danhos_y_Robos_Reportar_Asistencia_Paso_1_34A: any = (lbl: string) => ({
  event: 'ga_event',
  category: 'Hogar',
  action: 'MFP - Daños y Robos - Reportar Asistencia - Paso 1',
  label: `${lbl}`
});

export const MFP_Danhos_y_Robos_Reportar_Asistencia_Paso_2_35A: any = (lbl: string) => ({
  control: 'declareSiniesterButtons',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Hogar',
  action: 'MFP - Daños y Robos - Reportar Asistencia - Paso 2',
  label: '{{label}}',
  dimentions: 'incidenteTipo:{{incidenteTipo}},motivoDaño:{{motivoDanho}}'
});

export const MFP_Danhos_y_Robos_Reportar_Asistencia_Paso_3_36A: any = (lbl: string) => ({
  event: 'ga_event',
  category: 'Hogar',
  action: 'MFP - Daños y Robos - Reportar Asistencia - Paso 3',
  label: `${lbl}`
});

export const MFP_Danhos_y_Robos_Reportar_Asistencia_Resumen_37A: any = () => ({
  control: 'linkEditAssistance',
  controlAction: 'click',
  event: 'ga_event',
  category: '{{g.section}}',
  action: 'MFP - {{g.subsection}} - Reportar Asistencia - Resumen',
  label: 'Modificar - {{label}}'
});

export const MFP_Danhos_y_Robos_Reportar_Asistencia_Resumen_37B: any = () => ({
  control: 'linkAssistanceStep4',
  controlAction: 'click',
  event: 'ga_event',
  category: '{{g.section}}',
  action: 'MFP - {{g.subsection}} - Reportar Asistencia - Resumen',
  label: '{{label}} - {{message}}'
});

export const MFP_Danhos_y_Robos_Reportar_Asistencia_Reportada_37C: any = (lbl: string) => ({
  event: 'ga_event',
  category: 'Hogar',
  action: 'MFP - Daños y Robos - Reportar Asistencia - Reportada',
  label: `${lbl}`
});

export const MFP_Cotizacion_Contacto_de_Agente_38A: any = () => ({
  control: 'linkContactByAgent',
  controlAction: 'click',
  event: 'ga_event',
  category: '{{g.section}}',
  action: 'MFP - {{g.subsection}} - Contacto de Agente',
  label: '{{label}}',
  dimentions: 'rangoHoras:{{hoursRange}}'
});

export const MFP_Cotizacion_Vida_39A: any = () => ({
  control: 'quoteNowLife',
  controlAction: 'click',
  event: 'ga_event',
  category: '{{g.section}}',
  action: 'MFP - {{g.subsection}}',
  label: '{{label}}',
  dimentions: 'tipoSeguro:{{qs([data-formName="insuranceType"] select)}}'
});

export const MFP_Cotizacion_Contacto_de_Agente_40A: any = (lbl: string, extraData: any) => ({
  event: 'ga_event',
  category: 'Decesos',
  action: 'MFP - Cotizacion - Contacto de Agente',
  label: `${lbl}`,
  ...extraData
});

export const MFP_Mis_Pagos_41A: any = () => ({
  control: 'liTabTop',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Mis Pagos',
  action: 'MFP - Mis Pagos',
  label: '{{tabTopText}}',
  dimentions: ''
});

export const MFP_Mis_Pagos_41B: any = () => ({
  control: 'linkAccoundStatus',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Mis Pagos',
  action: 'MFP - {{qs(mf-tab-top ul li.g-tablist__tab--active)}}',
  label: 'Descargar PDF - {{lbl}}',
  dimentions: ''
});

export const MFP_Mis_Pagos_41C: any = () => ({
  control: 'linkFilter',
  controlAction: 'click',
  event: 'ga_event',
  category: '{{g.section}}',
  action: 'MFP - {{qs(mf-tab-top ul li.g-tablist__tab--active)}} - Filtrar',
  label: 'Filtrar',
  dimentions: ''
});

export const MFP_Mis_Pagos_Filtrar_41D: any = () => ({
  control: 'linkFilterAction',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Mis Pagos',
  action: 'MFP - {{qs(mf-tab-top ul li.g-tablist__tab--active)}} - Filtrar',
  label: '{{label}}',
  dimentions: 'tipoSeguro:{{qs([formControlName="tipoPoliza"] select)}}'
});

export const MFP_Mis_Pagos_Pagar_42A: any = () => ({
  control: 'buttonPayPolicy',
  controlAction: 'custom',
  event: 'ga_event',
  category: 'Mis Pagos',
  action: 'MFP - PRÓXIMOS PAGOS',
  label: 'Pagar - {{numRecibo}}',
  value: '{{montoTotal}}',
  dimentions: 'numeroPoliza:{{policyNumber}},currency:{{currencySymbol}},categoriaPoliza:{{policyType}}'
});

export const MFP_Mis_Pagos_42B: any = () => ({
  control: 'linkDownloadPdf',
  controlAction: 'click',
  event: 'ga_event',
  category: '{{g.section}}',
  action: 'MFP - {{qs(mf-tab-top ul li.g-tablist__tab--active)}}',
  label: 'Descargar PDF - {{lbl}}',
  dimentions: ''
});

export const MFP_Mis_Pagos_42C: any = () => ({
  control: 'linkViewMore',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Mis Pagos',
  action: 'MFP - {{qs(mf-tab-top ul li.g-tablist__tab--active)}}',
  label: 'Ver Mas',
  dimentions: ''
});

export const MFP_Mis_Pagos_43A: any = () => ({
  control: 'linkBannerData',
  controlAction: 'click',
  event: 'ga_event',
  category: '{{g.section}}',
  action: 'MFP - {{qs(mf-tab-top ul li.g-tablist__tab--active)}}',
  label: '{{lbl}}',
  dimentions: ''
});

export const MFP_Mis_Pagos_43B: any = () => ({
  control: 'linkLabelUpdate',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Mis Pagos',
  action: 'MFP - {{qs(mf-tab-top ul li.g-tablist__tab--active)}} - Mis Polizas',
  label: 'Modificar - {{lbl}}',
  dimentions: ''
});

export const MFP_Configuracion_de_Pagos_44A: any = () => ({
  control: 'liTabVertical',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Mis Pagos',
  action: 'MFP - Configuracion de Pagos',
  label: '{{label}}'
});

export const MFP_Configuracion_de_Pagos_44B: any = () => ({
  control: 'settingRenevalChange',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Mis Pagos',
  action: 'MFP - Configuracion de Pagos - {{qs(mf-tab-vertical .g-tablist--menu .g-tablist-vertical__tab.active)}}',
  label: '{{label}}'
});

export const MPF_Configuracion_de_Pagos_44C: any = () => ({
  control: 'settingRenevalUseMPD',
  controlAction: 'change',
  event: 'ga_event',
  category: 'Mis Pagos',
  action: 'MPF - Configuracion de Pagos - {{qs(mf-tab-vertical .g-tablist--menu .g-tablist-vertical__tab.active)}}',
  label: '{{label}}: {{checkedtext}}'
});

export const MPF_Configuracion_de_Pagos_44D: any = () => ({
  control: 'settingRenevalBtns',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Mis Pagos',
  action: 'Configuracion de Pagos - {{qs(mf-tab-vertical .g-tablist--menu .g-tablist-vertical__tab.active)}}',
  label: '{{label}}',
  dimentions: 'modalidadRenovacion:{{modalidadRenovacion}}'
});

export const MPF_Configuracion_de_Pagos_45A: any = () => ({
  control: 'openRefinanceModal',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Mis Pagos',
  action: 'Configuracion de Pagos - {{qs(mf-tab-vertical .g-tablist--menu .g-tablist-vertical__tab.active)}}',
  label: '{{label}}'
});

export const MFP_Configuracion_de_Pagos_45B: any = () => ({
  control: 'btnModalPaymentSettingRefinance',
  controlAction: 'custom',
  event: 'ga_event',
  category: 'Mis Pagos',
  action: 'Configuracion de Pagos - Mis cuotas',
  label: 'Refinanciar Mi Poliza - {{lbl}}'
});

export const MFP_Servicios_Mapfre_46A: any = () => ({
  control: 'liTabTop',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Servicios Mapfre',
  action: 'MFP - Servicios Mapfre',
  label: '{{tabTopText}}'
});

export const MFP_Servicios_Mapfre_46B: any = () => ({
  control: 'sevicesRequestFilter',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Servicios Mapfre',
  action: 'MFP - Servicios Mapfre - {{qs(mf-tab-top ul li.g-tablist__tab--active)}}',
  label: 'Filtrar por - {{label}}'
});

export const MFP_Servicios_Mapfre_46C: any = () => ({
  control: 'inputSearch',
  controlAction: 'blur',
  event: 'ga_event',
  category: 'Servicios Mapfre',
  action: 'MFP - Servicios Mapfre - {{qs(mf-tab-top ul li.g-tablist__tab--active)}}',
  label: '{{searchtext}}'
});

export const MFP_Servicios_Mapfre_46D: any = () => ({
  control: 'serviceCardContact',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Servicios Mapfre',
  action: 'MFP - Servicios Mapfre - {{qs(mf-tab-top ul li.g-tablist__tab--active)}}',
  label: 'Solicitar - {{label}}'
});

export const Filtrar_47A: any = () => ({
  control: 'gmFilterBtn',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Red de Oficinas',
  action: 'Filtrar',
  label: 'Buscar Oficinas'
});

export const Buscar_47B: any = () => ({
  control: 'inputSearch',
  controlAction: 'blur',
  event: 'ga_event',
  category: 'Red de Oficinas',
  action: 'Buscar',
  label: '{{searchtext}}'
});

export const Ver_detalle_47C: any = () => ({
  control: 'gmFilterViewDetail',
  controlAction: 'click',
  event: 'ga_event',
  category: '{{g.section}}',
  action: '{{officeName}}',
  label: '{{label}}'
});

export const Filtrar_por_47D: any = () => ({
  control: 'checkbox',
  controlAction: 'change',
  event: 'ga_event',
  category: 'Red de Oficinas',
  action: 'Filtrar por',
  label: '{{labelCheckbox}} - {{checked}}'
});

export const Filtrar_por_47E: any = () => ({
  control: 'gmApplyFilter',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Red de Oficinas',
  action: 'Filtrar por',
  label: 'Aplicar',
  dimentions:
    'departamentoUbicOficina:{{qs([formcontrolname="departamento"] select)}},provinciaUbicOficina:{{qs([formcontrolname="provincia"] select)}},distritoUbicOficina:{{qs([formcontrolname="distrito"] select)}}'
});

export const Filtrar_48A: any = () => ({
  control: 'gmFilterBtn',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Buscador de Clinicas',
  action: 'Filtrar',
  label: `Buscar Clínicas`
});

export const Buscar_48B: any = () => ({
  control: 'inputSearch',
  controlAction: 'blur',
  event: 'ga_event',
  category: 'Buscador de Clinicas',
  action: 'Buscar',
  label: '{{searchtext}}'
});

export const Agregar_como_favorito_48C: any = () => ({
  control: 'gmFilterFavorite',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Buscador de Clinicas',
  action: '{{officeName}}',
  label: '{{favoritetoogle}} Favorito'
});

export const Ver_detalle_48D: any = () => ({
  control: 'gmFilterViewDetail',
  controlAction: 'click',
  event: 'ga_event',
  category: '{{g.section}}',
  action: '{{officeName}}',
  label: '{{label}}'
});

export const Filtrar_por_48E: any = () => ({
  control: 'checkbox',
  controlAction: 'change',
  event: 'ga_event',
  category: 'Buscador de Clinicas',
  action: 'Filtrar por',
  label: '{{labelCheckbox}} - {{checked}}'
});

export const Filtrar_por_48F: any = () => ({
  control: 'gmApplyFilter',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Buscador de Clinicas',
  action: 'Filtrar por',
  label: 'Aplicar',
  dimentions:
    'departamentoUbicOficina:{{qs([formcontrolname="department"] select)}},provinciaUbicOficina:{{qs([formcontrolname="province"] select)}},distritoUbicOficina:{{qs([formcontrolname="district"] select)}},tipoSeguro:{{qs([formcontrolname="policy"] select)}}'
});

export const Tu_Poliza_48G: any = () => ({
  control: 'openPolicyModal',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Buscador de Clinicas',
  action: 'Tu Poliza',
  label: 'Cambiar - {{label}}'
});

export const MFP_Buscador_clininca_48H: any = () => ({
  control: 'btnModalSelectPolicy',
  controlAction: 'custom',
  event: 'ga_event',
  category: 'Buscador de Clinicas',
  action: '{{action}}',
  label: '{{lbl}} - {{radioSelected}}'
});

export const GA_DIGITAL_CLINIC_ITEM = {
  control: 'gaDigitalClinicItem',
  controlAction: 'click',
  event: 'ga_event',
  category: 'Clínica Digital',
  action: '¿Qué necesitas? - {{itemAction}}',
  label: '{{itemLabel}}'
};

// tslint:disable-next-line: max-file-line-count
