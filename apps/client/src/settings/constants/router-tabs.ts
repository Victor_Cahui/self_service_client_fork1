// tslint:disable: max-file-line-count

import { environment } from '@mx/environments/environment';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { ITabItem } from '@mx/statemanagement/models/router.interface';

/**
 * Tabs Horizontal
 */
export const VEHICLES_INSURANCE_TAB = () => {
  return [
    {
      name: 'Detalles',
      route: '/detail',
      visible: true
    },
    {
      name: 'Coberturas',
      route: '/coverages',
      visible: environment.ACTIVATE_COVERAGES_VEHICLES
    }
  ];
};

export const VEHICLE_ACCIDENTS_TAB: Array<ITabItem> = [
  {
    name: 'Mis asistencias',
    route: '/assists',
    visible: environment.ACTIVATE_VEHICLE_ASSISTS,
    ga: []
  },
  {
    name: 'Reportar',
    route: '/reports',
    visible: true
  }
];

export const VEHICLE_STOLEN_TAB: Array<ITabItem> = [
  {
    name: 'Mis asistencias',
    route: '/assists',
    visible: environment.ACTIVATE_VEHICLE_ASSISTS
  },
  {
    name: 'Reportar',
    route: '/reports',
    visible: true
  }
];

export const GET_HEALTH_INSURANCE_TAB: any = (isVisibleAssistance: boolean) => {
  return [
    {
      name: 'Detalles',
      route: '/detail',
      visible: true
    },
    {
      name: 'Coberturas',
      route: '/coverages',
      visible: environment.ACTIVATE_COVERAGES_HEALTH
    },
    {
      name: 'Mis asistencias',
      route: '/assists',
      visible: isVisibleAssistance
    }
  ];
};

export const HEALTH_REFUND_TAB: Array<ITabItem> = [
  {
    name: 'Solicitar reembolso',
    route: '/request',
    visible: true
  },
  {
    name: 'Bandeja de solicitudes',
    route: '/tray',
    visible: true
  }
];

export const HOUSEHOLD_INSURANCE_TAB = () => {
  return [
    {
      name: 'Detalles',
      route: '/detail',
      visible: true
    },
    {
      name: 'Coberturas',
      route: '/coverages',
      visible: environment.ACTIVATE_COVERAGES_HOUSEHOLD
    }
  ];
};

export const HURT_AND_STEAL_TAB: Array<ITabItem> = [
  {
    name: 'Reportar',
    route: '/reports',
    visible: true
  },
  {
    name: 'Mis asistencias',
    route: '/assists',
    visible: environment.ACTIVATE_ASSISTS
  }
];

export const LIFE_INSURANCE_TAB = () => {
  return [
    {
      name: 'Detalles',
      route: '/detail',
      visible: true
    },
    {
      name: 'Coberturas',
      route: '/coverage',
      visible: environment.ACTIVATE_COVERAGES_LIFE
    }
  ];
};

export const TRAVEL_INSURANCE_TAB: Array<ITabItem> = [
  {
    name: 'Detalles',
    route: '/detail',
    visible: true
  },
  {
    name: 'Coberturas',
    route: '/coverage',
    visible: environment.ACTIVATE_COVERAGES_TRAVEL
  }
];

export const DEATH_INSURANCE_TAB: Array<ITabItem> = [
  {
    name: 'Detalles',
    route: '/detail',
    visible: true
  },
  {
    name: 'Coberturas',
    route: '/coverage',
    visible: environment.ACTIVATE_COVERAGES_DEATH
  }
];

export const PAYMENTS_TAB: Array<ITabItem> = [
  {
    name: 'Próximos pagos',
    route: '/payments/list',
    visible: true
  },
  {
    name: 'Pagos Realizados',
    route: '/payments/receipts',
    visible: true
  },
  {
    name: 'Configuración de pagos',
    route: '/payments/settings',
    visible: environment.ACTIVATE_CONFIGURATION_PAYMENT
  },
  {
    name: 'Mis Tarjetas',
    route: '/payments/mycards',
    visible: environment.ACTIVATE_MY_PAYMENT_CARDS
  }
];

export const SCTR_INSURANCE_TAB = () => {
  return [
    {
      name: 'Detalles',
      route: '/detail',
      visible: true
    },
    {
      name: 'Coberturas',
      route: '/coverages',
      visible: environment.ACTIVATE_COVERAGES_SCTR
    }
  ];
};

export const MAPFRE_SERVICES_TAB: Array<ITabItem> = [
  {
    name: 'Solicitar',
    route: '/request',
    visible: true
  },
  {
    name: 'Mis servicios',
    route: '/assists',
    visible: environment.ACTIVATE_MY_SERVICES
  }
];

/**
 * Tabs Vertical
 */

export const VEHICLES_COVERAGE_TABS: Array<ITabItem> = [
  {
    name: 'Coberturas',
    route: './',
    exact: true,
    visible: true
  },
  {
    name: 'Exclusiones',
    route: './exclusions',
    exact: false,
    visible: true
  },
  {
    name: 'Deducibles',
    route: './deductibles',
    exact: false,
    visible: true
  },
  {
    name: GeneralLang.Labels.FoilAttached,
    route: './foil-attached',
    exact: false,
    visible: true
  }
];

export const SCTR_COVERAGE_PENSION_TABS: Array<ITabItem> = [
  {
    name: 'Pensión por invalidez',
    route: './disability',
    exact: true,
    visible: true
  },
  {
    name: 'Pensión por sobrevivencia',
    route: './survival',
    exact: false,
    visible: true
  },
  {
    name: 'Gastos de sepelio',
    route: './funeral',
    exact: false,
    visible: true
  },
  {
    name: 'Exclusiones',
    route: './exclusions',
    exact: false,
    visible: true
  },
  {
    name: GeneralLang.Labels.FoilAttached,
    route: './foil-attached',
    exact: false,
    visible: true
  }
];

export const SCTR_COVERAGE_HEALTH_TABS: Array<ITabItem> = [
  {
    name: 'Asistencia y asesoramiento',
    route: './assistance',
    exact: true,
    visible: true
  },
  {
    name: 'Exclusiones',
    route: './exclusions',
    exact: false,
    visible: true
  },
  {
    name: GeneralLang.Labels.FoilAttached,
    route: './foil-attached',
    exact: false,
    visible: true
  }
];

export const SCTR_COVERAGE_DOUBLE_EMISSION_TABS: Array<ITabItem> = [
  {
    name: 'Asistencia y asesoramiento',
    route: './assistance',
    exact: true,
    visible: true
  },
  {
    name: 'Pensión por invalidez',
    route: './disability',
    exact: true,
    visible: true
  },
  {
    name: 'Pensión por sobrevivencia',
    route: './survival',
    exact: false,
    visible: true
  },
  {
    name: 'Gastos de sepelio',
    route: './funeral',
    exact: false,
    visible: true
  },
  {
    name: 'Exclusiones de salud',
    route: './exclusions',
    exact: false,
    visible: true
  },
  {
    name: 'Exclusiones de pensión',
    route: './exclusions-double-emission',
    exact: false,
    visible: true
  }
];

export const HEALTH_COVERAGE_TABS: Array<ITabItem> = [
  {
    name: 'Coberturas',
    route: './coverages',
    exact: true,
    visible: environment.ACTIVATE_COVERAGES
  },
  {
    name: 'Atención ambulatoria',
    route: './ambulatory-care',
    exact: false,
    visible: true
  },
  {
    name: 'Atención hospitalaria',
    route: './hospital-care',
    exact: false,
    visible: true
  },
  {
    name: 'Maternidad',
    route: './maternity',
    exact: false,
    visible: true
  },
  {
    name: 'Período de carencia',
    route: './waiting-period',
    exact: false,
    visible: true
  },
  {
    name: 'Condicionado',
    route: './conditioned',
    exact: false,
    visible: true
  },
  {
    name: GeneralLang.Labels.FoilAttached,
    route: './foil-attached',
    exact: false,
    visible: true
  },
  {
    name: 'Exclusiones',
    route: './exclusions',
    exact: false,
    visible: true
  },
  {
    name: 'Observaciones',
    route: './observations',
    exact: false,
    visible: true
  }
];

// HOGAR
export const HOUSEHOLD_COVERAGE_TABS: Array<ITabItem> = [
  {
    name: 'Coberturas',
    route: './coverages',
    exact: true,
    visible: environment.ACTIVATE_COVERAGES
  },
  {
    name: 'Deducibles',
    route: './deductibles',
    exact: false,
    visible: true
  },
  {
    name: 'Exclusiones',
    route: './exclusions',
    exact: false,
    visible: true
  },
  {
    name: GeneralLang.Labels.FoilAttached,
    route: './foil-attached',
    exact: false,
    visible: true
  }
];

// VIDA
export const LIFE_COVERAGE_TABS: Array<ITabItem> = [
  {
    name: 'Coberturas',
    route: './',
    exact: true,
    visible: true
  },
  {
    name: 'Exclusiones',
    route: './exclusions',
    exact: false,
    visible: true
  },
  {
    name: GeneralLang.Labels.FoilAttached,
    route: './foil-attached',
    exact: false,
    visible: true
  }
];

export const DEATH_COVERAGE_TABS: Array<ITabItem> = [
  {
    name: 'Coberturas',
    route: './',
    exact: true,
    visible: true
  },
  {
    name: 'Exclusiones',
    route: './exclusions',
    exact: false,
    visible: true
  }
];

// Configuracion de Pagos
export enum PAYMENTS_SETTINGS_OPTIONS_KEYS {
  EDIT = 'Editar',
  ASSOCIATE = 'Asociar'
}

export const PAYMENTS_SETTINGS_OPTIONS: Array<ITabItem> = [
  {
    name: PAYMENTS_SETTINGS_OPTIONS_KEYS.EDIT,
    route: './edit',
    visible: true
  },
  {
    name: PAYMENTS_SETTINGS_OPTIONS_KEYS.ASSOCIATE,
    route: './associate',
    visible: true
  }
];

export const APPOINTMENT_STEP_2 = [
  {
    icon: 'icon-mapfre_063_agendar',
    name: 'Por Fecha',
    value: 0,
    visible: true
  },
  {
    icon: 'icon-mapfre_168_medical_consultation',
    name: 'Por Doctor',
    value: 1,
    visible: true
  }
];
