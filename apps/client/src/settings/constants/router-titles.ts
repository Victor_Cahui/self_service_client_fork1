// tslint:disable-next-line: max-file-line-count
// tslint:disable: max-file-line-count

import { IHeader } from '@mx/services/general/header-helper.service';
import {
  APPS_ICON_IN,
  BIRD_ICON,
  CAR_ICON,
  CARD_ICON,
  CROSS_ICON,
  HEART_OUT_ICON,
  HELMET_OUT_ICON,
  HOSPITAL_ICON,
  HOUSE_ICON,
  PIN_ICON,
  PPFM_OUT_ICON,
  SERVICES_ICON,
  SHIELD_CHECK_OUT_ICON,
  SOAT_ICON,
  TOOL_ICON,
  TRAVEL_ICON
} from './images-values';

export const LOGIN_HEADER: IHeader = {
  title: '',
  subTitle: '',
  icon: '',
  back: false,
  url: '/login'
};

export const HOME_HEADER: IHeader = {
  title: 'BIENVENIDO AL PORTAL DE CLIENTES',
  subTitle: '',
  icon: '',
  back: false,
  url: '/home'
};

export const NOT_FOUND_HEADER: IHeader = {
  title: '',
  subTitle: '',
  icon: '',
  back: true,
  url: '/not-found'
};

export const PROFILE_HEADER: IHeader = {
  title: 'MI PERFIL',
  subTitle: '',
  icon: '',
  back: true,
  url: '/profile'
};

export const PROFILE_EDIT_HEADER: IHeader = {
  title: 'MODIFICAR DATOS',
  subTitle: 'MI PERFIL',
  icon: '',
  back: true,
  url: '/profile/edit/1'
};

export const PROFILE_DATA_HEADER: IHeader = {
  title: 'MIS DATOS',
  subTitle: 'MI PERFIL',
  icon: '',
  back: true,
  url: ''
};

export const POLICE_HEADER: IHeader = {
  title: 'MIS PÓLIZAS',
  subTitle: '',
  icon: SHIELD_CHECK_OUT_ICON,
  back: false,
  url: '/myPolicies'
};

export const VEHICLES_HEADER: IHeader = {
  title: 'VEHÍCULOS',
  subTitle: '',
  icon: '',
  back: true,
  url: '/vehicles'
};

export const VEHICLES_INSURANCE_HEADER: IHeader = {
  title: 'SEGURO VEHICULAR',
  subTitle: 'VEHÍCULOS',
  icon: CAR_ICON,
  back: false,
  url: '/vehicles/vehicularInsurance'
};

export const DETAIL_VEHICLE_HEADER: IHeader = {
  title: 'PLACA EN TRÁMITE - {modelo}',
  subTitle: 'PÓLIZA FULL COBERTURA',
  icon: '',
  back: true,
  url: ''
};

export const SOAT_HEADER: IHeader = {
  title: 'SOAT',
  subTitle: 'VEHÍCULOS',
  icon: SOAT_ICON,
  back: false,
  url: '/vehicles/soat'
};

export const SOAT_INSURANCE_HEADER: IHeader = {
  title: 'SOAT',
  subTitle: 'VEHÍCULOS',
  icon: SOAT_ICON,
  back: false,
  url: ''
};

export const VEHICLES_ACCIDENTS_HEADER: IHeader = {
  title: 'ACCIDENTES',
  subTitle: 'VEHÍCULOS',
  icon: CAR_ICON,
  back: false,
  url: '/vehicles/accidents'
};

export const VEHICLES_SHOP_HEADER: IHeader = {
  title: 'TALLERES',
  subTitle: 'VEHÍCULOS',
  icon: TOOL_ICON,
  back: false,
  url: '/vehicles/shop'
};

export const VEHICLES_STOLEN_HEADER: IHeader = {
  title: 'ROBOS',
  subTitle: 'VEHÍCULOS',
  icon: CAR_ICON,
  back: false,
  url: '/vehicles/stolen'
};

export const VEHICLES_QUOTE_HEADER: IHeader = {
  title: 'SEGURO VEHICULAR',
  subTitle: 'VEHÍCULOS',
  icon: CAR_ICON,
  back: false,
  url: '/vehicles/quote'
};

export const VEHICLES_QUOTE_DETAIL_HEADER: IHeader = {
  title: 'COTIZA TU SEGURO',
  subTitle: 'SEGURO VEHICULAR',
  icon: '',
  back: true,
  url: '/vehicles/quote/detail'
};

export const VEHICLES_QUOTE_BUY_HEADER: IHeader = {
  title: 'COMPRA TU SEGURO',
  subTitle: 'SEGURO VEHICULAR',
  icon: '',
  back: true,
  url: '/vehicles/quote/buy'
};
export const VEHICLES_QUOTE_BUY_CONTACT_HEADER: IHeader = {
  title: 'COMPRA TU SEGURO',
  subTitle: 'SEGURO VEHICULAR',
  icon: '',
  back: true,
  url: '/vehicles/quote/buy/contact'
};

export const VEHICLES_COMPARE_HEADER: IHeader = {
  title: 'COMPARADOR',
  subTitle: 'SEGURO VEHICULAR',
  icon: '',
  back: true,
  url: '/vehicles/vehicularInsurance/compare'
};

export const VEHICLES_FAQ_HEADER: IHeader = {
  title: 'PREGUNTAS FRECUENTES',
  subTitle: 'SEGURO VEHICULAR',
  icon: '',
  back: true,
  url: '/vehicles/vehicularInsurance/faqs'
};

export const LIFE_FAQ_HEADER: IHeader = {
  title: 'PREGUNTAS FRECUENTES',
  subTitle: 'SEGURO VIDA Y AHORRO',
  icon: '',
  back: true,
  url: '/life/life-insurance/faqs'
};

export const TRAVEL_FAQ_HEADER: IHeader = {
  title: 'PREGUNTAS FRECUENTES',
  subTitle: 'SEGURO DE VIAJES',
  icon: '',
  back: true,
  url: '/travel/travel-insurance/faqs'
};

export const HEALTH_HEADER: IHeader = {
  title: 'SALUD',
  subTitle: '',
  icon: CROSS_ICON,
  back: true,
  url: '/health'
};

export const HEALTH_INSURANCE_HEADER: IHeader = {
  title: 'SEGURO DE SALUD',
  subTitle: 'SALUD',
  icon: CROSS_ICON,
  back: false,
  url: '/health/health-insurance'
};

export const HEALTH_INSURANCE_EDIT: IHeader = {
  title: 'MODIFICAR DATOS DEL ASEGURADO',
  subTitle: 'PÓLIZA TREBOL SALUD',
  icon: '',
  back: true,
  url: ''
};

export const HEALTH_EPS_HEADER: IHeader = {
  title: 'EPS',
  subTitle: 'SALUD',
  icon: CROSS_ICON,
  back: true,
  url: '/health/eps'
};

export const HEALTH_QUOTE_HEADER: IHeader = {
  title: 'SEGURO DE SALUD',
  subTitle: 'SALUD',
  icon: CROSS_ICON,
  back: false,
  url: '/health/quote'
};

export const HEALTH_REFUND_HEADER: IHeader = {
  title: 'SOLICITUD DE REEMBOLSO',
  subTitle: 'SALUD',
  icon: CROSS_ICON,
  back: true,
  url: '/health/refund'
};

export const HEALTH_REFUND_REQUEST_HEADER: IHeader = {
  title: 'NUEVA SOLICITUD',
  subTitle: 'SOLICITUD DE REEMBOLSO',
  icon: CROSS_ICON,
  back: true,
  url: '/health/refund/request/step1'
};

export const HEALTH_REFUND_REQUEST_2_HEADER: IHeader = {
  title: 'NUEVA SOLICITUD',
  subTitle: 'SOLICITUD DE REEMBOLSO',
  icon: CROSS_ICON,
  back: true,
  url: '/health/refund/request/step2'
};

export const HEALTH_REFUND_REQUEST_3_HEADER: IHeader = {
  title: 'NUEVA SOLICITUD',
  subTitle: 'SOLICITUD DE REEMBOLSO',
  icon: CROSS_ICON,
  back: true,
  url: '/health/refund/request/summary'
};

export const HEALTH_REFUND_REQUEST_4_HEADER: IHeader = {
  title: 'NUEVA SOLICITUD',
  subTitle: 'SOLICITUD DE REEMBOLSO',
  icon: '',
  back: false,
  url: '/health/refund/request/finish'
};

export const HEALTH_GUARANTEE_LETTERS: IHeader = {
  title: 'SEGURO DE SALUD',
  subTitle: 'SALUD',
  icon: CROSS_ICON,
  back: false,
  url: '/health/guarantee-letters'
};

export const DIGITAL_CLINIC_HEADER: IHeader = {
  title: 'CLÍNICA DIGITAL',
  subTitle: '',
  icon: HOSPITAL_ICON,
  back: false,
  url: '/digital-clinic/services'
};

export const DIGITAL_CLINIC_LANDING_HEADER: IHeader = {
  title: 'CLÍNICA DIGITAL',
  subTitle: '',
  icon: HOSPITAL_ICON,
  back: false,
  url: '/digital-clinic/quote'
};

export const DIGITAL_CLINIC_ASSISTS: IHeader = {
  title: 'CLÍNICA DIGITAL',
  subTitle: '',
  icon: HOSPITAL_ICON,
  back: true,
  url: '/digital-clinic/assists',
  routeback: '/digital-clinic/services'
};

export const HOUSE_HEADER: IHeader = {
  title: 'HOGAR',
  subTitle: '',
  icon: '',
  back: true,
  url: '/household'
};

export const HOUSE_INSURANCE_HEADER: IHeader = {
  title: 'SEGURO DE HOGAR',
  subTitle: 'HOGAR',
  icon: HOUSE_ICON,
  back: false,
  url: '/household/household-insurance'
};

export const HOUSE_COMPARE_HEADER: IHeader = {
  title: 'COMPARADOR',
  subTitle: 'HOGAR',
  icon: '',
  back: true,
  url: '/household/household-insurance/compare'
};

export const HOUSE_HURTANDSTEAL_HEADER: IHeader = {
  title: 'DAÑOS Y ROBOS',
  subTitle: 'HOGAR',
  icon: HOUSE_ICON,
  back: false,
  url: '/household/hurt-and-steal'
};

export const HOUSE_DECLARE_SINISTER_STEP: IHeader = {
  title: 'REPORTAR ASISTENCIA',
  subTitle: 'DAÑOS Y ROBOS',
  icon: '',
  back: true,
  url: '/household/hurt-and-steal/reports'
};

export const HOUSE_DECLARE_SINISTER_FINALIZE: IHeader = {
  title: 'REPORTAR ASISTENCIA',
  subTitle: 'DAÑOS Y ROBOS',
  icon: '',
  back: false,
  url: '/household/hurt-and-steal/reports'
};

export const HOUSE_QUOTE_HEADER: IHeader = {
  title: 'SEGURO DE HOGAR',
  subTitle: 'HOGAR',
  icon: HOUSE_ICON,
  back: false,
  url: '/household/quote'
};

export const LIFE_HEADER: IHeader = {
  title: 'VIDA Y AHORRO',
  subTitle: '',
  icon: '',
  back: true,
  url: '/life'
};

export const LIFE_INSURANCE_HEADER: IHeader = {
  title: 'SEGURO DE VIDA Y AHORRO',
  subTitle: 'VIDA Y AHORRO',
  icon: HEART_OUT_ICON,
  back: false,
  url: '/life/life-insurance'
};

export const LIFE_QUOTE_HEADER: IHeader = {
  title: 'SEGURO DE VIDA Y AHORRO',
  subTitle: 'VIDA Y AHORRO',
  icon: HEART_OUT_ICON,
  back: false,
  url: '/life/quote'
};

export const TRAVEL_INSURANCE_HEADER: IHeader = {
  title: 'SEGURO DE VIAJES',
  subTitle: 'VIAJES',
  icon: TRAVEL_ICON,
  back: false,
  url: '/travel/travel-insurance'
};

export const TRAVEL_QUOTE_HEADER: IHeader = {
  title: 'SEGURO DE VIAJES',
  subTitle: 'VIAJES',
  icon: TRAVEL_ICON,
  back: false,
  url: '/travel/quote'
};

export const DEATH_INSURANCE_HEADER: IHeader = {
  title: 'SEGURO DE DECESOS',
  subTitle: 'DECESOS',
  icon: BIRD_ICON,
  back: false,
  url: '/death/death-insurance'
};

export const DEATH_HEADER: IHeader = {
  title: 'SEGURO DE DECESOS',
  subTitle: 'DECESOS',
  icon: BIRD_ICON,
  back: false,
  url: '/death/quote'
};

// Servicios MAPFRE
export const MAPFRE_SERVICE_HEADER: IHeader = {
  title: 'SERVICIOS MAPFRE',
  subTitle: '',
  icon: SERVICES_ICON,
  back: false,
  url: '/mapfre-services'
};

export const SCHEDULE_APPOINTMENT_HEADER: IHeader = {
  title: 'AGENDAR CITA MÉDICA CENTRO MÉDICO MAPFRE',
  subTitle: 'SERVICIOS MAPFRE',
  back: true,
  url: '/digital-clinic/schedule/map'
};

export const MAPFRE_SERVICE_REQUEST_HEADER: IHeader = {
  title: 'SERVICIOS MAPFRE',
  subTitle: '',
  icon: SERVICES_ICON,
  back: false,
  url: '/mapfre-services/request'
};

export const MAPFRE_SERVICE_ASSISTS_HEADER: IHeader = {
  title: 'SERVICIOS MAPFRE',
  subTitle: '',
  icon: SERVICES_ICON,
  back: false,
  url: '/digital-clinic/assists'
};

export const AMBULANCE_SERVICES_HEADER: IHeader = {
  title: 'SOLICITAR AMBULANCIA',
  subTitle: 'SERVICIOS MAPFRE',
  icon: '',
  back: true,
  url: '/mapfre-services/ambulance'
};

export const SUBSTITUTE_DRIVER_HEADER: IHeader = {
  title: 'CHOFER DE REEMPLAZO',
  subTitle: 'SERVICIOS MAPFRE',
  icon: '',
  back: true,
  url: '/mapfre-services/substitute-driver'
};

export const HOME_DOCTOR_HEADER: IHeader = {
  title: 'MÉDICO A DOMICILIO',
  subTitle: 'SERVICIOS MAPFRE',
  icon: '',
  back: true,
  url: '/mapfre-services/home-doctor'
};

// Pagos
export const PAYMENTS_HEADER: IHeader = {
  title: 'MIS PAGOS',
  subTitle: '',
  icon: CARD_ICON,
  back: false,
  url: '/payments'
};

export const PAYMENTS_RECEIPTS_HEADER: IHeader = {
  title: 'MIS PAGOS',
  subTitle: '',
  icon: CARD_ICON,
  back: false,
  url: '/payments/receipts'
};

export const PAYMENTS_SETTING_HEADER: IHeader = {
  title: 'MIS PAGOS',
  subTitle: '',
  icon: CARD_ICON,
  back: false,
  url: '/payments/settings'
};

export const PAYMENTS_SETTING_POLICY_LIST: IHeader = {
  title: 'CONFIGURACIÓN DE PAGOS',
  subTitle: 'MIS PAGOS',
  back: true,
  url: ''
};

export const OFFICES_HEADER: IHeader = {
  title: 'BUSCADOR DE OFICINAS MAPFRE',
  subTitle: '',
  icon: PIN_ICON,
  back: false,
  url: '/offices'
};

export const CLINICS_HEADER: IHeader = {
  title: 'RED DE CLÍNICAS',
  subTitle: '',
  icon: HOSPITAL_ICON,
  back: false,
  url: '/clinics'
};

export const CLINICS_SEARCH_HEADER: IHeader = {
  title: 'RED DE CLÍNICAS',
  subTitle: '',
  icon: HOSPITAL_ICON,
  back: false,
  url: '/clinics/search'
};

export const CLINIC_DETAIL_HEADER: IHeader = {
  title: '',
  subTitle: 'RED DE CLÍNICAS',
  icon: '',
  back: true,
  url: '/clinic/detail'
};

export const SINISTERS_IN_PROGRESS_HEADER: IHeader = {
  title: 'SINIESTROS',
  subTitle: 'MI MAPFRE',
  icon: '',
  back: true,
  url: '/sinistersInProgress'
};

export const SCTR_HEADER: IHeader = {
  title: 'SCTR',
  subTitle: '',
  icon: '',
  back: true,
  url: '/sctr'
};

export const SCTR_INSURANCE_HEADER: IHeader = {
  title: 'SEGURO COMPLEMENTARIO DE TRABAJO DE RIESGO',
  subTitle: 'SCTR',
  icon: HELMET_OUT_ICON,
  back: false,
  url: '/sctr/sctr-insurance'
};

export const SCTR_PERIOD_OPERATION_HEADER: IHeader = {
  title: 'DECLARACIÓN',
  subTitle: 'SCTR',
  icon: '',
  back: true,
  url: ''
};

export const SCTR_PERIOD_RECEIPTS_HEADER: IHeader = {
  title: 'PERIODOS',
  subTitle: 'SCTR',
  back: true
};

export const SCTR_PERIOD_PAYROLL_HEADER: IHeader = {
  title: 'PLANILLAS',
  subTitle: 'SCTR',
  back: true
};

export const SOAT_ELECT_HEADER: IHeader = {
  back: true,
  icon: '',
  subTitle: 'MIS PAGOS',
  title: 'RENOVACIÓN DE SOAT'
};

export const APPS_HEADER: IHeader = {
  title: 'APLICACIONES',
  subTitle: '',
  icon: APPS_ICON_IN,
  back: false,
  url: '/apps'
};

export const MAPFREDOLLARS_HEADER: IHeader = {
  title: 'MAPFRE DÓLARES',
  subTitle: 'Bienvenido al portal de clientes',
  back: true,
  url: '/mapfre-dollars'
};

export const SCHEDULE_APPOINTMENT_STEP_1_HEADER: IHeader = {
  title: 'AGENDAR CITA MÉDICA CENTRO MÉDICO MAPFRE',
  subTitle: 'SERVICIOS MAPFRE',
  back: true,
  url: '/digital-clinic/schedule/steps/1'
};

export const SCHEDULE_APPOINTMENT_STEP_2_HEADER: IHeader = {
  title: 'AGENDAR CITA MÉDICA CENTRO MÉDICO MAPFRE',
  subTitle: 'SERVICIOS MAPFRE',
  back: true,
  url: '/digital-clinic/schedule/steps/2'
};

export const SCHEDULE_APPOINTMENT_STEP_REVIEW_HEADER: IHeader = {
  title: 'AGENDAR CITA MÉDICA CENTRO MÉDICO MAPFRE',
  subTitle: 'SERVICIOS MAPFRE',
  back: true,
  url: '/digital-clinic/schedule/steps/3'
};

export const SCHEDULE_APPOINTMENT_STEP_SUCCESS_HEADER: IHeader = {
  title: 'AGENDAR CITA MÉDICA CENTRO MÉDICO MAPFRE',
  subTitle: 'SERVICIOS MAPFRE',
  back: true,
  url: '/digital-clinic/schedule/steps/4'
};

export const APPOINTMENT_DETAIL: IHeader = {
  title: 'DETALLE CITA MÉDICA',
  subTitle: 'SERVICIOS MAPFRE',
  back: true,
  url: '/digital-clinic/schedule-detail'
};

export const APPOINTMENT_DETAIL_STEP_2_HEADER: IHeader = {
  title: 'MODIFICACIÓN CITA MÉDICA',
  subTitle: 'SERVICIOS MAPFRE',
  back: true,
  url: '/digital-clinic/schedule-detail/2'
};

export const APPOINTMENT_DETAIL_STEP_REVIEW_HEADER: IHeader = {
  title: 'MODIFICACIÓN CITA MÉDICA',
  subTitle: 'SERVICIOS MAPFRE',
  back: true,
  url: '/digital-clinic/schedule-detail/3'
};

export const APPOINTMENT_DETAIL_STEP_SUCCESS_HEADER: IHeader = {
  title: 'MODIFICACIÓN CITA MÉDICA',
  subTitle: 'SERVICIOS MAPFRE',
  back: true,
  url: '/digital-clinic/schedule-detail/4'
};

export const APPOINTMENT_RESCHEDULE_STEP_2_HEADER: IHeader = {
  title: 'AGENDAR CITA MÉDICA',
  subTitle: 'SERVICIOS MAPFRE',
  back: true
};

export const APPOINTMENT_RESCHEDULE_STEP_REVIEW_HEADER: IHeader = {
  title: 'AGENDAR CITA MÉDICA',
  subTitle: 'SERVICIOS MAPFRE',
  back: true,
  url: '/mapfre-services/schedule-detail/4'
};

export const APPOINTMENT_RESCHEDULE_STEP_SUCCESS_HEADER: IHeader = {
  title: 'AGENDAR CITA MÉDICA CENTRO MÉDICO MAPFRE',
  subTitle: 'SERVICIOS MAPFRE',
  back: true,
  routeback: '/digital-clinic/services'
};

export const MEDIKTOR_HEADER: IHeader = {
  title: 'AUTOEVALUADOR',
  subTitle: '',
  icon: '',
  back: true,
  url: '/autoevaluador',
  routeback: '/digital-clinic/services'
};

export const BENEFICIOS_PPFM_HEADER: IHeader = {
  title: 'BENEFICIOS ADICIONALES',
  subTitle: 'PLAN DE PROTECCION FAMILIAR',
  icon: PPFM_OUT_ICON,
  back: false,
  url: '/ppfm'
};

export const SCHEDULE_APPOINTMENT_COVID_STEP_0_HEADER: IHeader = {
  title: 'AGENDAR PRUEBA COVID CENTRO MÉDICO MAPFRE',
  subTitle: 'SERVICIOS MAPFRE',
  back: true,
  url: '/digital-clinic/schedule/steps-covid/0'
};

export const SCHEDULE_APPOINTMENT_COVID_STEP_1_HEADER: IHeader = {
  title: 'AGENDAR PRUEBA COVID CENTRO MÉDICO MAPFRE',
  subTitle: 'SERVICIOS MAPFRE',
  back: true,
  url: '/digital-clinic/schedule/steps-covid/1'
};

export const APPOINTMENT_DETAIL_COVID: IHeader = {
  title: 'DETALLE PRUEBA COVID',
  subTitle: 'SERVICIOS MAPFRE',
  back: true,
  url: '/digital-clinic/schedule-detail'
};
