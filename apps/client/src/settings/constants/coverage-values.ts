import { ICoverageActions, ICoverageIcons } from '@mx/statemanagement/models/coverage.interface';
import {
  ACCIDENT_HOME_ICON,
  ACCIDENTAL_DEATH_ICON,
  AMBULANCE_ICON,
  CHILDREN_SCORT_ICON,
  DELAY_EXPENSES_REFUND_ICON,
  DISABILITY_ICON,
  FAILED_TRANSFER_ICON,
  HOSPITAL_MEDICINE_ICON,
  HOTEL_EXPENSES_CONVALESCENCE_ICON,
  HOTEL_EXPENSES_RELATIVE_ICON,
  LUGGAGE_LOST_COMPENSATION_ICON,
  MEDICAL_ASSIST_ICON,
  MEDICAL_ASSIST_PRE_ICON,
  MEDICINES_ICON,
  MESSAGE_TRANSMISSION_ICON,
  NATURAL_DEAD_ICON,
  ODONTOLOGY,
  OUTPATIENT_MEDICINE_ICON,
  PASSPORT_ICON,
  REFUND_TRAVEL_DELAYED_ICON,
  RELATIVE_TRANSFER_ICON,
  REPATRIATION_ICON,
  REPATRIATION_SANITARY_ICON,
  TRANSPORT_LOCATION_ICON,
  TRAVEL_CANCELLATION_ICON,
  TRAVEL_RATE_DIFFERENCE_ICON
} from './images-values';

// Icono por defecto
export const ICON_COVERAGE_DEFAULT = 'icon-mapfre_230_shield_out';

// Acciones - key se relaciona con el campo routeKey que devuelve el servicio
export const COVERAGES_ACTIONS: Array<ICoverageActions> = [
  {
    key: 'SRV_CHOFER_REEMPLAZO',
    route: ''
  },
  {
    key: 'SRV_AMBULANCIA',
    route: ''
  },
  {
    key: 'SRV_MEDICO_DOMICILIO',
    route: ''
  },
  {
    key: 'ACC_REPORTAR_ROBO_AUTOS',
    route: 'vehicles/stolen/reports'
  },
  {
    key: 'ACC_REPORTAR_ACCIDENTE_AUTOS',
    route: 'vehicles/accidents/reports'
  },
  {
    key: 'ACC_BUSCAR_CLINICA',
    route: 'health/clinic-search'
  },
  {
    key: 'ACC_REPORTAR_DANIO_HOGAR',
    route: 'household/hurt-and-steal/reports'
  },
  {
    key: 'ACC_TELEMEDICINA',
    route: ''
  },
  {
    key: 'ALL_SERVICES',
    route: 'mapfre-services/request'
  },
  {
    key: 'ACC_ASISTENCIA',
    route: ''
  }
];

// Vehiculos - key se relaciona con el campo iconKey que devuelve el servicio
export const COVERAGES_VEHICLES: Array<ICoverageIcons> = [
  {
    name: 'Daño Propio',
    key: 'COB_VEH_DANIO_PROPIO',
    icon: 'icon-mapfre_117_car'
  },
  {
    name: 'Accidente',
    key: 'COB_VEH_ACCIDENTE',
    icon: 'icon-mapfre_133_car_accident'
  },
  {
    name: 'Incendio',
    key: 'COB_VEH_INCENDIO',
    icon: 'icon-mapfre_139_fire'
  },
  {
    name: 'Robo o Hurto',
    key: 'COB_VEH_ROBO',
    icon: 'icon-mapfre_145_stole '
  },
  {
    name: 'Rotura de Lunas',
    key: 'COB_VEH_ROTURA_LUNA',
    icon: 'icon-mapfre_147_moons'
  },
  {
    name: 'Fenómenos Naturales',
    key: 'COB_VEH_FEN_NATURALES',
    icon: 'icon-mapfre_136_car_natural'
  },
  {
    name: 'Huelga, Conmoción Civil, Daño Malicioso, Vandalismo y terrorismo',
    key: 'COB_VEH_VANDALISMO',
    icon: 'icon-mapfre_138_strike'
  },
  {
    name: 'Ausencia de Control',
    key: 'COB_VEH_AUS_CONTROL',
    icon: 'icon-mapfre_146_imbalance'
  },
  {
    name: 'Pérdida Total',
    key: 'COB_VEH_PERDIDA_TOTAL',
    icon: 'icon-mapfre_111_steal'
  },
  {
    name: 'RC Terceros',
    key: 'COB_VEH_RT_ACCIDENTE',
    icon: 'icon-mapfre_108_person_car'
  },
  {
    name: 'RC Ocupantes',
    key: 'COB_VEH_OCUPANTES',
    icon: 'icon-mapfre_142_people'
  },
  {
    name: 'Accidentes Personales',
    key: 'COB_VEH_ACC_PERSONAL',
    icon: 'icon-mapfre_107_sinister'
  },
  {
    name: 'Muerte e invalidez permanente',
    key: 'COB_VEH_MUERTE_INVALIDEZ',
    icon: DISABILITY_ICON
  },
  {
    name: 'Curación',
    key: 'COB_VEH_CURACION',
    icon: 'icon-mapfre_107_sinister'
  },
  {
    name: 'Sepelio',
    key: 'COB_VEH_SEPELIO',
    icon: 'icon-mapfre_144_burial'
  },
  {
    name: 'Vehículo de reemplazo',
    key: 'COB_VEH_VEH_REEMPLAZO',
    icon: 'icon-mapfre_110_car_coverage'
  },
  {
    name: 'Chofer de reemplazo',
    key: 'COB_VEH_CHOFER_REEMPLAZO',
    icon: 'icon-mapfre_109_person_coverage'
  },
  {
    name: 'Asistencia a automóviles y personas',
    key: 'COB_VEH_ASISTENCIA',
    icon: 'icon-mapfre_108_person_car'
  },
  {
    name: 'Protección al automovilista',
    key: 'COB_VEH_REEMBOLSO',
    icon: 'icon-mapfre_105_money'
  },
  {
    name: 'Cirugia Estética',
    key: 'COB_VEH_CIRUGIA',
    icon: 'icon-mapfre_114_scissors'
  },
  {
    name: 'Amparo de Canasta Familiar',
    key: 'COB_VEH_CANASTA',
    icon: 'icon-mapfre_106_crate'
  },
  {
    name: 'Amparo de Renta Educativa',
    key: 'COB_VEH_RENT_EDU',
    icon: 'icon-mapfre_115_book'
  },
  {
    name: 'Cobertura Internacional',
    key: 'COB_VEH_COB_INTER',
    icon: 'icon-mapfre_140_world'
  },
  {
    name: 'Choque y robo parcial',
    key: 'COB_VEH_CHOQUE_ROBO_PARCIAL',
    icon: 'icon-mapfre_134_car_hurt'
  },
  {
    name: 'Robo de equipos audiovisuales',
    key: 'COB_VEH_ROBO_AUDIOVIS',
    icon: 'icon-mapfre_143_things'
  },
  {
    name: 'Daño a terceros por ausencia de control',
    key: 'COB_VEH_RT_AUS_CONTROL',
    icon: 'icon-mapfre_135_car_shock'
  },
  {
    name: 'Grúa y Auxilio Mecánico',
    key: 'COB_VEH_GRUA_AUX_MEC',
    icon: 'icon-mapfre_137_car_crane '
  }
];

// Salud
export const COVERAGES_HEALTH: Array<ICoverageIcons> = [
  {
    name: 'Consultas médicas',
    key: 'COB_SAL_CONS_MED',
    icon: 'icon-mapfre_168_medical_consultation'
  },
  {
    name: 'Suma Asegurada',
    key: 'COB_SAL_SUMA_ASEG',
    icon: 'icon-mapfre_179_money'
  },
  {
    name: 'Cobertura por cáncer',
    key: 'COB_SAL_COB_CANCER',
    icon: 'icon-mapfre_183_death'
  },
  {
    name: 'Emergencia accidental',
    key: 'COB_SAL_EMERG_ACCID',
    icon: 'icon-mapfre_169_accidental_emergency'
  },
  {
    name: 'Emergencia médica',
    key: 'COB_SAL_EMERG_MED',
    icon: 'icon-mapfre_170_medical_emergency'
  },
  {
    name: 'Hospitalización',
    key: 'COB_SAL_HOSPITAL',
    icon: 'icon-mapfre_173_hospitalization'
  },
  {
    name: 'Reconstrucción mamaria por mastectomía',
    key: 'COB_SAL_RECONS_MAM',
    icon: 'icon-mapfre_164_breast_reconstruction'
  },
  {
    name: 'Terapias biológicas',
    key: 'COB_SAL_TER_BIO',
    icon: 'icon-mapfre_180_injection'
  },
  {
    name: 'Médico a domicilio',
    key: 'COB_SAL_MED_DOM',
    icon: 'icon-mapfre_175_home_doctor'
  },
  {
    name: 'Servicio de Ambulancia a domicilio',
    key: 'COB_SAL_SERV_AMB_DOM',
    icon: 'icon-mapfre_178_ambulance_service'
  },
  {
    name: 'Sepelio',
    key: 'COB_SAL_SEPEL',
    icon: 'icon-mapfre_144_burial'
  },
  {
    name: 'Chequeo Preventivo Anual',
    key: 'COB_SAL_CHEQ_PREV_AN',
    icon: 'icon-mapfre_167_preventive_check'
  },
  {
    name: 'Enfermedades congénitas de recién nacidos',
    key: 'COB_SAL_ENFER_CONG_RN',
    icon: 'icon-mapfre_171_genetic_diseases'
  },
  {
    name: 'Enfermedades congénitas no conocidas',
    key: 'COB_SAL_ENFER_CONG_NC',
    icon: 'icon-mapfre_163_genetic'
  },
  {
    name: 'Prótesis quirúrgica',
    key: 'COB_SAL_PROT_QUIR',
    icon: 'icon-mapfre_177_surgical_prosthesis'
  },
  {
    name: 'Epidemias',
    key: 'COB_SAL_EPIDEMIAS',
    icon: 'icon-mapfre_172_epeymy'
  },
  {
    name: 'Transplantes de órganos',
    key: 'COB_SAL_TRANS_ORG',
    icon: 'icon-mapfre_181_organ_transplant'
  },
  {
    name: 'Asistencia en Viaje',
    key: 'COB_SAL_ASIS_VIAJE',
    icon: 'icon-COB_SAL_ASIS_VIAJE'
  },
  {
    name: 'Maternidad',
    key: 'COB_SAL_MATERNIDAD',
    icon: 'icon-mapfre_174_maternity'
  },
  {
    name: 'Parto natural',
    key: 'COB_SAL_PARTO_NAT',
    icon: 'icon-mapfre_176_natural_birth'
  },
  {
    name: 'Cesárea',
    key: 'COB_SAL_CESAREA',
    icon: 'icon-mapfre_166_caesarean'
  }
];

// coberturas para red de clinicas en mapa
export const COB_SAL_CONS_MED = 'COB_SAL_CONS_MED';
export const COB_SAL_HOSPITAL = 'COB_SAL_HOSPITAL';
export const COB_SAL_PARTO_NAT = 'COB_SAL_PARTO_NAT';
export const COB_SAL_CESAREA = 'COB_SAL_CESAREA';

// Hogar
export const COVERAGES_HOUSEHOLD: Array<ICoverageIcons> = [
  {
    key: 'COB_HOGAR_DANIO_MALI',
    name: 'Daño malicioso, vandalismo y terrorismo',
    icon: 'icon-mapfre_187_bomb'
  },
  {
    key: 'COB_HOGAR_DANIO_EQUIPOS',
    name: 'Daños o averías internas en equipos mecánicos, eléctricos o electrónicos',
    icon: 'icon-mapfre_188_damaged_monitor'
  },
  {
    key: 'COB_HOGAR_DANIO_AGUA',
    name: 'Daños por agua',
    icon: 'icon-mapfre_189_damaged_pipe'
  },
  {
    key: 'COB_HOGAR_DESHON_PER_DOM',
    name: 'Deshonestidad de personal doméstico',
    icon: 'icon-mapfre_190_dishonesty'
  },
  {
    key: 'COB_HOGAR_GASTOS_CURA_SERV',
    name: 'Gastos de curación para servidores del hogar',
    icon: 'icon-mapfre_191_injured'
  },
  {
    key: 'COB_HOGAR_HUELGA_CONM_CIV',
    name: 'Huelga y conmoción civil',
    icon: 'icon-mapfre_138_strike'
  },
  {
    key: 'COB_HOGAR_HURTO_DES_MIST',
    name: 'Hurto o desaparición misteriosa de contenidos',
    icon: 'icon-mapfre_192_mysterious_disappearance'
  },
  {
    key: 'COB_HOGAR_INCEN_EXPLO',
    name: 'Incendio y/o explosión',
    icon: 'icon-mapfre_155_fire'
  },
  {
    key: 'COB_HOGAR_LLUVIA_INUND',
    name: 'Lluvia e inundación',
    icon: 'icon-mapfre_161_seaquake'
  },
  {
    key: 'COB_HOGAR_MUERTE_ACCID',
    name: 'Muerte accidental de titular o cónyuge',
    icon: 'icon-mapfre_184_accidental_death'
  },
  {
    key: 'COB_HOGAR_REFRIGERACION',
    name: 'Refrigeración (solo contenido)',
    icon: 'icon-mapfre_196_refrigeration'
  },
  {
    key: 'COB_HOGAR_RESP_CIV_FAM',
    name: 'Responsabilidad Civil Familiar por daños personales y/o terceros',
    icon: 'icon-mapfre_194_family'
  },
  {
    key: 'COB_HOGAR_RESP_COV_SERV',
    name: 'Responsabilidad Civil frente a servidores del Hogar',
    icon: 'icon-mapfre_195_workers'
  },
  {
    key: 'COB_HOGAR_ROBO_HURTO',
    name: 'Robo y/o hurto agravado',
    icon: 'icon-mapfre_197_aggravated_robbery'
  },
  {
    key: 'COB_HOGAR_TERREMOTO',
    name: 'Terremoto y otros riesgos de la naturaleza',
    icon: 'icon-mapfre_160_earthquake'
  }
];

// Vida y Ahorro
export const COVERAGES_LIFE: Array<ICoverageIcons> = [
  {
    key: 'COB_VIDA_MUERT_NAT',
    name: 'Muerte natural',
    icon: NATURAL_DEAD_ICON
  },
  {
    key: 'COB_VIDA_SOBREVIVENCIA',
    name: 'Sobrevivencia',
    icon: 'icon-mapfre_186_survival'
  },
  {
    key: 'COB_VIDA_INVAL_PERM_TOT',
    name: 'Invalidez permanente, total y definitiva',
    icon: 'icon-mapfre_141_disability'
  },
  {
    key: 'COB_VIDA_MUERT_ACCID',
    name: 'Muerte accidental',
    icon: 'icon-mapfre_184_accidental_death'
  },
  {
    key: 'COB_VIDA_ANTIC_CAP_ENF',
    name: 'Anticipo de capital por enfermedad',
    icon: 'icon-mapfre_182_disease'
  },
  {
    key: 'COB_VIDA_COBER_ONCO',
    name: 'Cobertura oncológica',
    icon: 'icon-mapfre_183_death'
  }
];

// Viajes
export const COVERAGES_TRAVEL: Array<ICoverageIcons> = [
  {
    key: 'COB_VIAJE_ASIST_MED_ACCID',
    name: 'Asistencia médica por accidente',
    icon: AMBULANCE_ICON
  },
  {
    key: 'COB_VIAJE_ASIST_MED_ENFER',
    name: 'Asistencia médica por enfermedad',
    icon: MEDICAL_ASSIST_ICON
  },
  {
    key: 'COB_VIAJE_ASIST_MED_ENFER_PRE',
    name: 'Asistencia médica por enfermedad Pre-existente',
    icon: MEDICAL_ASSIST_PRE_ICON
  },
  {
    key: 'COB_VIAJE_INVAL_PERM_TOT',
    name: 'Invalidez permanente, total y definitiva',
    icon: DISABILITY_ICON
  },
  {
    key: 'COB_VIAJE_MED_AMBULA',
    name: 'Medicamentos Ambulatorios',
    icon: OUTPATIENT_MEDICINE_ICON
  },
  {
    key: 'COB_VIAJE_MED_HOSPIT',
    name: 'Medicamentos hospitalarios',
    icon: HOSPITAL_MEDICINE_ICON
  },
  {
    key: 'COB_VIAJE_MUERT_ACCID',
    name: 'Muerte accidental',
    icon: ACCIDENTAL_DEATH_ICON
  },
  {
    key: 'COB_VIAJE_GAST_HOT_CONVA',
    name: 'Gastos Hotel por Convalencia Máximo global',
    icon: HOTEL_EXPENSES_CONVALESCENCE_ICON
  },
  {
    key: 'COB_VIAJE_GAST_HOT_FAMIL',
    name: 'Gastos de Hotel Familiar Acompañante',
    icon: HOTEL_EXPENSES_RELATIVE_ICON
  },
  {
    key: 'COB_VIAJE_TRASL_FAMIL',
    name: 'Traslados de un familiar',
    icon: RELATIVE_TRANSFER_ICON
  },
  {
    key: 'COB_VIAJE_TRANS_SANI',
    name: 'Transporte o repatriación Sanitaria en Caso de lesiones o enfermedad',
    icon: REPATRIATION_SANITARY_ICON
  },
  {
    key: 'COB_VIAJE_REPAT_REST',
    name: 'Repatriación de restos mortales',
    icon: REPATRIATION_ICON
  },
  {
    key: 'COB_VIAJE_ODONT_URGE',
    name: 'Odontología de Urgencia',
    icon: ODONTOLOGY
  },
  {
    key: 'COB_VIAJE_ACOMP_MENOR',
    name: 'Acompañamiento de menores',
    icon: CHILDREN_SCORT_ICON
  },
  {
    key: 'COB_VIAJE_TRANSM_MSJ',
    name: 'Transmisión de mensajes urgentes',
    icon: MESSAGE_TRANSMISSION_ICON
  },
  {
    key: 'COB_VIAJE_REEMB_GAST',
    name: 'Reembolso Gastos por vuelo demorado o cancelado',
    icon: REFUND_TRAVEL_DELAYED_ICON
  },
  {
    key: 'COB_VIAJE_LOCAL_EQUIP',
    name: 'Localización y transporte de equipaje y efectos personales',
    icon: TRANSPORT_LOCATION_ICON
  },
  {
    key: 'COB_VIAJE_COMP_PERD_EQUI',
    name: 'Compensación por pérdida de equipaje en vuelo regular',
    icon: LUGGAGE_LOST_COMPENSATION_ICON
  },
  {
    key: 'COB_VIAJE_REEMB_ENTR_EQUIP',
    name: 'Reembolso gastos por demora en la entrega de equipaje',
    icon: DELAY_EXPENSES_REFUND_ICON
  },
  {
    key: 'COB_VIAJE_ENV_MEDICAM',
    name: 'Envío de medicamentos fuera del Perú',
    icon: MEDICINES_ICON
  },
  {
    key: 'COB_VIAJE_INTERR_FALLEC',
    name: 'Interrupción de viaje por fallecimiento de familiar',
    icon: FAILED_TRANSFER_ICON
  },
  {
    key: 'COB_VIAJE_REGR_ANTICIP',
    name: 'Regreso anticipado por siniestro grave en el domicilio',
    icon: ACCIDENT_HOME_ICON
  },
  {
    key: 'COB_VIAJE_DIF_TARIFA',
    name: 'Diferencia de tarifado viaje de regreso',
    icon: TRAVEL_RATE_DIFFERENCE_ICON
  },
  {
    key: 'COB_VIAJE_ASIST_ROB_DOC',
    name: 'Asistencia en caso de robos o extravío de documentos',
    icon: PASSPORT_ICON
  },
  {
    key: 'COB_VIAJE_COMP_CANCEL',
    name: 'Compensación por cancelación de viaje',
    icon: TRAVEL_CANCELLATION_ICON
  }
];

export const COVERAGES_DEATH: Array<ICoverageIcons> = [
  {
    key: 'COB_DECESOS_DESAMP_FAMI_SUB',
    name: 'Desamparo Familiar Subito',
    icon: 'icon-COB_DECESOS_DESAMP_FAMI_SUB'
  },
  {
    key: 'COB_DECESOS_INDEM_MUERT_NAT',
    name: 'Indemnización por muerte natural',
    icon: 'icon-COB_DECESOS_INDEM_MUERT_NAT'
  },
  {
    key: 'COB_DECESOS_SERV_SEPELIO',
    name: 'Servicio de Sepelio',
    icon: 'icon-COB_DECESOS_SERV_SEPELIO'
  },
  {
    key: 'COB_DECESOS_REEM_TRAS_REST',
    name: 'Reembolso de gastos por traslado de restos',
    icon: 'icon-COB_DECESOS_REEM_TRAS_REST'
  },
  {
    key: 'COB_DECESOS_INDEM_MUERT_ACC',
    name: 'Indemnización por muerte accidental',
    icon: 'icon-COB_DECESOS_INDEM_MUERT_ACC'
  }
];

// DEFINIR EL ORIGEN DE CONSULTA DE COBERTURAS
export enum QUERY_SOURCE {
  CLINIC_SEARCH = 'BUSCADOR_CLINICAS',
  POLICY_DETAIL = 'DETALLE_POLIZA'
  // tslint:disable-next-line: max-file-line-count
}
