export const TOOLTIP = {
  top: 10,
  width: 2,
  height: 15,
  conditional: {
    left: 30,
    width: 75,
    top: 0
  }
};

export const DROPDOWN = {
  top: 10,
  width: 2,
  height: 15,
  conditional: {
    left: 30,
    width: 75,
    top: 0,
    height: 300
  }
};
