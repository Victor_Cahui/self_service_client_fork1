export const TYPE_DOCUMENT_DNI = 'DNI';
export const TYPE_DOCUMENT_RUC = 'RUC';
export const TYPE_DOCUMENT_PEX = 'PEX';
export const TYPE_DOCUMENT_CEX = 'CEX';
export const TYPE_DOCUMENT_CIP = 'CIP';
export const TYPE_DATA_NUMERIC = 'NUMERICO';
export const TYPE_DATA_ALPHANUMERIC = 'ALFANUMERICO';
export const TYPE_DOCUMENT_MAX_LENGTH = '10';
