import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { DeclareSinisterHomeService, IStepsSinester } from '@mx/services/home/declare-sinister-home.service';

@Injectable()
export class DeclareSinesterStepThreeGuard implements CanActivate {
  constructor(
    private readonly declareSinesterHomeService: DeclareSinisterHomeService,
    private readonly router: Router
  ) {}

  canActivate(): boolean {
    const steps: IStepsSinester = this.declareSinesterHomeService.getSteeps();
    if (!steps.step1 || !steps.step2) {
      this.router.navigate([`/household/hurt-and-steal/declare-sinister-home/step/2`]);
    }

    return steps.step1 && steps.step2;
  }
}
