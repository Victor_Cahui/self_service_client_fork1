import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { PolicyUtil } from '@mx/components/shared/utils/policy.util';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';

@Injectable()
export class SctrInsuranceCoverageRedirectionGuard implements CanActivate {
  constructor(private readonly router: Router, private readonly policiesInfoService: PoliciesInfoService) {}

  private fnCoverageRedirection(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const policyNumber = next.params['policyNumber'];
    const clientPolicy = this.policiesInfoService.getClientSctrPolicy(policyNumber);
    const isSctrDoubleEmission = PolicyUtil.isSctrDoubleEmission(clientPolicy.numeroPoliza);
    const isSctrPension = PolicyUtil.isSctrPension(clientPolicy.tipoPoliza, clientPolicy.codCia, clientPolicy.codRamo);
    const redirectTo = isSctrDoubleEmission ? 'assistance' : isSctrPension ? 'disability' : 'assistance';

    this.router.navigate([`sctr/sctr-insurance/${policyNumber}/coverages/${redirectTo}`]);

    return true;
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.fnCoverageRedirection(next, state);
  }
}
