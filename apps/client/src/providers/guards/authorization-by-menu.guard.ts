import { Injectable } from '@angular/core';
import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  RouterStateSnapshot
} from '@angular/router';
import { MenuService } from '@mx/services/menu/menu.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationByMenuGuard implements CanActivate, CanActivateChild {
  constructor(private readonly menuService: MenuService, private readonly activatedRoute: ActivatedRoute) {}

  private fnAuthorizationByMenu(
    nextState: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    isChild?: boolean
  ): boolean {
    const isHistorical =
      nextState.queryParams['historical'] === 'true' ||
      this.activatedRoute.snapshot.queryParams['historical'] === 'true';
    if (isHistorical) {
      return true;
    }

    const splitCurrentState = currentState.url.split('/');
    const routesByMenu = this.menuService.getRoutesByMenu();
    const authorizationByMenu = routesByMenu.find(itemRoute => {
      const splitItemRoute = itemRoute.split('/');

      return isChild
        ? splitCurrentState[1] === splitItemRoute[1] && splitCurrentState[2] === splitItemRoute[2]
        : splitCurrentState[1] === splitItemRoute[1];
    });

    return !!authorizationByMenu;
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.fnAuthorizationByMenu(next, state);
  }

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.fnAuthorizationByMenu(next, state, true);
  }
}
