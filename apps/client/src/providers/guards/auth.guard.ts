import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { VerifySessionService } from '@mx/core/shared/common/services/verify-session.service';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { KEY_USER_TOKEN } from '@mx/settings/auth/auth-values';
import { LOCAL_STORAGE_KEYS } from '@mx/settings/constants/key-values';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private readonly _LocalStorageService: LocalStorageService,
    private readonly router: Router,
    private readonly _VerifySessionService: VerifySessionService
  ) {}

  canActivate(): boolean {
    if (this._VerifySessionService.verifyTimeToken()) {
      return true;
    }
    if (!this._VerifySessionService.verifyTimeToken()) {
      this._LocalStorageService.removeItem(KEY_USER_TOKEN);
      this.router.navigate(['/login']);

      return false;
    }
    if (this._LocalStorageService.getData(LOCAL_STORAGE_KEYS.GROUP_TYPE)) {
      this.router.navigate(['/login-type']);

      return false;
    }
  }
}
