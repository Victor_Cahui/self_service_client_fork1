import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '@mx/core/shared/common/services/auth.service';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { LOCAL_STORAGE_KEYS } from '@mx/settings/constants/key-values';

@Injectable()
export class WelcomeGuard implements CanActivate {
  constructor(
    private readonly _AuthService: AuthService,
    private readonly router: Router,
    private readonly localStorage: LocalStorageService
  ) {}

  canActivate(): boolean {
    if (this._AuthService.getAuthorizationToken()) {
      this.router.navigate(['/home']);

      return false;
    }

    if (!this._AuthService.getAuthorizationToken() && this.localStorage.getData(LOCAL_STORAGE_KEYS.USER)) {
      return true;
    }

    if (this.localStorage.getData(LOCAL_STORAGE_KEYS.GROUP_TYPE)) {
      this.router.navigate(['/login-type']);

      return false;
    }

    if (!this.localStorage.getData(LOCAL_STORAGE_KEYS.USER)) {
      this.router.navigate(['/login']);

      return false;
    }

    return true;
  }
}
