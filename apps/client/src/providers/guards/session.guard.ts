import { Injectable } from '@angular/core';
import { CanActivateChild, Router } from '@angular/router';
import { VerifySessionService } from '@mx/core/shared/common/services/verify-session.service';

@Injectable()
export class SessionGuard implements CanActivateChild {
  constructor(private readonly verifySessionService: VerifySessionService, private readonly router: Router) {}

  canActivateChild(): boolean {
    if (this.verifySessionService.verifyTimeToken()) {
      return true;
    }
    this.verifySessionService.onSessionExpired.next();
    this.router.navigate([this.router.url]);

    return false;
  }
}
