import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { PaymentService } from '@mx/services/payment.service';

@Injectable()
export class VehicleBuyConfirmGuard implements CanActivate {
  constructor(private readonly paymentService: PaymentService) {}

  canActivate(): boolean {
    const quote = this.paymentService.getCurrentQuote();

    return !!(quote && quote.confirm);
  }
}
