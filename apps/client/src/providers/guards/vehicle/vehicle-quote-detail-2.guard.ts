import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { PoliciesQuoteService } from '@mx/services/policy/policy-quote.service';

@Injectable()
export class VehicleQuoteDetail2Guard implements CanActivate {
  constructor(private readonly router: Router, private readonly policiesQuoteService: PoliciesQuoteService) {}

  canActivate(): boolean {
    const vehicleData = this.policiesQuoteService.getVehicleData();
    const success =
      vehicleData &&
      vehicleData.placa &&
      vehicleData.codigoMarca &&
      vehicleData.codigoModelo &&
      vehicleData.codigoTipoVehiculo &&
      vehicleData.valorSugerido &&
      vehicleData.codigoUso &&
      vehicleData.codigoCiudad;

    if (!success) {
      this.router.navigate([this.router.url]);
    }

    return !!success;
  }
}
