import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { PoliciesQuoteService } from '@mx/services/policy/policy-quote.service';

@Injectable()
export class VehicleQuoteDetailGuard implements CanActivate {
  constructor(private readonly router: Router, private readonly policiesQuoteService: PoliciesQuoteService) {}

  canActivate(): boolean {
    const vehicleData = this.policiesQuoteService.getVehicleData();

    if (!vehicleData) {
      this.router.navigate([this.router.url]);
    }

    return !!vehicleData;
  }
}
