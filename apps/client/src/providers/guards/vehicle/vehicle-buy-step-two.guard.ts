import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { PoliciesQuoteService } from '@mx/services/policy/policy-quote.service';

@Injectable()
export class VehicleBuyStepTwoGuard implements CanActivate {
  constructor(private readonly policiesQuoteService: PoliciesQuoteService, private readonly router: Router) {}

  canActivate(): boolean {
    const hasVehicle = this.policiesQuoteService.isVehicleDataCompleted();

    if (!hasVehicle) {
      this.router.navigate(['vehicles/quote/buy/step1']);
    }

    return hasVehicle;
  }
}
