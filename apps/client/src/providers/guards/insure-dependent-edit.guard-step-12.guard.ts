import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { InsuredDependentEditService } from '@mx/services/health/insured-dependent-edit.service';

@Injectable()
export class InsureDependentEditGuardStep12 implements CanActivate {
  constructor(private readonly service: InsuredDependentEditService) {}

  canActivate(): boolean {
    const success = this.service.getSuccess();

    return !success;
  }
}
