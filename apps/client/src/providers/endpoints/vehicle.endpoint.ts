// tslint:disable: max-line-length
// tslint:disable: max-line-length
import { environment } from '@mx/environments/environment';

export class VehicleEndpoint {
  public static clientVehicle = `${environment.API_URL_CLIENT}vehiculo/{tipoDocumento}/{documento}/{placa}`;
  public static vehiclesList = `${environment.API_URL_CLIENT}vehiculos/{codigoApp}/{numeroPoliza}`;
  public static updateVehiclePlate = `${environment.API_URL_CLIENT}vehiculos/placa/{codigoApp}/{numeroPoliza}`;
  public static getVehicle = `${environment.API_URL_CLIENT}vehiculo/{codigoApp}/{numeroPoliza}/{chasis}/{nroMotor}`;
  public static getStolens = `${environment.API_URL_CLIENT}vehiculos/robos/{codigoApp}`;
  public static addLetter = `${environment.API_URL_CLIENT}vehiculos/adjuntarCartaNoAdeudo/{codigoApp}`;
}

// Accesorios
export class VehicleAccesoryEndpoint {
  public static getAccesoriesType = `${environment.API_URL_CLIENT}vehiculos/accesorios/tiposAccesorios`;
  public static addAccesory = `${environment.API_URL_CLIENT}vehiculos/accesorio/{codigoApp}/{numeroPoliza}/{chasis}/{nroMotor}`;
  public static editOrDeleteAccesory = `${environment.API_URL_CLIENT}vehiculos/accesorio/{codigoApp}/
  {numeroPoliza}/{chasis}/{nroMotor}/{accesorioId}`;
}

// Accidents
export class VehicleAccidentEndpoint {
  public static getAssists = `${environment.API_URL_CLIENT}vehiculos/accidentes/{codigoApp}`;
  public static getWorkshopsPDF = `${environment.API_URL_CLIENT}vehiculos/asistencias/talleresPreferentes/{codigoApp}`;
}

// Cotizacion
export class VehicleQuoteEndpoint {
  public static getBrands = `${environment.API_URL_CLIENT}vehiculos/cotizacion/marcas/{codigoApp}`;
  public static getModels = `${environment.API_URL_CLIENT}vehiculos/cotizacion/modelos/{codigoApp}/{marca}`;
  public static getType = `${environment.API_URL_CLIENT}vehiculos/cotizacion/tiposVehiculo/{codigoApp}/{marca}/{modelo}`;
  public static getValues = `${environment.API_URL_CLIENT}vehiculos/cotizacion/valorSugerido/{codigoApp}/{marca}/{modelo}/{anio}/{tipoVehiculo}`;
  public static getUse = `${environment.API_URL_CLIENT}vehiculos/cotizacion/usosVehiculo/{codigoApp}/{marca}/{modelo}/{tipoVehiculo}`;
  public static getCity = `${environment.API_URL_CLIENT}vehiculos/cotizacion/ciudades/{codigoApp}`;
  public static getGPSRequired = `${environment.API_URL_CLIENT}vehiculos/cotizacion/validarGPS/{codigoApp}/{marca}/{modelo}/{anio}`;
  public static verifyPlate = `${environment.API_URL_CLIENT}vehiculos/cotizacion/buscarPorPlaca/{codigoApp}/{placa}`;
  // Comparación de Pólizas
  public static PoliciesCompareHeader = `${environment.API_URL_CLIENT}vehiculos/cotizacion/productos/{codigoApp}`;
  public static PoliciesCompareDetails = `${environment.API_URL_CLIENT}vehiculos/cotizacion/coberturas/{codigoApp}`;
}

// Compra
export class VehicleBuyEndpoint {
  public static SearchChassisMotor = `${environment.API_URL_CLIENT}vehiculos/compra/buscarChasisMotor/{codigoApp}/{placa}`;
  public static VerifyPolicyChassisMotor = `${environment.API_URL_CLIENT}vehiculos/compra/validarPoliza/{codigoApp}/{placa}/{chasis}/{motor}`;
  public static GetColors = `${environment.API_URL_CLIENT}vehiculos/compra/colores/{codigoApp}`;
  public static VerifyInspection = `${environment.API_URL_CLIENT}vehiculos/compra/determinarInspeccion/{codigoApp}`;
  public static PersonalDataClause = `${environment.API_URL_CLIENT}vehiculos/compra/clausulaDatosPersonales/{codigoApp}`;
  public static InspectionConditions = `${environment.API_URL_CLIENT}vehiculos/compra/condicionesInspeccion/{codigoApp}`;
  public static RangeHoursInspection = `${environment.API_URL_CLIENT}vehiculos/compra/rangoHorasInspeccion/{codigoApp}`;
  public static QuoteData = `${environment.API_URL_CLIENT}vehiculos/compra/cotizacion/{codigoApp}`;
  public static Indication = `${environment.API_URL_CLIENT}vehiculos/compra/indicacion/PolizaElectronica/{codigoApp}`;
  public static Conditions = `${environment.API_URL_CLIENT}vehiculos/compra/obtenerCondicionesProducto/{codigoApp}/{codigoModalidad}`;
  public static EmitPayment = `${environment.API_URL_CLIENT}vehiculos/compra/emision/{codigoApp}`;
  public static InspectionRequest = `${environment.API_URL_CLIENT}vehiculos/compra/generarSolicitudInspeccion/{codigoApp}`;
}

// Asistencias
export class VehicleAssistanceEndpoint {
  public static AccidentDetail = `${environment.API_URL_CLIENT}vehiculos/detallesAccidente/{codigoApp}/{numeroAsistencia}`;
  public static StolenDetail = `${environment.API_URL_CLIENT}vehiculos/detallesRobo/{codigoApp}/{numeroAsistencia}`;
  public static RateWorkshop = `${environment.API_URL_CLIENT}vehiculos/calificarTaller`;
}
