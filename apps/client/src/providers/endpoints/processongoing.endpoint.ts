import { environment } from '@mx/environments/environment';

export class ProcessOngoingEndpoint {
  public static PROCESS_ONGOING = `${environment.API_URL_CLIENT}tramites/buscarPorCliente/{codigoApp}`;
}
