import { appConstants } from '@mx/core/shared/providers/constants';
import { environment } from '@mx/environments/environment';

export class PaymentEndpoint {
  public static PaymentSearchByClient = `${environment.API_URL_CLIENT}pagos/buscarPorCliente/{codigoApp}`;
  public static PaymentType = `${appConstants.apis.autoservicios}/pagos/tipos/{codigoApp}`;
  public static PaymentTypePerUser = `${environment.API_URL_CLIENT}pagos/tipos/{codigoApp}`;
  public static ReceiptsSearchByClient = `${environment.API_URL_CLIENT}recibos/{codigoApp}`;
  public static DownLoadReceiptPDF = `${environment.API_URL_CLIENT}recibo/{codigoApp}/{numeroRecibo}`;
  public static SendReceiptPDF = `${environment.API_URL_CLIENT}recibo/correo/{codigoApp}/{numeroRecibo}`;
  public static ReceiptPay = `${environment.API_URL_CLIENT}recibo/pago/{codigoApp}`;
  // Cotizacion y Emision de SOAT
  public static SoatQuote = `${environment.API_URL_CLIENT}soat/cotizacion/{codigoApp}/{numeroPoliza}`;
  public static SoatIssuance = `${environment.API_URL_CLIENT}soat/emision/{codigoApp}`;
  // Configuración de Pagos
  public static PaymentConfiguration = `${environment.API_URL_CLIENT}pagos/configuraciones/{codigoApp}`;
  public static PaymentConfigurationRenewal = `${environment.API_URL_CLIENT}pagos/renovacion/{codigoApp}`;
  public static PaymentConfigurationRefinancing = `${environment.API_URL_CLIENT}pagos/refinanciamiento/{codigoApp}/{numeroPoliza}`;
  public static PaymentConfigurationRefinancingRequest = `
    ${environment.API_URL_CLIENT}pagos/refinanciamiento/solicitar/{codigoApp}/{numeroPoliza}`;
}
