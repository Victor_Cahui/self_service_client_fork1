import { environment } from '@mx/environments/environment';

export class SinisterEndpoint {
  public static SINISTERS = `${environment.API_URL_CLIENT}siniestros/buscarPorCliente/{codigoApp}`;
}
