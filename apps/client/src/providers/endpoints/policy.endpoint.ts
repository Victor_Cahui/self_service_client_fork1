// tslint:disable: max-line-length
import { environment } from '@mx/environments/environment';

export class PolicyEndpoint {
  public static PoliciesSearchByClient = `${environment.API_URL_CLIENT}polizas/buscarPorCliente/{codigoApp}`;
  public static PoliciesAndProductsByClient = `${environment.API_URL_CLIENT}polizas/tipos/{codigoApp}`;
  public static PolicySearchByNumber = `${environment.API_URL_CLIENT}polizas/{tipoDocumento}/{documento}/{numeroPoliza}`;
  public static SeePolicePdf = `${environment.API_URL_CLIENT}poliza/`;
  public static Houses = `${environment.API_URL_CLIENT}poliza/hogares/{codigoApp}/{numeroPoliza}`;
  public static Insured = `${environment.API_URL_CLIENT}poliza/asegurados/{codigoApp}/{numeroPoliza}`;
  public static FeePlan = `${environment.API_URL_CLIENT}poliza/cuotas/{codigoApp}/{numeroPoliza}`;
  public static EditInsuredData = `${environment.API_URL_CLIENT}poliza/asegurados/datosPersonales`;
  public static Coverages = `${environment.API_URL_CLIENT}poliza/coberturas/{codigoApp}/{numeroPoliza}`;
  public static SctrSearchByClient = `${environment.API_URL_CLIENT}polizas/sctr/buscarPorCliente/{codigoApp}`;
  public static SctrConstancyPdf = `${environment.API_URL_CLIENT}sctr/constancias/{numeroPoliza}`;
  public static GetDeductiblesByPolicy = `${environment.API_URL_CLIENT}poliza/deducibles/vehiculos/{codigoApp}/{numeroPoliza}`;
  public static SeeExclusionsPdfByPolicy = `${environment.API_URL_CLIENT}poliza/condicionado/{codigoApp}/{numeroPoliza}`;
  public static PolicyTransfer = `${environment.API_URL_CLIENT}poliza/solicitudTraspaso/{codigoApp}/{numeroPoliza}`;
  public static FoilAttachedByPolicy = `${environment.API_URL_CLIENT}poliza/hojaAnexa/{codigoApp}/{numeroPoliza}`;
  public static CoveragesByVehicle = `${environment.API_URL_CLIENT}poliza/coberturas/{codigoApp}/{numeroPoliza}/{chasis}/{nroMotor}`;
  public static AgentByPolicy = `${environment.API_URL_CLIENT}poliza/agente/{codigoApp}/{numeroPoliza}`;

  // Salud
  public static ExclusionsByInsuredHealth = `${environment.API_URL_CLIENT}salud/coberturas/exclusiones/{codigoApp}/{numeroPoliza}/{tipoDocumentoAsegurado}/{documentoAsegurado}`;
  public static ObservationsByInsuredHealth = `${environment.API_URL_CLIENT}salud/coberturas/observaciones/{codigoApp}/{numeroPoliza}/{tipoDocumentoAsegurado}/{documentoAsegurado}`;
  public static CareNetworks = `${environment.API_URL_CLIENT}salud/coberturas/redesAtencion/{codigoApp}/{numeroPoliza}/{tipoCobertura}`;

  // Cambio de Modalidad
  public static ChangeModality = `${environment.API_URL_CLIENT}poliza/cambiarmodalidad/{codigoApp}/{numeroPoliza}`;
}
