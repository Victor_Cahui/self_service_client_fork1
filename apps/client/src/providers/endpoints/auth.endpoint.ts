import { environment } from '@mx/environments/environment';
export class AuthEndpoint {
  // Login
  // TODO: Cambiar por ruta correcta, depende de @juan_castro
  public static login = `${environment.API_URL_AUTH}login`;
  public static users = `${environment.API_URL_AUTH}person/getusers`;
  public static loginGroupType = `${environment.API_URL_AUTH}claims/GenerateClaimsByGroupType`;
  public static claims = `${environment.API_URL_AUTH}claims/list`;

  // Habilitar y recuperar usuario
  public static enableUser = `${environment.API_URL_AUTH}seguridad/InsertPersonClientUserAuto`;
  public static recoverUser = `${environment.API_URL_AUTH}seguridad/acceso/SendEmailRecoverPassword`;
  public static changePassword = `${environment.API_URL_AUTH}seguridad/acceso/ChangePassword`;
  public static updatePassword = `${environment.API_URL_AUTH}seguridad/ChangePasswordWithOldPassword`;
  public static recoverUserFromOIM = `${environment.API_URL_AUTH}token/isvalid`;
  public static loginOIM = `${environment.OIM_LOGIN_URL}`;
}
