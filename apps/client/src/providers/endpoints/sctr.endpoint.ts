import { environment } from '@mx/environments/environment';
export class SctrEndpoint {
  public static GetInsuredsByRecord = `${environment.API_URL_CLIENT}sctr/constancias/asegurados/{codigoApp}/{numeroConstancia}`;
  public static GetRecords = `${environment.API_URL_CLIENT}sctr/constancias/{codigoApp}/{numeroPoliza}`;
  public static DownloadRecord = `${environment.API_URL_CLIENT}sctr/constancias/base64/{codigoApp}/{numeroPoliza}/{numeroConstancia}`;
  public static UploadPayroll = `${environment.API_URL_CLIENT}polizas/sctr/subir/planilla`;
}
