import { environment } from '@mx/environments/environment';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';

export class GeneralEndpoint {
  public static parameters = `${environment.API_URL_COMUN}parametros/${APPLICATION_CODE}`;
  public static typesDocument = `${environment.API_URL_COMUN}tiposdocumento/`;
  public static salaryRange = `${environment.API_URL_COMUN}rangosSalariales`;
  public static typesRelation = `${environment.API_URL_COMUN}tiposRelacionFamiliar`;
  public static countriesPhone = `${environment.API_URL_COMUN}pais/contacto/S`;
}
