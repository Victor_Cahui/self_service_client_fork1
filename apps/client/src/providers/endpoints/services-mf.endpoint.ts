// tslint:disable: max-line-length
import { environment } from '@mx/environments/environment';

export class ServicesMFEndpoint {
  // Servicios del Cliente
  public static clientServices = `${environment.API_URL_CLIENT}servicios/{codigoApp}`;
  public static clientServicesHistory = `${environment.API_URL_CLIENT}servicios/buscarPorCliente`;
  // Chofer de Reemplazo
  public static SubstituteDriver = `${environment.API_URL_CLIENT}servicios/chofer/solicitar/{codigoApp}`;
  public static HomeDoctor = `${environment.API_URL_CLIENT}servicios/medico/solicitar/{codigoApp}`;
  public static AllowedAreasDriver = `${environment.API_URL_CLIENT}servicios/chofer/zonas/{codigoApp}`;
  public static AllowedAreasDoctor = `${environment.API_URL_CLIENT}servicios/medico/zonas/{codigoApp}`;
  public static getVehiclesSubstituteDriver = `${environment.API_URL_CLIENT}servicios/chofer/tipoVehiculos/{codigoApp}`;
  public static getPoliciesHomeDoctor = `${environment.API_URL_CLIENT}servicios/medico/tipoPolizas/{codigoApp}`;
}
