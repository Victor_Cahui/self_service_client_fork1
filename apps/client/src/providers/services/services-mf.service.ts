import { Injectable } from '@angular/core';
import { ApiService } from '@mx/core/shared/helpers/api';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { ServicesMFEndpoint } from '@mx/endpoints/services-mf.endpoint';
import { GeneralRequest } from '@mx/statemanagement/models/general.models';
import {
  IAllowedAreasResponse,
  IClientServiceDetail,
  IHomeDoctorRequest,
  IHomeDoctorResponse,
  IServicesHistoryResponse,
  IServicesListRequest,
  ISubstituteDriverRequest,
  ISubstituteDriverResponse,
  IVehicleSubstituteDriverResponse
} from '@mx/statemanagement/models/service.interface';
import { Observable } from 'rxjs';
import { IServicesItemResponse } from '../../statemanagement/models/service.interface';

export enum KEYS {
  SERVICES = 'services'
}

@Injectable({
  providedIn: 'root'
})
export class ServicesMFService {
  constructor(private readonly apiService: ApiService, private readonly storageService: StorageService) {}

  // Lista de Servicios disponibles para un cliente
  getServicesList(params: IServicesListRequest): Observable<Array<IClientServiceDetail>> {
    return this.apiService.get(ServicesMFEndpoint.clientServices, { params, default: [] });
  }

  // Historial de Servicios
  getServicesHistory(params: GeneralRequest): Observable<IServicesHistoryResponse> {
    return this.apiService.get(ServicesMFEndpoint.clientServicesHistory, { params, default: {} });
  }

  getServicesHistoryData(): IServicesHistoryResponse {
    return this.storageService.getStorage(KEYS.SERVICES);
  }

  getServiceData(type: string, number: string): IServicesItemResponse {
    const data = this.getServicesHistoryData();
    if (!data) {
      return null;
    }
    let service = data.enCurso.find(
      (item: IServicesItemResponse) =>
        item.codigoTipoSolicitud === type && item.identificadorSolicitud.toString() === number
    );
    if (service) {
      return service;
    }
    service = data.historial.find(
      (item: IServicesItemResponse) =>
        item.codigoTipoSolicitud === type && item.identificadorSolicitud.toString() === number
    );

    return service || null;
  }

  setServicesHistoryData(data: IServicesHistoryResponse): void {
    this.storageService.saveStorage(KEYS.SERVICES, data);
  }

  clearServiceHistoryData(): void {
    this.storageService.removeItem(KEYS.SERVICES);
  }

  // Solicitud Chofer de Reemplazo
  substituteDriverRequest(
    body: ISubstituteDriverRequest,
    params: GeneralRequest
  ): Observable<ISubstituteDriverResponse> {
    return this.apiService.post(ServicesMFEndpoint.SubstituteDriver, body, { params, preloader: true, default: {} }); // Verifcar Preloader
  }

  // Solicitud Medico a domicilio
  homeDoctorRequest(body: IHomeDoctorRequest, params: GeneralRequest): Observable<ISubstituteDriverResponse> {
    return this.apiService.post(ServicesMFEndpoint.HomeDoctor, body, { params, preloader: true }); // Verifcar Preloader
  }

  // Zonas Permitidas
  getAllowedAreasDriver(params: GeneralRequest): Observable<Array<IAllowedAreasResponse>> {
    return this.apiService.get(ServicesMFEndpoint.AllowedAreasDriver, { params, default: [] });
  }

  getAllowedAreasDoctor(params: GeneralRequest): Observable<Array<IAllowedAreasResponse>> {
    return this.apiService.get(ServicesMFEndpoint.AllowedAreasDoctor, { params, default: [] });
  }

  // Tipo de Vehiculo
  getVehiclesSubstituteDriver(params: GeneralRequest): Observable<Array<IVehicleSubstituteDriverResponse>> {
    return this.apiService.get(ServicesMFEndpoint.getVehiclesSubstituteDriver, { params, default: [] });
  }
  // Policy to Home Dotor
  getPoliciesHomeDoctor(params: GeneralRequest): Observable<Array<IHomeDoctorResponse>> {
    return this.apiService.get(ServicesMFEndpoint.getPoliciesHomeDoctor, { params });
  }
}
