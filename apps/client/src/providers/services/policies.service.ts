// tslint:disable: no-for-in
import { Injectable } from '@angular/core';
import { COVERAGE_STATUS } from '@mx/components/shared/utils/policy';
import { ApiService } from '@mx/core/shared/helpers/api';
import { ArrayUtil } from '@mx/core/shared/helpers/util/array';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { PolicyEndpoint } from '@mx/endpoints';
import { SEARCH_ORDEN_DESC } from '@mx/settings/constants/general-values';
import { POLICY_TYPES } from '@mx/settings/constants/policy-values';
import {
  IAgent,
  IBodySendPolicyTransfer,
  ICareNetworksRequest,
  ICareNetworksResponse,
  IChangeModalityBody,
  IChangeModalityRequest,
  ICoveragesPolicy,
  ICoveragesPolicyRequest,
  ICoveragesPolicyVehicleRequest,
  IDeductiblePolicy,
  IExclusionsByInsuredRequest,
  IExclusionsResponse,
  IFoilAttachedRequest,
  IFoilAttachedResponse,
  IObservationsResponse,
  IPoliciesAndProductsByClientResponse,
  IPoliciesByClientRequest,
  IPoliciesByClientResponse,
  IPolicyFeeRequest,
  IPolicyFeeResponse,
  IPolicyHousesRequest,
  IPolicyHousesResponse,
  IPolicyInsuredEditDataRequest,
  IPolicyInsuredRequest,
  IPolicyInsuredResponse,
  IPolicySearchByNumberRequest,
  IPolicyTransferRequest,
  ISctrByClientRequest,
  ISctrByClientResponse
} from '@mx/statemanagement/models/policy.interface';
import { BehaviorSubject, Observable, Subject, timer } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import { KEY_USER_TOKEN } from '../../settings/auth/auth-values';

export enum STORAGE_KEYS {
  POLICIES_BY_CLIENT = 'policies_by_client',
  POLICY_TYPES = 'policies_type',
  HAS_HEALTH_POLICIES = 'hasHealthPolicies',
  HAS_DIGITAL_CLINIC = 'hasDigitalClinic'
}

@Injectable({
  providedIn: 'root'
})
export class PoliciesService {
  private readonly policies: BehaviorSubject<Array<IPoliciesByClientResponse>>;
  private readonly policy: Subject<IPoliciesByClientResponse>;

  policyTypes: Array<IPoliciesAndProductsByClientResponse>;
  policyTypesSubject: BehaviorSubject<Array<IPoliciesAndProductsByClientResponse>>;

  constructor(private readonly apiService: ApiService, private readonly storageService: StorageService) {
    this.policy = new Subject<IPoliciesByClientResponse>();

    // Polizas por Cliente
    const policiesStorage = this.storageService.getStorage(STORAGE_KEYS.POLICIES_BY_CLIENT);
    this.policies = new BehaviorSubject<Array<IPoliciesByClientResponse>>(policiesStorage || []);

    // Tipos de Poliza por Cliente
    this.policyTypesSubject = new BehaviorSubject<Array<IPoliciesAndProductsByClientResponse>>(null);
  }

  /**
   * Pólizas por cliente.
   * @description Obtener las pólizas desde el servidor
   * @param policiesByClientRequest :IPoliciesByClientRequest
   */
  searchByClientService(
    policiesByClientRequest: IPoliciesByClientRequest
  ): Observable<Array<IPoliciesByClientResponse>> {
    return this.apiService.get(PolicyEndpoint.PoliciesSearchByClient, {
      params: { ...policiesByClientRequest, pSeccion: 'Bandeja' },
      default: []
    });
  }

  /**
   * Pólizas por cliente.
   * @description Obtener las pólizas almacenadas en local desde el menu
   * @param policiesByClientRequest :IPoliciesByClientRequest
   */
  searchByClientLocal(): BehaviorSubject<Array<IPoliciesByClientResponse>> {
    return this.policies;
  }

  setSearchByClientLocal(policies: Array<IPoliciesByClientResponse>): void {
    this.policies.next(policies);
    this.storageService.saveStorage(STORAGE_KEYS.POLICIES_BY_CLIENT, policies);
  }

  getHealthyPolicy(): any {
    return this.policies.pipe(map((policy: any) => policy.filter(this._filterPolicies.bind(this))));
  }

  getSortedHealthyPolicies(): any {
    return this.getHealthyPolicy().pipe(
      map((policies: any) => {
        const byStartDate = ArrayUtil.arraySortByKey(policies, 'fechaInicio', SEARCH_ORDEN_DESC);

        return ArrayUtil.customSort(
          byStartDate,
          [POLICY_TYPES.MD_SALUD.code, POLICY_TYPES.MD_EPS.code, POLICY_TYPES.MD_SCTR.code],
          'tipoPoliza'
        );
      })
    );
  }

  // Tipos de Seguro y Productos asociados a un Cliente
  policiesAndProductsByClient(
    policiesByClientRequest: IPoliciesByClientRequest
  ): Observable<Array<IPoliciesAndProductsByClientResponse>> {
    this.policyTypes = this.getPolicyTypesData();
    if (!this.policyTypes || (this.policyTypes && !this.policyTypes.length)) {
      return this.apiService.get(PolicyEndpoint.PoliciesAndProductsByClient, {
        params: policiesByClientRequest,
        default: []
      });
    }
    this.policyTypesSubject.next(this.policyTypes);

    return this.policyTypesSubject.asObservable();
  }

  getPolicyTypesData(): Array<IPoliciesAndProductsByClientResponse> {
    return this.policyTypes || this.storageService.getStorage(STORAGE_KEYS.POLICY_TYPES);
  }

  setPolicyTypes(data: Array<IPoliciesAndProductsByClientResponse>): void {
    if (data) {
      this.policyTypes = data;
      this.storageService.saveStorage(STORAGE_KEYS.POLICY_TYPES, data);
      this.policyTypesSubject.next(data);
    }
  }

  // Descargar Poliza por PDF
  seePolicePdfById(numberPolice: string): Observable<{ base64: string }> {
    return this.apiService.get(`${PolicyEndpoint.SeePolicePdf}${numberPolice}`, { params: {}, default: [] });
  }

  // Detalle de poliza por su numero
  searchByNumber(policyByNumberRequest: IPolicySearchByNumberRequest): Observable<any> {
    return this.apiService.get(PolicyEndpoint.PolicySearchByNumber, { params: policyByNumberRequest, default: {} });
  }

  // Obtener hogares asegurados por la póliza.
  houses(policyHousesRequest: IPolicyHousesRequest): Observable<Array<IPolicyHousesResponse>> {
    return this.apiService.get(PolicyEndpoint.Houses, { params: policyHousesRequest, default: [] });
  }

  // Obtener personas aseguradas por la póliza.
  insured(policyInsuredRequest: IPolicyInsuredRequest): Observable<Array<IPolicyInsuredResponse>> {
    return this.apiService.get(PolicyEndpoint.Insured, { params: policyInsuredRequest, default: [] });
  }

  // Obtener plan de palgos (cuotas) por póliza.
  feePlan(policyFeeRequest: IPolicyFeeRequest): Observable<Array<IPolicyFeeResponse>> {
    return this.apiService.get(PolicyEndpoint.FeePlan, { params: policyFeeRequest, default: [] });
  }

  // Coberturas de Pólizas (menos vehículo)
  getCoveragesByPolicy(coveragePolicyRequest: ICoveragesPolicyRequest): Observable<Array<ICoveragesPolicy>> {
    return this.apiService.get(PolicyEndpoint.Coverages, { params: coveragePolicyRequest, default: [] });
  }

  // Agente por Póliza
  getAgentByPolicy(coveragePolicyRequest: ICoveragesPolicyRequest): Observable<{ agente: IAgent }> {
    return this.apiService.get(PolicyEndpoint.AgentByPolicy, { params: coveragePolicyRequest, default: {} });
  }

  // SCTR Pólizas por cliente
  sctrSearchByClient(sctrByClientRequest: ISctrByClientRequest): Observable<Array<ISctrByClientResponse>> {
    return this.apiService.get(PolicyEndpoint.SctrSearchByClient, { params: sctrByClientRequest, default: [] });
  }

  // Descargar Constancia en PDF
  constancyPdf(numberPolice: string): Observable<{ base64: string }> {
    return this.apiService.get(`${PolicyEndpoint.SctrConstancyPdf}`, {
      params: { numeroPoliza: numberPolice },
      default: {}
    });
  }

  getDeductiblesByPolicy(coveragePolicyRequest: ICoveragesPolicyRequest): Observable<Array<IDeductiblePolicy>> {
    return this.apiService.get(`${PolicyEndpoint.GetDeductiblesByPolicy}`, {
      params: coveragePolicyRequest,
      default: []
    });
  }

  seeExclusionsPdfByPolicy(coveragePolicyRequest: ICoveragesPolicyRequest): Observable<Array<IDeductiblePolicy>> {
    return this.apiService.get(`${PolicyEndpoint.SeeExclusionsPdfByPolicy}`, {
      params: coveragePolicyRequest,
      default: []
    });
  }

  getCoveragesByPolicyAndVehicle(
    coveragePolicyVehicleRequest: ICoveragesPolicyVehicleRequest
  ): Observable<Array<ICoveragesPolicy>> {
    return this.apiService.get(PolicyEndpoint.CoveragesByVehicle, {
      params: coveragePolicyVehicleRequest,
      default: []
    });
  }

  // Editar Data de asegurado dependiente.
  editInsured(policyInsuredEditDataRequest: IPolicyInsuredEditDataRequest): Observable<any> {
    const codigoApp = policyInsuredEditDataRequest.codigoApp;
    const numeroPoliza = policyInsuredEditDataRequest.numeroPoliza;
    const tipoDocumentoAsegurado = policyInsuredEditDataRequest.tipoDocumento;
    const documentoAsegurado = policyInsuredEditDataRequest.documento;
    delete policyInsuredEditDataRequest.codigoApp;
    delete policyInsuredEditDataRequest.numeroPoliza;
    delete policyInsuredEditDataRequest.tipoDocumento;
    delete policyInsuredEditDataRequest.documento;
    const params = [
      `codigoApp=${codigoApp}`,
      `numeroPoliza=${numeroPoliza}`,
      `tipoDocumentoAsegurado=${tipoDocumentoAsegurado}`,
      `documentoAsegurado=${documentoAsegurado}`
    ].join('&');
    const query = `${PolicyEndpoint.EditInsuredData}?${params}`;

    return Observable.create(observer => {
      const request = new XMLHttpRequest();
      const formData: any = new FormData();
      for (const key in policyInsuredEditDataRequest) {
        if (policyInsuredEditDataRequest.hasOwnProperty(key)) {
          formData.append(key, policyInsuredEditDataRequest[key]);
        }
      }
      request.onload = () => {
        if (request.status === 200) {
          observer.next(request);
        } else {
          observer.error(new Error(request.statusText));
        }
      };
      request.onerror = () => {
        observer.error(new Error(`XMLHttpRequest Error: ${request.statusText}`));
      };
      request.open('PUT', query);
      request.setRequestHeader('Authorization', `Bearer ${this.storageService.getItem(KEY_USER_TOKEN)}`);
      request.send(formData);
    });
  }

  // Enviar solicitud de Traspaso de Póliza
  sendPolicyTransferRequest(
    body: IBodySendPolicyTransfer,
    request: IPolicyTransferRequest
  ): Observable<IBodySendPolicyTransfer> {
    return this.apiService.post(PolicyEndpoint.PolicyTransfer, body, { params: request, default: {} });
  }

  getFoildAttachedPolicy(foilAttachedRequest: IFoilAttachedRequest): Observable<IFoilAttachedResponse> {
    return this.apiService.get(`${PolicyEndpoint.FoilAttachedByPolicy}`, { params: foilAttachedRequest, default: {} });
  }

  getPolicyBaseComponent(): Subject<IPoliciesByClientResponse> {
    return this.policy;
  }

  setPolicyBaseComponent(objPolicy: IPoliciesByClientResponse): void {
    const timer$ = timer(0).subscribe(() => {
      this.getPolicyBaseComponent().next(objPolicy);
      timer$.unsubscribe();
    });
  }

  // Exclusiones por asegurado
  getExclusionsByInsured(coveragePolicyRequest: IExclusionsByInsuredRequest): Observable<Array<IExclusionsResponse>> {
    return this.apiService.get(`${PolicyEndpoint.ExclusionsByInsuredHealth}`, {
      params: coveragePolicyRequest,
      default: []
    });
  }

  // Observaciones por asegurado
  getObservationsByInsured(coveragePolicyRequest: IExclusionsByInsuredRequest): Observable<IObservationsResponse> {
    return this.apiService.get(`${PolicyEndpoint.ObservationsByInsuredHealth}`, {
      params: coveragePolicyRequest,
      default: {}
    });
  }

  // Redes de Atención Salud
  getNetworksCare(careNetworksRequest: ICareNetworksRequest): Observable<Array<ICareNetworksResponse>> {
    return this.apiService.get(`${PolicyEndpoint.CareNetworks}`, { params: careNetworksRequest, default: [] });
  }

  // Cambiar de modalidad
  changeModality(data: IChangeModalityRequest, body: IChangeModalityBody): Observable<any> {
    // devuelve objeto vacio
    return this.apiService.post(`${PolicyEndpoint.ChangeModality}`, body, { params: data });
  }

  setHasHealthPolicies(hasHealthPolicies: boolean): void {
    this.storageService.saveStorage(STORAGE_KEYS.HAS_HEALTH_POLICIES, hasHealthPolicies);
  }

  getHasHealthPolicies(): boolean {
    return this.storageService.getStorage(STORAGE_KEYS.HAS_HEALTH_POLICIES);
  }

  setHasDigitalClinic(hasDigitalClinic: string): void {
    this.storageService.saveStorage(STORAGE_KEYS.HAS_DIGITAL_CLINIC, hasDigitalClinic);
  }

  getHasClinicDigital(): boolean {
    return this.storageService.getStorage(STORAGE_KEYS.HAS_DIGITAL_CLINIC);
  }

  private _filterPolicies(p): boolean {
    const isSALUD = p.tipoPoliza === POLICY_TYPES.MD_SALUD.code && this._isCoverageStatusContemplated(p);
    const isEPS = p.tipoPoliza === POLICY_TYPES.MD_EPS.code;

    return isSALUD || isEPS;
  }

  private _isCoverageStatusContemplated(p): boolean {
    return [COVERAGE_STATUS.CON_COBERTURA, COVERAGE_STATUS.RECIBO_VENCIDO].includes(p.estadoCobertura);
  }
}
