import { Injectable } from '@angular/core';
import { ApiService } from '@mx/core/shared/helpers/api';
import { UbigeoEndpoint } from '@mx/endpoints/ubigeo.endpoint';
import { IUbigeoResponse } from '@mx/statemanagement/models/ubigeo.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UbigeoService {
  constructor(private readonly apiService: ApiService) {}

  getDepartaments(): Observable<Array<IUbigeoResponse>> {
    return this.apiService.get(`${UbigeoEndpoint.departamentos}`, { default: [] });
  }

  getProvinces(departamento: string): Observable<Array<IUbigeoResponse>> {
    return this.apiService.get(`${UbigeoEndpoint.provincias}/${departamento}`, { default: [] });
  }

  getDistricts(provincia: string): Observable<Array<IUbigeoResponse>> {
    return this.apiService.get(`${UbigeoEndpoint.distritos}/${provincia}`, { default: [] });
  }
}
