import { Injectable } from '@angular/core';
import { ApiService } from '@mx/core/shared/helpers/api';
import { OfficeEndpoint } from '@mx/endpoints/office.endpoint';
import { IOffice, IOfficeListRequest } from '@mx/statemanagement/models/office.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OfficeService {
  constructor(private readonly apiService: ApiService) {}

  getOfficeList(params: IOfficeListRequest): Observable<Array<IOffice>> {
    return this.apiService.get(OfficeEndpoint.officeList, { params, default: [] });
  }
}
