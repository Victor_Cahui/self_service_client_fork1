import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiService } from '@mx/core/shared/helpers/api';
import { SkipInsterceptorJsonHeader } from '@mx/core/shared/helpers/interceptors/content-type-json-interceptor';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { ObjectUtil } from '@mx/core/shared/helpers/util/object';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { ClientEndpoint } from '@mx/endpoints/client.endpoint';
import { AuthService } from '@mx/services/auth/auth.service';
import { GROUP_TYPE_ID_COMPANY, GROUP_TYPE_ID_PERSON, KEY_GROUP_TYPE_AUTH } from '@mx/settings/auth/auth-values';
import {
  IBodySendDataContact,
  IClientAddressRequest,
  IClientAddressResponse,
  IClientInformationResponse,
  IClientJobInformationResponse,
  IObtenerConfiguracionRequest,
  IObtenerConfiguracionResponse,
  IReferredContactRequest,
  IReferredContactResponse,
  IUpdateDataWorkerRequest,
  IVerificacionDocumentoRequest,
  IVerificacionDocumentoResponse
} from '@mx/statemanagement/models/client.models';
import { GeneralRequest, GeneralSearchRequest, IMenuItem } from '@mx/statemanagement/models/general.models';
import {
  IContactsDataRequest,
  IContactsDataResponse,
  IProfessionsDataRequest,
  IProfessionsDataResponse,
  IProfileNonSensibleDataRequest,
  IProfileSensibleDataRequest
} from '@mx/statemanagement/models/profile.model';
import { isEmpty } from 'lodash-es';
import { UploadFile } from 'ngx-uploader';
import { BehaviorSubject, Observable } from 'rxjs';

export enum KEYS {
  USERDATA = 'user_data'
}
@Injectable({
  providedIn: 'root'
})
export class ClientService {
  userDataSubject: BehaviorSubject<IClientInformationResponse>;
  canShowModalElectronicPolicy: boolean;

  constructor(
    private readonly apiService: ApiService,
    private readonly _LocalStorageService: LocalStorageService,
    private readonly authService: AuthService,
    private readonly storageService: StorageService
  ) {
    this.userDataSubject = new BehaviorSubject<IClientInformationResponse>(this.getUserData());
  }

  /**
   * Obtener el menu del cliente
   */
  getClientMenu(request: GeneralRequest): Observable<Array<IMenuItem>> {
    return this.apiService.get(ClientEndpoint.GetClientMenu, { params: request, default: [] });
  }

  /**
   * Verificar documento. Se revisa en las distintas bases de datos de MAPFRE para obtener el tipo de usuario del que se trata.
   * @param documentVerification Objeto de consulta para obtener la datos.
   */
  getDocumentVerification(
    documentVerification: IVerificacionDocumentoRequest
  ): Observable<IVerificacionDocumentoResponse> {
    return this.apiService.get<IVerificacionDocumentoResponse>(ClientEndpoint.DocumentVerification, {
      params: documentVerification,
      default: {}
    });
  }

  /**
   * Obtener los datos personales de un determinado cliente, incluyendo la informacion del lugar de trabajo.
   * @param clientInformationRequest Objeto de consulta para obtener la datos.
   */
  getClientInformation(userId: GeneralSearchRequest): Observable<IClientInformationResponse> {
    const userData = this.getUserData();
    if (userData) {
      return this.userDataSubject.asObservable();
    }

    return this.apiService.get<IClientInformationResponse>(ClientEndpoint.GetInformation, { params: userId });
  }

  getUserData(): IClientInformationResponse {
    return this.storageService.getStorage(KEYS.USERDATA);
  }

  setUserData(data: IClientInformationResponse): void {
    const userData = this.getUserData();
    if (!ObjectUtil.isEquals(userData, data)) {
      this.storageService.saveStorage(KEYS.USERDATA, data);
      this.userDataSubject.next(data);
    }
  }

  getUserDataSubject(): BehaviorSubject<IClientInformationResponse> {
    return this.userDataSubject;
  }

  getUserEmail(): string {
    const data = this.getUserData();

    return data ? data.emailCliente : '';
  }

  getUserPhone(): string {
    const data = this.getUserData();

    return data ? data.telefonoMovil : '';
  }

  /**
   * Obtener la configuracion de un determinado cliente.
   * @param obtenerConfiguracionRequest Objeto de consulta para obtener la configuración.
   */
  getConfiguration(
    obtenerConfiguracionRequest: IObtenerConfiguracionRequest
  ): Observable<IObtenerConfiguracionResponse> {
    return this.apiService.get<IObtenerConfiguracionResponse>(ClientEndpoint.GetConfiguration, {
      params: obtenerConfiguracionRequest,
      default: {}
    });
  }

  /**
   * Obtener las direcciones de un determinado cliente, incluyendo las asociadas a las pólizas y de correspondencia.
   * @param clientAddressRequest Objeto de consulta para obtener las direcciones de un cliente.
   */
  getAddress(clientAddressRequest: IClientAddressRequest): Observable<Array<IClientAddressResponse>> {
    return this.apiService.get<Array<IClientAddressResponse>>(ClientEndpoint.GetAddress, {
      params: clientAddressRequest,
      default: []
    });
  }

  /**
   * Obtener la dirección de correspondencia de un determinado cliente.
   * @param clientAddressRequest Objeto de consulta para obtener la dirección de correspondencia.
   */
  getCorresponseAddress(clientAddressRequest: any): Observable<IClientAddressResponse> {
    return this.apiService.get<IClientAddressResponse>(ClientEndpoint.GetCorrespondenceAddress, {
      params: clientAddressRequest,
      default: {}
    });
  }

  updateClientEmail(identity: any, body: { nuevoEmailUsuario: string }): Observable<any> {
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');

    return this.apiService.put(ClientEndpoint.updateClientEmail, body, {
      headers: header,
      params: identity,
      default: {}
    });
  }

  updateCorrespondenceAddress(identity: any, clientAddressRequest: IClientAddressResponse): Observable<any> {
    return this.apiService.put(ClientEndpoint.GetCorrespondenceAddress, clientAddressRequest, {
      params: identity,
      default: {}
    });
  }

  /**
   * Actualiza los datos de trabajador de un cliente
   */
  updateDataWork(identity: any, dataWorker: IUpdateDataWorkerRequest): Observable<IClientJobInformationResponse> {
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');

    return this.apiService.put(ClientEndpoint.UpdateDataWork, dataWorker, {
      headers: header,
      params: identity,
      default: {}
    });
  }

  /**
   * Enviar Contacto Agente
   */
  sendContactData(body: IBodySendDataContact, request: GeneralRequest): Observable<IBodySendDataContact> {
    return this.apiService.post(ClientEndpoint.AgentContact, body, { params: request, default: {} });
  }

  /**
   * Enviar Contacto Referidos
   */
  sendReferredContact(body: IReferredContactRequest, data: GeneralRequest): Observable<IReferredContactResponse> {
    return this.apiService.post(ClientEndpoint.ReferredContact, body, { params: data, default: {} });
  }

  /**
   * Actualizar Perfil de Usuario
   */
  sendRequestToUpdateSensibleData(
    sensibleDataRequest: IProfileSensibleDataRequest,
    file?: UploadFile
  ): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set(SkipInsterceptorJsonHeader, '');

    return this.apiService.putDataAndFile(
      `${ClientEndpoint.PutProfileSensibleData}`,
      sensibleDataRequest,
      isEmpty(file) ? [] : [file],
      {
        preloader: false,
        params: {},
        headers
      }
    );
  }

  sendRequestToUpdateSensibleDataWithNoFile(sensibleDataRequest: IProfileSensibleDataRequest): Observable<any> {
    return this.apiService.put(`${ClientEndpoint.PutProfileSensibleData}`, sensibleDataRequest, {
      preloader: false,
      params: {}
    });
  }

  sendRequestToUpdateNoSensibleData(noSensibleDataRequest: IProfileNonSensibleDataRequest): Observable<String> {
    return this.apiService.put(ClientEndpoint.PutProfileNonSensibleData, noSensibleDataRequest, {
      preloader: false,
      params: {}
    });
  }

  getContactsData(contactsDataRequest: IContactsDataRequest): Observable<Array<IContactsDataResponse>> {
    return this.apiService.get(ClientEndpoint.GetContactData, { params: contactsDataRequest, default: [] });
  }

  getProfessions(professionsDataRequest: IProfessionsDataRequest): Observable<Array<IProfessionsDataResponse>> {
    return this.apiService.get(ClientEndpoint.GetProfessions, { params: professionsDataRequest, default: [] });
  }

  isGroupTypePerson(): boolean {
    const groupType = this._LocalStorageService.getItem(KEY_GROUP_TYPE_AUTH);

    return +GROUP_TYPE_ID_PERSON === +groupType;
  }

  isGroupTypeCompany(): boolean {
    return +GROUP_TYPE_ID_COMPANY === +this._LocalStorageService.getItem(KEY_GROUP_TYPE_AUTH);
  }
}
