import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { httpParamSerializerJQLike } from '@mx/core/shared/helpers';
import { ApiService } from '@mx/core/shared/helpers/api/api.service';
import { SkipInsterceptorAuthHeader } from '@mx/core/shared/helpers/interceptors/auth-interceptor';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { AuthEndpoint } from '@mx/endpoints/auth.endpoint';
import { PoliciesInfoService } from '@mx/services/policy/policy-info.service';
import {
  CLIENT_ID,
  CONTENT_TYPE,
  GRANT_TYPE,
  GROUP_TYPE_ID_PERSON,
  KEY_DOC_NUMBER_AUTH,
  KEY_DOC_TYPE_AUTH,
  KEY_EXPIRES_IN,
  KEY_GROUP_TYPE_AUTH,
  KEY_GROUP_TYPE_ID,
  KEY_USER_REFRESH_TOKEN,
  KEY_USER_TOKEN
} from '@mx/settings/auth/auth-values';
import { LOCAL_STORAGE_KEYS } from '@mx/settings/constants/key-values';
import {
  AuthInterface,
  IAuthResponse,
  ILogguedUser,
  IUserClaimResponse,
  IUserGroupType,
  IUsersGroupTypeResponse
} from '@mx/statemanagement/models/auth.interface';
import { IdDocument } from '@mx/statemanagement/models/typedocument.interface';
import { isEmpty } from 'lodash-es';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private documentType: string;
  private documentNumber: string;

  constructor(
    private readonly apiService: ApiService,
    private readonly policiesInfoService: PoliciesInfoService,
    private readonly router: Router,
    private readonly storageService: StorageService
  ) {}

  private getHeaderLogin(token: string = null, urlEncode = true): HttpHeaders {
    let header = new HttpHeaders();
    if (urlEncode) {
      header = header.set('Content-Type', CONTENT_TYPE);
    }
    if (token) {
      header = header.set('Authorization', `Bearer ${token}`);
    }
    header = header.set(SkipInsterceptorAuthHeader, '');

    return header;
  }

  login(obj: AuthInterface): Observable<IAuthResponse> {
    obj.client_id = CLIENT_ID;
    obj.grant_type = GRANT_TYPE;

    const header = this.getHeaderLogin();
    let req: any = {
      client_id: obj.client_id,
      grant_type: obj.grant_type,
      password: obj.password,
      username: obj.username,
      scope: 'encrypt'
    };

    if (obj.groupTypeId) {
      req = { ...req, groupTypeId: obj.groupTypeId };
    }

    if (obj.osa) {
      req = { ...req, osa: obj.osa };
    }

    return this.apiService.post(AuthEndpoint.login, httpParamSerializerJQLike(req), { headers: header });
  }

  getUsers(token: string): Observable<IUsersGroupTypeResponse> {
    return this.apiService.post(AuthEndpoint.users, undefined, { headers: this.getHeaderLogin(token, false) });
  }

  accessByGroupType(token: string, groupTypeId: number): Observable<IAuthResponse> {
    const body = `GroupTypeId=${groupTypeId}&IsEncrypt=true`;

    return this.apiService.post(AuthEndpoint.loginGroupType, body, { headers: this.getHeaderLogin(token) });
  }

  claims(token: string): Observable<IUserClaimResponse> {
    return this.apiService.post(AuthEndpoint.claims, undefined, { headers: this.getHeaderLogin(token, false) });
  }

  isAuth(): Boolean {
    return !!this.storageService.getItem(KEY_USER_TOKEN);
  }

  loginComplete(
    token: any,
    typeDoc: string,
    numberDoc: string,
    refreshToken: string,
    userGroupType: IUserGroupType
  ): void {
    const auth = this.retrieveEntity();
    if (auth.type !== typeDoc || auth.number !== numberDoc) {
      this.policiesInfoService.setPolicyBasicInfo(null);
      this.policiesInfoService.setPaymentPolicy(null);
      this.policiesInfoService.setProfileBasicInfo(null);
    }
    this.setUserToken(token);
    this.setUserRefreshToken(refreshToken);
    this.storeEntity(typeDoc, numberDoc, userGroupType);
  }

  logout(): void {
    const data: ILogguedUser = this.storageService.getItem(LOCAL_STORAGE_KEYS.USER);
    const canReload = localStorage.getItem('canReload');
    this.storageService.clear();
    this.storageService.setItem(LOCAL_STORAGE_KEYS.USER, data);
    localStorage.setItem('canReload', canReload);

    this.router.navigate(['/login']);
  }

  setUserToken(token: string): void {
    this.storageService.setItem(KEY_USER_TOKEN, token);
  }

  getUserToken(): string {
    return this.storageService.getItem(KEY_USER_TOKEN);
  }

  setExpirationTimeToken(expiresIn: number): void {
    const expirationDate: Date = new Date();
    expirationDate.setSeconds(expirationDate.getSeconds() + expiresIn);
    const expirationTime = Math.round(expirationDate.getTime() / 1000);

    this.storageService.setItem(KEY_EXPIRES_IN, expirationTime);
  }

  getExpirationTimeToken(): string {
    return this.storageService.getItem(KEY_EXPIRES_IN);
  }

  setUserRefreshToken(token: string): void {
    this.storageService.setItem(KEY_USER_REFRESH_TOKEN, token);
  }

  getUserRefreshToken(): string {
    return this.storageService.getItem(KEY_USER_REFRESH_TOKEN);
  }

  storeEntity(docType: string, docNumber: string, userGroupType: IUserGroupType): void {
    // parametros de consulta
    this.documentNumber = (userGroupType && userGroupType.documentNumber) || docNumber;
    this.documentType = (userGroupType && userGroupType.documentType) || docType;

    this.storageService.setItem(KEY_GROUP_TYPE_ID, userGroupType.groupType);
    // parametros de autorización
    this.storageService.setItem(
      KEY_GROUP_TYPE_AUTH,
      (userGroupType && userGroupType.groupType) || GROUP_TYPE_ID_PERSON
    );
  }

  retrieveEntity(): IdDocument {
    if (isEmpty(this.documentNumber) || isEmpty(this.documentType)) {
      const loggedUser: ILogguedUser = this.storageService.getStorage(LOCAL_STORAGE_KEYS.USER);
      this.documentNumber = loggedUser && loggedUser.documentNumber;
      this.documentType = loggedUser && loggedUser.documentType;
    }
    const idDoc: IdDocument = {
      number: this.documentNumber,
      type: this.documentType
    } as IdDocument;

    return idDoc;
  }

  retrieveEntityAuth(): IdDocument {
    const idDoc: IdDocument = {
      number: this.storageService.getItem(KEY_DOC_NUMBER_AUTH),
      type: this.storageService.getItem(KEY_DOC_TYPE_AUTH)
    } as IdDocument;

    return idDoc;
  }

  getUserFromTokenOIM(token: string): Observable<any> {
    let header = new HttpHeaders();
    header = header.set('Content-Type', 'application/json');
    header = header.set('Authorization', `Bearer ${token}`);

    return this.apiService.get(AuthEndpoint.recoverUserFromOIM, { headers: header });
  }
}
