import { Injectable } from '@angular/core';
import { ApiService } from '@mx/core/shared/helpers/api';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { PaymentEndpoint } from '@mx/endpoints/payment.endpoint';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { GeneralRequest } from '@mx/statemanagement/models/general.models';
import {
  IPaymentConfigurationResponse,
  IPaymentQuoteCard,
  IPaymentRefinancingRequest,
  IPaymentRefinancingRequestRequest,
  IPaymentRefinancingResponse,
  IPaymentRenewalRequest,
  IPaymentRequest,
  IPaymentResponse,
  IPaymentTypeResponse,
  IReceiptPayRequest,
  IReceiptRequest,
  IReceiptResponse,
  ISendReceiptRequest,
  ISoatIssuanceRequest,
  ISoatQuoteRequest,
  ISoatQuoteResponse
} from '@mx/statemanagement/models/payment.models';
import { BehaviorSubject, Observable } from 'rxjs';

export enum KEY_STORAGE_PAYMENT {
  CURRENT_QUOTE = 'payment_current_quote',
  PAYMENT_LIST = 'payment_list_settings'
}

@Injectable({
  providedIn: 'root'
})
export class PaymentService {
  public notificactionForPS = new BehaviorSubject<object>({});
  private readonly vigencyDateSub: BehaviorSubject<{ start: string; end: string }>;
  private readonly useMapfreDollarsSub: BehaviorSubject<boolean>;
  private readonly updateQuotaSub: BehaviorSubject<string>;
  private readonly paymetTypeSub: BehaviorSubject<IPaymentConfigurationResponse>;
  private paymentTypes: Array<IPaymentTypeResponse>;
  private readonly paymentTypesSubject: BehaviorSubject<Array<IPaymentTypeResponse>>;

  constructor(private readonly apiService: ApiService, private readonly storageService: StorageService) {
    this.vigencyDateSub = new BehaviorSubject<{ start: string; end: string }>({ start: '', end: '' });
    this.paymetTypeSub = new BehaviorSubject<IPaymentConfigurationResponse>(null);
    this.useMapfreDollarsSub = new BehaviorSubject<boolean>(false);
    this.updateQuotaSub = new BehaviorSubject<string>('');

    if (!this.paymentTypesSubject) {
      this.paymentTypesSubject = new BehaviorSubject<Array<IPaymentTypeResponse>>(null);
      this.apiService.get(PaymentEndpoint.PaymentType, { params: { codigoApp: APPLICATION_CODE } }).subscribe(
        (types: Array<IPaymentTypeResponse>) => {
          this.setPaymentTypes(types);
        },
        () => {}
      );
    }
  }

  // Pagos Pendientes
  paymentsByClient(paymentRequest: IPaymentRequest): Observable<Array<IPaymentResponse>> {
    return this.apiService.get(PaymentEndpoint.PaymentSearchByClient, { params: paymentRequest, default: [] });
  }

  // Recibos de Pago
  receiptsByClient(receiptRequest: IReceiptRequest): Observable<Array<IReceiptResponse>> {
    return this.apiService.get(PaymentEndpoint.ReceiptsSearchByClient, { params: receiptRequest, default: [] });
  }

  // Tipos de Pago
  paymentType(paymentType: any): Observable<Array<IPaymentTypeResponse>> {
    this.paymentTypes = this.getPaymentTypesData();
    if (!this.paymentTypes || (this.paymentTypes && !this.paymentTypes.length)) {
      return this.apiService.get(PaymentEndpoint.PaymentType, { params: paymentType, default: [] });
    }
    this.paymentTypesSubject.next(this.paymentTypes);

    return this.paymentTypesSubject.asObservable();
  }

  getPaymentTypesData(): Array<IPaymentTypeResponse> {
    return this.paymentTypes;
  }

  setPaymentTypes(data: Array<IPaymentTypeResponse>): void {
    this.paymentTypes = data;
    this.paymentTypesSubject.next(data);
  }

  // PDF Recibo de Pago
  downLoadReceiptPdf(receiptNumber: any): Observable<{ base64: string }> {
    return this.apiService.get(PaymentEndpoint.DownLoadReceiptPDF, { params: receiptNumber, default: {} });
  }

  // Enviar Recibo de Pago por Correo
  sendReceiptPdf(body: any, receiptRequest: ISendReceiptRequest): Observable<{ base64: string }> {
    return this.apiService.post(PaymentEndpoint.SendReceiptPDF, body, { params: receiptRequest, default: {} });
  }

  // Cotizar SOAT
  SoatQuote(soatQuoteRequest: ISoatQuoteRequest): Observable<ISoatQuoteResponse> {
    return this.apiService.get(PaymentEndpoint.SoatQuote, { params: soatQuoteRequest, preloader: true, default: {} });
  }

  // Emision y pago de SOAT
  SoatIssuance(body: any, soatIssuanceRequest: ISoatIssuanceRequest): Observable<any> {
    return this.apiService.post(PaymentEndpoint.SoatIssuance, body, {
      params: soatIssuanceRequest,
      preloader: true,
      default: {}
    });
  }

  // Pago de recibos
  receiptPay(body: IReceiptPayRequest, params: GeneralRequest): Observable<any> {
    return this.apiService.post(PaymentEndpoint.ReceiptPay, body, { params, preloader: true, default: {} });
  }

  // Configuracion de pagos
  paymentConfiguration(paymentRequest: IPaymentRequest): Observable<Array<IPaymentConfigurationResponse>> {
    return this.apiService.get(PaymentEndpoint.PaymentConfiguration, { params: paymentRequest, default: [] });
  }

  // Configuracion de pagos - renovacion
  paymentConfigurationRenewal(body: IPaymentRenewalRequest, params: GeneralRequest): Observable<any> {
    return this.apiService.post(PaymentEndpoint.PaymentConfigurationRenewal, body, { params });
  }

  // Configuracion de pagos - refinanciamiento
  paymentConfigurationRefinancing(
    paymentRefinancingRequest: IPaymentRefinancingRequest
  ): Observable<IPaymentRefinancingResponse> {
    return this.apiService.get(PaymentEndpoint.PaymentConfigurationRefinancing, { params: paymentRefinancingRequest });
  }

  // Configuracion de pagos - solicitar refinanciamiento
  paymentConfigurationRefinancingRequest(
    body: IPaymentRefinancingRequestRequest,
    params: GeneralRequest
  ): Observable<any> {
    return this.apiService.post(PaymentEndpoint.PaymentConfigurationRefinancingRequest, body, { params });
  }

  /***************************
    Datos de Pago en Storage
  ****************************/
  setCurrentQuote(quote?: IPaymentQuoteCard): void {
    this.storageService.saveStorage(KEY_STORAGE_PAYMENT.CURRENT_QUOTE, quote);
  }

  getCurrentQuote(): IPaymentQuoteCard {
    return this.storageService.getStorage(KEY_STORAGE_PAYMENT.CURRENT_QUOTE);
  }

  clearCurrentQuote(): void {
    this.storageService.removeItem(KEY_STORAGE_PAYMENT.CURRENT_QUOTE);
  }

  setVigencyDate(startDate: string, endDate: string): void {
    this.vigencyDateSub.next({ start: startDate, end: endDate });
  }

  onVigencyDateChange(): BehaviorSubject<{ start: string; end: string }> {
    return this.vigencyDateSub;
  }

  onUseMapfreDolarsChange(): BehaviorSubject<boolean> {
    return this.useMapfreDollarsSub;
  }

  updateQuota(): BehaviorSubject<string> {
    return this.updateQuotaSub;
  }

  isContractorCompleted(): boolean {
    const quote = this.getCurrentQuote();

    return !!(quote && quote.contractor && quote.contractor.document && quote.contractor.documentType);
  }

  /**********************************
   Configuración de Pagos en Storage
   **********************************/
  setPaymentListConfiguration(payments: Array<IPaymentConfigurationResponse>): void {
    this.storageService.saveStorage(KEY_STORAGE_PAYMENT.PAYMENT_LIST, payments);
  }

  onPaymentTypeChange(): BehaviorSubject<IPaymentConfigurationResponse> {
    return this.paymetTypeSub;
  }

  getPaymentListConfiguration(): Array<IPaymentConfigurationResponse> {
    return this.storageService.getStorage(KEY_STORAGE_PAYMENT.PAYMENT_LIST);
  }

  clearPaymentListConfiguration(): void {
    this.storageService.removeItem(KEY_STORAGE_PAYMENT.PAYMENT_LIST);
  }

  updatePaymenConfiguration(payment: IPaymentConfigurationResponse): void {
    const payments = this.getPaymentListConfiguration();
    this.paymetTypeSub.next(payment);
    payments[payments.findIndex(p => p.numeroPoliza === payment.numeroPoliza)] = payment;
    this.setPaymentListConfiguration(payments);
  }

  changeTotalLyra(): BehaviorSubject<string> {
    return this.updateQuotaSub;
  }
}
