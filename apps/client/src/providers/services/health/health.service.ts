import { Injectable } from '@angular/core';
import { ApiService } from '@mx/core/shared/helpers/api';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { HealthEndpoint } from '@mx/endpoints/health.endpoint';
import {
  IClinicCoverageRequest,
  IClinicCoverageResponse,
  IClinicFavoriteRequest,
  IClinicResponse,
  IClinicsRequest,
  IPoliciesByClinicRequest
} from '@mx/statemanagement/models/health.interface';
import {
  IContractingRequest,
  IContractingResponse,
  IPoliciesByClientResponse
} from '@mx/statemanagement/models/policy.interface';
import { BehaviorSubject, Observable } from 'rxjs';

export enum KEY_STORAGE_CLINIC {
  DETAIL = 'clinic_detail',
  CURRENT_POLICY = 'clinic_current_policy',
  POLICIES = 'clinic_policies',
  IMGLANDSCAPE = 'clinic_image_land_space',
  IMGPORTRAIT = 'clinic_image_portrait'
}

@Injectable({
  providedIn: 'root'
})
export class HealthService {
  private readonly policySelected: BehaviorSubject<IPoliciesByClientResponse>;
  private readonly policies: BehaviorSubject<Array<IPoliciesByClientResponse>>;
  private readonly filterClinic: BehaviorSubject<IClinicsRequest>;

  constructor(private readonly apiService: ApiService, private readonly storageService: StorageService) {
    const policy = this.storageService.getStorage(KEY_STORAGE_CLINIC.CURRENT_POLICY);
    const policies = this.storageService.getStorage(KEY_STORAGE_CLINIC.POLICIES) || [];

    this.policySelected = new BehaviorSubject<IPoliciesByClientResponse>(policy);
    this.policies = new BehaviorSubject<Array<IPoliciesByClientResponse>>(policies);
    this.filterClinic = new BehaviorSubject<IClinicsRequest>(null);
  }

  clinicFavorite(clinicFavoriteRequest: IClinicFavoriteRequest, body: any): Observable<Array<IClinicResponse>> {
    return this.apiService.post(HealthEndpoint.ClinicFavorite, body, { params: clinicFavoriteRequest, default: [] });
  }

  clinicsList(clinicsListRequest: IClinicsRequest): Observable<Array<IClinicResponse>> {
    return this.apiService.get(HealthEndpoint.ClinicsList, { params: clinicsListRequest, default: [] });
  }

  // Datos del Contratante
  getContractingAddress(contractingRequest: IContractingRequest): Observable<IContractingResponse> {
    return this.apiService.get(HealthEndpoint.ContractingAddress, { params: contractingRequest, default: {} });
  }

  getClinicCoverageByPolicy(coverageRequest: IClinicCoverageRequest): Observable<Array<IClinicCoverageResponse>> {
    return this.apiService.get(HealthEndpoint.ClinicCoverage, { params: coverageRequest, default: [] });
  }

  getPolicyByClinic(request: IPoliciesByClinicRequest): Observable<Array<IPoliciesByClientResponse>> {
    return this.apiService.get(HealthEndpoint.GetPolicyByClinic, { params: request, default: [] });
  }

  setCurrentClinic(clinic: IClinicResponse): void {
    this.storageService.saveStorage(KEY_STORAGE_CLINIC.DETAIL, clinic);
  }

  getCurrentClinic(): IClinicResponse {
    return this.storageService.getStorage(KEY_STORAGE_CLINIC.DETAIL);
  }

  setCurrentPolicy(policySelected: IPoliciesByClientResponse): void {
    this.storageService.saveStorage(KEY_STORAGE_CLINIC.CURRENT_POLICY, policySelected);
    this.policySelected.next(policySelected);
  }

  getCurrentPolicy(): BehaviorSubject<IPoliciesByClientResponse> {
    return this.policySelected;
  }

  setPolicies(policies: Array<IPoliciesByClientResponse>): void {
    this.storageService.saveStorage(KEY_STORAGE_CLINIC.POLICIES, policies);
    this.policies.next(policies);
  }

  getPolicies(): BehaviorSubject<Array<IPoliciesByClientResponse>> {
    return this.policies;
  }

  setFilterClinics(filter: IClinicsRequest): void {
    this.filterClinic.next(filter);
  }

  getFilterClinics(): BehaviorSubject<IClinicsRequest> {
    return this.filterClinic;
  }

  setImageLandScapeClinic(key: string, baseimg: string): void {
    this.storageService.saveStorage(KEY_STORAGE_CLINIC.IMGLANDSCAPE + key, baseimg);
  }

  getImageLandScapeClinic(key: string): string {
    return this.storageService.getStorage(KEY_STORAGE_CLINIC.IMGLANDSCAPE + key);
  }

  setImagePortraitClinic(key: string, baseimg: string): void {
    this.storageService.saveStorage(KEY_STORAGE_CLINIC.IMGPORTRAIT + key, baseimg);
  }

  getImagePortraitClinic(key: string): string {
    return this.storageService.getStorage(KEY_STORAGE_CLINIC.IMGPORTRAIT + key);
  }
}
