import { Injectable } from '@angular/core';
import { ApiService } from '@mx/core/shared/helpers/api';
import { ProcessOngoingEndpoint } from '@mx/endpoints';
import { IProcessOngoingRequest, IProcessOngoingResponse } from '@mx/statemanagement/models/process-ongoing.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProcessOngoingService {
  constructor(private readonly apiService: ApiService) {}

  searchByClient(processOngoingRequest: IProcessOngoingRequest): Observable<Array<IProcessOngoingResponse>> {
    return this.apiService.get(`${ProcessOngoingEndpoint.PROCESS_ONGOING}`, {
      params: processOngoingRequest,
      default: []
    });
  }
}
