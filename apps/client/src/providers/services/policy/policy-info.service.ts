import { Injectable } from '@angular/core';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { CURRENT_SCTR_PERIOD } from '@mx/settings/constants/key-values';
import {
  IPaymentPolicy,
  IPoliciesByClientResponse,
  IPolicyBasicInfo,
  ISctrByClientResponse,
  ISctrPeriodResponse
} from '@mx/statemanagement/models/policy.interface';
import { IProfileBasicInfo } from '@mx/statemanagement/models/profile.interface';
import { ITabItem } from '@mx/statemanagement/models/router.interface';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PoliciesInfoService {
  policyBasicInfoBeh: BehaviorSubject<IPolicyBasicInfo>;
  paymentPolicyBeh: BehaviorSubject<IPaymentPolicy>;
  profileBasicInfoBeh: BehaviorSubject<IProfileBasicInfo>;
  policyBeh: BehaviorSubject<IPoliciesByClientResponse>;
  policyBasicInfo: IPolicyBasicInfo;
  paymentPolicy: IPaymentPolicy;
  profileBasicInfo: IProfileBasicInfo;
  clientPolicyResponse: IPoliciesByClientResponse;
  clientSctrPolicyResponse: ISctrByClientResponse;
  clientSctrPeriodResponse: ISctrPeriodResponse;

  constructor(private readonly storageService: StorageService) {
    this.policyBasicInfoBeh = new BehaviorSubject<IPolicyBasicInfo>(null);
    this.paymentPolicyBeh = new BehaviorSubject<IPaymentPolicy>(null);
    this.profileBasicInfoBeh = new BehaviorSubject<IProfileBasicInfo>(null);
    this.policyBeh = new BehaviorSubject<IPoliciesByClientResponse>(null);
  }

  existPolicyBasicInfo(): boolean {
    return !!this.policyBasicInfo;
  }

  getPolicyBasicInfo(): BehaviorSubject<IPolicyBasicInfo> {
    return this.policyBasicInfoBeh;
  }

  setPolicyBasicInfo(policyBasicInfo: IPolicyBasicInfo): void {
    this.policyBasicInfo = policyBasicInfo;
    this.policyBasicInfoBeh.next(policyBasicInfo);
  }

  existPaymentPolicy(): boolean {
    return !!this.paymentPolicy;
  }

  getPaymentPolicy(): BehaviorSubject<IPaymentPolicy> {
    return this.paymentPolicyBeh;
  }

  setPaymentPolicy(paymentPolicy: IPaymentPolicy): void {
    this.paymentPolicy = paymentPolicy;
    this.paymentPolicyBeh.next(paymentPolicy);
  }

  existProfileBasicInfo(): boolean {
    return !!this.profileBasicInfo;
  }

  getProfileBasicInfo(): BehaviorSubject<IProfileBasicInfo> {
    return this.profileBasicInfoBeh;
  }

  setProfileBasicInfo(profileBasicInfo: IProfileBasicInfo): void {
    this.profileBasicInfo = profileBasicInfo;
    this.profileBasicInfoBeh.next(profileBasicInfo);
  }

  existClientPolicyByNumber(policyNumber: string): boolean {
    return !!this.clientPolicyResponse && this.clientPolicyResponse.numeroPoliza === policyNumber;
  }

  setClientPolicyResponse(clientPolicyRequest: IPoliciesByClientResponse): void {
    this.clientSctrPolicyResponse = undefined;
    this.clientPolicyResponse = clientPolicyRequest;
    this.policyBeh.next(clientPolicyRequest);
  }

  getClientPolicy(policyNumber: string): IPoliciesByClientResponse {
    return !!this.clientPolicyResponse && this.clientPolicyResponse.numeroPoliza === policyNumber
      ? this.clientPolicyResponse
      : void 0;
  }

  getPolicy(): IPoliciesByClientResponse {
    return this.clientPolicyResponse;
  }

  getPolicyBeh(): BehaviorSubject<IPoliciesByClientResponse> {
    return this.policyBeh;
  }

  resetClientPolicyResponse(): any {
    this.clientPolicyResponse = undefined;
    this.clientSctrPolicyResponse = undefined;

    return this.clientPolicyResponse;
  }

  canDownloadPolicy(): boolean {
    const policy: any = this.clientSctrPolicyResponse || this.clientPolicyResponse;

    return policy.puedeDescargarPoliza;
  }

  getMPKeys(): any {
    const policy: any = this.clientSctrPolicyResponse || this.clientPolicyResponse;
    if (!policy || !Object.keys(policy).length) {
      return void 0;
    }

    const keys: any[] = policy.acciones || [];

    return keys.reduce((acc, k) => ({ ...acc, [k.codigo]: k.valor }), {});
  }

  existClientSctrPolicyByNumber(policyNumber: string): boolean {
    return !!this.clientSctrPolicyResponse && this.clientSctrPolicyResponse.numeroPoliza === policyNumber;
  }

  // SCTR

  setClientSctrPolicyResponse(clientPolicyRequest: ISctrByClientResponse): void {
    this.clientPolicyResponse = undefined;
    this.clientSctrPolicyResponse = clientPolicyRequest;
  }

  getClientSctrPolicy(policyNumber: string): ISctrByClientResponse {
    const sctr = this.clientSctrPolicyResponse;

    return !!sctr && sctr.numeroPoliza.startsWith(policyNumber) ? sctr : void 0;
  }

  // SCTR - Periods

  setClientSctrPeriodResponse(period: ISctrPeriodResponse): void {
    this.storageService.setItem(CURRENT_SCTR_PERIOD, period.fechaPeriodo);
    this.clientSctrPeriodResponse = period;
  }

  getClientSctrPeriod(): ISctrPeriodResponse {
    const sctrPeriod = this.clientSctrPeriodResponse;

    return sctrPeriod ? sctrPeriod : void 0;
  }

  hideCoveragesTab(tabItemsList: Array<ITabItem>): Array<ITabItem> {
    return tabItemsList.map(tab => {
      const visible = tab.name === 'Coberturas' ? false : tab.visible;

      return { ...tab, visible };
    });
  }
}
