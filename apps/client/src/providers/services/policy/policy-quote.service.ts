import { Injectable } from '@angular/core';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { VEHICLES_QUOTE_BUY_STEP } from '@mx/settings/constants/router-step';
import { IClientAddressResponse } from '@mx/statemanagement/models/client.models';
import { IChoiseTime } from '@mx/statemanagement/models/policy.interface';
import {
  IClientVehicleDetail,
  IGetColorsResponse,
  IVerifyPlateResponse
} from '@mx/statemanagement/models/vehicle.interface';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

export enum KEYS {
  VEHICLE = 'quote_vehicle',
  HEADER = 'quote_header',
  DETAILS = 'quote_detail',
  CHOISE = 'choise_time',
  COLORS = 'colors',
  ADDRESS = 'c_address',
  PERSONAL_DATA_CLAUSE = 'clause_pd',
  INSPECTION_CONDITIONS = 'inspection'
}

@Injectable({
  providedIn: 'root'
})
export class PoliciesQuoteService {
  vehicle: IClientVehicleDetail;
  stepSubject: BehaviorSubject<number>;
  doingPaymentSubject: Subject<boolean>;
  currentStep: number;

  constructor(private readonly storageService: StorageService) {
    this.vehicle = this.newVehicle();
    this.currentStep = 1;
    this.stepSubject = new BehaviorSubject(this.currentStep);
    this.doingPaymentSubject = new Subject<boolean>();
  }

  /****************
   * Vehiculos
   ****************/
  getVehicleData(): IClientVehicleDetail {
    const data = this.storageService.getStorage(KEYS.VEHICLE);

    return data ? data : null;
  }

  setVehicleData(data: IClientVehicleDetail): void {
    this.vehicle = data;
    this.storageService.saveStorage(KEYS.VEHICLE, data);
  }

  // Obtener/Actualizar Placa
  getVehiclePlate(): string {
    const data = this.getVehicleData();

    return data ? data.placa : '';
  }

  setVehiclePlate(plate: string, noplate: boolean): void {
    const vehicle = this.newVehicle();
    vehicle.placa = plate;
    vehicle.placaEnTramite = noplate;
    this.setVehicleData(vehicle);
  }

  // Requiere Inspeccion
  requiresInspection(): boolean {
    const data = this.getVehicleData();

    return data ? data.requiereInspeccion : false;
  }

  selfInspection(): boolean {
    const data = this.getVehicleData();

    return data ? data.requiereInspeccion && data.puedeAutoInspeccionar : false;
  }

  isVehicleDataCompleted(): boolean {
    const vehicle = this.getVehicleData();

    return !!(
      vehicle &&
      vehicle.numeroChasis &&
      vehicle.numeroMotor &&
      vehicle.cantidadAccesorios >= 0 &&
      vehicle.codigoColor
    );
  }

  // Guardar Vehiculo Registrado
  setVehicleRegistred(data: IVerifyPlateResponse): void {
    const vehicle = this.newVehicle();
    vehicle.fechaVigencia = data.fechaVigencia;
    vehicle.flagEstadoPoliza = data.flagEstadoPoliza;
    vehicle.placa = data.placa;
    vehicle.placaEnTramite = false;
    vehicle.codigoMarca = data.identificadorMarca;
    vehicle.marca = data.nombreMarca;
    vehicle.codigoModelo = data.identificadorModelo;
    vehicle.modelo = data.nombreModelo;
    vehicle.anio = data.anioFabricacion;
    vehicle.codigoTipoVehiculo = data.identificadorTipoVehiculo;
    vehicle.tipoVehiculo = data.nombreTipoVehiculo;
    vehicle.codigoMonedaValorSugerido = data.codigoMoneda;
    vehicle.valorSugerido = data.montoValorSugerido;
    vehicle.valorMinimo = data.montoValorMinimo;
    vehicle.valorMaximo = data.montoValorMaximo;
    vehicle.codigoUso = data.identificadorUsoVehiculo;
    vehicle.uso = data.nombreUsoVehiculo;
    vehicle.requiereGPS = data.requiereGPS;
    this.setVehicleData(vehicle);
  }

  // Inicializa data
  newVehicle(): IClientVehicleDetail {
    return {
      placa: null,
      placaEnTramite: null,
      marca: null,
      modelo: null,
      anio: null,
      numeroChasis: null,
      numeroMotor: null,
      valorSugerido: null,
      codigoMonedaValorSugerido: null,
      color: null,
      tipoVehiculo: null,
      uso: null,
      accesorios: null,
      es0Km: false,
      codigoMarca: null,
      codigoModelo: null,
      codigoTipoVehiculo: null,
      codigoUso: null,
      valorMinimo: null,
      valorMaximo: null,
      requiereGPS: null,
      flagEstadoPoliza: null,
      fechaVigencia: null
    };
  }

  clearDataVehicleBuyStep1(): void {
    this.clearColors();
    const data = this.getVehicleData();
    if (data) {
      delete data.numeroMotor;
      delete data.numeroChasis;
      delete data.color;
      delete data.codigoColor;
      delete data.cantidadAccesorios;
      this.setVehicleData(data);
    }
  }

  clearVehicleData(): void {
    this.storageService.removeItem(KEYS.VEHICLE);
  }

  // Colores
  getColors(): Array<IGetColorsResponse> {
    return this.storageService.getStorage(KEYS.COLORS);
  }

  setColors(data: Array<IGetColorsResponse>): void {
    this.storageService.saveStorage(KEYS.COLORS, data);
  }

  clearColors(): void {
    this.storageService.removeItem(KEYS.COLORS);
  }

  /*********************
   * Datos para Cuadro
   *********************/
  // Header
  getHeaderData(): Array<any> {
    return this.storageService.getStorage(KEYS.HEADER);
  }

  setHeaderData(data: Array<any>): void {
    this.storageService.saveStorage(KEYS.HEADER, data);
  }

  clearHeaderData(): void {
    this.storageService.removeItem(KEYS.HEADER);
  }

  // Detalle
  getDetailsData(): any {
    return this.storageService.getStorage(KEYS.DETAILS);
  }

  setDetailsData(data: any): void {
    this.storageService.saveStorage(KEYS.DETAILS, data);
  }

  clearDetailsData(): void {
    this.storageService.removeItem(KEYS.DETAILS);
  }

  /************************
   * Seleccion del Cuadro
   ************************/
  getChoiseData(): IChoiseTime {
    return this.storageService.getStorage(KEYS.CHOISE);
  }

  setChoiseData(data: IChoiseTime): void {
    this.storageService.saveStorage(KEYS.CHOISE, data);
  }

  clearChoiseData(): void {
    this.storageService.removeItem(KEYS.CHOISE);
  }

  clearAllQuoteData(): void {
    this.clearHeaderData();
    this.clearDetailsData();
    this.clearVehicleData();
    this.clearChoiseData();
    this.clearColors();
    this.clearCorrespondenceAddress();
    this.clearInspectionConditions();
    this.clearPersonalDataClause();
    this.setCurrentStep(1);
  }

  /*******************
   * Paso de Compra
   *******************/
  setCurrentStep(step: number): void {
    this.currentStep = step;
    this.stepSubject.next(step);
  }

  getCurrentStep(): number {
    return this.currentStep;
  }

  getCurrentStepSubject(): Observable<number> {
    return this.stepSubject.asObservable();
  }

  getLastStep(): number {
    const stepList = VEHICLES_QUOTE_BUY_STEP;

    return stepList[stepList.length - 1].value;
  }

  /******************
   * Datos comunes
   ******************/
  // Clausula datos personales
  getPersonalDataClause(): string {
    return this.storageService.getStorage(KEYS.PERSONAL_DATA_CLAUSE, false);
  }

  setPersonalDataClause(data: string): void {
    this.storageService.saveStorage(KEYS.PERSONAL_DATA_CLAUSE, data, false);
  }

  clearPersonalDataClause(): void {
    this.storageService.removeItem(KEYS.PERSONAL_DATA_CLAUSE);
  }

  // Direccion de Correspondencia para Contratante e Inspeccion
  getCorrespondenceAddress(): IClientAddressResponse {
    return this.storageService.getStorage(KEYS.ADDRESS);
  }

  setCorrespondenceAddress(data: IClientAddressResponse): void {
    this.storageService.saveStorage(KEYS.ADDRESS, data);
  }

  clearCorrespondenceAddress(): void {
    this.storageService.removeItem(KEYS.ADDRESS);
  }

  // Condicion de Inspeccion
  getInspectionConditions(): string {
    return this.storageService.getStorage(KEYS.INSPECTION_CONDITIONS, false);
  }

  setInspectionConditions(data: string): void {
    this.storageService.saveStorage(KEYS.INSPECTION_CONDITIONS, data, false);
  }

  clearInspectionConditions(): void {
    this.storageService.removeItem(KEYS.INSPECTION_CONDITIONS);
  }
}
