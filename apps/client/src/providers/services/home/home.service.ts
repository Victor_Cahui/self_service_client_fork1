import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiService } from '@mx/core/shared/helpers/api';
import { SkipInsterceptorJsonHeader } from '@mx/core/shared/helpers/interceptors/content-type-json-interceptor';
import { HomeEndpoint } from '@mx/endpoints/home.endpoint';
import { IlistIncidentHoursRequest } from '@mx/statemanagement/models/general.models';
import {
  IHomeCoverageDescriptionResponse,
  IHomeCoverageRequest,
  IHomeCoverageResponse,
  IHomeCoverageSublimitResponse,
  IHomeDeductiblesResponse,
  IListHoursResponse,
  IListIncidentTypesRequest,
  IListIncidentTypesResponse,
  IRegisterIncidentBody,
  IRegisterIncidentResponse
} from '@mx/statemanagement/models/home.interface';
import {
  ICompareDetailsHomeResponse,
  ICompareHeaderHomeResponse,
  ICoveragesPolicyRequest
} from '@mx/statemanagement/models/policy.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  constructor(private readonly apiService: ApiService) {}
  ListIncidentHours(listIncidentHours: IlistIncidentHoursRequest): Observable<Array<IListHoursResponse>> {
    return this.apiService.get(HomeEndpoint.ListIncidentHours, { params: listIncidentHours, default: [] });
  }

  ListIncidentTypes(listIncidentTypes: IListIncidentTypesRequest): Observable<Array<IListIncidentTypesResponse>> {
    return this.apiService.get(HomeEndpoint.ListIncidentTypes, { params: listIncidentTypes, default: [] });
  }

  RegisterIncident(query: any, body: IRegisterIncidentBody): Observable<IRegisterIncidentResponse> {
    let headers = new HttpHeaders();
    headers = headers.set(SkipInsterceptorJsonHeader, '');

    return this.apiService.postOnlyFormData(HomeEndpoint.RegisterIncident, body, {
      params: query,
      headers,
      default: {}
    });
  }

  getCoverages(homeCoverageRequest: IHomeCoverageRequest): Observable<Array<IHomeCoverageResponse>> {
    return this.apiService.get(HomeEndpoint.ListCoverage, { params: homeCoverageRequest, default: [] });
  }

  getDeductibles(homeCoverageRequest: IHomeCoverageRequest): Observable<Array<IHomeDeductiblesResponse>> {
    return this.apiService.get(HomeEndpoint.ListDeductibles, { params: homeCoverageRequest, default: [] });
  }

  getCoveragesDecriptions(
    homeCoverageRequest: IHomeCoverageRequest
  ): Observable<Array<IHomeCoverageDescriptionResponse>> {
    return this.apiService.get(HomeEndpoint.ListDeductibleDescriptions, { params: homeCoverageRequest, default: [] });
  }

  getSublimits(homeCoverageRequest: IHomeCoverageRequest): Observable<Array<IHomeCoverageSublimitResponse>> {
    return this.apiService.get(HomeEndpoint.ListSublimits, { params: homeCoverageRequest, default: [] });
  }

  // Coberturas para cotizacion
  getCompareHeader(data: ICoveragesPolicyRequest): Observable<Array<ICompareHeaderHomeResponse>> {
    return this.apiService.get(HomeEndpoint.PoliciesCompareHeader, { params: data, default: [] });
  }
  getCompareDetails(data: ICoveragesPolicyRequest): Observable<ICompareDetailsHomeResponse> {
    return this.apiService.get(HomeEndpoint.PoliciesCompareDetails, { params: data, default: {} });
  }
}
