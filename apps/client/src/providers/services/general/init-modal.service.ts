// tslint:disable: invalid-void
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InitModalService {
  private readonly holdCLoseModal$: Subject<void>;
  constructor() {
    this.holdCLoseModal$ = new Subject<void>();
  }

  getCloseModalEvent(): Subject<void> {
    return this.holdCLoseModal$;
  }

  emitCloseModalEvent(): void {
    this.getCloseModalEvent().next();
  }
}
