import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject, timer } from 'rxjs';

export interface IHeader {
  title: string;
  subTitle: string;
  icon?: string;
  back: boolean;
  url?: string;
  routeback?: string;
}

@Injectable({
  providedIn: 'root'
})
export class HeaderHelperService {
  header: BehaviorSubject<IHeader>;
  tab: Subject<boolean>;
  constructor() {
    this.tab = new Subject<boolean>();
    this.header = new BehaviorSubject<IHeader>({
      title: '',
      subTitle: '',
      icon: '',
      back: false
    });
  }

  get(): BehaviorSubject<IHeader> {
    return this.header;
  }

  set(newHeader: IHeader): void {
    const timer$ = timer(0).subscribe(() => {
      this.get().next(newHeader);
      timer$.unsubscribe();
    });
  }

  getTab(): Subject<boolean> {
    return this.tab;
  }

  setTab(exist: boolean): void {
    const timer$ = timer(0).subscribe(() => {
      this.getTab().next(exist);
      timer$.unsubscribe();
    });
  }
}
