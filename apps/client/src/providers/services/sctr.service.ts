import { Injectable } from '@angular/core';
import { ApiService } from '@mx/core/shared/helpers/api';
import { SctrEndpoint } from '@mx/endpoints/sctr.endpoint';
import {
  IInsuredByRecordRequest,
  IInsuredsRecordPaginatorResponse,
  IRecordDownloadRequest,
  IRecordsPaginatorResponse,
  IRecordsRequest
} from '@mx/statemanagement/models/sctr.interface';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SctrService {
  insuredConstancyList: Array<string> = [];
  insuredConstancyListRx: BehaviorSubject<Array<string>> = new BehaviorSubject<Array<string>>([]);

  constructor(private readonly apiService: ApiService) {}

  getInsuredsByRecord(params: IInsuredByRecordRequest): Observable<IInsuredsRecordPaginatorResponse> {
    return this.apiService.get(`${SctrEndpoint.GetInsuredsByRecord}`, { params, default: {} });
  }

  getRecords(params: IRecordsRequest): Observable<IRecordsPaginatorResponse> {
    return this.apiService.get(`${SctrEndpoint.GetRecords}`, { params, default: {} });
  }

  downloadRecords(params: IRecordDownloadRequest): Observable<{ base64: string }> {
    return this.apiService.get(`${SctrEndpoint.DownloadRecord}`, { params, default: {} });
  }

  uploadPayroll(params: any, body: any): Observable<any> {
    return this.apiService.requestFormData('POST', SctrEndpoint.UploadPayroll, body, {
      params,
      default: {},
      reportProgress: true
    });
  }

  addInsuredConstancyList(insuredId: string): void {
    this.insuredConstancyList.push(insuredId);
    this.insuredConstancyListRx.next(this.insuredConstancyList);
  }

  removeInsuredConstancyList(insuredId: string): void {
    const index = this.insuredConstancyList.indexOf(insuredId);
    index > -1 && this.insuredConstancyList.splice(index, 1);
    this.insuredConstancyListRx.next(this.insuredConstancyList);
  }

  clearInsuredConstancyList(): void {
    this.insuredConstancyList = [];
    this.insuredConstancyListRx.next(this.insuredConstancyList);
  }

  getInsuredConstancyList(): Array<string> {
    return this.insuredConstancyList;
  }

  getInsuredConstancyListRx(): BehaviorSubject<Array<string>> {
    return this.insuredConstancyListRx;
  }
}
