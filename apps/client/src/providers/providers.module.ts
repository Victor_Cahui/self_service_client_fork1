import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ServicesModule } from '@mx/core/shared/common/services/services-module';
import { ApiModule, NotificationModule } from '@mx/core/shared/helpers';
import { AuthGuard } from '@mx/guards/auth.guard';
import { DeclareSinesterDeactivateGuard } from '@mx/guards/declare-sinester/declare-sinester-deactivate.guard';
import { DeclareSinesterFinalizeGuard } from '@mx/guards/declare-sinester/declare-sinester-finalize.guard';
import { DeclareSinesterStepFourGuard } from '@mx/guards/declare-sinester/declare-sinester-step-four.guard';
import { DeclareSinesterStepThreeGuard } from '@mx/guards/declare-sinester/declare-sinester-step-three.guard';
import { DeclareSinesterStepTwoGuard } from '@mx/guards/declare-sinester/declare-sinester-step-two.guard';
import { DeclareSinesterStepGuard } from '@mx/guards/declare-sinester/declare-sinister-step.guard';
import { InsureDependentEditDeactivateGuard } from '@mx/guards/insure-dependent-edit-deactivate.guard';
import { InsureDependentEditGuard } from '@mx/guards/insure-dependent-edit.guard';
import { InsureDependentEditGuardStep12 } from '@mx/guards/insure-dependent-edit.guard-step-12.guard';
import { InsureDependentEditGuardStep2 } from '@mx/guards/insure-dependent-edit.guard-step-2.guard';
import { InsureDependentEditGuardStep3 } from '@mx/guards/insure-dependent-edit.guard-step-3.guard';
import { LoginGuard } from '@mx/guards/login.guard';
import { WelcomeGuard } from '@mx/guards/welcome.guard';
import { AuthService } from '@mx/services/auth/auth.service';
import { UserAdminService } from '@mx/services/auth/user-admin.service';
import { ClientService } from '@mx/services/client.service';
import { ConfigurationService } from '@mx/services/general/configuration.service';
import { GeneralService } from '@mx/services/general/general.service';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { InitModalService } from '@mx/services/general/init-modal.service';
import { InsuredDependentEditService } from '@mx/services/health/insured-dependent-edit.service';
import { DeclareSinisterHomeService } from '@mx/services/home/declare-sinister-home.service';
import { HomeService } from '@mx/services/home/home.service';
import { NotificactionsService } from '@mx/services/notifications/notifications.service';
import { OfficeService } from '@mx/services/office.service';
import { PoliciesService } from '@mx/services/policies.service';
import { ProcessOngoingService } from '@mx/services/processongoing.service';
import { SctrService } from '@mx/services/sctr.service';
import { HealthRefundStepDontFinishGuard } from './guards/health/health-refund-step-dont-finish.guard';
import { HealthRefundStepFinishGuard } from './guards/health/health-refund-step-finish.guard';
import { HealthRefundStepThreeGuard } from './guards/health/health-refund-step-three.guard';
import { HealthRefundStepTwoGuard } from './guards/health/health-refund-step-two.guard';
import { HealthRefundGuard } from './guards/health/health-refund.guard';
import { PaymentQuoteConfirmGuard } from './guards/payments/payment-quote-confrm.guard';
import { PaymentQuoteGuard } from './guards/payments/payment-quote.guard';
import { SctrInsuranceCoverageRedirectionGuard } from './guards/sctr-insurance-coverage.guard';
import { SessionGuard } from './guards/session.guard';
import { UserAdminGuard } from './guards/user-admin.guard';
import { VehicleBuyConfirmGuard } from './guards/vehicle/vehicle-buy-confirm.guard';
import { VehicleBuyStepFourGuard } from './guards/vehicle/vehicle-buy-step-four.guard';
import { VehicleBuyStepThreeGuard } from './guards/vehicle/vehicle-buy-step-three.guard';
import { VehicleBuyStepTwoGuard } from './guards/vehicle/vehicle-buy-step-two.guard';
import { VehicleBuyGuard } from './guards/vehicle/vehicle-buy.guard';
import { VehicleQuoteDetail2Guard } from './guards/vehicle/vehicle-quote-detail-2.guard';
import { VehicleQuoteDetailGuard } from './guards/vehicle/vehicle-quote-detail.guard';
import { HttpInterceptorProviders } from './interceptors/http-interceptor.providers';

@NgModule({
  imports: [CommonModule, ApiModule, NotificationModule, ServicesModule],
  declarations: [],
  providers: [
    AuthService,
    UserAdminService,
    UserAdminGuard,
    PoliciesService,
    InitModalService,
    GeneralService,
    HttpInterceptorProviders,

    AuthGuard,
    SessionGuard,
    LoginGuard,
    WelcomeGuard,

    ProcessOngoingService,
    ConfigurationService,
    ClientService,
    HomeService,
    NotificactionsService,
    HeaderHelperService,
    DeclareSinisterHomeService,
    InsuredDependentEditService,
    SctrService,
    OfficeService,

    InsureDependentEditDeactivateGuard,
    InsureDependentEditGuard,
    InsureDependentEditGuardStep12,
    InsureDependentEditGuardStep2,
    InsureDependentEditGuardStep3,

    DeclareSinesterStepTwoGuard,
    DeclareSinesterStepThreeGuard,
    DeclareSinesterStepFourGuard,
    DeclareSinesterFinalizeGuard,
    DeclareSinesterStepGuard,
    DeclareSinesterDeactivateGuard,

    PaymentQuoteGuard,
    PaymentQuoteConfirmGuard,

    VehicleBuyGuard,
    VehicleBuyStepTwoGuard,
    VehicleBuyStepThreeGuard,
    VehicleBuyStepFourGuard,
    VehicleBuyConfirmGuard,

    VehicleQuoteDetailGuard,
    VehicleQuoteDetail2Guard,

    HealthRefundGuard,
    HealthRefundStepTwoGuard,
    HealthRefundStepThreeGuard,
    HealthRefundStepFinishGuard,
    HealthRefundStepDontFinishGuard,

    SctrInsuranceCoverageRedirectionGuard
  ]
})
export class ProvidersModule {}
