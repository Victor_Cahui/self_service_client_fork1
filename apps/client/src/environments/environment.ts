import { appConstants } from '@mx/core/shared/providers/constants';
import { envBase } from './environment.base';
import { decorateEnvironment } from './environment.utils';

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
export const environment: any = {
  ...envBase,
  API_URL_COMUN: `${appConstants.apis.comun}/mapfrePeru/utilitarios/v2/`,
  OIM_LOGIN_URL: `http://10.160.120.216/login/#/`,
  production: false,
  WEB_OIM_URL: 'http://dev.oim.mapfre.com.pe:6777',
  lyraPublicKeySoles: '79568046:testpublickey_f0jGS6Bmz1ODPJTyg7Q26UD7nQo52MEYv0gR2jNteruVQ',
  lyraPublicKeyDolares: '79568046:testpublickey_f0jGS6Bmz1ODPJTyg7Q26UD7nQo52MEYv0gR2jNteruVQ',
  urlWebSocketApi: `${appConstants.apis.autoservicios}/websockets`
};

decorateEnvironment(environment);

// tslint:disable-next-line: comment-type
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
