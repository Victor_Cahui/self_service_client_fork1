import { envBase } from './environment.base';
import { decorateEnvironment } from './environment.utils';

export const environment: any = {
  // API_URL_AUTH: 'http://10.160.120.214/oim_new_login/api/',
  // WEB_OIM_URL: 'http://10.160.120.216/login/#/',
  ...envBase,
  WEB_OIM_URL: 'http://10.160.120.216',
  // lyraPublicKey: '79568046:testpublickey_f0jGS6Bmz1ODPJTyg7Q26UD7nQo52MEYv0gR2jNteruVQ'
  lyraPublicKeySoles: '79568046:testpublickey_f0jGS6Bmz1ODPJTyg7Q26UD7nQo52MEYv0gR2jNteruVQ',
  lyraPublicKeyDolares: '79568046:testpublickey_f0jGS6Bmz1ODPJTyg7Q26UD7nQo52MEYv0gR2jNteruVQ',
  urlWebSocketApi: `https://api.pre.mapfreperu.com/asoim/v2/websockets`
};

decorateEnvironment(environment);
