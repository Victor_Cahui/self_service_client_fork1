import { DOCUMENT } from '@angular/common';
import { Component, HostListener, Inject, OnDestroy, OnInit, Renderer2, ViewChild } from '@angular/core';
import { Meta } from '@angular/platform-browser';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { NotificationService } from '@mx/core/shared/helpers/notification/service/notification.service';
import { LocalStorageService } from '@mx/core/shared/helpers/util/local-storage-manager';
import { PlatformService } from '@mx/core/shared/helpers/util/platform.service';
import { MfModalAlertComponent } from '@mx/core/ui/lib/components/modals/modal-alert/modal-alert.component';
import { environment } from '@mx/environments/environment';
import { HeaderHelperService } from '@mx/services/general/header-helper.service';
import { InitModalService } from '@mx/services/general/init-modal.service';
import { HealthRefundService } from '@mx/services/health/health-refund.service';
import { PaymentService } from '@mx/services/payment.service';
import { ClassDevice, ClassNavigator, IClassNavigator } from '@mx/settings/constants/class-navigator';
import { ERROR_TYPES, HTTP_CODE_GATEWAY_TIMEOUT } from '@mx/settings/constants/errors-values';
import * as GlobalHeaders from '@mx/settings/constants/router-titles';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { IErrorModal } from '@mx/statemanagement/models/general.models';
import { Subject, Subscription } from 'rxjs';
import { delay, filter } from 'rxjs/operators';

@Component({
  selector: 'client-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit, OnDestroy {
  @ViewChild(MfModalAlertComponent) modalError: MfModalAlertComponent;

  routerSub: Subscription;
  snipperSubjectSub: Subscription;
  errorHandlerSub: Subscription;
  snipperSubject: Subject<Boolean>;
  loader: boolean;

  descriptionLoader = GeneralLang.Messages.LoadingWithoutDots;
  btnConfirm = GeneralLang.Buttons.Confirm;
  errorModalContent: IErrorModal = {};

  // TODO: Ver el lugar idoneo para este Evento
  @HostListener('document:click', ['$event'])
  public onDocumentClick(event: any): void {
    const dropdownActive = document.querySelector('client-dropdown.active');
    if (dropdownActive) {
      const dropdownClic = event.target.closest('client-dropdown') as HTMLElement;
      if (dropdownClic !== dropdownActive) {
        this.renderer2.removeClass(dropdownActive, 'active');
      }
    }
  }

  constructor(
    private readonly router: Router,
    private readonly headerHelperService: HeaderHelperService,
    private readonly platformService: PlatformService,
    private readonly renderer2: Renderer2,
    private readonly notificationService: NotificationService,
    private readonly initModalService: InitModalService,
    private readonly paymentService: PaymentService,
    private readonly healthRefundService: HealthRefundService,
    private readonly meta: Meta,
    private readonly localStorage: LocalStorageService,
    @Inject(DOCUMENT) private readonly document: Document
  ) {
    this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe((ev: NavigationEnd) => {
      const url = `/${ev.url.split('/')[1]}`;
      let url_list: any[] = this.localStorage.getData('url_list');
      if (!url_list.includes(url)) {
        url_list = [];
        this.localStorage.setData('url_list', url_list);
      }
    });
  }

  ngOnInit(): void {
    this.handlerClass();
    this.loadingFullScreen();
    this.handlerRouterChange();
    this.handlerError();
    this.addMetadata();
  }

  ngOnDestroy(): void {
    if (this.routerSub) {
      this.routerSub.unsubscribe();
    }
    if (this.snipperSubjectSub) {
      this.snipperSubjectSub.unsubscribe();
    }
    if (this.errorHandlerSub) {
      this.errorHandlerSub.unsubscribe();
    }
  }

  handlerRouterChange(): void {
    const globalHeaders = Object.keys(GlobalHeaders).map(key => GlobalHeaders[key]);

    this.routerSub = this.router.events.subscribe((event: RouterEvent) => {
      if (event instanceof NavigationEnd) {
        const url = event.url.split('?')[0];
        const header = globalHeaders.find(_header => _header.url === url);
        if (header) {
          this.headerHelperService.set(header);
        } else if (url.startsWith('/payments')) {
          this.headerHelperService.set(GlobalHeaders.PAYMENTS_HEADER);
        }
        this.closeModalError();

        if (
          !url.startsWith('/vehicles/soat/renew') &&
          !url.startsWith('/payments/quota') &&
          !url.startsWith('/vehicles/quote/buy') &&
          !url.startsWith('/vehicles/quote/payment-confirm') &&
          this.paymentService.getCurrentQuote()
        ) {
          this.paymentService.clearCurrentQuote();
        }

        if (!url.startsWith('/health/refund/request')) {
          this.healthRefundService.setRefund(null);
        }
      }
    });
  }

  // Gestión de errores
  handlerError(): void {
    this.modalError.sizeClass = 'g-modal--medium';
    this.errorHandlerSub = this.notificationService.getNotifies().subscribe(error => {
      const currentError = this.notificationService.getLastError();
      if (currentError) {
        const errorData = currentError.data as any;
        const typeError = errorData.status || HTTP_CODE_GATEWAY_TIMEOUT;
        this.errorModalContent = ERROR_TYPES[typeError];
        if (this.errorModalContent) {
          if (errorData.error && errorData.error.codigoOperacion) {
            this.errorModalContent.operationCode = errorData.error.codigoOperacion;
          }
          if (errorData.status !== HTTP_CODE_GATEWAY_TIMEOUT) {
            if (errorData.error && errorData.error.mensaje) {
              this.errorModalContent.message = errorData.error.mensaje;
            }
          }
          this.modalError.open();
        }
      }
    });
  }

  closeModalError(): void {
    if (this.modalError.isOpen) {
      this.modalError.close();
    }
  }

  loadingFullScreen(): void {
    this.snipperSubject = this.notificationService.getSpinner();
    this.snipperSubjectSub = this.snipperSubject.pipe(delay(0)).subscribe((loader: boolean) => {
      this.loader = loader;
    });
  }

  closeModal(): void {
    this.initModalService.emitCloseModalEvent();
  }

  handlerClass(): void {
    const navigator = this.platformService.getPlarform();
    const device = this.platformService.getDevice();

    const classesNavigator = ClassNavigator.find(cn => cn.navigator === navigator);
    const classesDevice = ClassDevice.find(cn => cn.navigator === device);
    if (classesDevice) {
      this.handlerClassByType(classesDevice);
    }
    if (classesNavigator) {
      this.handlerClassByType(classesNavigator);
    }
  }

  private handlerClassByType(classes: IClassNavigator): void {
    const body = this.document.getElementsByTagName('body')[0];
    const html = this.document.getElementsByTagName('html')[0];
    if (classes) {
      (classes.body || []).forEach(cls => {
        this.renderer2.addClass(body, cls);
      });
      (classes.html || []).forEach(cls => {
        this.renderer2.addClass(html, cls);
      });
    }
  }

  private addMetadata(): void {
    this.meta.addTag({ name: 'version', content: environment.VERSION }, true);
  }
}
