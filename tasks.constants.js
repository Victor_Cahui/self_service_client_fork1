import argv from 'yargs';
import { getTime } from './core/bit/gulp/tasks/core';

export const BASECONFIG = {
  applicationName: 'AuWeb',
  env: argv.argv.env || 'prod'
};

const APPS = {
  // port 10
  autoservicios: {
    devMpf: 'http://autoservicio_ws_oim.ngrok.io/v2',
    local: 'http://127.0.0.1:10010',
    pre: 'https://api.pre.mapfre.com.pe/legacy/asoim/v2',
    prod: 'https://api.mapfre.com.pe/legacy/asoim/v2',
    trex: 'http://localhost:8091/v2'
  },
  // port 11
  seguridad: {
    devMpf: 'https://oim.mapfre.com.pe/oim_new_login',
    local: 'http://127.0.0.1:10011',
    pre: 'https://oim.pre.mapfre.com.pe/oim_new_login',
    prod: 'https://oim.mapfre.com.pe/oim_new_login',
    trex: 'https://oim.pre.mapfre.com.pe/oim_new_login'
  },
  // port 12
  sctr: {
    devMpf: 'http://74abfaaf.ngrok.io',
    local: 'http://127.0.0.1:10012',
    pre: 'http://74abfaaf.ngrok.io',
    prod: 'http://74abfaaf.ngrok.io',
    trex: 'http://74abfaaf.ngrok.io'
  },
  // port 13
  comun: {
    devMpf: 'http://autoservicio_ws_comun.ngrok.io/v2',
    local: 'http://127.0.0.1:10013',
    pre: 'https://api.pre.mapfre.com.pe/legacy/ascomun/v2',
    prod: 'https://api.mapfre.com.pe/legacy/ascomun/v2',
    trex: 'http://localhost:8095'
  }
};

export const DESCRIPTOR = {
  apps: APPS,
  descriptors: [
    {
      appName: 'Autoservicios',
      changeDomainServiceTo: {
        // listar los operationId a cambiar
        oim: ['obtenerModelos', 'obtenerMarcas']
      },
      doChangeDomainWhenEnvIs: ['devMpf', 'prod'],
      fileName: 'swagger_autoservicios',
      urlBase: `${APPS.autoservicios[BASECONFIG.env]}`
    },
    {
      appName: 'Comun',
      fileName: 'swagger_comun',
      formatPath: {
        pre: path => path.replace('/mapfrePeru/utilitarios/v2', ''),
        prod: path => path.replace('/mapfrePeru/utilitarios/v2', '')
      },
      urlBase: `${APPS.comun[BASECONFIG.env]}`
    },
    {
      appName: 'Sctr',
      fileName: 'swagger_SCTR',
      urlBase: `${APPS.sctr[BASECONFIG.env]}`
    },
    {
      appName: 'Seguridad',
      fileName: 'swagger_seguridad',
      urlBase: `${APPS.seguridad[BASECONFIG.env]}`
    }
  ],
  dest: './descriptors/',
  services: {
    barrelTpl: './core/bit/gulp/tasks/tpl/index-service.tpl',
    dest: '/core/shared/src/providers/services/proxy/',
    tpl: './tasks/tpl/services.tpl'
  },
  models: {
    barrelTpl: './core/bit/gulp/tasks/tpl/index-model.tpl',
    dest: '/core/shared/src/providers/models/proxy/',
    tpl: './tasks/tpl/model.tpl'
  },
  constants: {
    barrelTpl: './core/bit/gulp/tasks/tpl/index-constants.tpl', // me
    dest: '/core/shared/src/providers/constants/proxy/',
    tpl: './core/bit/gulp/tasks/tpl/constants.tpl'
  },
  projects: [
    {
      name: 'oim',
      env: {
        default: 'https://oim.mapfre.com.pe',
        devMpf: 'https://oim.mapfre.com.pe',
        devMx: 'https://oim.mapfre.com.pe',
        local: 'https://oim.mapfre.com.pe',
        prod: 'https://oim.mapfre.com.pe'
      }
    }
  ],
  relativePathToYaml: `../autoservicio_swagger/api/swagger/`
};

export const GITLOG = {
  dest: '/core/shared/src/providers/constants/proxy/',
  fileName: 'git-log.ts',
  tpl: './core/bit/gulp/tasks/tpl/git-log.tpl'
};

export const GITDESCRIBE = {
  dest: '/core/shared/src/providers/constants/proxy/',
  fileName: 'git-describe.ts',
  tpl: './core/bit/gulp/tasks/tpl/git-describe.tpl'
};

export const DEPLOYPC = {
  dest: '/core/shared/src/providers/constants/proxy/',
  fileName: 'deploy-pc.ts',
  tpl: './core/bit/gulp/tasks/tpl/deploy-pc.tpl',
  moreData: {}
};

export const FILETOZIP = {
  dest: './entregables/',
  files: ['./dist/client/**/*'],
  getName: () => `client.${BASECONFIG.env}.v${DEVINFO.moreData.version}.${getTime()}.zip`
};

const scssGeneral = {
  src: './src/**/*.s+(a|c)ss'
};

export const SASSLINT = {
  ...scssGeneral,
  options: {
    configFile: '.sass-lint.yml'
  }
};

export const STYLELINT = {
  ...scssGeneral,
  vendor: './src/css/vendor/**/*.{css,scss}'
};
