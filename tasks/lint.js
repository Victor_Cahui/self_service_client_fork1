var gulp = require('gulp');

var tslint = require('gulp-tslint'),
  gutil = require('gulp-util'),
  through = require('through'),
  argv = require('yargs').argv,
  plumber = require('gulp-plumber');

var CURRENT_TOTAL_ERRORS = argv.errorsAllowed;
var totalLintErrors = 0,
  reportLimit = argv.reportLimit || 10;

var onError = function(err) {
  gutil.log(err);
  this.emit('end');
};

gulp.task('tslint', done => {
  gulp
    .src([
      'src/app/**/*.ts',
      'apps/**/src/**/*.ts',
      'core/**/src/**/*.ts',
      '!apps/**/src/**/*.spec.ts',
      '!app/**/*.spec.ts',
      '!src/app/**/*.spec.ts',
      '!core/**/*.spec.ts',
      '!app*/**/*.spec.ts',
      '!**/client-e2e/**/*.ts',
      '!**/**/polyfills.ts'
    ])
    .pipe(plumber({ errorHandler: onError }))
    .pipe(
      tslint({
        formatter: 'stylish',
        configuration: 'tslint.json'
      })
    )
    .pipe(
      tslint.report({
        allowWarnings: true,
        summarizeFailureOutput: true,
        reportLimit: reportLimit,
        emitError: true
      })
    )
    .pipe(
      (function() {
        var hasError = false;
        return through(
          function(file) {
            totalLintErrors += file.tslint.errorCount;
            if (file.tslint.failureCount !== 0) {
              hasError = true;
            }
          },
          function() {
            if (hasError && totalLintErrors > CURRENT_TOTAL_ERRORS) {
              gutil.log(`[${gutil.colors.cyan('gulp-tslint')}]`, gutil.colors.red(`Total Errors: ${totalLintErrors}`));

              gutil.log(
                `[${gutil.colors.cyan('gulp-tslint')}]`,
                gutil.colors.yellow(`Current Allowed Limit is: ${CURRENT_TOTAL_ERRORS}`)
              );
              this.emit('error', new Error('gulp-tslint', 'Failed Tslint.'));
              done();
              process.exit(1);
            } else {
              done();
              this.emit('end');
            }
          }
        );
      })()
    );
});
