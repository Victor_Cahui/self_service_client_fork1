// tslint:disable

import { HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import * as model from '@mx/core/shared/providers/models';
import { ApiService } from '@mx/core/shared/helpers/api';
import { appConstants } from '@mx/core/shared/providers/constants';

interface ReqOptions {
  default?: any;
  headers?: HttpHeaders | {
    [header: string]: string | string[];
  };
  observe?: 'body'; // response, body
  params?: HttpParams | {
    [param: string]: string | string[];
  };
  preloader?: boolean;
  reportProgress?: boolean;
  responseType?: 'json';
  retry?: number;
  withCredentials?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class {{config.appName}} {
  constructor(private readonly _ApiService: ApiService) {}

  {{#data}}
  public {{operationId}}{{^operationId}}XXX{{/operationId}}(
    {{#hasPathReq}}
    pathReq: {{#parameters}}model.{{path.modelName}}{{/parameters}},
    {{/hasPathReq}}
    {{#hasQueryReq}}
    queryReq: {{#parameters}}model.{{query.modelName}}{{/parameters}},
    {{/hasQueryReq}}
    {{#hasBodyReq}}
    bodyReq{{#parameters}}{{^body.isRequired}}?{{/body.isRequired}}: model.{{body.modelName}}{{/parameters}},
    {{/hasBodyReq}}
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.{{httpMethod}}(
      `${ {{domainService}} }{{{path}}}`{{#hasBodyReq}}, model.{{#parameters}}{{body.modelName}}{{/parameters}}.create(bodyReq){{/hasBodyReq}}{{#hasQueryReq}}, { params: { ...model.{{#parameters}}{{query.modelName}}{{/parameters}}.create(queryReq) }, {{/hasQueryReq}}{{^hasQueryReq}}, { {{/hasQueryReq}} ...reqOptions }
    );
  }

  {{/data}}
}
