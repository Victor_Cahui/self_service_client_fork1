const { gitDescribeSync } = require('git-describe');
const { readFile, writeFileSync } = require('fs');
const handlebars = require('handlebars');

const gitInfo = gitDescribeSync();
const versionInfoJson = JSON.stringify(gitInfo, null, 2);

let data = {
  commit: gitInfo.suffix,
  gitdescribe: gitInfo
};


readFile('apps/client/templates/index.tpl.html', 'utf-8',
  function(error, source){
    handlebars.registerHelper('toJSON', (obj) => {
      return  JSON.stringify(obj, null, 2);
    });

    const template = handlebars.compile(source);
    const html = template(data);
    writeFileSync('apps/client/src/index.html', html);
  }
);

