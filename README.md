# SELF SERVICE - MAPFRE
## Instalar

- [Node](https://nodejs.org/dist/v8.11.4/node-v8.11.4-x64.msi) Version 8.11.4

Los siguiente 'npm global packages' **son opcionales** dado que toda ejecución es manejada por los npm scripts, y dado que estos paquetes son dependencies declaradas en el archivo package.json hace innecesaria su instalación global

> npm install -g typescript\
> npm install -g @angular/cli\
> npm install -g ts-node\
> npm install -g jasmine\
> npm install -g sass\
> npm install -g node-sass

- [NVM](https://github.com/creationix/nvm) (Node Version Manager)
- [Angular CLI](https://cli.angular.io/) version 6.0.0.

Una vez clonado el proyecto debera de instalarse sus dependencias ejecutando

```
npm install
o
npm i
```

## Development Process

We have integrated the Angular Development with the UX Design (Pattern Labs) in the same branch of development

Here the explanation of the following npm scripts

> npm start  

It will star the development server for the client application at the port 4200 from any client and open the index.html page with the default browser

> npm run start:dev

It will do the same as `npm start` but it will also start the patternlab server by calling the script `start:patternlab`

> npm run start:patternlab

It will only start the patternlab server at the port 3000 by calling the grunt task `patternlab:serve`

### Git Hooks
As part of the development there are 2 rules to consider before pushing a commit to the git repo

* Our code shouldn't have any tslint warning or error.
* Our commit message should follow the same naming convention that the Angular project follows

In order to compliance those rules we are using 'husky' and  we have implemented the following git hooks as npm scripts

```
"husky": {
    "hooks": {
      "commit-msg": "commitlint -E HUSKY_GIT_PARAMS",
      "pre-push": "run-s tslint build:prod",
      "pre-commit": "npm run tslint"
    }
  }
```

 - "commit-msg": We are using commitlint and @commitlint/config-conventional to compliance our policies for our commit messages. More details [here](COMMIT.md)

 - "pre-push": We use this hook before pushing our commist to the upstream git repo. In this case we are running tslint and building the app for production (```build:prod```), making sure that our code is compliance with Angular AOT rules.
 - "pre-commit": We are using gulp to run tslint rules before the commit got created. For this task, we are using gulp to run a customized verification

### Tslint

In order to enforce clean code practices we are using [codelyzer](http://codelyzer.com/), angular tslint rules [angular-tslint-rules](https://www.npmjs.com/package/angular-tslint-rules#installation), and [tslint-consistent-codestyle](https://github.com/ajafff/tslint-consistent-codestyle#readme). Those have been declared in the file [tslint.json](tslint.json)

```
  "rulesDirectory": [
    "node_modules/codelyzer"
  ],
  "extends": ["angular-tslint-rules", "tslint-consistent-codestyle"],
  "rules": {
    ....
  }
```

Additionally, we have updated some rules that we needed for our application in the same file

```
  "rules": {
    "prefer-function-over-method": false,
    "no-null-keyword": false,
    "no-unused": [true, "ignore-parameters"],
    "directive-selector": [
      false,
      "attribute",
      "app",
      "camelCase"
    ],
    "component-selector": [
      false,
      "element",
      "app",
      "kebab-case"
    ],
    ...
  }
```

For this intent, we have implemented these npm scripts:

- "tslint": this task will call the gulp tslint task that will receive 2 parameters: maximun number of tslint errors allowed before the task fails and the number of errors shows at the execution of this task,

- "tslint:fix": this task was created in order to autofix the tslint errors that tslint can fix by itself. In this case we are telling to work with the client workspace.

- "tslint:report": this task will generate an HTML report with all the tslint errors that were found. This task is implemented as node component where the file will be created at "/reports/tslint-report.html". 

## Build

Run `npm run build` or `npm run build:prod` to build the project. The build artifacts will be stored in the `dist/client` directory.

The most critical part of the building process is creating code for production with AOT (Ahead-Of-Time) compiler.

For this phase of the SLCD we have implemented these npm scripts

- "build:prod" : this task will use AOT compilation in all its sense.

- "build:dev": this task will compile the code with AOT but it will also attach the source map of the code to the process. Why? To allow debug the Typescript if needed when the code is compile with AOT. To enable this part we are using the attribute "sourceMap" of the angular.json file for the client project.

```
          "configurations": {
            "production": {
              "fileReplacements": [
                {
                  "replace": "apps/client/src/environments/environment.ts",
                  "with": "apps/client/src/environments/environment.prod.ts"
                }
              ],
              "optimization": true,
              "outputHashing": "all",
              "sourceMap": false,                   .... here is disabled for PROD
              "extractCss": true,
              "namedChunks": false,
              "aot": true,
              "extractLicenses": true,
              "vendorChunk": false,
              "buildOptimizer": true
            },
            "dev": {
              "optimization": true,
              "outputHashing": "all",
              "sourceMap": true,                   .... here is enabled for DEV
              "extractCss": true,
              "namedChunks": false,
              "aot": true,
              "extractLicenses": true,
              "vendorChunk": false,
              "buildOptimizer": true
            }
          }
        },
        "serve": {
```

It is recalled that we are using this last npm script as part of the bitbucket-pipelines.yml in the execution of the pipeline after every PR got merged in the develop branch.


## Code scaffolding

Ejecutar `ng generate component [component-name] --project [project-name]` para generar un nuevo componente. tambien puede utilizar `ng generate directive|pipe|service|class|guard|interface|enum|module`.

> ng generate componente my-component --project client

## Arquitectura de la Solución
![arquitecture](images_doc/arquitecture.png)

### Especificación de la arquitectura

Definición de cada espacio en la arquitectura de la solución para los proyectos

* Core
  * UI : Libreria de componentes
    * Style
    * Common
    * Lib
      * _Components_: Componentes comunes en todas las aplicaciones.
      * _Directives_: Funcionalidades de comportamiento.
      * Filters/Pipe: 
  * Shared: Funcionalidades compartida para las aplicaciones
    * Helpers
      * _Api_: La funcionalidad principal de del llamado a los servicios.
      * _Util_: Funcionalidades útiles como conversiones de texto, de fecha, etc.
      * _Functions_: Funcionalidades globales como un verificador de tamaño del navegador, etc.
      * _Notify_: Un notificador de mensajes (alert, warning, errors).
    * Common
      * _Interceptor_: Funcionalidades intermediarias en las peticiones, como agregar el valor de autorización a todas las solicitudes.
      * _Services_: Servicios genéricos en la aplicación, como guardar datos principales de la aplicación.
      * Events
* Apps
  * Main
    * _Pages_: Diseño de páginas como NotFound, NotAutorize, etc.
    * _Layouts_: Marcos de la aplicación (LoginLayout, MainLayout, BlankLayout, etc.). 
    * _Components_: Componentes de un proceso de negocio, por ejemplo, los datos de la persona, facturas de la persona, etc.
    * _Views_: Vistas generales de una pantalla, por ejemplo, el perfil.
      * _Routes_: Rutas hijas que puede tener una vista.
  * Providers
    * _Services_: Servicios para la validación de negocio y registro de datos.
    * _Guards_: Seguridad para las rutas, poder verificar si un usuario puede acceder o no a una determinada ruta, como las validaciones de roles.
    * _Endpoints_: Rutas de los servicios donde se solicitarán y enviarán datos para procesar el negocio con el Back-end.
  * Routes
  * Store/State Management: Manejar estados de los objetos.
    * _Models_: Representación de objetos/entidades
  * Settings
    * _Auth_: Parametros de seguridad
    * _Const/Enums_: Valores constantes o Enumeradores
    * _Lang_: Textos de la aplicación
  * Enviroment: Variables de publicación

------


## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
