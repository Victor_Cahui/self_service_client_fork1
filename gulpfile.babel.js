import gulp from 'gulp';
import postcss from 'gulp-postcss';
import syntax_scss from 'postcss-scss';
import stylelint from 'stylelint';
import path from 'path';
import chalk from 'chalk';
import symbols from 'log-symbols';
import _ from 'lodash';
import argv from 'yargs';
import requireDir from 'require-dir';
import rimraf from 'rimraf';
import doExec from 'child_process';

const imagemin = require('gulp-imagemin');

requireDir('./tasks');

// Used in reporterMessagesFormat as a regex
let oldCodeRegexTest = /old\/components\/(.+)/;
let myargs = argv.argv;

function reporterLogFrom(fromValue) {
  if (fromValue.charAt(0) === '<') {
    return fromValue;
  }
  return path.relative(process.cwd(), fromValue);
}

function reporterMessagesFormat(message, source) {
  // This is a regex test we do so that certain .scss/css in certain folders is considered
  // an error while old code we haven't migrated yet is just a warning
  var isOldCode = oldCodeRegexTest.test(source);
  var str = '';
  if (message.text) {
    // Just get the message text
    var regExp = /(.+)( \(.+\))/;
    var messageText = regExp.exec(message.text);
    // Get the column/line info
    if (message.line && message.column) {
      str += chalk.gray(message.line + ':' + message.column + ' ');
    }
    // Check whether we should call this an error or not
    if (message.severity === 'warning' && !isOldCode) {
      str += chalk.yellow(symbols.warning + ' Warning:');
    } else if (message.severity === 'error' || isOldCode) {
      str += chalk.red(symbols.error + ' Error:');
    }
    // Link to the rule so in apps like iTerm they can cmd click the link to open
    // to that rule in the browser
    str += ' ' + messageText[1];
    if (message.rule) {
      str += chalk.gray(' (' + chalk.blue('http://tiny.cc/pxoy7x/' + message.rule) + ')');
    }
  }
  return str;
}

gulp.task('lint:style', () => {
  var processors = [stylelint(), require('postcss-reporter')({ clearReportedMessages: true })];

  return gulp
    .src([
      'projects/self-service-lib/src/**/main/*.s+(a|c)ss',
      '!projects/self-service-lib/src/lib/01-basics/main/_bootstrap.scss'
    ])
    .pipe(postcss(processors, { syntax: syntax_scss }));
});

let totalStyleLintErrors = 0;

gulp.task('clean:dist', cb => {
  rimraf('./dist', cb);
});

gulp.task('resources:assets', () => {
  return gulp.src(['assets/**/*']).pipe(gulp.dest('dist/self-service-lib/main/'));
});

gulp.task('resources:images', () => {
  return gulp
    .src(['projects/self-service-lib/src/lib/01-basics/images/**/*'])
    .pipe(
      imagemin(
        [
          imagemin.gifsicle({ interlaced: true }),
          imagemin.jpegtran({ progressive: true }),
          imagemin.optipng({ optimizationLevel: 5 }),
          imagemin.svgo({
            plugins: [{ removeViewBox: true }, { cleanupIDs: false }]
          })
        ],
        { verbose: true }
      )
    )
    .pipe(gulp.dest('dist/self-service-lib/main/images/'));
});

gulp.task('resources:fonts', () => {
  return gulp
    .src(['projects/self-service-lib/src/lib/01-basics/fonts/**/*'])
    .pipe(gulp.dest('dist/self-service-lib/fonts/'));
});

gulp.task('lint:style:validate:noformat', () => {
  var processors = [
    stylelint(),
    require('postcss-reporter')({
      formatter: function(input) {
        // Custom formatter to display the warnings/errors
        var messages = input.messages;
        var output = '';

        // If there is a source, output it
        if (input.source) {
          output += chalk.underline(reporterLogFrom(input.source) + '\n');
        }

        // For each message (with content) loop the function `reporterMessagesFormat`, so lets customize how that output looks
        messages.forEach(function(message) {
          output += reporterMessagesFormat(message, input.source) + '\n';
        });

        totalStyleLintErrors += _.size(messages);
        // Ignore messages that are empty
        // if (!_.isEmpty(messages)) {
        //   return output;
        // }
        return '';
      },
      throwError: false,
      clearAllMessages: true
    })
  ];

  let stream = gulp
    .src([
      'projects/self-service-lib/src/**/main/*.s+(a|c)ss',
      '!projects/self-service-lib/src/lib/01-basics/main/_bootstrap.scss'
    ])
    .pipe(postcss(processors, { syntax: syntax_scss }));
  return stream;
});

gulp.task('lint:style:validate', gulp.series('lint:style:validate:noformat'), cb => {
  // https://github.com/stylelint/stylelint/issues/659
  // https://github.com/postcss/postcss-reporter

  if (totalStyleLintErrors > myargs.stylelinterrors) {
    console.log(
      chalk.red(
        `[Stylelint] Error: There are ${totalStyleLintErrors} errors, more than the allowed errors (${myargs.stylelinterrors})`
      )
    );
    process.exit(1);
  }

  // https://github.com/gulpjs/gulp/blob/master/docs/API.md#async-task-support
  cb();
});

gulp.task('resources', gulp.series('resources:fonts', 'resources:images', 'resources:assets'), cb => {
  cb();
});

requireDir('./core/bit/gulp/tasks', {
  recurse: true
});

gulp.task('prettier:proxies', done => {
  doExec.exec('npm run prettier-tslint:ts:proxies', function(err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    done(err);
  });
});

gulp.task('lint:scss', gulp.series('lint:sass', 'lint:stylelint'));

gulp.task(
  'getDescriptor',
  gulp.series('convertYamlToJson', 'wrServiceAndModelTemplates', 'wrIndexTemplates', 'wrConstants', 'prettier:proxies')
);

gulp.task('wrGitInfo', gulp.series('wrGitDescribe', 'wrGitLog'));
gulp.task('wrDevConstants', gulp.series('wrDeployPC', 'wrConstants'));
gulp.task('wrDevInfo', gulp.series('wrGitInfo', 'wrDevConstants'));
