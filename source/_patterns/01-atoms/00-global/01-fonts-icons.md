---
title: Ico Font
---
1. La fuente es creada usando [Icomoon](https://icomoon.io/app/#/).
2. Los archivos se guardan en la carpeta **fonts**.
3. El archivo **_'fonts/MFAutoservicio.json'_** contiene todos los íconos que puede ser importado en Icomoon y reconstruir la líbrería de íconos.