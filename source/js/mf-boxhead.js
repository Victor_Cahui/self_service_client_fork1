$(function() {
  $(".g-box-icons--menu").click(function(e){
      e.preventDefault();

      $(this).toggleClass("active");

      if($(this).hasClass("active")) {
        $(this).parent().next().slideDown();
      } else {
        $(this).parent().next().slideUp();
      }

  });

  $(document).mouseup(function(e) {
    var container = $(".g-box-icons--list, .g-box-icons--menu");

    if (!container.is(e.target) && container.has(e.target).length === 0) {
      $(".g-box-icons--list").slideUp();
      $(".g-box-icons--menu").removeClass("active");
    }
  });

});