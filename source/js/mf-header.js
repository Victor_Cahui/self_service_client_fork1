$(function() {

    $(window).bind('scroll', function () {
      if ($(".g-view-landing-health").length !== 0) {
        var div = $(".g-view-landing-health").offset().top - 150;

        if ($(window).scrollTop() > div) {
          $('.fixed--box').fadeIn();
        } else {
          $('.fixed--box').hide();
        }
      }
    });

    $(".g-top-scroll").click(function(e){
        e.preventDefault();
        $(".g-top-scroll").removeClass("g-font--Bold");

        $(this).addClass("g-font--Bold");
        $('html, body').animate({
            scrollTop: $(this).offset().top - 40
        }, 500);
        return false;
    });


    $(".header--box--icon").on("click", function (e) {
      e.preventDefault();
      $(this).toggleClass("active");

      if($(this).hasClass("active")) {
          $(this).next().addClass("header--box--active").slideDown();
      } else {
          $(this).next().removeClass("header--box--active").hide();
      }
    });

    function menuResize() {
        var windowsize = $(window).width();
        if (windowsize < 780) {
            $(".header--box--icon").removeClass("active");
            $(".header--box--phone").removeClass("header--box--active").removeAttr("style");
        }
    }

    menuResize();
    $(window).resize(menuResize);


});