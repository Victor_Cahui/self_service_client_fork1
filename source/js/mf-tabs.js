$(function() {

  var tab         = "[data-tab='container']",
      tab_menu    = "[data-tab='menu']",
      tab_select  = $("[data-tab='select']"),
      tab_item    = $("[data-tab='item']");

  function tab_update() {
    if ($(window).width() >= 780) {
      $(tab_menu).removeAttr("style");
    }
  }

  tab_update();
  $(window).resize(function() {
    tab_update();
  });

  tab_select.on("click", function(e) {
    e.preventDefault();

    $(this).toggleClass("active");
    $(this).next().slideDown();
  });

  $(tab_menu).each(function() {
    var first = $(this).find("div:first-child").text();

    tab_select.each(function() {
      $(this).find("div").text(first);
    });
  });

  $(tab_menu).find("div").on("click", function(e) {
    e.preventDefault();

    var text = $(this).text();

    $(this).parents(tab_menu).find("div").removeClass("active");
    $(this).addClass("active");

    tab_select.removeClass("active").find("div").text(text);

    if ($(window).width() <= 780) {
      $(tab_menu).slideUp();
    }

    $(this).parents(tab).find(tab_item).hide();
    $(this).parents(tab).find(tab_item).eq( $(this).index() ).show();
  });

});