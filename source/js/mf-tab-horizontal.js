$(function() {
  $(' [class^="tab-header"] > li ').on('click', function(e){
    e.preventDefault();
    $(this).siblings('li').removeClass('on');
    $(this).addClass('on');
    $(this).parent().siblings('.tab-container').children('.tab-content').removeClass('on');
    $(this).parent().siblings('.tab-container').children('.tab-content').eq( $(this).index() ).addClass('on');
  });
});