$(function() {

  $(".g-filterbox--click-search").on("click", function(e) {
    e.preventDefault();

    $(".g-filterbox--desktop").addClass("g-filterbox--show g-filterbox--search");
    $(".g-filterbox--desktop").show();
  });

  $(".g-filterbox--click-filter").on("click", function(e) {
    e.preventDefault();

    $(".g-filterbox--desktop").addClass("g-filterbox--show g-filterbox--options");
    $(".g-filterbox--desktop").show();
  });

  $(".g-filterbox--close").on("click", function(e) {
    e.preventDefault();

    $(".g-filterbox--desktop").removeClass("g-filterbox--show g-filterbox--search g-filterbox--options");
    $(".g-filterbox--desktop").hide();
  });

  function fn_filter_resize() {
    if ($(window).width() >= 991) {
      $(".g-filterbox--desktop").removeClass("g-filterbox--show g-filterbox--search g-filterbox--options");
      $(".g-filterbox--desktop").removeAttr("style");
    }
  }

  fn_filter_resize();
  $(window).resize(fn_filter_resize);

});