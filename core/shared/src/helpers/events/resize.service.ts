import { Injectable } from '@angular/core';
import { BehaviorSubject, fromEvent } from 'rxjs';

export const SCREEN_NAMES = {
  XS: 'xs',
  SM: 'sm',
  MD: 'md',
  LG: 'lg',
  XL: 'xl'
};

export const SCREEN_RESOLUTIONS: Array<IScreenResolutions> = [
  { type: SCREEN_NAMES.XS, min: 0, max: 479 },
  { type: SCREEN_NAMES.SM, min: 480, max: 767 },
  { type: SCREEN_NAMES.MD, min: 768, max: 991 },
  { type: SCREEN_NAMES.LG, min: 992, max: 1199 },
  { type: SCREEN_NAMES.XL, min: 1200, max: 5120 }
];

function _window(): any {
  return window;
}

@Injectable({
  providedIn: 'root'
})
export class ResizeService {
  currentWidth: number;
  currentHeigh: number;
  resizeEvent: BehaviorSubject<number>;
  constructor() {
    this.currentWidth = this.getNativeWindow().innerWidth;
    this.currentHeigh = this.getNativeWindow().innerHeight;
    this.resizeEvent = new BehaviorSubject<number>(this.currentWidth);
    fromEvent(this.getNativeWindow(), 'resize').subscribe((event: any) => {
      this.currentWidth = event.target.innerWidth;
      this.currentHeigh = event.target.innerHeight;
      this.resizeEvent.next(this.currentWidth);
    });
  }

  getNativeWindow(): any {
    return _window();
  }

  getEvent(): BehaviorSubject<number> {
    return this.resizeEvent;
  }

  is(screenType: string): boolean {
    return screenType === this.getScreenType();
  }

  isEqual(screenType: string, width: number): boolean {
    return screenType === this.getScreenType(width);
  }

  getScreenType(width?: number): string {
    if (!width) {
      // tslint:disable-next-line: no-parameter-reassignment
      width = this.currentWidth;
    }
    const screen = SCREEN_RESOLUTIONS.find(sr => sr.min <= width && width <= sr.max);

    return screen ? screen.type : '';
  }

  /**
   * @deprecated pendiente de eliminar
   */
  isMobile(width: number): boolean {
    return width < 767;
  }

  /**
   * @deprecated pendiente de eliminar
   */
  isTablet(width: number): boolean {
    return width > 767 && width < 991;
  }

  /**
   * @deprecated pendiente de eliminar
   */
  isDesktop(width: number): boolean {
    return width > 991;
  }

  /**
   * @deprecated pendiente de eliminar
   */
  isGreatherThanMd(width: number): boolean {
    return width > 767;
  }
}

export interface IScreenResolutions {
  type: string;
  min: number;
  max: number;
}
