// Validar que URL sea valida
// tslint:disable: prettier
export function validURL(url): boolean {
  const pattern = new RegExp(
    `^(https?://)(([\\w!~*\'().&=+$%-]+: )?` +
      `[\\w!~*\'().&=+$%-]+@)?(([0-9]{1,3}\\.){3}[0-9]{1,3}|([\\w!~*\'()-]+\\.)*([\\w^-][\\w-]{0,61})?` +
      `[\\w]\\.[a-z]{2,6})(:[0-9]{1,4})?((/*)|(/+[\\w!~*\'().;?:@&=+$,%#-]+)+/*)$`
  );

  return pattern.test(url);
}
