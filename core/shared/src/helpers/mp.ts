import { environment } from '@mx/environments/environment';

export function isVisibleForMP(betaKey: string, hasMPPermission: boolean): boolean {
  return environment[betaKey] && hasMPPermission;
}
