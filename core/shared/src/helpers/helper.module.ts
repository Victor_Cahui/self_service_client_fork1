import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ServicesModule } from '../common/services/services-module';
import { ApiService } from './api/api.service';

@NgModule({
  imports: [BrowserModule, HttpClientModule, ServicesModule],
  exports: [],
  declarations: [],
  providers: [
    // NotifyService,
    ApiService
  ]
})
export class HelperModule {}
