import { SimpleChanges } from '@angular/core';

// tslint:disable: no-for-in
export { ApiModule } from './api/api.module';
export { EventsModule } from './events/events.module';
export { FunctionsModule } from './functions/functions.module';
export { NotificationModule } from './notification/notification.module';

export type NumberOrString = number | string;

export function isFirstChange(objFrm: SimpleChanges): boolean {
  const arrInputsFrm = Object.keys(objFrm);

  return arrInputsFrm.reduce((acc, input: any) => acc && objFrm[input].firstChange, true);
}

export function hasPreviousValue(objFrm: SimpleChanges): boolean {
  const arrInputsFrm = Object.keys(objFrm);

  return arrInputsFrm.reduce((acc, input: any) => acc && !!objFrm[input].previousValue, true);
}

export function isPristine(changes: SimpleChanges): boolean {
  return isFirstChange(changes) && !hasPreviousValue(changes);
}

export function isObjEmpty(o: {}): boolean {
  return !(o && Object.keys(o).length);
}

export function everyPropsAreTruthy(o: {}): boolean {
  const keys = Object.keys(o);

  return keys.every(k => o[k]);
}

export const isDef = (v: any): boolean => v !== undefined && v !== null;
export const hasValue = (v: any): boolean => v !== undefined && v !== null && v !== '';

export function hasNotValidValue(v: any): boolean {
  return [undefined, null, NaN, ''].includes(v);
}

export function httpParamSerializerJQLike(data: {}): string {
  // tslint:disable-next-line: no-let
  let returnData = '';
  // tslint:disable-next-line: no-let
  let count = 0;
  // tslint:disable-next-line: no-loop-statement
  for (const i in data) {
    if (data.hasOwnProperty(i)) {
      if (count === 0) {
        returnData += `${i}=${encodeURIComponent(data[i])}`;
      } else {
        returnData += `&${i}=${encodeURIComponent(data[i])}`;
      }
      count++;
    }
  }

  return returnData;
}

interface LocalStore {
  // tslint:disable: prefer-method-signature
  readonly get: (key: string) => any;
  readonly remove: (key: string) => void;
  readonly set: (key: string, value: any) => void;
  // tslint:enable: prefer-method-signature
}

export function localStore(): LocalStore {
  const api = {
    get,
    remove,
    set
  };

  function _parserToString(val: any): string {
    return typeof val === 'string' ? val : JSON.stringify(val);
  }

  function get(key: string): any {
    return JSON.parse(window.localStorage.getItem(key));
  }

  function set(key: string, value: any): void {
    window.localStorage.setItem(key, _parserToString(value));
  }

  function remove(key: string): void {
    window.localStorage.removeItem(key);
  }

  return api;
}
