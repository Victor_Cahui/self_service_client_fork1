import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';
import { AuthService } from '../../common/services/auth.service';
import { VerifySessionService } from '../../common/services/verify-session.service';
import { SkipInsterceptorAuthHeader } from './auth-interceptor';

@Injectable()
export class SessionInterceptor implements HttpInterceptor {
  constructor(private readonly verifySessionService: VerifySessionService, private readonly authService: AuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      map(event => {
        // Validamos si token expiro
        const expired: boolean = !this.verifySessionService.verifyTimeToken();
        const authToken: string = this.authService.getAuthorizationToken();

        if (expired && authToken && !req.headers.has(SkipInsterceptorAuthHeader)) {
          // Generamos error
          throw new HttpErrorResponse({
            headers: req.headers,
            status: 401,
            url: req.url
          });
        }

        return event;
      })
    );
  }
}
