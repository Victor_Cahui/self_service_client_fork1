import { HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { finalize, tap } from 'rxjs/operators';
// import { NotificationService } from '../notification';

@Injectable()
export class LoggingInterceptor implements HttpInterceptor {
  // constructor(private notifyService: NotificationService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): any {
    const started = Date.now();
    let ok: string;

    // extend server response observable with logging
    return next.handle(req).pipe(
      tap(
        // Succeeds when there is a response; ignore other events
        event => (ok = event instanceof HttpResponse ? 'succeeded' : ''),
        // Operation failed; error is an HttpErrorResponse
        error => (ok = 'failed')
      ),
      // Log when response observable either completes or errors
      finalize(() => {
        const elapsed = Date.now() - started;
        // tslint:disable-next-line:no-unused
        const msg = `${req.method} "${req.urlWithParams}" ${ok} in ${elapsed} ms.`;
        // this.notifyService.consoleLog(msg);
      })
    );
  }
}
