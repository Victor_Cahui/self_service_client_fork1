import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

export const SkipInsterceptorJsonHeader = 'X-Skip-Json-Interceptor';

@Injectable()
export class ContentTypeJsonInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Se verifica si no se asigno la cabecera Content-Type
    if (!req.headers.has('Content-Type') && !req.headers.has(SkipInsterceptorJsonHeader)) {
      // Seteamos el valor del Content-Type para indicar que retorna un JSON
      const headers = req.headers.set('Content-Type', 'application/json');
      // req = req.clone({ headers: req.headers.set('Accept', 'application/json') });
      // Clonamos la solicitud y colocamos la nueva cabecera
      const jsonReq = req.clone({ headers });

      return next.handle(jsonReq);
    }

    return next.handle(req);
  }
}
