import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { AuthService } from '../../common/services/auth.service';

export const SkipInsterceptorAuthHeader = 'X-Skip-Auth-Interceptor';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private readonly auth: AuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Obtenemos el tocken autorizador desde el servicio
    const authToken: string = this.auth.getAuthorizationToken();
    // Basic / Bearer
    if (authToken && !req.headers.has(SkipInsterceptorAuthHeader)) {
      // Clonamos la solicitud y configuramos el nuevo encabezado de Autorización
      const authReq = req.clone({ setHeaders: { Authorization: `Bearer ${authToken}` } });

      // Enviamos la solicitud clonada con nuevo encabezado al siguiente controllador
      return next.handle(authReq);
    }

    return next.handle(req);
  }
}
