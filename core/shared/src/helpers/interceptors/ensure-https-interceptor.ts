import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class EnsureHttpsInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Clonamos la petición y reemplazamos 'http://' con 'https://' al mismo tiempo
    const secureReq = req.clone({
      url: req.url.replace('http://', 'https://')
    });

    // enviamos la solcitud clonada y segura al siguiente controlador
    return next.handle(secureReq);
  }
}
