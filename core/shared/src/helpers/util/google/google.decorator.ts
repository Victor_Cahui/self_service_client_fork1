import { GAService } from './google.analytics.service';
import { GoogleModule } from './google.module';

export function gaEventDecorator(): MethodDecorator {
  return (target: Object, key: string, descriptor: PropertyDescriptor) => {
    const originalMethod = descriptor.value; // save a reference to the original method

    descriptor.value = function(this: any, ...args: any[]): any {
      const gaService = GoogleModule.injector.get(GAService); // inject service
      this.ga = (this.ga || []).map(element => gaService.formatGaEvent(element));
      const result = originalMethod.apply(this, args);

      return result;
    };

    return descriptor;
  };
}
