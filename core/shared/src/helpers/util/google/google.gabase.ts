import { ElementRef, Input } from '@angular/core';
import { UnsubscribeOnDestroy } from '../unsubscribe-on-destroy';
import { GAService } from './google.analytics.service';
import { GA_EVENT_NAME } from './google.constant';
import { IGaPropertie } from './google.models';

export abstract class GaUnsubscribeBase extends UnsubscribeOnDestroy {
  private _ga: Array<IGaPropertie> = null;
  @Input()
  get ga(): Array<IGaPropertie> {
    return this._ga;
  }

  set ga(value: Array<IGaPropertie>) {
    this._ga = typeof value === 'string' ? JSON.parse(value) : value || [];
  }

  constructor(protected gaService?: GAService) {
    super();
  }

  onDomChange(_elementChange: ElementRef, data?: any): void {
    if (this.ga && this.ga.length) {
      const ctrl = _elementChange.nativeElement as HTMLElement;
      const ctrlName = _elementChange.nativeElement.getAttribute('ctrl');
      const gaCtrl = this.ga.filter(p => p.control === ctrlName);
      if (gaCtrl.length) {
        const isSelect = ctrl.constructor === HTMLSelectElement || ctrl.tagName === 'MF-SELECT';
        gaCtrl.forEach(ga => {
          const action = isSelect ? 'change' : ga.controlAction;
          ctrl.addEventListener(action, (event: Event) => {
            // const comp = (event.target as HTMLElement).closest('[keyc]');
            // if (event && event.target && event.target['nodeName'] === 'A' && event.target['target'] !== '_blank') {
            //   const el = event.target as HTMLAnchorElement;
            //   if (el.href) {
            //     event.preventDefault();
            //     setTimeout(() => {
            //       window.location.href = el.href;
            //     }, 500);
            //   }
            // }
            let dataTarget = {};
            if (event && event.target && ['INPUT', 'SELECT'].filter(e => e === event.target['nodeName']).length) {
              dataTarget = {
                value: event.target['value'],
                checked: event.target['checked']
              };

              if (event.target['nodeName'] === 'SELECT') {
                dataTarget['label'] = event.target['options'][event.target['selectedIndex']]['label'];
              }
            }

            let dataAttr = {};
            if (event && event.target && event.target['attributes'] && event.target['attributes'].length) {
              dataAttr = Object.values<{ name: string; value: string }>(event.target['attributes'])
                .filter(attr => attr.name.match(/^data-/))
                .reduce((pre, attr, index) => {
                  pre[attr.name.split('-')[1]] = attr.value;

                  return pre;
                }, {});
            }

            setTimeout(() => {
              const custom = new CustomEvent(GA_EVENT_NAME, {
                detail: {
                  ga: { ...ga, ...(this.ga.find(fv => fv.control === ga.control) || {}) },
                  event,
                  data: { ...dataTarget, ...dataAttr, ...(data || {}) }
                }
              });
              // document.dispatchEvent(custom);
              this.gaService.gaEvent(custom);
            }, 0);
          });
        });
      }
    }
  }

  addGAEvent(ga: IGaPropertie, data?: any): void {
    setTimeout(() => {
      const custom = new CustomEvent(GA_EVENT_NAME, {
        detail: {
          ga,
          data: { ...(data || {}) }
        }
      });
      this.gaService.gaEvent(custom);
    }, 0);
  }
}
