import { Directive, HostListener, Input } from '@angular/core';
import { GAService } from './google.analytics.service';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[gaClick]'
})
export class DatalayerClickDirective {
  constructor(public gaService: GAService) {}

  @Input() gaCategory?: string;
  @Input() gaAction?: string;
  @Input() gaLabel?: string;
  @Input() gaValue?: string;
  @Input() gaClick?: string;

  @HostListener('click', ['$event'])
  clickEvent($event: any): void {
    this.gaService.add({
      event: this.gaClick || 'ga_event',
      category: this.gaCategory,
      action: this.gaAction,
      label: this.gaLabel,
      value: this.gaValue
    });
  }
}
