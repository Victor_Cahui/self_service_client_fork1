export { gaEventDecorator } from './google.decorator';
export * from './datalayer-click.directive';
export * from './dom-change.directive';
export * from './google.analytics.service';
export * from './google.constant';
export * from './google.gabase';
export * from './google.models';
export * from './google.module';
