import { DOCUMENT } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { AuthService } from '@mx/services/auth/auth.service';
import { Md5 } from 'ts-md5/dist/md5';
import { StorageService } from '../storage-manager';
import { EventGoogleAnalytics, IEventCustomGoogleAnalytics, IGaPropertie } from './google.models';

declare let dataLayer: any;

// tslint:disable-next-line:only-arrow-functions
function _dataLayer(): any {
  return dataLayer || [];
}

@Injectable()
export class GAService {
  constructor(
    @Inject(DOCUMENT) private readonly document: Document,
    private readonly storageService: StorageService,
    private readonly authService: AuthService
  ) {}

  add(data: EventGoogleAnalytics): void {
    _dataLayer().push(data);
  }

  gaEvent(event: CustomEvent<IEventCustomGoogleAnalytics>): void {
    const ga = event.detail && event.detail.ga;
    if (ga) {
      const data = event.detail.data || {};

      const eventGa = new EventGoogleAnalytics({
        event: ga.event,
        category: this.getValues(ga.category, data),
        action: this.getValues(ga.action, data),
        label: this.getValues(ga.label, data),
        value: this.getValues(ga.value, data)
      });

      const dimension = this.getDimension(this.getValues(ga.dimentions, data));

      this.add({ ...eventGa, ...dimension });
    }
  }

  gaEvenCustom(event: EventGoogleAnalytics, data: any): void {
    const eventData = { ...event };
    this.recursive(eventData, data);

    this.add(eventData);
  }

  private recursive(propertie: any, data: any): any {
    if (Array.isArray(propertie)) {
      for (const p of propertie) {
        this.recursive(p, data);
      }
    } else {
      Object.keys(propertie).forEach(p => {
        if (typeof propertie[p] === 'object' && propertie[p]) {
          this.recursive(propertie[p], data);
        } else {
          if (propertie[p]) {
            propertie[p] = this.getValues(propertie[p], data);
          }
        }
      });
    }
  }

  private getValues(label: string, data?: any): string {
    let labelNew = label;
    if (label) {
      const existsGlobals = label.match(/{{g\.[a-zA-Z]+}}/gi);
      const existsData = label.match(/{{[a-zA-Z]+}}/gi);
      const existsQuerySelector = label.match(/{{qs\(.+?\)}}/gi);

      if (existsGlobals) {
        labelNew = this.replaceValues(labelNew, this.getGlobalValues());
      }

      if (existsData) {
        labelNew = this.replaceValues(labelNew, data);
      }

      if (existsQuerySelector) {
        labelNew = this.replaceValueQuerySelector(labelNew, existsQuerySelector);
      }
    }

    return labelNew;
  }

  private replaceValues(label: string, properties: any): string {
    let labelNew = label;
    Object.keys(properties).forEach(name => {
      if (typeof properties[name] === 'object' && properties[name]) {
        Object.keys(properties[name]).forEach(nameS => {
          labelNew = labelNew.replace(`{{${nameS}}}`, properties[name][nameS]);
        });
      } else {
        labelNew = labelNew.replace(`{{${name}}}`, properties[name]);
      }
    });

    return labelNew;
  }

  private replaceValueQuerySelector(value: string, existsQuerySelector: Array<string>): string {
    const TAG_PROPERTY = {
      INPUT: 'value',
      OPTION: 'text'
    };

    return existsQuerySelector.reduce((previous, current) => {
      const querySelector = current.match(/([^{{qs(].+)(?=[\)])/gi);
      const selectorElement = this.document.querySelector(querySelector ? querySelector[0] : null);
      let replaceValue = '';
      if (selectorElement) {
        if (selectorElement.tagName === 'SELECT') {
          replaceValue = (selectorElement as HTMLSelectElement).options[
            (selectorElement as HTMLSelectElement).selectedIndex
          ].text;
        } else {
          const property = TAG_PROPERTY[selectorElement.tagName] || 'textContent';
          replaceValue = (selectorElement[property] || '').trim();
        }
      }
      // tslint:disable-next-line: no-parameter-reassignment
      previous = previous.replace(current, replaceValue);

      return previous;
    }, value);
  }

  private getDimension(value: string): {} {
    if (!value) {
      return {};
    }

    const values = value.split(',');

    return values.reduce((previous, current) => {
      const currentSplit = current.split(':');
      previous[currentSplit[0]] = currentSplit[1];

      return previous;
    }, {});
  }

  private getGlobalValues(): {} {
    const referrer = document.referrer;
    const pathName = location.pathname;
    const urlPage = location.href;

    return {
      'g.url': urlPage,
      'g.path': pathName,
      'g.referrer': referrer,
      'g.section': this.getSection(),
      'g.subsection': this.getSubSection(),
      'g.userId': this.getUser(),
      'g.site': 'Mapfre Portal'
    };
  }

  getUser(): string {
    const md5 = new Md5();
    const auth = this.authService.retrieveEntity();
    const userId = auth.number || '';

    return userId
      ? md5
          .appendStr(userId)
          .end()
          .toString()
      : '';
  }

  getSection(): string {
    const qsSection = this.document.querySelector(
      'client-main-menu .g-menu--global a.active, client-main-menu .g-menu--global div.active'
    );

    return qsSection ? qsSection.textContent.trim() : 'HOME';
  }

  getSubSection(): string {
    const qsSubSection = this.document.querySelector('client-main-menu .g-menu--global div.active ~ div a.active');

    return qsSubSection ? qsSubSection.textContent.trim() : '';
  }

  formatGaEvent(ga: IGaPropertie): IGaPropertie {
    if (!ga) {
      return {};
    }

    return {
      ...ga,
      ...{
        category: this.getValues(ga.category, {}),
        action: this.getValues(ga.action, {}),
        label: this.getValues(ga.label, {}),
        value: this.getValues(ga.value, {})
      }
    } as IGaPropertie;
  }
}
