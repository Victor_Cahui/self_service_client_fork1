export class CardPayment {
  public static getCardInfo(card, typeCard): any {
    return {
      ownerName: card.clientAnswer.transactions[0].transactionDetails.cardDetails.cardHolderName,
      cardNumber: card.clientAnswer.transactions[0].transactionDetails.cardDetails.pan,
      cardExpiredMonth: '',
      cardExpiredYear: '',
      cvv: '',
      email: card.clientAnswer.customer.email,
      cardType: card.clientAnswer.transactions[0].transactionDetails.cardDetails.effectiveBrand,
      paymentMode: typeCard
    };
  }

  public static getCardInfoByToken(card, typeCard): any {
    return {
      cardNumber: card.numero,
      cardType: card.tipo,
      cardToken: card.tarjetaToken,
      paymentMode: typeCard
    };
  }
}
