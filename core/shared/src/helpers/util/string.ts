import { CurrencyPipe } from '@angular/common';
export class StringUtil {
  public static slugify(text: string): string {
    return text
      .toString()
      .toLowerCase()
      .replace(/\s+/g, '-') // Replace spaces with -
      .replace(/[^\w\-]+/g, '') // Remove all non-word chars
      .replace(/\-\-+/g, '-') // Replace multiple - with single -
      .replace(/^-+/, '') // Trim - from start of text
      .replace(/-+$/, ''); // Trim - from end of text
  }

  /** @ToDo este método puede mejroar, agregando afirmaciones como Si, No, True, Falso, etc. */
  public static stringToBoolean(k: string | boolean): boolean {
    return typeof k === 'string' ? k === 'true' : k;
  }

  public static replaceToDateFrom(word: string, from: number): string {
    return `${word.substring(0, from)} ...`;
  }

  public static getMoneyDescription(moneyCode: number): string {
    return moneyCode === 1 ? 'S/' : '$';
  }

  public static withoutDiacritics(text: string): string {
    // Symbol Ref => https://en.wikipedia.org/wiki/Combining_Diacritical_Marks
    return text.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
  }

  public static getMonthPaymentResume(currencyPipe: CurrencyPipe, amountPEN: number, amountUSD: number): string {
    const pen = `${currencyPipe.transform(amountPEN, `${StringUtil.getMoneyDescription(1)} `)}`;
    const usd = `${currencyPipe.transform(amountUSD, `${StringUtil.getMoneyDescription(2)} `)}`;

    return !!amountPEN && !!amountUSD ? `${pen} / ${usd}` : !!amountPEN ? `${pen}` : `${usd}`;
  }

  public static replaceInitials(name: string): string {
    let str = name.toLowerCase();
    str = str.replace(/(?:^|\s)\S/g, a => a.toUpperCase());
    const initials = [
      ' S.A.A.',
      ' S.A.A',
      ' SAA',
      ' S.A.C.',
      ' S.A.C',
      ' SAC',
      ' S.A.',
      ' S.A',
      ' SA',
      ' E.I.R.L.',
      ' E.I.R.L',
      ' EIRL',
      ' E.I.R.',
      ' E.I.R',
      ' EIR',
      ' E.I.R.LTDA.',
      ' E.I.R.LTDA'
    ];
    initials.forEach(initial => {
      str = str.toLowerCase().endsWith(initial.toLowerCase())
        ? `${str.substring(str.lastIndexOf(initial), str.length - initial.length)}${initial}`
        : str;
    });

    return str;
  }

  public static getClipboardText(event: any): string {
    const isIEOrEdge = /msie\s|trident\/|edge\//i.test(window.navigator.userAgent);
    const clipboard =
      event['clipboardData'] ||
      (event['originalEvent'] && event['originalEvent']['clipboardData']
        ? event['originalEvent']['clipboardData']
        : window['clipboardData']);
    const text = isIEOrEdge ? 'text' : 'text/plain';

    return clipboard.getData(text);
  }

  public static getCapitalizedText(text: string): string {
    return text.toLowerCase().replace(/\b\w/g, l => l.toUpperCase());
  }
}
