import { OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';

export abstract class UnsubscribeOnDestroy implements OnDestroy {
  unsubscribeDestroy$: Subject<boolean> = new Subject();
  // tslint:disable-next-line: readonly-array
  arrToDestroy: any[] = [];

  ngOnDestroy(): void {
    this.destroySubscriptions();
  }

  destroySubscriptions(): void {
    this.unsubscribeDestroy$.next(true);
    this.unsubscribeDestroy$.complete();

    Array.isArray(this.arrToDestroy) &&
      this.arrToDestroy.forEach(o => {
        o.unsubscribe && o.unsubscribe();
      });
  }
}
