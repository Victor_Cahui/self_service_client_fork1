export class ArrayUtil {
  public static arraySortByKey(array: Array<any>, key: string, order = 'ASC'): any {
    return array.sort((a, b) => {
      if (a[key] > b[key]) {
        return order === 'ASC' ? 1 : -1;
      }
      if (a[key] < b[key]) {
        return order === 'ASC' ? -1 : 1;
      }

      return 0;
    });
  }

  public static getTextfromList(list: Array<any>, value: any): string {
    // tslint:disable-next-line: no-parameter-reassignment
    value = value.toString();
    const name = list.find(v => v.value.toString() === value);

    return name ? name.text : '';
  }

  public static getValuefromList(list: Array<any>, text: string): string {
    return list.find(v => v.text === text).value;
  }

  public static customSort(list: Array<any>, sortOrder: Array<any>, key: string): Array<any> {
    const ordering = {};
    sortOrder.forEach((order, i) => (ordering[sortOrder[i]] = i));

    return list.sort((a, b) => {
      return ordering[a[key]] - ordering[b[key]];
    });
  }
}
