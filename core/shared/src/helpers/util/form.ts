import { ElementRef } from '@angular/core';

export class FormUtils {
  public static disabledClass(element: ElementRef, disabled: boolean): void {
    element.nativeElement.classList[disabled ? 'add' : 'remove']('disabled');
    element.nativeElement.disabled = disabled;
  }
}
