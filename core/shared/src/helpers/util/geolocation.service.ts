import { Injectable } from '@angular/core';
import { GMAPS_DIR } from '@mx/settings/constants/general-values';
import { ICoords } from '@mx/statemanagement/models/google-map.interface';
import { Observable } from 'rxjs';
declare var navigator;

@Injectable()
export class GeolocationService {
  private readonly options = {
    enableHighAccuracy: true,
    maximumAge: Infinity
  };

  getCurrentPosition(): Observable<Coordinates> {
    return Observable.create(observer => {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
          position => {
            observer.next(position.coords);
          },
          err => {
            observer.error(err);
          },
          this.options
        );
      } else {
        observer.error({ message: 'Dont support geolocation' });
      }
    });
  }

  hasPermission(): Promise<boolean> {
    try {
      return navigator.permissions.query({ name: 'geolocation' }).then(permission => permission.state === 'granted');
    } catch (error) {
      return Promise.resolve(false);
    }
  }

  drawRouteGM(from: ICoords, to: ICoords): void {
    const url = `${GMAPS_DIR}${from.latitude},${from.longitude}/${to.latitude},${to.longitude}`;
    window.open(url, '_blank');
  }
}
