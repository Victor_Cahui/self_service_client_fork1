import { isEmpty } from 'lodash-es';

export const monthNames = [
  'Enero',
  'Febrero',
  'Marzo',
  'Abril',
  'Mayo',
  'Junio',
  'Julio',
  'Agosto',
  'Setiembre',
  'Octubre',
  'Noviembre',
  'Diciembre'
];
export const monthsNamesAbrev = ['ENE', 'FEB', 'MAR', 'ABR', 'MAY', 'JUN', 'JUL', 'AGO', 'SET', 'OCT', 'NOV', 'DIC'];
export const dayNames = ['DOMINGO', 'LUNES', 'MARTES', 'MIÉRCOLES', 'JUEVES', 'VIERNES', 'SÁBADO'];

export const ddmmyyyy = /^([0-2][0-9]|3[0-1])(\/|-)(0[1-9]|1[0-2])\2(\d{4})$/;
export const ddmmyy = /^([0-2][0-9]|3[0-1])(\/|-)(0[1-9]|1[0-2])\2(\d{2})$/;
export const yyyymmdd = /^(\d{4})(\/|-)(0[1-9]|1[0-2])\2([0-2][0-9]|3[0-1])$/;
export const yymmdd = /^(\d{2})(\/|-)(0[1-9]|1[0-2])\2([0-2][0-9]|3[0-1])$/;

export const holidays = [
  { date: '2019-01-01 00:00:00', name: 'Año Nuevo' },
  { date: '2019-04-18 00:00:00', name: 'Jueves Santo' },
  { date: '2019-04-19 00:00:00', name: 'Viernes Santo' },
  { date: '2019-05-01 00:00:00', name: 'Día del Trabajo' },
  { date: '2019-06-29 00:00:00', name: 'San Pedro y San Pablo' },
  { date: '2019-07-28 00:00:00', name: 'Fiestas Patrias' },
  { date: '2019-07-29 00:00:00', name: 'Fiestas Patrias' },
  { date: '2019-08-30 00:00:00', name: 'Santa Rosa de Lima' },
  { date: '2019-10-08 00:00:00', name: 'Combate de Angamos' },
  { date: '2019-11-01 00:00:00', name: 'Todos los Santos' },
  { date: '2019-12-08 00:00:00', name: 'Inmaculada Concepción' },
  { date: '2019-12-25 00:00:00', name: 'Navidad' }
];

/**
 * Verificación de dos fechas para identificar si son iguales
 * @param date Fecha que se va a comparar
 * @param dateCompare Fecha que se utilizara como comparación
 * @returns TRUE si las fechas son iguales, de lo contrario FALSE
 */
export const IsSameDate = (date: Date, dateCompare: Date): boolean => {
  return (
    date &&
    date.getFullYear() === dateCompare.getFullYear() &&
    date.getMonth() === dateCompare.getMonth() &&
    date.getDate() === dateCompare.getDate()
  );
};

/**
 * Adición de horas a una fecha
 * @param date Fecha a la que se aumentara los días
 * @param hoursAdd Cantidad de horas que se agregaran a la fecha
 * @return Nueva fecha
 */
export const AddHours = (date: Date, hoursAdd: number): Date => {
  const dateNew = new Date(+date);
  dateNew.setHours(dateNew.getHours() + hoursAdd);

  return dateNew;
};

/**
 * Adición de días a una fecha
 * @param date Fecha a la que se aumentara los días
 * @param daysAdd Cantidad de días que se agregaran a la fecha
 * @return Nueva fecha
 */
export const AddDays = (date: Date, daysAdd: number): Date => {
  const dateNew = new Date(+date);
  dateNew.setDate(dateNew.getDate() + daysAdd);

  return dateNew;
};

/**
 * Adición de meses a una fecha
 * @param date Fecha a la que se aumentara los meses
 * @param monthsAdd Cantidad de meses que se agregaran a la fecha
 * @returns Nueva fecha
 */
export const AddMonths = (date: Date, monthsAdd: number): Date => {
  const dateNew = new Date(+date);
  dateNew.setMonth(dateNew.getMonth() + monthsAdd);

  return dateNew;
};

/**
 * Adición de años a una fecha
 * @param date Fecha a la que se aumentara los años
 * @param yearsAdd Cantidad de años que se agregaran a la fecha
 * @returns Nueva fecha
 */
export const AddYears = (date: Date, yearsAdd: number): Date => {
  const dateNew = new Date(+date);
  dateNew.setFullYear(dateNew.getFullYear() + yearsAdd);

  return dateNew;
};

export const DiffYears = (date: Date, years: number): Date => {
  const dateNew = new Date(+date);
  dateNew.setFullYear(dateNew.getFullYear() - years);

  return dateNew;
};

export const IsToday = (date: Date): boolean => {
  const today = new Date();

  return IsSameDate(date, today);
};

export const CloneDate = (date: Date): Date => {
  return new Date(+date);
};

export const IsAnotherMonth = (date: Date, dateCompate: Date): boolean => {
  return date && date.getMonth() !== dateCompate.getMonth();
};

export const IsWeekend = (date: Date): boolean => {
  return date.getDay() === 0 || date.getDay() === 6;
};

export const IsHoliday = (date: Date): boolean => {
  return !!holidays.find(holiday => new Date(holiday.date).getTime() === date.getTime());
};

/**
 * Verifica vencimiento
 * @param date String de Fecha
 * @returns true o false
 */
export function isExpired(date): boolean {
  const today = new Date();
  const endDate = new Date();
  const splitEndDate = date.split('-');
  endDate.setFullYear(splitEndDate[0], splitEndDate[1] - 1, splitEndDate[2]);

  return today > endDate;
}

/**
 * Verifica si la póliza tiene advertencia de vencimiento (semáforo)
 * @param date String de Fecha, parámetro fechaFin de la trama de pólizas
 * @param renewalDays Number, renovación anticipada, parámetro de servicio POLIZA_ANTIC_RENOVAR_DIAS
 * @returns true o false
 */
export function hasWarning(date: string, renewalDays: number): boolean {
  const endDate = new Date();
  endDate.setDate(endDate.getDate() + renewalDays);
  endDate.setHours(23, 59, 59, 0);

  const expirationDate = new Date();
  expirationDate.setHours(0, 0, 0, 0);
  const vSplitEndDate = date.split('-');
  expirationDate.setFullYear(+vSplitEndDate[0], +vSplitEndDate[1] - 1, +vSplitEndDate[2]);

  return endDate > expirationDate;
}

export function isEstimatedDate(date): boolean {
  const today = new Date();
  const endDate = new Date();
  const splitEndDate = date.split('-');
  endDate.setFullYear(splitEndDate[0], splitEndDate[1] - 1, splitEndDate[2]);

  return endDate > today;
}

/**
 * Verifica si fecha1 es mayor que fecha2
 * @param date1, date2 String de Fecha
 * @returns true o false
 */
export function isGreaterThan(date1, date2): boolean {
  const d1 = new Date();
  const d2 = new Date();
  const splitDate1 = date1.split('-');
  const splitDate2 = date2.split('-');
  d1.setFullYear(splitDate1[0], splitDate1[1] - 1, splitDate1[2]);
  d2.setFullYear(splitDate2[0], splitDate2[1] - 1, splitDate2[2]);

  return d1 > d2;
}

/**
 * Verifica si fecha1 es mayor que fecha2
 * @param date1 Date
 * @param date2 Date
 */
export function isGreaterThanDate(date1: Date, date2: Date): boolean {
  return date1 > date2;
}

export function betweenDate(dateFrom: Date, dateTo: Date, dateCheck: Date): boolean {
  return dateCheck >= dateFrom && dateCheck <= dateTo;
}

export const diffDays = (date: Date, days: number): Date => {
  const dateNew = new Date(+date);
  dateNew.setDate(dateNew.getDate() - days);

  return dateNew;
};

/**
 *  Convertir String a Fecha
 */
export function stringDDMMYYYYToDate(date: string | any): Date {
  if (!date) {
    return null;
  }
  const buff = splitDate(date);

  return new Date([buff[1], buff[0], buff[2]].join('/'));
}

export function stringYYYYMMDDToDate(date: string | any): Date {
  if (!date) {
    return null;
  }
  const buff = splitDate(date);

  return new Date(parseInt(buff[0], 10), parseInt(buff[1], 10) - 1, parseInt(buff[2], 10));
}

export function splitDate(date: string): object {
  return date.includes('/') ? date.split('/') : date.split('-');
}

/**
 *  Convertir Fecha a String
 */

export function dateToStringFormatMMDDYYYY(date: any): string {
  if (typeof date === 'string') {
    // tslint:disable-next-line: no-parameter-reassignment
    date = ValidateStringLikeDate(date);
  }

  return (
    (date &&
      date
        .toISOString()
        .substr(0, 10)
        .split('-')
        .reverse()
        .join('/')) ||
    ''
  );
}

export function getParsedStringDate(baseDate: string | any): any {
  // HACK: to fix datew with dash separation
  // https://stackoverflow.com/questions/5619202/converting-a-string-to-a-date-in-javascript
  const parsedStringDate =
    typeof baseDate === 'string' && /^\d{4}-[01]\d-[0-3]\d$/.test(baseDate) && baseDate.replace(/-/g, '/');

  return new Date(parsedStringDate || baseDate);
}

export function getDDMMYYYY(baseDate: string | any, separator = '/'): string {
  const newDate = getParsedStringDate(baseDate);
  const month = `0${newDate.getMonth() + 1}`.slice(-2);
  const day = `0${newDate.getDate()}`.slice(-2);
  const year = newDate.getFullYear();

  return [day.toString(), month.toString(), year.toString()].join(separator);
}

// Valida si la fecha tipo string es correcta
export function ValidateStringLikeDate(date: string): Date {
  // tslint:disable-next-line: no-parameter-reassignment
  date = date.trim();

  const buffDate =
    ddmmyyyy.test(date) || ddmmyy.test(date)
      ? stringDDMMYYYYToDate(date)
      : yyyymmdd.test(date) || yymmdd.test(date)
      ? stringYYYYMMDDToDate(date)
      : null;

  return buffDate ? buffDate : null;
}

// Format YYYY-MM-DD
export function dateToStringFormatYYYYMMDD(date: Date, delimiter = '-'): string {
  if (typeof date === 'string') {
    // tslint:disable-next-line: no-parameter-reassignment
    date = ValidateStringLikeDate(date);
  }

  return (
    (date &&
      date
        .toISOString()
        .substr(0, 10)
        .split('-')
        .join(delimiter)) ||
    ''
  );
}

export function dateToString(date: Date): string {
  return date.toISOString().substr(0, 10);
}

/**
 * Formato de Fecha
 */

// Formato ENE 2018
export function FormatMMMYYYY(date: string): string {
  const str = date.split('-');

  return `${monthNames[Number(str[1]) - 1].substr(0, 3)} ${str[0]}`;
}

// Formato LUNES 19 ENE 2019
export function FormatDDMMMYYYY(date, delimiter = '/'): string {
  const str = date.split(delimiter);
  const dateNew = new Date(str[0], Number(str[1]) - 1, str[2]);
  const day = dateNew.getDay();

  return `${dayNames[day]} ${str[2]} ${monthNames[Number(str[1]) - 1].substr(0, 3)} ${str[0]}`.toUpperCase();
}

export const FormatYYYYMMDD = (date: Date) => {
  return date
    .toISOString()
    .substr(0, 10)
    .replace(/-/gi, '');
};

export const FormatHHMMSS = (date: Date) => {
  return date
    .toISOString()
    .split('T')[1]
    .substr(0, 8)
    .replace(/:/gi, '');
};

export const GetStringDate = (date: Date): String => {
  const today = new Date();
  if (date.getMonth() === today.getMonth() && date.getDate() === today.getDate()) {
    return 'Hoy';
  } else if (date.getMonth() === today.getMonth() && date.getDate() === today.getDay() + 1) {
    return 'Mañana';
  } else if (date.getMonth() === today.getMonth() && date.getDate() === today.getDate() - 1) {
    return 'Ayer';
  }

  return `${date.getDate() + 1} de ${monthNames[date.getMonth()]} de ${date.getFullYear()}`;
};

export const convertMonth = (month: number): string => {
  return monthsNamesAbrev[month - 1];
};

export const diffMinutesBetween = (dt1: Date, dt2: Date, round = true): number => {
  let diff = (dt2.getTime() - dt1.getTime()) / 1000;
  diff /= 60;

  return Math.abs(round ? Math.round(diff) : diff);
};

// Año actual
export function CurrentYear(): number {
  const fecha = new Date();

  return fecha.getFullYear();
}

// Devuele dia de una fecha
export function getDay(myDate: string): string {
  return !isEmpty(myDate) ? myDate.split('-')[2] : '';
}

// Devuelve abreviatura de mes de una fecha
export function getMonth(myDate: string): string {
  return !isEmpty(myDate) ? convertMonth(parseInt(myDate.split('-')[1], 10)) : '';
}

// Devuelve año
export function getYear(myDate: string): string {
  return !isEmpty(myDate) ? myDate.split('-')[0] : '';
}

/**
 *
 *
 * @export
 * @param date1 Max date, must follow ISO 8601 format (yyyy-mm-dd). For eg: 2019-04-12T10:12:02
 * @param date2 min date, must follow ISO 8601 format (yyyy-mm-dd). For eg: 2019-04-12T10:12:02
 * @returns Difference in hours between two dates
 */
export function dateDiffInHours(date1: string | any, date2: string | any): number {
  const parseDate1: Date = getParsedStringDate(date1);
  const parseDate2 = getParsedStringDate(date2);
  const msPerHour = 1000 * 60 * 60;
  // Discard the time and time-zone information.
  const utc1 = Date.UTC(
    parseDate1.getFullYear(),
    parseDate1.getMonth(),
    parseDate1.getDate(),
    parseDate1.getHours(),
    parseDate1.getMinutes()
  );
  const utc2 = Date.UTC(
    parseDate2.getFullYear(),
    parseDate2.getMonth(),
    parseDate2.getDate(),
    parseDate2.getHours(),
    parseDate2.getMinutes()
  );

  return Math.floor((utc1 - utc2) / msPerHour);
  // tslint:disable-next-line: max-file-line-count
}

// tslint:disable-next-line: max-file-line-count
