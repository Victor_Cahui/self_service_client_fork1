// tslint:disable: unnecessary-constructor
import { Injectable } from '@angular/core';
import { environment } from '@mx/environments/environment';
import { LocalStorage } from './storage-manager';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService extends LocalStorage {
  constructor() {
    super();
  }

  setData(key: string, data: any): void {
    const dataString = JSON.stringify(data);
    const dataTmp = environment.production ? btoa(dataString) : dataString;
    this.setItem(key, dataTmp);
  }

  getData(key: string): any {
    const itemStorage = this.getItem(key) !== 'null' && this.getItem(key);

    return itemStorage ? JSON.parse(environment.production ? atob(itemStorage) : itemStorage) : null;
  }
}
