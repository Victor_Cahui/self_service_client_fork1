// tslint:disable

import { HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import * as model from '@mx/core/shared/providers/models';
import { ApiService } from '@mx/core/shared/helpers/api';
import { appConstants } from '@mx/core/shared/providers/constants';

interface ReqOptions {
  default?: any;
  headers?:
    | HttpHeaders
    | {
        [header: string]: string | string[];
      };
  observe?: 'body'; // response, body
  params?:
    | HttpParams
    | {
        [param: string]: string | string[];
      };
  preloader?: boolean;
  reportProgress?: boolean;
  responseType?: 'json';
  retry?: number;
  withCredentials?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class Autoservicios {
  constructor(private readonly _ApiService: ApiService) {}

  public ObtenerListaOficinas(
    pathReq: model.ReqPathGetObtenerListaOficinas,
    queryReq: model.ReqQueryGetObtenerListaOficinas,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/oficinas/${pathReq.codigoApp}`, {
      params: { ...model.ReqQueryGetObtenerListaOficinas.create(queryReq) },
      ...reqOptions
    });
  }

  public LoginFechaNacimiento(
    bodyReq: model.ReqBodyPostLoginFechaNacimiento,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/clientes/login/fechaNacimiento`,
      model.ReqBodyPostLoginFechaNacimiento.create(bodyReq),
      { ...reqOptions }
    );
  }

  public SolicitarNroComprobacion(
    pathReq: model.ReqPathPostSolicitarNroComprobacion,
    bodyReq: model.ReqBodyPostSolicitarNroComprobacion,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/clientes/nroComprobacion/solicitud/${pathReq.codigoApp}`,
      model.ReqBodyPostSolicitarNroComprobacion.create(bodyReq),
      { ...reqOptions }
    );
  }

  public VerificarNroComprobacion(
    pathReq: model.ReqPathPostVerificarNroComprobacion,
    bodyReq: model.ReqBodyPostVerificarNroComprobacion,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/clientes/nroComprobacion/verificacion/${pathReq.codigoApp}`,
      model.ReqBodyPostVerificarNroComprobacion.create(bodyReq),
      { ...reqOptions }
    );
  }

  public RegistrarDatosMovil(
    pathReq: model.ReqPathPostRegistrarDatosMovil,
    bodyReq: model.ReqBodyPostRegistrarDatosMovil,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/clientes/datosMovil/${pathReq.codigoApp}`,
      model.ReqBodyPostRegistrarDatosMovil.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ConsentimientoPoliza(
    pathReq: model.ReqPathPostConsentimientoPoliza,
    bodyReq: model.ReqBodyPostConsentimientoPoliza,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/clientes/consentimiento/${pathReq.codigoApp}`,
      model.ReqBodyPostConsentimientoPoliza.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ObtenerConfiguracionMenuPorCliente(
    pathReq: model.ReqPathGetObtenerConfiguracionMenuPorCliente,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/clientes/menu/${pathReq.codigoApp}`, {
      ...reqOptions
    });
  }

  public VerificarVersion(pathReq: model.ReqPathGetVerificarVersion, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/version/${pathReq.codigoApp}/${pathReq.version}`, {
      ...reqOptions
    });
  }

  public InformacionPersonal(reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/clientes/perfil/datosPersonales`, {
      ...reqOptions
    });
  }

  public ActualizarDatosPersonalesSensibles(
    bodyReq?: model.ReqFormDataPutActualizarDatosPersonalesSensibles,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.put(
      `${appConstants.apis.autoservicios}/areaPrivada/clientes/perfil/datosPersonalesSensibles`,
      model.ReqFormDataPutActualizarDatosPersonalesSensibles.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ActualizarDatosPersonalesNoSensibles(
    bodyReq?: model.ReqBodyPutActualizarDatosPersonalesNoSensibles,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.put(
      `${appConstants.apis.autoservicios}/areaPrivada/clientes/perfil/datosPersonalesNoSensibles`,
      model.ReqBodyPutActualizarDatosPersonalesNoSensibles.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ActualizarInformacionTrabajo(
    bodyReq: model.ReqBodyPutActualizarInformacionTrabajo,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.put(
      `${appConstants.apis.autoservicios}/areaPrivada/clientes/perfil/datosTrabajo`,
      model.ReqBodyPutActualizarInformacionTrabajo.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ObtenerConfiguracionUsuario(
    pathReq: model.ReqPathGetObtenerConfiguracionUsuario,
    queryReq: model.ReqQueryGetObtenerConfiguracionUsuario,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/clientes/perfil/obtenerConfiguracion/${pathReq.codigoApp}`,
      { params: { ...model.ReqQueryGetObtenerConfiguracionUsuario.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerdireccionesPoliza(
    queryReq: model.ReqQueryGetObtenerdireccionesPoliza,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/clientes/perfil/direcciones`, {
      params: { ...model.ReqQueryGetObtenerdireccionesPoliza.create(queryReq) },
      ...reqOptions
    });
  }

  public ObtenerDireccionCorrespondenciaPersonal(reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/clientes/perfil/direccionCorrespondencia`,
      { ...reqOptions }
    );
  }

  public ActualizarDireccionCorrespondenciaPersonal(
    bodyReq: model.ReqBodyPutActualizarDireccionCorrespondenciaPersonal,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.put(
      `${appConstants.apis.autoservicios}/areaPrivada/clientes/perfil/direccionCorrespondencia`,
      model.ReqBodyPutActualizarDireccionCorrespondenciaPersonal.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ObtenerMapfreDolares(reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/clientes/perfil/mapfreDolares`, {
      ...reqOptions
    });
  }

  public GuardarInformacionMD(
    pathReq: model.ReqPathPutGuardarInformacionMD,
    bodyReq: model.ReqBodyPutGuardarInformacionMD,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.put(
      `${appConstants.apis.autoservicios}/areaPrivada/clientes/mapfreDolares/info/${pathReq.codigoApp}`,
      model.ReqBodyPutGuardarInformacionMD.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ObtenerResumenMD(pathReq: model.ReqPathGetObtenerResumenMD, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/clientes/mapfreDolares/resumen/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public ObtenerDetallePaginadoMD(
    pathReq: model.ReqPathGetObtenerDetallePaginadoMD,
    queryReq: model.ReqQueryGetObtenerDetallePaginadoMD,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/clientes/mapfreDolares/detalle/${pathReq.codigoApp}`,
      { params: { ...model.ReqQueryGetObtenerDetallePaginadoMD.create(queryReq) }, ...reqOptions }
    );
  }

  public ActualizarCorreoElectronico(
    bodyReq?: model.ReqBodyPutActualizarCorreoElectronico,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.put(
      `${appConstants.apis.autoservicios}/areaPrivada/clientes/perfil/correoElectronico`,
      model.ReqBodyPutActualizarCorreoElectronico.create(bodyReq),
      { ...reqOptions }
    );
  }

  public RegistrarContactarAgente(
    pathReq: model.ReqPathPostRegistrarContactarAgente,
    bodyReq: model.ReqBodyPostRegistrarContactarAgente,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/clientes/perfil/contactoAgente/${pathReq.codigoApp}`,
      model.ReqBodyPostRegistrarContactarAgente.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ObtenerDatosContacto(pathReq: model.ReqPathGetObtenerDatosContacto, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/clientes/perfil/datosContactos/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public ObtenerListaProfesiones(
    pathReq: model.ReqPathGetObtenerListaProfesiones,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/clientes/perfil/profesiones/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public VerificarDocumento(
    pathReq: model.ReqPathGetVerificarDocumento,
    queryReq: model.ReqQueryGetVerificarDocumento,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/clientes/verificacionDocumento/${pathReq.codigoApp}`,
      { params: { ...model.ReqQueryGetVerificarDocumento.create(queryReq) }, ...reqOptions }
    );
  }

  public RegistrarLlamadaEmergencia(
    pathReq: model.ReqPathPostRegistrarLlamadaEmergencia,
    bodyReq: model.ReqBodyPostRegistrarLlamadaEmergencia,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/clientes/llamadaEmergencia/${pathReq.codigoApp}`,
      model.ReqBodyPostRegistrarLlamadaEmergencia.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ObtenerPermisos(
    pathReq: model.ReqPathGetObtenerPermisos,
    queryReq: model.ReqQueryGetObtenerPermisos,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/clientes/permisos/${pathReq.codigoApp}`,
      { params: { ...model.ReqQueryGetObtenerPermisos.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerAgentes(pathReq: model.ReqPathGetObtenerAgentes, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/clientes/agentes/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public ObtenerConfiguracionContacto(
    pathReq: model.ReqPathGetObtenerConfiguracionContacto,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/configuracionContacto/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public ActualizarConfiguracionContacto(
    pathReq: model.ReqPathPutActualizarConfiguracionContacto,
    bodyReq: model.ReqBodyPutActualizarConfiguracionContacto,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.put(
      `${appConstants.apis.autoservicios}/areaPrivada/configuracionContacto/${pathReq.codigoApp}`,
      model.ReqBodyPutActualizarConfiguracionContacto.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ContactarReferidos(
    pathReq: model.ReqPathPostContactarReferidos,
    bodyReq: model.ReqBodyPostContactarReferidos,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/clientes/perfil/contactoReferidos/${pathReq.codigoApp}`,
      model.ReqBodyPostContactarReferidos.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ObtenerNotificaciones(
    pathReq: model.ReqPathGetObtenerNotificaciones,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/notificaciones/${pathReq.codigoApp}`, {
      ...reqOptions
    });
  }

  public RegistrarNotificacionLeida(
    pathReq: model.ReqPathPostRegistrarNotificacionLeida,
    bodyReq: model.ReqBodyPostRegistrarNotificacionLeida,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/notificacion/leida/${pathReq.codigoApp}`,
      model.ReqBodyPostRegistrarNotificacionLeida.create(bodyReq),
      { ...reqOptions }
    );
  }

  public RegistrarTerminosCondiciones(
    pathReq: model.ReqPathPutRegistrarTerminosCondiciones,
    bodyReq: model.ReqBodyPutRegistrarTerminosCondiciones,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.put(
      `${appConstants.apis.autoservicios}/areaPrivada/clientes/terminosCondiciones/${pathReq.codigoApp}`,
      model.ReqBodyPutRegistrarTerminosCondiciones.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ObtenerTerminosCondiciones(
    pathReq: model.ReqPathGetObtenerTerminosCondiciones,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/clientes/terminosCondiciones/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public RegistrarNotificacion(
    pathReq: model.ReqPathPostRegistrarNotificacion,
    bodyReq: model.ReqBodyPostRegistrarNotificacion,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/registronotificaciones/${pathReq.codigoApp}`,
      model.ReqBodyPostRegistrarNotificacion.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ListadoPagos(
    pathReq: model.ReqPathGetListadoPagos,
    queryReq: model.ReqQueryGetListadoPagos,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/pagos/buscarPorCliente/${pathReq.codigoApp}`,
      { params: { ...model.ReqQueryGetListadoPagos.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerTiposPagos(pathReq: model.ReqPathGetObtenerTiposPagos, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/pagos/tipos/${pathReq.codigoApp}`, {
      ...reqOptions
    });
  }

  public ObtenerTiposPagosPorPersona(
    pathReq: model.ReqPathGetObtenerTiposPagosPorPersona,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/pagos/tipos/${pathReq.codigoApp}`, {
      ...reqOptions
    });
  }

  public ListarConfiguracionesPagos(
    pathReq: model.ReqPathGetListarConfiguracionesPagos,
    queryReq: model.ReqQueryGetListarConfiguracionesPagos,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/pagos/configuraciones/${pathReq.codigoApp}`,
      { params: { ...model.ReqQueryGetListarConfiguracionesPagos.create(queryReq) }, ...reqOptions }
    );
  }

  public ConfigurarRenovacion(
    pathReq: model.ReqPathPostConfigurarRenovacion,
    bodyReq: model.ReqBodyPostConfigurarRenovacion,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/pagos/renovacion/${pathReq.codigoApp}`,
      model.ReqBodyPostConfigurarRenovacion.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ObtenerCuotasRefinanciarPoliza(
    pathReq: model.ReqPathGetObtenerCuotasRefinanciarPoliza,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/pagos/refinanciamiento/${pathReq.codigoApp}/${pathReq.numeroPoliza}`,
      { ...reqOptions }
    );
  }

  public EnviarDatosRefinanciamiento(
    pathReq: model.ReqPathPostEnviarDatosRefinanciamiento,
    bodyReq: model.ReqBodyPostEnviarDatosRefinanciamiento,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/pagos/refinanciamiento/solicitar/${pathReq.codigoApp}/${pathReq.numeroPoliza}`,
      model.ReqBodyPostEnviarDatosRefinanciamiento.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ObtenerNumeroPagosPendientes(
    pathReq: model.ReqPathGetObtenerNumeroPagosPendientes,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/pagos/numeroPendientes/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public ObtenerRecibos(
    pathReq: model.ReqPathGetObtenerRecibos,
    queryReq: model.ReqQueryGetObtenerRecibos,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/recibos/${pathReq.codigoApp}`, {
      params: { ...model.ReqQueryGetObtenerRecibos.create(queryReq) },
      ...reqOptions
    });
  }

  public ObtenerGrupoRecibos(
    pathReq: model.ReqPathGetObtenerGrupoRecibos,
    queryReq: model.ReqQueryGetObtenerGrupoRecibos,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/recibos/agrupado/${pathReq.codigoApp}`,
      { params: { ...model.ReqQueryGetObtenerGrupoRecibos.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerDetalleRecibos(
    pathReq: model.ReqPathGetObtenerDetalleRecibos,
    queryReq: model.ReqQueryGetObtenerDetalleRecibos,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/recibos/detalle/${pathReq.codigoApp}`, {
      params: { ...model.ReqQueryGetObtenerDetalleRecibos.create(queryReq) },
      ...reqOptions
    });
  }

  public DescargarRecibo(pathReq: model.ReqPathGetDescargarRecibo, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/recibo/${pathReq.codigoApp}/${pathReq.numeroRecibo}`,
      { ...reqOptions }
    );
  }

  public EnviarMailRecibo(
    pathReq: model.ReqPathPostEnviarMailRecibo,
    bodyReq: model.ReqBodyPostEnviarMailRecibo,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/recibo/correo/${pathReq.codigoApp}/${pathReq.numeroRecibo}`,
      model.ReqBodyPostEnviarMailRecibo.create(bodyReq),
      { ...reqOptions }
    );
  }

  public PagarRecibo(
    pathReq: model.ReqPathPostPagarRecibo,
    bodyReq: model.ReqBodyPostPagarRecibo,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/recibo/pago/${pathReq.codigoApp}`,
      model.ReqBodyPostPagarRecibo.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ObtenerRecibosReordenado(
    pathReq: model.ReqPathGetObtenerRecibosReordenado,
    queryReq: model.ReqQueryGetObtenerRecibosReordenado,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/recibos/spto/vigente/${pathReq.codigoApp}`,
      { params: { ...model.ReqQueryGetObtenerRecibosReordenado.create(queryReq) }, ...reqOptions }
    );
  }

  public VerificarReciboAfiliado(
    pathReq: model.ReqPathGetVerificarReciboAfiliado,
    queryReq: model.ReqQueryGetVerificarReciboAfiliado,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/pagos/verificar/proceso/debito/automatico/${pathReq.codigoApp}/${pathReq.tipoDocPago}/${pathReq.numDocPago}`,
      { params: { ...model.ReqQueryGetVerificarReciboAfiliado.create(queryReq) }, ...reqOptions }
    );
  }

  public VerificarPagoRecibo(
    pathReq: model.ReqPathGetVerificarPagoRecibo,
    queryReq: model.ReqQueryGetVerificarPagoRecibo,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/recibos/verificarPago/${pathReq.codigoApp}`,
      { params: { ...model.ReqQueryGetVerificarPagoRecibo.create(queryReq) }, ...reqOptions }
    );
  }

  public ListarTramites(
    pathReq: model.ReqPathGetListarTramites,
    queryReq: model.ReqQueryGetListarTramites,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/tramites/buscarPorCliente/${pathReq.codigoApp}`,
      { params: { ...model.ReqQueryGetListarTramites.create(queryReq) }, ...reqOptions }
    );
  }

  public ListarTramitesDetallado(
    pathReq: model.ReqPathGetListarTramitesDetallado,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/tramites/buscarPorClienteDetalle/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public DetalleSolicitudCartaGarantia(
    pathReq: model.ReqPathGetDetalleSolicitudCartaGarantia,
    queryReq: model.ReqQueryGetDetalleSolicitudCartaGarantia,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/cartasGarantia/detalleSolicitud/${pathReq.codigoApp}`,
      { params: { ...model.ReqQueryGetDetalleSolicitudCartaGarantia.create(queryReq) }, ...reqOptions }
    );
  }

  public ListarPolizas(
    pathReq: model.ReqPathGetListarPolizas,
    queryReq: model.ReqQueryGetListarPolizas,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/polizas/buscarPorCliente/${pathReq.codigoApp}`,
      { params: { ...model.ReqQueryGetListarPolizas.create(queryReq) }, ...reqOptions }
    );
  }

  public DescargarPoliza(pathReq: model.ReqPathGetDescargarPoliza, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/poliza/${pathReq.numeroPoliza}`, {
      ...reqOptions
    });
  }

  public DescargarPolizaCondicionado(
    pathReq: model.ReqPathGetDescargarPolizaCondicionado,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/poliza/condicionado/${pathReq.codigoApp}/${pathReq.numeroPoliza}`,
      { ...reqOptions }
    );
  }

  public ObtenerTiposPolizasPorCliente(
    pathReq: model.ReqPathGetObtenerTiposPolizasPorCliente,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/polizas/tipos/${pathReq.codigoApp}`, {
      ...reqOptions
    });
  }

  public ObtenerTiposRamosPolizasPorCliente(
    pathReq: model.ReqPathGetObtenerTiposRamosPolizasPorCliente,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/polizas/tiposRamos/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public ObtenerCoberturasPoliza(
    pathReq: model.ReqPathGetObtenerCoberturasPoliza,
    queryReq: model.ReqQueryGetObtenerCoberturasPoliza,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/poliza/coberturas/${pathReq.codigoApp}/${pathReq.numeroPoliza}`,
      { params: { ...model.ReqQueryGetObtenerCoberturasPoliza.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerCoberturasVehicularesPoliza(
    pathReq: model.ReqPathGetObtenerCoberturasVehicularesPoliza,
    queryReq: model.ReqQueryGetObtenerCoberturasVehicularesPoliza,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/poliza/coberturas/${pathReq.codigoApp}/${pathReq.numeroPoliza}/${pathReq.chasis}/${pathReq.nroMotor}`,
      { params: { ...model.ReqQueryGetObtenerCoberturasVehicularesPoliza.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerDeducibleVehicularPoliza(
    pathReq: model.ReqPathGetObtenerDeducibleVehicularPoliza,
    queryReq: model.ReqQueryGetObtenerDeducibleVehicularPoliza,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/poliza/deducibles/vehiculos/${pathReq.codigoApp}/${pathReq.numeroPoliza}`,
      { params: { ...model.ReqQueryGetObtenerDeducibleVehicularPoliza.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerHogaresPoliza(pathReq: model.ReqPathGetObtenerHogaresPoliza, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/poliza/hogares/${pathReq.codigoApp}/${pathReq.numeroPoliza}`,
      { ...reqOptions }
    );
  }

  public RegistrarSolicitudTraspasoPoliza(
    pathReq: model.ReqPathPostRegistrarSolicitudTraspasoPoliza,
    bodyReq: model.ReqBodyPostRegistrarSolicitudTraspasoPoliza,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/poliza/solicitudTraspaso/${pathReq.codigoApp}/${pathReq.numeroPoliza}`,
      model.ReqBodyPostRegistrarSolicitudTraspasoPoliza.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ObtenerHojaAnexaPoliza(
    pathReq: model.ReqPathGetObtenerHojaAnexaPoliza,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/poliza/hojaAnexa/${pathReq.codigoApp}/${pathReq.numeroPoliza}`,
      { ...reqOptions }
    );
  }

  public ObtenerAgentePoliza(pathReq: model.ReqPathGetObtenerAgentePoliza, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/poliza/agente/${pathReq.codigoApp}/${pathReq.numeroPoliza}`,
      { ...reqOptions }
    );
  }

  public CambiarModalidadPolizaVehicular(
    pathReq: model.ReqPathPostCambiarModalidadPolizaVehicular,
    bodyReq: model.ReqBodyPostCambiarModalidadPolizaVehicular,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/poliza/cambiarmodalidad/${pathReq.codigoApp}/${pathReq.numeroPoliza}`,
      model.ReqBodyPostCambiarModalidadPolizaVehicular.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ObtenerContratante(pathReq: model.ReqPathGetObtenerContratante, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/poliza/contratante/${pathReq.codigoApp}/${pathReq.numPoliza}`,
      { ...reqOptions }
    );
  }

  public ObtenerCategorias(pathReq: model.ReqPathGetObtenerCategorias, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/polizas/categorias/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public ListadoPagosCuotas(
    pathReq: model.ReqPathGetListadoPagosCuotas,
    queryReq: model.ReqQueryGetListadoPagosCuotas,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/poliza/cuotas/${pathReq.codigoApp}/${pathReq.numeroPoliza}`,
      { params: { ...model.ReqQueryGetListadoPagosCuotas.create(queryReq) }, ...reqOptions }
    );
  }

  public ListarLimiteAnios(
    pathReq: model.ReqPathGetListarLimiteAnios,
    queryReq: model.ReqQueryGetListarLimiteAnios,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/polizas/limiteAnios/${pathReq.codigoApp}`,
      { params: { ...model.ReqQueryGetListarLimiteAnios.create(queryReq) }, ...reqOptions }
    );
  }

  public ListarPolizasPaginado(
    pathReq: model.ReqPathGetListarPolizasPaginado,
    queryReq: model.ReqQueryGetListarPolizasPaginado,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/polizas/buscarPorCliente/paginado/${pathReq.codigoApp}`,
      { params: { ...model.ReqQueryGetListarPolizasPaginado.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerDatosContratante(
    pathReq: model.ReqPathGetObtenerDatosContratante,
    queryReq: model.ReqQueryGetObtenerDatosContratante,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/poliza/${pathReq.numeroPoliza}/contratante`,
      { params: { ...model.ReqQueryGetObtenerDatosContratante.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerDatosTitular(
    pathReq: model.ReqPathGetObtenerDatosTitular,
    queryReq: model.ReqQueryGetObtenerDatosTitular,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/poliza/${pathReq.numeroPoliza}/titular`,
      { params: { ...model.ReqQueryGetObtenerDatosTitular.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerVehiculo(pathReq: model.ReqPathGetObtenerVehiculo, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculo/${pathReq.codigoApp}/${pathReq.numeroPoliza}/${pathReq.chasis}/${pathReq.nroMotor}`,
      { ...reqOptions }
    );
  }

  public ObtenerVehiculosPorPoliza(
    pathReq: model.ReqPathGetObtenerVehiculosPorPoliza,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/${pathReq.codigoApp}/${pathReq.numeroPoliza}`,
      { ...reqOptions }
    );
  }

  public ActualizarPlaca(
    pathReq: model.ReqPathPutActualizarPlaca,
    bodyReq: model.ReqBodyPutActualizarPlaca,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.put(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/placa/${pathReq.codigoApp}/${pathReq.numeroPoliza}`,
      model.ReqBodyPutActualizarPlaca.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ObtenerVehiculoPorPlaca(
    pathReq: model.ReqPathGetObtenerVehiculoPorPlaca,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculo/${pathReq.codigoApp}/${pathReq.placa}`,
      { ...reqOptions }
    );
  }

  public ObtenerVehiculoPorUsuario(
    pathReq: model.ReqPathGetObtenerVehiculoPorUsuario,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculosPorUsuario/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public VerificarDisponibilidadAutoprocuracion(
    pathReq: model.ReqPathPostVerificarDisponibilidadAutoprocuracion,
    bodyReq: model.ReqBodyPostVerificarDisponibilidadAutoprocuracion,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/verificacionAutoprocuracion/${pathReq.codigoApp}`,
      model.ReqBodyPostVerificarDisponibilidadAutoprocuracion.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ObtenerTiposAccesorios(reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/vehiculos/accesorios/tiposAccesorios`, {
      ...reqOptions
    });
  }

  public RegistrarAccesorio(
    pathReq: model.ReqPathPostRegistrarAccesorio,
    bodyReq: model.ReqBodyPostRegistrarAccesorio,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/accesorio/${pathReq.codigoApp}/${pathReq.numeroPoliza}/${pathReq.chasis}/${pathReq.nroMotor}`,
      model.ReqBodyPostRegistrarAccesorio.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ActualizarAccesorio(
    pathReq: model.ReqPathPutActualizarAccesorio,
    bodyReq: model.ReqBodyPutActualizarAccesorio,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.put(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/accesorio/${pathReq.codigoApp}/${pathReq.numeroPoliza}/${pathReq.chasis}/${pathReq.nroMotor}/${pathReq.accesorioId}`,
      model.ReqBodyPutActualizarAccesorio.create(bodyReq),
      { ...reqOptions }
    );
  }

  public EliminarAccesorio(pathReq: model.ReqPathDeleteEliminarAccesorio, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.delete(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/accesorio/${pathReq.codigoApp}/${pathReq.numeroPoliza}/${pathReq.chasis}/${pathReq.nroMotor}/${pathReq.accesorioId}`,
      { ...reqOptions }
    );
  }

  public ObtenerAccidentes(pathReq: model.ReqPathGetObtenerAccidentes, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/accidentes/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public ObtenerRobos(pathReq: model.ReqPathGetObtenerRobos, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/vehiculos/robos/${pathReq.codigoApp}`, {
      ...reqOptions
    });
  }

  public ObtenerAccidentesListaUnica(
    pathReq: model.ReqPathGetObtenerAccidentesListaUnica,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/accidentes/listaUnica/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public ObtenerRobosListaUnica(
    pathReq: model.ReqPathGetObtenerRobosListaUnica,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/robos/listaUnica/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public ObtenerDetallesAccidente(
    pathReq: model.ReqPathGetObtenerDetallesAccidente,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/detallesAccidente/${pathReq.codigoApp}/${pathReq.numeroAsistencia}`,
      { ...reqOptions }
    );
  }

  public ObtenerDetallesRobo(pathReq: model.ReqPathGetObtenerDetallesRobo, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/detallesRobo/${pathReq.codigoApp}/${pathReq.numeroAsistencia}`,
      { ...reqOptions }
    );
  }

  public CalificarTaller(bodyReq: model.ReqBodyPostCalificarTaller, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/calificarTaller`,
      model.ReqBodyPostCalificarTaller.create(bodyReq),
      { ...reqOptions }
    );
  }

  public AdjuntarCartaNoAdeudo(
    pathReq: model.ReqPathPostAdjuntarCartaNoAdeudo,
    bodyReq?: model.ReqFormDataPostAdjuntarCartaNoAdeudo,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/adjuntarCartaNoAdeudo/${pathReq.codigoApp}`,
      model.ReqFormDataPostAdjuntarCartaNoAdeudo.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ObtenerMarcasCotizacion(
    pathReq: model.ReqPathGetObtenerMarcasCotizacion,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/cotizacion/marcas/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public ObtenerModelosCotizacion(
    pathReq: model.ReqPathGetObtenerModelosCotizacion,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/cotizacion/modelos/${pathReq.codigoApp}/${pathReq.marca}`,
      { ...reqOptions }
    );
  }

  public ObtenerTiposVehiculo(pathReq: model.ReqPathGetObtenerTiposVehiculo, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/cotizacion/tiposVehiculo/${pathReq.codigoApp}/${pathReq.marca}/${pathReq.modelo}`,
      { ...reqOptions }
    );
  }

  public ObtenerValorSugerido(
    pathReq: model.ReqPathGetObtenerValorSugerido,
    queryReq: model.ReqQueryGetObtenerValorSugerido,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/cotizacion/valorSugerido/${pathReq.codigoApp}/${pathReq.marca}/${pathReq.modelo}/${pathReq.anio}/${pathReq.tipoVehiculo}`,
      { params: { ...model.ReqQueryGetObtenerValorSugerido.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerUsosVehiculo(pathReq: model.ReqPathGetObtenerUsosVehiculo, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/cotizacion/usosVehiculo/${pathReq.codigoApp}/${pathReq.marca}/${pathReq.modelo}/${pathReq.tipoVehiculo}`,
      { ...reqOptions }
    );
  }

  public ObtenerCiudades(pathReq: model.ReqPathGetObtenerCiudades, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/cotizacion/ciudades/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public BuscarVehiculoPorPlaca(
    pathReq: model.ReqPathGetBuscarVehiculoPorPlaca,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/cotizacion/buscarPorPlaca/${pathReq.codigoApp}/${pathReq.placa}`,
      { ...reqOptions }
    );
  }

  public ValidarGPS(
    pathReq: model.ReqPathGetValidarGPS,
    queryReq: model.ReqQueryGetValidarGPS,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/cotizacion/validarGPS/${pathReq.codigoApp}/${pathReq.marca}/${pathReq.modelo}/${pathReq.anio}`,
      { params: { ...model.ReqQueryGetValidarGPS.create(queryReq) }, ...reqOptions }
    );
  }

  public CotizarPolizaVehicularProductos(
    pathReq: model.ReqPathPostCotizarPolizaVehicularProductos,
    queryReq: model.ReqQueryPostCotizarPolizaVehicularProductos,
    bodyReq?: model.ReqBodyPostCotizarPolizaVehicularProductos,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/cotizacion/productos/${pathReq.codigoApp}`,
      model.ReqBodyPostCotizarPolizaVehicularProductos.create(bodyReq),
      { params: { ...model.ReqQueryPostCotizarPolizaVehicularProductos.create(queryReq) }, ...reqOptions }
    );
  }

  public CotizarPolizaVehicularCoberturas(
    pathReq: model.ReqPathPostCotizarPolizaVehicularCoberturas,
    queryReq: model.ReqQueryPostCotizarPolizaVehicularCoberturas,
    bodyReq?: model.ReqBodyPostCotizarPolizaVehicularCoberturas,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/cotizacion/coberturas/${pathReq.codigoApp}`,
      model.ReqBodyPostCotizarPolizaVehicularCoberturas.create(bodyReq),
      { params: { ...model.ReqQueryPostCotizarPolizaVehicularCoberturas.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerPDFTalleresPreferentes(
    pathReq: model.ReqPathGetObtenerPDFTalleresPreferentes,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/asistencias/talleresPreferentes/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public ObtenerColorPorVehiculos(
    pathReq: model.ReqPathGetObtenerColorPorVehiculos,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/compra/colores/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public ObtenerChasisMotorPorPlaca(
    pathReq: model.ReqPathGetObtenerChasisMotorPorPlaca,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/compra/buscarChasisMotor/${pathReq.codigoApp}/${pathReq.placa}`,
      { ...reqOptions }
    );
  }

  public ValidarPolizaPorChasisMotor(
    pathReq: model.ReqPathGetValidarPolizaPorChasisMotor,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/compra/validarPoliza/${pathReq.codigoApp}/${pathReq.placa}/${pathReq.chasis}/${pathReq.motor}`,
      { ...reqOptions }
    );
  }

  public ValidarClausula(pathReq: model.ReqPathGetValidarClausula, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/compra/clausulaDatosPersonales/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public ObtenerCondicionInspeccion(
    pathReq: model.ReqPathGetObtenerCondicionInspeccion,
    queryReq: model.ReqQueryGetObtenerCondicionInspeccion,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/compra/condicionesInspeccion/${pathReq.codigoApp}`,
      { params: { ...model.ReqQueryGetObtenerCondicionInspeccion.create(queryReq) }, ...reqOptions }
    );
  }

  public ValidarInspecciones(
    pathReq: model.ReqPathGetValidarInspecciones,
    queryReq: model.ReqQueryGetValidarInspecciones,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/compra/determinarInspeccion/${pathReq.codigoApp}`,
      { params: { ...model.ReqQueryGetValidarInspecciones.create(queryReq) }, ...reqOptions }
    );
  }

  public RangoHorasInspeccionPath(
    pathReq: model.ReqPathGetRangoHorasInspeccionPath,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/compra/rangoHorasInspeccion/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public IndicacionesPolizaElectronica(
    pathReq: model.ReqPathGetIndicacionesPolizaElectronica,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/compra/indicacion/PolizaElectronica/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public ObtenerCondicionesProducto(
    pathReq: model.ReqPathGetObtenerCondicionesProducto,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/compra/obtenerCondicionesProducto/${pathReq.codigoApp}/${pathReq.codigoModalidad}`,
      { ...reqOptions }
    );
  }

  public GenerarSolicitudInspeccion(
    pathReq: model.ReqPathPostGenerarSolicitudInspeccion,
    bodyReq: model.ReqBodyPostGenerarSolicitudInspeccion,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/compra/generarSolicitudInspeccion/${pathReq.codigoApp}`,
      model.ReqBodyPostGenerarSolicitudInspeccion.create(bodyReq),
      { ...reqOptions }
    );
  }

  public CotizarVehiculoProducto(
    pathReq: model.ReqPathPostCotizarVehiculoProducto,
    bodyReq?: model.ReqBodyPostCotizarVehiculoProducto,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/compra/cotizacion/${pathReq.codigoApp}`,
      model.ReqBodyPostCotizarVehiculoProducto.create(bodyReq),
      { ...reqOptions }
    );
  }

  public EmitirVehiculoProducto(
    pathReq: model.ReqPathPostEmitirVehiculoProducto,
    bodyReq?: model.ReqBodyPostEmitirVehiculoProducto,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/vehiculos/compra/emision/${pathReq.codigoApp}`,
      model.ReqBodyPostEmitirVehiculoProducto.create(bodyReq),
      { ...reqOptions }
    );
  }

  public PreregistrarRobo(
    pathReq: model.ReqPathPostPreregistrarRobo,
    bodyReq: model.ReqBodyPostPreregistrarRobo,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/robo/preregistro/${pathReq.codigoApp}`,
      model.ReqBodyPostPreregistrarRobo.create(bodyReq),
      { ...reqOptions }
    );
  }

  public RegistrarFotoRobo(
    pathReq: model.ReqPathPostRegistrarFotoRobo,
    bodyReq?: model.ReqFormDataPostRegistrarFotoRobo,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/robo/foto/${pathReq.codigoApp}`,
      model.ReqFormDataPostRegistrarFotoRobo.create(bodyReq),
      { ...reqOptions }
    );
  }

  public EliminarFotoRobo(pathReq: model.ReqPathDeleteEliminarFotoRobo, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.delete(
      `${appConstants.apis.autoservicios}/areaPrivada/robo/foto/${pathReq.codigoApp}/${pathReq.roboId}/${pathReq.tipoFoto}`,
      { ...reqOptions }
    );
  }

  public RegistrarRobo(
    pathReq: model.ReqPathPostRegistrarRobo,
    bodyReq: model.ReqBodyPostRegistrarRobo,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/robo/${pathReq.codigoApp}/${pathReq.roboId}`,
      model.ReqBodyPostRegistrarRobo.create(bodyReq),
      { ...reqOptions }
    );
  }

  public PreregistrarAccidente(
    pathReq: model.ReqPathPostPreregistrarAccidente,
    bodyReq: model.ReqBodyPostPreregistrarAccidente,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/accidente/preregistro/${pathReq.codigoApp}`,
      model.ReqBodyPostPreregistrarAccidente.create(bodyReq),
      { ...reqOptions }
    );
  }

  public RegistrarFotoAccidente(
    pathReq: model.ReqPathPostRegistrarFotoAccidente,
    bodyReq?: model.ReqFormDataPostRegistrarFotoAccidente,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/accidente/foto/${pathReq.codigoApp}`,
      model.ReqFormDataPostRegistrarFotoAccidente.create(bodyReq),
      { ...reqOptions }
    );
  }

  public EliminarFotoAccidente(
    pathReq: model.ReqPathDeleteEliminarFotoAccidente,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.delete(
      `${appConstants.apis.autoservicios}/areaPrivada/accidente/foto/${pathReq.codigoApp}/${pathReq.accidenteId}/${pathReq.tipoFoto}`,
      { ...reqOptions }
    );
  }

  public RegistrarAccidente(
    pathReq: model.ReqPathPostRegistrarAccidente,
    bodyReq: model.ReqBodyPostRegistrarAccidente,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/accidente/${pathReq.codigoApp}/${pathReq.accidenteId}`,
      model.ReqBodyPostRegistrarAccidente.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ActualizarAseguradosPoliza(
    queryReq: model.ReqQueryPutActualizarAseguradosPoliza,
    bodyReq: model.ReqBodyPutActualizarAseguradosPoliza,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.put(
      `${appConstants.apis.autoservicios}/areaPrivada/poliza/asegurados/`,
      model.ReqBodyPutActualizarAseguradosPoliza.create(bodyReq),
      { params: { ...model.ReqQueryPutActualizarAseguradosPoliza.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerAseguradosPoliza(
    pathReq: model.ReqPathGetObtenerAseguradosPoliza,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/poliza/asegurados/${pathReq.codigoApp}/${pathReq.numeroPoliza}`,
      { ...reqOptions }
    );
  }

  public ObtenerAseguradosTipoUsuario(
    pathReq: model.ReqPathGetObtenerAseguradosTipoUsuario,
    queryReq: model.ReqQueryGetObtenerAseguradosTipoUsuario,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/poliza/${pathReq.numeroPoliza}/exclusiones/asegurados/`,
      { params: { ...model.ReqQueryGetObtenerAseguradosTipoUsuario.create(queryReq) }, ...reqOptions }
    );
  }

  public ActualizarDatosPersonalesAsegurado(
    queryReq: model.ReqQueryPutActualizarDatosPersonalesAsegurado,
    bodyReq?: model.ReqFormDataPutActualizarDatosPersonalesAsegurado,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.put(
      `${appConstants.apis.autoservicios}/areaPrivada/poliza/asegurados/datosPersonales/`,
      model.ReqFormDataPutActualizarDatosPersonalesAsegurado.create(bodyReq),
      { params: { ...model.ReqQueryPutActualizarDatosPersonalesAsegurado.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerExclusionesPorAsegurado(
    pathReq: model.ReqPathGetObtenerExclusionesPorAsegurado,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/coberturas/exclusiones/${pathReq.codigoApp}/${pathReq.numeroPoliza}`,
      { ...reqOptions }
    );
  }

  public ObtenerObservacionesPorAsegurado(
    pathReq: model.ReqPathGetObtenerObservacionesPorAsegurado,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/coberturas/observaciones/${pathReq.codigoApp}/${pathReq.numeroPoliza}`,
      { ...reqOptions }
    );
  }

  public ObtenerRedesAtencionAmbuHospiMat(
    pathReq: model.ReqPathGetObtenerRedesAtencionAmbuHospiMat,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/coberturas/redesAtencion/${pathReq.codigoApp}/${pathReq.numeroPoliza}/${pathReq.tipoCobertura}`,
      { ...reqOptions }
    );
  }

  public ObtenerContratanteEPS(
    pathReq: model.ReqPathGetObtenerContratanteEPS,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/eps/datosContratante/${pathReq.codigoApp}/${pathReq.numeroContrato}`,
      { ...reqOptions }
    );
  }

  public DescargarFormatoSolicitud(
    pathReq: model.ReqPathGetDescargarFormatoSolicitud,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/reembolso/formatoSolicitud/${pathReq.codigoApp}/${pathReq.codCia}/${pathReq.codRamo}/${pathReq.codModalidad}`,
      { ...reqOptions }
    );
  }

  public ListarPoliza(pathReq: model.ReqPathGetListarPoliza, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/reembolso/listarPolizasSalud/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public RegistrarPreSolicitudReembolso(
    pathReq: model.ReqPathPostRegistrarPreSolicitudReembolso,
    bodyReq?: model.ReqBodyPostRegistrarPreSolicitudReembolso,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/reembolso/registrar/${pathReq.codigoApp}`,
      model.ReqBodyPostRegistrarPreSolicitudReembolso.create(bodyReq),
      { ...reqOptions }
    );
  }

  public RegistrarArchivosAnexos(
    pathReq: model.ReqPathPostRegistrarArchivosAnexos,
    bodyReq?: model.ReqFormDataPostRegistrarArchivosAnexos,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/reembolso/cargarDocumento/${pathReq.codigoApp}`,
      model.ReqFormDataPostRegistrarArchivosAnexos.create(bodyReq),
      { ...reqOptions }
    );
  }

  public EliminarArchivosAnexos(
    pathReq: model.ReqPathPostEliminarArchivosAnexos,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/reembolso/eliminarDocumento/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public ConfirmarPreSolicitudReembolso(
    pathReq: model.ReqPathPostConfirmarPreSolicitudReembolso,
    bodyReq?: model.ReqBodyPostConfirmarPreSolicitudReembolso,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/reembolso/confirmar/${pathReq.codigoApp}`,
      model.ReqBodyPostConfirmarPreSolicitudReembolso.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ObtenerSolicitudesReembolso(
    pathReq: model.ReqPathGetObtenerSolicitudesReembolso,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/reembolso/buscarPorCliente/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public ObtenerSolicitudesReembolsoListaUnica(
    pathReq: model.ReqPathGetObtenerSolicitudesReembolsoListaUnica,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/reembolso/listaUnica/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public ObtenerProveedor(pathReq: model.ReqPathGetObtenerProveedor, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/reembolso/obtenerProveedor/${pathReq.codigoApp}/${pathReq.numeroRuc}`,
      { ...reqOptions }
    );
  }

  public ListarBeneficio(
    pathReq: model.ReqPathGetListarBeneficio,
    queryReq: model.ReqQueryGetListarBeneficio,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/reembolso/listarBeneficios/${pathReq.codigoApp}`,
      { params: { ...model.ReqQueryGetListarBeneficio.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerDetalleSolicitudReembolso(
    pathReq: model.ReqPathGetObtenerDetalleSolicitudReembolso,
    queryReq: model.ReqQueryGetObtenerDetalleSolicitudReembolso,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/reembolso/detalleSolicitud/${pathReq.codigoApp}`,
      { params: { ...model.ReqQueryGetObtenerDetalleSolicitudReembolso.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerCantidadSolicitudesPendientes(
    pathReq: model.ReqPathGetObtenerCantidadSolicitudesPendientes,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/reembolso/obtenerCantidadPendientes/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public ObtenerDocumentoReembolso(
    pathReq: model.ReqPathGetObtenerDocumentoReembolso,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/reembolso/documento/${pathReq.codigoApp}/${pathReq.numeroSolicitud}/${pathReq.idetificadorDocumento}`,
      { ...reqOptions }
    );
  }

  public ObtenerCartasGarantia(
    pathReq: model.ReqPathGetObtenerCartasGarantia,
    queryReq: model.ReqQueryGetObtenerCartasGarantia,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/cartasGarantia/${pathReq.codigoApp}`,
      { params: { ...model.ReqQueryGetObtenerCartasGarantia.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerClinicas(
    pathReq: model.ReqPathGetObtenerClinicas,
    queryReq: model.ReqQueryGetObtenerClinicas,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/clinicas/${pathReq.codigoApp}/${pathReq.numeroPoliza}`,
      { params: { ...model.ReqQueryGetObtenerClinicas.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerClinicasNoUsuarioSalud(
    pathReq: model.ReqPathGetObtenerClinicasNoUsuarioSalud,
    queryReq: model.ReqQueryGetObtenerClinicasNoUsuarioSalud,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/salud/clinicas/${pathReq.codigoApp}`, {
      params: { ...model.ReqQueryGetObtenerClinicasNoUsuarioSalud.create(queryReq) },
      ...reqOptions
    });
  }

  public ObtenerClinicasGeneral(
    queryReq: model.ReqQueryGetObtenerClinicasGeneral,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/salud/clinicas`, {
      params: { ...model.ReqQueryGetObtenerClinicasGeneral.create(queryReq) },
      ...reqOptions
    });
  }

  public RegistrarClinicaFavorita(
    pathReq: model.ReqPathPostRegistrarClinicaFavorita,
    bodyReq: model.ReqBodyPostRegistrarClinicaFavorita,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/clinicas/favorito/${pathReq.codigoApp}/${pathReq.clinicaId}`,
      model.ReqBodyPostRegistrarClinicaFavorita.create(bodyReq),
      { ...reqOptions }
    );
  }

  public CoberturasDetalleClinica(
    pathReq: model.ReqPathGetCoberturasDetalleClinica,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/clinicas/coberturas/${pathReq.codigoApp}/${pathReq.clinicaId}/${pathReq.numeroPoliza}`,
      { ...reqOptions }
    );
  }

  public ObtenerPolizasPorClienteClinica(
    pathReq: model.ReqPathGetObtenerPolizasPorClienteClinica,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/clinicas/polizas/${pathReq.codigoApp}/${pathReq.identificadorClinica}`,
      { ...reqOptions }
    );
  }

  public ObtenerImagenClinica(
    pathReq: model.ReqPathGetObtenerImagenClinica,
    queryReq: model.ReqQueryGetObtenerImagenClinica,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/clinica/imagen/${pathReq.codigoApp}`,
      { params: { ...model.ReqQueryGetObtenerImagenClinica.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerImagenesClinica(
    pathReq: model.ReqPathGetObtenerImagenesClinica,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/clinica/imagenes/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public ObtenerClinicasAgendables(
    pathReq: model.ReqPathGetObtenerClinicasAgendables,
    queryReq: model.ReqQueryGetObtenerClinicasAgendables,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/cita/clinicasAgendables/${pathReq.codigoApp}`,
      { params: { ...model.ReqQueryGetObtenerClinicasAgendables.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerDetalleCita(
    pathReq: model.ReqPathGetObtenerDetalleCita,
    queryReq: model.ReqQueryGetObtenerDetalleCita,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/cita/detalleCita/${pathReq.codigoApp}/${pathReq.clinicaId}/${pathReq.numeroPoliza}`,
      { params: { ...model.ReqQueryGetObtenerDetalleCita.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerFechas(
    pathReq: model.ReqPathGetObtenerFechas,
    queryReq: model.ReqQueryGetObtenerFechas,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/cita/fechasDisponibles/${pathReq.codigoApp}/${pathReq.clinicaId}/${pathReq.especialidadId}`,
      { params: { ...model.ReqQueryGetObtenerFechas.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerHorarios(
    pathReq: model.ReqPathGetObtenerHorarios,
    queryReq: model.ReqQueryGetObtenerHorarios,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/cita/horariosDisponibles/${pathReq.codigoApp}/${pathReq.clinicaId}/${pathReq.especialidadId}/${pathReq.fecha}`,
      { params: { ...model.ReqQueryGetObtenerHorarios.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerDoctorPorFechaYHora(
    pathReq: model.ReqPathGetObtenerDoctorPorFechaYHora,
    queryReq: model.ReqQueryGetObtenerDoctorPorFechaYHora,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/cita/doctores/${pathReq.codigoApp}/${pathReq.clinicaId}/${pathReq.especialidadId}/${pathReq.fecha}/${pathReq.horaInicio}`,
      { params: { ...model.ReqQueryGetObtenerDoctorPorFechaYHora.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerDoctores(
    pathReq: model.ReqPathGetObtenerDoctores,
    queryReq: model.ReqQueryGetObtenerDoctores,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/cita/doctores/${pathReq.codigoApp}/${pathReq.clinicaId}/${pathReq.especialidadId}`,
      { params: { ...model.ReqQueryGetObtenerDoctores.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerFechasPorDoctor(
    pathReq: model.ReqPathGetObtenerFechasPorDoctor,
    queryReq: model.ReqQueryGetObtenerFechasPorDoctor,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/cita/fechasDisponibles/${pathReq.codigoApp}/${pathReq.clinicaId}/${pathReq.especialidadId}/${pathReq.doctorId}`,
      { params: { ...model.ReqQueryGetObtenerFechasPorDoctor.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerHorariosPorDoctor(
    pathReq: model.ReqPathGetObtenerHorariosPorDoctor,
    queryReq: model.ReqQueryGetObtenerHorariosPorDoctor,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/cita/horariosDisponibles/${pathReq.codigoApp}/${pathReq.clinicaId}/${pathReq.especialidadId}/${pathReq.doctorId}/${pathReq.fecha}`,
      { params: { ...model.ReqQueryGetObtenerHorariosPorDoctor.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerPacientesXTitular(
    pathReq: model.ReqPathGetObtenerPacientesXTitular,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/cita/pacientes/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public ObtenerPacientesXUsuarioLogueado(
    pathReq: model.ReqPathPostObtenerPacientesXUsuarioLogueado,
    bodyReq: model.ReqBodyPostObtenerPacientesXUsuarioLogueado,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/cita/pacientes/usuario/${pathReq.codigoApp}`,
      model.ReqBodyPostObtenerPacientesXUsuarioLogueado.create(bodyReq),
      { ...reqOptions }
    );
  }

  public RegistrarCita(
    pathReq: model.ReqPathPostRegistrarCita,
    bodyReq: model.ReqBodyPostRegistrarCita,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/cita/${pathReq.codigoApp}`,
      model.ReqBodyPostRegistrarCita.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ModificarCita(
    pathReq: model.ReqPathPutModificarCita,
    bodyReq: model.ReqBodyPutModificarCita,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.put(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/cita/${pathReq.codigoApp}/${pathReq.citaId}`,
      model.ReqBodyPutModificarCita.create(bodyReq),
      { ...reqOptions }
    );
  }

  public AnularCita(pathReq: model.ReqPathDeleteAnularCita, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.delete(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/cita/${pathReq.codigoApp}/${pathReq.citaId}`,
      { ...reqOptions }
    );
  }

  public ObtenerCitas(
    pathReq: model.ReqPathGetObtenerCitas,
    queryReq: model.ReqQueryGetObtenerCitas,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/salud/citas/${pathReq.codigoApp}`, {
      params: { ...model.ReqQueryGetObtenerCitas.create(queryReq) },
      ...reqOptions
    });
  }

  public ObtenerCitasAsegurado(
    pathReq: model.ReqPathGetObtenerCitasAsegurado,
    queryReq: model.ReqQueryGetObtenerCitasAsegurado,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/citas/asegurado/${pathReq.codigoApp}`,
      { params: { ...model.ReqQueryGetObtenerCitasAsegurado.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerCita(
    pathReq: model.ReqPathGetObtenerCita,
    queryReq: model.ReqQueryGetObtenerCita,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/salud/citas/${pathReq.citaId}/detalle`,
      { params: { ...model.ReqQueryGetObtenerCita.create(queryReq) }, ...reqOptions }
    );
  }

  public ListarPolizasSCTR(
    pathReq: model.ReqPathGetListarPolizasSCTR,
    queryReq: model.ReqQueryGetListarPolizasSCTR,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/polizas/sctr/buscarPorCliente/${pathReq.codigoApp}`,
      { params: { ...model.ReqQueryGetListarPolizasSCTR.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerConstanciasSCTR(
    pathReq: model.ReqPathGetObtenerConstanciasSCTR,
    queryReq: model.ReqQueryGetObtenerConstanciasSCTR,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/sctr/constancias/${pathReq.codigoApp}/${pathReq.numeroPoliza}`,
      { params: { ...model.ReqQueryGetObtenerConstanciasSCTR.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerAseguradosConstanciaSCTR(
    pathReq: model.ReqPathGetObtenerAseguradosConstanciaSCTR,
    queryReq: model.ReqQueryGetObtenerAseguradosConstanciaSCTR,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/sctr/constancias/asegurados/${pathReq.codigoApp}/${pathReq.numeroConstancia}`,
      { params: { ...model.ReqQueryGetObtenerAseguradosConstanciaSCTR.create(queryReq) }, ...reqOptions }
    );
  }

  public DescargarConstanciaSCTR(
    pathReq: model.ReqPathGetDescargarConstanciaSCTR,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/sctr/constancias/base64/${pathReq.codigoApp}/${pathReq.numeroPoliza}/${pathReq.numeroConstancia}`,
      { ...reqOptions }
    );
  }

  public ListarPeridoSCTR(
    pathReq: model.ReqPathGetListarPeridoSCTR,
    queryReq: model.ReqQueryGetListarPeridoSCTR,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/polizas/sctr/${pathReq.numeroPoliza}/periodos`,
      { params: { ...model.ReqQueryGetListarPeridoSCTR.create(queryReq) }, ...reqOptions }
    );
  }

  public ListarPeridoAdicionalesSCTR(
    pathReq: model.ReqPathGetListarPeridoAdicionalesSCTR,
    queryReq: model.ReqQueryGetListarPeridoAdicionalesSCTR,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/polizas/sctr/${pathReq.numeroPoliza}/periodos/adicionales`,
      { params: { ...model.ReqQueryGetListarPeridoAdicionalesSCTR.create(queryReq) }, ...reqOptions }
    );
  }

  public FiltrosConstancias(
    pathReq: model.ReqPathGetFiltrosConstancias,
    queryReq: model.ReqQueryGetFiltrosConstancias,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/polizas/sctr/${pathReq.numeroPoliza}/filtros`,
      { params: { ...model.ReqQueryGetFiltrosConstancias.create(queryReq) }, ...reqOptions }
    );
  }

  public ListarRiesgoSCTR(
    pathReq: model.ReqPathGetListarRiesgoSCTR,
    queryReq: model.ReqQueryGetListarRiesgoSCTR,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/polizas/sctr/${pathReq.numeroPoliza}/riesgo`,
      { params: { ...model.ReqQueryGetListarRiesgoSCTR.create(queryReq) }, ...reqOptions }
    );
  }

  public DescargarPlanillaSCTR(reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/polizas/sctr/descarga/planilla`, {
      ...reqOptions
    });
  }

  public SubirValidarPlanillaSCTR(
    queryReq: model.ReqQueryPostSubirValidarPlanillaSCTR,
    bodyReq?: model.ReqFormDataPostSubirValidarPlanillaSCTR,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/polizas/sctr/subir/planilla`,
      model.ReqFormDataPostSubirValidarPlanillaSCTR.create(bodyReq),
      { params: { ...model.ReqQueryPostSubirValidarPlanillaSCTR.create(queryReq) }, ...reqOptions }
    );
  }

  public DescargarValidacionPlanillaSCTR(
    queryReq: model.ReqQueryGetDescargarValidacionPlanillaSCTR,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/polizas/sctr/descargar/planilla/errores`,
      { params: { ...model.ReqQueryGetDescargarValidacionPlanillaSCTR.create(queryReq) }, ...reqOptions }
    );
  }

  public ListarDeclaracionResumenSCTR(
    queryReq: model.ReqQueryGetListarDeclaracionResumenSCTR,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/polizas/sctr/declaracion/resumen`, {
      params: { ...model.ReqQueryGetListarDeclaracionResumenSCTR.create(queryReq) },
      ...reqOptions
    });
  }

  public ListarInclusionResumenSCTR(
    queryReq: model.ReqQueryGetListarInclusionResumenSCTR,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/polizas/sctr/inclusion/resumen`, {
      params: { ...model.ReqQueryGetListarInclusionResumenSCTR.create(queryReq) },
      ...reqOptions
    });
  }

  public GenerarDeclaracionSCTR(
    queryReq: model.ReqQueryPostGenerarDeclaracionSCTR,
    bodyReq: model.ReqBodyPostGenerarDeclaracionSCTR,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/polizas/sctr/declaracion`,
      model.ReqBodyPostGenerarDeclaracionSCTR.create(bodyReq),
      { params: { ...model.ReqQueryPostGenerarDeclaracionSCTR.create(queryReq) }, ...reqOptions }
    );
  }

  public InclusionFacturacionSCTR(
    queryReq: model.ReqQueryGetInclusionFacturacionSCTR,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/polizas/sctr/inclusion/facturacion`, {
      params: { ...model.ReqQueryGetInclusionFacturacionSCTR.create(queryReq) },
      ...reqOptions
    });
  }

  public GenerarInclusionSCTR(
    queryReq: model.ReqQueryPostGenerarInclusionSCTR,
    bodyReq: model.ReqBodyPostGenerarInclusionSCTR,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/polizas/sctr/inclusion`,
      model.ReqBodyPostGenerarInclusionSCTR.create(bodyReq),
      { params: { ...model.ReqQueryPostGenerarInclusionSCTR.create(queryReq) }, ...reqOptions }
    );
  }

  public ListarPeriodosFiltros(
    pathReq: model.ReqPathGetListarPeriodosFiltros,
    queryReq: model.ReqQueryGetListarPeriodosFiltros,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/polizas/sctr/${pathReq.numeroPoliza}/periodos/filtros`,
      { params: { ...model.ReqQueryGetListarPeriodosFiltros.create(queryReq) }, ...reqOptions }
    );
  }

  public ListarRecibosPorPeriodo(
    pathReq: model.ReqPathPostListarRecibosPorPeriodo,
    queryReq: model.ReqQueryPostListarRecibosPorPeriodo,
    bodyReq?: model.ReqBodyPostListarRecibosPorPeriodo,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/polizas/sctr/${pathReq.numeroPoliza}/periodos/recibos`,
      model.ReqBodyPostListarRecibosPorPeriodo.create(bodyReq),
      { params: { ...model.ReqQueryPostListarRecibosPorPeriodo.create(queryReq) }, ...reqOptions }
    );
  }

  public ActualizarAseguradoSctr(
    pathReq: model.ReqPathPutActualizarAseguradoSctr,
    queryReq: model.ReqQueryPutActualizarAseguradoSctr,
    bodyReq: model.ReqBodyPutActualizarAseguradoSctr,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.put(
      `${appConstants.apis.autoservicios}/areaPrivada/polizas/sctr/${pathReq.numeroPoliza}/periodos/planilla/asegurados/${pathReq.codAsegurado}`,
      model.ReqBodyPutActualizarAseguradoSctr.create(bodyReq),
      { params: { ...model.ReqQueryPutActualizarAseguradoSctr.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerPlanillaAsegurados(
    pathReq: model.ReqPathGetObtenerPlanillaAsegurados,
    queryReq: model.ReqQueryGetObtenerPlanillaAsegurados,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/polizas/sctr/${pathReq.numeroPoliza}/periodos/planilla/asegurados`,
      { params: { ...model.ReqQueryGetObtenerPlanillaAsegurados.create(queryReq) }, ...reqOptions }
    );
  }

  public DescargarConstanciaManualSCTR(
    pathReq: model.ReqPathPostDescargarConstanciaManualSCTR,
    queryReq: model.ReqQueryPostDescargarConstanciaManualSCTR,
    bodyReq?: model.ReqBodyPostDescargarConstanciaManualSCTR,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/polizas/sctr/${pathReq.numeroPoliza}/periodos/constancias/descargar`,
      model.ReqBodyPostDescargarConstanciaManualSCTR.create(bodyReq),
      { params: { ...model.ReqQueryPostDescargarConstanciaManualSCTR.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerCoordenadasZonasChofer(
    pathReq: model.ReqPathGetObtenerCoordenadasZonasChofer,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/servicios/chofer/zonas/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public ObtenerCoordenadasZonasMedico(
    pathReq: model.ReqPathGetObtenerCoordenadasZonasMedico,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/servicios/medico/zonas/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public ObtenerServicios(pathReq: model.ReqPathGetObtenerServicios, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/servicios/${pathReq.codigoApp}`, {
      ...reqOptions
    });
  }

  public SolicitarChoferReemplazo(
    pathReq: model.ReqPathPostSolicitarChoferReemplazo,
    bodyReq: model.ReqBodyPostSolicitarChoferReemplazo,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/servicios/chofer/solicitar/${pathReq.codigoApp}`,
      model.ReqBodyPostSolicitarChoferReemplazo.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ObtenerSolicitudes(
    pathReq: model.ReqPathGetObtenerSolicitudes,
    queryReq: model.ReqQueryGetObtenerSolicitudes,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/servicios/buscarPorCliente/${pathReq.codigoApp}`,
      { params: { ...model.ReqQueryGetObtenerSolicitudes.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerSolicitudesPost(
    queryReq: model.ReqQueryGetObtenerSolicitudesPost,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/servicios/buscarPorCliente`, {
      params: { ...model.ReqQueryGetObtenerSolicitudesPost.create(queryReq) },
      ...reqOptions
    });
  }

  public ObtenerSolicitudesFiltro(
    queryReq: model.ReqQueryGetObtenerSolicitudesFiltro,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/servicios/solicitudes`, {
      params: { ...model.ReqQueryGetObtenerSolicitudesFiltro.create(queryReq) },
      ...reqOptions
    });
  }

  public ObtenerTipoVehiculos(pathReq: model.ReqPathGetObtenerTipoVehiculos, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/servicios/chofer/tipoVehiculos/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public ObtenerPolizasMedicoDomicilio(
    pathReq: model.ReqPathGetObtenerPolizasMedicoDomicilio,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/servicios/medico/tipoPolizas/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public SolicitarMedicoDomicilio(
    pathReq: model.ReqPathPostSolicitarMedicoDomicilio,
    bodyReq: model.ReqBodyPostSolicitarMedicoDomicilio,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/servicios/medico/solicitar/${pathReq.codigoApp}`,
      model.ReqBodyPostSolicitarMedicoDomicilio.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ObtenerVehiculoSoat(
    pathReq: model.ReqPathGetObtenerVehiculoSoat,
    queryReq: model.ReqQueryGetObtenerVehiculoSoat,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/soat/vehiculo/${pathReq.placa}`, {
      params: { ...model.ReqQueryGetObtenerVehiculoSoat.create(queryReq) },
      ...reqOptions
    });
  }

  public ObtenerVehiculoSoatPorPoliza(
    pathReq: model.ReqPathGetObtenerVehiculoSoatPorPoliza,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/soat/vehiculo/${pathReq.codigoApp}/${pathReq.numeroPoliza}`,
      { ...reqOptions }
    );
  }

  public ObtenerAsientos(pathReq: model.ReqPathGetObtenerAsientos, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/soat/asientos/${pathReq.codigoApp}/${pathReq.tipoVehiculoId}/${pathReq.marcaId}/${pathReq.modeloId}`,
      { ...reqOptions }
    );
  }

  public ObtenerMarcas(pathReq: model.ReqPathGetObtenerMarcas, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/oim_polizas/api/soat/marca/1/${pathReq.tipoVehiculoId}`,
      { ...reqOptions }
    );
  }

  public ObtenerModelos(pathReq: model.ReqPathGetObtenerModelos, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/oim_polizas/api/soat/modelo/1/${pathReq.tipoVehiculoId}/${pathReq.marcaId}`,
      { ...reqOptions }
    );
  }

  public ObtenerContratanteSOAT(
    pathReq: model.ReqPathGetObtenerContratanteSOAT,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/soat/contratante/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public ObtenerTiposUso(pathReq: model.ReqPathGetObtenerTiposUso, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/soat/tiposUso/${pathReq.codigoApp}/${pathReq.tipoVehiculoId}/${pathReq.departamentoId}`,
      { ...reqOptions }
    );
  }

  public GenerarCotizacionSOAT(
    pathReq: model.ReqPathPostGenerarCotizacionSOAT,
    bodyReq: model.ReqBodyPostGenerarCotizacionSOAT,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/soat/cotizacion/${pathReq.codigoApp}`,
      model.ReqBodyPostGenerarCotizacionSOAT.create(bodyReq),
      { ...reqOptions }
    );
  }

  public GenerarEmisionSOAT(
    pathReq: model.ReqPathPostGenerarEmisionSOAT,
    bodyReq: model.ReqBodyPostGenerarEmisionSOAT,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/soat/emision/${pathReq.codigoApp}`,
      model.ReqBodyPostGenerarEmisionSOAT.create(bodyReq),
      { ...reqOptions }
    );
  }

  public GenerarEmisionSOATLyra(
    pathReq: model.ReqPathPostGenerarEmisionSOATLyra,
    bodyReq: model.ReqBodyPostGenerarEmisionSOATLyra,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/soat/emision/lyra/${pathReq.codigoApp}`,
      model.ReqBodyPostGenerarEmisionSOATLyra.create(bodyReq),
      { ...reqOptions }
    );
  }

  public VerificarSerialVin(
    pathReq: model.ReqPathPostVerificarSerialVin,
    bodyReq: model.ReqBodyPostVerificarSerialVin,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/soat/datos/vehiculo/${pathReq.codigoApp}`,
      model.ReqBodyPostVerificarSerialVin.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ObtenerDistritosDelivery(
    pathReq: model.ReqPathGetObtenerDistritosDelivery,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/soat/delivery/distritos/${pathReq.codigoApp}/${pathReq.codigoDepartamento}/${pathReq.codigoProvincia}`,
      { ...reqOptions }
    );
  }

  public ObtenerUrbanizacionesDelivery(
    pathReq: model.ReqPathGetObtenerUrbanizacionesDelivery,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/soat/delivery/urbanizaciones/${pathReq.codigoApp}/${pathReq.codigoDepartamento}/${pathReq.codigoProvincia}/${pathReq.codigoDistrito}`,
      { ...reqOptions }
    );
  }

  public EnviarCorreoSOAT(
    pathReq: model.ReqPathPostEnviarCorreoSOAT,
    bodyReq: model.ReqBodyPostEnviarCorreoSOAT,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/soat/envioCorreo/${pathReq.codigoApp}`,
      model.ReqBodyPostEnviarCorreoSOAT.create(bodyReq),
      { ...reqOptions }
    );
  }

  public EnviarReferido(
    pathReq: model.ReqPathPostEnviarReferido,
    bodyReq: model.ReqBodyPostEnviarReferido,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/soat/envioReferido/${pathReq.codigoApp}`,
      model.ReqBodyPostEnviarReferido.create(bodyReq),
      { ...reqOptions }
    );
  }

  public CotizarRenovacionSoat(
    pathReq: model.ReqPathGetCotizarRenovacionSoat,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/soat/cotizacion/${pathReq.codigoApp}/${pathReq.numeroPoliza}`,
      { ...reqOptions }
    );
  }

  public ObtenerDatosSOAT(pathReq: model.ReqPathGetObtenerDatosSOAT, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/soat/${pathReq.codigoApp}/${pathReq.placa}`,
      { ...reqOptions }
    );
  }

  public OlvidarSOAT(pathReq: model.ReqPathDeleteOlvidarSOAT, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.delete(
      `${appConstants.apis.autoservicios}/areaPrivada/soat/${pathReq.codigoApp}/${pathReq.placa}`,
      { ...reqOptions }
    );
  }

  public ObtenerListaSOAT(pathReq: model.ReqPathGetObtenerListaSOAT, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/soat/${pathReq.codigoApp}`, {
      ...reqOptions
    });
  }

  public ObtenerListaAutoinspeccion(
    pathReq: model.ReqPathGetObtenerListaAutoinspeccion,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/autoinspeccion/pendientes/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public RegistrarFoto(
    pathReq: model.ReqPathPostRegistrarFoto,
    bodyReq?: model.ReqFormDataPostRegistrarFoto,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/autoinspeccion/foto/${pathReq.codigoApp}`,
      model.ReqFormDataPostRegistrarFoto.create(bodyReq),
      { ...reqOptions }
    );
  }

  public RegistrarAutoinspeccion(
    pathReq: model.ReqPathPostRegistrarAutoinspeccion,
    bodyReq: model.ReqBodyPostRegistrarAutoinspeccion,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/autoinspeccion/${pathReq.codigoApp}`,
      model.ReqBodyPostRegistrarAutoinspeccion.create(bodyReq),
      { ...reqOptions }
    );
  }

  public RegistrarInspeccion(
    pathReq: model.ReqPathPostRegistrarInspeccion,
    bodyReq: model.ReqBodyPostRegistrarInspeccion,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/registrar/inspeccion/${pathReq.codigoApp}`,
      model.ReqBodyPostRegistrarInspeccion.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ActualizarInspeccion(pathReq: model.ReqPathPutActualizarInspeccion, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.put(
      `${appConstants.apis.autoservicios}/areaPrivada/actualizar/inspeccion/${pathReq.codigoApp}/${pathReq.numeroInspeccion}`,
      { ...reqOptions }
    );
  }

  public ObtenerObservacionPerito(
    pathReq: model.ReqPathGetObtenerObservacionPerito,
    queryReq: model.ReqQueryGetObtenerObservacionPerito,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/autoinspeccion/observacion/${pathReq.codigoApp}`,
      { params: { ...model.ReqQueryGetObtenerObservacionPerito.create(queryReq) }, ...reqOptions }
    );
  }

  public FotoTipoVehiculo(
    pathReq: model.ReqPathGetFotoTipoVehiculo,
    queryReq: model.ReqQueryGetFotoTipoVehiculo,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/autoinspeccion/foto/vehiculo/${pathReq.codigoApp}`,
      { params: { ...model.ReqQueryGetFotoTipoVehiculo.create(queryReq) }, ...reqOptions }
    );
  }

  public RangoHorasIncidentesPath(
    pathReq: model.ReqPathGetRangoHorasIncidentesPath,
    queryReq: model.ReqQueryGetRangoHorasIncidentesPath,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/hogar/incidentes/rangosHorarios/${pathReq.codigoApp}`,
      { params: { ...model.ReqQueryGetRangoHorasIncidentesPath.create(queryReq) }, ...reqOptions }
    );
  }

  public TiposIncidentesHogarPath(
    pathReq: model.ReqPathGetTiposIncidentesHogarPath,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/hogar/incidentes/tipos/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public RegistrarIncidenteHogarPath(
    bodyReq?: model.ReqFormDataPostRegistrarIncidenteHogarPath,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/hogar/incidentes/registrar`,
      model.ReqFormDataPostRegistrarIncidenteHogarPath.create(bodyReq),
      { ...reqOptions }
    );
  }

  public CoberturasDeduciblesPath(
    pathReq: model.ReqPathGetCoberturasDeduciblesPath,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/hogar/deducibles/${pathReq.codigoApp}/${pathReq.numeroPoliza}/${pathReq.numeroSuplemento}/${pathReq.numeroRiesgo}/${pathReq.codRamo}`,
      { ...reqOptions }
    );
  }

  public ObtenerCoberturasHogarRiesgo(
    pathReq: model.ReqPathGetObtenerCoberturasHogarRiesgo,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/hogar/coberturas/${pathReq.codigoApp}/${pathReq.numeroPoliza}/${pathReq.numeroSuplemento}/${pathReq.numeroRiesgo}/${pathReq.codRamo}`,
      { ...reqOptions }
    );
  }

  public ComparacionHogarProductos(
    pathReq: model.ReqPathGetComparacionHogarProductos,
    queryReq: model.ReqQueryGetComparacionHogarProductos,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/hogar/comparacion/productos/${pathReq.codigoApp}`,
      { params: { ...model.ReqQueryGetComparacionHogarProductos.create(queryReq) }, ...reqOptions }
    );
  }

  public ComparacionHogarCoberturas(
    pathReq: model.ReqPathGetComparacionHogarCoberturas,
    queryReq: model.ReqQueryGetComparacionHogarCoberturas,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/hogar/comparacion/coberturas/${pathReq.codigoApp}`,
      { params: { ...model.ReqQueryGetComparacionHogarCoberturas.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerDisponibilidadSolicitudAsistencia(
    pathReq: model.ReqPathGetObtenerDisponibilidadSolicitudAsistencia,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/asistencias/solicitud/disponibilidad/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public ObtenerSolicitudAsistencia(
    pathReq: model.ReqPathGetObtenerSolicitudAsistencia,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/asistencias/solicitud/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public RegistrarSolicitudAsistencia(
    pathReq: model.ReqPathPostRegistrarSolicitudAsistencia,
    bodyReq: model.ReqBodyPostRegistrarSolicitudAsistencia,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/asistencias/solicitud/${pathReq.codigoApp}`,
      model.ReqBodyPostRegistrarSolicitudAsistencia.create(bodyReq),
      { ...reqOptions }
    );
  }

  public EliminarSolicitudAsistencia(
    pathReq: model.ReqPathDeleteEliminarSolicitudAsistencia,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.delete(
      `${appConstants.apis.autoservicios}/areaPrivada/asistencias/solicitud/${pathReq.codigoApp}/${pathReq.solicitudAsistenciaId}`,
      { ...reqOptions }
    );
  }

  public ObtenerAsistenciasPorUsuario(
    pathReq: model.ReqPathGetObtenerAsistenciasPorUsuario,
    queryReq: model.ReqQueryGetObtenerAsistenciasPorUsuario,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/asistencias/${pathReq.codigoApp}`, {
      params: { ...model.ReqQueryGetObtenerAsistenciasPorUsuario.create(queryReq) },
      ...reqOptions
    });
  }

  public ObtenerAsistencia(
    pathReq: model.ReqPathGetObtenerAsistencia,
    queryReq: model.ReqQueryGetObtenerAsistencia,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/asistencia/${pathReq.codigoApp}/${pathReq.asistenciaId}`,
      { params: { ...model.ReqQueryGetObtenerAsistencia.create(queryReq) }, ...reqOptions }
    );
  }

  public EliminarAsistencia(pathReq: model.ReqPathDeleteEliminarAsistencia, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.delete(
      `${appConstants.apis.autoservicios}/areaPrivada/asistencia/${pathReq.codigoApp}/${pathReq.asistenciaId}`,
      { ...reqOptions }
    );
  }

  public ObtenerSiniestro(
    pathReq: model.ReqPathGetObtenerSiniestro,
    queryReq: model.ReqQueryGetObtenerSiniestro,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/siniestros/buscarPorCliente/${pathReq.codigoApp}`,
      { params: { ...model.ReqQueryGetObtenerSiniestro.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerAniosSiniestro(
    pathReq: model.ReqPathGetObtenerAniosSiniestro,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/siniestros/buscarPorCliente/anios/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public DescargarEcu(pathReq: model.ReqPathGetDescargarEcu, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/ecu/${pathReq.codigoApp}`, {
      ...reqOptions
    });
  }

  public DescargarConprobante(pathReq: model.ReqPathGetDescargarConprobante, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/comprobantespagos/${pathReq.codigoApp}/${pathReq.codCia}/${pathReq.numeroRecibo}/${pathReq.tipo}`,
      { ...reqOptions }
    );
  }

  public ObtenerComprobantePagoHpexstream(
    pathReq: model.ReqPathGetObtenerComprobantePagoHpexstream,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/comprobantespagos/v2/${pathReq.codigoApp}/${pathReq.codCia}/${pathReq.numeroRecibo}/${pathReq.tipoDocumentoPago}`,
      { ...reqOptions }
    );
  }

  public PasarelaFormularioToken(
    pathReq: model.ReqPathPostPasarelaFormularioToken,
    bodyReq: model.ReqBodyPostPasarelaFormularioToken,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/pasarela/lyra/formularioToken/${pathReq.codigoApp}`,
      model.ReqBodyPostPasarelaFormularioToken.create(bodyReq),
      { ...reqOptions }
    );
  }

  public PagarReciboLyra(
    pathReq: model.ReqPathPostPagarReciboLyra,
    bodyReq: model.ReqBodyPostPagarReciboLyra,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/recibo/pago/lyra/${pathReq.codigoApp}`,
      model.ReqBodyPostPagarReciboLyra.create(bodyReq),
      { ...reqOptions }
    );
  }

  public RegistrarTallerFavorito(
    pathReq: model.ReqPathPostRegistrarTallerFavorito,
    bodyReq: model.ReqBodyPostRegistrarTallerFavorito,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/talleres/favorito/${pathReq.codigoApp}/${pathReq.tallerId}`,
      model.ReqBodyPostRegistrarTallerFavorito.create(bodyReq),
      { ...reqOptions }
    );
  }

  public BuscarTallerFavorito(
    pathReq: model.ReqPathGetBuscarTallerFavorito,
    queryReq: model.ReqQueryGetBuscarTallerFavorito,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/talleres/buscar/${pathReq.codigoApp}`, {
      params: { ...model.ReqQueryGetBuscarTallerFavorito.create(queryReq) },
      ...reqOptions
    });
  }

  public BuscarTallerUbigeoFavorito(
    pathReq: model.ReqPathGetBuscarTallerUbigeoFavorito,
    queryReq: model.ReqQueryGetBuscarTallerUbigeoFavorito,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/talleres/buscar/ubigeo/${pathReq.codigoApp}`,
      { params: { ...model.ReqQueryGetBuscarTallerUbigeoFavorito.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerEpsContratanteDatosCardContrato(
    pathReq: model.ReqPathGetObtenerEpsContratanteDatosCardContrato,
    queryReq: model.ReqQueryGetObtenerEpsContratanteDatosCardContrato,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/eps/contratos/${pathReq.nroContrato}/contrato`,
      { params: { ...model.ReqQueryGetObtenerEpsContratanteDatosCardContrato.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerEpsContratanteConceptoPago(
    pathReq: model.ReqPathGetObtenerEpsContratanteConceptoPago,
    queryReq: model.ReqQueryGetObtenerEpsContratanteConceptoPago,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/eps/contratos/${pathReq.nroContrato}/conceptoPago`,
      { params: { ...model.ReqQueryGetObtenerEpsContratanteConceptoPago.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerEpsContratanteDatosCardAfiliados(
    pathReq: model.ReqPathGetObtenerEpsContratanteDatosCardAfiliados,
    queryReq: model.ReqQueryGetObtenerEpsContratanteDatosCardAfiliados,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/eps/contratos/${pathReq.nroContrato}/afiliados`,
      { params: { ...model.ReqQueryGetObtenerEpsContratanteDatosCardAfiliados.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerEpsContratanteDatosDocsPago(
    pathReq: model.ReqPathGetObtenerEpsContratanteDatosDocsPago,
    queryReq: model.ReqQueryGetObtenerEpsContratanteDatosDocsPago,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/eps/contratos/${pathReq.nroContrato}/docsPago`,
      { params: { ...model.ReqQueryGetObtenerEpsContratanteDatosDocsPago.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerEpsContratanteAfiliadosFiltros(
    pathReq: model.ReqPathGetObtenerEpsContratanteAfiliadosFiltros,
    queryReq: model.ReqQueryGetObtenerEpsContratanteAfiliadosFiltros,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/eps/contratos/${pathReq.nroContrato}/filtros`,
      { params: { ...model.ReqQueryGetObtenerEpsContratanteAfiliadosFiltros.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerEpsContratanteAfiliadosDetalle(
    pathReq: model.ReqPathGetObtenerEpsContratanteAfiliadosDetalle,
    queryReq: model.ReqQueryGetObtenerEpsContratanteAfiliadosDetalle,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/eps/contratos/${pathReq.nroContrato}/afiliados/detalle`,
      { params: { ...model.ReqQueryGetObtenerEpsContratanteAfiliadosDetalle.create(queryReq) }, ...reqOptions }
    );
  }

  public ActualizarEpsContratanteAfiliados(
    pathReq: model.ReqPathPutActualizarEpsContratanteAfiliados,
    queryReq: model.ReqQueryPutActualizarEpsContratanteAfiliados,
    bodyReq: model.ReqBodyPutActualizarEpsContratanteAfiliados,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.put(
      `${appConstants.apis.autoservicios}/areaPrivada/eps/contratos/${pathReq.nroContrato}/afiliado`,
      model.ReqBodyPutActualizarEpsContratanteAfiliados.create(bodyReq),
      { params: { ...model.ReqQueryPutActualizarEpsContratanteAfiliados.create(queryReq) }, ...reqOptions }
    );
  }

  public DescargarEpsContratanteManualAfiliado(
    pathReq: model.ReqPathGetDescargarEpsContratanteManualAfiliado,
    queryReq: model.ReqQueryGetDescargarEpsContratanteManualAfiliado,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/eps/contratos/${pathReq.nroContrato}/descarga/manualAfiliado`,
      { params: { ...model.ReqQueryGetDescargarEpsContratanteManualAfiliado.create(queryReq) }, ...reqOptions }
    );
  }

  public RegistrarToken(
    queryReq: model.ReqQueryPostRegistrarToken,
    bodyReq: model.ReqBodyPostRegistrarToken,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/mediktor/tokens`,
      model.ReqBodyPostRegistrarToken.create(bodyReq),
      { params: { ...model.ReqQueryPostRegistrarToken.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerToken(queryReq: model.ReqQueryGetObtenerToken, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/mediktor/tokens`, {
      params: { ...model.ReqQueryGetObtenerToken.create(queryReq) },
      ...reqOptions
    });
  }

  public RegistrarDiagnostico(
    pathReq: model.ReqPathPostRegistrarDiagnostico,
    queryReq: model.ReqQueryPostRegistrarDiagnostico,
    bodyReq: model.ReqBodyPostRegistrarDiagnostico,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/mediktor/tokens/${pathReq.token}/diagnostico`,
      model.ReqBodyPostRegistrarDiagnostico.create(bodyReq),
      { params: { ...model.ReqQueryPostRegistrarDiagnostico.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerBeneficiosAdicionales(
    queryReq: model.ReqQueryGetObtenerBeneficiosAdicionales,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/ppfm/beneficios/adicionales`, {
      params: { ...model.ReqQueryGetObtenerBeneficiosAdicionales.create(queryReq) },
      ...reqOptions
    });
  }

  public ObtenerAseguradosBeneficiosAdicionales(
    queryReq: model.ReqQueryGetObtenerAseguradosBeneficiosAdicionales,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/ppfm/beneficios/asegurados`, {
      params: { ...model.ReqQueryGetObtenerAseguradosBeneficiosAdicionales.create(queryReq) },
      ...reqOptions
    });
  }

  public ObtenerTarjetas(queryReq: model.ReqQueryGetObtenerTarjetas, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/tarjetas`, {
      params: { ...model.ReqQueryGetObtenerTarjetas.create(queryReq) },
      ...reqOptions
    });
  }

  public GuardarTarjeta(
    queryReq: model.ReqQueryPostGuardarTarjeta,
    bodyReq: model.ReqBodyPostGuardarTarjeta,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/tarjetas`,
      model.ReqBodyPostGuardarTarjeta.create(bodyReq),
      { params: { ...model.ReqQueryPostGuardarTarjeta.create(queryReq) }, ...reqOptions }
    );
  }

  public EliminarTarjeta(queryReq: model.ReqQueryDeleteEliminarTarjeta, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.delete(`${appConstants.apis.autoservicios}/areaPrivada/tarjetas`, {
      params: { ...model.ReqQueryDeleteEliminarTarjeta.create(queryReq) },
      ...reqOptions
    });
  }

  public AfiliarTarjeta(
    queryReq: model.ReqQueryPostAfiliarTarjeta,
    bodyReq: model.ReqBodyPostAfiliarTarjeta,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/tarjetas/afiliacion`,
      model.ReqBodyPostAfiliarTarjeta.create(bodyReq),
      { params: { ...model.ReqQueryPostAfiliarTarjeta.create(queryReq) }, ...reqOptions }
    );
  }

  public DesafiliarTarjeta(queryReq: model.ReqQueryDeleteDesafiliarTarjeta, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.delete(`${appConstants.apis.autoservicios}/areaPrivada/tarjetas/afiliacion`, {
      params: { ...model.ReqQueryDeleteDesafiliarTarjeta.create(queryReq) },
      ...reqOptions
    });
  }

  public ObtenerEstadoMantenimiento(
    queryReq: model.ReqQueryGetObtenerEstadoMantenimiento,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/actuator/maintenance`, {
      params: { ...model.ReqQueryGetObtenerEstadoMantenimiento.create(queryReq) },
      ...reqOptions
    });
  }

  public ObtenerListaCartasGarantia(
    queryReq: model.ReqQueryGetObtenerListaCartasGarantia,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/cartasgarantia`, {
      params: { ...model.ReqQueryGetObtenerListaCartasGarantia.create(queryReq) },
      ...reqOptions
    });
  }

  public ObtenerTipoEstados(queryReq: model.ReqQueryGetObtenerTipoEstados, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/cartasgarantia/tipoEstados`, {
      params: { ...model.ReqQueryGetObtenerTipoEstados.create(queryReq) },
      ...reqOptions
    });
  }

  public ObtenerClinica(queryReq: model.ReqQueryGetObtenerClinica, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/cartasgarantia/clinicas`, {
      params: { ...model.ReqQueryGetObtenerClinica.create(queryReq) },
      ...reqOptions
    });
  }

  public DescargarCarta(
    pathReq: model.ReqPathGetDescargarCarta,
    queryReq: model.ReqQueryGetDescargarCarta,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.autoservicios}/areaPrivada/cartasgarantia/${pathReq.codCia}/${pathReq.anio}/${pathReq.correlativo}/${pathReq.version}/descarga`,
      { params: { ...model.ReqQueryGetDescargarCarta.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerFiltros(queryReq: model.ReqQueryGetObtenerFiltros, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/cartasgarantia/filtros`, {
      params: { ...model.ReqQueryGetObtenerFiltros.create(queryReq) },
      ...reqOptions
    });
  }

  public ObtenerPlanesCdm(queryReq: model.ReqQueryGetObtenerPlanesCdm, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/clinicadigital/planes`, {
      params: { ...model.ReqQueryGetObtenerPlanesCdm.create(queryReq) },
      ...reqOptions
    });
  }

  public ObtenerParametrosCdm(
    queryReq: model.ReqQueryGetObtenerParametrosCdm,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/clinicadigital/parametros`, {
      params: { ...model.ReqQueryGetObtenerParametrosCdm.create(queryReq) },
      ...reqOptions
    });
  }

  public GenerarCotizacionCdm(
    queryReq: model.ReqQueryPostGenerarCotizacionCdm,
    bodyReq: model.ReqBodyPostGenerarCotizacionCdm,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.autoservicios}/areaPrivada/clinicadigital/cotizaciones`,
      model.ReqBodyPostGenerarCotizacionCdm.create(bodyReq),
      { params: { ...model.ReqQueryPostGenerarCotizacionCdm.create(queryReq) }, ...reqOptions }
    );
  }

  public ObtenerServiciosCdm(queryReq: model.ReqQueryGetObtenerServiciosCdm, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.autoservicios}/areaPrivada/clinicadigital/servicios`, {
      params: { ...model.ReqQueryGetObtenerServiciosCdm.create(queryReq) },
      ...reqOptions
    });
  }
}
