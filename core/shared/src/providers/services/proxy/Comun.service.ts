// tslint:disable

import { HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import * as model from '@mx/core/shared/providers/models';
import { ApiService } from '@mx/core/shared/helpers/api';
import { appConstants } from '@mx/core/shared/providers/constants';

interface ReqOptions {
  default?: any;
  headers?:
    | HttpHeaders
    | {
        [header: string]: string | string[];
      };
  observe?: 'body'; // response, body
  params?:
    | HttpParams
    | {
        [param: string]: string | string[];
      };
  preloader?: boolean;
  reportProgress?: boolean;
  responseType?: 'json';
  retry?: number;
  withCredentials?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class Comun {
  constructor(private readonly _ApiService: ApiService) {}

  public ObtenerObtenerNotificaciones(
    pathReq: model.ReqPathGetObtenerObtenerNotificaciones,
    queryReq: model.ReqQueryGetObtenerObtenerNotificaciones,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.comun}/areaPrivada/notificaciones/${pathReq.codigoApp}`, {
      params: { ...model.ReqQueryGetObtenerObtenerNotificaciones.create(queryReq) },
      ...reqOptions
    });
  }

  public ComunObtenerComercio(pathReq: model.ReqPathGetComunObtenerComercio, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.comun}/tarjeta/comercio/${pathReq.codigoPasarela}/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public ComunGenerarOrdenCompra(
    pathReq: model.ReqPathPostComunGenerarOrdenCompra,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.comun}/tarjeta/ordenCompra/${pathReq.codigoTarjeta}/${pathReq.codigoApp}`,
      { ...reqOptions }
    );
  }

  public McProcesosGenerarFirma(
    bodyReq: model.ReqBodyPostMcProcesosGenerarFirma,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.comun}/tarjeta/mcProcesos/firma`,
      model.ReqBodyPostMcProcesosGenerarFirma.create(bodyReq),
      { ...reqOptions }
    );
  }

  public McProcesosGrabarTransaccionPago(
    bodyReq: model.ReqBodyPostMcProcesosGrabarTransaccionPago,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.comun}/tarjeta/mcProcesos/transaccion`,
      model.ReqBodyPostMcProcesosGrabarTransaccionPago.create(bodyReq),
      { ...reqOptions }
    );
  }

  public McProcesosVerificarTransaccionPago(
    pathReq: model.ReqPathGetMcProcesosVerificarTransaccionPago,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.comun}/tarjeta/mcProcesos/verificacionTransaccion/${pathReq.numeroReferencia}`,
      { ...reqOptions }
    );
  }

  public ObtenerDatosServidor(reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.comun}/datosServidor`, { ...reqOptions });
  }

  public VisaNetGrabarTransaccionPago(
    bodyReq: model.ReqBodyPostVisaNetGrabarTransaccionPago,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.comun}/tarjeta/visaNet/transaccion`,
      model.ReqBodyPostVisaNetGrabarTransaccionPago.create(bodyReq),
      { ...reqOptions }
    );
  }

  public VisaNetIniciarSesion(
    bodyReq: model.ReqBodyPostVisaNetIniciarSesion,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.comun}/tarjeta/visaNet/iniciarSesion`,
      model.ReqBodyPostVisaNetIniciarSesion.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ObtenerRangoSalariales(reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.comun}/rangosSalariales`, { ...reqOptions });
  }

  public Lookup(reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.comun}/lookup`, { ...reqOptions });
  }

  public ListarEntidades(reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.comun}/entidad`, { ...reqOptions });
  }

  public ObtenerParametrosGenerales(
    pathReq: model.ReqPathGetObtenerParametrosGenerales,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.comun}/parametros/${pathReq.codigoApp}`, { ...reqOptions });
  }

  public ObtenerEstadoDespliegue(
    pathReq: model.ReqPathGetObtenerEstadoDespliegue,
    queryReq: model.ReqQueryGetObtenerEstadoDespliegue,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.comun}/despliegue/${pathReq.codigoApp}`, {
      params: { ...model.ReqQueryGetObtenerEstadoDespliegue.create(queryReq) },
      ...reqOptions
    });
  }

  public ObtenerTiposDocumento(
    pathReq: model.ReqPathGetObtenerTiposDocumento,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.comun}/tiposdocumento/${pathReq.seccion}`, { ...reqOptions });
  }

  public ListarUbigeoDepartamentos(reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.comun}/ubigeo/departamentos`, { ...reqOptions });
  }

  public ListarUbigeoProvincias(
    pathReq: model.ReqPathGetListarUbigeoProvincias,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.comun}/ubigeo/provincias/${pathReq.departamentoId}`, {
      ...reqOptions
    });
  }

  public ListarUbigeoDistritos(
    pathReq: model.ReqPathGetListarUbigeoDistritos,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.comun}/ubigeo/distritos/${pathReq.provinciaId}`, {
      ...reqOptions
    });
  }

  public TiposRelacionFamiliar(reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.comun}/tiposRelacionFamiliar`, { ...reqOptions });
  }

  public ObtenerUbicacionGoogle(
    queryReq: model.ReqQueryGetObtenerUbicacionGoogle,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.comun}/ubicacionGoogle`, {
      params: { ...model.ReqQueryGetObtenerUbicacionGoogle.create(queryReq) },
      ...reqOptions
    });
  }

  public ObtenerContactoPais(pathReq: model.ReqPathGetObtenerContactoPais, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.comun}/pais/contacto/${pathReq.estado}`, { ...reqOptions });
  }

  public ObtenerFcm(pathReq: model.ReqPathGetObtenerFcm, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.comun}/areaPrivada/fcm/device/${pathReq.codigoApp}/${pathReq.usuario}`,
      { ...reqOptions }
    );
  }

  public GenerarFcmApp(
    pathReq: model.ReqPathPostGenerarFcmApp,
    bodyReq: model.ReqBodyPostGenerarFcmApp,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.comun}/areaPrivada/fcm/device/${pathReq.codigoApp}`,
      model.ReqBodyPostGenerarFcmApp.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ObtenerFcmServer(pathReq: model.ReqPathGetObtenerFcmServer, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(
      `${appConstants.apis.comun}/areaPrivada/fcm/server/${pathReq.codigoApp}/${pathReq.usuario}`,
      { ...reqOptions }
    );
  }

  public GenerarFcmAppServer(
    pathReq: model.ReqPathPostGenerarFcmAppServer,
    bodyReq: model.ReqBodyPostGenerarFcmAppServer,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.comun}/areaPrivada/fcm/server/${pathReq.codigoApp}`,
      model.ReqBodyPostGenerarFcmAppServer.create(bodyReq),
      { ...reqOptions }
    );
  }

  public EnviarFcmAppServer(
    pathReq: model.ReqPathPostEnviarFcmAppServer,
    bodyReq: model.ReqBodyPostEnviarFcmAppServer,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.comun}/areaPrivada/fcm/message/send/${pathReq.codigoApp}`,
      model.ReqBodyPostEnviarFcmAppServer.create(bodyReq),
      { ...reqOptions }
    );
  }

  public ObtenerFunciones(pathReq: model.ReqPathGetObtenerFunciones, reqOptions?: ReqOptions): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.comun}/areaPrivada/funciones/${pathReq.codigoApp}`, {
      ...reqOptions
    });
  }

  public ObtenerTerminosCondicionesCliente(
    queryReq: model.ReqQueryGetObtenerTerminosCondicionesCliente,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.get(`${appConstants.apis.comun}/areaPrivada/generales/terminoscondiciones`, {
      params: { ...model.ReqQueryGetObtenerTerminosCondicionesCliente.create(queryReq) },
      ...reqOptions
    });
  }

  public AprobarTerminosCondiciones(
    pathReq: model.ReqPathPutAprobarTerminosCondiciones,
    queryReq: model.ReqQueryPutAprobarTerminosCondiciones,
    bodyReq?: model.ReqBodyPutAprobarTerminosCondiciones,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.put(
      `${appConstants.apis.comun}/areaPrivada/generales/terminoscondiciones/${pathReq.id}`,
      model.ReqBodyPutAprobarTerminosCondiciones.create(bodyReq),
      { params: { ...model.ReqQueryPutAprobarTerminosCondiciones.create(queryReq) }, ...reqOptions }
    );
  }

  public GuardarMovimiento(
    queryReq: model.ReqQueryPostGuardarMovimiento,
    bodyReq: model.ReqBodyPostGuardarMovimiento,
    reqOptions?: ReqOptions
  ): Observable<any> {
    return this._ApiService.post(
      `${appConstants.apis.comun}/areaPrivada/eventos/movimientos`,
      model.ReqBodyPostGuardarMovimiento.create(bodyReq),
      { params: { ...model.ReqQueryPostGuardarMovimiento.create(queryReq) }, ...reqOptions }
    );
  }
}
