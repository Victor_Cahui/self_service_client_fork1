// tslint:disable:no-identical-functions

import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmailValidator } from '@mx/core/ui/lib/validators/validators';

@Injectable({
  providedIn: 'root'
})
export class FrmProvider {
  constructor(private readonly _FormBuilder: FormBuilder) {}

  VehiclesRecordsComponent(): FormGroup {
    return this._FormBuilder.group({
      year: ['', []]
    });
  }

  CardMyPoliciesComponent(): FormGroup {
    return this._FormBuilder.group({
      activarSoatVencidos: [true, []],
      anio: ['', []],
      estadoPoliza: ['', []],
      numeroPoliza: ['', []],
      tipoPoliza: ['', []]
    });
  }

  CardPaymentsDetailComponent(): FormGroup {
    return this._FormBuilder.group({
      numeroPoliza: ['', []],
      numRecibo: ['', []],
      tipoPoliza: ['', []]
    });
  }

  CardReceiptsListComponent(): FormGroup {
    return this._FormBuilder.group({
      numeroPoliza: ['', []],
      numRecibo: ['', []],
      tiposPagosId: ['', []],
      tipoPoliza: ['', []],
      anioPago: ['', []]
    });
  }

  CardSinistersInProgressComponent(): FormGroup {
    return this._FormBuilder.group({
      numeroPoliza: ['', []],
      estado: ['', []],
      tipoPoliza: ['', []],
      anioOcurrencia: ['', []]
    });
  }

  HomeProfileComponent(): FormGroup {
    return this._FormBuilder.group({
      beneficios: ['', []],
      promociones: ['', []],
      seguros: ['', []],
      terminos: ['', []]
    });
  }

  MapfeDollarsComponent(): FormGroup {
    return this._FormBuilder.group({
      estado: ['', []]
    });
  }

  ScheduleStep1Component(): FormGroup {
    return this._FormBuilder.group({
      documentoPaciente: ['', [Validators.required]],
      numeroPoliza: ['', [Validators.required]],
      especialidadId: ['', [Validators.required]],
      contactCellphone: [
        '',
        [
          Validators.required,
          Validators.minLength(9),
          Validators.maxLength(12),
          Validators.pattern(/^[+]{0,1}[0-9]{0,12}$/)
        ]
      ]
    });
  }

  ScheduleCovidStep1Component(): FormGroup {
    return this._FormBuilder.group({
      documentoPaciente: ['', [Validators.required]],
      numeroPoliza: ['', [Validators.required]],
      contactCellphone: [
        '',
        [
          Validators.required,
          Validators.minLength(9),
          Validators.maxLength(12),
          Validators.pattern(/^[+]{0,1}[0-9]{0,12}$/)
        ]
      ]
    });
  }

  ScheduleStep3VComponentByDate(): FormGroup {
    return this._FormBuilder.group({ ...this._getFormForAppointmentVirtualStep3() });
  }

  ScheduleStep3VComponentByDoctor(): FormGroup {
    return this._FormBuilder.group({ ...this._getFormForAppointmentVirtualStep3() });
  }

  ScheduleStep3PComponentByDate(): FormGroup {
    return this._FormBuilder.group({ ...this._getFormForAppointmentPresentialStep3() });
  }

  ScheduleStep3PComponentByDoctor(): FormGroup {
    return this._FormBuilder.group({ ...this._getFormForAppointmentPresentialStep3() });
  }

  AffiliatesComponent(): FormGroup {
    return this._FormBuilder.group({
      codPlan: ['', [Validators.required]],
      periodo: ['', [Validators.required]]
    });
  }

  AffiliateComponent(arr: any[]): FormGroup {
    const objFrm = arr.reduce((acc, affi) => {
      return {
        ...acc,
        [affi.frmNamePhone]: [
          affi.telefono,
          [Validators.maxLength(18), Validators.minLength(7), Validators.pattern(/^[+]{0,1}[0-9]{0,18}$/)]
        ],
        [affi.frmNameEmail]: [affi.correo || affi.correoElectronico, [EmailValidator.isValid]]
      };
    }, {});

    return this._FormBuilder.group(objFrm);
  }

  SctrRecordComponent(): FormGroup {
    return this._FormBuilder.group({
      vigencyMonth: ['', [Validators.required]],
      recordType: ['', [Validators.required]]
    });
  }

  SctrPeriodReceiptsComponent(): FormGroup {
    return this._FormBuilder.group({
      periodMonth: ['']
    });
  }

  SctrPeriodReceiptsFilter(): FormGroup {
    return this._FormBuilder.group({
      situationType: ['Todos']
    });
  }

  SctrPeriodPayrollFilter(): FormGroup {
    return this._FormBuilder.group({
      policyType: ['Todos']
    });
  }

  private _getFormForAppointmentPresentialStep3(): any {
    return {
      clinicaId: ['', [Validators.required]],
      doctorId: ['', [Validators.required]],
      fecha: ['', [Validators.required]],
      horaFin: ['', [Validators.required]],
      horaInicio: ['', [Validators.required]]
    };
  }

  private _getFormForAppointmentVirtualStep3(): any {
    return {
      doctorId: ['', [Validators.required]],
      fecha: ['', [Validators.required]],
      horaFin: ['', [Validators.required]],
      horaInicio: ['', [Validators.required]]
    };
  }

  public DirectPayFormComponent(): FormGroup {
    return this._FormBuilder.group({});
  }

  public InsuredFormComponent(arr: any[]): FormGroup {
    const objFrm = arr.reduce((acc, insured) => {
      return {
        ...acc,
        [`phone_${insured.documento}`]: [
          insured.telefono,
          [Validators.maxLength(18), Validators.minLength(7), Validators.pattern(/^[+]{0,1}[0-9]{0,18}$/)]
        ],
        [`email_${insured.documento}`]: [insured.correoElectronico, [EmailValidator.isValid]]
      };
    }, {});

    return this._FormBuilder.group(objFrm);
  }

  public HomeDoctorRequestFormComponent(): FormGroup {
    return this._FormBuilder.group({
      policy: [null, [Validators.required]],
      patient: [null, [Validators.required]],
      specialty: [null, [Validators.required]],
      telephone: [
        null,
        [Validators.maxLength(18), Validators.minLength(7), Validators.pattern(/^[+]{0,1}[0-9]{0,18}$/)]
      ],
      reasonForConsultation: [null, [Validators.maxLength(150)]]
    });
  }
}
