import { DOCUMENT } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { SCRIPT_TYPE, ScriptModel } from '../models/script.interface';

@Injectable({
  providedIn: 'root'
})
export class ScriptService {
  constructor(@Inject(DOCUMENT) private readonly document: Document) {}

  public async load(scripts: ScriptModel[]): Promise<any[]> {
    const promises: any[] = [];
    scripts.forEach(script => promises.push(this._loadScript(script)));

    return Promise.all(promises);
  }

  public remove(scripts: ScriptModel[]): void {
    scripts.forEach(script => {
      const elements: any = this.document.querySelector(this._getSelector(script));
      if (elements) {
        elements.remove();
      }
    });
  }

  private async _loadScript(newScript: ScriptModel): Promise<any> {
    return new Promise(resolve => {
      // resolve if already loaded
      const elements: any = this.document.querySelector(this._getSelector(newScript));

      if (elements) {
        resolve({ script: newScript, loaded: true, status: 'Already Loaded' });

        return;
      }

      // load script
      const element: any = this.document.createElement(this._getElementTag(newScript));
      this._setUrl(element, newScript);

      if (newScript.attributes) {
        Object.keys(newScript.attributes).forEach(key => element.setAttribute(key, newScript.attributes[key]));
      }

      if (element.readyState) {
        // IE
        element.onreadystatechange = () => {
          if (element.readyState === 'loaded' || element.readyState === 'complete') {
            element.onreadystatechange = undefined;
            resolve({ script: newScript, loaded: true, status: 'Loaded' });
          }
        };
      } else {
        // Others
        element.onload = () => {
          resolve({ script: newScript, loaded: true, status: 'Loaded' });
        };
      }

      element.onerror = () => {
        resolve({ script: newScript, loaded: false, status: 'Loaded' });
      };

      this.document.getElementsByTagName('head')[0].appendChild(element);
    });
  }

  private readonly _getSelector: (script: ScriptModel) => string = (script: ScriptModel) => {
    return script.type === SCRIPT_TYPE.JS ? `script[src='${script.src}']` : `link[href='${script.src}']`;
    // tslint:disable-next-line: prettier
  }

  private readonly _getElementTag: (script: ScriptModel) => string = (script: ScriptModel) => {
    return script.type === SCRIPT_TYPE.JS ? 'script' : 'link';
    // tslint:disable-next-line: prettier
  }

  private readonly _setUrl: (element: any, script: ScriptModel) => void = (element: any, script: ScriptModel) => {
    switch (script.type) {
      case SCRIPT_TYPE.JS:
        element.src = script.src;
        break;
      case SCRIPT_TYPE.CSS:
        element.href = script.src;
        element.rel = 'stylesheet';
        break;

      default:
        break;
    }
    // tslint:disable-next-line: prettier
  }
}
