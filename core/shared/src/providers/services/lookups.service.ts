// tslint:disable:no-identical-functions
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs/internal/observable/throwError';
import { catchError } from 'rxjs/internal/operators/catchError';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';

import { ApiService } from '@mx/core/shared/helpers/api/api.service';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { GeneralEndpoint } from '@mx/endpoints/general.endpoint';
import { AuthService } from '@mx/services/auth/auth.service';
import { STORAGE_KEYS as generalStorageKeys } from '@mx/services/general/general.service';
import { STORAGE_KEYS } from '@mx/services/policies.service';
import { APPLICATION_CODE, STORAGE } from '@mx/settings/constants/general-values';
import { ANIO_LIMITE_POLIZA, ANIO_LIMITE_SINISTER, TIPO_SEGURO } from '@mx/settings/constants/key-values';
import { Autoservicios } from './proxy';

@Injectable({
  providedIn: 'root'
})
export class LookupsProvider {
  objReqLookups;

  constructor(
    private readonly _ApiService: ApiService,
    private readonly _AuthService: AuthService,
    private readonly _Autoservicios: Autoservicios,
    private readonly _StorageService: StorageService
  ) {}

  public getForkLookups(): Observable<any> {
    this._setServicesLookups();
    const lookupsKeys = this._getLookupsKeys();

    return forkJoin(lookupsKeys.map(k => this.objReqLookups[k].pipe(catchError(this._handleError.bind(this)))));
  }

  public setLookups(r: any[]): void {
    this._getLookupsKeys().forEach((lk, idx) => {
      this._StorageService.saveStorage(lk, r[idx]);
    });
  }

  private _setServicesLookups(): void {
    this.objReqLookups = {
      [TIPO_SEGURO]: this._Autoservicios.ObtenerCategorias({
        codigoApp: APPLICATION_CODE
      }),
      [ANIO_LIMITE_POLIZA]: this._Autoservicios.ListarLimiteAnios(
        {
          codigoApp: APPLICATION_CODE
        },
        { orden: 'DESC' }
      ),
      [ANIO_LIMITE_SINISTER]: this._Autoservicios.ObtenerAniosSiniestro({
        codigoApp: APPLICATION_CODE
      }),
      [STORAGE.PAYMENT_CHANNEL_PER_USER]: this._Autoservicios.ObtenerTiposPagosPorPersona({
        codigoApp: APPLICATION_CODE
      }),
      [STORAGE_KEYS.POLICY_TYPES]: this._Autoservicios.ObtenerTiposPolizasPorCliente({
        codigoApp: APPLICATION_CODE
      }),
      [generalStorageKeys.PARAMETERS]: this._ApiService.get(`${GeneralEndpoint.parameters}`, { default: [] }),
      [generalStorageKeys.DOCUMENT_TYPES]: this._ApiService.get(
        `${GeneralEndpoint.typesDocument}${APPLICATION_CODE}`,
        {}
      )
    };
  }

  private _handleError(): Observable<any[]> {
    return throwError([]);
  }

  private _getLookupsKeys(): string[] {
    return Object.keys(this.objReqLookups);
  }
}
