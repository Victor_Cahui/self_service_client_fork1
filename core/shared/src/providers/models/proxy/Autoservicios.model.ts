// tslint:disable

import { hasValue } from '@mx/core/shared/helpers';

function removeUndefinedFromObj(obj: any): any {
  const arrObjKeys = Object.keys(obj);

  return arrObjKeys.reduce((acc, k) => (hasValue(obj[k]) ? { ...acc, [k]: obj[k] } : acc), {});
}

export class ReqPathGetObtenerListaOficinas {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerListaOficinas {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetObtenerListaOficinas {
  public latitud?: number;
  public longitud?: number;

  public static create(obj): ReqQueryGetObtenerListaOficinas {
    return removeUndefinedFromObj({
      latitud: hasValue(obj.latitud) ? +obj.latitud : undefined,
      longitud: hasValue(obj.longitud) ? +obj.longitud : undefined
    });
  }
}

export class ReqBodyPostLoginFechaNacimiento {
  public tipoDocumento?: string;
  public numeroDocumento?: string;
  public fechaNacimiento?: string;

  public static create(obj): ReqBodyPostLoginFechaNacimiento {
    return removeUndefinedFromObj({
      tipoDocumento: hasValue(obj.tipoDocumento) ? '' + obj.tipoDocumento : undefined, // DNI, CEX
      numeroDocumento: hasValue(obj.numeroDocumento) ? '' + obj.numeroDocumento : undefined,
      fechaNacimiento: hasValue(obj.fechaNacimiento) ? '' + obj.fechaNacimiento : undefined
    });
  }
}

export class ReqPathPostSolicitarNroComprobacion {
  public codigoApp: string;

  public static create(obj): ReqPathPostSolicitarNroComprobacion {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostSolicitarNroComprobacion {
  public telefonoMovil?: string;

  public static create(obj): ReqBodyPostSolicitarNroComprobacion {
    return removeUndefinedFromObj({
      telefonoMovil: hasValue(obj.telefonoMovil) ? '' + obj.telefonoMovil : undefined
    });
  }
}

export class ReqPathPostVerificarNroComprobacion {
  public codigoApp: string;

  public static create(obj): ReqPathPostVerificarNroComprobacion {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostVerificarNroComprobacion {
  public nroComprobacion?: string;
  public telefonoMovil?: string;

  public static create(obj): ReqBodyPostVerificarNroComprobacion {
    return removeUndefinedFromObj({
      nroComprobacion: hasValue(obj.nroComprobacion) ? '' + obj.nroComprobacion : undefined,
      telefonoMovil: hasValue(obj.telefonoMovil) ? '' + obj.telefonoMovil : undefined
    });
  }
}

export class ReqPathPostRegistrarDatosMovil {
  public codigoApp: string;

  public static create(obj): ReqPathPostRegistrarDatosMovil {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostRegistrarDatosMovil {
  public firebaseId?: string;
  public deviceId?: string;
  public so?: string;
  public numeroTelefono?: string;
  public oneSignal?: object;

  public static create(obj): ReqBodyPostRegistrarDatosMovil {
    return removeUndefinedFromObj({
      firebaseId: hasValue(obj.firebaseId) ? '' + obj.firebaseId : undefined,
      deviceId: hasValue(obj.deviceId) ? '' + obj.deviceId : undefined,
      so: hasValue(obj.so) ? '' + obj.so : undefined, // android, ios
      numeroTelefono: hasValue(obj.numeroTelefono) ? '' + obj.numeroTelefono : undefined,
      oneSignal: hasValue(obj.oneSignal) ? obj.oneSignal : undefined
    });
  }
}

export class ReqPathPostConsentimientoPoliza {
  public codigoApp: string;

  public static create(obj): ReqPathPostConsentimientoPoliza {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostConsentimientoPoliza {
  public consentimiento?: string;
  public appUsuario?: string;

  public static create(obj): ReqBodyPostConsentimientoPoliza {
    return removeUndefinedFromObj({
      consentimiento: hasValue(obj.consentimiento) ? '' + obj.consentimiento : undefined,
      appUsuario: hasValue(obj.appUsuario) ? '' + obj.appUsuario : undefined
    });
  }
}

export class ReqPathGetObtenerConfiguracionMenuPorCliente {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerConfiguracionMenuPorCliente {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetVerificarVersion {
  public codigoApp: string;
  public version: string;

  public static create(obj): ReqPathGetVerificarVersion {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      version: hasValue(obj.version) ? '' + obj.version : undefined
    });
  }
}

export class ReqFormDataPutActualizarDatosPersonalesSensibles {
  public tipoDocumentoModificado?: string;
  public numeroDocumentoModificado?: string;
  public apellidoPaterno?: string;
  public apellidoMaterno?: string;
  public nombre?: string;
  public fechaNacimiento?: string;
  public fotosDocumento?: any;

  public static create(obj): ReqFormDataPutActualizarDatosPersonalesSensibles {
    return removeUndefinedFromObj({
      tipoDocumentoModificado: hasValue(obj.tipoDocumentoModificado) ? '' + obj.tipoDocumentoModificado : undefined,
      numeroDocumentoModificado: hasValue(obj.numeroDocumentoModificado)
        ? '' + obj.numeroDocumentoModificado
        : undefined,
      apellidoPaterno: hasValue(obj.apellidoPaterno) ? '' + obj.apellidoPaterno : undefined,
      apellidoMaterno: hasValue(obj.apellidoMaterno) ? '' + obj.apellidoMaterno : undefined,
      nombre: hasValue(obj.nombre) ? '' + obj.nombre : undefined,
      fechaNacimiento: hasValue(obj.fechaNacimiento) ? '' + obj.fechaNacimiento : undefined,
      fotosDocumento: hasValue(obj.fotosDocumento) ? obj.fotosDocumento : undefined
    });
  }
}

export class ReqBodyPutActualizarDatosPersonalesNoSensibles {
  public telefonoMovil?: string;

  public static create(obj): ReqBodyPutActualizarDatosPersonalesNoSensibles {
    return removeUndefinedFromObj({
      telefonoMovil: hasValue(obj.telefonoMovil) ? '' + obj.telefonoMovil : undefined
    });
  }
}

export class ReqBodyPutActualizarInformacionTrabajo {
  public lugarTrabajo?: string;
  public profesionId?: number;
  public profesionDescripcion?: string;
  public fechaIngreso?: string;
  public rangoSalarialId?: number;
  public identificadorDepartamento?: number;
  public identificadorProvincia?: number;
  public identificadorDistrito?: number;
  public direccion?: string;
  public telefono?: string;

  public static create(obj): ReqBodyPutActualizarInformacionTrabajo {
    return removeUndefinedFromObj({
      lugarTrabajo: hasValue(obj.lugarTrabajo) ? '' + obj.lugarTrabajo : undefined,
      profesionId: hasValue(obj.profesionId) ? +obj.profesionId : undefined,
      profesionDescripcion: hasValue(obj.profesionDescripcion) ? '' + obj.profesionDescripcion : undefined,
      fechaIngreso: hasValue(obj.fechaIngreso) ? '' + obj.fechaIngreso : undefined,
      rangoSalarialId: hasValue(obj.rangoSalarialId) ? +obj.rangoSalarialId : undefined,
      identificadorDepartamento: hasValue(obj.identificadorDepartamento) ? +obj.identificadorDepartamento : undefined,
      identificadorProvincia: hasValue(obj.identificadorProvincia) ? +obj.identificadorProvincia : undefined,
      identificadorDistrito: hasValue(obj.identificadorDistrito) ? +obj.identificadorDistrito : undefined,
      direccion: hasValue(obj.direccion) ? '' + obj.direccion : undefined,
      telefono: hasValue(obj.telefono) ? '' + obj.telefono : undefined
    });
  }
}

export class ReqPathGetObtenerConfiguracionUsuario {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerConfiguracionUsuario {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetObtenerConfiguracionUsuario {
  public seccion?: string;
  public esClinicaDigital?: string;

  public static create(obj): ReqQueryGetObtenerConfiguracionUsuario {
    return removeUndefinedFromObj({
      seccion: hasValue(obj.seccion) ? '' + obj.seccion : undefined, // MD_AUTOS, MD_SALUD, MD_VIDA, MD_HOGAR, MD_DECESOS, MD_SCTR, MD_SOAT, MD_SOAT_ELECTRO, MD_EPS, MD_SOAT|MD_SOAT_ELECTRO, MD_SALUD|MD_EPS, MD_SALUD_COTIZACION, MD_TIPO_USUARIO, MD_SCTR_PENSION, MD_SCTR_SALUD, MD_VIAJES, MD_AUTOS|MD_SOAT, MD_AUTOS|MD_SOAT|MD_SOAT_ELECTRO, MD_OTROS
      esClinicaDigital: hasValue(obj.esClinicaDigital) ? '' + obj.esClinicaDigital : undefined // S, N
    });
  }
}

export class ReqQueryGetObtenerdireccionesPoliza {
  public tipoPoliza?: string;

  public static create(obj): ReqQueryGetObtenerdireccionesPoliza {
    return removeUndefinedFromObj({
      tipoPoliza: hasValue(obj.tipoPoliza) ? '' + obj.tipoPoliza : undefined // MD_AUTOS, MD_SALUD, MD_VIDA, MD_HOGAR, MD_DECESOS, MD_SCTR, MD_SOAT, MD_SOAT_ELECTRO, MD_EPS, MD_SOAT|MD_SOAT_ELECTRO, MD_SALUD|MD_EPS, MD_SALUD_COTIZACION, MD_TIPO_USUARIO, MD_SCTR_PENSION, MD_SCTR_SALUD, MD_VIAJES, MD_AUTOS|MD_SOAT, MD_AUTOS|MD_SOAT|MD_SOAT_ELECTRO, MD_OTROS
    });
  }
}

export class ReqBodyPutActualizarDireccionCorrespondenciaPersonal {
  public identificadorDepartamento?: number;
  public identificadorProvincia?: number;
  public identificadorDistrito?: number;
  public direccion?: string;

  public static create(obj): ReqBodyPutActualizarDireccionCorrespondenciaPersonal {
    return removeUndefinedFromObj({
      identificadorDepartamento: hasValue(obj.identificadorDepartamento) ? +obj.identificadorDepartamento : undefined,
      identificadorProvincia: hasValue(obj.identificadorProvincia) ? +obj.identificadorProvincia : undefined,
      identificadorDistrito: hasValue(obj.identificadorDistrito) ? +obj.identificadorDistrito : undefined,
      direccion: hasValue(obj.direccion) ? '' + obj.direccion : undefined
    });
  }
}

export class ReqPathPutGuardarInformacionMD {
  public codigoApp: string;

  public static create(obj): ReqPathPutGuardarInformacionMD {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPutGuardarInformacionMD {
  public flagInformacion?: any;

  public static create(obj): ReqBodyPutGuardarInformacionMD {
    return removeUndefinedFromObj({
      flagInformacion: hasValue(obj.flagInformacion) ? obj.flagInformacion : undefined
    });
  }
}

export class ReqPathGetObtenerResumenMD {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerResumenMD {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerDetallePaginadoMD {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerDetallePaginadoMD {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetObtenerDetallePaginadoMD {
  public estado?: string;
  public registros?: number;
  public pagina?: number;

  public static create(obj): ReqQueryGetObtenerDetallePaginadoMD {
    return removeUndefinedFromObj({
      estado: hasValue(obj.estado) ? '' + obj.estado : undefined, // GANADO, USADO, VENCIDO
      registros: hasValue(obj.registros) ? +obj.registros : undefined,
      pagina: hasValue(obj.pagina) ? +obj.pagina : undefined
    });
  }
}

export class ReqBodyPutActualizarCorreoElectronico {
  public nuevoEmailUsuario?: string;

  public static create(obj): ReqBodyPutActualizarCorreoElectronico {
    return removeUndefinedFromObj({
      nuevoEmailUsuario: hasValue(obj.nuevoEmailUsuario) ? '' + obj.nuevoEmailUsuario : undefined
    });
  }
}

export class ReqPathPostRegistrarContactarAgente {
  public codigoApp: string;

  public static create(obj): ReqPathPostRegistrarContactarAgente {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostRegistrarContactarAgente {
  public telefono?: string;
  public rangoHorario?: string;
  public motivoContacto?: string;
  public submotivoContacto?: string;

  public static create(obj): ReqBodyPostRegistrarContactarAgente {
    return removeUndefinedFromObj({
      telefono: hasValue(obj.telefono) ? '' + obj.telefono : undefined,
      rangoHorario: hasValue(obj.rangoHorario) ? '' + obj.rangoHorario : undefined,
      motivoContacto: hasValue(obj.motivoContacto) ? '' + obj.motivoContacto : undefined, // AUTOS, DECESOS, HOGAR, VIDA_AHORRO, SALUD
      submotivoContacto: hasValue(obj.submotivoContacto) ? '' + obj.submotivoContacto : undefined // COMPARACION, COTIZACION
    });
  }
}

export class ReqPathGetObtenerDatosContacto {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerDatosContacto {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerListaProfesiones {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerListaProfesiones {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetVerificarDocumento {
  public codigoApp: string;

  public static create(obj): ReqPathGetVerificarDocumento {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetVerificarDocumento {
  public documento: string;

  public static create(obj): ReqQueryGetVerificarDocumento {
    return removeUndefinedFromObj({
      documento: hasValue(obj.documento) ? '' + obj.documento : undefined
    });
  }
}

export class ReqPathPostRegistrarLlamadaEmergencia {
  public codigoApp: string;

  public static create(obj): ReqPathPostRegistrarLlamadaEmergencia {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostRegistrarLlamadaEmergencia {
  public tipoDocumento?: string;
  public numeroDocumento?: string;
  public apellidoPaterno?: string;
  public apellidoMaterno?: string;
  public nombre?: string;
  public numeroTelefono?: string;
  public latitud?: number;
  public longitud?: number;
  public nota?: string;

  public static create(obj): ReqBodyPostRegistrarLlamadaEmergencia {
    return removeUndefinedFromObj({
      tipoDocumento: hasValue(obj.tipoDocumento) ? '' + obj.tipoDocumento : undefined, // DNI, PEX
      numeroDocumento: hasValue(obj.numeroDocumento) ? '' + obj.numeroDocumento : undefined,
      apellidoPaterno: hasValue(obj.apellidoPaterno) ? '' + obj.apellidoPaterno : undefined,
      apellidoMaterno: hasValue(obj.apellidoMaterno) ? '' + obj.apellidoMaterno : undefined,
      nombre: hasValue(obj.nombre) ? '' + obj.nombre : undefined,
      numeroTelefono: hasValue(obj.numeroTelefono) ? '' + obj.numeroTelefono : undefined,
      latitud: hasValue(obj.latitud) ? +obj.latitud : undefined,
      longitud: hasValue(obj.longitud) ? +obj.longitud : undefined,
      nota: hasValue(obj.nota) ? '' + obj.nota : undefined
    });
  }
}

export class ReqPathGetObtenerPermisos {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerPermisos {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetObtenerPermisos {
  public documento: string;
  public tipoDocumento: string;
  public esInvitado?: string;

  public static create(obj): ReqQueryGetObtenerPermisos {
    return removeUndefinedFromObj({
      documento: hasValue(obj.documento) ? '' + obj.documento : undefined,
      tipoDocumento: hasValue(obj.tipoDocumento) ? '' + obj.tipoDocumento : undefined, // DNI, RUC, PEX, CEX, CIP
      esInvitado: hasValue(obj.esInvitado) ? '' + obj.esInvitado : undefined // S, N
    });
  }
}

export class ReqPathGetObtenerAgentes {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerAgentes {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerConfiguracionContacto {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerConfiguracionContacto {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathPutActualizarConfiguracionContacto {
  public codigoApp: string;

  public static create(obj): ReqPathPutActualizarConfiguracionContacto {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPutActualizarConfiguracionContacto {
  public recibirNotificacion?: any;
  public recibirComunicacionesGenerales?: any;
  public recibirEstadoCuenta?: any;
  public medioContacto?: object;

  public static create(obj): ReqBodyPutActualizarConfiguracionContacto {
    return removeUndefinedFromObj({
      recibirNotificacion: hasValue(obj.recibirNotificacion) ? obj.recibirNotificacion : undefined,
      recibirComunicacionesGenerales: hasValue(obj.recibirComunicacionesGenerales)
        ? obj.recibirComunicacionesGenerales
        : undefined,
      recibirEstadoCuenta: hasValue(obj.recibirEstadoCuenta) ? obj.recibirEstadoCuenta : undefined,
      medioContacto: hasValue(obj.medioContacto) ? obj.medioContacto : undefined
    });
  }
}

export class ReqPathPostContactarReferidos {
  public codigoApp: string;

  public static create(obj): ReqPathPostContactarReferidos {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostContactarReferidos {
  public landingID?: string;
  public code?: string;

  public static create(obj): ReqBodyPostContactarReferidos {
    return removeUndefinedFromObj({
      landingID: hasValue(obj.landingID) ? '' + obj.landingID : undefined, // HOGAR_COMPARACION_POLIZA_MULTIRIESGO, HOGAR_COMPARACION_POLIZA_CAMBIO
      code: hasValue(obj.code) ? '' + obj.code : undefined
    });
  }
}

export class ReqPathGetObtenerNotificaciones {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerNotificaciones {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathPostRegistrarNotificacionLeida {
  public codigoApp: string;

  public static create(obj): ReqPathPostRegistrarNotificacionLeida {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostRegistrarNotificacionLeida {
  public notificacionId?: number;
  public notificacionLeida?: any;

  public static create(obj): ReqBodyPostRegistrarNotificacionLeida {
    return removeUndefinedFromObj({
      notificacionId: hasValue(obj.notificacionId) ? +obj.notificacionId : undefined,
      notificacionLeida: hasValue(obj.notificacionLeida) ? obj.notificacionLeida : undefined
    });
  }
}

export class ReqPathPutRegistrarTerminosCondiciones {
  public codigoApp: string;

  public static create(obj): ReqPathPutRegistrarTerminosCondiciones {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPutRegistrarTerminosCondiciones {
  public promociones?: any;
  public seguros?: any;
  public beneficios?: any;
  public terminos?: any;
  public actualizacion?: any;

  public static create(obj): ReqBodyPutRegistrarTerminosCondiciones {
    return removeUndefinedFromObj({
      promociones: hasValue(obj.promociones) ? obj.promociones : undefined,
      seguros: hasValue(obj.seguros) ? obj.seguros : undefined,
      beneficios: hasValue(obj.beneficios) ? obj.beneficios : undefined,
      terminos: hasValue(obj.terminos) ? obj.terminos : undefined,
      actualizacion: hasValue(obj.actualizacion) ? obj.actualizacion : undefined
    });
  }
}

export class ReqPathGetObtenerTerminosCondiciones {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerTerminosCondiciones {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathPostRegistrarNotificacion {
  public codigoApp: string;

  public static create(obj): ReqPathPostRegistrarNotificacion {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostRegistrarNotificacion {
  public notificacionId?: number;
  public tipoNotification?: string;
  public titulo?: string;
  public description?: string;
  public fecha?: string;
  public hora?: string;
  public datoExtra?: string;
  public leido?: any;

  public static create(obj): ReqBodyPostRegistrarNotificacion {
    return removeUndefinedFromObj({
      notificacionId: hasValue(obj.notificacionId) ? +obj.notificacionId : undefined,
      tipoNotification: hasValue(obj.tipoNotification) ? '' + obj.tipoNotification : undefined,
      titulo: hasValue(obj.titulo) ? '' + obj.titulo : undefined,
      description: hasValue(obj.description) ? '' + obj.description : undefined,
      fecha: hasValue(obj.fecha) ? '' + obj.fecha : undefined,
      hora: hasValue(obj.hora) ? '' + obj.hora : undefined,
      datoExtra: hasValue(obj.datoExtra) ? '' + obj.datoExtra : undefined,
      leido: hasValue(obj.leido) ? obj.leido : undefined
    });
  }
}

export class ReqPathGetListadoPagos {
  public codigoApp: string;

  public static create(obj): ReqPathGetListadoPagos {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetListadoPagos {
  public tipoPoliza?: string;
  public estadoPago?: string;
  public numeroPoliza?: string;
  public numRecibo?: string;
  public pSeccion?: string;
  public criterio?: string;
  public orden?: string;
  public registros?: number;
  public pagina?: number;

  public static create(obj): ReqQueryGetListadoPagos {
    return removeUndefinedFromObj({
      tipoPoliza: hasValue(obj.tipoPoliza) ? '' + obj.tipoPoliza : undefined, // , MD_AUTOS, MD_SALUD, MD_VIDA, MD_HOGAR, MD_DECESOS, MD_SCTR, MD_SOAT_ELECTRO, MD_EPS, MD_VIAJES, MD_SOAT, MD_OTROS, MD_AUTOS|MD_SOAT|MD_SOAT_ELECTRO, MD_AUTOS|MD_SOAT, MD_SALUD|MD_EPS
      estadoPago: hasValue(obj.estadoPago) ? '' + obj.estadoPago : undefined, // Pendiente
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined,
      numRecibo: hasValue(obj.numRecibo) ? '' + obj.numRecibo : undefined,
      pSeccion: hasValue(obj.pSeccion) ? '' + obj.pSeccion : undefined,
      criterio: hasValue(obj.criterio) ? '' + obj.criterio : undefined,
      orden: hasValue(obj.orden) ? '' + obj.orden : undefined, // ASC, DESC
      registros: hasValue(obj.registros) ? +obj.registros : undefined,
      pagina: hasValue(obj.pagina) ? +obj.pagina : undefined
    });
  }
}

export class ReqPathGetObtenerTiposPagos {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerTiposPagos {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerTiposPagosPorPersona {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerTiposPagosPorPersona {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetListarConfiguracionesPagos {
  public codigoApp: string;

  public static create(obj): ReqPathGetListarConfiguracionesPagos {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetListarConfiguracionesPagos {
  public numeroPoliza?: string;

  public static create(obj): ReqQueryGetListarConfiguracionesPagos {
    return removeUndefinedFromObj({
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqPathPostConfigurarRenovacion {
  public codigoApp: string;

  public static create(obj): ReqPathPostConfigurarRenovacion {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostConfigurarRenovacion {
  public numeroPoliza?: string;
  public tipoRenovacionId?: number;
  public usarMapfreDolares?: any;

  public static create(obj): ReqBodyPostConfigurarRenovacion {
    return removeUndefinedFromObj({
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined,
      tipoRenovacionId: hasValue(obj.tipoRenovacionId) ? +obj.tipoRenovacionId : undefined,
      usarMapfreDolares: hasValue(obj.usarMapfreDolares) ? obj.usarMapfreDolares : undefined
    });
  }
}

export class ReqPathGetObtenerCuotasRefinanciarPoliza {
  public codigoApp: string;
  public numeroPoliza: string;

  public static create(obj): ReqPathGetObtenerCuotasRefinanciarPoliza {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqPathPostEnviarDatosRefinanciamiento {
  public codigoApp: string;
  public numeroPoliza: string;

  public static create(obj): ReqPathPostEnviarDatosRefinanciamiento {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqBodyPostEnviarDatosRefinanciamiento {
  public cuotaActual?: number;
  public montoRefinanciar?: number;
  public codigoMonedaMontoRefinanciar?: number;
  public tipoCuotaNueva?: string;
  public tieneInteres?: any;
  public interes?: number;
  public montoCuota?: number;
  public codigoMonedaMonto?: number;

  public static create(obj): ReqBodyPostEnviarDatosRefinanciamiento {
    return removeUndefinedFromObj({
      cuotaActual: hasValue(obj.cuotaActual) ? +obj.cuotaActual : undefined,
      montoRefinanciar: hasValue(obj.montoRefinanciar) ? +obj.montoRefinanciar : undefined,
      codigoMonedaMontoRefinanciar: hasValue(obj.codigoMonedaMontoRefinanciar)
        ? +obj.codigoMonedaMontoRefinanciar
        : undefined, // 1, 2
      tipoCuotaNueva: hasValue(obj.tipoCuotaNueva) ? '' + obj.tipoCuotaNueva : undefined,
      tieneInteres: hasValue(obj.tieneInteres) ? obj.tieneInteres : undefined,
      interes: hasValue(obj.interes) ? +obj.interes : undefined,
      montoCuota: hasValue(obj.montoCuota) ? +obj.montoCuota : undefined,
      codigoMonedaMonto: hasValue(obj.codigoMonedaMonto) ? +obj.codigoMonedaMonto : undefined // 1, 2
    });
  }
}

export class ReqPathGetObtenerNumeroPagosPendientes {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerNumeroPagosPendientes {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerRecibos {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerRecibos {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetObtenerRecibos {
  public tipoPoliza?: string;
  public tiposPagosId?: number;
  public anioPago?: number;
  public numeroPoliza?: string;
  public numRecibo?: string;
  public criterio?: string;
  public orden?: string;
  public registros?: number;
  public pagina?: number;

  public static create(obj): ReqQueryGetObtenerRecibos {
    return removeUndefinedFromObj({
      tipoPoliza: hasValue(obj.tipoPoliza) ? '' + obj.tipoPoliza : undefined, // , MD_AUTOS, MD_SALUD, MD_VIDA, MD_HOGAR, MD_DECESOS, MD_SCTR, MD_SOAT_ELECTRO, MD_EPS, MD_VIAJES, MD_SOAT, MD_OTROS, MD_AUTOS|MD_SOAT|MD_SOAT_ELECTRO, MD_AUTOS|MD_SOAT, MD_SALUD|MD_EPS
      tiposPagosId: hasValue(obj.tiposPagosId) ? +obj.tiposPagosId : undefined,
      anioPago: hasValue(obj.anioPago) ? +obj.anioPago : undefined,
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined,
      numRecibo: hasValue(obj.numRecibo) ? '' + obj.numRecibo : undefined,
      criterio: hasValue(obj.criterio) ? '' + obj.criterio : undefined,
      orden: hasValue(obj.orden) ? '' + obj.orden : undefined, // ASC, DESC
      registros: hasValue(obj.registros) ? +obj.registros : undefined,
      pagina: hasValue(obj.pagina) ? +obj.pagina : undefined
    });
  }
}

export class ReqPathGetObtenerGrupoRecibos {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerGrupoRecibos {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetObtenerGrupoRecibos {
  public tipoPoliza?: string;
  public tiposPagosId?: number;
  public anioPago?: number;
  public numeroPoliza?: string;
  public numRecibo?: string;
  public criterio?: string;
  public orden?: string;
  public registros?: number;
  public pagina?: number;

  public static create(obj): ReqQueryGetObtenerGrupoRecibos {
    return removeUndefinedFromObj({
      tipoPoliza: hasValue(obj.tipoPoliza) ? '' + obj.tipoPoliza : undefined, // , MD_AUTOS, MD_SALUD, MD_VIDA, MD_HOGAR, MD_DECESOS, MD_SCTR, MD_SOAT_ELECTRO, MD_EPS, MD_VIAJES, MD_SOAT, MD_OTROS, MD_AUTOS|MD_SOAT|MD_SOAT_ELECTRO, MD_AUTOS|MD_SOAT, MD_SALUD|MD_EPS
      tiposPagosId: hasValue(obj.tiposPagosId) ? +obj.tiposPagosId : undefined,
      anioPago: hasValue(obj.anioPago) ? +obj.anioPago : undefined,
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined,
      numRecibo: hasValue(obj.numRecibo) ? '' + obj.numRecibo : undefined,
      criterio: hasValue(obj.criterio) ? '' + obj.criterio : undefined,
      orden: hasValue(obj.orden) ? '' + obj.orden : undefined, // ASC, DESC
      registros: hasValue(obj.registros) ? +obj.registros : undefined,
      pagina: hasValue(obj.pagina) ? +obj.pagina : undefined
    });
  }
}

export class ReqPathGetObtenerDetalleRecibos {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerDetalleRecibos {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetObtenerDetalleRecibos {
  public tipoPoliza?: string;
  public tiposPagosId?: number;
  public anioPago?: number;
  public numeroPoliza?: string;
  public numRecibo?: string;
  public anioMes: string;
  public criterio?: string;
  public orden?: string;
  public registros?: number;
  public pagina?: number;

  public static create(obj): ReqQueryGetObtenerDetalleRecibos {
    return removeUndefinedFromObj({
      tipoPoliza: hasValue(obj.tipoPoliza) ? '' + obj.tipoPoliza : undefined, // , MD_AUTOS, MD_SALUD, MD_VIDA, MD_HOGAR, MD_DECESOS, MD_SCTR, MD_SOAT_ELECTRO, MD_EPS, MD_VIAJES, MD_SOAT, MD_OTROS, MD_AUTOS|MD_SOAT|MD_SOAT_ELECTRO, MD_AUTOS|MD_SOAT, MD_SALUD|MD_EPS
      tiposPagosId: hasValue(obj.tiposPagosId) ? +obj.tiposPagosId : undefined,
      anioPago: hasValue(obj.anioPago) ? +obj.anioPago : undefined,
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined,
      numRecibo: hasValue(obj.numRecibo) ? '' + obj.numRecibo : undefined,
      anioMes: hasValue(obj.anioMes) ? '' + obj.anioMes : undefined,
      criterio: hasValue(obj.criterio) ? '' + obj.criterio : undefined,
      orden: hasValue(obj.orden) ? '' + obj.orden : undefined, // ASC, DESC
      registros: hasValue(obj.registros) ? +obj.registros : undefined,
      pagina: hasValue(obj.pagina) ? +obj.pagina : undefined
    });
  }
}

export class ReqPathGetDescargarRecibo {
  public codigoApp: string;
  public numeroRecibo: string;

  public static create(obj): ReqPathGetDescargarRecibo {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroRecibo: hasValue(obj.numeroRecibo) ? '' + obj.numeroRecibo : undefined
    });
  }
}

export class ReqPathPostEnviarMailRecibo {
  public codigoApp: string;
  public numeroRecibo: string;

  public static create(obj): ReqPathPostEnviarMailRecibo {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroRecibo: hasValue(obj.numeroRecibo) ? '' + obj.numeroRecibo : undefined
    });
  }
}

export class ReqBodyPostEnviarMailRecibo {
  public destinatarios?: object[];

  public static create(obj): ReqBodyPostEnviarMailRecibo {
    return obj.destinatarios.map(v => v);
  }
}

export class ReqPathPostPagarRecibo {
  public codigoApp: string;

  public static create(obj): ReqPathPostPagarRecibo {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostPagarRecibo {
  public modalidadPago?: string;
  public tarjeta?: object;
  public recibo?: object;
  public transaccion?: object;

  public static create(obj): ReqBodyPostPagarRecibo {
    return removeUndefinedFromObj({
      modalidadPago: hasValue(obj.modalidadPago) ? '' + obj.modalidadPago : undefined, // PAGO_TARJETA, PAGO_BANCARIO
      tarjeta: hasValue(obj.tarjeta) ? obj.tarjeta : undefined,
      recibo: hasValue(obj.recibo) ? obj.recibo : undefined,
      transaccion: hasValue(obj.transaccion) ? obj.transaccion : undefined
    });
  }
}

export class ReqPathGetObtenerRecibosReordenado {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerRecibosReordenado {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetObtenerRecibosReordenado {
  public tipoPoliza?: string;
  public tiposPagosId?: number;
  public anioPago?: number;
  public numeroPoliza?: string;
  public numRecibo?: string;
  public criterio?: string;
  public orden?: string;
  public registros?: number;
  public pagina?: number;

  public static create(obj): ReqQueryGetObtenerRecibosReordenado {
    return removeUndefinedFromObj({
      tipoPoliza: hasValue(obj.tipoPoliza) ? '' + obj.tipoPoliza : undefined, // , MD_AUTOS, MD_SALUD, MD_VIDA, MD_HOGAR, MD_DECESOS, MD_SCTR, MD_SOAT_ELECTRO, MD_EPS, MD_VIAJES, MD_SOAT, MD_OTROS, MD_AUTOS|MD_SOAT|MD_SOAT_ELECTRO, MD_AUTOS|MD_SOAT, MD_SALUD|MD_EPS
      tiposPagosId: hasValue(obj.tiposPagosId) ? +obj.tiposPagosId : undefined,
      anioPago: hasValue(obj.anioPago) ? +obj.anioPago : undefined,
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined,
      numRecibo: hasValue(obj.numRecibo) ? '' + obj.numRecibo : undefined,
      criterio: hasValue(obj.criterio) ? '' + obj.criterio : undefined,
      orden: hasValue(obj.orden) ? '' + obj.orden : undefined, // ASC, DESC
      registros: hasValue(obj.registros) ? +obj.registros : undefined,
      pagina: hasValue(obj.pagina) ? +obj.pagina : undefined
    });
  }
}

export class ReqPathGetVerificarReciboAfiliado {
  public codigoApp: string;
  public tipoDocPago: string;
  public numDocPago: string;

  public static create(obj): ReqPathGetVerificarReciboAfiliado {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      tipoDocPago: hasValue(obj.tipoDocPago) ? '' + obj.tipoDocPago : undefined, // RECIBO, AVISO
      numDocPago: hasValue(obj.numDocPago) ? '' + obj.numDocPago : undefined
    });
  }
}

export class ReqQueryGetVerificarReciboAfiliado {
  public tipoPago?: string;

  public static create(obj): ReqQueryGetVerificarReciboAfiliado {
    return removeUndefinedFromObj({
      tipoPago: hasValue(obj.tipoPago) ? '' + obj.tipoPago : undefined // DEBITO_AUTOMATICO, PAGO_BANCARIO
    });
  }
}

export class ReqPathGetVerificarPagoRecibo {
  public codigoApp: string;

  public static create(obj): ReqPathGetVerificarPagoRecibo {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetVerificarPagoRecibo {
  public tipDocumPago: string;
  public numDocumPago: string;

  public static create(obj): ReqQueryGetVerificarPagoRecibo {
    return removeUndefinedFromObj({
      tipDocumPago: hasValue(obj.tipDocumPago) ? '' + obj.tipDocumPago : undefined, // RECIBO, AVISO
      numDocumPago: hasValue(obj.numDocumPago) ? '' + obj.numDocumPago : undefined
    });
  }
}

export class ReqPathGetListarTramites {
  public codigoApp: string;

  public static create(obj): ReqPathGetListarTramites {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetListarTramites {
  public tipoTramite: string;
  public tipoSeguro?: string;
  public criterio?: string;
  public orden?: string;
  public registros?: number;
  public pagina?: number;

  public static create(obj): ReqQueryGetListarTramites {
    return removeUndefinedFromObj({
      tipoTramite: hasValue(obj.tipoTramite) ? '' + obj.tipoTramite : undefined, // enCurso, historicos, todos
      tipoSeguro: hasValue(obj.tipoSeguro) ? '' + obj.tipoSeguro : undefined, // MD_AUTOS, MD_SALUD, MD_VIDA, MD_HOGAR, MD_DECESOS, MD_SCTR, MD_SOAT, MD_SOAT_ELECTRO, MD_EPS, MD_SOAT|MD_SOAT_ELECTRO, MD_SALUD|MD_EPS, MD_SALUD_COTIZACION, MD_TIPO_USUARIO, MD_SCTR_PENSION, MD_SCTR_SALUD, MD_VIAJES, MD_AUTOS|MD_SOAT, MD_AUTOS|MD_SOAT|MD_SOAT_ELECTRO, MD_OTROS
      criterio: hasValue(obj.criterio) ? '' + obj.criterio : undefined,
      orden: hasValue(obj.orden) ? '' + obj.orden : undefined, // ASC, DESC
      registros: hasValue(obj.registros) ? +obj.registros : undefined,
      pagina: hasValue(obj.pagina) ? +obj.pagina : undefined
    });
  }
}

export class ReqPathGetListarTramitesDetallado {
  public codigoApp: string;

  public static create(obj): ReqPathGetListarTramitesDetallado {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetDetalleSolicitudCartaGarantia {
  public codigoApp: string;

  public static create(obj): ReqPathGetDetalleSolicitudCartaGarantia {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetDetalleSolicitudCartaGarantia {
  public numeroSolicitud: number;
  public version: number;

  public static create(obj): ReqQueryGetDetalleSolicitudCartaGarantia {
    return removeUndefinedFromObj({
      numeroSolicitud: hasValue(obj.numeroSolicitud) ? +obj.numeroSolicitud : undefined,
      version: hasValue(obj.version) ? +obj.version : undefined
    });
  }
}

export class ReqPathGetListarPolizas {
  public codigoApp: string;

  public static create(obj): ReqPathGetListarPolizas {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetListarPolizas {
  public tipoPoliza?: string;
  public numeroPoliza?: string;
  public estadoPoliza?: string;
  public criterio?: string;
  public orden?: string;
  public registros?: number;
  public pagina?: number;
  public anio?: number;
  public activarSoatVencidos?: string;
  public pSeccion?: string;

  public static create(obj): ReqQueryGetListarPolizas {
    return removeUndefinedFromObj({
      tipoPoliza: hasValue(obj.tipoPoliza) ? '' + obj.tipoPoliza : undefined, // MD_AUTOS, MD_SALUD, MD_VIDA, MD_HOGAR, MD_DECESOS, MD_SCTR, MD_SOAT, MD_SOAT_ELECTRO, MD_EPS, MD_SOAT|MD_SOAT_ELECTRO, MD_SALUD|MD_EPS, MD_SALUD_COTIZACION, MD_TIPO_USUARIO, MD_SCTR_PENSION, MD_SCTR_SALUD, MD_VIAJES, MD_AUTOS|MD_SOAT, MD_AUTOS|MD_SOAT|MD_SOAT_ELECTRO, MD_OTROS
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined,
      estadoPoliza: hasValue(obj.estadoPoliza) ? '' + obj.estadoPoliza : undefined, // V, E, A, C
      criterio: hasValue(obj.criterio) ? '' + obj.criterio : undefined,
      orden: hasValue(obj.orden) ? '' + obj.orden : undefined, // ASC, DESC
      registros: hasValue(obj.registros) ? +obj.registros : undefined,
      pagina: hasValue(obj.pagina) ? +obj.pagina : undefined,
      anio: hasValue(obj.anio) ? +obj.anio : undefined,
      activarSoatVencidos: hasValue(obj.activarSoatVencidos) ? '' + obj.activarSoatVencidos : undefined,
      pSeccion: hasValue(obj.pSeccion) ? '' + obj.pSeccion : undefined // MisPolizas, Bandeja, HOME
    });
  }
}

export class ReqPathGetDescargarPoliza {
  public numeroPoliza: string;

  public static create(obj): ReqPathGetDescargarPoliza {
    return removeUndefinedFromObj({
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqPathGetDescargarPolizaCondicionado {
  public codigoApp: string;
  public numeroPoliza: string;

  public static create(obj): ReqPathGetDescargarPolizaCondicionado {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqPathGetObtenerTiposPolizasPorCliente {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerTiposPolizasPorCliente {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerTiposRamosPolizasPorCliente {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerTiposRamosPolizasPorCliente {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerCoberturasPoliza {
  public codigoApp: string;
  public numeroPoliza: string;

  public static create(obj): ReqPathGetObtenerCoberturasPoliza {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqQueryGetObtenerCoberturasPoliza {
  public registros?: number;
  public pagina?: number;
  public origenConsulta?: string;

  public static create(obj): ReqQueryGetObtenerCoberturasPoliza {
    return removeUndefinedFromObj({
      registros: hasValue(obj.registros) ? +obj.registros : undefined,
      pagina: hasValue(obj.pagina) ? +obj.pagina : undefined,
      origenConsulta: hasValue(obj.origenConsulta) ? '' + obj.origenConsulta : undefined // BUSCADOR_CLINICAS, DETALLE_POLIZA
    });
  }
}

export class ReqPathGetObtenerCoberturasVehicularesPoliza {
  public codigoApp: string;
  public numeroPoliza: string;
  public chasis: string;
  public nroMotor: string;

  public static create(obj): ReqPathGetObtenerCoberturasVehicularesPoliza {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined,
      chasis: hasValue(obj.chasis) ? '' + obj.chasis : undefined,
      nroMotor: hasValue(obj.nroMotor) ? '' + obj.nroMotor : undefined
    });
  }
}

export class ReqQueryGetObtenerCoberturasVehicularesPoliza {
  public registros?: number;
  public pagina?: number;

  public static create(obj): ReqQueryGetObtenerCoberturasVehicularesPoliza {
    return removeUndefinedFromObj({
      registros: hasValue(obj.registros) ? +obj.registros : undefined,
      pagina: hasValue(obj.pagina) ? +obj.pagina : undefined
    });
  }
}

export class ReqPathGetObtenerDeducibleVehicularPoliza {
  public codigoApp: string;
  public numeroPoliza: string;

  public static create(obj): ReqPathGetObtenerDeducibleVehicularPoliza {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqQueryGetObtenerDeducibleVehicularPoliza {
  public registros?: number;
  public pagina?: number;

  public static create(obj): ReqQueryGetObtenerDeducibleVehicularPoliza {
    return removeUndefinedFromObj({
      registros: hasValue(obj.registros) ? +obj.registros : undefined,
      pagina: hasValue(obj.pagina) ? +obj.pagina : undefined
    });
  }
}

export class ReqPathGetObtenerHogaresPoliza {
  public codigoApp: string;
  public numeroPoliza: string;

  public static create(obj): ReqPathGetObtenerHogaresPoliza {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqPathPostRegistrarSolicitudTraspasoPoliza {
  public codigoApp: string;
  public numeroPoliza: string;

  public static create(obj): ReqPathPostRegistrarSolicitudTraspasoPoliza {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqBodyPostRegistrarSolicitudTraspasoPoliza {
  public tipoDocumento?: string;
  public documento?: string;
  public apellidoPaterno?: string;
  public apellidoMaterno?: string;
  public nombre?: string;
  public telefono?: string;
  public email?: string;

  public static create(obj): ReqBodyPostRegistrarSolicitudTraspasoPoliza {
    return removeUndefinedFromObj({
      tipoDocumento: hasValue(obj.tipoDocumento) ? '' + obj.tipoDocumento : undefined, // DNI, RUC, PEX, CEX, CIP
      documento: hasValue(obj.documento) ? '' + obj.documento : undefined,
      apellidoPaterno: hasValue(obj.apellidoPaterno) ? '' + obj.apellidoPaterno : undefined,
      apellidoMaterno: hasValue(obj.apellidoMaterno) ? '' + obj.apellidoMaterno : undefined,
      nombre: hasValue(obj.nombre) ? '' + obj.nombre : undefined,
      telefono: hasValue(obj.telefono) ? '' + obj.telefono : undefined,
      email: hasValue(obj.email) ? '' + obj.email : undefined
    });
  }
}

export class ReqPathGetObtenerHojaAnexaPoliza {
  public codigoApp: string;
  public numeroPoliza: string;

  public static create(obj): ReqPathGetObtenerHojaAnexaPoliza {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqPathGetObtenerAgentePoliza {
  public codigoApp: string;
  public numeroPoliza: string;

  public static create(obj): ReqPathGetObtenerAgentePoliza {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqPathPostCambiarModalidadPolizaVehicular {
  public codigoApp: string;
  public numeroPoliza: string;

  public static create(obj): ReqPathPostCambiarModalidadPolizaVehicular {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqBodyPostCambiarModalidadPolizaVehicular {
  public datosPolizaVigente?: object;
  public datosPolizaNueva?: object;

  public static create(obj): ReqBodyPostCambiarModalidadPolizaVehicular {
    return removeUndefinedFromObj({
      datosPolizaVigente: hasValue(obj.datosPolizaVigente) ? obj.datosPolizaVigente : undefined,
      datosPolizaNueva: hasValue(obj.datosPolizaNueva) ? obj.datosPolizaNueva : undefined
    });
  }
}

export class ReqPathGetObtenerContratante {
  public codigoApp: string;
  public numPoliza: string;

  public static create(obj): ReqPathGetObtenerContratante {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numPoliza: hasValue(obj.numPoliza) ? '' + obj.numPoliza : undefined
    });
  }
}

export class ReqPathGetObtenerCategorias {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerCategorias {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetListadoPagosCuotas {
  public codigoApp: string;
  public numeroPoliza: string;

  public static create(obj): ReqPathGetListadoPagosCuotas {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqQueryGetListadoPagosCuotas {
  public codCia: number;
  public numSpto: number;

  public static create(obj): ReqQueryGetListadoPagosCuotas {
    return removeUndefinedFromObj({
      codCia: hasValue(obj.codCia) ? +obj.codCia : undefined,
      numSpto: hasValue(obj.numSpto) ? +obj.numSpto : undefined
    });
  }
}

export class ReqPathGetListarLimiteAnios {
  public codigoApp: string;

  public static create(obj): ReqPathGetListarLimiteAnios {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetListarLimiteAnios {
  public orden?: string;

  public static create(obj): ReqQueryGetListarLimiteAnios {
    return removeUndefinedFromObj({
      orden: hasValue(obj.orden) ? '' + obj.orden : undefined // ASC, DESC
    });
  }
}

export class ReqPathGetListarPolizasPaginado {
  public codigoApp: string;

  public static create(obj): ReqPathGetListarPolizasPaginado {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetListarPolizasPaginado {
  public tipoPoliza?: string;
  public numeroPoliza?: string;
  public estadoPoliza?: string;
  public criterio?: string;
  public orden?: string;
  public registros?: number;
  public pagina?: number;
  public anio?: number;
  public activarSoatVencidos?: string;
  public pSeccion?: string;

  public static create(obj): ReqQueryGetListarPolizasPaginado {
    return removeUndefinedFromObj({
      tipoPoliza: hasValue(obj.tipoPoliza) ? '' + obj.tipoPoliza : undefined, // MD_AUTOS, MD_SALUD, MD_VIDA, MD_HOGAR, MD_DECESOS, MD_SCTR, MD_SOAT, MD_SOAT_ELECTRO, MD_EPS, MD_SOAT|MD_SOAT_ELECTRO, MD_SALUD|MD_EPS, MD_SALUD_COTIZACION, MD_TIPO_USUARIO, MD_SCTR_PENSION, MD_SCTR_SALUD, MD_VIAJES, MD_AUTOS|MD_SOAT, MD_AUTOS|MD_SOAT|MD_SOAT_ELECTRO, MD_OTROS
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined,
      estadoPoliza: hasValue(obj.estadoPoliza) ? '' + obj.estadoPoliza : undefined, // V, E, A, C
      criterio: hasValue(obj.criterio) ? '' + obj.criterio : undefined,
      orden: hasValue(obj.orden) ? '' + obj.orden : undefined, // ASC, DESC
      registros: hasValue(obj.registros) ? +obj.registros : undefined,
      pagina: hasValue(obj.pagina) ? +obj.pagina : undefined,
      anio: hasValue(obj.anio) ? +obj.anio : undefined,
      activarSoatVencidos: hasValue(obj.activarSoatVencidos) ? '' + obj.activarSoatVencidos : undefined,
      pSeccion: hasValue(obj.pSeccion) ? '' + obj.pSeccion : undefined // MisPolizas, Bandeja, HOME
    });
  }
}

export class ReqPathGetObtenerDatosContratante {
  public numeroPoliza: string;

  public static create(obj): ReqPathGetObtenerDatosContratante {
    return removeUndefinedFromObj({
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqQueryGetObtenerDatosContratante {
  public codigoApp: string;
  public codCia: number;

  public static create(obj): ReqQueryGetObtenerDatosContratante {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB
      codCia: hasValue(obj.codCia) ? +obj.codCia : undefined // 1, 2, 3
    });
  }
}

export class ReqPathGetObtenerDatosTitular {
  public numeroPoliza: string;

  public static create(obj): ReqPathGetObtenerDatosTitular {
    return removeUndefinedFromObj({
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqQueryGetObtenerDatosTitular {
  public codigoApp: string;
  public codCia: number;

  public static create(obj): ReqQueryGetObtenerDatosTitular {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      codCia: hasValue(obj.codCia) ? +obj.codCia : undefined // 1, 2, 3
    });
  }
}

export class ReqPathGetObtenerVehiculo {
  public codigoApp: string;
  public numeroPoliza: string;
  public chasis: string;
  public nroMotor: string;

  public static create(obj): ReqPathGetObtenerVehiculo {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined,
      chasis: hasValue(obj.chasis) ? '' + obj.chasis : undefined,
      nroMotor: hasValue(obj.nroMotor) ? '' + obj.nroMotor : undefined
    });
  }
}

export class ReqPathGetObtenerVehiculosPorPoliza {
  public codigoApp: string;
  public numeroPoliza: string;

  public static create(obj): ReqPathGetObtenerVehiculosPorPoliza {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqPathPutActualizarPlaca {
  public codigoApp: string;
  public numeroPoliza: string;

  public static create(obj): ReqPathPutActualizarPlaca {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqBodyPutActualizarPlaca {
  public numeroChasis?: string;
  public numeroMotor?: string;
  public nuevaPlaca?: string;

  public static create(obj): ReqBodyPutActualizarPlaca {
    return removeUndefinedFromObj({
      numeroChasis: hasValue(obj.numeroChasis) ? '' + obj.numeroChasis : undefined,
      numeroMotor: hasValue(obj.numeroMotor) ? '' + obj.numeroMotor : undefined,
      nuevaPlaca: hasValue(obj.nuevaPlaca) ? '' + obj.nuevaPlaca : undefined
    });
  }
}

export class ReqPathGetObtenerVehiculoPorPlaca {
  public codigoApp: string;
  public placa: string;

  public static create(obj): ReqPathGetObtenerVehiculoPorPlaca {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      placa: hasValue(obj.placa) ? '' + obj.placa : undefined
    });
  }
}

export class ReqPathGetObtenerVehiculoPorUsuario {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerVehiculoPorUsuario {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathPostVerificarDisponibilidadAutoprocuracion {
  public codigoApp: string;

  public static create(obj): ReqPathPostVerificarDisponibilidadAutoprocuracion {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostVerificarDisponibilidadAutoprocuracion {
  public numeroPoliza?: string;
  public placa?: string;
  public solicitudAsistencia?: object;

  public static create(obj): ReqBodyPostVerificarDisponibilidadAutoprocuracion {
    return removeUndefinedFromObj({
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined,
      placa: hasValue(obj.placa) ? '' + obj.placa : undefined,
      solicitudAsistencia: hasValue(obj.solicitudAsistencia) ? obj.solicitudAsistencia : undefined
    });
  }
}

export class ReqPathPostRegistrarAccesorio {
  public codigoApp: string;
  public numeroPoliza: string;
  public chasis: string;
  public nroMotor: string;

  public static create(obj): ReqPathPostRegistrarAccesorio {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined,
      chasis: hasValue(obj.chasis) ? '' + obj.chasis : undefined,
      nroMotor: hasValue(obj.nroMotor) ? '' + obj.nroMotor : undefined
    });
  }
}

export class ReqBodyPostRegistrarAccesorio {
  public tipoAccesorioId?: number;
  public tipoAccesorioDescripcion?: string;
  public modelo?: string;
  public monto?: number;

  public static create(obj): ReqBodyPostRegistrarAccesorio {
    return removeUndefinedFromObj({
      tipoAccesorioId: hasValue(obj.tipoAccesorioId) ? +obj.tipoAccesorioId : undefined,
      tipoAccesorioDescripcion: hasValue(obj.tipoAccesorioDescripcion) ? '' + obj.tipoAccesorioDescripcion : undefined,
      modelo: hasValue(obj.modelo) ? '' + obj.modelo : undefined,
      monto: hasValue(obj.monto) ? +obj.monto : undefined
    });
  }
}

export class ReqPathPutActualizarAccesorio {
  public codigoApp: string;
  public numeroPoliza: string;
  public chasis: string;
  public nroMotor: string;
  public accesorioId: string;

  public static create(obj): ReqPathPutActualizarAccesorio {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined,
      chasis: hasValue(obj.chasis) ? '' + obj.chasis : undefined,
      nroMotor: hasValue(obj.nroMotor) ? '' + obj.nroMotor : undefined,
      accesorioId: hasValue(obj.accesorioId) ? '' + obj.accesorioId : undefined
    });
  }
}

export class ReqBodyPutActualizarAccesorio {
  public tipoAccesorioId?: number;
  public tipoAccesorioDescripcion?: string;
  public modelo?: string;
  public monto?: number;

  public static create(obj): ReqBodyPutActualizarAccesorio {
    return removeUndefinedFromObj({
      tipoAccesorioId: hasValue(obj.tipoAccesorioId) ? +obj.tipoAccesorioId : undefined,
      tipoAccesorioDescripcion: hasValue(obj.tipoAccesorioDescripcion) ? '' + obj.tipoAccesorioDescripcion : undefined,
      modelo: hasValue(obj.modelo) ? '' + obj.modelo : undefined,
      monto: hasValue(obj.monto) ? +obj.monto : undefined
    });
  }
}

export class ReqPathDeleteEliminarAccesorio {
  public codigoApp: string;
  public numeroPoliza: string;
  public chasis: string;
  public nroMotor: string;
  public accesorioId: string;

  public static create(obj): ReqPathDeleteEliminarAccesorio {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined,
      chasis: hasValue(obj.chasis) ? '' + obj.chasis : undefined,
      nroMotor: hasValue(obj.nroMotor) ? '' + obj.nroMotor : undefined,
      accesorioId: hasValue(obj.accesorioId) ? '' + obj.accesorioId : undefined
    });
  }
}

export class ReqPathGetObtenerAccidentes {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerAccidentes {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerRobos {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerRobos {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerAccidentesListaUnica {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerAccidentesListaUnica {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerRobosListaUnica {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerRobosListaUnica {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerDetallesAccidente {
  public codigoApp: string;
  public numeroAsistencia: string;

  public static create(obj): ReqPathGetObtenerDetallesAccidente {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroAsistencia: hasValue(obj.numeroAsistencia) ? '' + obj.numeroAsistencia : undefined
    });
  }
}

export class ReqPathGetObtenerDetallesRobo {
  public codigoApp: string;
  public numeroAsistencia: string;

  public static create(obj): ReqPathGetObtenerDetallesRobo {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroAsistencia: hasValue(obj.numeroAsistencia) ? '' + obj.numeroAsistencia : undefined
    });
  }
}

export class ReqBodyPostCalificarTaller {
  public codigoApp?: string;
  public tipoDocumento?: string;
  public documento?: string;
  public numeroAsistencia?: string;
  public identificadorTaller?: number;
  public calificacion?: number;

  public static create(obj): ReqBodyPostCalificarTaller {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      tipoDocumento: hasValue(obj.tipoDocumento) ? '' + obj.tipoDocumento : undefined,
      documento: hasValue(obj.documento) ? '' + obj.documento : undefined,
      numeroAsistencia: hasValue(obj.numeroAsistencia) ? '' + obj.numeroAsistencia : undefined,
      identificadorTaller: hasValue(obj.identificadorTaller) ? +obj.identificadorTaller : undefined,
      calificacion: hasValue(obj.calificacion) ? +obj.calificacion : undefined
    });
  }
}

export class ReqPathPostAdjuntarCartaNoAdeudo {
  public codigoApp: string;

  public static create(obj): ReqPathPostAdjuntarCartaNoAdeudo {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqFormDataPostAdjuntarCartaNoAdeudo {
  public numeroAsistencia: number;
  public archivos: any;

  public static create(obj): ReqFormDataPostAdjuntarCartaNoAdeudo {
    return removeUndefinedFromObj({
      numeroAsistencia: hasValue(obj.numeroAsistencia) ? +obj.numeroAsistencia : undefined,
      archivos: hasValue(obj.archivos) ? obj.archivos : undefined
    });
  }
}

export class ReqPathGetObtenerMarcasCotizacion {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerMarcasCotizacion {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerModelosCotizacion {
  public codigoApp: string;
  public marca: number;

  public static create(obj): ReqPathGetObtenerModelosCotizacion {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      marca: hasValue(obj.marca) ? +obj.marca : undefined
    });
  }
}

export class ReqPathGetObtenerTiposVehiculo {
  public codigoApp: string;
  public marca: number;
  public modelo: number;

  public static create(obj): ReqPathGetObtenerTiposVehiculo {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      marca: hasValue(obj.marca) ? +obj.marca : undefined,
      modelo: hasValue(obj.modelo) ? +obj.modelo : undefined
    });
  }
}

export class ReqPathGetObtenerValorSugerido {
  public codigoApp: string;
  public marca: number;
  public modelo: number;
  public anio: number;
  public tipoVehiculo: number;

  public static create(obj): ReqPathGetObtenerValorSugerido {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      marca: hasValue(obj.marca) ? +obj.marca : undefined,
      modelo: hasValue(obj.modelo) ? +obj.modelo : undefined,
      anio: hasValue(obj.anio) ? +obj.anio : undefined,
      tipoVehiculo: hasValue(obj.tipoVehiculo) ? +obj.tipoVehiculo : undefined
    });
  }
}

export class ReqQueryGetObtenerValorSugerido {
  public es0KM?: any;
  public usoVehiculo?: number;

  public static create(obj): ReqQueryGetObtenerValorSugerido {
    return removeUndefinedFromObj({
      es0KM: hasValue(obj.es0KM) ? obj.es0KM : undefined,
      usoVehiculo: hasValue(obj.usoVehiculo) ? +obj.usoVehiculo : undefined
    });
  }
}

export class ReqPathGetObtenerUsosVehiculo {
  public codigoApp: string;
  public marca: number;
  public modelo: number;
  public tipoVehiculo: number;

  public static create(obj): ReqPathGetObtenerUsosVehiculo {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      marca: hasValue(obj.marca) ? +obj.marca : undefined,
      modelo: hasValue(obj.modelo) ? +obj.modelo : undefined,
      tipoVehiculo: hasValue(obj.tipoVehiculo) ? +obj.tipoVehiculo : undefined
    });
  }
}

export class ReqPathGetObtenerCiudades {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerCiudades {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetBuscarVehiculoPorPlaca {
  public codigoApp: string;
  public placa: string;

  public static create(obj): ReqPathGetBuscarVehiculoPorPlaca {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      placa: hasValue(obj.placa) ? '' + obj.placa : undefined
    });
  }
}

export class ReqPathGetValidarGPS {
  public codigoApp: string;
  public marca: number;
  public modelo: number;
  public anio: number;

  public static create(obj): ReqPathGetValidarGPS {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      marca: hasValue(obj.marca) ? +obj.marca : undefined,
      modelo: hasValue(obj.modelo) ? +obj.modelo : undefined,
      anio: hasValue(obj.anio) ? +obj.anio : undefined
    });
  }
}

export class ReqQueryGetValidarGPS {
  public es0KM?: any;
  public tipoVehiculo?: number;
  public usoVehiculo?: number;

  public static create(obj): ReqQueryGetValidarGPS {
    return removeUndefinedFromObj({
      es0KM: hasValue(obj.es0KM) ? obj.es0KM : undefined,
      tipoVehiculo: hasValue(obj.tipoVehiculo) ? +obj.tipoVehiculo : undefined,
      usoVehiculo: hasValue(obj.usoVehiculo) ? +obj.usoVehiculo : undefined
    });
  }
}

export class ReqPathPostCotizarPolizaVehicularProductos {
  public codigoApp: string;

  public static create(obj): ReqPathPostCotizarPolizaVehicularProductos {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryPostCotizarPolizaVehicularProductos {
  public numeroPoliza?: string;

  public static create(obj): ReqQueryPostCotizarPolizaVehicularProductos {
    return removeUndefinedFromObj({
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqBodyPostCotizarPolizaVehicularProductos {
  public marcaId?: number;
  public modeloId?: number;
  public anio?: number;
  public tipoVehiculoId?: number;
  public es0KM?: any;
  public codigoMoneda?: number;
  public valorSugerido?: number;
  public usoVehiculoId?: number;
  public ciudadId?: number;
  public tienePlaca?: any;
  public placa?: string;

  public static create(obj): ReqBodyPostCotizarPolizaVehicularProductos {
    return removeUndefinedFromObj({
      marcaId: hasValue(obj.marcaId) ? +obj.marcaId : undefined,
      modeloId: hasValue(obj.modeloId) ? +obj.modeloId : undefined,
      anio: hasValue(obj.anio) ? +obj.anio : undefined,
      tipoVehiculoId: hasValue(obj.tipoVehiculoId) ? +obj.tipoVehiculoId : undefined,
      es0KM: hasValue(obj.es0KM) ? obj.es0KM : undefined,
      codigoMoneda: hasValue(obj.codigoMoneda) ? +obj.codigoMoneda : undefined, // 2, 1
      valorSugerido: hasValue(obj.valorSugerido) ? +obj.valorSugerido : undefined,
      usoVehiculoId: hasValue(obj.usoVehiculoId) ? +obj.usoVehiculoId : undefined,
      ciudadId: hasValue(obj.ciudadId) ? +obj.ciudadId : undefined,
      tienePlaca: hasValue(obj.tienePlaca) ? obj.tienePlaca : undefined,
      placa: hasValue(obj.placa) ? '' + obj.placa : undefined
    });
  }
}

export class ReqPathPostCotizarPolizaVehicularCoberturas {
  public codigoApp: string;

  public static create(obj): ReqPathPostCotizarPolizaVehicularCoberturas {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryPostCotizarPolizaVehicularCoberturas {
  public numeroPoliza?: string;

  public static create(obj): ReqQueryPostCotizarPolizaVehicularCoberturas {
    return removeUndefinedFromObj({
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqBodyPostCotizarPolizaVehicularCoberturas {
  public marcaId?: number;
  public modeloId?: number;
  public anio?: number;
  public tipoVehiculoId?: number;
  public es0KM?: any;
  public codigoMoneda?: number;
  public valorSugerido?: number;
  public usoVehiculoId?: number;
  public ciudadId?: number;
  public tienePlaca?: any;
  public placa?: string;

  public static create(obj): ReqBodyPostCotizarPolizaVehicularCoberturas {
    return removeUndefinedFromObj({
      marcaId: hasValue(obj.marcaId) ? +obj.marcaId : undefined,
      modeloId: hasValue(obj.modeloId) ? +obj.modeloId : undefined,
      anio: hasValue(obj.anio) ? +obj.anio : undefined,
      tipoVehiculoId: hasValue(obj.tipoVehiculoId) ? +obj.tipoVehiculoId : undefined,
      es0KM: hasValue(obj.es0KM) ? obj.es0KM : undefined,
      codigoMoneda: hasValue(obj.codigoMoneda) ? +obj.codigoMoneda : undefined, // 2, 1
      valorSugerido: hasValue(obj.valorSugerido) ? +obj.valorSugerido : undefined,
      usoVehiculoId: hasValue(obj.usoVehiculoId) ? +obj.usoVehiculoId : undefined,
      ciudadId: hasValue(obj.ciudadId) ? +obj.ciudadId : undefined,
      tienePlaca: hasValue(obj.tienePlaca) ? obj.tienePlaca : undefined,
      placa: hasValue(obj.placa) ? '' + obj.placa : undefined
    });
  }
}

export class ReqPathGetObtenerPDFTalleresPreferentes {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerPDFTalleresPreferentes {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerColorPorVehiculos {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerColorPorVehiculos {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerChasisMotorPorPlaca {
  public codigoApp: string;
  public placa: string;

  public static create(obj): ReqPathGetObtenerChasisMotorPorPlaca {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      placa: hasValue(obj.placa) ? '' + obj.placa : undefined
    });
  }
}

export class ReqPathGetValidarPolizaPorChasisMotor {
  public codigoApp: string;
  public placa: string;
  public chasis: string;
  public motor: string;

  public static create(obj): ReqPathGetValidarPolizaPorChasisMotor {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      placa: hasValue(obj.placa) ? '' + obj.placa : undefined,
      chasis: hasValue(obj.chasis) ? '' + obj.chasis : undefined,
      motor: hasValue(obj.motor) ? '' + obj.motor : undefined
    });
  }
}

export class ReqPathGetValidarClausula {
  public codigoApp: string;

  public static create(obj): ReqPathGetValidarClausula {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerCondicionInspeccion {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerCondicionInspeccion {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetObtenerCondicionInspeccion {
  public placa: string;
  public numeroChasis: string;
  public numeroMotor: string;

  public static create(obj): ReqQueryGetObtenerCondicionInspeccion {
    return removeUndefinedFromObj({
      placa: hasValue(obj.placa) ? '' + obj.placa : undefined,
      numeroChasis: hasValue(obj.numeroChasis) ? '' + obj.numeroChasis : undefined,
      numeroMotor: hasValue(obj.numeroMotor) ? '' + obj.numeroMotor : undefined
    });
  }
}

export class ReqPathGetValidarInspecciones {
  public codigoApp: string;

  public static create(obj): ReqPathGetValidarInspecciones {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetValidarInspecciones {
  public codigoMarca: number;
  public codigoModelo: number;
  public anio: number;
  public numeroChasis: string;
  public numeroMotor: string;
  public codigoTipoVehiculo: number;
  public codigoMonedaValorSugerido: number;
  public sumaAsegurada: number;
  public accesorios: number;
  public esCeroKm: any;
  public codigoModalidad: string;
  public numeroPlaca: string;

  public static create(obj): ReqQueryGetValidarInspecciones {
    return removeUndefinedFromObj({
      codigoMarca: hasValue(obj.codigoMarca) ? +obj.codigoMarca : undefined,
      codigoModelo: hasValue(obj.codigoModelo) ? +obj.codigoModelo : undefined,
      anio: hasValue(obj.anio) ? +obj.anio : undefined,
      numeroChasis: hasValue(obj.numeroChasis) ? '' + obj.numeroChasis : undefined,
      numeroMotor: hasValue(obj.numeroMotor) ? '' + obj.numeroMotor : undefined,
      codigoTipoVehiculo: hasValue(obj.codigoTipoVehiculo) ? +obj.codigoTipoVehiculo : undefined,
      codigoMonedaValorSugerido: hasValue(obj.codigoMonedaValorSugerido) ? +obj.codigoMonedaValorSugerido : undefined,
      sumaAsegurada: hasValue(obj.sumaAsegurada) ? +obj.sumaAsegurada : undefined,
      accesorios: hasValue(obj.accesorios) ? +obj.accesorios : undefined,
      esCeroKm: hasValue(obj.esCeroKm) ? obj.esCeroKm : undefined,
      codigoModalidad: hasValue(obj.codigoModalidad) ? '' + obj.codigoModalidad : undefined,
      numeroPlaca: hasValue(obj.numeroPlaca) ? '' + obj.numeroPlaca : undefined
    });
  }
}

export class ReqPathGetRangoHorasInspeccionPath {
  public codigoApp: string;

  public static create(obj): ReqPathGetRangoHorasInspeccionPath {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetIndicacionesPolizaElectronica {
  public codigoApp: string;

  public static create(obj): ReqPathGetIndicacionesPolizaElectronica {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerCondicionesProducto {
  public codigoApp: string;
  public codigoModalidad: string;

  public static create(obj): ReqPathGetObtenerCondicionesProducto {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      codigoModalidad: hasValue(obj.codigoModalidad) ? '' + obj.codigoModalidad : undefined
    });
  }
}

export class ReqPathPostGenerarSolicitudInspeccion {
  public codigoApp: string;

  public static create(obj): ReqPathPostGenerarSolicitudInspeccion {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostGenerarSolicitudInspeccion {
  public datosContratante?: object;
  public datosVehiculo?: object;
  public datosInspeccion?: object;

  public static create(obj): ReqBodyPostGenerarSolicitudInspeccion {
    return removeUndefinedFromObj({
      datosContratante: hasValue(obj.datosContratante) ? obj.datosContratante : undefined,
      datosVehiculo: hasValue(obj.datosVehiculo) ? obj.datosVehiculo : undefined,
      datosInspeccion: hasValue(obj.datosInspeccion) ? obj.datosInspeccion : undefined
    });
  }
}

export class ReqPathPostCotizarVehiculoProducto {
  public codigoApp: string;

  public static create(obj): ReqPathPostCotizarVehiculoProducto {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostCotizarVehiculoProducto {
  public numeroCotizacion?: string;
  public fechaInicioVigencia?: string;
  public codigoCompania?: number;
  public codigoRamo?: number;
  public codigoModalidad?: string;
  public numeroPlaca?: string;
  public tipoVehiculo?: number;
  public codigoMarca?: number;
  public codigoModelo?: number;
  public anioFabricacion?: number;
  public tipoUsoVehiculo?: number;
  public sumaAsegurada?: number;
  public esCeroKm?: any;
  public ciudadId?: number;
  public tienePlaca?: any;
  public codigoMoneda?: number;
  public numeroCuota?: number;
  public codigoFraccionamiento?: string;

  public static create(obj): ReqBodyPostCotizarVehiculoProducto {
    return removeUndefinedFromObj({
      numeroCotizacion: hasValue(obj.numeroCotizacion) ? '' + obj.numeroCotizacion : undefined,
      fechaInicioVigencia: hasValue(obj.fechaInicioVigencia) ? '' + obj.fechaInicioVigencia : undefined,
      codigoCompania: hasValue(obj.codigoCompania) ? +obj.codigoCompania : undefined,
      codigoRamo: hasValue(obj.codigoRamo) ? +obj.codigoRamo : undefined,
      codigoModalidad: hasValue(obj.codigoModalidad) ? '' + obj.codigoModalidad : undefined,
      numeroPlaca: hasValue(obj.numeroPlaca) ? '' + obj.numeroPlaca : undefined,
      tipoVehiculo: hasValue(obj.tipoVehiculo) ? +obj.tipoVehiculo : undefined,
      codigoMarca: hasValue(obj.codigoMarca) ? +obj.codigoMarca : undefined,
      codigoModelo: hasValue(obj.codigoModelo) ? +obj.codigoModelo : undefined,
      anioFabricacion: hasValue(obj.anioFabricacion) ? +obj.anioFabricacion : undefined,
      tipoUsoVehiculo: hasValue(obj.tipoUsoVehiculo) ? +obj.tipoUsoVehiculo : undefined,
      sumaAsegurada: hasValue(obj.sumaAsegurada) ? +obj.sumaAsegurada : undefined,
      esCeroKm: hasValue(obj.esCeroKm) ? obj.esCeroKm : undefined,
      ciudadId: hasValue(obj.ciudadId) ? +obj.ciudadId : undefined,
      tienePlaca: hasValue(obj.tienePlaca) ? obj.tienePlaca : undefined,
      codigoMoneda: hasValue(obj.codigoMoneda) ? +obj.codigoMoneda : undefined, // 2, 1
      numeroCuota: hasValue(obj.numeroCuota) ? +obj.numeroCuota : undefined,
      codigoFraccionamiento: hasValue(obj.codigoFraccionamiento) ? '' + obj.codigoFraccionamiento : undefined
    });
  }
}

export class ReqPathPostEmitirVehiculoProducto {
  public codigoApp: string;

  public static create(obj): ReqPathPostEmitirVehiculoProducto {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostEmitirVehiculoProducto {
  public poliza?: object;
  public producto?: object;
  public vehiculo?: object;
  public pago?: object;
  public tarjeta?: object;

  public static create(obj): ReqBodyPostEmitirVehiculoProducto {
    return removeUndefinedFromObj({
      poliza: hasValue(obj.poliza) ? obj.poliza : undefined,
      producto: hasValue(obj.producto) ? obj.producto : undefined,
      vehiculo: hasValue(obj.vehiculo) ? obj.vehiculo : undefined,
      pago: hasValue(obj.pago) ? obj.pago : undefined,
      tarjeta: hasValue(obj.tarjeta) ? obj.tarjeta : undefined
    });
  }
}

export class ReqPathPostPreregistrarRobo {
  public codigoApp: string;

  public static create(obj): ReqPathPostPreregistrarRobo {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostPreregistrarRobo {
  public numeroPoliza?: string;
  public placa?: string;
  public preguntas?: any[];
  public solicitudAsistencia?: object;

  public static create(obj): ReqBodyPostPreregistrarRobo {
    return removeUndefinedFromObj({
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined,
      placa: hasValue(obj.placa) ? '' + obj.placa : undefined,
      preguntas: hasValue(obj.preguntas) ? obj.preguntas : undefined,
      solicitudAsistencia: hasValue(obj.solicitudAsistencia) ? obj.solicitudAsistencia : undefined
    });
  }
}

export class ReqPathPostRegistrarFotoRobo {
  public codigoApp: string;

  public static create(obj): ReqPathPostRegistrarFotoRobo {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqFormDataPostRegistrarFotoRobo {
  public roboId: number;
  public tipoFoto: string;
  public foto: any;

  public static create(obj): ReqFormDataPostRegistrarFotoRobo {
    return removeUndefinedFromObj({
      roboId: hasValue(obj.roboId) ? +obj.roboId : undefined,
      tipoFoto: hasValue(obj.tipoFoto) ? '' + obj.tipoFoto : undefined, // AUTOPROC_ROBO_FOTO_ENTORNO, AUTOPROC_ROBO_FOTO_DANIO_01, AUTOPROC_ROBO_FOTO_DANIO_02, AUTOPROC_ROBO_FOTO_DANIO_03
      foto: hasValue(obj.foto) ? obj.foto : undefined
    });
  }
}

export class ReqPathDeleteEliminarFotoRobo {
  public codigoApp: string;
  public roboId: number;
  public tipoFoto: string;

  public static create(obj): ReqPathDeleteEliminarFotoRobo {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      roboId: hasValue(obj.roboId) ? +obj.roboId : undefined,
      tipoFoto: hasValue(obj.tipoFoto) ? '' + obj.tipoFoto : undefined // AUTOPROC_ROBO_FOTO_ENTORNO, AUTOPROC_ROBO_FOTO_DANIO_01, AUTOPROC_ROBO_FOTO_DANIO_02, AUTOPROC_ROBO_FOTO_DANIO_03
    });
  }
}

export class ReqPathPostRegistrarRobo {
  public codigoApp: string;
  public roboId: number;

  public static create(obj): ReqPathPostRegistrarRobo {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      roboId: hasValue(obj.roboId) ? +obj.roboId : undefined
    });
  }
}

export class ReqBodyPostRegistrarRobo {
  public roboId?: number;
  public fecha?: string;
  public hora?: string;
  public relato?: string;
  public ubicacionSiniestro?: object;

  public static create(obj): ReqBodyPostRegistrarRobo {
    return removeUndefinedFromObj({
      roboId: hasValue(obj.roboId) ? +obj.roboId : undefined,
      fecha: hasValue(obj.fecha) ? '' + obj.fecha : undefined,
      hora: hasValue(obj.hora) ? '' + obj.hora : undefined,
      relato: hasValue(obj.relato) ? '' + obj.relato : undefined,
      ubicacionSiniestro: hasValue(obj.ubicacionSiniestro) ? obj.ubicacionSiniestro : undefined
    });
  }
}

export class ReqPathPostPreregistrarAccidente {
  public codigoApp: string;

  public static create(obj): ReqPathPostPreregistrarAccidente {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostPreregistrarAccidente {
  public numeroPoliza?: string;
  public placa?: string;
  public preguntas?: any[];
  public solicitudAsistencia?: object;

  public static create(obj): ReqBodyPostPreregistrarAccidente {
    return removeUndefinedFromObj({
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined,
      placa: hasValue(obj.placa) ? '' + obj.placa : undefined,
      preguntas: hasValue(obj.preguntas) ? obj.preguntas : undefined,
      solicitudAsistencia: hasValue(obj.solicitudAsistencia) ? obj.solicitudAsistencia : undefined
    });
  }
}

export class ReqPathPostRegistrarFotoAccidente {
  public codigoApp: string;

  public static create(obj): ReqPathPostRegistrarFotoAccidente {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqFormDataPostRegistrarFotoAccidente {
  public accidenteId: number;
  public tipoFoto: string;
  public foto: any;

  public static create(obj): ReqFormDataPostRegistrarFotoAccidente {
    return removeUndefinedFromObj({
      accidenteId: hasValue(obj.accidenteId) ? +obj.accidenteId : undefined,
      tipoFoto: hasValue(obj.tipoFoto) ? '' + obj.tipoFoto : undefined, // AUTOPROC_ACCIDENTE_FOTO_ENTORNO, AUTOPROC_ACCIDENTE_FOTO_BREVETE_FRONTAL, AUTOPROC_ACCIDENTE_FOTO_BREVETE_REVERSO, AUTOPROC_ACCIDENTE_FOTO_DANIO_01, AUTOPROC_ACCIDENTE_FOTO_DANIO_02, AUTOPROC_ACCIDENTE_FOTO_DANIO_03
      foto: hasValue(obj.foto) ? obj.foto : undefined
    });
  }
}

export class ReqPathDeleteEliminarFotoAccidente {
  public codigoApp: string;
  public accidenteId: number;
  public tipoFoto: string;

  public static create(obj): ReqPathDeleteEliminarFotoAccidente {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      accidenteId: hasValue(obj.accidenteId) ? +obj.accidenteId : undefined,
      tipoFoto: hasValue(obj.tipoFoto) ? '' + obj.tipoFoto : undefined // AUTOPROC_ACCIDENTE_FOTO_ENTORNO, AUTOPROC_ACCIDENTE_FOTO_BREVETE_FRONTAL, AUTOPROC_ACCIDENTE_FOTO_BREVETE_REVERSO, AUTOPROC_ACCIDENTE_FOTO_DANIO_01, AUTOPROC_ACCIDENTE_FOTO_DANIO_02, AUTOPROC_ACCIDENTE_FOTO_DANIO_03
    });
  }
}

export class ReqPathPostRegistrarAccidente {
  public codigoApp: string;
  public accidenteId: number;

  public static create(obj): ReqPathPostRegistrarAccidente {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      accidenteId: hasValue(obj.accidenteId) ? +obj.accidenteId : undefined
    });
  }
}

export class ReqBodyPostRegistrarAccidente {
  public accidenteId?: number;
  public fecha?: string;
  public hora?: string;
  public relato?: string;
  public ocurrenciaAccidenteId?: number;
  public ocurrenciaTitulo?: string;
  public ocurrenciaDescripcion?: string;
  public ubicacionSiniestro?: object;
  public conductor?: string;
  public datosConductor?: object;

  public static create(obj): ReqBodyPostRegistrarAccidente {
    return removeUndefinedFromObj({
      accidenteId: hasValue(obj.accidenteId) ? +obj.accidenteId : undefined,
      fecha: hasValue(obj.fecha) ? '' + obj.fecha : undefined,
      hora: hasValue(obj.hora) ? '' + obj.hora : undefined,
      relato: hasValue(obj.relato) ? '' + obj.relato : undefined,
      ocurrenciaAccidenteId: hasValue(obj.ocurrenciaAccidenteId) ? +obj.ocurrenciaAccidenteId : undefined,
      ocurrenciaTitulo: hasValue(obj.ocurrenciaTitulo) ? '' + obj.ocurrenciaTitulo : undefined,
      ocurrenciaDescripcion: hasValue(obj.ocurrenciaDescripcion) ? '' + obj.ocurrenciaDescripcion : undefined,
      ubicacionSiniestro: hasValue(obj.ubicacionSiniestro) ? obj.ubicacionSiniestro : undefined,
      conductor: hasValue(obj.conductor) ? '' + obj.conductor : undefined, // USUARIO_MANEJABA, OTRO_CONDUCTOR, SIN_CONDUCTOR
      datosConductor: hasValue(obj.datosConductor) ? obj.datosConductor : undefined
    });
  }
}

export class ReqQueryPutActualizarAseguradosPoliza {
  public codigoApp: string;

  public static create(obj): ReqQueryPutActualizarAseguradosPoliza {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPutActualizarAseguradosPoliza {
  public codAfiliado?: string;
  public telefono?: string;
  public correo?: string;
  public polizas?: any[];

  public static create(obj): ReqBodyPutActualizarAseguradosPoliza {
    return removeUndefinedFromObj({
      codAfiliado: hasValue(obj.codAfiliado) ? '' + obj.codAfiliado : undefined,
      telefono: hasValue(obj.telefono) ? '' + obj.telefono : undefined,
      correo: hasValue(obj.correo) ? '' + obj.correo : undefined,
      polizas: hasValue(obj.polizas) ? obj.polizas : undefined
    });
  }
}

export class ReqPathGetObtenerAseguradosPoliza {
  public codigoApp: string;
  public numeroPoliza: string;

  public static create(obj): ReqPathGetObtenerAseguradosPoliza {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqPathGetObtenerAseguradosTipoUsuario {
  public numeroPoliza: string;

  public static create(obj): ReqPathGetObtenerAseguradosTipoUsuario {
    return removeUndefinedFromObj({
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqQueryGetObtenerAseguradosTipoUsuario {
  public codigoApp: string;

  public static create(obj): ReqQueryGetObtenerAseguradosTipoUsuario {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryPutActualizarDatosPersonalesAsegurado {
  public numeroPoliza: string;
  public tipoDocumentoAsegurado: string;
  public numeroDocumentoAsegurado: string;

  public static create(obj): ReqQueryPutActualizarDatosPersonalesAsegurado {
    return removeUndefinedFromObj({
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined,
      tipoDocumentoAsegurado: hasValue(obj.tipoDocumentoAsegurado) ? '' + obj.tipoDocumentoAsegurado : undefined, // DNI, RUC, PEX, CEX, CIP
      numeroDocumentoAsegurado: hasValue(obj.numeroDocumentoAsegurado) ? '' + obj.numeroDocumentoAsegurado : undefined
    });
  }
}

export class ReqFormDataPutActualizarDatosPersonalesAsegurado {
  public tipoDocumentoModificado?: string;
  public numeroDocumentoModificado?: string;
  public apellidoPaterno?: string;
  public apellidoMaterno?: string;
  public nombre?: string;
  public fechaNacimiento?: string;
  public relacionId?: number;
  public telefonoMovil?: string;
  public correoElectronico?: string;
  public fotosDocumento?: any;

  public static create(obj): ReqFormDataPutActualizarDatosPersonalesAsegurado {
    return removeUndefinedFromObj({
      tipoDocumentoModificado: hasValue(obj.tipoDocumentoModificado) ? '' + obj.tipoDocumentoModificado : undefined, // DNI, RUC, PEX, CEX, CIP
      numeroDocumentoModificado: hasValue(obj.numeroDocumentoModificado)
        ? '' + obj.numeroDocumentoModificado
        : undefined,
      apellidoPaterno: hasValue(obj.apellidoPaterno) ? '' + obj.apellidoPaterno : undefined,
      apellidoMaterno: hasValue(obj.apellidoMaterno) ? '' + obj.apellidoMaterno : undefined,
      nombre: hasValue(obj.nombre) ? '' + obj.nombre : undefined,
      fechaNacimiento: hasValue(obj.fechaNacimiento) ? '' + obj.fechaNacimiento : undefined,
      relacionId: hasValue(obj.relacionId) ? +obj.relacionId : undefined,
      telefonoMovil: hasValue(obj.telefonoMovil) ? '' + obj.telefonoMovil : undefined,
      correoElectronico: hasValue(obj.correoElectronico) ? '' + obj.correoElectronico : undefined,
      fotosDocumento: hasValue(obj.fotosDocumento) ? obj.fotosDocumento : undefined
    });
  }
}

export class ReqPathGetObtenerExclusionesPorAsegurado {
  public codigoApp: string;
  public numeroPoliza: string;

  public static create(obj): ReqPathGetObtenerExclusionesPorAsegurado {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqPathGetObtenerObservacionesPorAsegurado {
  public codigoApp: string;
  public numeroPoliza: string;

  public static create(obj): ReqPathGetObtenerObservacionesPorAsegurado {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqPathGetObtenerRedesAtencionAmbuHospiMat {
  public codigoApp: string;
  public numeroPoliza: string;
  public tipoCobertura: string;

  public static create(obj): ReqPathGetObtenerRedesAtencionAmbuHospiMat {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined,
      tipoCobertura: hasValue(obj.tipoCobertura) ? '' + obj.tipoCobertura : undefined // AT_AMBU, AT_HOSP, AT_MATE
    });
  }
}

export class ReqPathGetObtenerContratanteEPS {
  public codigoApp: string;
  public numeroContrato: string;

  public static create(obj): ReqPathGetObtenerContratanteEPS {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroContrato: hasValue(obj.numeroContrato) ? '' + obj.numeroContrato : undefined
    });
  }
}

export class ReqPathGetDescargarFormatoSolicitud {
  public codigoApp: string;
  public codCia: number;
  public codRamo: number;
  public codModalidad: number;

  public static create(obj): ReqPathGetDescargarFormatoSolicitud {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      codCia: hasValue(obj.codCia) ? +obj.codCia : undefined,
      codRamo: hasValue(obj.codRamo) ? +obj.codRamo : undefined,
      codModalidad: hasValue(obj.codModalidad) ? +obj.codModalidad : undefined
    });
  }
}

export class ReqPathGetListarPoliza {
  public codigoApp: string;

  public static create(obj): ReqPathGetListarPoliza {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathPostRegistrarPreSolicitudReembolso {
  public codigoApp: string;

  public static create(obj): ReqPathPostRegistrarPreSolicitudReembolso {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostRegistrarPreSolicitudReembolso {
  public numeroPreSolicitud?: number;
  public numeroRuc?: number;
  public fechaIncidente?: string;
  public fechaTratamiento?: string;
  public codigoBeneficio?: number;
  public montoReembolso?: number;
  public tipoDocumentoBeneficiario?: string;
  public numeroDocumentoBeneficiario?: string;
  public tipoPoliza?: string;
  public numeroPoliza?: string;

  public static create(obj): ReqBodyPostRegistrarPreSolicitudReembolso {
    return removeUndefinedFromObj({
      numeroPreSolicitud: hasValue(obj.numeroPreSolicitud) ? +obj.numeroPreSolicitud : undefined,
      numeroRuc: hasValue(obj.numeroRuc) ? +obj.numeroRuc : undefined,
      fechaIncidente: hasValue(obj.fechaIncidente) ? '' + obj.fechaIncidente : undefined,
      fechaTratamiento: hasValue(obj.fechaTratamiento) ? '' + obj.fechaTratamiento : undefined,
      codigoBeneficio: hasValue(obj.codigoBeneficio) ? +obj.codigoBeneficio : undefined,
      montoReembolso: hasValue(obj.montoReembolso) ? +obj.montoReembolso : undefined,
      tipoDocumentoBeneficiario: hasValue(obj.tipoDocumentoBeneficiario)
        ? '' + obj.tipoDocumentoBeneficiario
        : undefined,
      numeroDocumentoBeneficiario: hasValue(obj.numeroDocumentoBeneficiario)
        ? '' + obj.numeroDocumentoBeneficiario
        : undefined,
      tipoPoliza: hasValue(obj.tipoPoliza) ? '' + obj.tipoPoliza : undefined,
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqPathPostRegistrarArchivosAnexos {
  public codigoApp: string;

  public static create(obj): ReqPathPostRegistrarArchivosAnexos {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqFormDataPostRegistrarArchivosAnexos {
  public numeroPreSolicitud: number;
  public tipoArchivo: string;
  public archivo: any;
  public numeroRuc?: number;
  public numeroArchivo?: number;

  public static create(obj): ReqFormDataPostRegistrarArchivosAnexos {
    return removeUndefinedFromObj({
      numeroPreSolicitud: hasValue(obj.numeroPreSolicitud) ? +obj.numeroPreSolicitud : undefined,
      tipoArchivo: hasValue(obj.tipoArchivo) ? '' + obj.tipoArchivo : undefined, // FORMATO_SOLICITUD, COMPROBANTE_PAGO, OTROS_ADJUNTOS
      archivo: hasValue(obj.archivo) ? obj.archivo : undefined,
      numeroRuc: hasValue(obj.numeroRuc) ? +obj.numeroRuc : undefined,
      numeroArchivo: hasValue(obj.numeroArchivo) ? +obj.numeroArchivo : undefined
    });
  }
}

export class ReqPathPostEliminarArchivosAnexos {
  public codigoApp: string;

  public static create(obj): ReqPathPostEliminarArchivosAnexos {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathPostConfirmarPreSolicitudReembolso {
  public codigoApp: string;

  public static create(obj): ReqPathPostConfirmarPreSolicitudReembolso {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostConfirmarPreSolicitudReembolso {
  public numeroPreSolicitud?: number;

  public static create(obj): ReqBodyPostConfirmarPreSolicitudReembolso {
    return removeUndefinedFromObj({
      numeroPreSolicitud: hasValue(obj.numeroPreSolicitud) ? +obj.numeroPreSolicitud : undefined
    });
  }
}

export class ReqPathGetObtenerSolicitudesReembolso {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerSolicitudesReembolso {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerSolicitudesReembolsoListaUnica {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerSolicitudesReembolsoListaUnica {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerProveedor {
  public codigoApp: string;
  public numeroRuc: number;

  public static create(obj): ReqPathGetObtenerProveedor {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroRuc: hasValue(obj.numeroRuc) ? +obj.numeroRuc : undefined
    });
  }
}

export class ReqPathGetListarBeneficio {
  public codigoApp: string;

  public static create(obj): ReqPathGetListarBeneficio {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetListarBeneficio {
  public codCia: number;
  public codRamo: number;
  public codModalidad: number;

  public static create(obj): ReqQueryGetListarBeneficio {
    return removeUndefinedFromObj({
      codCia: hasValue(obj.codCia) ? +obj.codCia : undefined,
      codRamo: hasValue(obj.codRamo) ? +obj.codRamo : undefined,
      codModalidad: hasValue(obj.codModalidad) ? +obj.codModalidad : undefined
    });
  }
}

export class ReqPathGetObtenerDetalleSolicitudReembolso {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerDetalleSolicitudReembolso {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetObtenerDetalleSolicitudReembolso {
  public numeroSolicitud: number;

  public static create(obj): ReqQueryGetObtenerDetalleSolicitudReembolso {
    return removeUndefinedFromObj({
      numeroSolicitud: hasValue(obj.numeroSolicitud) ? +obj.numeroSolicitud : undefined
    });
  }
}

export class ReqPathGetObtenerCantidadSolicitudesPendientes {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerCantidadSolicitudesPendientes {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerDocumentoReembolso {
  public codigoApp: string;
  public numeroSolicitud: number;
  public idetificadorDocumento: number;

  public static create(obj): ReqPathGetObtenerDocumentoReembolso {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroSolicitud: hasValue(obj.numeroSolicitud) ? +obj.numeroSolicitud : undefined,
      idetificadorDocumento: hasValue(obj.idetificadorDocumento) ? +obj.idetificadorDocumento : undefined
    });
  }
}

export class ReqPathGetObtenerCartasGarantia {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerCartasGarantia {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetObtenerCartasGarantia {
  public criterio?: string;
  public orden?: string;
  public registros?: number;
  public pagina?: number;

  public static create(obj): ReqQueryGetObtenerCartasGarantia {
    return removeUndefinedFromObj({
      criterio: hasValue(obj.criterio) ? '' + obj.criterio : undefined,
      orden: hasValue(obj.orden) ? '' + obj.orden : undefined, // ASC, DESC
      registros: hasValue(obj.registros) ? +obj.registros : undefined,
      pagina: hasValue(obj.pagina) ? +obj.pagina : undefined
    });
  }
}

export class ReqPathGetObtenerClinicas {
  public codigoApp: string;
  public numeroPoliza: string;

  public static create(obj): ReqPathGetObtenerClinicas {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqQueryGetObtenerClinicas {
  public tipoCobertura?: string;
  public tiposEstablecimiento?: string;
  public latitud?: string;
  public longitud?: string;
  public nombre?: string;
  public descripcionUbigeo?: string;
  public departamentoId?: string;
  public provinciaId?: string;
  public distritoId?: string;
  public criterio?: string;
  public orden?: string;
  public registros?: number;
  public pagina?: number;

  public static create(obj): ReqQueryGetObtenerClinicas {
    return removeUndefinedFromObj({
      tipoCobertura: hasValue(obj.tipoCobertura) ? '' + obj.tipoCobertura : undefined,
      tiposEstablecimiento: hasValue(obj.tiposEstablecimiento) ? '' + obj.tiposEstablecimiento : undefined,
      latitud: hasValue(obj.latitud) ? '' + obj.latitud : undefined,
      longitud: hasValue(obj.longitud) ? '' + obj.longitud : undefined,
      nombre: hasValue(obj.nombre) ? '' + obj.nombre : undefined,
      descripcionUbigeo: hasValue(obj.descripcionUbigeo) ? '' + obj.descripcionUbigeo : undefined,
      departamentoId: hasValue(obj.departamentoId) ? '' + obj.departamentoId : undefined,
      provinciaId: hasValue(obj.provinciaId) ? '' + obj.provinciaId : undefined,
      distritoId: hasValue(obj.distritoId) ? '' + obj.distritoId : undefined,
      criterio: hasValue(obj.criterio) ? '' + obj.criterio : undefined,
      orden: hasValue(obj.orden) ? '' + obj.orden : undefined, // ASC, DESC
      registros: hasValue(obj.registros) ? +obj.registros : undefined,
      pagina: hasValue(obj.pagina) ? +obj.pagina : undefined
    });
  }
}

export class ReqPathGetObtenerClinicasNoUsuarioSalud {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerClinicasNoUsuarioSalud {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetObtenerClinicasNoUsuarioSalud {
  public tiposEstablecimiento?: string;
  public latitud?: string;
  public longitud?: string;
  public nombre?: string;
  public descripcionUbigeo?: string;
  public criterio?: string;
  public orden?: string;
  public registros?: number;
  public pagina?: number;

  public static create(obj): ReqQueryGetObtenerClinicasNoUsuarioSalud {
    return removeUndefinedFromObj({
      tiposEstablecimiento: hasValue(obj.tiposEstablecimiento) ? '' + obj.tiposEstablecimiento : undefined,
      latitud: hasValue(obj.latitud) ? '' + obj.latitud : undefined,
      longitud: hasValue(obj.longitud) ? '' + obj.longitud : undefined,
      nombre: hasValue(obj.nombre) ? '' + obj.nombre : undefined,
      descripcionUbigeo: hasValue(obj.descripcionUbigeo) ? '' + obj.descripcionUbigeo : undefined,
      criterio: hasValue(obj.criterio) ? '' + obj.criterio : undefined,
      orden: hasValue(obj.orden) ? '' + obj.orden : undefined, // ASC, DESC
      registros: hasValue(obj.registros) ? +obj.registros : undefined,
      pagina: hasValue(obj.pagina) ? +obj.pagina : undefined
    });
  }
}

export class ReqQueryGetObtenerClinicasGeneral {
  public codigoApp?: string;
  public tipoCobertura?: string;
  public tiposEstablecimiento?: string;
  public latitud?: number;
  public longitud?: number;
  public nombre?: string;
  public descripcionUbigeo?: string;
  public departamentoId?: string;
  public provinciaId?: string;
  public distritoId?: string;
  public criterio?: string;
  public orden?: string;
  public registros?: number;
  public pagina?: number;

  public static create(obj): ReqQueryGetObtenerClinicasGeneral {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      tipoCobertura: hasValue(obj.tipoCobertura) ? '' + obj.tipoCobertura : undefined,
      tiposEstablecimiento: hasValue(obj.tiposEstablecimiento) ? '' + obj.tiposEstablecimiento : undefined,
      latitud: hasValue(obj.latitud) ? +obj.latitud : undefined,
      longitud: hasValue(obj.longitud) ? +obj.longitud : undefined,
      nombre: hasValue(obj.nombre) ? '' + obj.nombre : undefined,
      descripcionUbigeo: hasValue(obj.descripcionUbigeo) ? '' + obj.descripcionUbigeo : undefined,
      departamentoId: hasValue(obj.departamentoId) ? '' + obj.departamentoId : undefined,
      provinciaId: hasValue(obj.provinciaId) ? '' + obj.provinciaId : undefined,
      distritoId: hasValue(obj.distritoId) ? '' + obj.distritoId : undefined,
      criterio: hasValue(obj.criterio) ? '' + obj.criterio : undefined,
      orden: hasValue(obj.orden) ? '' + obj.orden : undefined, // ASC, DESC
      registros: hasValue(obj.registros) ? +obj.registros : undefined,
      pagina: hasValue(obj.pagina) ? +obj.pagina : undefined
    });
  }
}

export class ReqPathPostRegistrarClinicaFavorita {
  public codigoApp: string;
  public clinicaId: string;

  public static create(obj): ReqPathPostRegistrarClinicaFavorita {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      clinicaId: hasValue(obj.clinicaId) ? '' + obj.clinicaId : undefined
    });
  }
}

export class ReqBodyPostRegistrarClinicaFavorita {
  public esFavorito?: any;

  public static create(obj): ReqBodyPostRegistrarClinicaFavorita {
    return removeUndefinedFromObj({
      esFavorito: hasValue(obj.esFavorito) ? obj.esFavorito : undefined
    });
  }
}

export class ReqPathGetCoberturasDetalleClinica {
  public codigoApp: string;
  public clinicaId: string;
  public numeroPoliza: string;

  public static create(obj): ReqPathGetCoberturasDetalleClinica {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      clinicaId: hasValue(obj.clinicaId) ? '' + obj.clinicaId : undefined,
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqPathGetObtenerPolizasPorClienteClinica {
  public codigoApp: string;
  public identificadorClinica: string;

  public static create(obj): ReqPathGetObtenerPolizasPorClienteClinica {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      identificadorClinica: hasValue(obj.identificadorClinica) ? '' + obj.identificadorClinica : undefined
    });
  }
}

export class ReqPathGetObtenerImagenClinica {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerImagenClinica {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetObtenerImagenClinica {
  public codigoImg: string;

  public static create(obj): ReqQueryGetObtenerImagenClinica {
    return removeUndefinedFromObj({
      codigoImg: hasValue(obj.codigoImg) ? '' + obj.codigoImg : undefined
    });
  }
}

export class ReqPathGetObtenerImagenesClinica {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerImagenesClinica {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerClinicasAgendables {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerClinicasAgendables {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetObtenerClinicasAgendables {
  public latitud?: string;
  public longitud?: string;
  public especialidadId?: number;

  public static create(obj): ReqQueryGetObtenerClinicasAgendables {
    return removeUndefinedFromObj({
      latitud: hasValue(obj.latitud) ? '' + obj.latitud : undefined,
      longitud: hasValue(obj.longitud) ? '' + obj.longitud : undefined,
      especialidadId: hasValue(obj.especialidadId) ? +obj.especialidadId : undefined
    });
  }
}

export class ReqPathGetObtenerDetalleCita {
  public codigoApp: string;
  public clinicaId: string;
  public numeroPoliza: string;

  public static create(obj): ReqPathGetObtenerDetalleCita {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      clinicaId: hasValue(obj.clinicaId) ? '' + obj.clinicaId : undefined,
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqQueryGetObtenerDetalleCita {
  public tipoCita?: string;
  public flagEspecialidad?: string;

  public static create(obj): ReqQueryGetObtenerDetalleCita {
    return removeUndefinedFromObj({
      tipoCita: hasValue(obj.tipoCita) ? '' + obj.tipoCita : undefined, // P, V
      flagEspecialidad: hasValue(obj.flagEspecialidad) ? '' + obj.flagEspecialidad : undefined // S, N
    });
  }
}

export class ReqPathGetObtenerFechas {
  public codigoApp: string;
  public clinicaId: string;
  public especialidadId: number;

  public static create(obj): ReqPathGetObtenerFechas {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      clinicaId: hasValue(obj.clinicaId) ? '' + obj.clinicaId : undefined,
      especialidadId: hasValue(obj.especialidadId) ? +obj.especialidadId : undefined
    });
  }
}

export class ReqQueryGetObtenerFechas {
  public tipoCita?: string;

  public static create(obj): ReqQueryGetObtenerFechas {
    return removeUndefinedFromObj({
      tipoCita: hasValue(obj.tipoCita) ? '' + obj.tipoCita : undefined // P, V
    });
  }
}

export class ReqPathGetObtenerHorarios {
  public codigoApp: string;
  public clinicaId: string;
  public especialidadId: number;
  public fecha: string;

  public static create(obj): ReqPathGetObtenerHorarios {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      clinicaId: hasValue(obj.clinicaId) ? '' + obj.clinicaId : undefined,
      especialidadId: hasValue(obj.especialidadId) ? +obj.especialidadId : undefined,
      fecha: hasValue(obj.fecha) ? '' + obj.fecha : undefined
    });
  }
}

export class ReqQueryGetObtenerHorarios {
  public tipoCita?: string;
  public tipoDocPaciente?: string;
  public docPaciente?: string;

  public static create(obj): ReqQueryGetObtenerHorarios {
    return removeUndefinedFromObj({
      tipoCita: hasValue(obj.tipoCita) ? '' + obj.tipoCita : undefined, // P, V
      tipoDocPaciente: hasValue(obj.tipoDocPaciente) ? '' + obj.tipoDocPaciente : undefined,
      docPaciente: hasValue(obj.docPaciente) ? '' + obj.docPaciente : undefined
    });
  }
}

export class ReqPathGetObtenerDoctorPorFechaYHora {
  public codigoApp: string;
  public clinicaId: string;
  public especialidadId: number;
  public fecha: string;
  public horaInicio: string;

  public static create(obj): ReqPathGetObtenerDoctorPorFechaYHora {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      clinicaId: hasValue(obj.clinicaId) ? '' + obj.clinicaId : undefined,
      especialidadId: hasValue(obj.especialidadId) ? +obj.especialidadId : undefined,
      fecha: hasValue(obj.fecha) ? '' + obj.fecha : undefined,
      horaInicio: hasValue(obj.horaInicio) ? '' + obj.horaInicio : undefined
    });
  }
}

export class ReqQueryGetObtenerDoctorPorFechaYHora {
  public tipoCita?: string;

  public static create(obj): ReqQueryGetObtenerDoctorPorFechaYHora {
    return removeUndefinedFromObj({
      tipoCita: hasValue(obj.tipoCita) ? '' + obj.tipoCita : undefined // P, V
    });
  }
}

export class ReqPathGetObtenerDoctores {
  public codigoApp: string;
  public clinicaId: string;
  public especialidadId: number;

  public static create(obj): ReqPathGetObtenerDoctores {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      clinicaId: hasValue(obj.clinicaId) ? '' + obj.clinicaId : undefined,
      especialidadId: hasValue(obj.especialidadId) ? +obj.especialidadId : undefined
    });
  }
}

export class ReqQueryGetObtenerDoctores {
  public tipoCita?: string;

  public static create(obj): ReqQueryGetObtenerDoctores {
    return removeUndefinedFromObj({
      tipoCita: hasValue(obj.tipoCita) ? '' + obj.tipoCita : undefined // P, V
    });
  }
}

export class ReqPathGetObtenerFechasPorDoctor {
  public codigoApp: string;
  public clinicaId: string;
  public especialidadId: number;
  public doctorId: number;

  public static create(obj): ReqPathGetObtenerFechasPorDoctor {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      clinicaId: hasValue(obj.clinicaId) ? '' + obj.clinicaId : undefined,
      especialidadId: hasValue(obj.especialidadId) ? +obj.especialidadId : undefined,
      doctorId: hasValue(obj.doctorId) ? +obj.doctorId : undefined
    });
  }
}

export class ReqQueryGetObtenerFechasPorDoctor {
  public tipoCita?: string;

  public static create(obj): ReqQueryGetObtenerFechasPorDoctor {
    return removeUndefinedFromObj({
      tipoCita: hasValue(obj.tipoCita) ? '' + obj.tipoCita : undefined // P, V
    });
  }
}

export class ReqPathGetObtenerHorariosPorDoctor {
  public codigoApp: string;
  public clinicaId: string;
  public especialidadId: number;
  public doctorId: number;
  public fecha: string;

  public static create(obj): ReqPathGetObtenerHorariosPorDoctor {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      clinicaId: hasValue(obj.clinicaId) ? '' + obj.clinicaId : undefined,
      especialidadId: hasValue(obj.especialidadId) ? +obj.especialidadId : undefined,
      doctorId: hasValue(obj.doctorId) ? +obj.doctorId : undefined,
      fecha: hasValue(obj.fecha) ? '' + obj.fecha : undefined
    });
  }
}

export class ReqQueryGetObtenerHorariosPorDoctor {
  public tipoCita?: string;
  public tipoDocPaciente?: string;
  public docPaciente?: string;

  public static create(obj): ReqQueryGetObtenerHorariosPorDoctor {
    return removeUndefinedFromObj({
      tipoCita: hasValue(obj.tipoCita) ? '' + obj.tipoCita : undefined, // P, V
      tipoDocPaciente: hasValue(obj.tipoDocPaciente) ? '' + obj.tipoDocPaciente : undefined,
      docPaciente: hasValue(obj.docPaciente) ? '' + obj.docPaciente : undefined
    });
  }
}

export class ReqPathGetObtenerPacientesXTitular {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerPacientesXTitular {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined
    });
  }
}

export class ReqPathPostObtenerPacientesXUsuarioLogueado {
  public codigoApp: string;

  public static create(obj): ReqPathPostObtenerPacientesXUsuarioLogueado {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined
    });
  }
}

export class ReqBodyPostObtenerPacientesXUsuarioLogueado {
  public RequestGetPacientesVO?: object[];

  public static create(obj): ReqBodyPostObtenerPacientesXUsuarioLogueado {
    return obj.RequestGetPacientesVO.map(v => v);
  }
}

export class ReqPathPostRegistrarCita {
  public codigoApp: string;

  public static create(obj): ReqPathPostRegistrarCita {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostRegistrarCita {
  public citaId?: number;
  public nombresUsuario?: string;
  public clinicaId?: string;
  public clinicaDescripcion?: string;
  public numeroPoliza?: string;
  public descripcionPoliza?: string;
  public tipoDocumentoPaciente?: string;
  public documentoPaciente?: string;
  public apellidoPaternoPaciente?: string;
  public apellidoMaternoPaciente?: string;
  public nombresPaciente?: string;
  public especialidadId?: number;
  public especialidadDescripcion?: string;
  public codigoTipoDeducible?: string;
  public codigoMonedaCopago?: number;
  public montoCopago?: number;
  public fecha?: string;
  public horaInicio?: string;
  public horaFin?: string;
  public doctorId?: number;
  public nombreCompleto?: string;
  public estadoCita?: string;
  public tipoCita?: string;
  public telefono?: string;

  public static create(obj): ReqBodyPostRegistrarCita {
    return removeUndefinedFromObj({
      citaId: hasValue(obj.citaId) ? +obj.citaId : undefined,
      nombresUsuario: hasValue(obj.nombresUsuario) ? '' + obj.nombresUsuario : undefined,
      clinicaId: hasValue(obj.clinicaId) ? '' + obj.clinicaId : undefined,
      clinicaDescripcion: hasValue(obj.clinicaDescripcion) ? '' + obj.clinicaDescripcion : undefined,
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined,
      descripcionPoliza: hasValue(obj.descripcionPoliza) ? '' + obj.descripcionPoliza : undefined,
      tipoDocumentoPaciente: hasValue(obj.tipoDocumentoPaciente) ? '' + obj.tipoDocumentoPaciente : undefined, // DNI, PEX, CEX, CIP
      documentoPaciente: hasValue(obj.documentoPaciente) ? '' + obj.documentoPaciente : undefined,
      apellidoPaternoPaciente: hasValue(obj.apellidoPaternoPaciente) ? '' + obj.apellidoPaternoPaciente : undefined,
      apellidoMaternoPaciente: hasValue(obj.apellidoMaternoPaciente) ? '' + obj.apellidoMaternoPaciente : undefined,
      nombresPaciente: hasValue(obj.nombresPaciente) ? '' + obj.nombresPaciente : undefined,
      especialidadId: hasValue(obj.especialidadId) ? +obj.especialidadId : undefined,
      especialidadDescripcion: hasValue(obj.especialidadDescripcion) ? '' + obj.especialidadDescripcion : undefined,
      codigoTipoDeducible: hasValue(obj.codigoTipoDeducible) ? '' + obj.codigoTipoDeducible : undefined, // DAGR, CON
      codigoMonedaCopago: hasValue(obj.codigoMonedaCopago) ? +obj.codigoMonedaCopago : undefined, // 1, 2
      montoCopago: hasValue(obj.montoCopago) ? +obj.montoCopago : undefined,
      fecha: hasValue(obj.fecha) ? '' + obj.fecha : undefined,
      horaInicio: hasValue(obj.horaInicio) ? '' + obj.horaInicio : undefined,
      horaFin: hasValue(obj.horaFin) ? '' + obj.horaFin : undefined,
      doctorId: hasValue(obj.doctorId) ? +obj.doctorId : undefined,
      nombreCompleto: hasValue(obj.nombreCompleto) ? '' + obj.nombreCompleto : undefined,
      estadoCita: hasValue(obj.estadoCita) ? '' + obj.estadoCita : undefined, // PENDIENTE, PASADA
      tipoCita: hasValue(obj.tipoCita) ? '' + obj.tipoCita : undefined, // P, V
      telefono: hasValue(obj.telefono) ? '' + obj.telefono : undefined
    });
  }
}

export class ReqPathPutModificarCita {
  public codigoApp: string;
  public citaId: number;

  public static create(obj): ReqPathPutModificarCita {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      citaId: hasValue(obj.citaId) ? +obj.citaId : undefined
    });
  }
}

export class ReqBodyPutModificarCita {
  public citaId?: number;
  public nombresUsuario?: string;
  public clinicaId?: string;
  public clinicaDescripcion?: string;
  public numeroPoliza?: string;
  public descripcionPoliza?: string;
  public tipoDocumentoPaciente?: string;
  public documentoPaciente?: string;
  public apellidoPaternoPaciente?: string;
  public apellidoMaternoPaciente?: string;
  public nombresPaciente?: string;
  public especialidadId?: number;
  public especialidadDescripcion?: string;
  public codigoTipoDeducible?: string;
  public codigoMonedaCopago?: number;
  public montoCopago?: number;
  public fecha?: string;
  public horaInicio?: string;
  public horaFin?: string;
  public doctorId?: number;
  public nombreCompleto?: string;
  public estadoCita?: string;
  public tipoCita?: string;
  public telefono?: string;

  public static create(obj): ReqBodyPutModificarCita {
    return removeUndefinedFromObj({
      citaId: hasValue(obj.citaId) ? +obj.citaId : undefined,
      nombresUsuario: hasValue(obj.nombresUsuario) ? '' + obj.nombresUsuario : undefined,
      clinicaId: hasValue(obj.clinicaId) ? '' + obj.clinicaId : undefined,
      clinicaDescripcion: hasValue(obj.clinicaDescripcion) ? '' + obj.clinicaDescripcion : undefined,
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined,
      descripcionPoliza: hasValue(obj.descripcionPoliza) ? '' + obj.descripcionPoliza : undefined,
      tipoDocumentoPaciente: hasValue(obj.tipoDocumentoPaciente) ? '' + obj.tipoDocumentoPaciente : undefined, // DNI, PEX, CEX, CIP
      documentoPaciente: hasValue(obj.documentoPaciente) ? '' + obj.documentoPaciente : undefined,
      apellidoPaternoPaciente: hasValue(obj.apellidoPaternoPaciente) ? '' + obj.apellidoPaternoPaciente : undefined,
      apellidoMaternoPaciente: hasValue(obj.apellidoMaternoPaciente) ? '' + obj.apellidoMaternoPaciente : undefined,
      nombresPaciente: hasValue(obj.nombresPaciente) ? '' + obj.nombresPaciente : undefined,
      especialidadId: hasValue(obj.especialidadId) ? +obj.especialidadId : undefined,
      especialidadDescripcion: hasValue(obj.especialidadDescripcion) ? '' + obj.especialidadDescripcion : undefined,
      codigoTipoDeducible: hasValue(obj.codigoTipoDeducible) ? '' + obj.codigoTipoDeducible : undefined, // DAGR, CON
      codigoMonedaCopago: hasValue(obj.codigoMonedaCopago) ? +obj.codigoMonedaCopago : undefined, // 1, 2
      montoCopago: hasValue(obj.montoCopago) ? +obj.montoCopago : undefined,
      fecha: hasValue(obj.fecha) ? '' + obj.fecha : undefined,
      horaInicio: hasValue(obj.horaInicio) ? '' + obj.horaInicio : undefined,
      horaFin: hasValue(obj.horaFin) ? '' + obj.horaFin : undefined,
      doctorId: hasValue(obj.doctorId) ? +obj.doctorId : undefined,
      nombreCompleto: hasValue(obj.nombreCompleto) ? '' + obj.nombreCompleto : undefined,
      estadoCita: hasValue(obj.estadoCita) ? '' + obj.estadoCita : undefined, // PENDIENTE, PASADA
      tipoCita: hasValue(obj.tipoCita) ? '' + obj.tipoCita : undefined, // P, V
      telefono: hasValue(obj.telefono) ? '' + obj.telefono : undefined
    });
  }
}

export class ReqPathDeleteAnularCita {
  public codigoApp: string;
  public citaId: number;

  public static create(obj): ReqPathDeleteAnularCita {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      citaId: hasValue(obj.citaId) ? +obj.citaId : undefined
    });
  }
}

export class ReqPathGetObtenerCitas {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerCitas {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetObtenerCitas {
  public estado?: string;

  public static create(obj): ReqQueryGetObtenerCitas {
    return removeUndefinedFromObj({
      estado: hasValue(obj.estado) ? '' + obj.estado : undefined // PENDIENTE, PASADA
    });
  }
}

export class ReqPathGetObtenerCitasAsegurado {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerCitasAsegurado {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetObtenerCitasAsegurado {
  public estado?: string;
  public latitud?: number;
  public longitud?: number;

  public static create(obj): ReqQueryGetObtenerCitasAsegurado {
    return removeUndefinedFromObj({
      estado: hasValue(obj.estado) ? '' + obj.estado : undefined, // PENDIENTE, PASADA
      latitud: hasValue(obj.latitud) ? +obj.latitud : undefined,
      longitud: hasValue(obj.longitud) ? +obj.longitud : undefined
    });
  }
}

export class ReqPathGetObtenerCita {
  public citaId: string;

  public static create(obj): ReqPathGetObtenerCita {
    return removeUndefinedFromObj({
      citaId: hasValue(obj.citaId) ? '' + obj.citaId : undefined
    });
  }
}

export class ReqQueryGetObtenerCita {
  public codigoApp: string;

  public static create(obj): ReqQueryGetObtenerCita {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetListarPolizasSCTR {
  public codigoApp: string;

  public static create(obj): ReqPathGetListarPolizasSCTR {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetListarPolizasSCTR {
  public numeroPoliza?: string;
  public estadoPoliza?: string;
  public criterio?: string;
  public orden?: string;
  public registros?: number;
  public pagina?: number;

  public static create(obj): ReqQueryGetListarPolizasSCTR {
    return removeUndefinedFromObj({
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined,
      estadoPoliza: hasValue(obj.estadoPoliza) ? '' + obj.estadoPoliza : undefined, // V, E, A, C
      criterio: hasValue(obj.criterio) ? '' + obj.criterio : undefined,
      orden: hasValue(obj.orden) ? '' + obj.orden : undefined, // ASC, DESC
      registros: hasValue(obj.registros) ? +obj.registros : undefined,
      pagina: hasValue(obj.pagina) ? +obj.pagina : undefined
    });
  }
}

export class ReqPathGetObtenerConstanciasSCTR {
  public codigoApp: string;
  public numeroPoliza: string;

  public static create(obj): ReqPathGetObtenerConstanciasSCTR {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqQueryGetObtenerConstanciasSCTR {
  public numeroConstancia?: string;
  public fechaInicioVigencia?: string;
  public fechaFinVigencia?: string;
  public tipoMovimiento?: string;
  public registros?: number;
  public pagina?: number;

  public static create(obj): ReqQueryGetObtenerConstanciasSCTR {
    return removeUndefinedFromObj({
      numeroConstancia: hasValue(obj.numeroConstancia) ? '' + obj.numeroConstancia : undefined,
      fechaInicioVigencia: hasValue(obj.fechaInicioVigencia) ? '' + obj.fechaInicioVigencia : undefined,
      fechaFinVigencia: hasValue(obj.fechaFinVigencia) ? '' + obj.fechaFinVigencia : undefined,
      tipoMovimiento: hasValue(obj.tipoMovimiento) ? '' + obj.tipoMovimiento : undefined,
      registros: hasValue(obj.registros) ? +obj.registros : undefined,
      pagina: hasValue(obj.pagina) ? +obj.pagina : undefined
    });
  }
}

export class ReqPathGetObtenerAseguradosConstanciaSCTR {
  public codigoApp: string;
  public numeroConstancia: string;

  public static create(obj): ReqPathGetObtenerAseguradosConstanciaSCTR {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroConstancia: hasValue(obj.numeroConstancia) ? '' + obj.numeroConstancia : undefined
    });
  }
}

export class ReqQueryGetObtenerAseguradosConstanciaSCTR {
  public nombreODocumento?: string;
  public registros?: number;
  public pagina?: number;

  public static create(obj): ReqQueryGetObtenerAseguradosConstanciaSCTR {
    return removeUndefinedFromObj({
      nombreODocumento: hasValue(obj.nombreODocumento) ? '' + obj.nombreODocumento : undefined,
      registros: hasValue(obj.registros) ? +obj.registros : undefined,
      pagina: hasValue(obj.pagina) ? +obj.pagina : undefined
    });
  }
}

export class ReqPathGetDescargarConstanciaSCTR {
  public codigoApp: string;
  public numeroPoliza: string;
  public numeroConstancia: string;

  public static create(obj): ReqPathGetDescargarConstanciaSCTR {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined,
      numeroConstancia: hasValue(obj.numeroConstancia) ? '' + obj.numeroConstancia : undefined
    });
  }
}

export class ReqPathGetListarPeridoSCTR {
  public numeroPoliza: string;

  public static create(obj): ReqPathGetListarPeridoSCTR {
    return removeUndefinedFromObj({
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqQueryGetListarPeridoSCTR {
  public codigoApp: string;
  public tipoDocumento: string;
  public documento: string;
  public registros?: number;
  public pagina?: number;
  public fechaFinVigencia?: string;
  public fechaInicioVigencia?: string;

  public static create(obj): ReqQueryGetListarPeridoSCTR {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      tipoDocumento: hasValue(obj.tipoDocumento) ? '' + obj.tipoDocumento : undefined, // DNI, RUC, PEX, CEX, CIP
      documento: hasValue(obj.documento) ? '' + obj.documento : undefined,
      registros: hasValue(obj.registros) ? +obj.registros : undefined,
      pagina: hasValue(obj.pagina) ? +obj.pagina : undefined,
      fechaFinVigencia: hasValue(obj.fechaFinVigencia) ? '' + obj.fechaFinVigencia : undefined,
      fechaInicioVigencia: hasValue(obj.fechaInicioVigencia) ? '' + obj.fechaInicioVigencia : undefined
    });
  }
}

export class ReqPathGetListarPeridoAdicionalesSCTR {
  public numeroPoliza: string;

  public static create(obj): ReqPathGetListarPeridoAdicionalesSCTR {
    return removeUndefinedFromObj({
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqQueryGetListarPeridoAdicionalesSCTR {
  public codigoApp: string;
  public tipoDocumento: string;
  public documento: string;
  public registros?: number;
  public pagina?: number;
  public fechaFinVigencia?: string;
  public fechaInicioVigencia?: string;

  public static create(obj): ReqQueryGetListarPeridoAdicionalesSCTR {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      tipoDocumento: hasValue(obj.tipoDocumento) ? '' + obj.tipoDocumento : undefined, // DNI, RUC, PEX, CEX, CIP
      documento: hasValue(obj.documento) ? '' + obj.documento : undefined,
      registros: hasValue(obj.registros) ? +obj.registros : undefined,
      pagina: hasValue(obj.pagina) ? +obj.pagina : undefined,
      fechaFinVigencia: hasValue(obj.fechaFinVigencia) ? '' + obj.fechaFinVigencia : undefined,
      fechaInicioVigencia: hasValue(obj.fechaInicioVigencia) ? '' + obj.fechaInicioVigencia : undefined
    });
  }
}

export class ReqPathGetFiltrosConstancias {
  public numeroPoliza: string;

  public static create(obj): ReqPathGetFiltrosConstancias {
    return removeUndefinedFromObj({
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqQueryGetFiltrosConstancias {
  public codigoApp: string;
  public tipoDocumento: string;
  public documento: string;

  public static create(obj): ReqQueryGetFiltrosConstancias {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      tipoDocumento: hasValue(obj.tipoDocumento) ? '' + obj.tipoDocumento : undefined, // DNI, RUC, PEX, CEX, CIP
      documento: hasValue(obj.documento) ? '' + obj.documento : undefined
    });
  }
}

export class ReqPathGetListarRiesgoSCTR {
  public numeroPoliza: string;

  public static create(obj): ReqPathGetListarRiesgoSCTR {
    return removeUndefinedFromObj({
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqQueryGetListarRiesgoSCTR {
  public codigoApp: string;
  public tipoDocumento: string;
  public documento: string;

  public static create(obj): ReqQueryGetListarRiesgoSCTR {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      tipoDocumento: hasValue(obj.tipoDocumento) ? '' + obj.tipoDocumento : undefined, // DNI, RUC, PEX, CEX, CIP
      documento: hasValue(obj.documento) ? '' + obj.documento : undefined
    });
  }
}

export class ReqQueryPostSubirValidarPlanillaSCTR {
  public tipoDocumento: string;
  public documento: string;
  public codigoApp: string;
  public tipoMovimiento: string;

  public static create(obj): ReqQueryPostSubirValidarPlanillaSCTR {
    return removeUndefinedFromObj({
      tipoDocumento: hasValue(obj.tipoDocumento) ? '' + obj.tipoDocumento : undefined, // DNI, RUC, PEX, CEX, CIP
      documento: hasValue(obj.documento) ? '' + obj.documento : undefined,
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      tipoMovimiento: hasValue(obj.tipoMovimiento) ? '' + obj.tipoMovimiento : undefined // IN, DE
    });
  }
}

export class ReqFormDataPostSubirValidarPlanillaSCTR {
  public declaracionJson: string;
  public planilla: any;

  public static create(obj): ReqFormDataPostSubirValidarPlanillaSCTR {
    return removeUndefinedFromObj({
      declaracionJson: hasValue(obj.declaracionJson) ? '' + obj.declaracionJson : undefined,
      planilla: hasValue(obj.planilla) ? obj.planilla : undefined
    });
  }
}

export class ReqQueryGetDescargarValidacionPlanillaSCTR {
  public numMovimiento: number;
  public codigoRespuesta: string;

  public static create(obj): ReqQueryGetDescargarValidacionPlanillaSCTR {
    return removeUndefinedFromObj({
      numMovimiento: hasValue(obj.numMovimiento) ? +obj.numMovimiento : undefined,
      codigoRespuesta: hasValue(obj.codigoRespuesta) ? '' + obj.codigoRespuesta : undefined // ERROR, OBSERVADO
    });
  }
}

export class ReqQueryGetListarDeclaracionResumenSCTR {
  public numMovimiento: number;

  public static create(obj): ReqQueryGetListarDeclaracionResumenSCTR {
    return removeUndefinedFromObj({
      numMovimiento: hasValue(obj.numMovimiento) ? +obj.numMovimiento : undefined
    });
  }
}

export class ReqQueryGetListarInclusionResumenSCTR {
  public numMovimiento: number;

  public static create(obj): ReqQueryGetListarInclusionResumenSCTR {
    return removeUndefinedFromObj({
      numMovimiento: hasValue(obj.numMovimiento) ? +obj.numMovimiento : undefined
    });
  }
}

export class ReqQueryPostGenerarDeclaracionSCTR {
  public numMovimiento: number;
  public tipoDocumento: string;
  public documento: string;

  public static create(obj): ReqQueryPostGenerarDeclaracionSCTR {
    return removeUndefinedFromObj({
      numMovimiento: hasValue(obj.numMovimiento) ? +obj.numMovimiento : undefined,
      tipoDocumento: hasValue(obj.tipoDocumento) ? '' + obj.tipoDocumento : undefined, // DNI, RUC, PEX, CEX, CIP
      documento: hasValue(obj.documento) ? '' + obj.documento : undefined
    });
  }
}

export class ReqBodyPostGenerarDeclaracionSCTR {
  public informacionOperacion?: object;

  public static create(obj): ReqBodyPostGenerarDeclaracionSCTR {
    return removeUndefinedFromObj({
      informacionOperacion: hasValue(obj.informacionOperacion) ? obj.informacionOperacion : undefined
    });
  }
}

export class ReqQueryGetInclusionFacturacionSCTR {
  public numMovimiento: number;

  public static create(obj): ReqQueryGetInclusionFacturacionSCTR {
    return removeUndefinedFromObj({
      numMovimiento: hasValue(obj.numMovimiento) ? +obj.numMovimiento : undefined
    });
  }
}

export class ReqQueryPostGenerarInclusionSCTR {
  public numMovimiento: number;
  public tipoDocumento: string;
  public documento: string;

  public static create(obj): ReqQueryPostGenerarInclusionSCTR {
    return removeUndefinedFromObj({
      numMovimiento: hasValue(obj.numMovimiento) ? +obj.numMovimiento : undefined,
      tipoDocumento: hasValue(obj.tipoDocumento) ? '' + obj.tipoDocumento : undefined, // DNI, RUC, PEX, CEX, CIP
      documento: hasValue(obj.documento) ? '' + obj.documento : undefined
    });
  }
}

export class ReqBodyPostGenerarInclusionSCTR {
  public informacionOperacion?: object;
  public incluirFacturacion?: any;

  public static create(obj): ReqBodyPostGenerarInclusionSCTR {
    return removeUndefinedFromObj({
      informacionOperacion: hasValue(obj.informacionOperacion) ? obj.informacionOperacion : undefined,
      incluirFacturacion: hasValue(obj.incluirFacturacion) ? obj.incluirFacturacion : undefined
    });
  }
}

export class ReqPathGetListarPeriodosFiltros {
  public numeroPoliza: string;

  public static create(obj): ReqPathGetListarPeriodosFiltros {
    return removeUndefinedFromObj({
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqQueryGetListarPeriodosFiltros {
  public codigoApp: string;
  public tipoDocumento: string;
  public documento: string;
  public registros?: number;
  public pagina?: number;
  public fechaFinVigencia?: string;
  public fechaInicioVigencia?: string;

  public static create(obj): ReqQueryGetListarPeriodosFiltros {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      tipoDocumento: hasValue(obj.tipoDocumento) ? '' + obj.tipoDocumento : undefined, // DNI, RUC, PEX, CEX, CIP
      documento: hasValue(obj.documento) ? '' + obj.documento : undefined,
      registros: hasValue(obj.registros) ? +obj.registros : undefined,
      pagina: hasValue(obj.pagina) ? +obj.pagina : undefined,
      fechaFinVigencia: hasValue(obj.fechaFinVigencia) ? '' + obj.fechaFinVigencia : undefined,
      fechaInicioVigencia: hasValue(obj.fechaInicioVigencia) ? '' + obj.fechaInicioVigencia : undefined
    });
  }
}

export class ReqPathPostListarRecibosPorPeriodo {
  public numeroPoliza: string;

  public static create(obj): ReqPathPostListarRecibosPorPeriodo {
    return removeUndefinedFromObj({
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqQueryPostListarRecibosPorPeriodo {
  public codigoApp: string;
  public tipoDocumento: string;
  public documento: string;
  public registros?: number;
  public pagina?: number;
  public fechaFinVigencia?: string;
  public fechaInicioVigencia?: string;
  public tipoSituacion?: string;

  public static create(obj): ReqQueryPostListarRecibosPorPeriodo {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      tipoDocumento: hasValue(obj.tipoDocumento) ? '' + obj.tipoDocumento : undefined, // DNI, RUC, PEX, CEX, CIP
      documento: hasValue(obj.documento) ? '' + obj.documento : undefined,
      registros: hasValue(obj.registros) ? +obj.registros : undefined,
      pagina: hasValue(obj.pagina) ? +obj.pagina : undefined,
      fechaFinVigencia: hasValue(obj.fechaFinVigencia) ? '' + obj.fechaFinVigencia : undefined,
      fechaInicioVigencia: hasValue(obj.fechaInicioVigencia) ? '' + obj.fechaInicioVigencia : undefined,
      tipoSituacion: hasValue(obj.tipoSituacion) ? '' + obj.tipoSituacion : undefined // PAGADO, PENDIENTE
    });
  }
}

export class ReqBodyPostListarRecibosPorPeriodo {
  public polizas?: any[];

  public static create(obj): ReqBodyPostListarRecibosPorPeriodo {
    return removeUndefinedFromObj({
      polizas: hasValue(obj.polizas) ? obj.polizas : undefined
    });
  }
}

export class ReqPathPutActualizarAseguradoSctr {
  public numeroPoliza: string;
  public codAsegurado: string;

  public static create(obj): ReqPathPutActualizarAseguradoSctr {
    return removeUndefinedFromObj({
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined,
      codAsegurado: hasValue(obj.codAsegurado) ? '' + obj.codAsegurado : undefined
    });
  }
}

export class ReqQueryPutActualizarAseguradoSctr {
  public codigoApp: string;
  public tipoDocumento: string;
  public documento: string;
  public fechaInicioVigencia?: string;
  public fechaFinVigencia?: string;

  public static create(obj): ReqQueryPutActualizarAseguradoSctr {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      tipoDocumento: hasValue(obj.tipoDocumento) ? '' + obj.tipoDocumento : undefined, // DNI, RUC, PEX, CEX, CIP
      documento: hasValue(obj.documento) ? '' + obj.documento : undefined,
      fechaInicioVigencia: hasValue(obj.fechaInicioVigencia) ? '' + obj.fechaInicioVigencia : undefined,
      fechaFinVigencia: hasValue(obj.fechaFinVigencia) ? '' + obj.fechaFinVigencia : undefined
    });
  }
}

export class ReqBodyPutActualizarAseguradoSctr {
  public asegurado?: object;
  public polizas?: any[];

  public static create(obj): ReqBodyPutActualizarAseguradoSctr {
    return removeUndefinedFromObj({
      asegurado: hasValue(obj.asegurado) ? obj.asegurado : undefined,
      polizas: hasValue(obj.polizas) ? obj.polizas : undefined
    });
  }
}

export class ReqPathGetObtenerPlanillaAsegurados {
  public numeroPoliza: string;

  public static create(obj): ReqPathGetObtenerPlanillaAsegurados {
    return removeUndefinedFromObj({
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqQueryGetObtenerPlanillaAsegurados {
  public codigoApp: string;
  public tipoDocumento: string;
  public documento: string;
  public registros?: number;
  public pagina?: number;
  public fechaFinVigencia?: string;
  public fechaInicioVigencia?: string;
  public tipoSituacion?: string;
  public codRamo?: string;
  public textoBuscar?: string;

  public static create(obj): ReqQueryGetObtenerPlanillaAsegurados {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      tipoDocumento: hasValue(obj.tipoDocumento) ? '' + obj.tipoDocumento : undefined, // DNI, RUC, PEX, CEX, CIP
      documento: hasValue(obj.documento) ? '' + obj.documento : undefined,
      registros: hasValue(obj.registros) ? +obj.registros : undefined,
      pagina: hasValue(obj.pagina) ? +obj.pagina : undefined,
      fechaFinVigencia: hasValue(obj.fechaFinVigencia) ? '' + obj.fechaFinVigencia : undefined,
      fechaInicioVigencia: hasValue(obj.fechaInicioVigencia) ? '' + obj.fechaInicioVigencia : undefined,
      tipoSituacion: hasValue(obj.tipoSituacion) ? '' + obj.tipoSituacion : undefined, // PAGADO, PENDIENTE
      codRamo: hasValue(obj.codRamo) ? '' + obj.codRamo : undefined,
      textoBuscar: hasValue(obj.textoBuscar) ? '' + obj.textoBuscar : undefined
    });
  }
}

export class ReqPathPostDescargarConstanciaManualSCTR {
  public numeroPoliza: string;

  public static create(obj): ReqPathPostDescargarConstanciaManualSCTR {
    return removeUndefinedFromObj({
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqQueryPostDescargarConstanciaManualSCTR {
  public codigoApp: string;
  public fechaFinVigencia?: string;
  public fechaInicioVigencia?: string;

  public static create(obj): ReqQueryPostDescargarConstanciaManualSCTR {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      fechaFinVigencia: hasValue(obj.fechaFinVigencia) ? '' + obj.fechaFinVigencia : undefined,
      fechaInicioVigencia: hasValue(obj.fechaInicioVigencia) ? '' + obj.fechaInicioVigencia : undefined
    });
  }
}

export class ReqBodyPostDescargarConstanciaManualSCTR {
  public seleccionadoTodos?: any;
  public listaSeleccionada?: any[];
  public informacionOperacion?: object;
  public polizas?: any[];

  public static create(obj): ReqBodyPostDescargarConstanciaManualSCTR {
    return removeUndefinedFromObj({
      seleccionadoTodos: hasValue(obj.seleccionadoTodos) ? obj.seleccionadoTodos : undefined,
      listaSeleccionada: hasValue(obj.listaSeleccionada) ? obj.listaSeleccionada : undefined,
      informacionOperacion: hasValue(obj.informacionOperacion) ? obj.informacionOperacion : undefined,
      polizas: hasValue(obj.polizas) ? obj.polizas : undefined
    });
  }
}

export class ReqPathGetObtenerCoordenadasZonasChofer {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerCoordenadasZonasChofer {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerCoordenadasZonasMedico {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerCoordenadasZonasMedico {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerServicios {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerServicios {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathPostSolicitarChoferReemplazo {
  public codigoApp: string;

  public static create(obj): ReqPathPostSolicitarChoferReemplazo {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostSolicitarChoferReemplazo {
  public fechaRecojo?: string;
  public horaRecojo?: string;
  public origen?: object;
  public destino?: object;
  public producto?: object;
  public pago?: object;
  public telefono?: string;

  public static create(obj): ReqBodyPostSolicitarChoferReemplazo {
    return removeUndefinedFromObj({
      fechaRecojo: hasValue(obj.fechaRecojo) ? '' + obj.fechaRecojo : undefined,
      horaRecojo: hasValue(obj.horaRecojo) ? '' + obj.horaRecojo : undefined,
      origen: hasValue(obj.origen) ? obj.origen : undefined,
      destino: hasValue(obj.destino) ? obj.destino : undefined,
      producto: hasValue(obj.producto) ? obj.producto : undefined,
      pago: hasValue(obj.pago) ? obj.pago : undefined,
      telefono: hasValue(obj.telefono) ? '' + obj.telefono : undefined
    });
  }
}

export class ReqPathGetObtenerSolicitudes {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerSolicitudes {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetObtenerSolicitudes {
  public codigoTipoSolicitud?: string;
  public estado?: string;

  public static create(obj): ReqQueryGetObtenerSolicitudes {
    return removeUndefinedFromObj({
      codigoTipoSolicitud: hasValue(obj.codigoTipoSolicitud) ? '' + obj.codigoTipoSolicitud : undefined, // CHOFER_REEMPLAZO, MEDICO_DOMICILIO
      estado: hasValue(obj.estado) ? '' + obj.estado : undefined // PENDIENTE, PASADO
    });
  }
}

export class ReqQueryGetObtenerSolicitudesPost {
  public codigoApp: string;
  public codigoTipoSolicitud?: string;
  public estado?: string;

  public static create(obj): ReqQueryGetObtenerSolicitudesPost {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      codigoTipoSolicitud: hasValue(obj.codigoTipoSolicitud) ? '' + obj.codigoTipoSolicitud : undefined, // CHOFER_REEMPLAZO, MEDICO_DOMICILIO, CITA_MEDICA
      estado: hasValue(obj.estado) ? '' + obj.estado : undefined // PENDIENTE, PASADO
    });
  }
}

export class ReqQueryGetObtenerSolicitudesFiltro {
  public codigoApp: string;
  public codigoTipoEstado?: string;

  public static create(obj): ReqQueryGetObtenerSolicitudesFiltro {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      codigoTipoEstado: hasValue(obj.codigoTipoEstado) ? '' + obj.codigoTipoEstado : undefined // C, H
    });
  }
}

export class ReqPathGetObtenerTipoVehiculos {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerTipoVehiculos {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerPolizasMedicoDomicilio {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerPolizasMedicoDomicilio {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathPostSolicitarMedicoDomicilio {
  public codigoApp: string;

  public static create(obj): ReqPathPostSolicitarMedicoDomicilio {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostSolicitarMedicoDomicilio {
  public fechaRecojo?: string;
  public horaRecojo?: string;
  public esUrgente?: any;
  public destino?: object;
  public producto?: object;
  public pago?: object;
  public telefono?: string;
  public sintomas?: string;
  public beneficiario?: number;
  public especialidad?: number;

  public static create(obj): ReqBodyPostSolicitarMedicoDomicilio {
    return removeUndefinedFromObj({
      fechaRecojo: hasValue(obj.fechaRecojo) ? '' + obj.fechaRecojo : undefined,
      horaRecojo: hasValue(obj.horaRecojo) ? '' + obj.horaRecojo : undefined,
      esUrgente: hasValue(obj.esUrgente) ? obj.esUrgente : undefined,
      destino: hasValue(obj.destino) ? obj.destino : undefined,
      producto: hasValue(obj.producto) ? obj.producto : undefined,
      pago: hasValue(obj.pago) ? obj.pago : undefined,
      telefono: hasValue(obj.telefono) ? '' + obj.telefono : undefined,
      sintomas: hasValue(obj.sintomas) ? '' + obj.sintomas : undefined,
      beneficiario: hasValue(obj.beneficiario) ? +obj.beneficiario : undefined,
      especialidad: hasValue(obj.especialidad) ? +obj.especialidad : undefined
    });
  }
}

export class ReqPathGetObtenerVehiculoSoat {
  public placa: string;

  public static create(obj): ReqPathGetObtenerVehiculoSoat {
    return removeUndefinedFromObj({
      placa: hasValue(obj.placa) ? '' + obj.placa : undefined
    });
  }
}

export class ReqQueryGetObtenerVehiculoSoat {
  public codigoApp: string;

  public static create(obj): ReqQueryGetObtenerVehiculoSoat {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerVehiculoSoatPorPoliza {
  public codigoApp: string;
  public numeroPoliza: string;

  public static create(obj): ReqPathGetObtenerVehiculoSoatPorPoliza {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqPathGetObtenerAsientos {
  public codigoApp: string;
  public tipoVehiculoId: string;
  public marcaId: string;
  public modeloId: string;

  public static create(obj): ReqPathGetObtenerAsientos {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      tipoVehiculoId: hasValue(obj.tipoVehiculoId) ? '' + obj.tipoVehiculoId : undefined,
      marcaId: hasValue(obj.marcaId) ? '' + obj.marcaId : undefined,
      modeloId: hasValue(obj.modeloId) ? '' + obj.modeloId : undefined
    });
  }
}

export class ReqPathGetObtenerMarcas {
  public tipoVehiculoId: number;

  public static create(obj): ReqPathGetObtenerMarcas {
    return removeUndefinedFromObj({
      tipoVehiculoId: hasValue(obj.tipoVehiculoId) ? +obj.tipoVehiculoId : undefined
    });
  }
}

export class ReqPathGetObtenerModelos {
  public tipoVehiculoId: number;
  public marcaId: number;

  public static create(obj): ReqPathGetObtenerModelos {
    return removeUndefinedFromObj({
      tipoVehiculoId: hasValue(obj.tipoVehiculoId) ? +obj.tipoVehiculoId : undefined,
      marcaId: hasValue(obj.marcaId) ? +obj.marcaId : undefined
    });
  }
}

export class ReqPathGetObtenerContratanteSOAT {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerContratanteSOAT {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerTiposUso {
  public codigoApp: string;
  public tipoVehiculoId: number;
  public departamentoId: number;

  public static create(obj): ReqPathGetObtenerTiposUso {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      tipoVehiculoId: hasValue(obj.tipoVehiculoId) ? +obj.tipoVehiculoId : undefined,
      departamentoId: hasValue(obj.departamentoId) ? +obj.departamentoId : undefined
    });
  }
}

export class ReqPathPostGenerarCotizacionSOAT {
  public codigoApp: string;

  public static create(obj): ReqPathPostGenerarCotizacionSOAT {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostGenerarCotizacionSOAT {
  public vehiculo?: object;
  public contratante?: object;
  public montoMapfreDolaresDisponibles?: number;
  public fechaInicioVigencia?: string;
  public codigoModalidad?: string;

  public static create(obj): ReqBodyPostGenerarCotizacionSOAT {
    return removeUndefinedFromObj({
      vehiculo: hasValue(obj.vehiculo) ? obj.vehiculo : undefined,
      contratante: hasValue(obj.contratante) ? obj.contratante : undefined,
      montoMapfreDolaresDisponibles: hasValue(obj.montoMapfreDolaresDisponibles)
        ? +obj.montoMapfreDolaresDisponibles
        : undefined,
      fechaInicioVigencia: hasValue(obj.fechaInicioVigencia) ? '' + obj.fechaInicioVigencia : undefined,
      codigoModalidad: hasValue(obj.codigoModalidad) ? '' + obj.codigoModalidad : undefined // 30205, 30212
    });
  }
}

export class ReqPathPostGenerarEmisionSOAT {
  public codigoApp: string;

  public static create(obj): ReqPathPostGenerarEmisionSOAT {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostGenerarEmisionSOAT {
  public vehiculo?: object;
  public contratante?: object;
  public cotizacion?: object;
  public vigencia?: object;
  public pago?: object;
  public tarjeta?: object;

  public static create(obj): ReqBodyPostGenerarEmisionSOAT {
    return removeUndefinedFromObj({
      vehiculo: hasValue(obj.vehiculo) ? obj.vehiculo : undefined,
      contratante: hasValue(obj.contratante) ? obj.contratante : undefined,
      cotizacion: hasValue(obj.cotizacion) ? obj.cotizacion : undefined,
      vigencia: hasValue(obj.vigencia) ? obj.vigencia : undefined,
      pago: hasValue(obj.pago) ? obj.pago : undefined,
      tarjeta: hasValue(obj.tarjeta) ? obj.tarjeta : undefined
    });
  }
}

export class ReqPathPostGenerarEmisionSOATLyra {
  public codigoApp: string;

  public static create(obj): ReqPathPostGenerarEmisionSOATLyra {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostGenerarEmisionSOATLyra {
  public vehiculo?: object;
  public contratante?: object;
  public cotizacion?: object;
  public vigencia?: object;
  public pago?: object;
  public tarjeta?: object;
  public transaccionPayment?: object;

  public static create(obj): ReqBodyPostGenerarEmisionSOATLyra {
    return removeUndefinedFromObj({
      vehiculo: hasValue(obj.vehiculo) ? obj.vehiculo : undefined,
      contratante: hasValue(obj.contratante) ? obj.contratante : undefined,
      cotizacion: hasValue(obj.cotizacion) ? obj.cotizacion : undefined,
      vigencia: hasValue(obj.vigencia) ? obj.vigencia : undefined,
      pago: hasValue(obj.pago) ? obj.pago : undefined,
      tarjeta: hasValue(obj.tarjeta) ? obj.tarjeta : undefined,
      transaccionPayment: hasValue(obj.transaccionPayment) ? obj.transaccionPayment : undefined
    });
  }
}

export class ReqPathPostVerificarSerialVin {
  public codigoApp: string;

  public static create(obj): ReqPathPostVerificarSerialVin {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostVerificarSerialVin {
  public codCia?: number;
  public codRamo?: number;
  public vehiculo?: object;
  public vigencia?: object;

  public static create(obj): ReqBodyPostVerificarSerialVin {
    return removeUndefinedFromObj({
      codCia: hasValue(obj.codCia) ? +obj.codCia : undefined,
      codRamo: hasValue(obj.codRamo) ? +obj.codRamo : undefined,
      vehiculo: hasValue(obj.vehiculo) ? obj.vehiculo : undefined,
      vigencia: hasValue(obj.vigencia) ? obj.vigencia : undefined
    });
  }
}

export class ReqPathGetObtenerDistritosDelivery {
  public codigoApp: string;
  public codigoDepartamento: string;
  public codigoProvincia: string;

  public static create(obj): ReqPathGetObtenerDistritosDelivery {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      codigoDepartamento: hasValue(obj.codigoDepartamento) ? '' + obj.codigoDepartamento : undefined, // 15
      codigoProvincia: hasValue(obj.codigoProvincia) ? '' + obj.codigoProvincia : undefined // 128, 67
    });
  }
}

export class ReqPathGetObtenerUrbanizacionesDelivery {
  public codigoApp: string;
  public codigoDepartamento: string;
  public codigoProvincia: string;
  public codigoDistrito: string;

  public static create(obj): ReqPathGetObtenerUrbanizacionesDelivery {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      codigoDepartamento: hasValue(obj.codigoDepartamento) ? '' + obj.codigoDepartamento : undefined, // 15
      codigoProvincia: hasValue(obj.codigoProvincia) ? '' + obj.codigoProvincia : undefined, // 128, 67
      codigoDistrito: hasValue(obj.codigoDistrito) ? '' + obj.codigoDistrito : undefined
    });
  }
}

export class ReqPathPostEnviarCorreoSOAT {
  public codigoApp: string;

  public static create(obj): ReqPathPostEnviarCorreoSOAT {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostEnviarCorreoSOAT {
  public numeroCotizacion?: string;
  public numeroPoliza?: string;
  public correos?: any[];

  public static create(obj): ReqBodyPostEnviarCorreoSOAT {
    return removeUndefinedFromObj({
      numeroCotizacion: hasValue(obj.numeroCotizacion) ? '' + obj.numeroCotizacion : undefined,
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined,
      correos: hasValue(obj.correos) ? obj.correos : undefined
    });
  }
}

export class ReqPathPostEnviarReferido {
  public codigoApp: string;

  public static create(obj): ReqPathPostEnviarReferido {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostEnviarReferido {
  public vehiculo?: object;
  public contratante?: object;
  public cotizacion?: object;

  public static create(obj): ReqBodyPostEnviarReferido {
    return removeUndefinedFromObj({
      vehiculo: hasValue(obj.vehiculo) ? obj.vehiculo : undefined,
      contratante: hasValue(obj.contratante) ? obj.contratante : undefined,
      cotizacion: hasValue(obj.cotizacion) ? obj.cotizacion : undefined
    });
  }
}

export class ReqPathGetCotizarRenovacionSoat {
  public codigoApp: string;
  public numeroPoliza: string;

  public static create(obj): ReqPathGetCotizarRenovacionSoat {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqPathGetObtenerDatosSOAT {
  public codigoApp: string;
  public placa: string;

  public static create(obj): ReqPathGetObtenerDatosSOAT {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      placa: hasValue(obj.placa) ? '' + obj.placa : undefined
    });
  }
}

export class ReqPathDeleteOlvidarSOAT {
  public codigoApp: string;
  public placa: string;

  public static create(obj): ReqPathDeleteOlvidarSOAT {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      placa: hasValue(obj.placa) ? '' + obj.placa : undefined
    });
  }
}

export class ReqPathGetObtenerListaSOAT {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerListaSOAT {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerListaAutoinspeccion {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerListaAutoinspeccion {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathPostRegistrarFoto {
  public codigoApp: string;

  public static create(obj): ReqPathPostRegistrarFoto {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqFormDataPostRegistrarFoto {
  public autoinspeccionId: number;
  public tipoFoto: string;
  public fotoAutoinspeccion: any;

  public static create(obj): ReqFormDataPostRegistrarFoto {
    return removeUndefinedFromObj({
      autoinspeccionId: hasValue(obj.autoinspeccionId) ? +obj.autoinspeccionId : undefined,
      tipoFoto: hasValue(obj.tipoFoto) ? '' + obj.tipoFoto : undefined, // AI_TARJETA_PROPIEDAD_FRONTAL, AI_TARJETA_PROPIEDAD_REVERSO, AI_AUTO_FRONTAL_IZQUIERDA, AI_AUTO_TRASERA_DERECHA, AI_AUTO_MALETERA_LLANTA, AI_AUTO_TERCERA_PLACA, AI_AUTO_PANEL
      fotoAutoinspeccion: hasValue(obj.fotoAutoinspeccion) ? obj.fotoAutoinspeccion : undefined
    });
  }
}

export class ReqPathPostRegistrarAutoinspeccion {
  public codigoApp: string;

  public static create(obj): ReqPathPostRegistrarAutoinspeccion {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostRegistrarAutoinspeccion {
  public autoinspeccionId?: number;
  public kilometraje?: string;
  public codigoColor?: number;
  public accesorios?: any[];
  public latitud?: number;
  public longitud?: number;
  public aprobacionPolizaElectronica?: any;
  public correoElectronico?: string;
  public nombreUsuario?: string;
  public esObservacion?: string;
  public datosVehiculo?: object;
  public idTipoFotos?: any[];

  public static create(obj): ReqBodyPostRegistrarAutoinspeccion {
    return removeUndefinedFromObj({
      autoinspeccionId: hasValue(obj.autoinspeccionId) ? +obj.autoinspeccionId : undefined,
      kilometraje: hasValue(obj.kilometraje) ? '' + obj.kilometraje : undefined,
      codigoColor: hasValue(obj.codigoColor) ? +obj.codigoColor : undefined,
      accesorios: hasValue(obj.accesorios) ? obj.accesorios : undefined,
      latitud: hasValue(obj.latitud) ? +obj.latitud : undefined,
      longitud: hasValue(obj.longitud) ? +obj.longitud : undefined,
      aprobacionPolizaElectronica: hasValue(obj.aprobacionPolizaElectronica)
        ? obj.aprobacionPolizaElectronica
        : undefined,
      correoElectronico: hasValue(obj.correoElectronico) ? '' + obj.correoElectronico : undefined,
      nombreUsuario: hasValue(obj.nombreUsuario) ? '' + obj.nombreUsuario : undefined,
      esObservacion: hasValue(obj.esObservacion) ? '' + obj.esObservacion : undefined,
      datosVehiculo: hasValue(obj.datosVehiculo) ? obj.datosVehiculo : undefined,
      idTipoFotos: hasValue(obj.idTipoFotos) ? obj.idTipoFotos : undefined
    });
  }
}

export class ReqPathPostRegistrarInspeccion {
  public codigoApp: string;

  public static create(obj): ReqPathPostRegistrarInspeccion {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostRegistrarInspeccion {
  public idInspeccion?: number;

  public static create(obj): ReqBodyPostRegistrarInspeccion {
    return removeUndefinedFromObj({
      idInspeccion: hasValue(obj.idInspeccion) ? +obj.idInspeccion : undefined
    });
  }
}

export class ReqPathPutActualizarInspeccion {
  public codigoApp: string;
  public numeroInspeccion: number;

  public static create(obj): ReqPathPutActualizarInspeccion {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroInspeccion: hasValue(obj.numeroInspeccion) ? +obj.numeroInspeccion : undefined
    });
  }
}

export class ReqPathGetObtenerObservacionPerito {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerObservacionPerito {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetObtenerObservacionPerito {
  public inspectionId: number;

  public static create(obj): ReqQueryGetObtenerObservacionPerito {
    return removeUndefinedFromObj({
      inspectionId: hasValue(obj.inspectionId) ? +obj.inspectionId : undefined
    });
  }
}

export class ReqPathGetFotoTipoVehiculo {
  public codigoApp: string;

  public static create(obj): ReqPathGetFotoTipoVehiculo {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetFotoTipoVehiculo {
  public codigoTipoVehiculo: number;

  public static create(obj): ReqQueryGetFotoTipoVehiculo {
    return removeUndefinedFromObj({
      codigoTipoVehiculo: hasValue(obj.codigoTipoVehiculo) ? +obj.codigoTipoVehiculo : undefined
    });
  }
}

export class ReqPathGetRangoHorasIncidentesPath {
  public codigoApp: string;

  public static create(obj): ReqPathGetRangoHorasIncidentesPath {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetRangoHorasIncidentesPath {
  public fecha?: string;

  public static create(obj): ReqQueryGetRangoHorasIncidentesPath {
    return removeUndefinedFromObj({
      fecha: hasValue(obj.fecha) ? '' + obj.fecha : undefined
    });
  }
}

export class ReqPathGetTiposIncidentesHogarPath {
  public codigoApp: string;

  public static create(obj): ReqPathGetTiposIncidentesHogarPath {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqFormDataPostRegistrarIncidenteHogarPath {
  public numeroPoliza: string;
  public numeroSuplemento: number;
  public numeroRiesgo: number;
  public codRamo: number;
  public direccion: string;
  public fecha: string;
  public identificadorRango: number;
  public rangoHorario: string;
  public identificadorIncidente: number;
  public identificadorMotivo: number;
  public descripcionIncidente: string;
  public fotosIncidente: any;

  public static create(obj): ReqFormDataPostRegistrarIncidenteHogarPath {
    return removeUndefinedFromObj({
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined,
      numeroSuplemento: hasValue(obj.numeroSuplemento) ? +obj.numeroSuplemento : undefined,
      numeroRiesgo: hasValue(obj.numeroRiesgo) ? +obj.numeroRiesgo : undefined,
      codRamo: hasValue(obj.codRamo) ? +obj.codRamo : undefined,
      direccion: hasValue(obj.direccion) ? '' + obj.direccion : undefined,
      fecha: hasValue(obj.fecha) ? '' + obj.fecha : undefined,
      identificadorRango: hasValue(obj.identificadorRango) ? +obj.identificadorRango : undefined,
      rangoHorario: hasValue(obj.rangoHorario) ? '' + obj.rangoHorario : undefined,
      identificadorIncidente: hasValue(obj.identificadorIncidente) ? +obj.identificadorIncidente : undefined,
      identificadorMotivo: hasValue(obj.identificadorMotivo) ? +obj.identificadorMotivo : undefined,
      descripcionIncidente: hasValue(obj.descripcionIncidente) ? '' + obj.descripcionIncidente : undefined,
      fotosIncidente: hasValue(obj.fotosIncidente) ? obj.fotosIncidente : undefined
    });
  }
}

export class ReqPathGetCoberturasDeduciblesPath {
  public codigoApp: string;
  public numeroPoliza: string;
  public numeroSuplemento: number;
  public numeroRiesgo: number;
  public codRamo: number;

  public static create(obj): ReqPathGetCoberturasDeduciblesPath {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined,
      numeroSuplemento: hasValue(obj.numeroSuplemento) ? +obj.numeroSuplemento : undefined,
      numeroRiesgo: hasValue(obj.numeroRiesgo) ? +obj.numeroRiesgo : undefined,
      codRamo: hasValue(obj.codRamo) ? +obj.codRamo : undefined
    });
  }
}

export class ReqPathGetObtenerCoberturasHogarRiesgo {
  public codigoApp: string;
  public numeroPoliza: string;
  public numeroSuplemento: number;
  public numeroRiesgo: number;
  public codRamo: number;

  public static create(obj): ReqPathGetObtenerCoberturasHogarRiesgo {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined,
      numeroSuplemento: hasValue(obj.numeroSuplemento) ? +obj.numeroSuplemento : undefined,
      numeroRiesgo: hasValue(obj.numeroRiesgo) ? +obj.numeroRiesgo : undefined,
      codRamo: hasValue(obj.codRamo) ? +obj.codRamo : undefined
    });
  }
}

export class ReqPathGetComparacionHogarProductos {
  public codigoApp: string;

  public static create(obj): ReqPathGetComparacionHogarProductos {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetComparacionHogarProductos {
  public numeroPoliza?: string;

  public static create(obj): ReqQueryGetComparacionHogarProductos {
    return removeUndefinedFromObj({
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqPathGetComparacionHogarCoberturas {
  public codigoApp: string;

  public static create(obj): ReqPathGetComparacionHogarCoberturas {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetComparacionHogarCoberturas {
  public numeroPoliza?: string;

  public static create(obj): ReqQueryGetComparacionHogarCoberturas {
    return removeUndefinedFromObj({
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqPathGetObtenerDisponibilidadSolicitudAsistencia {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerDisponibilidadSolicitudAsistencia {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerSolicitudAsistencia {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerSolicitudAsistencia {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathPostRegistrarSolicitudAsistencia {
  public codigoApp: string;

  public static create(obj): ReqPathPostRegistrarSolicitudAsistencia {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostRegistrarSolicitudAsistencia {
  public solicitudAsistenciaId?: number;
  public tipoDocumento?: string;
  public documento?: string;
  public latitudTelefono?: number;
  public longitudTelefono?: number;
  public latitudSeleccionada?: number;
  public longitudSeleccionada?: number;
  public fecha?: string;
  public hora?: string;
  public direccion?: string;
  public codigoTipoServicioSolicitado?: string;
  public datosMovil?: object;
  public nota?: string;

  public static create(obj): ReqBodyPostRegistrarSolicitudAsistencia {
    return removeUndefinedFromObj({
      solicitudAsistenciaId: hasValue(obj.solicitudAsistenciaId) ? +obj.solicitudAsistenciaId : undefined,
      tipoDocumento: hasValue(obj.tipoDocumento) ? '' + obj.tipoDocumento : undefined, // DNI, RUC, PEX, CEX, CIP
      documento: hasValue(obj.documento) ? '' + obj.documento : undefined,
      latitudTelefono: hasValue(obj.latitudTelefono) ? +obj.latitudTelefono : undefined,
      longitudTelefono: hasValue(obj.longitudTelefono) ? +obj.longitudTelefono : undefined,
      latitudSeleccionada: hasValue(obj.latitudSeleccionada) ? +obj.latitudSeleccionada : undefined,
      longitudSeleccionada: hasValue(obj.longitudSeleccionada) ? +obj.longitudSeleccionada : undefined,
      fecha: hasValue(obj.fecha) ? '' + obj.fecha : undefined,
      hora: hasValue(obj.hora) ? '' + obj.hora : undefined,
      direccion: hasValue(obj.direccion) ? '' + obj.direccion : undefined,
      codigoTipoServicioSolicitado: hasValue(obj.codigoTipoServicioSolicitado)
        ? '' + obj.codigoTipoServicioSolicitado
        : undefined, // 001, 002, 003, 004
      datosMovil: hasValue(obj.datosMovil) ? obj.datosMovil : undefined,
      nota: hasValue(obj.nota) ? '' + obj.nota : undefined
    });
  }
}

export class ReqPathDeleteEliminarSolicitudAsistencia {
  public codigoApp: string;
  public solicitudAsistenciaId: number;

  public static create(obj): ReqPathDeleteEliminarSolicitudAsistencia {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      solicitudAsistenciaId: hasValue(obj.solicitudAsistenciaId) ? +obj.solicitudAsistenciaId : undefined
    });
  }
}

export class ReqPathGetObtenerAsistenciasPorUsuario {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerAsistenciasPorUsuario {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetObtenerAsistenciasPorUsuario {
  public latitud?: number;
  public longitud?: number;

  public static create(obj): ReqQueryGetObtenerAsistenciasPorUsuario {
    return removeUndefinedFromObj({
      latitud: hasValue(obj.latitud) ? +obj.latitud : undefined,
      longitud: hasValue(obj.longitud) ? +obj.longitud : undefined
    });
  }
}

export class ReqPathGetObtenerAsistencia {
  public codigoApp: string;
  public asistenciaId: number;

  public static create(obj): ReqPathGetObtenerAsistencia {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      asistenciaId: hasValue(obj.asistenciaId) ? +obj.asistenciaId : undefined
    });
  }
}

export class ReqQueryGetObtenerAsistencia {
  public latitud?: number;
  public longitud?: number;

  public static create(obj): ReqQueryGetObtenerAsistencia {
    return removeUndefinedFromObj({
      latitud: hasValue(obj.latitud) ? +obj.latitud : undefined,
      longitud: hasValue(obj.longitud) ? +obj.longitud : undefined
    });
  }
}

export class ReqPathDeleteEliminarAsistencia {
  public codigoApp: string;
  public asistenciaId: number;

  public static create(obj): ReqPathDeleteEliminarAsistencia {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      asistenciaId: hasValue(obj.asistenciaId) ? +obj.asistenciaId : undefined
    });
  }
}

export class ReqPathGetObtenerSiniestro {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerSiniestro {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetObtenerSiniestro {
  public tipo?: string;
  public estado?: string;
  public numeroPoliza?: string;
  public anioOcurrencia?: number;
  public tipoPoliza?: string;
  public criterio?: string;
  public orden?: string;
  public registros?: number;
  public pagina?: number;

  public static create(obj): ReqQueryGetObtenerSiniestro {
    return removeUndefinedFromObj({
      tipo: hasValue(obj.tipo) ? '' + obj.tipo : undefined, // enCurso, historicos
      estado: hasValue(obj.estado) ? '' + obj.estado : undefined, // P, T
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined,
      anioOcurrencia: hasValue(obj.anioOcurrencia) ? +obj.anioOcurrencia : undefined,
      tipoPoliza: hasValue(obj.tipoPoliza) ? '' + obj.tipoPoliza : undefined, // MD_AUTOS, MD_SALUD, MD_VIDA, MD_HOGAR, MD_DECESOS, MD_SCTR, MD_SOAT, MD_SOAT_ELECTRO, MD_EPS, MD_SOAT|MD_SOAT_ELECTRO, MD_SALUD|MD_EPS, MD_SALUD_COTIZACION, MD_TIPO_USUARIO, MD_SCTR_PENSION, MD_SCTR_SALUD, MD_VIAJES, MD_AUTOS|MD_SOAT, MD_AUTOS|MD_SOAT|MD_SOAT_ELECTRO, MD_OTROS
      criterio: hasValue(obj.criterio) ? '' + obj.criterio : undefined,
      orden: hasValue(obj.orden) ? '' + obj.orden : undefined, // ASC, DESC
      registros: hasValue(obj.registros) ? +obj.registros : undefined,
      pagina: hasValue(obj.pagina) ? +obj.pagina : undefined
    });
  }
}

export class ReqPathGetObtenerAniosSiniestro {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerAniosSiniestro {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetDescargarEcu {
  public codigoApp: string;

  public static create(obj): ReqPathGetDescargarEcu {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetDescargarConprobante {
  public codigoApp: string;
  public codCia: number;
  public numeroRecibo: string;
  public tipo: string;

  public static create(obj): ReqPathGetDescargarConprobante {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      codCia: hasValue(obj.codCia) ? +obj.codCia : undefined, // 1, 2, 3
      numeroRecibo: hasValue(obj.numeroRecibo) ? '' + obj.numeroRecibo : undefined,
      tipo: hasValue(obj.tipo) ? '' + obj.tipo : undefined // RE, AV
    });
  }
}

export class ReqPathGetObtenerComprobantePagoHpexstream {
  public codigoApp: string;
  public codCia: number;
  public numeroRecibo: string;
  public tipoDocumentoPago: string;

  public static create(obj): ReqPathGetObtenerComprobantePagoHpexstream {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      codCia: hasValue(obj.codCia) ? +obj.codCia : undefined, // 1, 2, 3
      numeroRecibo: hasValue(obj.numeroRecibo) ? '' + obj.numeroRecibo : undefined,
      tipoDocumentoPago: hasValue(obj.tipoDocumentoPago) ? '' + obj.tipoDocumentoPago : undefined // RECIBO, AVISO
    });
  }
}

export class ReqPathPostPasarelaFormularioToken {
  public codigoApp: string;

  public static create(obj): ReqPathPostPasarelaFormularioToken {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostPasarelaFormularioToken {
  public codigoMoneda?: number;
  public monto?: number;
  public descripcionOrden?: string;
  public cliente?: object;
  public formaDePago?: string;
  public aliasToken?: string;
  public tipoDocumentoPago?: string;

  public static create(obj): ReqBodyPostPasarelaFormularioToken {
    return removeUndefinedFromObj({
      codigoMoneda: hasValue(obj.codigoMoneda) ? +obj.codigoMoneda : undefined, // 1, 2
      monto: hasValue(obj.monto) ? +obj.monto : undefined,
      descripcionOrden: hasValue(obj.descripcionOrden) ? '' + obj.descripcionOrden : undefined,
      cliente: hasValue(obj.cliente) ? obj.cliente : undefined,
      formaDePago: hasValue(obj.formaDePago) ? '' + obj.formaDePago : undefined, // RP, AR, RT
      aliasToken: hasValue(obj.aliasToken) ? '' + obj.aliasToken : undefined,
      tipoDocumentoPago: hasValue(obj.tipoDocumentoPago) ? '' + obj.tipoDocumentoPago : undefined
    });
  }
}

export class ReqPathPostPagarReciboLyra {
  public codigoApp: string;

  public static create(obj): ReqPathPostPagarReciboLyra {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostPagarReciboLyra {
  public modalidadPago?: string;
  public codigoTransaccion?: string;
  public fechaTransaccion?: string;
  public estadoOrden?: string;
  public tienePagoAutomatico?: any;
  public guardarTarjeta?: any;
  public trazaPasarelaJson?: string;
  public tarjeta?: object;
  public recibo?: object;

  public static create(obj): ReqBodyPostPagarReciboLyra {
    return removeUndefinedFromObj({
      modalidadPago: hasValue(obj.modalidadPago) ? '' + obj.modalidadPago : undefined, // PAGO_TARJETA, PAGO_BANCARIO, PAGO_TOKEN
      codigoTransaccion: hasValue(obj.codigoTransaccion) ? '' + obj.codigoTransaccion : undefined,
      fechaTransaccion: hasValue(obj.fechaTransaccion) ? '' + obj.fechaTransaccion : undefined,
      estadoOrden: hasValue(obj.estadoOrden) ? '' + obj.estadoOrden : undefined,
      tienePagoAutomatico: hasValue(obj.tienePagoAutomatico) ? obj.tienePagoAutomatico : undefined,
      guardarTarjeta: hasValue(obj.guardarTarjeta) ? obj.guardarTarjeta : undefined,
      trazaPasarelaJson: hasValue(obj.trazaPasarelaJson) ? '' + obj.trazaPasarelaJson : undefined,
      tarjeta: hasValue(obj.tarjeta) ? obj.tarjeta : undefined,
      recibo: hasValue(obj.recibo) ? obj.recibo : undefined
    });
  }
}

export class ReqPathPostRegistrarTallerFavorito {
  public codigoApp: string;
  public tallerId: string;

  public static create(obj): ReqPathPostRegistrarTallerFavorito {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      tallerId: hasValue(obj.tallerId) ? '' + obj.tallerId : undefined
    });
  }
}

export class ReqBodyPostRegistrarTallerFavorito {
  public esFavorito?: any;

  public static create(obj): ReqBodyPostRegistrarTallerFavorito {
    return removeUndefinedFromObj({
      esFavorito: hasValue(obj.esFavorito) ? obj.esFavorito : undefined
    });
  }
}

export class ReqPathGetBuscarTallerFavorito {
  public codigoApp: string;

  public static create(obj): ReqPathGetBuscarTallerFavorito {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetBuscarTallerFavorito {
  public latitud: number;
  public longitud: number;
  public radio: number;

  public static create(obj): ReqQueryGetBuscarTallerFavorito {
    return removeUndefinedFromObj({
      latitud: hasValue(obj.latitud) ? +obj.latitud : undefined,
      longitud: hasValue(obj.longitud) ? +obj.longitud : undefined,
      radio: hasValue(obj.radio) ? +obj.radio : undefined
    });
  }
}

export class ReqPathGetBuscarTallerUbigeoFavorito {
  public codigoApp: string;

  public static create(obj): ReqPathGetBuscarTallerUbigeoFavorito {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetBuscarTallerUbigeoFavorito {
  public idDepartamento: number;
  public idProvincia: number;
  public idDistrito: number;

  public static create(obj): ReqQueryGetBuscarTallerUbigeoFavorito {
    return removeUndefinedFromObj({
      idDepartamento: hasValue(obj.idDepartamento) ? +obj.idDepartamento : undefined,
      idProvincia: hasValue(obj.idProvincia) ? +obj.idProvincia : undefined,
      idDistrito: hasValue(obj.idDistrito) ? +obj.idDistrito : undefined
    });
  }
}

export class ReqPathGetObtenerEpsContratanteDatosCardContrato {
  public nroContrato: string;

  public static create(obj): ReqPathGetObtenerEpsContratanteDatosCardContrato {
    return removeUndefinedFromObj({
      nroContrato: hasValue(obj.nroContrato) ? '' + obj.nroContrato : undefined
    });
  }
}

export class ReqQueryGetObtenerEpsContratanteDatosCardContrato {
  public codigoApp: string;

  public static create(obj): ReqQueryGetObtenerEpsContratanteDatosCardContrato {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerEpsContratanteConceptoPago {
  public nroContrato: string;

  public static create(obj): ReqPathGetObtenerEpsContratanteConceptoPago {
    return removeUndefinedFromObj({
      nroContrato: hasValue(obj.nroContrato) ? '' + obj.nroContrato : undefined
    });
  }
}

export class ReqQueryGetObtenerEpsContratanteConceptoPago {
  public codigoApp: string;
  public codCliente: string;

  public static create(obj): ReqQueryGetObtenerEpsContratanteConceptoPago {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      codCliente: hasValue(obj.codCliente) ? '' + obj.codCliente : undefined
    });
  }
}

export class ReqPathGetObtenerEpsContratanteDatosCardAfiliados {
  public nroContrato: string;

  public static create(obj): ReqPathGetObtenerEpsContratanteDatosCardAfiliados {
    return removeUndefinedFromObj({
      nroContrato: hasValue(obj.nroContrato) ? '' + obj.nroContrato : undefined
    });
  }
}

export class ReqQueryGetObtenerEpsContratanteDatosCardAfiliados {
  public codigoApp: string;

  public static create(obj): ReqQueryGetObtenerEpsContratanteDatosCardAfiliados {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerEpsContratanteDatosDocsPago {
  public nroContrato: string;

  public static create(obj): ReqPathGetObtenerEpsContratanteDatosDocsPago {
    return removeUndefinedFromObj({
      nroContrato: hasValue(obj.nroContrato) ? '' + obj.nroContrato : undefined
    });
  }
}

export class ReqQueryGetObtenerEpsContratanteDatosDocsPago {
  public codigoApp: string;

  public static create(obj): ReqQueryGetObtenerEpsContratanteDatosDocsPago {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerEpsContratanteAfiliadosFiltros {
  public nroContrato: string;

  public static create(obj): ReqPathGetObtenerEpsContratanteAfiliadosFiltros {
    return removeUndefinedFromObj({
      nroContrato: hasValue(obj.nroContrato) ? '' + obj.nroContrato : undefined
    });
  }
}

export class ReqQueryGetObtenerEpsContratanteAfiliadosFiltros {
  public codigoApp: string;

  public static create(obj): ReqQueryGetObtenerEpsContratanteAfiliadosFiltros {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerEpsContratanteAfiliadosDetalle {
  public nroContrato: string;

  public static create(obj): ReqPathGetObtenerEpsContratanteAfiliadosDetalle {
    return removeUndefinedFromObj({
      nroContrato: hasValue(obj.nroContrato) ? '' + obj.nroContrato : undefined
    });
  }
}

export class ReqQueryGetObtenerEpsContratanteAfiliadosDetalle {
  public codigoApp: string;
  public codPlan?: number;
  public periodo?: string;
  public datoPersonal?: string;
  public registros?: number;
  public pagina?: number;

  public static create(obj): ReqQueryGetObtenerEpsContratanteAfiliadosDetalle {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      codPlan: hasValue(obj.codPlan) ? +obj.codPlan : undefined,
      periodo: hasValue(obj.periodo) ? '' + obj.periodo : undefined,
      datoPersonal: hasValue(obj.datoPersonal) ? '' + obj.datoPersonal : undefined,
      registros: hasValue(obj.registros) ? +obj.registros : undefined,
      pagina: hasValue(obj.pagina) ? +obj.pagina : undefined
    });
  }
}

export class ReqPathPutActualizarEpsContratanteAfiliados {
  public nroContrato: string;

  public static create(obj): ReqPathPutActualizarEpsContratanteAfiliados {
    return removeUndefinedFromObj({
      nroContrato: hasValue(obj.nroContrato) ? '' + obj.nroContrato : undefined
    });
  }
}

export class ReqQueryPutActualizarEpsContratanteAfiliados {
  public codigoApp: string;

  public static create(obj): ReqQueryPutActualizarEpsContratanteAfiliados {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPutActualizarEpsContratanteAfiliados {
  public afiliadosAActualizar?: object[];

  public static create(obj): ReqBodyPutActualizarEpsContratanteAfiliados {
    return obj.afiliadosAActualizar.map(v => v);
  }
}

export class ReqPathGetDescargarEpsContratanteManualAfiliado {
  public nroContrato: string;

  public static create(obj): ReqPathGetDescargarEpsContratanteManualAfiliado {
    return removeUndefinedFromObj({
      nroContrato: hasValue(obj.nroContrato) ? '' + obj.nroContrato : undefined
    });
  }
}

export class ReqQueryGetDescargarEpsContratanteManualAfiliado {
  public codigoApp: string;

  public static create(obj): ReqQueryGetDescargarEpsContratanteManualAfiliado {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryPostRegistrarToken {
  public codigoApp: string;
  public tipoDocumento: string;
  public documento: string;

  public static create(obj): ReqQueryPostRegistrarToken {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      tipoDocumento: hasValue(obj.tipoDocumento) ? '' + obj.tipoDocumento : undefined, // DNI, RUC, PEX, CEX, CIP
      documento: hasValue(obj.documento) ? '' + obj.documento : undefined
    });
  }
}

export class ReqBodyPostRegistrarToken {
  public token?: string;

  public static create(obj): ReqBodyPostRegistrarToken {
    return removeUndefinedFromObj({
      token: hasValue(obj.token) ? '' + obj.token : undefined
    });
  }
}

export class ReqQueryGetObtenerToken {
  public codigoApp: string;
  public tipoDocumento: string;
  public documento: string;

  public static create(obj): ReqQueryGetObtenerToken {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      tipoDocumento: hasValue(obj.tipoDocumento) ? '' + obj.tipoDocumento : undefined, // DNI, RUC, PEX, CEX, CIP
      documento: hasValue(obj.documento) ? '' + obj.documento : undefined
    });
  }
}

export class ReqPathPostRegistrarDiagnostico {
  public token: string;

  public static create(obj): ReqPathPostRegistrarDiagnostico {
    return removeUndefinedFromObj({
      token: hasValue(obj.token) ? '' + obj.token : undefined
    });
  }
}

export class ReqQueryPostRegistrarDiagnostico {
  public codigoApp: string;
  public tipoDocumento: string;
  public documento: string;

  public static create(obj): ReqQueryPostRegistrarDiagnostico {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      tipoDocumento: hasValue(obj.tipoDocumento) ? '' + obj.tipoDocumento : undefined, // DNI, RUC, PEX, CEX, CIP
      documento: hasValue(obj.documento) ? '' + obj.documento : undefined
    });
  }
}

export class ReqBodyPostRegistrarDiagnostico {
  public idSession?: string;
  public descripcion?: string;

  public static create(obj): ReqBodyPostRegistrarDiagnostico {
    return removeUndefinedFromObj({
      idSession: hasValue(obj.idSession) ? '' + obj.idSession : undefined,
      descripcion: hasValue(obj.descripcion) ? '' + obj.descripcion : undefined
    });
  }
}

export class ReqQueryGetObtenerBeneficiosAdicionales {
  public codigoApp: string;

  public static create(obj): ReqQueryGetObtenerBeneficiosAdicionales {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetObtenerAseguradosBeneficiosAdicionales {
  public codigoApp: string;

  public static create(obj): ReqQueryGetObtenerAseguradosBeneficiosAdicionales {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetObtenerTarjetas {
  public codigoApp: string;

  public static create(obj): ReqQueryGetObtenerTarjetas {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryPostGuardarTarjeta {
  public codigoApp: string;

  public static create(obj): ReqQueryPostGuardarTarjeta {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostGuardarTarjeta {
  public tarjetaToken?: string;
  public numero?: string;
  public tipo?: string;
  public fechaVencimiento?: string;

  public static create(obj): ReqBodyPostGuardarTarjeta {
    return removeUndefinedFromObj({
      tarjetaToken: hasValue(obj.tarjetaToken) ? '' + obj.tarjetaToken : undefined,
      numero: hasValue(obj.numero) ? '' + obj.numero : undefined,
      tipo: hasValue(obj.tipo) ? '' + obj.tipo : undefined, // VISA, VISA_DEBIT, VISA_ELECTRON, MASTERCARD, DINERS_CLUB, AMEX, MASTERCARD_DEBIT, AMERICAN_EXPRESS
      fechaVencimiento: hasValue(obj.fechaVencimiento) ? '' + obj.fechaVencimiento : undefined
    });
  }
}

export class ReqQueryDeleteEliminarTarjeta {
  public codigoApp: string;
  public tarjetaToken: string;

  public static create(obj): ReqQueryDeleteEliminarTarjeta {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      tarjetaToken: hasValue(obj.tarjetaToken) ? '' + obj.tarjetaToken : undefined
    });
  }
}

export class ReqQueryPostAfiliarTarjeta {
  public codigoApp: string;

  public static create(obj): ReqQueryPostAfiliarTarjeta {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostAfiliarTarjeta {
  public tarjetaToken?: string;
  public codCia?: number;
  public numeroPoliza?: string;

  public static create(obj): ReqBodyPostAfiliarTarjeta {
    return removeUndefinedFromObj({
      tarjetaToken: hasValue(obj.tarjetaToken) ? '' + obj.tarjetaToken : undefined,
      codCia: hasValue(obj.codCia) ? +obj.codCia : undefined,
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqQueryDeleteDesafiliarTarjeta {
  public codigoApp: string;
  public tarjetaToken?: string;
  public codCia: number;
  public numeroPoliza: string;

  public static create(obj): ReqQueryDeleteDesafiliarTarjeta {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      tarjetaToken: hasValue(obj.tarjetaToken) ? '' + obj.tarjetaToken : undefined,
      codCia: hasValue(obj.codCia) ? +obj.codCia : undefined,
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined
    });
  }
}

export class ReqQueryGetObtenerEstadoMantenimiento {
  public codigoApp: string;
  public documento?: string;

  public static create(obj): ReqQueryGetObtenerEstadoMantenimiento {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      documento: hasValue(obj.documento) ? '' + obj.documento : undefined
    });
  }
}

export class ReqQueryGetObtenerListaCartasGarantia {
  public codigoApp: string;
  public textoBuscar?: string;
  public estado?: number;
  public fechaIngresoInicial?: string;
  public fechaIngresoFinal?: string;
  public clinicaId?: number;
  public registros?: number;
  public pagina?: number;

  public static create(obj): ReqQueryGetObtenerListaCartasGarantia {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      textoBuscar: hasValue(obj.textoBuscar) ? '' + obj.textoBuscar : undefined,
      estado: hasValue(obj.estado) ? +obj.estado : undefined,
      fechaIngresoInicial: hasValue(obj.fechaIngresoInicial) ? '' + obj.fechaIngresoInicial : undefined,
      fechaIngresoFinal: hasValue(obj.fechaIngresoFinal) ? '' + obj.fechaIngresoFinal : undefined,
      clinicaId: hasValue(obj.clinicaId) ? +obj.clinicaId : undefined,
      registros: hasValue(obj.registros) ? +obj.registros : undefined,
      pagina: hasValue(obj.pagina) ? +obj.pagina : undefined
    });
  }
}

export class ReqQueryGetObtenerTipoEstados {
  public codigoApp: string;

  public static create(obj): ReqQueryGetObtenerTipoEstados {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetObtenerClinica {
  public codigoApp: string;

  public static create(obj): ReqQueryGetObtenerClinica {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetDescargarCarta {
  public codCia: number;
  public anio: number;
  public correlativo: number;
  public version: number;

  public static create(obj): ReqPathGetDescargarCarta {
    return removeUndefinedFromObj({
      codCia: hasValue(obj.codCia) ? +obj.codCia : undefined, // 1, 2, 3
      anio: hasValue(obj.anio) ? +obj.anio : undefined,
      correlativo: hasValue(obj.correlativo) ? +obj.correlativo : undefined,
      version: hasValue(obj.version) ? +obj.version : undefined
    });
  }
}

export class ReqQueryGetDescargarCarta {
  public codigoApp: string;

  public static create(obj): ReqQueryGetDescargarCarta {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetObtenerFiltros {
  public codigoApp: string;

  public static create(obj): ReqQueryGetObtenerFiltros {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetObtenerPlanesCdm {
  public codigoApp: string;

  public static create(obj): ReqQueryGetObtenerPlanesCdm {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetObtenerParametrosCdm {
  public codigoApp: string;
  public codigoGrupo?: string;

  public static create(obj): ReqQueryGetObtenerParametrosCdm {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      codigoGrupo: hasValue(obj.codigoGrupo) ? '' + obj.codigoGrupo : undefined
    });
  }
}

export class ReqQueryPostGenerarCotizacionCdm {
  public codigoApp: string;

  public static create(obj): ReqQueryPostGenerarCotizacionCdm {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostGenerarCotizacionCdm {
  public poliza?: object;
  public asegurados?: any[];

  public static create(obj): ReqBodyPostGenerarCotizacionCdm {
    return removeUndefinedFromObj({
      poliza: hasValue(obj.poliza) ? obj.poliza : undefined,
      asegurados: hasValue(obj.asegurados) ? obj.asegurados : undefined
    });
  }
}

export class ReqQueryGetObtenerServiciosCdm {
  public codigoApp: string;

  public static create(obj): ReqQueryGetObtenerServiciosCdm {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export default '';
