// tslint:disable

import { hasValue } from '@mx/core/shared/helpers';

function removeUndefinedFromObj(obj: any): any {
  const arrObjKeys = Object.keys(obj);

  return arrObjKeys.reduce((acc, k) => (hasValue(obj[k]) ? { ...acc, [k]: obj[k] } : acc), {});
}

export class ReqFormDataPostLogin {
  public client_id?: string;
  public grant_type?: string;
  public username?: string;
  public password?: string;
  public groupTypeId?: number;
  public refresh_token?: string;

  public static create(obj): ReqFormDataPostLogin {
    return removeUndefinedFromObj({
      client_id: hasValue(obj.client_id) ? '' + obj.client_id : undefined,
      grant_type: hasValue(obj.grant_type) ? '' + obj.grant_type : undefined,
      username: hasValue(obj.username) ? '' + obj.username : undefined,
      password: hasValue(obj.password) ? '' + obj.password : undefined,
      groupTypeId: hasValue(obj.groupTypeId) ? +obj.groupTypeId : undefined,
      refresh_token: hasValue(obj.refresh_token) ? '' + obj.refresh_token : undefined
    });
  }
}

export class ReqFormDataPostLoginPaso1 {
  public client_id?: string;
  public grant_type?: string;
  public username?: string;
  public password?: string;

  public static create(obj): ReqFormDataPostLoginPaso1 {
    return removeUndefinedFromObj({
      client_id: hasValue(obj.client_id) ? '' + obj.client_id : undefined,
      grant_type: hasValue(obj.grant_type) ? '' + obj.grant_type : undefined,
      username: hasValue(obj.username) ? '' + obj.username : undefined,
      password: hasValue(obj.password) ? '' + obj.password : undefined
    });
  }
}

export class ReqFormDataPostLoginPaso2 {
  public GroupTypeId?: number;

  public static create(obj): ReqFormDataPostLoginPaso2 {
    return removeUndefinedFromObj({
      GroupTypeId: hasValue(obj.GroupTypeId) ? +obj.GroupTypeId : undefined
    });
  }
}

export class ReqBodyPostCambioContrasenaUsuarioPersona {
  public documentType?: string;
  public documentNumber?: string;
  public newPassword?: string;

  public static create(obj): ReqBodyPostCambioContrasenaUsuarioPersona {
    return removeUndefinedFromObj({
      documentType: hasValue(obj.documentType) ? '' + obj.documentType : undefined, // DNI, RUC, PEX, CEX, CIP
      documentNumber: hasValue(obj.documentNumber) ? '' + obj.documentNumber : undefined,
      newPassword: hasValue(obj.newPassword) ? '' + obj.newPassword : undefined
    });
  }
}

export class ReqBodyPostCambiarContraseniaConContraseniaAntigua {
  public documentType?: string;
  public documentNumber?: string;
  public oldPassword?: string;
  public newPassword?: string;

  public static create(obj): ReqBodyPostCambiarContraseniaConContraseniaAntigua {
    return removeUndefinedFromObj({
      documentType: hasValue(obj.documentType) ? '' + obj.documentType : undefined, // DNI, RUC, PEX, CEX, CIP
      documentNumber: hasValue(obj.documentNumber) ? '' + obj.documentNumber : undefined,
      oldPassword: hasValue(obj.oldPassword) ? '' + obj.oldPassword : undefined,
      newPassword: hasValue(obj.newPassword) ? '' + obj.newPassword : undefined
    });
  }
}

export class ReqBodyPostCambiarContraseniaConToken {
  public token?: string;
  public password?: string;

  public static create(obj): ReqBodyPostCambiarContraseniaConToken {
    return removeUndefinedFromObj({
      token: hasValue(obj.token) ? '' + obj.token : undefined,
      password: hasValue(obj.password) ? '' + obj.password : undefined
    });
  }
}

export class ReqBodyPostRecuperarContraseniaMail {
  public documentType?: string;
  public documentNumber?: string;
  public applicationCode?: string;

  public static create(obj): ReqBodyPostRecuperarContraseniaMail {
    return removeUndefinedFromObj({
      documentType: hasValue(obj.documentType) ? '' + obj.documentType : undefined, // DNI, RUC, CE
      documentNumber: hasValue(obj.documentNumber) ? '' + obj.documentNumber : undefined,
      applicationCode: hasValue(obj.applicationCode) ? '' + obj.applicationCode : undefined
    });
  }
}

export class ReqBodyPostHabilitarUsuario {
  public documentType?: string;
  public documentNumber?: string;

  public static create(obj): ReqBodyPostHabilitarUsuario {
    return removeUndefinedFromObj({
      documentType: hasValue(obj.documentType) ? '' + obj.documentType : undefined, // DNI, RUC, CE
      documentNumber: hasValue(obj.documentNumber) ? '' + obj.documentNumber : undefined
    });
  }
}

export class ReqBodyPostGenerarTokenDummy {
  public origin?: string;
  public destiny?: string;
  public connectionIdentifier?: string;
  public urlRedirection?: string;

  public static create(obj): ReqBodyPostGenerarTokenDummy {
    return removeUndefinedFromObj({
      origin: hasValue(obj.origin) ? '' + obj.origin : undefined,
      destiny: hasValue(obj.destiny) ? '' + obj.destiny : undefined,
      connectionIdentifier: hasValue(obj.connectionIdentifier) ? '' + obj.connectionIdentifier : undefined,
      urlRedirection: hasValue(obj.urlRedirection) ? '' + obj.urlRedirection : undefined
    });
  }
}

export class ReqBodyPostTransformarTokenDummy {
  public tokenDummy?: string;
  public connectionIdentifier?: string;

  public static create(obj): ReqBodyPostTransformarTokenDummy {
    return removeUndefinedFromObj({
      tokenDummy: hasValue(obj.tokenDummy) ? '' + obj.tokenDummy : undefined,
      connectionIdentifier: hasValue(obj.connectionIdentifier) ? '' + obj.connectionIdentifier : undefined
    });
  }
}

export default '';
