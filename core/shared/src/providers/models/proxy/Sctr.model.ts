// tslint:disable

import { hasValue } from '@mx/core/shared/helpers';

function removeUndefinedFromObj(obj: any): any {
  const arrObjKeys = Object.keys(obj);

  return arrObjKeys.reduce((acc, k) => (hasValue(obj[k]) ? { ...acc, [k]: obj[k] } : acc), {});
}

export class ReqBodyPostHacerCotizacion {
  public numeroTrabajadores?: number;
  public montoPlanilla?: number;
  public lugarActividad?: string;
  public duracion?: number;
  public frecuenciaDeclaracion?: string;
  public actividadRiesgoId?: number;
  public subActividadRiesgoId?: number;

  public static create(obj): ReqBodyPostHacerCotizacion {
    return removeUndefinedFromObj({
      numeroTrabajadores: hasValue(obj.numeroTrabajadores) ? +obj.numeroTrabajadores : undefined,
      montoPlanilla: hasValue(obj.montoPlanilla) ? +obj.montoPlanilla : undefined,
      lugarActividad: hasValue(obj.lugarActividad) ? '' + obj.lugarActividad : undefined,
      duracion: hasValue(obj.duracion) ? +obj.duracion : undefined,
      frecuenciaDeclaracion: hasValue(obj.frecuenciaDeclaracion) ? '' + obj.frecuenciaDeclaracion : undefined, // MEN, BIM
      actividadRiesgoId: hasValue(obj.actividadRiesgoId) ? +obj.actividadRiesgoId : undefined,
      subActividadRiesgoId: hasValue(obj.subActividadRiesgoId) ? +obj.subActividadRiesgoId : undefined
    });
  }
}

export class ReqBodyPostHacerCotizacionReferida {
  public nombre?: string;
  public telefono?: string;
  public incumplimiento?: object;

  public static create(obj): ReqBodyPostHacerCotizacionReferida {
    return removeUndefinedFromObj({
      nombre: hasValue(obj.nombre) ? '' + obj.nombre : undefined,
      telefono: hasValue(obj.telefono) ? '' + obj.telefono : undefined,
      incumplimiento: hasValue(obj.incumplimiento) ? obj.incumplimiento : undefined
    });
  }
}

export class ReqBodyPostEmitirPagoBancario {
  public cotizacion?: object;
  public trabajadores?: any[];
  public contratante?: object;
  public fechaInicio?: string;
  public fechaFin?: string;
  public pago?: object;

  public static create(obj): ReqBodyPostEmitirPagoBancario {
    return removeUndefinedFromObj({
      cotizacion: hasValue(obj.cotizacion) ? obj.cotizacion : undefined,
      trabajadores: hasValue(obj.trabajadores) ? obj.trabajadores : undefined,
      contratante: hasValue(obj.contratante) ? obj.contratante : undefined,
      fechaInicio: hasValue(obj.fechaInicio) ? '' + obj.fechaInicio : undefined,
      fechaFin: hasValue(obj.fechaFin) ? '' + obj.fechaFin : undefined,
      pago: hasValue(obj.pago) ? obj.pago : undefined
    });
  }
}

export class ReqBodyPostEmitirTarjetas {
  public cotizacion?: object;
  public trabajadores?: any[];
  public contratante?: object;
  public fechaInicio?: string;
  public fechaFin?: string;
  public pago?: object;

  public static create(obj): ReqBodyPostEmitirTarjetas {
    return removeUndefinedFromObj({
      cotizacion: hasValue(obj.cotizacion) ? obj.cotizacion : undefined,
      trabajadores: hasValue(obj.trabajadores) ? obj.trabajadores : undefined,
      contratante: hasValue(obj.contratante) ? obj.contratante : undefined,
      fechaInicio: hasValue(obj.fechaInicio) ? '' + obj.fechaInicio : undefined,
      fechaFin: hasValue(obj.fechaFin) ? '' + obj.fechaFin : undefined,
      pago: hasValue(obj.pago) ? obj.pago : undefined
    });
  }
}

export class ReqBodyPostEmitirCorreo {
  public polizas?: any[];
  public destinatarios?: any[];

  public static create(obj): ReqBodyPostEmitirCorreo {
    return removeUndefinedFromObj({
      polizas: hasValue(obj.polizas) ? obj.polizas : undefined,
      destinatarios: hasValue(obj.destinatarios) ? obj.destinatarios : undefined
    });
  }
}

export class ReqPathGetObtenerClausula {
  public subActividadId: number;

  public static create(obj): ReqPathGetObtenerClausula {
    return removeUndefinedFromObj({
      subActividadId: hasValue(obj.subActividadId) ? +obj.subActividadId : undefined
    });
  }
}

export class ReqQueryGetObtenerListaDocumentos {
  public numeroPolizas: string;

  public static create(obj): ReqQueryGetObtenerListaDocumentos {
    return removeUndefinedFromObj({
      numeroPolizas: hasValue(obj.numeroPolizas) ? '' + obj.numeroPolizas : undefined
    });
  }
}

export class ReqPathGetEmitirDocumentos {
  public numeroPoliza: string;
  public codigoDocumento: string;

  public static create(obj): ReqPathGetEmitirDocumentos {
    return removeUndefinedFromObj({
      numeroPoliza: hasValue(obj.numeroPoliza) ? '' + obj.numeroPoliza : undefined,
      codigoDocumento: hasValue(obj.codigoDocumento) ? '' + obj.codigoDocumento : undefined
    });
  }
}

export class ReqPathGetObtenerConstanciasYAsegurados {
  public codigoApp: string;
  public nroConstancia: string;
  public tipoDocumento: string;
  public documento: string;

  public static create(obj): ReqPathGetObtenerConstanciasYAsegurados {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      nroConstancia: hasValue(obj.nroConstancia) ? '' + obj.nroConstancia : undefined,
      tipoDocumento: hasValue(obj.tipoDocumento) ? '' + obj.tipoDocumento : undefined, // DNI, RUC, PEX, CEX, CIP
      documento: hasValue(obj.documento) ? '' + obj.documento : undefined
    });
  }
}

export default '';
