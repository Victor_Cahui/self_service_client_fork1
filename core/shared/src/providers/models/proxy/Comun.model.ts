// tslint:disable

import { hasValue } from '@mx/core/shared/helpers';

function removeUndefinedFromObj(obj: any): any {
  const arrObjKeys = Object.keys(obj);

  return arrObjKeys.reduce((acc, k) => (hasValue(obj[k]) ? { ...acc, [k]: obj[k] } : acc), {});
}

export class ReqPathGetObtenerObtenerNotificaciones {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerObtenerNotificaciones {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetObtenerObtenerNotificaciones {
  public tipoNotificacion?: string;

  public static create(obj): ReqQueryGetObtenerObtenerNotificaciones {
    return removeUndefinedFromObj({
      tipoNotificacion: hasValue(obj.tipoNotificacion) ? '' + obj.tipoNotificacion : undefined // ESTADO_SINIESTRO, PAGO, RENOVACION, MENSAJE, SERVICIO
    });
  }
}

export class ReqPathGetComunObtenerComercio {
  public codigoPasarela: string;
  public codigoApp: string;

  public static create(obj): ReqPathGetComunObtenerComercio {
    return removeUndefinedFromObj({
      codigoPasarela: hasValue(obj.codigoPasarela) ? '' + obj.codigoPasarela : undefined, // [object Object], [object Object]
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathPostComunGenerarOrdenCompra {
  public codigoTarjeta: string;
  public codigoApp: string;

  public static create(obj): ReqPathPostComunGenerarOrdenCompra {
    return removeUndefinedFromObj({
      codigoTarjeta: hasValue(obj.codigoTarjeta) ? '' + obj.codigoTarjeta : undefined, // [object Object], [object Object], [object Object], [object Object]
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostMcProcesosGenerarFirma {
  public codigoApp?: string;
  public codigoComercio?: number;
  public codigPais?: string;
  public numeroReferencia?: string;
  public montoTransaccion?: number;
  public monedaTransaccion?: string;
  public fechaTransaccion?: string;
  public horaTransaccion?: string;
  public autogenerado?: string;
  public codigoCliente?: string;

  public static create(obj): ReqBodyPostMcProcesosGenerarFirma {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      codigoComercio: hasValue(obj.codigoComercio) ? +obj.codigoComercio : undefined,
      codigPais: hasValue(obj.codigPais) ? '' + obj.codigPais : undefined, // PER
      numeroReferencia: hasValue(obj.numeroReferencia) ? '' + obj.numeroReferencia : undefined,
      montoTransaccion: hasValue(obj.montoTransaccion) ? +obj.montoTransaccion : undefined,
      monedaTransaccion: hasValue(obj.monedaTransaccion) ? '' + obj.monedaTransaccion : undefined, // USD, PEN
      fechaTransaccion: hasValue(obj.fechaTransaccion) ? '' + obj.fechaTransaccion : undefined,
      horaTransaccion: hasValue(obj.horaTransaccion) ? '' + obj.horaTransaccion : undefined,
      autogenerado: hasValue(obj.autogenerado) ? '' + obj.autogenerado : undefined,
      codigoCliente: hasValue(obj.codigoCliente) ? '' + obj.codigoCliente : undefined
    });
  }
}

export class ReqBodyPostMcProcesosGrabarTransaccionPago {
  public resultadoTransaccion?: string;
  public codigoAutorizacion?: string;
  public numeroReferencia?: string;
  public numeroCuota?: number;
  public fechaPrimeraCuota?: string;
  public monedaCuota?: string;
  public montoCuota?: number;
  public monedaMontoTotal?: string;
  public montoTotal?: number;
  public fechaTransaccion?: string;
  public horaTransaccion?: string;
  public codigoRespuesta?: string;
  public codigPais?: string;
  public numeroTarjeta?: string;
  public codigoSeguridad?: number;
  public mensajeRespuesta?: string;
  public codigoCliente?: string;
  public codigoPaisTxn?: string;
  public firma?: string;

  public static create(obj): ReqBodyPostMcProcesosGrabarTransaccionPago {
    return removeUndefinedFromObj({
      resultadoTransaccion: hasValue(obj.resultadoTransaccion) ? '' + obj.resultadoTransaccion : undefined,
      codigoAutorizacion: hasValue(obj.codigoAutorizacion) ? '' + obj.codigoAutorizacion : undefined,
      numeroReferencia: hasValue(obj.numeroReferencia) ? '' + obj.numeroReferencia : undefined,
      numeroCuota: hasValue(obj.numeroCuota) ? +obj.numeroCuota : undefined,
      fechaPrimeraCuota: hasValue(obj.fechaPrimeraCuota) ? '' + obj.fechaPrimeraCuota : undefined,
      monedaCuota: hasValue(obj.monedaCuota) ? '' + obj.monedaCuota : undefined, // USD, PEN
      montoCuota: hasValue(obj.montoCuota) ? +obj.montoCuota : undefined,
      monedaMontoTotal: hasValue(obj.monedaMontoTotal) ? '' + obj.monedaMontoTotal : undefined, // USD, PEN
      montoTotal: hasValue(obj.montoTotal) ? +obj.montoTotal : undefined,
      fechaTransaccion: hasValue(obj.fechaTransaccion) ? '' + obj.fechaTransaccion : undefined,
      horaTransaccion: hasValue(obj.horaTransaccion) ? '' + obj.horaTransaccion : undefined,
      codigoRespuesta: hasValue(obj.codigoRespuesta) ? '' + obj.codigoRespuesta : undefined,
      codigPais: hasValue(obj.codigPais) ? '' + obj.codigPais : undefined, // PER
      numeroTarjeta: hasValue(obj.numeroTarjeta) ? '' + obj.numeroTarjeta : undefined,
      codigoSeguridad: hasValue(obj.codigoSeguridad) ? +obj.codigoSeguridad : undefined,
      mensajeRespuesta: hasValue(obj.mensajeRespuesta) ? '' + obj.mensajeRespuesta : undefined,
      codigoCliente: hasValue(obj.codigoCliente) ? '' + obj.codigoCliente : undefined,
      codigoPaisTxn: hasValue(obj.codigoPaisTxn) ? '' + obj.codigoPaisTxn : undefined, // PER
      firma: hasValue(obj.firma) ? '' + obj.firma : undefined
    });
  }
}

export class ReqPathGetMcProcesosVerificarTransaccionPago {
  public numeroReferencia: string;

  public static create(obj): ReqPathGetMcProcesosVerificarTransaccionPago {
    return removeUndefinedFromObj({
      numeroReferencia: hasValue(obj.numeroReferencia) ? '' + obj.numeroReferencia : undefined
    });
  }
}

export class ReqBodyPostVisaNetGrabarTransaccionPago {
  public codigoToken?: string;
  public codigoTransaccion?: string;

  public static create(obj): ReqBodyPostVisaNetGrabarTransaccionPago {
    return removeUndefinedFromObj({
      codigoToken: hasValue(obj.codigoToken) ? '' + obj.codigoToken : undefined,
      codigoTransaccion: hasValue(obj.codigoTransaccion) ? '' + obj.codigoTransaccion : undefined
    });
  }
}

export class ReqBodyPostVisaNetIniciarSesion {
  public codigoComercio?: number;
  public montoTransaccion?: number;

  public static create(obj): ReqBodyPostVisaNetIniciarSesion {
    return removeUndefinedFromObj({
      codigoComercio: hasValue(obj.codigoComercio) ? +obj.codigoComercio : undefined,
      montoTransaccion: hasValue(obj.montoTransaccion) ? +obj.montoTransaccion : undefined
    });
  }
}

export class ReqPathGetObtenerParametrosGenerales {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerParametrosGenerales {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqPathGetObtenerEstadoDespliegue {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerEstadoDespliegue {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetObtenerEstadoDespliegue {
  public plataforma: string;

  public static create(obj): ReqQueryGetObtenerEstadoDespliegue {
    return removeUndefinedFromObj({
      plataforma: hasValue(obj.plataforma) ? '' + obj.plataforma : undefined // ANDROID, IOS
    });
  }
}

export class ReqPathGetObtenerTiposDocumento {
  public seccion: string;

  public static create(obj): ReqPathGetObtenerTiposDocumento {
    return removeUndefinedFromObj({
      seccion: hasValue(obj.seccion) ? '' + obj.seccion : undefined // cotizacion, asegurados, contratante, resumen, pago, SCC, SCO, APC, MOB
    });
  }
}

export class ReqPathGetListarUbigeoProvincias {
  public departamentoId: string;

  public static create(obj): ReqPathGetListarUbigeoProvincias {
    return removeUndefinedFromObj({
      departamentoId: hasValue(obj.departamentoId) ? '' + obj.departamentoId : undefined
    });
  }
}

export class ReqPathGetListarUbigeoDistritos {
  public provinciaId: string;

  public static create(obj): ReqPathGetListarUbigeoDistritos {
    return removeUndefinedFromObj({
      provinciaId: hasValue(obj.provinciaId) ? '' + obj.provinciaId : undefined
    });
  }
}

export class ReqQueryGetObtenerUbicacionGoogle {
  public input: string;
  public key: string;
  public types?: string;
  public language?: string;

  public static create(obj): ReqQueryGetObtenerUbicacionGoogle {
    return removeUndefinedFromObj({
      input: hasValue(obj.input) ? '' + obj.input : undefined,
      key: hasValue(obj.key) ? '' + obj.key : undefined,
      types: hasValue(obj.types) ? '' + obj.types : undefined,
      language: hasValue(obj.language) ? '' + obj.language : undefined
    });
  }
}

export class ReqPathGetObtenerContactoPais {
  public estado: string;

  public static create(obj): ReqPathGetObtenerContactoPais {
    return removeUndefinedFromObj({
      estado: hasValue(obj.estado) ? '' + obj.estado : undefined // S, N
    });
  }
}

export class ReqPathGetObtenerFcm {
  public codigoApp: string;
  public usuario: string;

  public static create(obj): ReqPathGetObtenerFcm {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      usuario: hasValue(obj.usuario) ? '' + obj.usuario : undefined
    });
  }
}

export class ReqPathPostGenerarFcmApp {
  public codigoApp: string;

  public static create(obj): ReqPathPostGenerarFcmApp {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostGenerarFcmApp {
  public usuarioLogin?: string;
  public deviceKey?: string;

  public static create(obj): ReqBodyPostGenerarFcmApp {
    return removeUndefinedFromObj({
      usuarioLogin: hasValue(obj.usuarioLogin) ? '' + obj.usuarioLogin : undefined,
      deviceKey: hasValue(obj.deviceKey) ? '' + obj.deviceKey : undefined
    });
  }
}

export class ReqPathGetObtenerFcmServer {
  public codigoApp: string;
  public usuario: string;

  public static create(obj): ReqPathGetObtenerFcmServer {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      usuario: hasValue(obj.usuario) ? '' + obj.usuario : undefined
    });
  }
}

export class ReqPathPostGenerarFcmAppServer {
  public codigoApp: string;

  public static create(obj): ReqPathPostGenerarFcmAppServer {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostGenerarFcmAppServer {
  public usuarioLogin?: string;
  public deviceKey?: string;

  public static create(obj): ReqBodyPostGenerarFcmAppServer {
    return removeUndefinedFromObj({
      usuarioLogin: hasValue(obj.usuarioLogin) ? '' + obj.usuarioLogin : undefined,
      deviceKey: hasValue(obj.deviceKey) ? '' + obj.deviceKey : undefined
    });
  }
}

export class ReqPathPostEnviarFcmAppServer {
  public codigoApp: string;

  public static create(obj): ReqPathPostEnviarFcmAppServer {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostEnviarFcmAppServer {
  public usuarioLogin?: string;
  public deviceKey?: string;

  public static create(obj): ReqBodyPostEnviarFcmAppServer {
    return removeUndefinedFromObj({
      usuarioLogin: hasValue(obj.usuarioLogin) ? '' + obj.usuarioLogin : undefined,
      deviceKey: hasValue(obj.deviceKey) ? '' + obj.deviceKey : undefined
    });
  }
}

export class ReqPathGetObtenerFunciones {
  public codigoApp: string;

  public static create(obj): ReqPathGetObtenerFunciones {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqQueryGetObtenerTerminosCondicionesCliente {
  public codigoApp: string;
  public modulo?: string;
  public estado?: string;

  public static create(obj): ReqQueryGetObtenerTerminosCondicionesCliente {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined, // SCC, SCO, APC, MOB, ADP
      modulo: hasValue(obj.modulo) ? '' + obj.modulo : undefined, // CDM, PAGOS
      estado: hasValue(obj.estado) ? '' + obj.estado : undefined // APROBADO, POR_APROBAR
    });
  }
}

export class ReqPathPutAprobarTerminosCondiciones {
  public id: string;

  public static create(obj): ReqPathPutAprobarTerminosCondiciones {
    return removeUndefinedFromObj({
      id: hasValue(obj.id) ? '' + obj.id : undefined
    });
  }
}

export class ReqQueryPutAprobarTerminosCondiciones {
  public codigoApp: string;

  public static create(obj): ReqQueryPutAprobarTerminosCondiciones {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPutAprobarTerminosCondiciones {
  public id?: string;
  public descripcion?: string;
  public modulo?: string;
  public estado?: string;

  public static create(obj): ReqBodyPutAprobarTerminosCondiciones {
    return removeUndefinedFromObj({
      id: hasValue(obj.id) ? '' + obj.id : undefined,
      descripcion: hasValue(obj.descripcion) ? '' + obj.descripcion : undefined,
      modulo: hasValue(obj.modulo) ? '' + obj.modulo : undefined, // CDM, PAGOS
      estado: hasValue(obj.estado) ? '' + obj.estado : undefined // APROBADO, POR_APROBAR
    });
  }
}

export class ReqQueryPostGuardarMovimiento {
  public codigoApp: string;

  public static create(obj): ReqQueryPostGuardarMovimiento {
    return removeUndefinedFromObj({
      codigoApp: hasValue(obj.codigoApp) ? '' + obj.codigoApp : undefined // SCC, SCO, APC, MOB, ADP
    });
  }
}

export class ReqBodyPostGuardarMovimiento {
  public modulo?: string;
  public proceso?: string;
  public descripcion?: string;

  public static create(obj): ReqBodyPostGuardarMovimiento {
    return removeUndefinedFromObj({
      modulo: hasValue(obj.modulo) ? '' + obj.modulo : undefined, // CDM
      proceso: hasValue(obj.proceso) ? '' + obj.proceso : undefined, // SERVICIOS, LANDING, AUTOEVALUADOR, PRUEBA_COVID, VIDEO_LLAMADA_MAPFREDOC, CHAT_DOCTOR, AGENDAMIENTO_ESPECIALISTA(COVID), DELIVERY_MEDICAMENTO, MEDICO_DOMICILIO(COVID), AMBULANCIA, CONSULTA_CLINICA, CONTRATACION
      descripcion: hasValue(obj.descripcion) ? '' + obj.descripcion : undefined // INICIO, FIN, EVALUACION_INICIO, EVALUACION_FIN, COTIZACION_INICIO, COTIZACION_FIN, TENGO_CUENTA, CREAR_CUENTA, CONTINUAR, CANCELAR, LLAMADA_TELEFONO, LLAMADA_WHATSAPP
    });
  }
}

export default '';
