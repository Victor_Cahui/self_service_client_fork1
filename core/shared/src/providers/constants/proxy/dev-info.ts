// tslint:disable
export const deploy = {
  commitDate: `2020-6-12--12-23-23`,
  commitMsg: `feat(proxies): upload generated templates`,
  deployDate: `2020-6-12--12-39-38`,
  developer: `kevinrodbemx`,
  env: `pre`,
  hash: `7bb3e40`,
  isDevMpf: false,
  isProd: false,
  pc: `kevin’s MacBook Pro`,
  version: `3.1.1.r`
};

// tslint:disable: ban-ts-ignore
// @ts-ignore
window.dev = { ...deploy };
