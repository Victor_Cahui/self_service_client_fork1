// HACK: if its barril used, generate circular dependencie
import { STATE_KEY } from '@mx/core/shared/state/app/app.state';
import { constants, deployPc, gitDescribe, gitLog } from './proxy';

// HACK: usado para el descriptor
export const appConstants = { ...constants };
export const dev = {
  ...gitLog,
  ...gitDescribe,
  ...deployPc
};
export const prefixKeyApp = 'auweb_';

export const keysStateStorage = {
  app: `${prefixKeyApp + STATE_KEY}`
};

// tslint:disable: ban-ts-ignore
// @ts-ignore
window.dev = { ...dev };
