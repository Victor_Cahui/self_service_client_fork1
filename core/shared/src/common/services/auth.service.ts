import { Injectable } from '@angular/core';
import { StorageService } from '@mx/core/shared/helpers/util/storage-manager';
import { KEY_EXPIRES_IN, KEY_USER_TOKEN } from '@mx/settings/auth/auth-values';

@Injectable()
export class AuthService {
  private readonly IsAuthentication: Boolean = false;
  private readonly UserMail: String = '';
  private readonly AccessTocken: String = '';
  private readonly Claims;

  constructor(private readonly storageService: StorageService) {
    this.init();
  }

  init(): void {}

  setAuthorization(): void {
    // setear datos de configuracion
  }

  /**
   * Recupera el código del Token de Autorización
   */
  getAuthorizationToken(): string {
    return this.storageService.getItem(KEY_USER_TOKEN);
  }

  /**
   * Retorna el tiempo de expiración
   */
  getExpirationTimeToken(): number {
    return +this.storageService.getItem(KEY_EXPIRES_IN);
  }

  /**
   * Verifica si se esta autenticado
   */
  isAuth(): Boolean {
    return this.IsAuthentication;
  }

  /**
   * Obtiene el correo autenticado
   */
  getUserMail(): string {
    return this.UserMail.toString();
  }
}
