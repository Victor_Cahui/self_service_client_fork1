import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ResizeService } from '@mx/core/shared/helpers/events/resize.service';
import { AuthService } from './auth.service';
import { VerifySessionService } from './verify-session.service';

@NgModule({
  imports: [CommonModule, HttpClientModule],
  exports: [],
  declarations: [],
  providers: [AuthService, VerifySessionService, ResizeService]
})
export class ServicesModule {}
