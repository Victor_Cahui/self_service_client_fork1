import { EventEmitter, Injectable } from '@angular/core';
import * as jwt_decode from 'jwt-decode';
import { interval, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class VerifySessionService {
  onSessionExpired: EventEmitter<any> = new EventEmitter<any>();

  constructor(private readonly authService: AuthService) {}

  watchVerifySession(time: number): Observable<boolean> {
    return interval(time * 1000).pipe(map(() => this.verifyTimeToken()));
  }

  verifyTimeToken(token?: string): boolean {
    try {
      const expirationTime = this.authService.getExpirationTimeToken();
      if (expirationTime === 0 && !token) {
        return false;
      }

      const currentTime = Math.round(new Date().getTime() / 1000);

      return expirationTime ? expirationTime > currentTime : jwt_decode(token).exp > currentTime;
    } catch (error) {}

    return false;
  }

  validToken(token: string): boolean {
    try {
      return !!token;
    } catch (error) {
      return false;
    }
  }
}
