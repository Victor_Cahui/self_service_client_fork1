declare let dataLayer: any;

export const GAPushEvent = (eventData: any): void => {
  if (!window['dataLayer']) {
    return void 0;
  }

  dataLayer.push(removeExtraPros(eventData));
};

function removeExtraPros(eventData: any): any {
  // tslint:disable-next-line: no-unused
  const { control, controlAction, dimentions, ...gaProps } = eventData;

  return gaProps;
}
