import { Injectable } from '@angular/core';
import { select, State, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as AllAffiliatesActions from './affiliates.actions';
import { AffiliatesSelectors } from './affiliates.selectors';
import * as AffiliatesState from './affiliates.state';

@Injectable()
export class AffiliatesFacade {
  public readonly getState$: Observable<any> = this._Store.pipe(select(AffiliatesSelectors.getState));

  constructor(private readonly _State: State<any>, private readonly _Store: Store<AffiliatesState.PartialState>) {}

  public getState(): AffiliatesState.State {
    return this._State.getValue().affiliates;
  }

  public AddNroPolicy(nroPolicy): void {
    this._Store.dispatch(new AllAffiliatesActions.AddNroPolicy(nroPolicy));
  }

  public GetAllAffiliates(frm): void {
    this._Store.dispatch(new AllAffiliatesActions.GetAllAffiliates(frm));
  }

  public UpdateAffiliate(affiliate): void {
    this._Store.dispatch(new AllAffiliatesActions.UpdateAffiliate(affiliate));
  }

  public CleanState(): void {
    this._Store.dispatch(new AllAffiliatesActions.CleanState());
  }
}
