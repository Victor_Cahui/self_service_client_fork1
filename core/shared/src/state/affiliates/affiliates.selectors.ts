import { createFeatureSelector, createSelector } from '@ngrx/store';

import * as AffiliatesState from './affiliates.state';
import { mapAffiliatesForUI } from './affiliates.utils';

const getAffiliatesState = createFeatureSelector<AffiliatesState.State>(AffiliatesState.STATE_KEY);

export const { selectAll: selectAllAffiliates } = AffiliatesState.adapter.getSelectors(getAffiliatesState);

export const getState = createSelector(
  getAffiliatesState,
  selectAllAffiliates,
  (state, affiliates) => ({ ...state, listaResultados: mapAffiliatesForUI(affiliates) })
);

export const AffiliatesSelectors = {
  getAffiliatesState,
  getState
};
