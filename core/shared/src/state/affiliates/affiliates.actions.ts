// tslint:disable:max-classes-per-file no-unused-variable max-line-length
import { Action } from '@ngrx/store';

export enum Types {
  AddCustom = '[Affiliates][API] Schedule Detail - AddCustom',
  AddNroPolicy = '[Affiliates][Page] Policy Detail - AddNroPolicy',
  CleanState = '[Affiliates][API] CleanState',
  GetAllAffiliates = '[Affiliates][Page] Affiliates Page - GetAllAffiliates',
  GetAllAffiliatesFailure = '[Affiliates][API] Affiliates Page - GetAllAffiliatesFailure',
  GetAllAffiliatesSuccess = '[Affiliates][API] Affiliates Page - GetAllAffiliatesSuccess',
  UpdateAffiliate = '[Affiliates][Page] Affiliates Page - UpdateAffiliate',
  UpdateAffiliateFailure = '[Affiliates][API] Affiliates Page - UpdateAffiliateFailure',
  UpdateAffiliateSuccess = '[Affiliates][API] Affiliates Page - UpdateAffiliateSuccess'
}

export class AddCustom implements Action {
  public readonly type = Types.AddCustom;
  constructor(public payload: any) {}
}

export class CleanState implements Action {
  public readonly type = Types.CleanState;
}

export class AddNroPolicy implements Action {
  public readonly type = Types.AddNroPolicy;
  constructor(public payload: any) {}
}

export class GetAllAffiliates implements Action {
  public readonly type = Types.GetAllAffiliates;
  constructor(public payload: any) {}
}

export class GetAllAffiliatesSuccess implements Action {
  public readonly type = Types.GetAllAffiliatesSuccess;
  constructor(public payload: any) {}
}

export class GetAllAffiliatesFailure implements Action {
  public readonly type = Types.GetAllAffiliatesFailure;
}

export class UpdateAffiliate implements Action {
  public readonly type = Types.UpdateAffiliate;
  constructor(public payload: any) {}
}

export class UpdateAffiliateSuccess implements Action {
  public readonly type = Types.UpdateAffiliateSuccess;
  constructor(public payload: any) {}
}

export class UpdateAffiliateFailure implements Action {
  public readonly type = Types.UpdateAffiliateFailure;
}

export type Actions =
  | AddCustom
  | AddNroPolicy
  | GetAllAffiliates
  | GetAllAffiliatesFailure
  | GetAllAffiliatesSuccess
  | UpdateAffiliate
  | UpdateAffiliateFailure
  | UpdateAffiliateSuccess
  | CleanState;
