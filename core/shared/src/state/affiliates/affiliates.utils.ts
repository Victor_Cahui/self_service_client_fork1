function getLabelWithoutInfo(data): string {
  return data || 'Sin información';
}

function getObjUpdatedAffiliate(o): any {
  return {
    codAfiliado: o.codAfiliado,
    correo: o.correo,
    telefono: o.telefono
  };
}

function getPropsForUI(a): any {
  return {
    ...a,
    frmNameEmail: `email_${a.nroDocumento}`,
    frmNamePhone: `phone_${a.nroDocumento}`,
    uiEmail: getLabelWithoutInfo(a.correo),
    uiPhone: getLabelWithoutInfo(a.telefono)
  };
}

function getAffiliate(a): any {
  return {
    ...getPropsForUI(a),
    dependientes: a.dependientes.map(getPropsForUI),
    isVisibleDependent: hasMatchingInDependents(a.dependientes)
  };
}

function hasMatchingInDependents(arr = []): boolean {
  return !!arr.find((d = {}) => d.colorearFila);
}

function getAffiliateFrmValue(frm, a): any {
  return {
    ...a,
    correo: frm[a.frmNameEmail],
    telefono: frm[a.frmNamePhone]
  };
}

// public

export function mapAffiliatesForUI(arr: any[]): any[] {
  return arr.map(getAffiliate);
}

export function getUpdatedAffiliate(affiliate, frmHolder, frmDependents): any {
  return {
    ...getAffiliateFrmValue(frmHolder, affiliate),
    dependientes: affiliate.dependientes.map(getAffiliateFrmValue.bind(null, frmDependents))
  };
}

export function getUpdatedAffiliateEPS(affiliate, frmHolder): any {
  return {
    ...getAffiliateFrmValue(frmHolder, affiliate)
  };
}

export function getAffiliateRequest(affiliate): any[] {
  const holder = getObjUpdatedAffiliate(affiliate);
  const dependents = affiliate.dependientes.map(getObjUpdatedAffiliate);

  return [holder, ...dependents];
}

export function getEPSAffiliateRequest(affiliate): any[] {
  const holder = getObjUpdatedAffiliate(affiliate);

  return [holder];
}
