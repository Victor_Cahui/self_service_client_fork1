import { ActionReducer, ActionReducerMap, MetaReducer } from '@ngrx/store';
import { storeFreeze } from 'ngrx-store-freeze';
import { localStorageSync } from 'ngrx-store-localstorage';
import { storeLogger } from 'ngrx-store-logger';

import { prefixKeyApp } from '@mx/core/shared/providers/constants';
import * as AffiliatesState from './affiliates';
import { affiliatesReducer } from './affiliates/affiliates.reducer';
import * as AppState from './app';
import { AUWEBState } from './app.interface';
import { appReducer as appStateReducer } from './app/app.reducer';
import * as AppointmentState from './appointment';
import { appointmentReducer } from './appointment/appointment.reducer';

export function logger(reducer: ActionReducer<AUWEBState>): any {
  return storeLogger()(reducer);
}

export function localStorageSyncReducer(reducer: ActionReducer<AUWEBState>): ActionReducer<AUWEBState> {
  return localStorageSync({
    keys: [AppState.STATE_KEY, AppointmentState.STATE_KEY, AffiliatesState.STATE_KEY],
    rehydrate: true,
    storageKeySerializer: key => `${prefixKeyApp}${key}`
  })(reducer);
}

export const appReducer: ActionReducerMap<AUWEBState> = {
  app: appStateReducer,
  appointment: appointmentReducer,
  affiliates: affiliatesReducer
};

// TODO: implements enviroment and do:
// environment.production ? [] : [logger];
// export const appMetaReducers: Array<MetaReducer<AUWEBState>> = [localStorageSyncReducer];
// tslint:disable-next-line: readonly-array
export const appMetaReducers: Array<MetaReducer<AUWEBState>> = [logger, storeFreeze, localStorageSyncReducer];
