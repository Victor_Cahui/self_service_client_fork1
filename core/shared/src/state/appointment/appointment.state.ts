export const STATE_KEY = 'appointment';

export interface PartialState {
  readonly [STATE_KEY]: State;
}

export const initialState: State = {};

export interface Patient {
  readonly apellidoMaterno?: string;
  readonly apellidoPaterno?: string;
  readonly documento?: string;
  readonly fullName?: string;
  readonly nombreCompleto?: string;
  readonly nombres?: string;
  readonly tipoDocumento?: string;
  readonly topLbl?: string;
}

export interface Doctor {
  readonly cmpDescription?: string;
  readonly codigoColegioMedico?: string;
  readonly doctorId?: number | string;
  readonly especialidad?: string;
  readonly especialidades?: any[];
  readonly nombreCompleto?: string;
  readonly specialitiesDescription?: string;
}

export interface Time {
  readonly description?: string;
  readonly duracionHorario?: number;
  readonly horaFin?: string;
  readonly horaInicio?: string;
  readonly selectedLbl?: string;
  readonly topLbl?: string;
}

export interface Speciality {
  readonly codigoMonedaCopago?: number;
  readonly codigoTipoDeducible?: string;
  readonly especialidadDescripcion: string;
  readonly especialidadId: number;
  readonly montoCopago?: number;
}
export interface State extends Doctor, Time {
  readonly canModify?: boolean;
  readonly canReschedule?: boolean;
  readonly citaId?: number;
  readonly clinicaDescripcion?: string;
  readonly clinicaId?: string;
  readonly codigoMonedaCopago?: number;
  readonly codigoSeps?: string;
  readonly codigoTipoDeducible?: string;
  readonly departamentoDescripcion?: string;
  readonly departamentoId?: string;
  readonly descripcion?: string;
  readonly direccion?: string;
  readonly distancia?: number;
  readonly distritoDescripcion?: string;
  readonly doctor?: Doctor;
  readonly documentoUsuario?: string;
  readonly esFavorito?: boolean;
  readonly especialidad?: any;
  readonly estadoCita?: string;
  readonly fecha?: string;
  readonly frmStep1?: any;
  readonly frmStep2?: any;
  readonly tipoCita?: any;
  readonly fullDirection?: string;
  readonly horarioAtencion?: string;
  readonly imagenLandscapeUrl?: string;
  readonly imagenPortraitUrl?: string;
  readonly isEditable?: boolean;
  readonly latitud?: number;
  readonly longitud?: number;
  readonly montoCopago?: number;
  readonly reagendar?: boolean;
  readonly nombre?: string;
  readonly operation?: string;
  readonly paciente?: Patient;
  readonly paginaWeb?: string;
  readonly poliza?: any;
  readonly provinciaDescripcion?: string;
  readonly provinciaId?: string;
  readonly puedeEliminar?: boolean;
  readonly puedeModificar?: boolean;
  readonly redCodigo?: number;
  readonly redDescripcion?: string;
  readonly tabSelected?: number;
  readonly telefono?: string;
  readonly textoDeducibleCorto?: string;
  readonly time?: Time;
  readonly tipoDocumentoUsuario?: string;
  readonly cellphone?: string;
  readonly pagado?: boolean;
}
