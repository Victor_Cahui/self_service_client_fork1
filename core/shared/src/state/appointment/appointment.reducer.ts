import * as AppointmentActions from './appointment.actions';
import * as AppointmentState from './appointment.state';
import { getAppointmentState, getFullDirection } from './appointment.utils';

// tslint:disable-next-line: cyclomatic-complexity
export function appointmentReducer(
  state: AppointmentState.State = AppointmentState.initialState,
  action: AppointmentActions.Actions
): AppointmentState.State {
  switch (action.type) {
    case AppointmentActions.Types.Modality:
      return { ...state, tipoCita: action.payload };

    case AppointmentActions.Types.AddCellphone:
      return { ...state, cellphone: action.payload };

    case AppointmentActions.Types.AddCustom:
      return { ...state, ...action.payload };

    case AppointmentActions.Types.GetAppointmentForDetailSuccess:
      return { ...state, ...getAppointmentState(action.payload), isEditable: true };

    case AppointmentActions.Types.AddFrmStep1:
      return { ...state, frmStep1: { ...action.payload } };

    case AppointmentActions.Types.AddFrmStep2:
      const { tabSelected, ...frm } = action.payload;

      return { ...state, frmStep2: { ...frm }, tabSelected };

    case AppointmentActions.Types.CleanAppointment:
      return AppointmentState.initialState;

    case AppointmentActions.Types.AddDate:
      return { ...state, ...action.payload };

    case AppointmentActions.Types.AddDoctor:
      return { ...state, doctor: { ...action.payload } };

    case AppointmentActions.Types.AddHour:
      return { ...state, time: { ...action.payload } };

    case AppointmentActions.Types.AddEstablishment:
      return {
        ...state,
        ...action.payload,
        fullDirection: getFullDirection(action.payload)
      };

    case AppointmentActions.Types.AddPolicy:
      return { ...state, poliza: { ...action.payload } };

    case AppointmentActions.Types.AddPatient:
      return { ...state, paciente: { ...action.payload } };

    case AppointmentActions.Types.AddSpeciality:
      return { ...state, especialidad: { ...action.payload } };

    case AppointmentActions.Types.CleanPatient:
      return { ...state, paciente: {} };

    case AppointmentActions.Types.CleanPolicy:
      return { ...state, poliza: {} };

    case AppointmentActions.Types.CleanSpeciality:
      return { ...state, especialidad: {} };

    case AppointmentActions.Types.CleanDate:
      return { ...state, fecha: '' };

    case AppointmentActions.Types.CleanDoctor:
      return { ...state, doctor: {} };

    case AppointmentActions.Types.CleanHour:
      return { ...state, time: {} };

    case AppointmentActions.Types.CleanFrmStep2:
      return { ...state, frmStep2: {}, tabSelected: 0 };

    case AppointmentActions.Types.CleanStep2:
      return { ...state, frmStep2: {}, tabSelected: 0, doctor: {}, time: {}, fecha: '' };

    default:
      return state;
  }
}
