import { StatusAppointment } from '@mx/settings/constants/general-values';

function isInProgress(status: string): boolean {
  return ['ACEPTADO', 'ENESPERA'].includes(status.toUpperCase());
}

function isPast(status: string): boolean {
  return ['RECHAZADO', 'ATENDIDO'].includes(status.toUpperCase());
}

function canReschedule(status: string): boolean {
  return [StatusAppointment.pasada].includes(status.toUpperCase());
}

function getStatus(status: string): string {
  if (isInProgress(status)) {
    return StatusAppointment.pendiente;
  }

  if (isPast(status)) {
    return StatusAppointment.pasada;
  }

  return status;
}

export function getFullDirection(payload): string {
  const { direccion, distritoDescripcion, provinciaDescripcion, departamentoDescripcion } = payload;

  return `${direccion}, ${distritoDescripcion}, ${provinciaDescripcion}, ${departamentoDescripcion}`;
}

export function getAppointmentState(payload): any {
  const {
    apellidoMaternoPaciente,
    apellidoPaternoPaciente,
    canModify,
    citaId,
    clinicaDescripcion,
    clinicaId,
    codigoMonedaCopago,
    codigoTipoDeducible,
    departamentoDescripcion,
    descripcionPoliza,
    direccion,
    distancia,
    distritoDescripcion,
    doctorId,
    documentoPaciente,
    especialidadDescripcion,
    especialidadId,
    estadoCita,
    fecha,
    horaFin,
    horaInicio,
    horarioAtencion,
    imagenLandscapeUrl,
    imagenPortraitUrl,
    latitud,
    longitud,
    montoCopago,
    nombreCompleto,
    nombresPaciente,
    numeroPoliza,
    paginaWeb,
    provinciaDescripcion,
    puedeEliminar,
    puedeModificar,
    reagendar,
    telefono,
    tipoCita,
    tipoDocumentoPaciente,
    pagado
  } = payload;

  return {
    canModify,
    canReschedule: canReschedule(estadoCita),
    citaId,
    clinicaId,
    departamentoDescripcion,
    direccion,
    distancia,
    tipoCita,
    distritoDescripcion,
    estadoCita: getStatus(estadoCita), //
    fullDirection: getFullDirection(payload),
    horarioAtencion,
    imagenLandscapeUrl,
    imagenPortraitUrl,
    latitud,
    longitud,
    puedeEliminar,
    puedeModificar,
    frmStep1: {
      documentoPaciente,
      especialidadId,
      numeroPoliza
    },
    frmStep2: {},
    nombre: clinicaDescripcion,
    paginaWeb,
    provinciaDescripcion,
    reagendar,
    telefono,
    poliza: {
      descripcionPoliza,
      numeroPoliza
    },
    doctor: {
      doctorId,
      nombreCompleto
    },
    fecha,
    time: {
      horaFin,
      horaInicio
    },
    paciente: {
      apellidoMaterno: apellidoMaternoPaciente,
      apellidoPaterno: apellidoPaternoPaciente,
      documento: documentoPaciente,
      nombreCompleto: nombresPaciente,
      nombresPaciente,
      tipoDocumento: tipoDocumentoPaciente,
      topLbl: `${nombresPaciente} ${apellidoMaternoPaciente} ${apellidoPaternoPaciente}`,
      polizas: [
        {
          descripcionPoliza,
          numeroPoliza
        }
      ]
    },
    especialidad: {
      codigoMonedaCopago,
      codigoTipoDeducible,
      especialidadDescripcion,
      especialidadId,
      montoCopago
    },
    pagado
  };
}
