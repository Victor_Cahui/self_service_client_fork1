import { Injectable } from '@angular/core';
import { select, State, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { Actions, ofType } from '@ngrx/effects';
import * as AllAppointmentActions from './appointment.actions';
import { AppointmentSelectors } from './appointment.selectors';
import * as AppointmentState from './appointment.state';

@Injectable()
export class AppointmentFacade {
  public readonly getAppointment$: Observable<any> = this._Store.pipe(select(AppointmentSelectors.getAppointment));
  public readonly getPatient$: Observable<any> = this._Store.pipe(select(AppointmentSelectors.getPatient));
  public readonly getModality$: Observable<any> = this._Store.pipe(select(AppointmentSelectors.getModality));
  public readonly getDoctor$: Observable<any> = this._Store.pipe(select(AppointmentSelectors.getDoctor));
  public readonly getPolicy$: Observable<any> = this._Store.pipe(select(AppointmentSelectors.getPolicy));
  public readonly getSpeciality$: Observable<any> = this._Store.pipe(select(AppointmentSelectors.getSpeciality));
  public readonly getTime$: Observable<any> = this._Store.pipe(select(AppointmentSelectors.getTime));
  public readonly GetAppointmentForDetailSuccess$: Observable<any> = this._Actions.pipe(
    ofType(AllAppointmentActions.Types.GetAppointmentForDetailSuccess)
  );

  constructor(
    private readonly _Actions: Actions,
    private readonly _State: State<any>,
    private readonly _Store: Store<AppointmentState.PartialState>
  ) {}

  public getState(): any {
    return this._State.getValue().appointment;
  }

  public CleanDoctor(): void {
    this._Store.dispatch(new AllAppointmentActions.CleanDoctor());
  }

  public CleanAppointment(): void {
    this._Store.dispatch(new AllAppointmentActions.CleanAppointment());
  }

  public CleanHour(): void {
    this._Store.dispatch(new AllAppointmentActions.CleanHour());
  }

  public CleanDate(): void {
    this._Store.dispatch(new AllAppointmentActions.CleanDate());
  }

  public CleanPatient(): void {
    this._Store.dispatch(new AllAppointmentActions.CleanPatient());
  }

  public CleanSpeciality(): void {
    this._Store.dispatch(new AllAppointmentActions.CleanSpeciality());
  }

  public CleanFrmStep2(): void {
    this._Store.dispatch(new AllAppointmentActions.CleanFrmStep2());
  }

  public AddCellphone(cellphone): void {
    this._Store.dispatch(new AllAppointmentActions.AddCellphone(cellphone));
  }

  public AddSpeciality(speciality): void {
    this._Store.dispatch(new AllAppointmentActions.AddSpeciality(speciality));
  }

  public GetAppointmentForDetail(data): void {
    this._Store.dispatch(new AllAppointmentActions.GetAppointmentForDetail(data));
  }

  public AddPatient(patient): void {
    this._Store.dispatch(new AllAppointmentActions.AddPatient(patient));
  }

  public AddPolicy(policy): void {
    this._Store.dispatch(new AllAppointmentActions.AddPolicy(policy));
  }

  public AddModality(modality): void {
    this._Store.dispatch(new AllAppointmentActions.AddModality(modality));
  }

  public AddDate(fecha: string): void {
    this._Store.dispatch(new AllAppointmentActions.AddDate({ fecha }));
  }

  public AddDoctor(doctor): void {
    this._Store.dispatch(new AllAppointmentActions.AddDoctor(doctor));
  }

  public AddHour(hour: any): void {
    this._Store.dispatch(new AllAppointmentActions.AddHour(hour));
  }

  public AddEstablishment(establishment): void {
    this._Store.dispatch(new AllAppointmentActions.AddEstablishment(establishment));
  }

  public AddCustom(data): void {
    this._Store.dispatch(new AllAppointmentActions.AddCustom(data));
  }

  public AddFrmStep1(data): void {
    this._Store.dispatch(new AllAppointmentActions.AddFrmStep1(data));
  }

  public AddFrmStep2(data): void {
    this._Store.dispatch(new AllAppointmentActions.AddFrmStep2(data));
  }

  public CleanStep2(): void {
    this._Store.dispatch(new AllAppointmentActions.CleanStep2());
  }
}
