import * as AffiliatesState from './affiliates';
import * as AppState from './app';
import * as AppointmentState from './appointment';

export interface AUWEBState {
  readonly [AppState.STATE_KEY]: AppState.State;
  readonly [AppointmentState.STATE_KEY]: AppointmentState.State;
  readonly [AffiliatesState.STATE_KEY]: AffiliatesState.State;
}

export type State = AUWEBState;
