import * as AppActions from './app.actions';
import * as AppState from './app.state';

// tslint:disable-next-line:cyclomatic-complexity
export function appReducer(state: AppState.State = AppState.initialState, action: AppActions.Actions): AppState.State {
  switch (action.type) {
    case AppActions.Types.AddCustomApp:
      return { ...state, ...action.payload };

    case AppActions.Types.GetServerDateSuccess:
      return { ...state, ...action.payload };

    case AppActions.Types.GetServerDateFailure:
      return { ...state };

    case AppActions.Types.CleanState:
      return AppState.initialState;

    case AppActions.Types.LoadLookupsSuccess:
      return { ...state, isLookupLoaded: true };

    case AppActions.Types.LoadLookupsFailure:
      return { ...state, isLookupLoaded: false };

    default:
      return state;
  }
}
