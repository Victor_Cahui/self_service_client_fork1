import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

import { Comun, LookupsProvider } from '@mx/core/shared/providers/services';
import * as AppActions from './app.actions';

@Injectable()
export class AppEffects {
  @Effect()
  public getServerDate$: Observable<Action> = this._Actions$.pipe(
    ofType<AppActions.GetServerDate>(AppActions.Types.GetServerDate),
    switchMap(() =>
      this._Comun.ObtenerDatosServidor().pipe(
        map(r => new AppActions.GetServerDateSuccess(r)),
        catchError(() => of(new AppActions.GetServerDateFailure()))
      )
    ),
    catchError(() => of(new AppActions.GetServerDateFailure()))
  );

  @Effect()
  public LoadLookups$: Observable<Action> = this._Actions$.pipe(
    ofType<AppActions.LoadLookups>(AppActions.Types.LoadLookups),
    switchMap(() =>
      this._LookupsProvider.getForkLookups().pipe(
        tap(r => {
          this._LookupsProvider.setLookups(r);
        }),
        map(() => new AppActions.LoadLookupsSuccess()),
        catchError(() => of(new AppActions.LoadLookupsFailure()))
      )
    ),
    catchError(() => {
      return of(new AppActions.LoadLookupsFailure());
    })
  );

  constructor(
    private readonly _Actions$: Actions,
    private readonly _Comun: Comun,
    private readonly _LookupsProvider: LookupsProvider
  ) {}
}
