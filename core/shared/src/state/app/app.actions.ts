// tslint:disable:max-classes-per-file no-unused-variable max-line-length typedef
// HACK: don't type TYPE because launch error: https://github.com/ngrx/platform/issues/31

import { Action } from '@ngrx/store';

export enum Types {
  AddCustomApp = '[APP][API] AddCustomApp - custom',
  CleanState = '[APP][API] CleanState',

  GetServerDate = '[APP][API] GetServerDate',
  GetServerDateSuccess = '[App][API] GetServerDate Success',
  GetServerDateFailure = '[App][API] GetServerDate Failure',

  LoadLookups = '[APP][API] LoadLookups',
  LoadLookupsSuccess = '[App][API] LoadLookups Success',
  LoadLookupsFailure = '[App][API] LoadLookups Failure'
}

export class AddCustomApp implements Action {
  // HACK: don't type because launch error: https://github.com/ngrx/platform/issues/31
  public readonly type = Types.AddCustomApp;
  constructor(public payload: any) {}
}

export class CleanState implements Action {
  public readonly type = Types.CleanState;
}

export class GetServerDate implements Action {
  public readonly type = Types.GetServerDate;
}

export class GetServerDateSuccess implements Action {
  public readonly type = Types.GetServerDateSuccess;
  constructor(public payload: any) {}
}

export class GetServerDateFailure implements Action {
  public readonly type = Types.GetServerDateFailure;
}

export class LoadLookups implements Action {
  public readonly type = Types.LoadLookups;
}

export class LoadLookupsSuccess implements Action {
  public readonly type = Types.LoadLookupsSuccess;
}

export class LoadLookupsFailure implements Action {
  public readonly type = Types.LoadLookupsFailure;
}

export type Actions =
  | AddCustomApp
  | CleanState
  | GetServerDate
  | GetServerDateSuccess
  | GetServerDateFailure
  | LoadLookups
  | LoadLookupsSuccess
  | LoadLookupsFailure;
