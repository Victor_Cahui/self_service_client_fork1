// HACK: launch error in PartialState
// tslint:disable-next-line: typedef
export const STATE_KEY = 'app';

export interface State {
  readonly fecha?: string;
  readonly hora?: string;
  readonly isLookupLoaded?: boolean;
}

export interface PartialState {
  readonly [STATE_KEY]: State;
}

export const initialState: State = {};
