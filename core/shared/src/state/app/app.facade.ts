import { Injectable } from '@angular/core';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/internal/Observable';

import { keysStateStorage } from '@mx/core/shared/providers/constants';
import * as AppActions from './app.actions';
import * as AppState from './app.state';

@Injectable()
export class AppFacade {
  public GetServerDateSuccess$: Observable<any> = this._Actions.pipe(ofType(AppActions.Types.GetServerDateSuccess));

  constructor(private readonly _Actions: Actions, private readonly _Store: Store<AppState.PartialState>) {}

  public GetServerDate(): void {
    this._Store.dispatch(new AppActions.GetServerDate());
  }

  public CleanState(): void {
    this._Store.dispatch(new AppActions.CleanState());
  }

  public LoadLookups(): void {
    this._Store.dispatch(new AppActions.LoadLookups());
  }

  public getSyncState(): AppState.State {
    return JSON.parse(window.sessionStorage.getItem(keysStateStorage.app)) || {};
  }
}
