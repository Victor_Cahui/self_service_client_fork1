import gulp from 'gulp';
import zip from 'gulp-zip';

import { FILETOZIP } from '../tasks.constants';

gulp.task('zip', function() {
  return gulp
    .src(FILETOZIP.files)
    .pipe(zip(FILETOZIP.getName()))
    .pipe(gulp.dest(FILETOZIP.dest));
});
