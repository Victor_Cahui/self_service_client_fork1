import { writeFile } from './core';

export function getFormattedStringJSON(json) {
  return JSON.stringify(json, null, 2)
    .replace(/"/g, `'`)
    .replace(/( {2}')/g, '  ')
    .replace(/(': )/g, ': ')
    .replace(/(: ')/g, ': `')
    .replace(/(',)/g, '`,')
    .replace(/'\n/g, '`\n');
}

export function getTime(time) {
  const dateObj = time ? new Date(time) : new Date();
  const today = dateObj.toLocaleDateString(undefined, {
    day: 'numeric',
    month: 'numeric',
    year: 'numeric',
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit'
  });
  return today.replace(/[/,:]/g, '-').replace(/ /g, '--');
}

export function isEmptyArray(arrParams = []) {
  return !arrParams.length;
}

export function isEmptyObj(obj = {}) {
  return !Object.keys(obj).length;
}

export function upperFirstChar(word = '') {
  return word.charAt(0).toUpperCase() + word.substr(1);
}

export function saveFile(data, config) {
  return writeFile(process.cwd() + `${config.path}${config.fileName}`, data);
}
