import fs from 'fs';
import mustache from 'mustache';

export function generateFile(tpl, objData) {
  return new Promise(resolve => {
    resolve(mustache.render(tpl.data, objData));
  });
}

export function readFile(file) {
  return new Promise((resolve, reject) => {
    fs.readFile(file, 'utf8', (err, data) => {
      err
        ? reject(err)
        : resolve({
            data
          });
    });
  });
}

export function writeFile(fileName, data) {
  return new Promise((resolve, reject) => {
    fs.writeFile(fileName, data, 'UTF-8', err => {
      err ? reject(err) : resolve(true);
    });
  });
}
