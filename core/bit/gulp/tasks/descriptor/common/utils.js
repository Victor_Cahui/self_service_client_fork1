const dataType = {
  array: 'any[]', // TODO: map model
  Boolean: 'boolean',
  Double: 'number',
  integer: 'number',
  number: 'number', // sometimes has format property with value 'double'
  object: 'object', // TODO: map model
  string: 'string'
};

// const reqBodyTypes = ['body', 'formData'];

export function getDataTypes(obj) {
  return {
    isBoolean: isBoolean(obj.type),
    isNumber: isNumber(obj.type),
    isObject: isObject(obj.type),
    isString: isString(obj.type),
    isArray: isArray(obj.type)
  };
}

export function formatUrl(url = '') {
  return url.replace(/\{/g, '${pathReq.');
}

function isBoolean(typeName) {
  return parseDataType(typeName) === 'boolean';
}

function isString(typeName) {
  return parseDataType(typeName) === 'string';
}

function isObject(typeName) {
  return parseDataType(typeName) === 'object';
}

function isArray(typeName) {
  return parseDataType(typeName) === 'array';
}

function isNumber(typeName) {
  return parseDataType(typeName) === 'number';
}

export function parseDataType(typeName) {
  return dataType[typeName] || 'any';
}

export function getProjects(projects, env) {
  return projects.reduce((acc, cp) => [...acc, { name: [cp.name], url: cp.env[env] || cp.env.default }], []);
}
