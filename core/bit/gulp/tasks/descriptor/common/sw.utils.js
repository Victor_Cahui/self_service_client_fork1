export function hasPathReq(arrParams = []) {
  return arrParams.some(p => p.in === 'path');
}

export function hasBodyOrQueryReq(arrParams = []) {
  return hasBodyReq(arrParams) || hasQueryReq(arrParams);
}

export function isArrayParams(objBody = {}) {
  return !!objBody.schema.items;
}

export function isObjectParams(objBody = {}) {
  return !!objBody.schema.properties;
}

export function hasBodyReq(arrParams = []) {
  return arrParams.some(p => {
    // TODO: debug for type body array task DK-1
    // if (p.in === 'body' && p.schema.type === 'array') {
    //   console.log(p);
    // }
    if (p.in === 'formData') {
      return true;
      // TODO: patch for type body array task DK-1, before are: p.schema.type !== 'array'
    } else if (p.in === 'body' && ['array', 'object'].includes(p.schema.type)) {
      return true;
    }
  });
}

export function hasQueryReq(arrParams = []) {
  return arrParams.some(p => p.in === 'query');
}
