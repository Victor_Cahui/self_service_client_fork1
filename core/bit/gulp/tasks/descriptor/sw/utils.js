import {
  hasBodyOrQueryReq,
  hasBodyReq,
  hasPathReq,
  hasQueryReq,
  isArrayParams,
  isObjectParams
} from '../common/sw.utils';
import { formatUrl, getDataTypes, getProjects, parseDataType } from '../common';
import { generateFile, isEmptyArray, isEmptyObj, upperFirstChar } from '../../core';
import { BASECONFIG } from '../../tasks.constants';

const propCtrl = 'x-swagger-router-controller';
const propsToIgnore = ['x-swagger-router-controller'];

export function getArrDescriptor(configDescrip, objPaths, accP, p) {
  const objPath = objPaths[p];
  const props = Object.keys(objPath);
  const httpMethods = props.filter(pr => !propsToIgnore.includes(pr));

  return [...accP, ...httpMethods.reduce(getArrMethods.bind(null, configDescrip, p, objPath), [])];
}

//

export function generateConstantsFile(tpl, data) {
  const descriptorParse = data.descriptors.map((v, idx, arr) => ({
    ...v,
    appName: v.appName.toLowerCase(),
    comma: arr.length - 1 !== idx
  }));

  return generateFile(tpl, {
    descriptor: descriptorParse,
    projects: getProjects(data.projects, data.BASECONFIG.env)
  });
}

export function generateServicesModelFile(tpl, jsonDescriptor, configDescrip) {
  const arrDescriptorParse = filterDescriptor({ ...jsonDescriptor, configDescrip: configDescrip });

  return generateFile(tpl, {
    config: configDescrip,
    data: arrDescriptorParse
  });
}

function filterDescriptor(descriptor) {
  const descriptorParse = JSON.parse(descriptor.data);
  const { configDescrip } = descriptor;
  const objPaths = descriptorParse.paths;
  const arrPaths = Object.keys(objPaths);
  const newDescriptor = arrPaths.reduce(getArrDescriptor.bind(null, configDescrip, objPaths), []);

  return newDescriptor;
}

//

function getArrMethods(configDescrip, urlPath, objPath, accHM, hm) {
  const objHttpMethod = objPath[hm];
  const operationId = objHttpMethod.operationId;

  return [
    ...accHM,
    {
      appName: configDescrip.appName.toLowerCase(),
      consumes: objHttpMethod.consumes ? objHttpMethod.consumes[0] : '',
      ctrl: objPath[propCtrl],
      description: objHttpMethod.description,
      hasBodyOrQueryReq: hasBodyOrQueryReq(objHttpMethod.parameters),
      hasBodyReq: hasBodyReq(objHttpMethod.parameters),
      hasParameters: !isEmptyArray(objHttpMethod.parameters),
      hasPathReq: hasPathReq(objHttpMethod.parameters),
      hasQueryReq: hasQueryReq(objHttpMethod.parameters),
      httpMethod: hm,
      operationId: upperFirstChar(operationId),
      parameters: getReqParameters('Req', hm, objHttpMethod),
      path: getPath(formatUrl(urlPath), configDescrip),
      produces: objHttpMethod.produces ? objHttpMethod.produces[0] : '',
      response: getRespPlot('Res', hm, objHttpMethod),
      summary: objHttpMethod.summary,
      urlBase: configDescrip.urlBase,
      domainService: getDomainService(configDescrip, operationId)
    }
  ];
}

function getPath(originPath, configDescrip) {
  if (!configDescrip.formatPath) {
    return originPath;
  }

  const envs = Object.keys(configDescrip.formatPath);
  if (!envs.length) {
    return originPath;
  }

  const replacePath = configDescrip.formatPath[BASECONFIG.env];

  return replacePath ? replacePath(originPath) : originPath;
}

function getDomainService(configDescrip, operationId) {
  const appName = configDescrip.appName.toLowerCase();
  const domainAPIs = `appConstants.apis.${appName}`;
  const domainOIM = `appConstants.urlBase.`;
  let newDomain;
  if (!(configDescrip.doChangeDomainWhenEnvIs || []).includes(BASECONFIG.env)) {
    return domainAPIs;
  }

  const domainsListTochange = configDescrip.changeDomainServiceTo;
  const arrNameDomains = Object.keys(domainsListTochange);
  const willChange = arrNameDomains.find(nd => {
    newDomain = domainsListTochange[nd].includes(operationId) && nd;

    return !!newDomain;
  });

  return willChange ? `${domainOIM}${newDomain}` : domainAPIs;
}

function getRespPlot(plotType, httpMethod, objHttpMethod) {
  //! 1
  const response = objHttpMethod.responses || {};
  const schemaCode200 = (response[200] || {}).schema;
  if (!schemaCode200) {
    return {};
  }

  return getObjPLotForResp(plotType, httpMethod, objHttpMethod, schemaCode200);
}

function getReqParameters(plotType, httpMethod, objHttpMethod) {
  return {
    body: getObjPLotForBody('body', plotType, httpMethod, objHttpMethod),
    path: getObjPLot('path', plotType, httpMethod, objHttpMethod),
    query: getObjPLot('query', plotType, httpMethod, objHttpMethod)
  };
}

function getObjPLot(reqType, plotType, httpMethod, objHttpMethod) {
  const arrParams = (objHttpMethod.parameters || []).filter(p => p.in === reqType);

  return {
    modelName: `${upperFirstChar(plotType)}${upperFirstChar(reqType)}${upperFirstChar(httpMethod)}${upperFirstChar(
      objHttpMethod.operationId
    )}`,
    params: mapParameters(arrParams),
    type: reqType
  };
}

function getObjPLotForBody(reqType, plotType, httpMethod, objHttpMethod) {
  const arrParamsBody = (objHttpMethod.parameters || []).filter(p => p.in === 'body');
  const arrParamsFormData = (objHttpMethod.parameters || []).filter(p => p.in === 'formData');
  // TODO: debug for type body array task DK-1
  // const arrParmasArrayBody = (objHttpMethod.parameters || []).filter(p => p.in === 'body' && p.schema.type === 'array');
  if (isEmptyArray(arrParamsBody) && isEmptyArray(arrParamsFormData)) {
    return {};
  }
  if (!isEmptyArray(arrParamsBody)) {
    const objBody = arrParamsBody[0] || {};

    return {
      ...getObjPLot('body', plotType, httpMethod, objHttpMethod),
      isArray: isArrayParams(objBody),
      isObject: isObjectParams(objBody),
      isRequired: objBody.required,
      params: mapParametersForBody(objBody)
    };
  }
  if (!isEmptyArray(arrParamsFormData)) {
    return { ...getObjPLot('formData', plotType, httpMethod, objHttpMethod), isObject: true };
  }
}

function getObjPLotForResp(plotType, httpMethod, objHttpMethod, schema) {
  // TODO: get ARRAY segun OBJ ARRAY del jsonSwagger
  const arrParams = getParams(plotType, httpMethod, objHttpMethod, schema);
  if (isEmptyArray(arrParams)) {
    return {};
  }

  // REQ    QUERY    PUT    ADDClIENT
  return {
    modelName: `${upperFirstChar(plotType)}${upperFirstChar(httpMethod)}${upperFirstChar(objHttpMethod.operationId)}`,
    params: arrParams
  };
}

function getParams(plotType, httpMethod, objHttpMethod, schema) {
  const respType = schema.type; // get si es array u object
  const types = {
    array: mapRespForArray,
    object: mapRespForObj
  };

  return types[plotType] ? types[plotType](schema) : [];
}

function mapRespForObj(schema) {
  //! 3.1
  const objProperties = schema.properties;
  const opKeys = Object.keys(objProperties);

  return opKeys.reduce(getArrRespForObj.bind(null, objProperties), []);
}

function mapRespForArray(schema) {
  //! 3.2
  // objItems
}

function getArrRespForObj(objProperties, acc, kp, idx, arr) {
  //! 4
  const op = objProperties[kp];

  return [
    ...acc,
    {
      comma: arr.length - 1 !== idx,
      description: op.description,
      name: kp,
      type: op.type,
      ...getDataTypes(op)
    }
  ];
}

function mapParametersForBody(objBody = {}) {
  if (isEmptyObj(objBody)) {
    return [];
  }
  // areaPrivada/recibo/correo/{codigoApp}/{numeroRecibo} de au-servi devuelve array
  const arrItems = objBody.schema.items;
  if (isArrayParams(objBody)) {
    return [{ name: objBody.name, type: `${parseDataType(arrItems.type)}[]`, ...getDataTypes(arrItems) }];
  }
  const objProperties = objBody.schema.properties || {};
  const propKeys = Object.keys(objProperties);

  return propKeys.reduce(getArrParamsForBody.bind(null, objProperties), []);
}

function mapParameters(arrParams = []) {
  return arrParams.reduce(getArrParams, []);
}

function getArrParams(acc, op, idx, arr) {
  return [
    ...acc,
    {
      name: op.name,
      isRequired: op.required,
      type: parseDataType(op.type),
      enum: (op.enum || []).map((e, idx, arr) => ({ val: e, hasComma: arr.length - 1 !== idx })),
      hasEnum: !isEmptyArray(op.enum),
      comma: arr.length - 1 !== idx,
      ...getDataTypes(op)
    }
  ];
}

function getArrParamsForBody(objProperties, acc, kp, idx, arr) {
  const op = objProperties[kp] || {};

  return [
    ...acc,
    {
      comma: arr.length - 1 !== idx,
      description: op.description,
      enum: (op.enum || []).map((e, idx, arr) => ({ val: e, hasComma: arr.length - 1 !== idx })),
      hasEnum: !isEmptyArray(op.enum),
      name: kp,
      type: parseDataType(op.type),
      ...getDataTypes(op)
    }
  ];
}
