/* eslint-disable no-console */
import gulp from 'gulp';

import { GITLOG, GITDESCRIBE, DEPLOYPC } from '../tasks.constants';
import { saveFile, generateFile, getFormattedStringJSON, readFile } from '../core';
import { getGitLog } from './git-log';
import { getGitDescribe } from './git-describe';
import { getDeployPc } from './deploy-pc';

gulp.task('wrGitLog', function(done) {
  Promise.all([readFile(GITLOG.tpl), getGitLog()])
    .then(([tpl, gitLog]) =>
      generateFile(tpl, {
        data: getFormattedStringJSON(gitLog)
      })
    )
    .then(tplGenerado => saveFile(tplGenerado, { path: GITLOG.dest, fileName: GITLOG.fileName }))
    .then(() => {
      console.info('-- TERMINADO 🍺🍷 Template de Developer Info 🍷🍺');
      done();
    })
    .catch(err => {
      console.error('-- ERROR ❌❌💩 Al generar Templates de Developer Info 💩❌❌');
      console.error(err);
      done();
    });
});

gulp.task('wrGitDescribe', function(done) {
  Promise.all([readFile(GITDESCRIBE.tpl), getGitDescribe()])
    .then(([tpl, gitDescribe]) =>
      generateFile(tpl, {
        data: getFormattedStringJSON(gitDescribe)
      })
    )
    .then(tplGenerado => saveFile(tplGenerado, { path: GITDESCRIBE.dest, fileName: GITDESCRIBE.fileName }))
    .then(() => {
      console.info('-- TERMINADO 🍺🍷 Template de Git Describe 🍷🍺');
      done();
    })
    .catch(err => {
      console.error('-- ERROR ❌❌💩 Al generar Templates de Git Describe 💩❌❌');
      console.error(err);
      done();
    });
});

gulp.task('wrDeployPC', function(done) {
  Promise.all([readFile(DEPLOYPC.tpl), getDeployPc()])
    .then(([tpl, deployPC]) =>
      generateFile(tpl, {
        data: getFormattedStringJSON(deployPC)
      })
    )
    .then(tplGenerado => saveFile(tplGenerado, { path: DEPLOYPC.dest, fileName: DEPLOYPC.fileName }))
    .then(() => {
      console.info('-- TERMINADO 🍺🍷 Template de Deploy PC 🍷🍺');
      done();
    })
    .catch(err => {
      console.error('-- ERROR ❌❌💩 Al generar Templates de Deploy PC 💩❌❌');
      console.error(err);
      done();
    });
});
