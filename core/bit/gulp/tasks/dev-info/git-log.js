import gitLogToJson from 'git-log-to-json';

import { getTime } from '../core';

export function getGitLog() {
  const opts = {
    limit: 1
  };
  const defaultResp = {
    commitDate: 'NO SE PUDO OBTENER LA INFORMACIÓN DEL COMMIT',
    commitMsg: 'NO SE PUDO OBTENER LA INFORMACIÓN DEL COMMIT',
    developer: 'NO SE PUDO OBTENER LA INFORMACIÓN DEL COMMIT',
    hash: 'NO SE PUDO OBTENER LA INFORMACIÓN DEL COMMIT'
  };
  const defaultPromise = Promise.resolve(defaultResp);

  return process.platform === 'win32'
    ? defaultPromise
    : gitLogToJson('.', opts).then(r => {
        const {
          hash,
          date,
          subject,
          author: { name }
        } = r[0];

        return {
          ...defaultResp,
          commitDate: getTime(date),
          commitMsg: subject,
          developer: name,
          hash: hash.substr(0, 7)
        };
      });
}
