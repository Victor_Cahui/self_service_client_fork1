import gulp from 'gulp';
import stylelint from 'stylelint';
import postcss from 'gulp-postcss';
import reporter from 'postcss-reporter';
import syntax_scss from 'postcss-scss';

import { STYLELINT } from '../tasks.constants';

gulp.task('lint:stylelint', () => {
  var processors = [
    stylelint(),
    reporter({
      clearReportedMessages: true
    })
  ];

  return gulp.src([STYLELINT.src, '!' + STYLELINT.vendor]).pipe(postcss(processors, { syntax: syntax_scss }));
});
