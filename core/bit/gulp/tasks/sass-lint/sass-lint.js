import gulp from 'gulp';
import gLoadPlugins from 'gulp-load-plugins';

import { SASSLINT } from '../tasks.constants';

const $ = gLoadPlugins({ lazy: true });

gulp.task('lint:sass', () => {
  return gulp
    .src(SASSLINT.src)
    .pipe($.sassLint(SASSLINT.options))
    .pipe($.sassLint.format())
    .pipe($.sassLint.failOnError()); // TODO improve formatters
});
