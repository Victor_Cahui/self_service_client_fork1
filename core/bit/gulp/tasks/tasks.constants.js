import * as tasksConstants from '../../../../tasks.constants';

export const BASECONFIG = tasksConstants.BASECONFIG;
export const DESCRIPTOR = tasksConstants.DESCRIPTOR;
export const FILETOZIP = tasksConstants.FILETOZIP;
export const GITLOG = tasksConstants.GITLOG;
export const GITDESCRIBE = tasksConstants.GITDESCRIBE;
export const DEPLOYPC = tasksConstants.DEPLOYPC;
