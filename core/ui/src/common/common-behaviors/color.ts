import { ElementRef } from '@angular/core';

import { Constructor } from './constructor';

export interface CanColor {
  color: ThemePalette;
}

export interface HasElementRef {
  _elementRef: ElementRef;
}

export type ThemePalette = 'primary' | 'outline' | undefined;

// tslint:disable-next-line: only-arrow-functions
export function mixinColor<T extends Constructor<HasElementRef>>(
  base: T,
  defaultColor?: ThemePalette
): Constructor<CanColor> & T {
  return class extends base {
    // tslint:disable-next-line: prefer-readonly
    private _color: ThemePalette;

    get color(): ThemePalette {
      return this._color;
    }
    set color(value: ThemePalette) {
      const colorPalette = value || defaultColor;

      if (colorPalette !== this._color) {
        if (this._color) {
          this._elementRef.nativeElement.classList.remove(`g-btn-${this._color}`);
        }
        if (colorPalette) {
          this._elementRef.nativeElement.classList.add(`g-btn-${colorPalette}`);
        }

        this._color = colorPalette;
      }
    }

    constructor(...args: Array<any>) {
      super(...args);

      this.color = defaultColor;
    }
  };
}
