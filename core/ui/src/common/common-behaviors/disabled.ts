import { ElementRef } from '@angular/core';

import { coerceBooleanProp } from '../helpers';
import { Constructor } from './constructor';

export interface CanDisable {
  disabled: Disabled;
}
export interface HasElementRef {
  _elementRef: ElementRef;
}
export type Disabled = boolean;

// tslint:disable-next-line: only-arrow-functions
export function mixinDisabled<T extends Constructor<HasElementRef>>(base: T): Constructor<CanDisable> & T {
  return class extends base {
    // tslint:disable-next-line: prefer-readonly
    private _disabled: Disabled;

    get disabled(): any {
      return this._disabled;
    }
    set disabled(value: any) {
      // tslint:disable-next-line: comment-type
      /* this._disabled = coerceBooleanProp(value); */
      const disabled = coerceBooleanProp(value);

      if (this._disabled !== disabled) {
        this._elementRef.nativeElement.classList[disabled ? 'add' : 'remove']('disabled');
        this._elementRef.nativeElement.disabled = disabled;

        this._disabled = disabled;
      }
    }

    constructor(...args: Array<any>) {
      super(...args);
    }
  };
}
