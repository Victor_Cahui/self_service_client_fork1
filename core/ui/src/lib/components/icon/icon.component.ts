import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'mf-icon',
  template: `
    <span class="g-items-icon" [ngClass]="getClassIcon(icon, size)"></span>
  `
})
export class IconComponent implements OnInit {
  @Input() icon: string;
  @Input() size: string;

  ngOnInit(): void {}

  getClassIcon(icon, size): Array<String> {
    const codIcon = {
      MD_AUTOS: 'icon-mapfre_019_car',
      MD_SALUD: 'icon-mapfre_020_cross',
      MD_VIDA: 'icon-mapfre_016_heart',
      MD_HOGAR: 'icon-mapfre_017_house',
      MD_DECESOS: 'icon-mapfre_079_decesos2',
      MD_SOAT: 'icon-mapfre_077_soat',
      MD_SCTR: 'icon-mapfre_073_casco'
    };
    const codSize = {
      lg: 'g-font--45'
    };

    return [codIcon[icon] || 'icon-mapfre_019_car', codSize[size] || 'g-font--45'];
  }
}
