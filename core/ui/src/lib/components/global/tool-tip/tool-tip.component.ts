import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'mf-tool-tip',
  templateUrl: './tool-tip.component.html',
  styleUrls: ['./tool-tip.component.scss']
})
export class MfToolTipComponent implements OnInit {
  @Input() title: string;
  @Input() text: string;

  ngOnInit(): void {}
}
