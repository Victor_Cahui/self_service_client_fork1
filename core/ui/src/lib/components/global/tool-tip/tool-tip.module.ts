import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfToolTipComponent } from './tool-tip.component';

@NgModule({
  imports: [CommonModule],
  declarations: [MfToolTipComponent],
  exports: [MfToolTipComponent]
})
export class MfToolTipModule {}
