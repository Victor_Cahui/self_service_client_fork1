import { Component, Input } from '@angular/core';

// @dynamic
@Component({
  selector: 'mf-loader-full-screen',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class MfLoaderComponent {
  @Input() description: string;
  @Input() fullScreen = false;
}
