import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfLoaderComponent } from './loader.component';

@NgModule({
  imports: [CommonModule],
  declarations: [MfLoaderComponent],
  exports: [MfLoaderComponent]
})
export class MfLoaderModule {}
