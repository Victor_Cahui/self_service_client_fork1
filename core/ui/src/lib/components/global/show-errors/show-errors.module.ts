import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfShowErrorsComponent } from './show-errors.component';

@NgModule({
  imports: [CommonModule],
  declarations: [MfShowErrorsComponent],
  exports: [MfShowErrorsComponent]
})
export class MfShowErrorsModule {}
