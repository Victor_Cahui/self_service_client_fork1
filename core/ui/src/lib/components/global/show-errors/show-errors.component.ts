import { Component, Input } from '@angular/core';
import { AbstractControl, AbstractControlDirective } from '@angular/forms';

// @dynamic
@Component({
  selector: 'mf-show-errors',
  templateUrl: './show-errors.component.html',
  styleUrls: ['./show-errors.component.scss']
})
export class MfShowErrorsComponent {
  readonly errorMessages = {
    diff: params => params.msg || 'La contraseña no puede ser igual a la anterior.',
    required: () => 'Este campo es obligatorio.',
    invalidEmail: () => 'El correo electrónico es incorrecto.',
    whitespace: params => `${params}`,
    email: () => 'Ingrese un correo electrónico válido.',
    invalidPhone: () => 'Número telefónico inválido.',
    invalidAmount: () => 'Solo se admiten montos con 2 decimales como máximo.',
    invalidNumber: () => 'Solo se admiten números.',
    invalidPassword: () => 'La contraseña es incorrecta',
    invalidRange: params => `
    El valor tiene que estar entre <span class="text-nowrap">${this.min || params.min}</span> y
     <span class="text-nowrap">${this.max || params.max}</span>.`,
    invalidlLetter: () => 'Solo se admiten letras.',
    invalidAlphanumeric: () => 'Solo se admiten números y letras.',
    invalidWhitespace: () => 'No se admiten espacios en blanco.',
    invalidRUC: () => 'Número de RUC inválido.',
    invalidCard: () => 'Tarjeta inválida.',
    sameDocNumber: () => 'El documento ingresado coincide con el contratante.',
    minlength: params => `El número mínimo de caracteres es ${params.requiredLength}.`,
    minLengthCopy: params => `El número mínimo de caracteres es ${params}.`,
    maxlength: params => `El número máximo de caracteres es ${params.requiredLength}.`,
    min: params => `El valor mínimo es ${params.min}.`,
    max: params => `El valor máximo es ${params.max}.`,
    pattern: () => `El formato ingresado es incorrecto.`,
    matchFields: params => `${this.word} no coincide.`,
    cardExpiredDate: params => `La fecha es inválida.`,
    custom: text => `${text}`
  };

  @Input() private readonly control: AbstractControlDirective | AbstractControl;
  @Input() word: string;
  @Input() min: string;
  @Input() max: string;

  shouldShowErrors(): boolean {
    return (
      (this.control && this.control.errors && (this.control.dirty || this.control.touched)) ||
      (this.control && this.control.invalid && this.control.value !== '')
    );
  }

  listOfErrors(): Array<string> {
    let _errors: Array<any> = [];
    if (this.control.errors) {
      _errors = Object.keys(this.control.errors).map(field => this.getMessage(field, this.control.errors[field]));
    }

    return _errors.length ? [_errors[_errors.length - 1]] : [];
  }

  trackByFn(index, item): any {
    return index;
  }

  protected getMessage(type: string, params: any): string {
    return this.errorMessages[type](params);
  }
}
