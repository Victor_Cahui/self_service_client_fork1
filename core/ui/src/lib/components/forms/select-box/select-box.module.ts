import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MfSelectBoxComponent } from './select-box.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule],
  declarations: [MfSelectBoxComponent],
  exports: [MfSelectBoxComponent]
})
export class MfSelectBoxModule {}
