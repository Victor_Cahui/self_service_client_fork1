export function mapSelectedOpt(arr, optSelected, id): any[] {
  return arr.map(o => ({ ...o, isSelected: o[id] === optSelected[id] }));
}

export const optAllDoctors = {
  cmpDescription: '',
  doctorId: 'default',
  isDefault: true,
  nombreCompleto: 'Todos los doctores',
  specialitiesDescription: 'Se seleccionará un doctor entre los disponibles'
};
