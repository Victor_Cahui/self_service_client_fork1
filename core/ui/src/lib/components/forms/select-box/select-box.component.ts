// tslint:disable: max-file-line-count
import { DOCUMENT } from '@angular/common';
import {
  Component,
  EventEmitter,
  Inject,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { hasNotValidValue, isObjEmpty, isPristine, NumberOrString } from '@mx/core/shared/helpers';
import { dateDiffInHours, dateToStringFormatYYYYMMDD } from '@mx/core/shared/helpers/util/date';
import { UnsubscribeOnDestroy } from '@mx/core/shared/helpers/util/unsubscribe-on-destroy';
import { Autoservicios, Comun } from '@mx/core/shared/providers/services';
import { AuthService } from '@mx/services/auth/auth.service';
import { PoliciesService } from '@mx/services/policies.service';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { mapArrPolicies } from '@mx/views/mapfre-services/schedule-appointment/steps/step-1/schedule-step-1.utils';
import { of } from 'rxjs/internal/observable/of';
import { map } from 'rxjs/internal/operators/map';
import { switchMap } from 'rxjs/internal/operators/switchMap';
import { takeUntil } from 'rxjs/internal/operators/takeUntil';
import { Observable } from 'rxjs/Observable';
import { mapSelectedOpt, optAllDoctors } from './select-box.utils';

interface Config {
  lbl?: string;
  mustSetFirstOpt?: boolean;
  placeholder?: string;
}

@Component({
  selector: 'mf-select-box',
  templateUrl: './select-box.component.html'
})
export class MfSelectBoxComponent extends UnsubscribeOnDestroy implements OnChanges, OnInit, OnDestroy {
  @Input() actionType: string;
  @Input() arrCustom: any[];
  @Input() config: Config = {};
  @Input() dependent1: any;
  @Input() dependent2: any;
  @Input() dependent3: any;
  @Input() dependent4: any;
  @Input() dependent5: any;
  @Input() dataPatient: any;
  @Input() frm: FormGroup;
  @Input() isOpened: boolean;
  @Input() isValid: boolean;
  @Input() name: string;
  @Input() showEnableWhenHasOneItem: boolean;
  @Input() citaCovid: boolean;
  @Output() getLoadingState: EventEmitter<any> = new EventEmitter();
  @Output() getSelectedOpt: EventEmitter<any> = new EventEmitter();
  @Output() getSelectedOptDoctor: EventEmitter<any> = new EventEmitter();

  arrOptions: any[];
  isDisabled: boolean;
  selectedOptForLbl: string;
  private _namePropInResp: any;
  private _isFirstTime = true;

  constructor(
    @Inject(DOCUMENT) private readonly _document: Document,
    private readonly _AuthService: AuthService,
    private readonly _Autoservicios: Autoservicios,
    private readonly _Comun: Comun,
    private readonly _PoliciesService: PoliciesService
  ) {
    super();
  }

  ngOnInit(): void {
    this._initValues();
  }

  ngOnChanges(changes: SimpleChanges): void {
    const actions = this._getActions();
    if (actions[this.actionType]) {
      const selectedValueId = this.frm.value[this.name];
      setTimeout(() => {
        isPristine(changes) || this._resetCbo();
        actions[this.actionType]();
        isPristine(changes) && this._setValueInFrm(selectedValueId);
      });
    }
  }

  ngOnDestroy(): void {
    this._document.removeEventListener('click', this._eventForClickInDocument.bind(this), false);
  }

  open(): void {
    this._closeAllCbos();
    this.isOpened = !this.isOpened;
  }

  selectOption(opt): void {
    this.isOpened = false;
    this.getSelectedOpt.emit({ isFirstTime: this._isFirstTime, ...opt });
    this._setValue(opt);
    this._isFirstTime = false;
  }

  private _getActions(): any {
    return {
      doctorByDate: this._doctorByDate.bind(this),
      doctorByDoctor: this._doctorByDoctor.bind(this),
      documentoPaciente: this._patient.bind(this),
      horaInicio: this._hoursByDate.bind(this),
      horaInicioByDoctor: this._hoursByDoctor.bind(this),
      numeroPoliza: this._policy.bind(this)
    };
  }

  private _getMapping(): any {
    return {
      doctorId: this._mapDoctor.bind(this),
      documentoPaciente: this._mapPatient.bind(this),
      numeroPoliza: this._mapPolicy.bind(this)
    };
  }

  private _setValue(opt): void {
    if (isObjEmpty(opt)) {
      this._resetCbo();

      return void 0;
    }

    this.arrOptions = mapSelectedOpt(this.arrOptions, opt, this._namePropInResp);
    const selectedOption = opt.topLbl ? opt : this._getMapping()[this.name](opt);
    this.selectedOptForLbl = selectedOption.selectedLbl;
    this._setValueInFrm(opt[this._namePropInResp]);
  }

  private _patient(): void {
    this._namePropInResp = 'documento';

    const arrOptions$ = this._PoliciesService.getSortedHealthyPolicies().pipe(
      switchMap((ph: any = []) =>
        this._Autoservicios.ObtenerPacientesXUsuarioLogueado(
          {
            codigoApp: APPLICATION_CODE
          },
          {
            RequestGetPacientesVO: ph.map((p: any) => ({ tipoPoliza: p.tipoPoliza, numeroPoliza: p.numeroPoliza }))
          }
        )
      )
    );

    this._doActionsWhenAsynEnds(arrOptions$);
  }

  private _policy(): void {
    if (!Array.isArray(this.arrCustom)) {
      return void 0;
    }

    this._namePropInResp = 'numeroPoliza';
    this._doActionsWhenAsynEnds(of(mapArrPolicies(this.arrCustom)));
  }

  private _hoursByDate(): void {
    this.getLoadingState.emit({ isLoadingVisible: true });
    if ([this.dependent1, this.dependent2, this.dependent3].some(hasNotValidValue)) {
      return void 0;
    }

    this._namePropInResp = 'horaInicio';
    const selectedDate = dateToStringFormatYYYYMMDD(this.dependent3);
    const serverDateTime$ = this._Comun.ObtenerDatosServidor();
    const hoursByDate$ = this._Autoservicios.ObtenerHorarios(
      {
        clinicaId: this.dependent1,
        codigoApp: APPLICATION_CODE,
        especialidadId: this.dependent2,
        fecha: selectedDate
      },
      {
        tipoCita: this.dependent5 ? this.dependent5 : 'P',
        tipoDocPaciente: this.dataPatient.tipoDocumento,
        docPaciente: this.dataPatient.documento
      }
    );
    const arrOptions$ = serverDateTime$.pipe(
      switchMap(s =>
        hoursByDate$.pipe(
          map((arr: any[]) =>
            arr
              .filter(f => {
                const serverDatetime = `${s.fecha}T${s.hora}`;
                const appointmentDatetime = `${selectedDate}T${f.horaInicio}`;

                return dateDiffInHours(appointmentDatetime, serverDatetime) >= 3;
              })
              .map(this._mapHour.bind(this))
          )
        )
      )
    );
    this._doActionsWhenAsynEnds(arrOptions$);
  }

  private _hoursByDoctor(): void {
    this.getLoadingState.emit({ isLoadingVisible: true });
    if ([this.dependent1, this.dependent2, this.dependent3, this.dependent4].some(hasNotValidValue)) {
      return void 0;
    }

    this._namePropInResp = 'horaInicio';
    const selectedDate = dateToStringFormatYYYYMMDD(this.dependent4);
    const serverDateTime$ = this._Comun.ObtenerDatosServidor();
    const hoursByDate$ = this._Autoservicios.ObtenerHorariosPorDoctor(
      {
        clinicaId: this.dependent1,
        codigoApp: APPLICATION_CODE,
        doctorId: this.dependent3,
        especialidadId: this.dependent2,
        fecha: selectedDate
      },
      {
        tipoCita: this.dependent5 ? this.dependent5 : 'P',
        tipoDocPaciente: this.dataPatient.tipoDocumento,
        docPaciente: this.dataPatient.documento
      }
    );

    const arrOptions$ = serverDateTime$.pipe(
      switchMap(s =>
        hoursByDate$.pipe(
          map((arr: any[]) =>
            arr
              .filter(f => {
                const serverDatetime = `${s.fecha}T${s.hora}`;
                const appointmentDatetime = `${selectedDate}T${f.horaInicio}`;

                return dateDiffInHours(appointmentDatetime, serverDatetime) >= 3;
              })
              .map(this._mapHour.bind(this))
          )
        )
      )
    );
    this._doActionsWhenAsynEnds(arrOptions$);
  }

  private _doctorByDate(): void {
    this.getLoadingState.emit({ isLoadingVisible: true });
    if ([this.dependent1, this.dependent2, this.dependent3, this.dependent4].some(hasNotValidValue)) {
      return void 0;
    }

    this._namePropInResp = 'doctorId';
    const selectedDate = dateToStringFormatYYYYMMDD(this.dependent3);
    const doctors$: Observable<any> = this._Autoservicios.ObtenerDoctorPorFechaYHora(
      {
        codigoApp: APPLICATION_CODE,
        clinicaId: this.dependent1,
        especialidadId: this.dependent2,
        fecha: selectedDate,
        horaInicio: this.dependent4
      },
      {
        tipoCita: this.dependent5 ? this.dependent5 : 'P'
      }
    );

    const arrOptions$ = doctors$.pipe(
      map(m => {
        const doctors = m.map(u => ({
          ...u,
          cmpDescription: `CMP: ${u.codigoColegioMedico}`,
          specialitiesDescription: u.especialidades.map(e => e.especialidadDescripcion).join(', ')
        }));

        return doctors;
      })
    );
    this._doActionsWhenAsynEnds(arrOptions$);
  }

  private _doctorByDoctor(): void {
    if ([this.dependent1, this.dependent2].some(hasNotValidValue)) {
      return void 0;
    }

    this._namePropInResp = 'doctorId';
    const doctors$ = this._Autoservicios.ObtenerDoctores(
      {
        codigoApp: APPLICATION_CODE,
        clinicaId: this.dependent1,
        especialidadId: this.dependent2
      },
      {
        tipoCita: this.dependent5 ? this.dependent5 : 'P'
      }
    );

    const arrOptions$ = doctors$.pipe(
      map(arr =>
        arr.map(d => ({
          ...d,
          cmpDescription: `CMP: ${d.codigoColegioMedico}`,
          specialitiesDescription: d.especialidades.map(e => e.especialidadDescripcion).join(', ')
        }))
      )
    );
    this._doActionsWhenAsynEnds(arrOptions$);
  }

  private _mapPatient(obj): any {
    return {
      ...obj,
      bottomLbl: `${obj.tipoDocumento}: ${obj.documento}`,
      selectedLbl: `${obj.nombreCompleto}`,
      topLbl: `${obj.nombreCompleto}`
    };
  }

  private _mapPolicy(obj): any {
    return {
      ...obj,
      bottomLbl: `Nro. de póliza: ${obj.numeroPoliza}`,
      selectedLbl: `${obj.descripcionPoliza} - ${obj.numeroPoliza}`,
      topLbl: `${obj.descripcionPoliza} - ${obj.numeroPoliza}`
    };
  }

  private _mapDoctor(obj): any {
    return {
      ...obj,
      bottomLbl: `${obj.specialitiesDescription} ${obj.cmpDescription ? '/' : ''} ${obj.cmpDescription}`,
      selectedLbl: obj.isDefault ? optAllDoctors.nombreCompleto : `${obj.nombreCompleto}`,
      topLbl: obj.isDefault ? optAllDoctors.nombreCompleto : `${obj.nombreCompleto}`
    };
  }

  private _mapHour(obj, index: number): any {
    return { ...obj, id: index, selectedLbl: `${obj.horaInicio}`, topLbl: `${obj.horaInicio} - ${obj.horaFin}` };
  }

  private _doActionsWhenAsynEnds(obs$: Observable<any>): void {
    obs$.pipe(takeUntil(this.unsubscribeDestroy$)).subscribe(
      (arr: any[] = []) => {
        this.getLoadingState.emit({ isLoadingVisible: false });
        this.isValid = true;
        if (arr && !arr.length) {
          this.arrOptions = [];

          return void 0;
        }

        const mappings = this._getMapping();
        this.arrOptions = mappings[this.name] ? arr.map(mappings[this.name].bind(this)) : [...arr];

        this.isDisabled = false;
        setTimeout(() => {
          if (!this.frm.value[this.name]) {
            this.config.mustSetFirstOpt && this._setFirstOpt(this.arrOptions);

            if (this.citaCovid) {
              this.getSelectedOptDoctor.emit(this.arrOptions);
            }

            return void 0;
          }
          const opt = this._getSelectedOptByFrm();
          this.selectOption(opt);
        });
        // tslint:disable-next-line: no-unbound-method
      },
      () => {
        this.getLoadingState.emit({ isLoadingVisible: false });
        this.arrOptions = [];
        this.isValid = true;
      }
    );
  }

  private _getSelectedOptByFrm(): any {
    return this.arrOptions.find(o => `${o[this._namePropInResp]}` === `${this.frm.value[this.name]}`);
  }

  private _setFirstOpt(arr: any[] = []): void {
    if (arr.length > 1) {
      return void 0;
    }

    const opt = arr[0];
    this.isDisabled = !this.showEnableWhenHasOneItem && true;
    this.selectOption(opt);
  }

  private _setValueInFrm(selectedValueId: NumberOrString): void {
    if (hasNotValidValue(selectedValueId)) {
      return void 0;
    }

    this.frm.patchValue({
      [this.name]: selectedValueId || ''
    });
  }

  private _resetCbo(): void {
    this.frm.patchValue({
      [this.name]: ''
    });
    this.arrOptions = undefined;
  }

  private _closeOnOutSideClick(): void {
    this._document.addEventListener('click', this._eventForClickInDocument.bind(this), false);
  }

  private _eventForClickInDocument(e: any): void {
    const clic: any = e.target;
    if (!clic.closest('.js-c-dropdown-custom')) {
      this.isOpened = false;
    }
  }

  private _closeAllCbos(): void {
    const cbos: any = this._document.querySelectorAll('.c-dropdown-custom__body');
    cbos.forEach(c => {
      c.classList.remove('active');
    });
  }

  private _initValues(): void {
    this.selectedOptForLbl = this.config.lbl;
    this._closeOnOutSideClick();
  }
}
