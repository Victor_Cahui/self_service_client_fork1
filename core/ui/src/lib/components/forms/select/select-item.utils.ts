export function mapSelectedOpt(arr, optSelected, id): any[] {
  return arr.map(o => ({ ...o, isSelected: o[id] === optSelected[id] }));
}

export const optAllDoctors = {
  cmpDescription: '',
  doctorId: 'default',
  isDefault: true,
  nombreCompleto: 'Todos los doctores',
  specialitiesDescription: 'Se seleccionará un doctor entre los disponibles'
};

export function _mapHour(obj, index: number): any {
  return { ...obj, id: index, description: `${obj.horaInicio} - ${obj.horaFin}` };
}
