import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MfSelectComponent } from './select.component';

@NgModule({
  imports: [CommonModule, FormsModule],
  declarations: [MfSelectComponent],
  exports: [MfSelectComponent]
})
export class MfSelectModule {}
