export class SelectListItem {
  text: string;
  value: any;
  selected = false;
  disabled = false;

  group?: SelectListGroup;

  constructor(item?: any) {
    if (item.text) {
      this.text = item.text;
    }
    if (item.value) {
      this.value = item.value;
    }
    if (item.selected) {
      this.selected = item.selected;
    }
    if (item.disabled) {
      this.disabled = item.disabled;
    }
    if (item.group) {
      this.group = item.group;
    }
  }
}

export class SelectListGroup {
  text: string;
  disabled = false;

  constructor(text: string, disabled: boolean = false) {
    this.text = text;
    this.disabled = disabled;
  }
}
