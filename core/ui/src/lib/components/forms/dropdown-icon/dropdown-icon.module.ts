import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfDropdownIconComponent } from './dropdown-icon.component';

@NgModule({
  imports: [CommonModule],
  declarations: [MfDropdownIconComponent],
  exports: [MfDropdownIconComponent]
})
export class MfDropdownIconModule {}
