import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfDropdownComponent } from './dropdown.component';

@NgModule({
  imports: [CommonModule],
  declarations: [MfDropdownComponent],
  exports: [MfDropdownComponent]
})
export class MfDropdownModule {}
