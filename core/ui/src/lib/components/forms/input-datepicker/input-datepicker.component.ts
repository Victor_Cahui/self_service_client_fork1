import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostBinding,
  Input,
  OnChanges,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { ControlValueAccessor, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { hasNotValidValue, isPristine } from '@mx/core/shared/helpers';
import { getParsedStringDate } from '@mx/core/shared/helpers/util/date';
import { FormUtils } from '@mx/core/shared/helpers/util/form';
import { UnsubscribeOnDestroy } from '@mx/core/shared/helpers/util/unsubscribe-on-destroy';
import { Autoservicios } from '@mx/core/shared/providers/services';
import { APPLICATION_CODE } from '@mx/settings/constants/general-values';
import { combineLatest } from 'rxjs/internal/observable/combineLatest';
import { timer } from 'rxjs/internal/observable/timer';
import { map } from 'rxjs/internal/operators/map';
import { Observable } from 'rxjs/Observable';
import { Size } from '../../../../common';
import { coerceBooleanProp } from '../../../../common/helpers';

@Component({
  selector: 'mf-input-datepicker',
  templateUrl: './input-datepicker.component.html',
  styleUrls: ['./input-datepicker.component.scss'],
  providers: [
    // tslint:disable-next-line:no-forward-ref
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => MfInputDatepickerComponent), multi: true }
  ]
})
export class MfInputDatepickerComponent extends UnsubscribeOnDestroy
  implements AfterViewInit, ElementRef, ControlValueAccessor, OnChanges, OnInit {
  nativeElement: any;
  @ViewChild('mfInputDatepickerComponent', { read: ElementRef })
  input_native: ElementRef;

  @ViewChild('container', { read: ElementRef })
  container: ElementRef;

  @HostBinding('attr.currency') currency: boolean;

  @HostBinding('attr.max') max: string;

  @HostBinding('attr.min') min: string;

  @HostBinding('attr.maxlength') maxlength_host: string;

  @HostBinding('attr.minlength') minlength_host: string;
  @Input() frm: FormGroup;
  @Output() getLoadingState: EventEmitter<any> = new EventEmitter();
  @Input() name: string;
  @Input() minD: Date;
  @Input() maxD: Date;
  @Input()
  public dependent1: any;
  @Input()
  public dependent2: any;
  @Input()
  public dependent3: any;
  @Input()
  public dependent5: any;
  @Input() actionType: string;
  @Input() filter: any;
  get maxlength(): string {
    return this._maxlength;
  }
  set maxlength(maxlength: string) {
    this._maxlength = maxlength;
  }
  protected _maxlength: string;

  @Input()
  get minlength(): string {
    return this._minlength;
  }
  set minlength(minlength: string) {
    this._minlength = minlength;
  }
  protected _minlength: string;

  @Input() OnlyNumber: boolean;

  @Input()
  get value(): string {
    return this._value;
  }
  set value(value: string) {
    this._value = value;
    if (!this._disabled) {
      this.propagateChange(this.value);
    }
  }
  protected _value;

  @Input()
  get type(): string {
    return this._type;
  }
  set type(type: string) {
    this._type = type || 'text';
  }
  protected _type = 'text';

  @Input() size: Size = 'md';
  @Input()
  get label(): string {
    return this._label;
  }
  set label(label: string) {
    this._label = label;
    if (!this._label) {
      this._isLabelHidden = true;
    }
  }
  protected _label: string;

  @Input()
  get placeholder(): string {
    return this._label;
  }
  set placeholder(placeholder: string) {
    this.label = placeholder; // analizar
  }

  @Input()
  get isLabelHidden(): boolean {
    return this._isLabelHidden;
  }
  set isLabelHidden(hidden: boolean) {
    this._isLabelHidden = hidden;
  }
  protected _isLabelHidden = false;

  @Input()
  get disabled(): boolean {
    return this._disabled;
  }
  set disabled(value: boolean) {
    const disabled = coerceBooleanProp(value);
    if (this._disabled !== disabled) {
      FormUtils.disabledClass(this.container, disabled);
      this._disabled = disabled;
    }
    // Browsers may not fire the blur event if the input is disabled too quickly.
    // Reset from here to ensure that the element doesn't become stuck.
    // tslint:disable-next-line: comment-type
    /*if (this.focused) {
      this.focused = false;
      this.stateChanges.next();
    }*/
  }
  protected _disabled = false;
  validDates: any[];

  @Input()
  get error(): boolean {
    return this._error;
  }
  set error(value: boolean) {
    this._error = coerceBooleanProp(value);
  }
  protected _error = false;

  @Input()
  get maskcurrency(): boolean {
    return this._maskCurrency;
  }
  set maskcurrency(_value: boolean) {
    // this._maskCurrency = coerceBooleanProp(value);
  }
  protected _maskCurrency = false;

  @Output()
  public onblur: EventEmitter<string> = new EventEmitter<string>();

  constructor(private readonly _Autoservicios: Autoservicios) {
    super();
  }

  onBlurInput(_$event: any): void {
    if (!this._disabled) {
      if (this._value) {
        this.onblur.emit(this._value);
      }
    }
  }

  propagateChange: any = () => {};

  ngAfterViewInit(): void {
    // outputs `I am span`
    // tslint:disable-next-line: comment-type
    /*  console.log('ngAfterViewInit nativeElement', this.nativeElement);
    console.log('ngAfterViewInit ElementRef', this.input_native.nativeElement); */
  }

  ngOnInit(): void {}

  ngOnChanges(changes: any): void {
    if (!this._disabled) {
      this.propagateChange(this.value);
    }
    const actions = this._getActions();
    if (actions[this.actionType]) {
      const selectedDate = this.frm.value[this.name];
      setTimeout(() => {
        isPristine(changes) || this._resetCbo();
        actions[this.actionType]();
        isPristine(changes) && this._setValueInFrm(selectedDate);
      });
    }
  }

  writeValue(value: any): void {
    if (value !== undefined) {
      this.value = value;
    }
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(_fn: any): void {}

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  private _getActions(): any {
    return {
      byDate: this._ObtenerFechas.bind(this),
      byDoctor: this._ObtenerFechasPorDoctor.bind(this)
    };
  }

  private _mapDates(obj: any): any {
    const fecha = obj.fecha
      .split('/')
      .reverse()
      .join('/');

    return getParsedStringDate(fecha).getTime();
  }

  private _ObtenerFechas(): void {
    if ([this.dependent1, this.dependent2].some(hasNotValidValue)) {
      return void 0;
    }

    const dates$ = combineLatest(
      this._Autoservicios.ObtenerFechas(
        {
          clinicaId: this.dependent1,
          codigoApp: APPLICATION_CODE,
          especialidadId: this.dependent2
        },
        {
          tipoCita: this.dependent5 ? this.dependent5 : 'P'
        }
      ),
      timer(1000)
    ).pipe(map(([dates]) => (dates !== null ? dates.map(this._mapDates.bind(this)) : [])));

    this._doActionsWhenAsynEnds(dates$);
  }

  private _ObtenerFechasPorDoctor(): void {
    this.getLoadingState.emit({ isLoadingVisible: true });
    if ([this.dependent1, this.dependent2, this.dependent3].some(hasNotValidValue)) {
      return void (this.disabled = true);
    }

    const dates$ = combineLatest(
      this._Autoservicios.ObtenerFechasPorDoctor(
        {
          clinicaId: this.dependent1,
          codigoApp: APPLICATION_CODE,
          doctorId: this.dependent3,
          especialidadId: this.dependent2
        },
        {
          tipoCita: this.dependent5 ? this.dependent5 : 'P'
        }
      ),
      timer(1000)
    ).pipe(map(([dates]) => (dates !== null ? dates.map(this._mapDates.bind(this)) : [])));

    this._doActionsWhenAsynEnds(dates$);
  }

  private _doActionsWhenAsynEnds(obs$: Observable<any>): void {
    const servi$ = obs$.subscribe((res: any) => {
      this.getLoadingState.emit({ isLoadingVisible: false });
      this.disabled = false;
      this.validDates = res;
      this.filter = this._getValidDates.bind(this);
      setTimeout(() => {
        // this._setValue(opt);
      });
      // tslint:disable-next-line: no-unbound-method
    }, console.error);

    this.arrToDestroy.push(servi$);
  }

  private _getValidDates(date: Date): boolean {
    return this.validDates.includes(date.getTime());
  }

  private _resetCbo(): void {
    this.frm.patchValue({
      [this.name]: ''
    });
  }

  private _setValueInFrm(selectedDate: any): void {
    if (hasNotValidValue(selectedDate)) {
      return void 0;
    }

    this.frm.patchValue({
      [this.name]: selectedDate || ''
    });
  }
}
