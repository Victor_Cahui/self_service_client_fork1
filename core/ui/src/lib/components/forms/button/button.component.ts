// tslint:disable: unnecessary-constructor
import { ChangeDetectionStrategy, Component, ElementRef, Input } from '@angular/core';

import {
  Block,
  Bordered,
  CanBlock,
  CanBorder,
  CanColor,
  CanDisable,
  CanSize,
  CanSpecial,
  Disabled,
  mixinBlock,
  mixinBordered,
  mixinColor,
  mixinDisabled,
  mixinSize,
  mixinSpecial,
  Size,
  Special,
  ThemePalette
} from '../../../../common';

export class ButtonBase {
  constructor(public _elementRef: ElementRef) {
    this.init();
  }

  init(): void {
    this._elementRef.nativeElement.classList.add('g-btn');
    this._elementRef.nativeElement.classList.add('g-text--uppercase');
  }
}

export const _ButtonMixinBase = mixinBlock(
  mixinSpecial(mixinSize(mixinBordered(mixinDisabled(mixinColor(ButtonBase)))))
);

@Component({
  selector: 'button[mf-button], a[mf-button]',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MfButtonComponent extends _ButtonMixinBase
  implements CanDisable, CanColor, CanBorder, CanSize, CanBlock, CanSpecial {
  @Input() color: ThemePalette = 'primary';
  @Input() outline: Bordered;
  @Input() block: Block;
  @Input() size: Size = 'md';
  @Input() disabled: Disabled = false;
  @Input() special: Special;

  constructor(elementRef: ElementRef) {
    super(elementRef);
  }
}
