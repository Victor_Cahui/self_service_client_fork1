import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfButtonComponent } from './button.component';

@NgModule({
  imports: [CommonModule],
  declarations: [MfButtonComponent],
  exports: [MfButtonComponent]
})
export class MfButtonModule {}
