import { Component, EventEmitter, forwardRef, Input, OnChanges, OnInit, Output } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'mf-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
  providers: [
    // tslint:disable-next-line:no-forward-ref
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => MfCheckboxComponent), multi: true }
  ]
})
export class MfCheckboxComponent implements ControlValueAccessor, OnChanges, OnInit {
  @Input() label = '';
  @Input() extraLabel: string;
  @Input() isDisabled: boolean;
  @Input() checked: boolean;
  @Input() classLabel: string;
  @Input()
  get value(): any {
    return this._value;
  }
  set value(value: any) {
    this._value = value;
    if (!this._disabled) {
      this.propagateChange(this.value);
    }
  }
  protected _value;
  protected _disabled = false;

  myId: string;

  @Output() extraLabelAction: EventEmitter<any>;

  propagateChange: any = () => {};

  constructor() {
    this.extraLabelAction = new EventEmitter();
  }

  ngOnInit(): void {
    this.myId = `checkbox-${Math.round(Math.random() * 10000).toString()}`;
    this.value = this.checked;
  }

  onExtraAction(e: Event): any {
    e.preventDefault();
    this.extraLabelAction.next();
  }

  ngOnChanges(): any {
    if (!this._disabled) {
      this.propagateChange(this.value);
    }
  }

  writeValue(value: any): void {
    if (value !== undefined) {
      this.value = value;
    }
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {}
}
