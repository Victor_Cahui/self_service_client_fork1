import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DirectivesModule } from '../../../directives/directives.module';
import { MfInputComponent } from './input.component';
import { CurrencyMaskDirective } from './mask/currency/currency-mask.directive';

@NgModule({
  imports: [CommonModule, FormsModule, DirectivesModule],
  declarations: [CurrencyMaskDirective, MfInputComponent],
  exports: [CurrencyMaskDirective, MfInputComponent]
})
export class MfInputModule {}
