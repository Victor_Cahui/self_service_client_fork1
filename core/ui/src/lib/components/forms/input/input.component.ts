import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostBinding,
  Input,
  OnChanges,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { FormUtils } from '@mx/core/shared/helpers/util/form';
import { Size } from '../../../../common';
import { coerceBooleanProp } from '../../../../common/helpers';

@Component({
  selector: 'mf-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  providers: [
    // tslint:disable-next-line:no-forward-ref
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => MfInputComponent), multi: true }
  ]
})
export class MfInputComponent implements AfterViewInit, ElementRef, ControlValueAccessor, OnChanges, OnInit {
  nativeElement: any;

  @ViewChild('mfInputComponent', { read: ElementRef })
  input_native: ElementRef;

  @ViewChild('container', { read: ElementRef })
  container: ElementRef;

  @HostBinding('attr.currency') currency: boolean;
  @HostBinding('attr.maxlength') maxlength_host: string;
  @HostBinding('attr.minlength') minlength_host: string;

  @Input()
  get pattern(): string {
    return this._pattern;
  }
  set pattern(pattern: string) {
    this._pattern = pattern;
  }
  protected _pattern: string;

  @Input()
  get maxlength(): string {
    return this._maxlength;
  }
  set maxlength(maxlength: string) {
    this._maxlength = maxlength;
  }
  protected _maxlength: string;

  @Input()
  get minlength(): string {
    return this._minlength;
  }
  set minlength(minlength: string) {
    this._minlength = minlength;
  }
  protected _minlength: string;

  @Input()
  get max(): string {
    return this._max;
  }
  set max(max: string) {
    this._max = max;
  }
  protected _max: string;

  @Input()
  get min(): string {
    return this._min;
  }

  set min(min: string) {
    this._min = min;
  }
  protected _min: string;

  @Input() spellcheck = false;
  @Input() OnlyNumber: boolean;
  @Input() OnlyLetter: boolean;
  @Input() Number: boolean;
  @Input() NumberLetter: boolean;
  @Input() dontWhiteSpace: boolean;
  @Input() prefixCurrency: boolean;
  @Input() currencySymbol = 'g-input-symbol--dolar';
  whiteSpaceI: string;

  @Input()
  get upperCase(): boolean {
    return this._uppercase;
  }

  set upperCase(value: boolean) {
    this._uppercase = value;
  }
  protected _uppercase = false;

  @Input()
  get value(): string {
    return this._value;
  }
  set value(value: string) {
    this._value = value;
    if (!this._disabled) {
      if (this.upperCase) {
        this._value = value.toUpperCase();
      }
      this.propagateChange(this.value);
    }
  }
  protected _value;

  @Input()
  get type(): string {
    return this._type;
  }
  set type(type: string) {
    this._type = type || 'text';
  }
  protected _type = 'text';

  @Input() size: Size = 'md';
  @Input()
  get label(): string {
    return this._label;
  }
  set label(label: string) {
    this._label = label;
    if (!this._label) {
      this._isLabelHidden = true;
    }
  }
  protected _label: string;

  @Input()
  get placeholder(): string {
    return this._label;
  }
  set placeholder(placeholder: string) {
    this.label = placeholder; // analizar
  }
  @Input()
  get isLabelHidden(): boolean {
    return this._isLabelHidden;
  }

  set isLabelHidden(hidden: boolean) {
    this._isLabelHidden = hidden;
  }
  protected _isLabelHidden = false;

  @Input()
  get disabled(): boolean {
    return this._disabled;
  }
  set disabled(value: boolean) {
    const disabled = coerceBooleanProp(value);
    if (this._disabled !== disabled) {
      FormUtils.disabledClass(this.container, disabled);
      this._disabled = disabled;
    }
    // Browsers may not fire the blur event if the input is disabled too quickly.
    // Reset from here to ensure that the element doesn't become stuck.
    // tslint:disable-next-line: comment-type
    /*if (this.focused) {
      this.focused = false;
      this.stateChanges.next();
    }*/
  }
  protected _disabled = false;

  @Input()
  get error(): boolean {
    return this._error;
  }
  set error(value: boolean) {
    this._error = coerceBooleanProp(value);
  }
  protected _error = false;

  @Input()
  get maskcurrency(): boolean {
    return this._maskCurrency;
  }
  set maskcurrency(value: boolean) {
    // this._maskCurrency = coerceBooleanProp(value);
  }
  protected _maskCurrency = false;

  @Input() autocomplete = 'on';
  @Input() isEmailType = false;
  @Input() enableCheck: boolean;
  @Input() readonly: boolean;

  @Output() onblur: EventEmitter<string> = new EventEmitter<string>();
  @Output() onpaste: EventEmitter<string> = new EventEmitter<string>();
  @Output() onfocus: EventEmitter<string> = new EventEmitter<string>();
  @Output() onkeypress: EventEmitter<string> = new EventEmitter<string>();
  @Output() onkeyup: EventEmitter<string> = new EventEmitter<string>();

  onBlurInput($event: any): void {
    if (!this._disabled) {
      if (this._value) {
        this.onblur.emit(this._value);
      }
    }
  }

  onFocusInput($event: any): void {
    if (!this._disabled) {
      this.onfocus.emit(this._value);
    }
  }

  onPasteInput(event: any): void {
    if (!this._disabled) {
      this.onpaste.emit(event);
    }
  }

  onKeyPress($event): void {
    if (!this._disabled) {
      this.onkeypress.emit(this._value);
    }
  }

  onKeyUp($event): void {
    if (!this.disabled) {
      this.onkeyup.emit($event.target.value);
    }
  }

  propagateChange: any = () => {};

  ngAfterViewInit(): void {
    // outputs `I am span`
    // tslint:disable-next-line: comment-type
    /*  console.log('ngAfterViewInit nativeElement', this.nativeElement);
    console.log('ngAfterViewInit ElementRef', this.input_native.nativeElement); */
  }

  ngOnInit(): void {
    if (this.dontWhiteSpace) {
      this.whiteSpaceI = 'blur';
    }
  }

  ngOnChanges(changes: any): void {
    if (!this._disabled) {
      this.propagateChange(this.value);
    }
  }

  writeValue(value: any): void {
    if (value !== undefined) {
      this.value = value;
    }
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {}

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}
