import { CurrencyMaskConfig } from './currency-mask.config';
import { InputCurrencyManager } from './input.currency.manager';

export class InputCurrencyService {
  private readonly inputManager: InputCurrencyManager;

  constructor(private readonly htmlInputElement: any, private options: CurrencyMaskConfig) {
    this.inputManager = new InputCurrencyManager(htmlInputElement);
  }

  addNumber(keyCode: number): void {
    if (!this.rawValue) {
      this.rawValue = this.applyMask(false, '0');
    }

    const keyChar = String.fromCharCode(keyCode);
    const selectionStart = this.inputSelection.selectionStart;
    const selectionEnd = this.inputSelection.selectionEnd;
    this.rawValue =
      this.rawValue.substring(0, selectionStart) +
      keyChar +
      this.rawValue.substring(selectionEnd, this.rawValue.length);
    this.updateFieldValue(selectionStart + 1);
  }

  applyMask(isNumber: boolean, rawValue: string): string {
    const { allowNegative, decimal, precision, prefix, suffix, thousands } = this.options;
    // tslint:disable-next-line: no-parameter-reassignment
    rawValue = isNumber ? parseFloat(rawValue).toFixed(precision) : rawValue;
    const onlyNumbers = rawValue.replace(/[^0-9]/g, '');

    if (!onlyNumbers) {
      return '';
    }

    let integerPart = onlyNumbers
      .slice(0, onlyNumbers.length - precision)
      .replace(/^0*/g, '')
      .replace(/\B(?=(\d{3})+(?!\d))/g, thousands);

    if (integerPart === '') {
      integerPart = '0';
    }

    let newRawValue = integerPart;
    const decimalPart = onlyNumbers.slice(onlyNumbers.length - precision);

    if (precision > 0) {
      newRawValue += decimal + decimalPart;
    }

    const isZero = parseInt(integerPart, 10) === 0 && (parseInt(decimalPart, 10) === 0 || decimalPart === '');
    const operator = rawValue.indexOf('-') > -1 && allowNegative && !isZero ? '-' : '';

    return operator + prefix + newRawValue + suffix;
  }

  clearMask(rawValue: string): number {
    if (this.isNullable() && rawValue === '') {
      return void 0; // undefined;
    }

    let value = (rawValue || '0').replace(this.options.prefix, '').replace(this.options.suffix, '');

    if (this.options.thousands) {
      value = value.replace(new RegExp(`\\${this.options.thousands}`, 'g'), '');
    }

    if (this.options.decimal) {
      value = value.replace(this.options.decimal, '.');
    }

    return parseFloat(value);
  }

  changeToNegative(): void {
    if (this.options.allowNegative && this.rawValue !== '' && this.rawValue.charAt(0) !== '-' && this.value !== 0) {
      this.rawValue = `-${this.rawValue}`;
    }
  }

  changeToPositive(): void {
    this.rawValue = this.rawValue.replace('-', '');
  }

  removeNumber(keyCode: number): void {
    if (this.isNullable() && this.value === 0) {
      this.rawValue = undefined; // null

      return;
    }

    let selectionEnd = this.inputSelection.selectionEnd;
    let selectionStart = this.inputSelection.selectionStart;

    if (selectionStart > this.rawValue.length - this.options.suffix.length) {
      selectionEnd = this.rawValue.length - this.options.suffix.length;
      selectionStart = this.rawValue.length - this.options.suffix.length;
    }

    selectionEnd = keyCode === 46 || keyCode === 63272 ? selectionEnd + 1 : selectionEnd;
    selectionStart = keyCode === 8 ? selectionStart - 1 : selectionStart;
    this.rawValue =
      this.rawValue.substring(0, selectionStart) + this.rawValue.substring(selectionEnd, this.rawValue.length);
    this.updateFieldValue(selectionStart);
  }

  updateFieldValue(selectionStart?: number): void {
    const newRawValue = this.applyMask(false, this.rawValue || '');
    // tslint:disable-next-line: no-parameter-reassignment
    selectionStart = selectionStart === undefined ? this.rawValue.length : selectionStart;
    this.inputManager.updateValueAndCursor(newRawValue, this.rawValue.length, selectionStart);
  }

  updateOptions(options: any): void {
    const value: number = this.value;
    this.options = options;
    this.value = value;
  }

  prefixLenght(): any {
    return this.options.prefix.length;
  }

  isNullable(): boolean {
    return this.options.nullable;
  }

  get canInputMoreNumbers(): boolean {
    return this.inputManager.canInputMoreNumbers;
  }

  get inputSelection(): any {
    return this.inputManager.inputSelection;
  }

  get rawValue(): string {
    return this.inputManager.rawValue;
  }

  set rawValue(value: string) {
    this.inputManager.rawValue = value;
  }

  get storedRawValue(): string {
    return this.inputManager.storedRawValue;
  }

  get value(): number {
    return this.clearMask(this.rawValue);
  }

  set value(value: number) {
    this.rawValue = this.applyMask(true, `${value}`);
  }
}
