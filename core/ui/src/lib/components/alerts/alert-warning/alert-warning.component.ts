import { Component, Input } from '@angular/core';

@Component({
  selector: 'mf-alert-warning',
  templateUrl: './alert-warning.component.html',
  styleUrls: ['./alert-warning.component.scss']
})
export class MfAlertWarningComponent {
  @Input() text: string;
  @Input() sizeIcon = 'g-font--20 g-font-lg--28';
}
