export {
  MfAlertWarningComponent as MfAlertWarning
} from '@mx/core/ui/lib/components/alerts/alert-warning/alert-warning.component';
export { MfAlertWarningModule } from '@mx/core/ui/lib/components/alerts/alert-warning/alert-warning.module';
