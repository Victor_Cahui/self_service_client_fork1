import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfAlertWarningComponent } from './alert-warning.component';

@NgModule({
  imports: [CommonModule],
  declarations: [MfAlertWarningComponent],
  exports: [MfAlertWarningComponent]
})
export class MfAlertWarningModule {}
