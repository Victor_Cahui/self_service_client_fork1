export {
  MfCustomAlertComponent as MfCustomAlert
} from '@mx/core/ui/lib/components/alerts/custom-alert/custom-alert.component';
export { MfCustomAlertModule } from '@mx/core/ui/lib/components/alerts/custom-alert/custom-alert.module';
