import { Component, Input, OnInit } from '@angular/core';
import {
  NOTIFICATION_INFO_ICON,
  NOTIFICATION_SUCCESS_ICON,
  NOTIFICATION_WARNING_ICON
} from '@mx/settings/constants/images-values';

export enum NotificationStatus {
  INFO = 1,
  SUCCESS = 2,
  WARNING = 3
}

@Component({
  selector: 'mf-custom-alert',
  templateUrl: './custom-alert.component.html',
  styleUrls: ['./custom-alert.component.scss']
})
export class MfCustomAlertComponent implements OnInit {
  @Input() text: string;
  @Input() status: number;
  icon: string;
  statusColor: string;

  ngOnInit(): void {
    this.changeNotificationByStatus(this.status);
  }

  private changeNotificationByStatus(status): void {
    switch (status) {
      case NotificationStatus.INFO:
        this.icon = NOTIFICATION_INFO_ICON;
        this.statusColor = 'g-bg-c--info1';
        break;
      case NotificationStatus.SUCCESS:
        this.icon = NOTIFICATION_SUCCESS_ICON;
        this.statusColor = 'g-bg-c--success1';
        break;
      default:
        this.icon = NOTIFICATION_WARNING_ICON;
        this.statusColor = 'g-bg-c--warning1';
        break;
    }
  }
}
