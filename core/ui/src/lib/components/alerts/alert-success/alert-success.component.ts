import { Component, Input } from '@angular/core';

@Component({
  selector: 'mf-alert-success',
  templateUrl: './alert-success.component.html',
  styleUrls: ['./alert-success.component.scss']
})
export class MfAlertSuccessComponent {
  @Input() text: string;
  @Input() sizeIcon = 'g-font--16 g-font-lg--18';
  @Input() sizeNotification = 'mr-4 ml-4';
}
