import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfAlertSuccessComponent } from './alert-success.component';

@NgModule({
  imports: [CommonModule],
  declarations: [MfAlertSuccessComponent],
  exports: [MfAlertSuccessComponent]
})
export class MfAlertSuccessModule {}
