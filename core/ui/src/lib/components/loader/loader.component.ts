import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'mf-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class MfLoaderComponent implements OnInit {
  @Input() customClass = 'loader';

  ngOnInit(): void {}
}
