import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgxPaginationModule, PaginatePipe as MfPaginate } from 'ngx-pagination';
import { MfPaginatorComponent } from './paginator.component';

@NgModule({
  imports: [CommonModule, NgxPaginationModule],
  exports: [MfPaginatorComponent, MfPaginate],
  declarations: [MfPaginatorComponent],
  providers: []
})
export class MfPaginatorModule {}
