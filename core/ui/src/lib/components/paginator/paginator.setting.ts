export const MfPaginatorLang = {
  Show: 'Mostrar'
};

export const MfPaginatorDefault = {
  perPageList: [10, 50, 100],
  page: 1,
  maxSize: 7
};
