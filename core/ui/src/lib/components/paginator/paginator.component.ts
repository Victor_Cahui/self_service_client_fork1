import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MfPaginatorDefault as Default, MfPaginatorLang as Lang } from './paginator.setting';

@Component({
  selector: 'mf-paginator',
  templateUrl: './paginator.component.html'
})
export class MfPaginatorComponent implements OnInit {
  @Output() pageChange: EventEmitter<number>;
  @Output() itemsPerPageChange: EventEmitter<number>;

  @Input() labelSingular: string;
  @Input() labelPlural: string;
  @Input() perPageList: Array<number> = [];
  @Input() items: Array<any>;
  @Input() total: number;
  @Input() page = 1;
  @Input() maxSize: number;
  @Input() itemsPerPage: number;
  @Input() autoHide: boolean;
  @Input() isHeaderLabelsVisible = true;
  @Input() marginTop = true;
  @Input() hasBorder = true;

  @Input() textBold: string;
  @Input() perPageListColor = 'g-font--medium';
  @Input() marginCard = 'g-p--15 g-py-lg--35 g-px-lg--50';

  lang = Lang;

  constructor() {
    this.itemsPerPageChange = new EventEmitter<number>();
    this.pageChange = new EventEmitter<number>();
  }

  ngOnInit(): void {
    this.perPageList = this.perPageList && this.perPageList.length ? this.perPageList : Default.perPageList;
    this.itemsPerPage = this.perPageList.length ? this.perPageList[0] : 0;
    this.page = this.page !== undefined ? this.page : Default.page;
    this.maxSize = this.maxSize !== undefined ? this.maxSize : Default.maxSize;

    this.itemsPerPageFn(this.itemsPerPage);
    this.pageFn(this.page);
  }

  itemsPerPageFn(itemsPerPage: number): void {
    if (this.itemsPerPage !== itemsPerPage) {
      this.itemsPerPage = itemsPerPage;
      this.itemsPerPageChange.next(itemsPerPage);
    }
  }

  pageFn(page: number): void {
    if (this.page !== page) {
      this.page = page;
      this.pageChange.next(page);
    }
  }

  getClassPerPage(perPage: number, last: any): Array<string> {
    const classes: Array<string> = [];
    if (perPage === this.itemsPerPage) {
      classes.push(this.perPageListColor);
    }
    if (!last) {
      classes.push('g-br--1 g-bc--gray18 g-pr--5');
    }

    return classes;
  }
}
