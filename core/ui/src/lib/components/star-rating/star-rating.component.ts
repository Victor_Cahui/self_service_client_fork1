import { Component, EventEmitter, HostBinding, Input, Output } from '@angular/core';
export const STAR_OUT_ICON = 'icon-mapfre_126_star_out';
export const STAR_ICON = 'icon-mapfre_125_star';

@Component({
  selector: 'client-star-rating',
  templateUrl: './star-rating.component.html'
})
export class StarRatingComponent {
  @HostBinding('attr.class') class = 'w-100';

  @Input() label: string;
  @Input() enabled: boolean;
  @Input() currentSelection: number;

  @Output() selection: EventEmitter<number>;

  stars = [5, 4, 3, 2, 1];

  starIcon = STAR_ICON;
  starOutIcon = STAR_OUT_ICON;

  constructor() {
    this.selection = new EventEmitter<number>();
  }

  setSelection(star: number): void {
    if (this.enabled) {
      this.currentSelection = star;
      this.selection.next(star);
    }
  }
}
