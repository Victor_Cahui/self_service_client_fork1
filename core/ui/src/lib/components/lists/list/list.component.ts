import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'mf-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class MfListComponent implements OnInit {
  @Input() list: Array<string> = [];

  ngOnInit(): void {}

  trackByFn(index, item): any {
    return index;
  }
}
