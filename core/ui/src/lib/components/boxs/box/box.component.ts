import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'mf-box',
  templateUrl: './box.component.html',
  styleUrls: ['./box.component.scss']
})
export class MfBoxComponent implements OnInit {
  @Input() text = 'Jusmar';

  ngOnInit(): void {}
}
