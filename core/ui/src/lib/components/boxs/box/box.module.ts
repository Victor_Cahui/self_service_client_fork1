import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfBoxComponent } from './box.component';

@NgModule({
  imports: [CommonModule],
  declarations: [MfBoxComponent],
  exports: [MfBoxComponent]
})
export class MfBoxModule {}
