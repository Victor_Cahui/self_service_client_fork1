import { ChangeDetectorRef, Component, EventEmitter, forwardRef, HostListener, Input, Output } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'mf-switch',
  template: `
    <span
      class="switch"
      [class.checked]="checked"
      [class.disabled]="disabled"
      [class.switch-large]="size === 'large'"
      [class.switch-medium]="size === 'medium'"
      [class.switch-small]="size === 'small'"
      [style.background-color]="getColor()"
      [style.border-color]="getColor('borderColor')"
    >
      <span class="switch-pane" *ngIf="checkedLabel || uncheckedLabel">
        <span class="switch-label-checked">{{ this.checkedLabel }}</span>
        <span class="switch-label-unchecked">{{ this.uncheckedLabel }}</span>
      </span>
      <small [style.background]="getColor('switchColor')">
        <ng-content></ng-content>
      </small>
    </span>
  `,
  providers: [
    // tslint:disable-next-line:no-forward-ref
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => SwitchComponent), multi: true }
  ]
})
export class SwitchComponent implements ControlValueAccessor {
  private _checked: boolean;
  private _disabled: boolean;
  private _reverse: boolean;

  @Input() size = 'medium';
  @Input() color: string;
  @Input() switchOffColor: string;
  @Input() switchColor: string;
  @Input() defaultBgColor: string;
  @Input() defaultBoColor: string;
  @Input() checkedLabel: string;
  @Input() uncheckedLabel: string;

  @Input()
  set checked(checked: boolean) {
    this._checked = checked;
  }

  get checked(): boolean {
    return this._checked;
  }

  @Input()
  set disabled(disabled: boolean) {
    this._disabled = disabled;
  }

  get disabled(): boolean {
    return this._disabled;
  }

  @Input()
  set reverse(reverse: boolean) {
    this._reverse = reverse;
  }

  get reverse(): boolean {
    return this._reverse;
  }

  /**
   * Emits changed value
   */
  // tslint:disable-next-line:no-output-named-after-standard-event
  @Output() change = new EventEmitter<boolean>();

  /**
   * Emits DOM event
   */
  @Output() changeEvent = new EventEmitter<MouseEvent>();

  /**
   * Emits changed value
   */
  @Output() valueChange = new EventEmitter<boolean>();

  constructor(private readonly cdr: ChangeDetectorRef) {}

  getColor(flag = ''): string {
    if (flag === 'borderColor') {
      return this.defaultBoColor;
    }
    if (flag === 'switchColor') {
      if (this.reverse) {
        return !this.checked ? this.switchColor : this.switchOffColor || this.switchColor;
      }

      return this.checked ? this.switchColor : this.switchOffColor || this.switchColor;
    }
    if (this.reverse) {
      return !this.checked ? this.color : this.defaultBgColor;
    }

    return this.checked ? this.color : this.defaultBgColor;
  }

  @HostListener('click', ['$event'])
  onToggle(event: MouseEvent): void {
    if (this.disabled) {
      return;
    }
    this.checked = !this.checked;

    // Component events
    this.change.emit(this.checked);
    this.valueChange.emit(this.checked);
    this.changeEvent.emit(event);

    // value accessor callbacks
    this.onChangeCallback(this.checked);
    this.onTouchedCallback(this.checked);
    this.cdr.markForCheck();
  }

  writeValue(obj: any): void {
    if (obj !== this.checked) {
      this.checked = !!obj;
    }
    if (this.cdr) {
      this.cdr.markForCheck();
    }
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  private onTouchedCallback = (v: any) => {};
  private onChangeCallback = (v: any) => {};
}
