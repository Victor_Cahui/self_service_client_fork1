import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NavbarFixedComponent } from './navbar-fixed.component';

@NgModule({
  imports: [CommonModule],
  declarations: [NavbarFixedComponent],
  exports: [NavbarFixedComponent]
})
export class NavbarFixedModule {}
