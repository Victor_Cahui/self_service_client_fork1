import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CarouselItemDirective } from './caroulse-item.directive';
import { CarouselItemElementDirective } from './carousel-item-element.directive';
import { CarouselWrapperDirective } from './carousel-wrapper.directive';
import { CarouselComponent } from './carousel.component';

@NgModule({
  imports: [CommonModule],
  declarations: [CarouselComponent, CarouselItemElementDirective, CarouselItemDirective, CarouselWrapperDirective],
  exports: [CarouselComponent, CarouselItemElementDirective, CarouselWrapperDirective, CarouselItemDirective]
})
export class CarouselModule {}
