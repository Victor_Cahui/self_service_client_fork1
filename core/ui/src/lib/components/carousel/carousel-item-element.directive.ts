import { Directive } from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '.mx-carousel-item'
})
export class CarouselItemElementDirective {}
