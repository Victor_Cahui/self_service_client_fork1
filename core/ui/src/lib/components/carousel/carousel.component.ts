import { animate, AnimationBuilder, AnimationFactory, AnimationPlayer, style } from '@angular/animations';
import {
  AfterViewInit,
  Component,
  ContentChildren,
  ElementRef,
  EventEmitter,
  Host,
  Input,
  OnDestroy,
  OnInit,
  Output,
  QueryList,
  ViewChild,
  ViewChildren
} from '@angular/core';
import * as Hammer from 'hammerjs';
import { Subscription } from 'rxjs';
import { pairwise, startWith } from 'rxjs/operators';
import { ResizeService, SCREEN_NAMES } from '../../../../../../core/shared/src/helpers/events/resize.service';
import { CarouselItemDirective } from './caroulse-item.directive';
import { CarouselItemElementDirective } from './carousel-item-element.directive';
import { CarouselWrapperDirective } from './carousel-wrapper.directive';

export interface NumberItemsForScreen {
  XS: number;
  SM: number;
  MD: number;
  LG: number;
  XL: number;
}

@Component({
  selector: 'mx-carousel',
  exportAs: 'carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('carousel') private readonly carousel: ElementRef;
  @ViewChildren(CarouselItemElementDirective, { read: ElementRef }) private readonly itemsElements: QueryList<
    ElementRef
  >;
  @ContentChildren(CarouselItemDirective) items: QueryList<CarouselItemDirective>;

  @Input() timing = '250ms ease-in';
  @Input() showControls = true;
  @Input() showIndicators = true;
  @Input() currentSlide = 0;
  @Input() numberItems: NumberItemsForScreen = {
    XS: 1,
    SM: 1,
    MD: 1,
    LG: 1,
    XL: 1
  };
  @Output() changeSlide?: EventEmitter<number> = new EventEmitter<number>();

  private itemsInCarousel: number;
  private player: AnimationPlayer;
  private itemWidth: number;
  private itemsInCarouselWidth: number;
  listIndicators: Array<number>;

  carouselWrapperStyle = {};
  carouselTotalStyle = {};
  carouselItemStyle = {};
  subResize: Subscription;

  ngAfterViewInit(): void {
    this.subResize = this.resizeService
      .getEvent()
      .pipe(
        startWith(null),
        pairwise()
      )
      .subscribe(([previousValue, currentValue]) => {
        let timer = 0;
        if (
          this.resizeService.isEqual(SCREEN_NAMES.XL, previousValue) &&
          this.resizeService.isEqual(SCREEN_NAMES.XS, currentValue)
        ) {
          timer = 500;
        }
        if (this.resizeService.is(SCREEN_NAMES.XS)) {
          this.itemsInCarousel = this.numberItems.XS;
        } else if (this.resizeService.is(SCREEN_NAMES.SM)) {
          this.itemsInCarousel = this.numberItems.SM;
        } else if (this.resizeService.is(SCREEN_NAMES.MD)) {
          this.itemsInCarousel = this.numberItems.MD;
        } else if (this.resizeService.is(SCREEN_NAMES.LG)) {
          this.itemsInCarousel = this.numberItems.LG;
        } else if (this.resizeService.is(SCREEN_NAMES.XL)) {
          this.itemsInCarousel = this.numberItems.XL;
        }

        this.itemWidth = this.wrapper.getWidth();
        this.readjustWidth();
        this.readjustHeigth();
        this.setAfterResize(this.currentSlide);

        // Indicadores
        setTimeout(() => {
          if (!!this.items) {
            const arr = [];
            for (let i = 0; i < this.items.length - this.itemsInCarousel + 1; i++) {
              arr.push(i);
            }
            this.listIndicators = [...arr];
          }
        }, timer);
      });
  }

  constructor(
    private readonly builder: AnimationBuilder,
    private readonly resizeService: ResizeService,
    @Host() private readonly wrapper: CarouselWrapperDirective
  ) {
    this.listIndicators = [];
  }

  ngOnInit(): void {
    // Swipe en carousel
    const mc = new Hammer(document.getElementById('swipe-box'));
    mc.get('swipe').set({
      direction: Hammer.DIRECTION_HORIZONTAL,
      threshold: 1,
      velocity: 0.1
    });
    mc.on('swipeleft swiperight tap press', event => {
      const action = event.type;
      if (action === 'swiperight') {
        this.prev();
      }
      if (action === 'swipeleft') {
        this.next();
      }
    });
  }

  ngOnDestroy(): void {
    if (this.subResize) {
      this.subResize.unsubscribe();
    }
  }

  next(): void {
    this.currentSlide =
      this.currentSlide + this.itemsInCarousel + 1 > this.items.length
        ? 0
        : (this.currentSlide + 1) % this.items.length;
    this.onChange();
  }

  prev(): void {
    this.currentSlide =
      this.currentSlide === 0
        ? this.items.length - this.itemsInCarousel
        : (this.currentSlide - 1 + this.items.length) % this.items.length;
    this.onChange();
  }

  goTo(index: number): void {
    this.currentSlide = index > this.listIndicators.length - 1 ? this.listIndicators.length - 1 : index;
    this.onChange();
  }

  onChange(): void {
    const offset = this.currentSlide * this.itemsInCarouselWidth;
    const myAnimation: AnimationFactory = this.builder.build([
      animate(this.timing, style({ transform: `translateX(-${offset}px)` }))
    ]);
    this.player = myAnimation.create(this.carousel.nativeElement);
    this.player.play();
    this.changeSlide.emit(this.currentSlide);
  }

  setAfterResize(index: number): void {
    this.currentSlide = index + this.itemsInCarousel > this.items.length ? 0 : index;
    const offset = this.currentSlide * this.itemsInCarouselWidth;
    const myAnimation: AnimationFactory = this.builder.build([
      animate('0ms ease-in', style({ transform: `translateX(-${offset}px)` }))
    ]);
    this.player = myAnimation.create(this.carousel.nativeElement);
    this.player.play();
  }

  trackByFn(index, item): number {
    return index;
  }

  private readjustWidth(): void {
    this.itemsInCarouselWidth = this.itemWidth / this.itemsInCarousel;
    this.carouselWrapperStyle = {
      width: `${this.itemWidth}px`
    };
    this.carouselTotalStyle = {
      width: `${this.itemsInCarouselWidth * this.itemsElements.length}px`
    };
    this.carouselItemStyle = {
      width: `${this.itemsInCarouselWidth}px`
    };
  }

  private readjustHeigth(): void {
    const divs = document.getElementsByClassName('match-heigth');

    setTimeout(() => {
      const maxHeight = document.getElementsByClassName('mx-carousel-item')[0].clientHeight;
      if (maxHeight > 0) {
        Array.from(divs).forEach((x: HTMLElement) => (x.style.height = `${maxHeight}px`));
      }
    }, 100); // para que se tome correctamente el alto de la div
  }
}
