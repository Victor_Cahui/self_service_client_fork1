import { Directive, ElementRef, HostBinding } from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '.wrapper-slider'
})
export class CarouselWrapperDirective {
  @HostBinding('style.width') private readonly _width;

  constructor(private readonly el: ElementRef) {}

  getWidth(): number {
    return this.el.nativeElement.getBoundingClientRect().width;
  }
}
