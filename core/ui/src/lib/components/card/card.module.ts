import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardActionLinkComponent } from './card-action-link/card-action-link.component';
import { CardActionsLinkComponent } from './card-actions-link/card-actions-link.component';
import { CardContentComponent } from './card-content/card-content.component';
import { CardFooterComponent } from './card-footer/card-footer.component';
import { CardHeaderComponent } from './card-header/card-header.component';
import { CardItemIconComponent } from './card-item-icon/card-item-icon.component';
import { CardItemComponent } from './card-item/card-item.component';
import { CardWithoutDataComponent } from './card-without-data/card-without-data.component';
import { CardComponent } from './card/card.component';

@NgModule({
  imports: [CommonModule],
  declarations: [
    CardComponent,
    CardHeaderComponent,
    CardFooterComponent,
    CardContentComponent,
    CardItemComponent,
    CardActionsLinkComponent,
    CardActionLinkComponent,
    CardWithoutDataComponent,
    CardItemIconComponent
  ],
  exports: [
    CardComponent,
    CardHeaderComponent,
    CardFooterComponent,
    CardContentComponent,
    CardItemComponent,
    CardActionsLinkComponent,
    CardActionLinkComponent,
    CardWithoutDataComponent,
    CardItemIconComponent
  ]
})
export class CardModule {}
