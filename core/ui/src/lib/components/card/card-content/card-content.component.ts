import { Component, Input } from '@angular/core';
import { coerceBooleanProp } from '../../../../common/helpers';

@Component({
  selector: 'mf-card-content',
  template: `
    <div class="" [ngClass]="{ 'g-px-lg--10 g-py-lg--10': !wpd, 'g-bt--1': showIconWeb, 'g-bc--gray11': !showIconWeb }">
      <ng-content></ng-content>
    </div>
  `
})
export class CardContentComponent {
  @Input() showIconWeb = false;
  @Input()
  // Whitout Padding
  get wpd(): boolean {
    return this._wpd;
  }
  set wpd(value: boolean) {
    this._wpd = coerceBooleanProp(value);
  }
  protected _wpd = false;
}
