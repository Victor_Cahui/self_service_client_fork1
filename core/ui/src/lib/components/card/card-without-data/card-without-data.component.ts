import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'mf-card-without-data',
  templateUrl: './card-without-data.component.html'
})
export class CardWithoutDataComponent implements OnInit {
  @Input() icon: string;
  @Input() message: string;
  @Input() subMessage: string;
  @Input() center: boolean;
  @Input() pl = true;

  ngOnInit(): void {}
}
