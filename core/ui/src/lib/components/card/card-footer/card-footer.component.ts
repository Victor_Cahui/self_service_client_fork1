import { Component } from '@angular/core';

@Component({
  selector: 'mf-card-footer',
  template: `
    <div class="mt-auto g-bg-c--white1 d-block g-bt--1 w-100 text-center p-xs--2">
      <ng-content></ng-content>
    </div>
  `
})
export class CardFooterComponent {}
