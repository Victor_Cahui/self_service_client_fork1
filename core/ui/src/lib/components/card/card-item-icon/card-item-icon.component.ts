import { Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'mf-card-item-icon',
  template: `
    <div class="g-items-icon g-font--25 g-font-md--21 mr-3 g-c--gray3" [ngClass]="[icon]"></div>
    <div class="g-items-text g-c--gray3 d-flex align-items-center g-ie-flex--0-1-auto">
      {{ text }}<ng-content></ng-content>
    </div>
  `,
  styles: []
})
export class CardItemIconComponent {
  @HostBinding('attr.class') attr_class =
    'g-font--light g-items-list d-flex g-p--15 d-flex align-items-center g-bt--1 g-bc--gray11';
  @Input() icon: string;
  @Input() text: string;
}
