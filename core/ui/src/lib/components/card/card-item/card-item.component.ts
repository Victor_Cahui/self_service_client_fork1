import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'mf-card-item',
  templateUrl: './card-item.component.html',
  styleUrls: ['./card-item.component.scss']
})
export class CardItemComponent implements OnInit {
  @Input() mustRemoveClassForIE: boolean;

  ngOnInit(): void {}
}
