import { Component } from '@angular/core';

@Component({
  selector: 'mf-card-actions-link',
  templateUrl: './card-actions-link.component.html'
})
export class CardActionsLinkComponent {
  collapse = false;

  toggle(): void {
    this.collapse = !this.collapse;
  }
}
