import { Component, HostBinding } from '@angular/core';

@Component({
  selector: 'mf-card-action-link',
  template: `
    <ng-content></ng-content>
  `
})
export class CardActionLinkComponent {
  @HostBinding('attr.class') class = 'g-box-icons--item d-flex align-items-center mr-lg-4';
}
