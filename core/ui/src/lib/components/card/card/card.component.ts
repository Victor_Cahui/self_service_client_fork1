import { Component, HostBinding } from '@angular/core';

@Component({
  selector: 'mf-card',
  template: `
    <ng-content></ng-content>
  `
})
export class CardComponent {
  @HostBinding('attr.class')
  attr_class = 'g-box--shadow1 g-bg-c--white1 d-flex flex-column w-100 g-b-radius--3 h-lg-100';
}
