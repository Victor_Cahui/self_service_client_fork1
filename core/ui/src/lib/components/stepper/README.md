# Uso desde componentes:

## Si la acción va a estar en componente

```
<mf-stepper [stepList]="stepList" (selected)="stepSelected($event)"></mf-stepper>
```

## Si se permite navegar entre rutas

```
<mf-stepper [stepList]="stepList" [actions]="true"></mf-stepper>
```

stepList, debe tener la siguiente estructura:

```
export interface IStepperItem {
  name: string;
  value: number;
  route?: string;
}
```
