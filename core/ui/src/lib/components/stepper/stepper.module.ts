import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { StepperComponent } from './stepper.component';

@NgModule({
  imports: [CommonModule, RouterModule],
  declarations: [StepperComponent],
  exports: [StepperComponent]
})
export class StepperModule {}
