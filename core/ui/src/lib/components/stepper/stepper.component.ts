import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';

export interface IStepperItem {
  name: string;
  value: number;
  visible?: boolean;
  route?: string;
}

@Component({
  selector: 'mf-stepper',
  templateUrl: './stepper.component.html'
})
export class StepperComponent implements OnInit, OnChanges {
  @Input() stepList: Array<IStepperItem>;
  @Input() actions = false;
  @Input() currentStep = 1;
  @Output() stepSelected?: EventEmitter<IStepperItem> = new EventEmitter<IStepperItem>();
  @Input() customClass = 'g-pt-lg--40 g-pt--20 g-pb--20 g-px--20 g-px-lg--0 g-pb-lg--40';

  constructor(private readonly router: Router) {}

  ngOnInit(): void {
    this.filterVisible();
  }

  ngOnChanges(): void {
    this.filterVisible();
  }

  onSelect(step: IStepperItem): void {
    // Sin accion
    if (!this.actions) {
      return;
    }
    this.currentStep = step.value;
    // Si tiene ruta
    if (step.route) {
      this.router.navigate([step.route]);
    } else {
      // Accion en componente
      this.stepSelected.emit(step);
    }
  }

  // Asignar step actual desde componente
  setStepSelected(value): void {
    this.currentStep = value;
  }

  // Filtrar step ocultos
  filterVisible(): void {
    this.stepList = this.stepList.filter(item => item.visible);
  }
}
