import { DOCUMENT } from '@angular/common';
import {
  Component,
  ElementRef,
  HostListener,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild
} from '@angular/core';
import { ResizeService } from '@mx/core/shared/helpers/events/resize.service';
import { Subscription } from 'rxjs';

export const TOOLTIP = {
  top: 10,
  width: 2,
  height: 15,
  conditional: {
    left: 30,
    width: 75,
    top: 0,
    height: 300
  },
  windowMargin: 30
};

@Component({
  selector: 'mf-tooltips',
  templateUrl: './tooltips.component.html',
  styles: [
    `
      /deep/ .g-tooltip--alternative-message--bottom {
        bottom: 40px;
      }
      /deep/ .g-tooltip--alternative-message--bottom .g-tooltip--alternative-arrow {
        left: 20px;
        right: auto;
        bottom: -25px;
        top: auto;
        transform: translateX(-50%) rotate(0deg);
      }
    `
  ]
})
export class TooltipsComponent implements OnDestroy, OnInit {
  @ViewChild('target') target: ElementRef;
  @ViewChild('arrow') arrow: ElementRef;
  @ViewChild('content') content: ElementRef;

  @Input() classPosition: string;
  @Input() detail: string;
  @Input() text: string;
  @Input() classIcon = 'icon-mapfre_048_tooltip g-tooltip--color g-font--26';
  @Input() classAlign: string;
  @Input() classColor: string;
  @Input() disabled: boolean;

  leftTooltip: number;
  topTooltip: number;
  subResize: Subscription;
  windowWidth: number;
  classBox: string;

  constructor(
    private readonly resizeService: ResizeService,
    public renderer2: Renderer2,
    @Inject(DOCUMENT) private readonly document: Document
  ) {}

  ngOnInit(): void {
    // Posicion y color
    this.classBox = `${this.classAlign || ''} ${this.classColor || ''}`;

    if (this.target) {
      this.subResize = this.resizeService.getEvent().subscribe(width => {
        this.windowWidth = width;
        this.getPosition(this.target);
        setTimeout(() => {
          this.initTooltip();
          this.arrowPosition();
        }, 100);
      });
    }
  }

  ngOnDestroy(): void {
    if (!!this.subResize) {
      this.subResize.unsubscribe();
    }
  }

  @HostListener('touchstart', ['$event']) onTouchStart(event: Event): void {}

  onHover(): void {
    this.getPosition(this.target);
    this.initTooltip();
    this.arrowPosition();
  }

  getPosition(el): void {
    let offsetLeft = 0;
    let offsetTop = 0;
    offsetLeft = el.nativeElement.offsetLeft;
    offsetTop = el.nativeElement.offsetTop;
    // tslint:disable-next-line: no-parameter-reassignment
    el = el.nativeElement.offsetParent;
    while (el) {
      offsetLeft += el.offsetLeft;
      offsetTop += el.offsetTop;
      // tslint:disable-next-line: no-parameter-reassignment
      el = el.offsetParent;
    }
    this.leftTooltip = offsetLeft;
    this.topTooltip = offsetTop;
  }

  initTooltip(): void {
    const posLeft =
      this.leftTooltip +
      this.target.nativeElement.offsetWidth / TOOLTIP.width -
      this.arrow.nativeElement.offsetWidth / TOOLTIP.width;
    let posTop = this.topTooltip - this.target.nativeElement.offsetHeight - TOOLTIP.top;

    if (!this.classAlign) {
      // Left
      if (posLeft < TOOLTIP.conditional.left) {
        this.renderer2.addClass(this.content.nativeElement, 'g-tooltip--alternative-message--left');
      } else {
        this.renderer2.removeClass(this.content.nativeElement, 'g-tooltip--alternative-message--left');
      }

      // Right
      if (
        posLeft + this.arrow.nativeElement.offsetWidth + TOOLTIP.conditional.width >
        this.windowWidth - TOOLTIP.windowMargin
      ) {
        this.renderer2.addClass(this.content.nativeElement, 'g-tooltip--alternative-message--right');
      } else {
        this.renderer2.removeClass(this.content.nativeElement, 'g-tooltip--alternative-message--right');
      }

      // Top
      if (posTop < TOOLTIP.conditional.top) {
        posTop = this.topTooltip + this.target.nativeElement.offsetHeight + TOOLTIP.height;
        this.renderer2.addClass(this.content.nativeElement, 'g-tooltip--alternative-message--top');
      } else {
        this.renderer2.removeClass(this.content.nativeElement, 'g-tooltip--alternative-message--top');
      }

      // Bottom (mensaje arriba)
      if (
        posTop + this.arrow.nativeElement.offsetHeight + TOOLTIP.conditional.height >
        this.document.body.scrollHeight
      ) {
        posTop = this.topTooltip + this.target.nativeElement.offsetHeight + TOOLTIP.height;
        this.renderer2.addClass(this.content.nativeElement, 'g-tooltip--alternative-message--bottom');
      } else {
        this.renderer2.removeClass(this.content.nativeElement, 'g-tooltip--alternative-message--bottom');
      }
    }
  }

  // Flecha arriba
  arrowPosition(): void {
    if (this.content.nativeElement.classList.contains('g-tooltip--alternative-message--bottom')) {
      this.renderer2.addClass(this.arrow.nativeElement, 'active');
    } else {
      this.renderer2.removeClass(this.arrow.nativeElement, 'active');
    }
  }
}
