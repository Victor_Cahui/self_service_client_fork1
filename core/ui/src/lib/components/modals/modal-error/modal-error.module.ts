import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfModalAlertModule } from '../modal-alert/modal-alert.module';
import { MfModalErrorComponent } from './modal-error.component';

@NgModule({
  imports: [CommonModule, MfModalAlertModule],
  exports: [MfModalErrorComponent],
  declarations: [MfModalErrorComponent]
})
export class MfModalErrorModule {}
