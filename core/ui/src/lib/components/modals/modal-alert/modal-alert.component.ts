// tslint:disable: invalid-void
import { DOCUMENT } from '@angular/common';
import { Component, ElementRef, EventEmitter, Inject, Input, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'mf-modal-alert',
  templateUrl: './modal-alert.component.html'
})
export class MfModalAlertComponent {
  status = false;
  /** inputs & Outputs */
  @Input() title: string;
  @Input() isDisabledAutoClose: boolean;
  @Input() showLoading: boolean;
  @Input() noMinHeight = false;
  @Input() confirm = false;
  @Input() cancelButtonText = 'Cancelar';
  @Input() confirmButtonText = 'Aceptar';
  @Input() disableTopClose = false;
  @Input() sizeClass = 'g-modal--confirm';
  @Input() outline = true;
  @Output() confirmResponse: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() statusModal: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() closeEvent: EventEmitter<void> = new EventEmitter<void>();

  /** elements */
  @ViewChild('gModal') gModal: ElementRef<HTMLDivElement>;
  @ViewChild('gModalContainer') gModalContainer: ElementRef<HTMLDivElement>;
  @ViewChild('gModalWrapper') gModalWrapper: ElementRef<HTMLDivElement>;
  @ViewChild('gModalClose') gModalClose: ElementRef<HTMLDivElement>;
  @ViewChild('gModalContent') gModalContent: ElementRef<HTMLDivElement>;
  @ViewChild('gModalFooter') gModalFooter: ElementRef<HTMLDivElement>;

  currentScrollY: number;

  constructor(@Inject(DOCUMENT) private readonly document: Document) {}

  get isOpen(): boolean {
    return this.gModal.nativeElement.style.display === 'block';
  }

  onConfirm(response: boolean): void {
    this.confirmResponse.next(response);
    this.isDisabledAutoClose || this.close();
  }

  open(): void {
    this.currentScrollY = window.scrollY;
    this.gModal.nativeElement.style.display = 'block';
    this.addOrRemoveScroll(false);
    this.statusModal.next(true);
  }

  close(): void {
    this.closeEvent.emit();
    this.gModal.nativeElement.style.display = 'none';
    this.addOrRemoveScroll(true);
    this.statusModal.next(false);
  }

  protected _fnGetPosY(): any {
    return window.pageYOffset;
  }

  addOrRemoveScroll(remove?: boolean): void {
    const htmlElement = this.document.querySelector('html');
    const bodyElement = this.document.querySelector('body');
    htmlElement.classList[remove ? 'remove' : 'add']('body-hidden');
    htmlElement.classList[remove ? 'remove' : 'add']('g-modal-no-scroll');
    bodyElement.classList[remove ? 'remove' : 'add']('g-modal--open');

    if (remove) {
      htmlElement.removeAttribute('style');
    } else {
      htmlElement.style.marginTop = `-${window.pageYOffset}px`;
    }

    if (this.currentScrollY) {
      window.scrollTo(0, this.currentScrollY);
    }
  }

  protected getUiView(): Array<HTMLElement> {
    return [document.querySelector('html'), document.querySelector('body')];
  }
}
