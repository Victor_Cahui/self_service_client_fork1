import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfButtonModule } from '../../forms/button';
import { MfModalTermsComponent } from './modal-terms.component';

@NgModule({
  imports: [CommonModule, MfButtonModule],
  declarations: [MfModalTermsComponent],
  exports: [MfModalTermsComponent]
})
export class MfModalTermsModule {}
