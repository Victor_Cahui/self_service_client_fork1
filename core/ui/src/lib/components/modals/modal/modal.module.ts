import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfButtonModule } from '../../forms/button';
import { MfModalComponent } from './modal.component';

@NgModule({
  imports: [CommonModule, MfButtonModule],
  declarations: [MfModalComponent],
  exports: [MfModalComponent]
})
export class MfModalModule {}
