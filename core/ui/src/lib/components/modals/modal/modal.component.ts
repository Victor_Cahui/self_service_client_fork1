// tslint:disable: invalid-void
import { DOCUMENT } from '@angular/common';
import {
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Inject,
  Input,
  OnChanges,
  OnDestroy,
  Output,
  ViewChild
} from '@angular/core';
import { PlatformService } from '@mx/core/shared/helpers/util/platform.service';

@Component({
  selector: 'mf-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  providers: [PlatformService]
})
export class MfModalComponent implements OnChanges, OnDestroy {
  /** inputs */
  @Input() confirm = false;
  @Input() cancelButtonText = 'Cancelar';
  @Input() confirmButtonText = 'Guardar';
  @Input() split = true;
  @Input() titleMobile: string;
  @Input() secondTitleMobile: string;
  @Input() title: string;
  @Input() editFooter: boolean;
  @Input() actionButtonText = 'Cerrar';
  @Input() defaultEventClose = true;
  @Input() disableButton = false;
  @Input() showLoading = false;
  @Input() sizeClass = 'g-full-modal--medium';
  @Input() removeBorder = true;
  @Input() name: string;
  @Input() isAbove: boolean;
  // Para que al dar confirmar tru o false, no emita el evento closeEvent
  @Input() confirmDiferentClose = false;
  @Input() hideHeaderMobile: boolean;
  @Input() hideHeaderDescktop: boolean;
  /** emitters */
  @Output() actionEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() changeStatus: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() confirmResponse: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() beforeOpen: EventEmitter<void> = new EventEmitter<void>();
  @Output() closeEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  @ViewChild('gModal') gModal: ElementRef<HTMLDivElement>;
  @ViewChild('gModalHeader') gModalHeader: ElementRef<HTMLDivElement>;
  @ViewChild('gModalContainer') gModalContainer: ElementRef<HTMLDivElement>;
  @ViewChild('gModalWrapper') gModalWrapper: ElementRef<HTMLDivElement>;
  @ViewChild('gModalContent') gModalContent: ElementRef<HTMLDivElement>;
  @ViewChild('gModalFooter') gModalFooter: ElementRef<HTMLDivElement>;
  @ViewChild('actionButton') actionButton: ElementRef<HTMLButtonElement>;

  isDesktop: boolean;
  modalState: boolean;
  currentScrollY: number;

  constructor(
    private readonly platformService: PlatformService,
    @Inject(DOCUMENT) private readonly document: Document
  ) {}

  ngOnChanges(): void {
    this.removeBorder = !this.title;
  }

  ngOnDestroy(): void {
    this.addOrRemoveScroll(true);
  }

  open(): void {
    this.modalState = true;
    this.currentScrollY = window.scrollY;
    setTimeout(() => {
      this.isDesktop = this.platformService.isDesktop;

      this.addOrRemoveScroll();
      this.gModal.nativeElement.classList.add('open');
      if (!this.isDesktop) {
        this.gModalContent.nativeElement.style.top = `${this.gModalHeader.nativeElement.offsetHeight - 1}px`;
        this.gModalContent.nativeElement.style.bottom = `${this.gModalFooter.nativeElement.offsetHeight - 1}px`;
      } else {
        this.gModalContent.nativeElement.style.top = 'auto';
        this.gModalContent.nativeElement.style.bottom = 'auto';
      }
    }, 10);
  }

  overrideClose(): void {
    if (this.defaultEventClose) {
      this.close();
    } else {
      this.actionEvent.emit(true);
    }
  }

  close(b?: boolean): void {
    this.closeEvent.emit(b);
    this.resetPage();
  }

  resetPage(): void {
    this.addOrRemoveScroll(true);
    this.gModal.nativeElement.classList.remove('open');
    this.gModalContent.nativeElement.style.top = 'auto';
    this.gModalContent.nativeElement.style.bottom = 'auto';
    this.modalState = false;
  }

  @HostListener('window:resize', ['$event'])
  resize($event): void {
    if (this.modalState && this.gModalContent) {
      this.isDesktop = this.platformService.isDesktop;

      const isOpen = this.gModal.nativeElement.classList.contains('open');
      if (!this.isDesktop && isOpen) {
        this.gModalContent.nativeElement.style.top = `${this.gModalHeader.nativeElement.offsetHeight - 1}px`;
        this.gModalContent.nativeElement.style.bottom = `${this.gModalFooter.nativeElement.offsetHeight - 1}px`;
      } else {
        this.gModalContent.nativeElement.style.top = 'auto';
        this.gModalContent.nativeElement.style.bottom = 'auto';
      }
    }
  }

  addOrRemoveScroll(remove?: boolean): void {
    const htmlElement = this.document.querySelector('html');
    const bodyElement = this.document.querySelector('body');
    htmlElement.classList[remove ? 'remove' : 'add']('body-hidden');
    htmlElement.classList[remove ? 'remove' : 'add']('g-modal-no-scroll');
    bodyElement.classList[remove ? 'remove' : 'add']('g-modal--open');

    if (remove) {
      htmlElement.removeAttribute('style');
    } else {
      htmlElement.style.marginTop = `-${window.pageYOffset}px`;
    }

    if (this.currentScrollY) {
      window.scrollTo(0, this.currentScrollY);
    }
  }

  onConfirm(confirm: boolean): void {
    if (this.confirmDiferentClose) {
      this.confirmResponse.emit(confirm);

      if (!confirm) {
        this.resetPage();
      }
    } else {
      if (!this.disableButton) {
        this.confirmResponse.emit(confirm);
      }
      if (!confirm) {
        this.close();
      }
    }
  }
}
