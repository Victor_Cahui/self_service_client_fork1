import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ButtonLinkComponent } from './button-link.component';

@NgModule({
  imports: [CommonModule, RouterModule],
  declarations: [ButtonLinkComponent],
  exports: [ButtonLinkComponent]
})
export class ButtonLinkModule {}
