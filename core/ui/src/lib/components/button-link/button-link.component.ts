import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { REG_EX } from '@mx/settings/constants/general-values';

@Component({
  selector: 'mf-button-link',
  templateUrl: './button-link.component.html',
  styleUrls: ['./button-link.component.scss']
})
export class ButtonLinkComponent implements OnInit {
  @Input() description: string;
  @Input() route: string;

  constructor(private readonly _Router: Router) {}

  ngOnInit(): void {}

  goTo(): void {
    if (REG_EX.url.test(this.route)) {
      window.open(this.route, '_blank');

      return void 0;
    }

    if (this.route) {
      this._Router.navigate([this.route]);
    }
  }
}
