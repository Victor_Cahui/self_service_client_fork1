import { AfterViewInit, Component, ElementRef, HostListener, Input, Renderer2, ViewChild } from '@angular/core';
import { ScrollUtil } from '@mx/core/shared/helpers/util/scroll';
import { Y_AXIS } from '@mx/settings/constants/general-values';

@Component({
  selector: 'mf-button-top',
  templateUrl: './button-top.component.html'
})
export class MfButtonTopComponent implements AfterViewInit {
  @ViewChild('button') button: ElementRef;

  @Input() image: string;
  @Input() showScrollHeight = 2200;
  @Input() hideScrollHeight;
  @Input() marginBotton = 10;
  showScroll: boolean;

  constructor(private readonly renderer2: Renderer2) {}

  ngAfterViewInit(): void {
    this.renderer2.setStyle(this.button.nativeElement, Y_AXIS.BOTTOM, `${this.marginBotton}px`);
  }

  @HostListener('window:scroll', [])
  onWindowScroll(): void {
    this.hideScrollHeight = this.hideScrollHeight || this.showScrollHeight;
    const pageYOffset = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;

    if (pageYOffset > this.showScrollHeight) {
      this.showScroll = true;
    } else if (this.showScroll && pageYOffset < this.hideScrollHeight) {
      this.showScroll = false;
    }
  }

  scrollToTop(): void {
    const currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
    if (currentScroll > 0) {
      ScrollUtil.goTo(0);
    }
  }
}
