import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfButtonTopComponent } from './button-top.component';

@NgModule({
  imports: [CommonModule],
  declarations: [MfButtonTopComponent],
  exports: [MfButtonTopComponent]
})
export class MfButtonTopModule {}
