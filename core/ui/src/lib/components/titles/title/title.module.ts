import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MfTitleComponent } from './title.component';

@NgModule({
  imports: [CommonModule],
  declarations: [MfTitleComponent],
  exports: [MfTitleComponent]
})
export class MfTitleModule {}
