# Uso desde componentes:

## Si la acción va a estar en componente

```
  <mf-tab-top [tabItemsList]="list" [local]="true" (selected)="onSelectTab($event)"></mf-tab-top>
```

## Si se permite navegar entre rutas

  si la ruta es relativa (./ruta)

```
  <mf-tab-top [tabItemsList]="tabItemsList"></mf-tab-top>
```

  ó, si la ruta no es relativa

```
  <mf-tab-top [tabItemsList]="list" [relative]="false"></mf-tab-top>
```

tabItemsList, debe tener la siguiente estructura:

```
export interface ITabItem {
  name: string;
  route?: string; // ruta al seleccionar tab
  value?: number; // tab seleccionado (acción en el mismo componente)
}
```
