import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ResizeService } from '@mx/core/shared/helpers/events/resize.service';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { MfTabTopComponent } from './tab-top.component';

@NgModule({
  imports: [CommonModule, RouterModule, GoogleModule],
  declarations: [MfTabTopComponent],
  exports: [MfTabTopComponent],
  providers: [ResizeService]
})
export class MfTabTopModule {}
