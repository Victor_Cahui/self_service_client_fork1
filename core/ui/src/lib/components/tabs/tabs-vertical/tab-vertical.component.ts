import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  QueryList,
  Renderer2,
  ViewChild,
  ViewChildren
} from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { ResizeService } from '@mx/core/shared/helpers/events/resize.service';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google/google.gabase';
import { Subscription, timer } from 'rxjs';
import { delay, distinctUntilChanged } from 'rxjs/operators';

export interface ITabItem {
  name: string;
  route?: string; // ruta al seleccionar tab
  exact?: boolean;
  visible?: boolean;
  value?: any; // tab seleccionado en el mismo componente
}

@Component({
  selector: 'mf-tab-vertical',
  templateUrl: './tab-vertical.component.html'
})
export class MfTabVerticalComponent extends GaUnsubscribeBase implements OnInit, AfterViewInit {
  @ViewChild('menu') menu: ElementRef;
  @ViewChildren('item') item: QueryList<ElementRef>;

  @Input() selectLabel: string;
  @Input() options: Array<ITabItem>;
  @Input() bgColor: string;
  @Input() boxClass: string;
  @Output() selected?: EventEmitter<number> = new EventEmitter<number>();

  collapse: boolean;
  isDesktop: boolean;
  maxHeight: number;
  resizeSub: Subscription;
  routerSub: Subscription;
  selectName: string;

  constructor(
    public renderer2: Renderer2,
    public activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly resizeService: ResizeService,
    protected gaService?: GAService
  ) {
    super(gaService);
  }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe();
    this.resizeSub = this.resizeService
      .getEvent()
      .pipe(
        distinctUntilChanged(),
        delay(0)
      )
      .subscribe((width: number) => {
        this.maxHeight = this.menu.nativeElement.scrollHeight;
        // tslint:disable-next-line: deprecation
        this.isDesktop = this.resizeService.isDesktop(width);
        if (!this.isDesktop) {
          this.renderer2.setStyle(this.menu.nativeElement, 'max-height', 0);
          this.renderer2.setStyle(this.menu.nativeElement, 'overflow', 'hidden');
        } else {
          this.renderer2.setStyle(this.menu.nativeElement, 'max-height', `${this.maxHeight}px`);
          this.collapse = false;
        }
      });
    this.routerSub = this.router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        this.changeNameSelect();
      }
    });
  }

  ngAfterViewInit(): void {
    this.changeNameSelect();
  }

  toggle(): void {
    if (this.collapse) {
      this.renderer2.setStyle(this.menu.nativeElement, 'max-height', 0);
      this.collapse = false;
    } else {
      this.collapse = true;
      this.renderer2.setStyle(this.menu.nativeElement, 'max-height', `${this.maxHeight}px`);
    }
  }

  changeNameSelect(): void {
    this.item.toArray().forEach((el: ElementRef) => {
      const timer$ = timer(100).subscribe(() => {
        if (el.nativeElement.className.indexOf('active') !== -1) {
          this.selectName = el.nativeElement.textContent;
        }
        timer$.unsubscribe();
      });
    });
  }

  changeSelect(name: string): void {
    if (this.isDesktop) {
      this.selectName = name;
    } else {
      this.toggle();
      this.selectName = name;
    }
  }
}
