import { AbstractControl, FormControl, ValidatorFn } from '@angular/forms';
import { AddYears, betweenDate } from '@mx/core/shared/helpers/util/date';

// tslint:disable-next-line:max-line-length
const EMAIL_REGEXP = /^[^<>()[\]\\,;:\%#^\s@\"$&!@]+@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z0-9-]+\.)+[a-zA-Z]{2,}))$/;

export class EmailValidator {
  static isValid(control: FormControl): { [s: string]: boolean } {
    if (control.value && (control.value.length <= 5 || !EMAIL_REGEXP.test(control.value))) {
      return { invalidEmail: true };
    }

    return undefined;
  }
}

/** @ToDo podria mejorarse con un solo Regex */
export class ManyEmailValidator {
  static isValid(control: FormControl): { [s: string]: boolean } {
    const emails = control.value
      .split(';')
      .filter(v => v !== '')
      .map(v => v.trim());
    const validEmails = emails.every(k => EMAIL_REGEXP.test(k));
    if (
      (control.value && (control.value.length <= 5 || !validEmails)) ||
      control.value.trim()[control.value.length - 1] === ';'
    ) {
      return { invalidEmail: true };
    }

    return undefined;
  }
}

export class AmountValidator {
  static isValid(control: FormControl): { [s: string]: boolean } {
    const AMOUNT_REGEXP = /^[0-9]*([,.][0-9]{1,2})?$/;
    if (control.value !== '' && !AMOUNT_REGEXP.test(control.value)) {
      return { invalidAmount: true };
    }

    return undefined;
  }
  static isRangeValid(min: number, max: number, value?: number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const val = value !== undefined ? value : control.value;

      if (val !== undefined && (isNaN(val) || val < min || val > max)) {
        return { invalidRange: { min, max } };
      }

      return null;
    };
  }
}

export class RUCValidator {
  static isValid(onlyPersonal: boolean = true): any {
    return (control: AbstractControl) => {
      const RUC_REGEXP = onlyPersonal ? /^(10)[0-9]*$/ : /^(10|20)[0-9]*$/;
      if (control.value !== '' && !RUC_REGEXP.test(control.value)) {
        return { invalidRUC: true };
      }

      return undefined;
    };
  }
}

export class NumberValidator {
  static isValid(control: FormControl): { [s: string]: boolean } {
    const NUMB_REGEXP = /^[0-9]*$/;
    if (control.value !== '' && !NUMB_REGEXP.test(control.value)) {
      return { invalidNumber: true };
    }

    return undefined;
  }
}

export class AlphanumericValidator {
  static isValid(control: FormControl): { [s: string]: boolean } {
    const ALPHAN_REGEXP = /^[a-zA-Zá-úÁ-Ú0-9\s]+$/;
    if (control.value !== '' && !ALPHAN_REGEXP.test(control.value)) {
      return { invalidAlphanumeric: true };
    }

    return void 0;
  }
}

export class PhoneValidator {
  static isValid(control: FormControl): { [s: string]: boolean } {
    const PHONE_REGEXP = /^[0-9()-]*$/;
    if (
      control.value &&
      (control.value.length <= 6 || control.value.length >= 20 || !PHONE_REGEXP.test(control.value))
    ) {
      return { invalidAlphanumeric: true };
    }

    return undefined;
  }
}

export class WhiteSpaceValidator {
  static isValid(control: FormControl | AbstractControl): { [s: string]: boolean } {
    if ((control.value || '').trim().length === 0 && (control.value && control.value.length > 0)) {
      return { invalidWhitespace: true };
    }

    return undefined;
  }
}

export class LetterValidator {
  static isValid(control: FormControl): { [s: string]: boolean } {
    const LETTER_REGEXP = /^[a-zA-ZñÑá-úÁ-Ú ]*$/g;
    if (control.value !== '' && !LETTER_REGEXP.test(control.value)) {
      return { invalidlLetter: true };
    }

    return undefined;
  }
}

export class MatchValidator {
  static validate(firstControlName, secondControlName): any {
    return (AC: AbstractControl) => {
      const firstControlValue = AC.get(firstControlName).value; // to get value in input tag
      const secondControlValue = AC.get(secondControlName).value; // to get value in input tag
      if (firstControlValue !== secondControlValue) {
        AC.get(secondControlName).setErrors({ matchFields: true });
      } else {
        return null;
      }
    };
  }
}

export class SameDocNumberValidator {
  static validate(userDocumentType, currentDocumentType, currentDocumentNumber): any {
    return (AC: AbstractControl) => {
      if (userDocumentType === currentDocumentType && AC.value === currentDocumentNumber) {
        return { sameDocNumber: true };
      }

      return null;
    };
  }
}

export class CardValidator {
  static isValid(control: FormControl): { [s: string]: boolean } {
    const CARD_REGEXP = /^(4|51|52|53|54|55|36|34|37)/;
    if (control.value !== '' && control.value.length >= 2 && !CARD_REGEXP.test(control.value)) {
      return { invalidCard: true };
    }

    return undefined;
  }
}

export class MinLengthCopyValidator {
  static validate(minLength: number, copyLength: number): any {
    return (AC: AbstractControl) => {
      if (AC.value.length < minLength) {
        return { minLengthCopy: copyLength };
      }

      return null;
    };
  }
}

export class DiffControlValidator {
  static validate(formControl: AbstractControl): (c: AbstractControl) => { readonly [key: string]: any } | null {
    return (c: AbstractControl): { readonly [key: string]: any } | null => {
      const value = c.value || '';
      const valueToCompare = formControl.value || '';

      return value === valueToCompare ? { diff: true } : null;
    };
  }
}

export class CardExpiredDateValidator {
  static validate(years: number): any {
    return (AC: AbstractControl) => {
      if (AC.value.length === 5) {
        const today = new Date();
        today.setHours(0, 0, 0, 0);
        today.setDate(1);

        const year: string = `${today.getFullYear()}`.slice(0, 2);
        const value = AC.value.replace('/', `/01/${year}`);
        const date = new Date(value);

        const limitDate = AddYears(today, years);
        limitDate.setMonth(11);
        limitDate.setDate(31);
        limitDate.setHours(23, 0, 0, 0);

        const isValid = betweenDate(today, limitDate, date);
        if (!isValid) {
          return { cardExpiredDate: true };
        }
      }

      return null;
    };
  }
}

export class PasswordValidator {
  static isValid(error: string): any {
    return (control: AbstractControl) => {
      const pwd = control.value;
      let c = 0;
      const PASSWORD_REGEXP = /^[a-zA-ZñÑ0-9!"#$%&'()*+_,-./:;<=>?@]{3,30}$/;
      if (PASSWORD_REGEXP.test(pwd)) {
        const hasUppercase = PasswordValidator.hasUppercase(pwd);
        const hasLowercase = PasswordValidator.hasLowercase(pwd);
        const hasNumber = PasswordValidator.hasNumber(pwd);
        const hasSymbol = PasswordValidator.hasSymbol(pwd);

        if (hasUppercase) {
          c = c + 1;
        }
        if (hasLowercase) {
          c = c + 1;
        }
        if (hasNumber) {
          c = c + 1;
        }
        if (hasSymbol) {
          c = c + 1;
        }

        if (c < 2) {
          return { custom: error };
        }
      } else {
        return { custom: error };
      }

      return null;
    };
  }
  // Mayusculas
  static hasUppercase(pwd): boolean {
    for (let i = 0; i < pwd.length; i++) {
      if ((pwd.charCodeAt(i) >= 65 && pwd.charCodeAt(i) <= 90) || pwd.charCodeAt(i) === 209) {
        return true;
      }
    }

    return false;
  }
  // Minusculas
  static hasLowercase(pwd): boolean {
    for (let i = 0; i < pwd.length; i++) {
      if ((pwd.charCodeAt(i) >= 97 && pwd.charCodeAt(i) <= 122) || pwd.charCodeAt(i) === 241) {
        return true;
      }
    }

    return false;
  }
  // Numeros
  static hasNumber(pwd): boolean {
    for (let i = 0; i < pwd.length; i++) {
      if (pwd.charCodeAt(i) >= 48 && pwd.charCodeAt(i) <= 57) {
        return true;
      }
    }

    return false;
  }
  // Simbolos
  static hasSymbol(pwd): boolean {
    for (let i = 0; i < pwd.length; i++) {
      if (
        (pwd.charCodeAt(i) >= 33 && pwd.charCodeAt(i) <= 47) ||
        (pwd.charCodeAt(i) >= 58 && pwd.charCodeAt(i) <= 64) ||
        pwd.charCodeAt(i) === 95
      ) {
        return true;
      }
    }

    return false;
  }
}
