import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GoogleModule } from '@mx/core/shared/helpers/util/google';
import { RouterModule } from '../../../../../node_modules/@angular/router';
import { LinkGreenComponent } from './link-green/link-green.component';

@NgModule({
  imports: [CommonModule, GoogleModule, RouterModule],
  declarations: [LinkGreenComponent],
  exports: [LinkGreenComponent]
})
export class LinksModule {}
