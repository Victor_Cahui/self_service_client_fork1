import { Component, Input } from '@angular/core';
import { GAService } from '@mx/core/shared/helpers/util/google';
import { GaUnsubscribeBase } from '@mx/core/shared/helpers/util/google/google.gabase';
@Component({
  selector: 'mf-link-green',
  templateUrl: './link-green.component.html'
})
export class LinkGreenComponent extends GaUnsubscribeBase {
  @Input() text: string;
  @Input() routerLink: Array<string>;
  @Input() center = true;
  _icon: string;

  constructor(protected gaService?: GAService) {
    super(gaService);
  }

  get icon(): string {
    return this._icon;
  }

  @Input() set icon(icon: string) {
    this._icon = icon;
  }
}
