import { Pipe, PipeTransform } from '@angular/core';
import { isEmpty } from 'lodash-es';

@Pipe({ name: 'dateInLine' })
export class DateInLinePipe implements PipeTransform {
  // MIE 15 ABR - 9:30am
  private _convertMonth(month: number): string {
    const listMonths = ['ENE', 'FEB', 'MAR', 'ABR', 'MAY', 'JUN', 'JUL', 'AGO', 'SET', 'OCT', 'NOV', 'DIC'];

    return listMonths[month - 1];
  }
  private _convertDay(day: number): string {
    const listMonths = ['LUN', 'MAR', 'MIE', 'JUE', 'VIE', 'SAB', 'DOM'];

    return listMonths[day - 1];
  }
  calculateNumberDay(myDate: string): string {
    if (!isEmpty(myDate)) {
      const numDay = myDate.split('-')[2];

      return numDay.split(' ')[0];
    }

    return '';
  }
  calculateNameDay(myDate: string): string {
    if (!isEmpty(myDate)) {
      const date = new Date(myDate).getDay();

      return this._convertDay(date);
    }

    return '';
  }

  calculateNameMonth(myDate: string): string {
    if (!isEmpty(myDate)) {
      return this._convertMonth(parseInt(myDate.split('-')[1], 10));
    }

    return '';
  }
  calculateHour(myDate: string): string {
    if (!isEmpty(myDate)) {
      const numDay = myDate.split('-')[2];
      const hourDay = numDay.split(' ')[1];

      return hourDay.substr(0, 5);
    }

    return '';
  }
  transform(date: string): string {
    const dateInLine = `${this.calculateNameDay(date)}
                        ${this.calculateNumberDay(date)}
                        ${this.calculateNameMonth(date)} - ${this.calculateHour(date)}`;

    return dateInLine;
  }
}
