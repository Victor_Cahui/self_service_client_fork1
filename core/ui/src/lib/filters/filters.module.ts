import { NgModule } from '@angular/core';
import { DateInLinePipe } from './date/date-in-line.pipe';

@NgModule({
  exports: [DateInLinePipe],
  declarations: [DateInLinePipe]
})
export class FiltersModule {}
