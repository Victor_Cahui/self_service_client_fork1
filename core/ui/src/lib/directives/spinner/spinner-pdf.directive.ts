import { Directive, ElementRef, Input, OnChanges, OnInit, Renderer2 } from '@angular/core';

@Directive({ selector: '[spinnerPdf]' })
export class SpinnerPDFDirective implements OnInit, OnChanges {
  // tslint:disable-next-line:no-input-rename
  @Input('spinnerPdf') showSpinner: boolean;
  @Input() defaultSize = true;
  @Input() iconSpinner = 'position-relative spinner g-mx--10';
  @Input() iconPDF = 'icon-mapfre_093_pdf';

  constructor(public element: ElementRef, public renderer: Renderer2) {}

  ngOnInit(): void {
    this.setClassElement();
  }

  ngOnChanges(): void {
    this.setClassElement();
  }

  private setClassElement(): void {
    this.iconPDF += ' g-c--pointer';
    this.defaultSize && (this.iconPDF += ' g-font--21');
    const el = this.element.nativeElement;
    const classEl = this.showSpinner ? this.iconSpinner : this.iconPDF;
    this.renderer.setAttribute(el, 'class', classEl);
  }
}
