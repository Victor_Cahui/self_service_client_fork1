import { Directive, ElementRef, Input, OnChanges, Renderer2 } from '@angular/core';

@Directive({
  selector: '[spinnerInner]'
})
export class SpinnerInnerDirective implements OnChanges {
  @Input() spinnerInner: boolean;

  private readonly classSpinner: string;

  constructor(private readonly element: ElementRef, private readonly renderer2: Renderer2) {
    this.classSpinner = 'g-full-form--load';
  }

  ngOnChanges(): void {
    this.spinnerInner = this.spinnerInner || false;
    const elem = this.element.nativeElement;
    if (this.spinnerInner) {
      this.renderer2.addClass(elem, this.classSpinner);
    } else {
      this.renderer2.removeClass(elem, this.classSpinner);
    }
  }
}
