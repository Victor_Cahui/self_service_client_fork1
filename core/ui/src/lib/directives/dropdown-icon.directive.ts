import { Directive, ElementRef, Input, OnChanges, Renderer2, SimpleChanges } from '@angular/core';

@Directive({
  selector: '[dropdown-icon]'
})
export class DropdownIconDirective implements OnChanges {
  // tslint:disable-next-line:no-input-rename
  @Input('dropdown-icon') collapse: boolean;

  constructor(private readonly elementRef: ElementRef, private readonly renderer2: Renderer2) {}

  ngOnChanges(changes: SimpleChanges): void {
    this.renderer2[this.collapse ? 'addClass' : 'removeClass'](
      this.elementRef.nativeElement,
      'icon-mapfre_101_ico-dow'
    );
    this.renderer2[!this.collapse ? 'addClass' : 'removeClass'](
      this.elementRef.nativeElement,
      'icon-mapfre_102_ico-up'
    );
  }
}
