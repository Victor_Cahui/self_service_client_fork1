import { AfterViewInit, Directive, EventEmitter, HostListener, Output } from '@angular/core';

function _windowRoot(): any {
  return window;
}

@Directive({
  selector: '[collapse-mobile]'
})
export class CollapseMobileDirective implements AfterViewInit {
  // tslint:disable-next-line:no-output-rename
  @Output('collapse-mobile') collapse: EventEmitter<boolean>;
  isDiffDesktop: boolean;

  constructor() {
    this.collapse = new EventEmitter<boolean>();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event): void {
    this.toggle();
  }

  ngAfterViewInit(): void {
    this.toggle();
  }

  toggle(): void {
    const isDiffDesktop = !this.isDesktop(this.getNativeWindow().innerWidth);
    if (this.isDiffDesktop !== isDiffDesktop) {
      this.isDiffDesktop = isDiffDesktop;
      setTimeout(() => {
        this.collapse.emit(this.isDiffDesktop);
      }, 100);
    }
  }

  getNativeWindow(): any {
    return _windowRoot();
  }

  isDesktop(width: number): boolean {
    return width > 991;
  }
}
