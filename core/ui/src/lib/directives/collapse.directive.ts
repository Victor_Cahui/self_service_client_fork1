import { animate, AnimationBuilder, AnimationFactory, AnimationPlayer, style } from '@angular/animations';
import {
  AfterViewInit,
  Directive,
  ElementRef,
  HostBinding,
  Input,
  OnChanges,
  Renderer2,
  SimpleChanges
} from '@angular/core';

@Directive({
  selector: '[collapse]'
})
export class CollapseDirective implements AfterViewInit, OnChanges {
  @Input() duration = 200;
  @Input() collapse: boolean;
  @Input() disableCollapse: boolean;
  @HostBinding('attr.aria-expanded') aria_expanded: boolean;
  @HostBinding('attr.aria-hidden') aria_hidden: boolean;

  private collapseAnimation: AnimationFactory;
  private expandedAnimation: AnimationFactory;

  constructor(
    private readonly animationBuilder: AnimationBuilder,
    private readonly elementRef: ElementRef,
    private readonly renderer2: Renderer2
  ) {}

  ngAfterViewInit(): void {
    this.expandedAnimation = this.animationBuilder.build([
      style({ height: 0, overflow: 'hidden' }),
      animate(this.duration, style({ height: `*` }))
    ]);

    this.collapseAnimation = this.animationBuilder.build([
      style({ height: `*` }),
      animate(this.duration, style({ height: 0, overflow: 'hidden' }))
    ]);

    this.toggle(true);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!changes.collapse.isFirstChange()) {
      this.toggle(false);
    }
  }

  toggle(init: boolean = true): void {
    if (!this.disableCollapse) {
      const el = this.elementRef.nativeElement;
      let player: AnimationPlayer;
      if (this.collapse) {
        player = this.collapseAnimation.create(el);
        player.onDone(() => {
          this.renderer2.setStyle(el, 'height', 0);
          this.renderer2.setStyle(el, 'overflow', 'hidden');
        });
      } else {
        this.renderer2.removeStyle(el, 'height');
        this.renderer2.removeStyle(el, 'overflow');
        player = this.expandedAnimation.create(el);
      }
      player.onDone(() => {
        player.destroy();
      });
      if (init) {
        player.finish();
      } else {
        this.aria_hidden = this.collapse;
        this.aria_expanded = !this.collapse;
        player.play();
      }
    }
  }
}
