import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[NumberLetter]'
})
export class NumberLetterDirective {
  private readonly regex: RegExp = new RegExp(/^[a-zA-Z0-9]*$/g);
  private readonly specialKeys: Array<string> = ['Backspace', 'Tab', 'End', 'Home'];

  @Input() NumberLetter: boolean;

  constructor(private readonly el: ElementRef) {}

  @HostListener('keypress', ['$event'])
  onKeyPress(event: KeyboardEvent): any {
    if (this.NumberLetter) {
      if (this.specialKeys.indexOf(event.key) !== -1) {
        return;
      }
      if (this.el) {
        const current: string = this.el.nativeElement.value ? this.el.nativeElement.value : '';
        const next: string = current.concat(event.key);
        if (next && !String(next).match(this.regex)) {
          event.preventDefault();
        }
      }
    }
  }
}
