import { convert, Unity } from './size-file';
export class FileItem {
  file: File;
  name: string;
  url: string;
  unit: Unity;
  constructor(obj?: any) {
    this.file = obj && obj.file;
    this.name = obj && obj.file.name;
    this.url = obj && obj.url;
    this.unit = !!obj.size ? convert(obj.size) : convert(this.file.size);
  }
}
