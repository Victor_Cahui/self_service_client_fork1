import { Directive, ElementRef, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { GeneralLang } from '@mx/settings/lang/general.lang';
import { FileItem } from './file-item.class';
import { getFile64, isImage, isValidSize } from './image';

export interface IErrorEvent {
  type: string;
  message: string;
}
@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[NgDropFiles]'
})
export class NgDropFilesDirective {
  @Input() maxMult: number;
  @Output() readonly enter: EventEmitter<boolean>;
  @Output() readonly fileUpload: EventEmitter<Array<FileItem>>;
  @Output() readonly errorEvent: EventEmitter<IErrorEvent>;

  constructor(public element: ElementRef) {
    this.enter = new EventEmitter<boolean>();
    this.fileUpload = new EventEmitter<Array<FileItem>>();
    this.errorEvent = new EventEmitter<IErrorEvent>();
  }

  @HostListener('dragenter', ['$event']) public onDragEnter(): void {
    this.enter.emit(true);
  }

  @HostListener('dragleave', ['$event']) public onDragLeave(): void {
    this.enter.emit(false);
  }

  @HostListener('dragover', ['$event']) public onDragOver(): void {
    this.enter.emit(true);
    this.preventEvent(event);
  }

  @HostListener('drop', ['$event']) public onDrop(event: any): void {
    const transferencia = this.getTransferencia(event);
    if (!transferencia) {
      return;
    }
    this.addFiles(transferencia);
    this.enter.emit(false);
    this.preventEvent(event);
  }

  @HostListener('change', ['$event']) public onchange(event: any): void {
    if (!event.target.files) {
      return;
    }
    this.addFiles(event.target);
    this.enter.emit(false);
    this.preventEvent(event);
    event.srcElement.value = '';
  }

  getFiles(obj: Array<any>): Array<FileItem> {
    const files = [];
    obj.forEach((item: any) => {
      files.push(new FileItem(item));
    });

    return [...files];
  }

  protected addFiles(target): void {
    const files = target.files as FileList;
    const promises = [];
    const error: IErrorEvent = {
      type: null,
      message: null
    };
    for (const property of Object.getOwnPropertyNames(files)) {
      const temp = files[property];
      if (!isImage(temp.type)) {
        target.value = '';
        error.type = 'type';
        error.message = GeneralLang.Messages.ErrorImageFormat;
        break;
      }
      if (!isValidSize(temp.size, this.maxMult)) {
        target.value = '';
        error.type = 'size';
        error.message = `${GeneralLang.Messages.ErrorMaxMB.start}${this.maxMult}${GeneralLang.Messages.ErrorMaxMB.end}`;
        break;
      }
      promises.push(getFile64(temp));
    }
    if (!!error.type) {
      this.errorEvent.next(error);
    } else {
      Promise.all(promises).then((result: Array<any>) => {
        const images = this.getFiles(result);
        this.fileUpload.emit(images);
      });
    }
  }

  protected preventEvent(event: any): void {
    event.preventDefault();
    event.stopPropagation();
  }

  protected getTransferencia(event: any): any {
    return event.dataTransfer ? event.dataTransfer : event.originalEvent.dataTransfer;
  }
}
