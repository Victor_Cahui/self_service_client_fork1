import { Renderer2 } from '@angular/core';

export const getFile64 = (file: File): Promise<any> => {
  return new Promise((resolve, reject) => {
    const reader: FileReader = new FileReader();
    reader.onloadend = () => {
      resolve({
        file,
        url: reader.result
      });
    };
    reader.readAsDataURL(file);
  });
};

export const isImage = (file: string): boolean => {
  return file === '' || file === undefined ? false : file === 'image/jpeg' || file === 'image/png';
};

export const isValidSize = (size: number, multi?: number): boolean => {
  return size > Math.pow(2, 20) * (!!multi ? multi : 20) ? false : true;
};

const stringToBytesFaster = (str: string) => {
  // http://stackoverflow.com/questions/1240408/reading-bytes-from-a-javascript-string
  let ch;
  let st;
  const re = [];
  let j = 0;
  for (let i = 0; i < str.length; i++) {
    ch = str.charCodeAt(i);
    if (ch < 127) {
      // tslint:disable-next-line:no-bitwise
      re[j++] = ch & 0xff;
    } else {
      st = []; // clear stack
      do {
        // tslint:disable-next-line:no-bitwise
        st.push(ch & 0xff); // push byte to stack
        // tslint:disable-next-line:no-bitwise
        ch = ch >> 8; // shift value down by 1 byte
      } while (ch);
      // add stack contents to result
      // done because chars have "wrong" endianness
      st = st.reverse();
      // tslint:disable-next-line:prefer-for-of
      for (let k = 0; k < st.length; k += 1) {
        re[j++] = st[k];
      }
    }
  }

  // return an array of bytes
  return re;
};

export const fileEnabledUpload = (file: File, multi?: number): boolean => {
  return isImage(file.type) && isValidSize(file.size, multi);
};

export const getImageCompressFile64 = (file: File, renderer2: Renderer2): Promise<any> => {
  return new Promise((resolve, reject) => {
    const reader: FileReader = new FileReader();
    reader.onloadend = () => {
      const image: HTMLImageElement = new Image();
      image.onload = () => {
        const canvas: HTMLCanvasElement = renderer2.createElement('canvas');
        const maxSize = 400;
        let width = image.width;
        let height = image.height;
        if (width > height) {
          if (width > maxSize) {
            height *= maxSize / width;
            width = maxSize;
          }
        } else {
          if (height > maxSize) {
            width *= maxSize / height;
            height = maxSize;
          }
        }
        canvas.width = width;
        canvas.height = height;
        canvas.getContext('2d').drawImage(image, 0, 0, width, height);
        const dataUrl = canvas.toDataURL(file.type);
        resolve({
          file,
          url: dataUrl,
          size: stringToBytesFaster(dataUrl).length
        });
      };
      image.src = reader.result as string;
    };
    reader.readAsDataURL(file);
  });
};
